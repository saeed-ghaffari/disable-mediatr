﻿using System;
using System.Threading.Tasks;
using Komodo.Caching.Abstractions;
using Komodo.Tests.Common;
using Xunit;

namespace Komodo.Caching.Redis.Tests
{
    [Collection(DbFixtureCollection.Name)]
    public class RedisCacheProviderTests
    {
        private readonly RedisCacheProvider _cacheProvider;

        public RedisCacheProviderTests(MockDbFixture mockDb)
        {
            _cacheProvider = new RedisCacheProvider(mockDb.Database);
        }

        [Fact]
        public async Task StoreAndFetchAsync()
        {
            var test = "VALUE";

            await _cacheProvider.StoreAsync("KEY", test, TimeSpan.FromSeconds(10), null);

            CacheItem<string> result = await _cacheProvider.FetchAsync<string>("KEY");

            Assert.Equal(test, result.Data);
        }

        [Fact]
        public async Task StoreAndRemoveAsync()
        {
            var test = "Value";

            await _cacheProvider.StoreAsync("KEY", test, TimeSpan.FromSeconds(10), null);

            await _cacheProvider.RemoveAsync("KEY");

            CacheItem<string> result = await _cacheProvider.FetchAsync<string>("KEY");

            Assert.Null(result);
        }

        [Fact]
        public async Task StoreAndRemoveAtAsync()
        {
            var test = "Value";

            await _cacheProvider.StoreAsync("KEY", test);

            await _cacheProvider.RemoveAsync("KEY",
                removeAt: TimeSpan.FromMilliseconds(50));

            CacheItem<string> result = await _cacheProvider.FetchAsync<string>("KEY");

            Assert.NotNull(result);

            await Task.Delay(50);

            result = await _cacheProvider.FetchAsync<string>("KEY");

            Assert.Null(result);
        }

        [Fact]
        public async Task StoreAndFetchWithReconstructTimeAsync()
        {
            var test = new TestModel
            {
                Long = 2323,
                TimeSpan = TimeSpan.MaxValue,
                DateTime = DateTime.Now,
                String = "STRING",
            };

            await _cacheProvider.StoreAsync(
                key: "KEY",
                value: test,
                expiration: TimeSpan.Zero,
                ttl: TimeSpan.FromSeconds(2000));

            await Task.Delay(1000);

            // this should increment expiration time of the cached item by 10 seconds
            // but return the already expired item
            CacheItem<TestModel> expiredResult
                = await _cacheProvider.FetchAsync<TestModel>(
                    key: "KEY",
                    reconstructWindow: TimeSpan.FromSeconds(10), 
                    expiration: TimeSpan.FromSeconds(10));

            Assert.True(expiredResult.IsExpired);
            Assert.Equal(test, expiredResult.Data);


            CacheItem<TestModel> recnstructedResult
                = await _cacheProvider.FetchAsync<TestModel>("KEY");

            Assert.False(recnstructedResult.IsExpired);
            Assert.Equal(test, recnstructedResult.Data);
        }
    }
}

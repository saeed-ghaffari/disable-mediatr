﻿using System;
using System.Threading.Tasks;
using Komodo.Caching.Abstractions;
using Komodo.Tests.Common;
using StackExchange.Redis;
using Xunit;

namespace Komodo.Caching.Redis.Tests
{

    public class NullCacheProviderTests
    {
        private readonly ICacheProvider _nullCacheProvider;

        public NullCacheProviderTests()
        {
            _nullCacheProvider = new NullCacheProvider();
        }
        
        [Fact]
        public async Task StoreAndFetchNullProvider()
        {
            await _nullCacheProvider.StoreAsync("KEY", "VALUE");

            var fetched = await _nullCacheProvider.FetchAsync<string>("KEY");

            Assert.Null(fetched);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test.Common
{
    [Serializable]
    public class TestModel
    {
        public int PublicInt;
        public long Long { get; set; }
        public string String { get; set; }
        public DateTime DateTime { get; set; }
        public TimeSpan TimeSpan { get; set; }
        public TestModel Model { get; set; }
        public List<TestModel> TestModels { get; set; }


        public int PrivateTestInt { get; private set; } = 10;

        public override bool Equals(object obj)
        {
            return obj is TestModel model &&
                   PublicInt == model.PublicInt &&
                   Long == model.Long &&
                   String == model.String &&
                   DateTime == model.DateTime &&
                   TimeSpan.Equals(model.TimeSpan) &&
                   EqualityComparer<TestModel>.Default.Equals(Model, model.Model) &&
                   (!(TestModels == null ^ model.TestModels == null)
                    || TestModels.SequenceEqual(model.TestModels));
        }

        public override int GetHashCode()
        {
            var hashCode = -470141322;
            hashCode = hashCode * -1521134295 + PublicInt.GetHashCode();
            hashCode = hashCode * -1521134295 + Long.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(String);
            hashCode = hashCode * -1521134295 + DateTime.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<TimeSpan>.Default.GetHashCode(TimeSpan);
            hashCode = hashCode * -1521134295 + EqualityComparer<TestModel>.Default.GetHashCode(Model);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<TestModel>>.Default.GetHashCode(TestModels);
            hashCode = hashCode * -1521134295 + PrivateTestInt.GetHashCode();
            return hashCode;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xunit;

namespace Komodo.Redis.StackExchange.ProtoBuf.Tests
{
    public class ProtobufSerializerTests
    {
        private readonly ISerializer _serializer = new ProtobufSerializer();

        [Fact]
        public Task SerializeObject()
        {
            var testModel = new ContractModel
            {
                Long = 2323,
                TimeSpan = TimeSpan.MaxValue,
                DateTime = DateTime.MaxValue,
                String = "dsfsdfsdf",
                TestModels = new List<ContractModel>()
                {
                    new ContractModel()
                },
                Model = new ContractModel()
                {
                    TestModels = new List<ContractModel>()
                    {
                        new ContractModel()
                    }
                },
                PublicInt = 23
            };


            var serialized = _serializer.Serialize(testModel);

            var deserialized = _serializer.Deserialize<ContractModel>(serialized);

            Assert.Equal(testModel, deserialized);

            return Task.CompletedTask;
        }

        [Fact]
        public Task SerializeNoContractObject()
        {
            var testModel = new NoContractModel()
            {
                Long = 2323,
                TimeSpan = TimeSpan.MaxValue,
                DateTime = DateTime.Now,
                String = "dsfsdfsdf",
                TestModels = new List<NoContractModel>()
                {
                    new NoContractModel()
                },
                Model = new NoContractModel()
                {
                    TestModels = new List<NoContractModel>()
                    {
                        new NoContractModel()
                    }
                },
                PublicInt = 23
            };
            var fields = typeof(NoContractModel)
                .GetProperties(BindingFlags.Public 
                               | BindingFlags.Instance 
                               | BindingFlags.GetProperty
                               | BindingFlags.SetProperty)
                .Select(p => p.Name)
                .ToList();

            fields.AddRange(typeof(NoContractModel)
                .GetFields(BindingFlags.Public | BindingFlags.Instance)
                .Select(f => f.Name));

            global::ProtoBuf.Meta.RuntimeTypeModel.Default.Add(typeof(NoContractModel), true)
                .Add(fields.ToArray());

            var serialized = _serializer.Serialize(testModel);

            var deserialized = _serializer.Deserialize<NoContractModel>(serialized);

            Assert.Equal(testModel, deserialized);

            return Task.CompletedTask;
        }
    }
}

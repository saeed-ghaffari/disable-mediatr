﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Cerberus.Utility.Tests
{
    public class ValidationHelperTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("sdasdasdasd")]
        [InlineData("11223344551")]
        [InlineData("1q320363185")]
        [InlineData("11320363185")]
        public void LegalUniqueIdentifierIsNotValid(string value)
        {
            Assert.False(value.IsLegalPersonUniqueIdentifierValid());
        }

        [Theory]
        [InlineData("10100018941")]
        [InlineData("10100033513")]
        [InlineData("10320363185")]
        [InlineData("10100027599")]
        public void LegalUniqueIdentifierIValid(string value)
        {
            Assert.True(value.IsLegalPersonUniqueIdentifierValid());
        }
    }
}

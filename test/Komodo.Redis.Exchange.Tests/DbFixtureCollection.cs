﻿using Komodo.Tests.Common;
using Xunit;

namespace Komodo.Redis.Exchange.Tests
{
    [CollectionDefinition(DbFixtureCollection.Name)]
    public class DbFixtureCollection : ICollectionFixture<MockDbFixture>
    {
        public const string Name = "DbFixtureCollection";
    }
}
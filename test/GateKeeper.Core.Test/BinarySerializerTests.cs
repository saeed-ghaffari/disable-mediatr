﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Komodo.Redis.StackExchange;
using Test.Common;
using Xunit;

namespace Gatekeeper.Core.Test
{
    public class BinarySerializerTests
    {
        private readonly ISerializer _serializer = new BinarySerializer();

        [Fact]
        public Task SerializeObject()
        {
            var testModel = new TestModel
            {
                Long = 2323,
                TimeSpan = TimeSpan.MaxValue,
                DateTime = DateTime.Now,
                String = "dsfsdfsdf",
                TestModels = new List<TestModel>(),
                Model = new TestModel()
                {
                    TestModels = new List<TestModel>()
                },
                PublicInt = 23
            };


            var serialized = _serializer.Serialize(testModel);

            var deserialized = _serializer.Deserialize<TestModel>(serialized);

            Assert.Equal(testModel, deserialized);

            return Task.CompletedTask;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Gatekeeper.Core.Test
{
    public class UtilityTest
    {
        [Fact]
        public void CreateRandomStringTest()
        {
            var length = 5;
            string chars = "1234567890";

            var randomList = new List<string>();


            for (int i = 0; i < 100; i++)
            {
                randomList.Add(Utility.CreateRandomString(length, chars));
            }


            Assert.Equal(randomList.Count(),randomList.Distinct().Count());
        }
    }
}
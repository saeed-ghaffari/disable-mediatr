﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Komodo.Tests.Common
{
    public class TestStruct : IEquatable<TestStruct>
    {
        public long Long { get; set; }
        public string String;

        public DateTime DateTime { get; set; }
        public TimeSpan TimeSpan { get; set; }
        public TestModel Model;
        public List<TestModel> TestModels { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as TestStruct);
        }

        public bool Equals(TestStruct other)
        {
            return other is TestStruct model &&
                   Long == other.Long &&
                   String == other.String &&
                   DateTime == other.DateTime &&
                   TimeSpan.Equals(other.TimeSpan) &&
                   EqualityComparer<TestModel>.Default.Equals(Model, model.Model) &&
                   (!(TestModels == null ^ model.TestModels == null)
                    || TestModels.SequenceEqual(model.TestModels));
        }

        public override int GetHashCode()
        {
            var hashCode = 1859791620;
            hashCode = hashCode * -1521134295 + Long.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(String);
            hashCode = hashCode * -1521134295 + DateTime.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<TimeSpan>.Default.GetHashCode(TimeSpan);
            hashCode = hashCode * -1521134295 + EqualityComparer<TestModel>.Default.GetHashCode(Model);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<TestModel>>.Default.GetHashCode(TestModels);
            return hashCode;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Repository.MsSql;
using Dapper;
using Komodo.Redis.StackExchange;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.SqlServer.Infrastructure.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;
using Newtonsoft.Json;
using File = Cerberus.Domain.Model.File;
using Profile = AutoMapper.Profile;

namespace ObjectCacheBenchMark
{
    class Program
    {
        static async Task<int> Main(string[] args)
        {
            //args[0];
            var uniqueIdentifiers = new string[]
            {
               "1142275140",
               "1142275140",
               "1142275140",
               "4650755451",
               "0073678643",
               "4310209629",
               "4313153373",
               "0021127794",
               "4570054031",
               "10100468600",
               "10103029660",
               "10720229137",
               "14007343979",

            };
            var services = new ServiceCollection();
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();

            services.AddCerberusServices(configuration);

            services.AddMapper(configuration);


            var serviceProvider = services.BuildServiceProvider();

            using (var scope = serviceProvider.CreateScope())
            {
                var repositoryService = scope.ServiceProvider.GetRequiredService<IProfileRepository>();

                for (int i = 0; i < uniqueIdentifiers.Length; i++)
                {
                    string uniqueIdentifier = uniqueIdentifiers[i];

                    Console.WriteLine(new string('#', 30));

                    Console.WriteLine($"Start {DateTime.Now.Second}-{DateTime.Now.Millisecond}");

                    var dapperProfile = await GetProfileDapper(uniqueIdentifier);

                    Console.WriteLine($"End Dapper{DateTime.Now.Second}-{DateTime.Now.Millisecond}");

                    var profile = await repositoryService
                        .GetByUniqueIdentifierAsync(uniqueIdentifier, true, true);

                    Console.WriteLine($"Repository {DateTime.Now.Second}-{DateTime.Now.Millisecond}");

                    Console.WriteLine(new string('#', 30));
                }

            }
            return 0;
        }

        public static async Task<Cerberus.Domain.Model.Profile> GetProfileDapper(string uniqueIdentifier)
        {
            using (var con = new SqlConnection("Data Source=db.sejam.dev;initial catalog=Cerberus;user ID=sejam.web;password=sejam@H9vznH2uEGMnrRfU;Connection Timeout=2;"))
            {
                var result = await con.QueryMultipleAsync("[SP_GetProfileByUniqueIdentifier] @UniqueIdentifier",
                    new { uniqueIdentifier });

                var profile = await result.ReadFirstOrDefaultAsync<Cerberus.Domain.Model.Profile>();
                if (profile == null)
                    return null;

                profile.Agent = result.Read<Agent, Cerberus.Domain.Model.Profile, PrivatePerson, Agent>((agent, agentProfile, privatePerson) =>
                {
                    agent.AgentProfile = agentProfile;
                    agent.AgentProfile.PrivatePerson = privatePerson;

                    return agent;
                }).FirstOrDefault();

                var dictFinancial = new Dictionary<long, FinancialInfo>();
                profile.FinancialInfo = result.Read<FinancialInfo, FinancialBroker, Broker, FinancialInfo>(
                    (info, infoBrokers, brokers) =>
                    {
                        if (info == null)
                            return null;

                        if (infoBrokers == null)
                            return info;

                        if (!dictFinancial.TryGetValue(info.Id, out var infoEntry))
                        {
                            infoEntry = info;
                            infoEntry.FinancialBrokers = new List<FinancialBroker>();
                            dictFinancial.Add(infoEntry.Id, infoEntry);
                        }

                        infoBrokers.Broker = brokers;

                        infoEntry.FinancialBrokers.Add(infoBrokers);

                        return infoEntry;
                    }).FirstOrDefault();

                profile.TradingCodes = (await result.ReadAsync<TradingCode>())?.ToList();

                profile.Payments = (await result.ReadAsync<Payment>())?.ToList();

                profile.Accounts = result.Read<BankingAccount, Bank, City, BankingAccount>((account, bank, city) =>
                {
                    if (account == null)
                        return null;
                    account.Bank = bank;
                    account.BranchCity = city;

                    return account;
                })?.ToList();

                profile.Addresses = result.Read<Address, Country, Province, City, AddressSection, Address>(
                    (address, country, province, city, section) =>
                    {
                        if (address == null)
                            return null;
                        address.Country = country;
                        address.Province = province;
                        address.City = city;
                        address.Section = section;

                        return address;
                    })?.ToList();

                if (profile.Type == ProfileOwnerType.IranianPrivatePerson)
                {
                    var privatePersonResult =
                        await con.QueryMultipleAsync(
                            "SP_GetProfilePrivatePersonInfoByProfileId @ProfileId,@UniqueIdentifier", new { ProfileId = profile.Id, profile.UniqueIdentifier });

                    profile.PrivatePerson = privatePersonResult.Read<PrivatePerson, File, PrivatePerson>(
                        (person, file) =>
                        {
                            if (person == null)
                                return null;

                            person.SignatureFile = file;
                            return person;
                        }).FirstOrDefault();

                    profile.JobInfo = privatePersonResult.Read<JobInfo, Job, JobInfo>((info, job) =>
                    {
                        if (info == null)
                            return null;
                        info.Job = job;
                        return info;
                    }).FirstOrDefault();

                   
                }

                if (profile.Type == ProfileOwnerType.IranianLegalPerson)
                {

                    var legalPersonResult =
                        await con.QueryMultipleAsync("SP_GetProfileLegalPersonInfoByProfileId @ProfileId",
                            new { ProfileId = profile.Id });

                    profile.LegalPerson = await legalPersonResult.ReadFirstOrDefaultAsync<LegalPerson>();

                    profile.LegalPersonShareholders = (await legalPersonResult.ReadAsync<LegalPersonShareholder>())?.ToList();


                    profile.LegalPersonStakeholders = legalPersonResult
                        .Read<LegalPersonStakeholder, Cerberus.Domain.Model.Profile, PrivatePerson,
                            LegalPersonStakeholder>(
                            (stake, stakeProfile, person) =>
                            {
                                if (stake == null)
                                    return null;

                                if (stakeProfile == null)
                                    return stake;

                                stake.StakeholderProfile = stakeProfile;
                                stake.StakeholderProfile.PrivatePerson = person;

                                return stake;
                            })?.ToList();

                    if (profile?.LegalPersonStakeholders != null)
                        foreach (var stakeholder in profile.LegalPersonStakeholders)
                        {
                            if (stakeholder?.StakeholderProfile != null)
                                stakeholder.UniqueIdentifier = stakeholder.StakeholderProfile.UniqueIdentifier;
                        }

                }

                if (profile?.Agent?.AgentProfile?.PrivatePerson != null)
                {
                    profile.Agent.UniqueIdentifier = profile?.Agent?.AgentProfile?.UniqueIdentifier;
                    profile.Agent.FirstName = profile?.Agent?.AgentProfile?.PrivatePerson?.FirstName;
                    profile.Agent.LastName = profile?.Agent?.AgentProfile?.PrivatePerson?.LastName;

                }

                return profile;
            }


        }
    }
}

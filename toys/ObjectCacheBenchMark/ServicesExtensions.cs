﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Cerberus.ApiService;
using Cerberus.ApiService.Implementations;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Repository.MsSql;
using Cerberus.Service;
using Cerberus.Service.BankService.Configuration;
using Cerberus.Service.Implementations;
using Cerberus.Service.Implementations.Inquiries;
using Cerberus.TasksManager.Core;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Repository;
using Gatekeeper.Core.Configurations;
using Gatekeeper.Core.Implementation;
using Gatekeeper.Core.Interface;
using Komodo.Caching.Abstractions;
using Komodo.Caching.Redis;
using Komodo.Redis.StackExchange;
using Komodo.Redis.StackExchange.ProtoBuf;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using File = Cerberus.Domain.Model.File;
using Profile = AutoMapper.Profile;

namespace ObjectCacheBenchMark
{
    public static class ServicesExtensions
    {
        public static void AddCerberusServices(this IServiceCollection services, IConfiguration configuration)
        {
            //Configurations

            var connectionSection = configuration.GetSection("ConnectionStrings");
            var connectionConfiguration = new ConnectionStringsConfiguration();
            connectionSection.Bind(connectionConfiguration);
            services.Configure<ConnectionStringsConfiguration>(connectionSection);

            services.Configure<MessagingConfiguration>(configuration.GetSection("MessagingConfiguration"));
            services.Configure<OtpConfiguration>(configuration.GetSection("OtpConfiguration"));
            services.Configure<JwtTokenConfiguration>(configuration.GetSection("JwtTokenConfiguration"));
            services.Configure<FileStorageConfiguration>(configuration.GetSection("FileStorageConfiguration"));
            services.Configure<ObjectCacheServicesConfiguration>(configuration.GetSection("ApiServiceConfiguration"));



            //dbcontext

            services.AddDbContextPool<CerberusDbContext>(options =>
            {
                options.UseSqlServer(connectionConfiguration.MasterDbConnection);
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

            }, 1024);

            //Redis
            var redisConfiguration = new ConfigurationOptions();
            foreach (var endpoint in connectionConfiguration.RedisWriteDbHost.Split(';'))
            {
                redisConfiguration.EndPoints.Add(endpoint);
            }

            redisConfiguration.Password = connectionConfiguration.RedisWriteDbPass;
            redisConfiguration.ConnectTimeout = 400;
            services.AddSingleton<ConfigurationOptions>(redisConfiguration);

            services.AddSingleton<IConnectionMultiplexerProvider>(
                new ConnectionMultiplexerProvider(redisConfiguration, Environment.ProcessorCount * 2));

            services.AddSingleton<IDataBaseProvider, DatabaseProvider>();

            services.AddTransient<IDatabase>(provider => provider.GetService<IDataBaseProvider>().GetWriteDatabase());

            services.AddSingleton<ISerializer, ProtobufSerializer>();

            //services.AddTransient<ICacheProvider>(provider =>
            //{
            //    return new RedisCacheProvider(
            //        provider.GetService<IDatabase>(),
            //        provider.GetService<IDatabase>(),
            //        provider.GetService<ISerializer>());
            //});

            services.AddTransient<ICacheProvider, NullCacheProvider>();

            //Repositories

            services.AddScoped<IThirdPartyServiceRepository, ThirdPartyServiceRepository>();
            services.AddScoped<IProfileRepository, ProfileRepository>();
            services.AddScoped<IAgentRepository, AgentRepository>();
            services.AddScoped<IAgentDocumentRepository, AgentDocumentRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IPrivatePersonRepository, PrivatePersonRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IBankingAccountRepository, BankingAccountRepository>();
            services.AddScoped<IBrokerRepository, BrokerRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IFinancialInfoRepository, FinancialInfoRepository>();
            services.AddScoped<IJobInfoRepository, JobInfoRepository>();
            services.AddScoped<IJobRepository, JobRepository>();
            services.AddScoped<IPrivatePersonRepository, PrivatePersonRepository>();
            services.AddScoped<ITradingCodeRepository, TradingCodeRepository>();
            services.AddScoped<IProvinceRepository, ProvinceRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IAddressSectionRepository, AddressSectionRepository>();
            services.AddScoped<IBankRepository, BankRepository>();
            services.AddScoped<IPaymentRepository, PaymentRepository>();
            services.AddScoped<ILegalPersonShareholderRepository, LegalPersonShareholderRepository>();
            services.AddScoped<ILegalPersonStakeholderRepository, LegalPersonStakeholderRepository>();
            services.AddScoped<ILegalPersonRepository, LegalPersonRepository>();
            services.AddScoped<IProfileHistoryRepository, ProfileHistoryRepository>();

            services.AddSingleton<IApiRequestLogRepository, ApiRequestLogRepository>();
            services.AddSingleton<IApiRequestLogRepository, ApiRequestLogRepository>();

            //todo:inject connection string
            Connection.SetConnectionString(connectionConfiguration.MasterDbConnection);
            services.AddScoped<ITaskRepository, SqlTaskRepository>();

            //services
            //----banking gateway
            //----Api Services
            services.AddScoped<IAddressApiService, AddressApiService>();
            services.AddScoped<IProfileApiService, ProfileApiService>();
            services.AddScoped<IBankingAccountApiService, BankingAccountApiService>();
            services.AddScoped<IFinancialInfoApiService, FinancialInfoApiService>();
            services.AddScoped<IJobInfoApiService, JobInfoApiService>();
            services.AddScoped<ILegalPersonApiService, LegalPersonApiService>();
            services.AddScoped<IPrivatePersonApiService, PrivatePersonApiService>();
            services.AddScoped<ITradingCodeApiService, TradingCodesApiService>();

            services.AddScoped<IThirdPartyServiceService, ThirdPartyServiceService>();
            services.AddScoped<IMessagingService, MessagingService>();
            services.AddScoped<IProfileService, ProfileService>();
            services.AddScoped<IAgentService, AgentService>();
            services.AddScoped<IAgentDocumentService, AgentDocumentService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IPrivatePersonService, PrivatePersonService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<IBankingAccountService, BankingAccountService>();
            services.AddScoped<IBrokerService, BrokerService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IFinancialInfoService, FinancialInfoService>();
            services.AddScoped<IJobInfoService, JobInfoService>();
            services.AddScoped<IJobService, JobService>();
            services.AddScoped<IPrivatePersonService, PrivatePersonService>();
            services.AddScoped<ITradingCodeService, TradingCodeService>();
            services.AddScoped<IBankService, BankService>();
            services.AddScoped<IProvinceService, ProvinceService>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IAddressSectionService, AddressSectionService>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IBankService, BankService>();
            services.AddScoped<ILegalPersonShareholderService, LegalPersonShareholderService>();
            services.AddScoped<ILegalPersonStakeholderService, LegalPersonStakeholderService>();
            services.AddScoped<ILegalPersonService, LegalPersonService>();
            services.AddScoped<IOfflineTaskService, OfflineTaskService>();
            services.AddScoped<ITaskRepository, SqlTaskRepository>();
            services.AddScoped<IRecurringTaskRepository, SqlRecurringTaskRepository>();
            services.AddScoped<IProfileHistoryService, ProfileHistoryService>();
            // gatekeeper
            services.AddScoped<IOtpService, OtpService>();
        }

        public static void AddMapper(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(provider =>
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<Profile, ProfileViewModel>();
                    cfg.CreateMap<PrivatePerson, PrivatePersonViewModel>();
                    cfg.CreateMap<LegalPerson, LegalPersonViewModel>();
                    cfg.CreateMap<Address, AddressViewModel>();
                    cfg.CreateMap<TradingCode, TradingCodeViewModel>();
                    cfg.CreateMap<Agent, AgentViewModel>();
                    cfg.CreateMap<BankingAccount, BankingAccountViewModel>();
                    cfg.CreateMap<JobInfo, JobInfoViewModel>();
                    cfg.CreateMap<FinancialInfo, FinancialInfoViewModel>();
                    cfg.CreateMap<LegalPersonShareholder, LegalPersonShareholderViewModel>();
                    cfg.CreateMap<LegalPersonStakeholder, LegalPersonStakeholderViewModel>();
                    cfg.CreateMap<Country, CountryViewModel>();
                    cfg.CreateMap<Province, ProvinceViewModel>();
                    cfg.CreateMap<City, CityViewModel>();
                    cfg.CreateMap<AddressSection, AddressSection>();
                    cfg.CreateMap<Broker, BrokerViewModel>();
                    cfg.CreateMap<FinancialBroker, FinancialBrokerViewModel>();
                    cfg.CreateMap<Bank, BankViewModel>();
                    cfg.CreateMap<Job, JobViewModel>();

                    cfg.CreateMap<File, FileViewModel>()
                        .ForMember(file => file.FileName,
                            exp => exp.ResolveUsing(fileViewModel =>
                                fileViewModel.FileName
                                    = $"{configuration.GetValue<string>("FileStorageConfiguration:CdnFilePath")}/{fileViewModel.FileName}"));
                });
                return config.CreateMapper();
            });
        }

    }
}
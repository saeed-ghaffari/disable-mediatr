﻿using System;
using System.Collections.Generic;
using System.Text;
using Cerberus.Domain.Interface.Repositories;
using Dapper;

namespace TestConnection
{
    public class UserRepository: IUserRepository
    {
        private readonly ISqlConnectionProvider _sqlConnectionProvider;

        public UserRepository(ISqlConnectionProvider sqlConnectionProvider)
        {

            _sqlConnectionProvider = sqlConnectionProvider;

        }

        public void GetList()
        {
            //using (var connection = _sqlConnectionProvider.GetConnection())
            //{
            //    Console.WriteLine($"Before :{connection.State}");
            //    //connection.Open();
            //    var res = connection.QueryAsync("select * from adm.user where id =5", new { id = 5 });

            //    Console.WriteLine($"After :{connection.State}");
            //    //connection.Close();
            //    //Console.WriteLine($"After Close:{connection.State}");

            //}

            var connection = _sqlConnectionProvider.GetConnection();

            Console.WriteLine($"Before :{connection.State}");
            var res = connection.QueryAsync("select * from adm.user where id =5", new { id = 5 });

            Console.WriteLine($"After :{connection.State}");




        }
    }
}

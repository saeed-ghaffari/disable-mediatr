﻿using System;
using System.Threading.Tasks;

namespace Komodo.Caching.Abstractions
{
    public interface ICacheProvider
    {
        /// <summary>
        ///     Stores an item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiration">The timespan specifying object expiration.</param>
        /// <param name="ttl">Overrides the lifetime of the object in cache. If provided, 
        /// the object will not be destroyed after expiration, instead it will be destroyed after ttl.
        /// Use TimeSpan.MaxValue to keep the object in cache forever.</param>
        /// <returns></returns>
        Task StoreAsync<T>(string key, T value, TimeSpan? expiration = null, TimeSpan? ttl = null);


        /// <summary>
        /// Fetches an item from cache. Returns null if key doesn't exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<CacheItem<T>> FetchAsync<T>(string key);

        /// <summary>
        /// Fetches an item, if reconstructWindow is not null and the cached object is expired. 
        /// the cached item expiration is automatically updated.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="reconstructWindow"></param>
        /// <param name="expiration">The timespan specifying object expiration.</param>
        /// <param name="ttl">Overrides the lifetime of the object in cache. If provided 
        /// the object will not be destroyed after expiration, instead it will be destroyed after ttl.
        /// Use TimeSpan.MaxValue to keep the object in cache forever.</param>
        /// <returns></returns>
        Task<CacheItem<T>> FetchAsync<T>(string key, TimeSpan? reconstructWindow,
            TimeSpan? expiration = null,
            TimeSpan? ttl = null);

        /// <summary>
        /// Removes a key from cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="removeAt">If not null, the key will be removed after the provided value</param>
        /// <returns></returns>
        Task RemoveAsync(string key, TimeSpan? removeAt = null);
    }
}
﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskBroker.Domain.Bus;
using TaskBroker.Domain.Commands;
using TaskBroker.Domain.Events;
using System.Linq;
using System.ComponentModel;

namespace TaskBroker.Infrastructure.Bus
{
    public sealed class RabbitMQBus : IEventBus
    {
        private readonly IMediator _mediator;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private static IConnection _rabbitMqConnection;
        private static IModel _rabbitMqChannel;
        private readonly Dictionary<string, List<Type>> _handlers;

        private string _hostname = "localhost";
        private int _port = 5672;

        public RabbitMQBus(IMediator mediator, IServiceScopeFactory serviceScopeFactory, string hostname, int port)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _serviceScopeFactory = serviceScopeFactory ?? throw new ArgumentNullException(nameof(serviceScopeFactory));
            _handlers = new Dictionary<string, List<Type>>();

            _hostname = hostname;
            _port = port;
        }


        private ConnectionFactory ConnectionFactory
        {
            get
            {
                return new ConnectionFactory
                {
                    HostName = _hostname,
                    Port = _port,
                    DispatchConsumersAsync = true
                };
            }
        }

        public Task SendCommand<TCommand>(TCommand command) where TCommand : Command
        {
            return _mediator.Send(command);
        }

        public void Publish(Event @event)
        {
            var eventName = @event.RouteKey;

            _rabbitMqConnection = _rabbitMqConnection ?? ConnectionFactory.CreateConnection();
            _rabbitMqChannel = _rabbitMqChannel ?? _rabbitMqConnection.CreateModel();
            _rabbitMqChannel.QueueDeclare(queue: eventName, durable: true, exclusive: false, autoDelete: false, arguments: null);

            var message = JsonConvert.SerializeObject(@event);
            var messageBody = Encoding.UTF8.GetBytes(message);

            _rabbitMqChannel.BasicPublish(exchange: string.Empty, routingKey: eventName, basicProperties: null, body: messageBody);
        }

        public void Subscribe<TEventHandler>(Event @event) where TEventHandler : IEventHandler
        {
            var eventName = @event.RouteKey;
            var eventHandlerType = typeof(TEventHandler);

            if (!_handlers.ContainsKey(eventName))
            {
                _handlers.Add(eventName, new List<Type>());
            }

            if (_handlers[eventName].Any(x => x.GetType() == eventHandlerType))
            {
                throw new ArgumentException($"Event handler type '{eventHandlerType}' is already registered for event '{eventName}'", nameof(eventHandlerType));
            }

            _handlers[eventName].Add(eventHandlerType);
            StartBasicConsume(@event);
        }

        private void StartBasicConsume(Event @event)
        {
            _rabbitMqConnection = _rabbitMqConnection ?? ConnectionFactory.CreateConnection();
            _rabbitMqChannel = _rabbitMqChannel ?? _rabbitMqConnection.CreateModel();
            _rabbitMqChannel.QueueDeclare(queue: @event.RouteKey, durable: true, exclusive: false, autoDelete: false, arguments: null);

            var consumer = new AsyncEventingBasicConsumer(_rabbitMqChannel);

            consumer.Received += ConsumerReceived;
            _rabbitMqChannel.BasicConsume(queue: @event.RouteKey, autoAck: true, consumer);
        }

        private async Task ConsumerReceived(object sender, BasicDeliverEventArgs @event)
        {
            var channel = ((AsyncDefaultBasicConsumer)sender).Model as IModel;

            var eventName = @event.RoutingKey;
            var message = Encoding.UTF8.GetString(@event.Body.ToArray());

            try
            {
                await ProcessEvent(eventName, message).ConfigureAwait(false);
            }
            catch (Exception)
            {
                //channel.BasicReject();
                throw;
            }
        }

        private async Task ProcessEvent(string eventName, string message)
        {
            if (!_handlers.ContainsKey(eventName))
                return;

            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var subscriptions = _handlers[eventName];

                foreach (var subscription in subscriptions)
                {
                    var handlerInstance = scope.ServiceProvider.GetService(subscription);

                    if (handlerInstance == null)
                    {
                        continue;
                    }

                    var handler = handlerInstance as IEventHandler;

                    if(handler != null)
                    {
                        var @event = JsonConvert.DeserializeObject<Event>(message);
                        await handler.Handle(@event);
                    }
                }
            }
        }
    }
}

﻿using System;
using System.Linq;

namespace Gatekeeper.Core
{
    public static class Utility
    {
        private static readonly Random Random = new Random();

        public static string CreateRandomString(int length, string chars)
        {
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }
    }
}
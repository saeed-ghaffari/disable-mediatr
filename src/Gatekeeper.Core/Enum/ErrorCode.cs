﻿namespace Gatekeeper.Core.Enum
{
    public enum ErrorCode
    {
        JwtKeyNotFound = 4011,
        InvalidJwtSignature = 4012
    }
}
﻿namespace Gatekeeper.Core.Enum
{
    public enum JwtHashAlgorithm
    {
        MD5,
        HS256,
        HS384,
        HS512
    }
}
﻿using System;
using System.Threading.Tasks;

namespace Gatekeeper.Core.Interface
{
    public interface IOtpService
    {
        /// <summary>
        /// create and store otp
        /// </summary>
        /// <param name="key"></param>
        /// <param name="ttl"></param>
        /// <returns></returns>
        Task<string> GetAsync(string key, TimeSpan ttl, string otp = null);


        /// <summary>
        /// validate otp
        /// </summary>
        /// <param name="key"></param>
        /// <param name="otp"></param>
        /// <param name="removingOtp">removed the key if the otp is valid</param>
        /// <returns></returns>
        Task<bool> IsValidAsync(string key, string otp, bool removingOtp = true);

        /// <summary>
        /// Kill Otp
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task KillAsync(string key);


        /// <summary>
        /// create and store otp
        /// </summary>
        /// <param name="key"></param>
        /// <param name="ttl"></param>
        /// <param name="length"></param>
        /// <param name="otp"></param>
        /// <param name="attempts"></param>
        /// <returns></returns>
        Task<string> CreateAsync(string key, TimeSpan ttl, int length = 6, string otp = null);
    }
}
﻿using System;
using System.Threading.Tasks;
using Gatekeeper.Core.Enum;

namespace Gatekeeper.Core.Interface
{
    public interface IJwtTokenService<T> where T : class
    {
        /// <summary>
        /// create jwt token and essential key
        /// </summary>
        /// <param name="payload">the object will be carry by token</param>
        /// <param name="ttl">time to live</param>
        /// <returns></returns>
        Task<string> CreateAsync(T payload, TimeSpan ttl);

        /// <summary>
        /// get payload from access token
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="validate">if set true ,validate signature and throw exception if failed </param>
        /// <param name="ttl">if set with not null value,refresh the jwt key until given timeSpan </param>
        /// <returns></returns>
        Task<T> RetrievePayloadFromTokenAsync(string accessToken,bool validate, TimeSpan? ttl = null);

        Task KillAsync(string token);

    }
}
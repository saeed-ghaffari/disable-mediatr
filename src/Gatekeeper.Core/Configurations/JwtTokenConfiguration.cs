﻿using Gatekeeper.Core.Enum;

namespace Gatekeeper.Core.Configurations
{
    public class JwtTokenConfiguration
    {
        public JwtHashAlgorithm Algorithm { get; set; }
    }
}
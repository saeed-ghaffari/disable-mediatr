﻿using System;
using System.Threading.Tasks;
using Gatekeeper.Core.Configurations;
using Gatekeeper.Core.Interface;
using Microsoft.Extensions.Options;
using StackExchange.Redis;

namespace Gatekeeper.Core.Implementation
{
    public class OtpService : IOtpService
    {
        private readonly OtpConfiguration _configuration;
        private readonly IDatabase _readDatabase;
        private readonly IDatabase _writeDatabase;

        public OtpService(IOptions<OtpConfiguration> configuration, IDatabase readDatabase, IDatabase writeDatabase)
        {
            _readDatabase = readDatabase;
            _writeDatabase = writeDatabase;
            _configuration = configuration.Value;
        }

        public async Task<string> GetAsync(string key, TimeSpan ttl, string otp = null)
        {
            if (otp == null)
                otp = Utility.CreateRandomString(_configuration.OtpLength, _configuration.OtpChars);

            var writeStatus = await _writeDatabase.StringSetAsync(key, HashingHelper.GenerateMD5Hash(otp), ttl);

            if (!writeStatus)
                throw new Exception("unable to save otp into write DB");

            return otp;

        }

        public async Task<bool> IsValidAsync(string key, string otp, bool removingOtp = true)
        {
            var storedKey = await _readDatabase.StringGetAsync(key);
            if (storedKey.IsNullOrEmpty)
                return false;

            var isValid = HashingHelper.GenerateMD5Hash(otp).Equals(storedKey.ToString());

            if (!isValid || !removingOtp)
                return isValid;

            await _writeDatabase.KeyDeleteAsync(key);

            return true;

        }
        public async Task KillAsync(string key)
        {
            var storedKey = await _readDatabase.StringGetAsync(key);
            if (!storedKey.IsNullOrEmpty)
                await _writeDatabase.KeyDeleteAsync(key);
        }
        public async Task<string> CreateAsync(string key, TimeSpan ttl, int length = 6, string otp = null)
        {
            if (otp == null)
                otp = Utility.CreateRandomString(length, _configuration.OtpChars);

            var writeStatus = await _writeDatabase.StringSetAsync(key, HashingHelper.GenerateMD5Hash(otp), ttl);

            if (!writeStatus)
                throw new Exception("unable to save otp into write DB");

            return otp;
        }

    }
}
﻿using System;
using System.Threading.Tasks;
using Gatekeeper.Core.Configurations;
using Gatekeeper.Core.Enum;
using Gatekeeper.Core.Interface;
using Gatekeeper.Core.Model;
using Microsoft.Extensions.Options;
using StackExchange.Redis;

namespace Gatekeeper.Core.Implementation
{
    public class JwtTokenService<T> : IJwtTokenService<T> where T : class
    {
        private const string JwtTokenPrefix = "jwtk";

        private readonly JwtTokenConfiguration _configuration;

        private readonly IDatabase _readDatabase;
        private readonly IDatabase _writeDatabase;

        public JwtTokenService(IDatabase readDatabase, IDatabase writeDatabase, IOptions<JwtTokenConfiguration> configuration)
        {
            _readDatabase = readDatabase;
            _writeDatabase = writeDatabase;
            _configuration = configuration.Value;
        }

        public async Task<string> CreateAsync(T payload, TimeSpan ttl)
        {
            var key = Guid.NewGuid().ToString("N");
            var jwtKey = $"{Guid.NewGuid():D}";

            var writeStatus = await _writeDatabase.StringSetAsync($"{JwtTokenPrefix}:{key}", jwtKey, ttl);
            if (!writeStatus)
                throw new Exception("unable to store jwt key into write DB");

            return JwtHelper.Encode(new JwtPayload<T>
            {
                CreationDate = DateTime.Now,
                Key = key,
                Payload = payload
            }, jwtKey, _configuration.Algorithm);
        }

        public Task KillAsync(string token)
        {
            var payload = JwtHelper.Decode<JwtPayload<T>>(token, string.Empty, false);
            return _writeDatabase.KeyDeleteAsync($"{JwtTokenPrefix}:{payload.Key}");
        }

        public async Task<T> RetrievePayloadFromTokenAsync(string accessToken, bool verify, TimeSpan? ttl = null)
        {
            var payload = JwtHelper.Decode<JwtPayload<T>>(accessToken, string.Empty, false);
            if(payload==null && verify)
                throw new GateKeeperException(ErrorCode.InvalidJwtSignature, $"Invalid jwt signature");

            var jwtKey = await _readDatabase.StringGetAsync($"{JwtTokenPrefix}:{payload.Key}");
            if (jwtKey.IsNullOrEmpty)
                throw new GateKeeperException(ErrorCode.JwtKeyNotFound, "the specific key expired or not found");

            var result = JwtHelper.Decode<JwtPayload<T>>(accessToken, jwtKey, verify).Payload;

            if (ttl.HasValue)
                await _writeDatabase.KeyExpireAsync($"{JwtTokenPrefix}:{payload.Key}", ttl);

            return result;
        }
    }
}

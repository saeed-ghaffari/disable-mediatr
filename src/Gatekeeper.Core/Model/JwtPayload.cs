﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Gatekeeper.Core.Model
{
    public class JwtPayload<T>
    {
        [JsonProperty(PropertyName = "cd")]
        public DateTime CreationDate { get; set; }

        [JsonProperty(PropertyName = "k")]
        public string Key { get; set; }

        [JsonProperty(PropertyName = "p")]
        public T Payload { get; set; }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Cerberus.Api.ExtraMethods
{
    public class NoPrintAttribute : Attribute { }
    public static class GetDisplayAtt
    {
        public static object ConvertToPrintObj(object parametersArr)
        {
            if (parametersArr == null)
                return null;
            var result = new List<object>();
            var classAttribute = parametersArr.GetType().GetCustomAttribute(typeof(DisplayNameAttribute), true) as DisplayNameAttribute;
            if (classAttribute != null && !string.IsNullOrEmpty(classAttribute.DisplayName))
            {
                var PropsCollection = parametersArr.GetType().GetProperties().ToList();
                Dictionary<string, object> ObjItem = new Dictionary<string, object>();
                for (int j = 0; j < PropsCollection.Count; j++)
                {
                    var PropAttribute = PropsCollection[j].GetCustomAttribute(typeof(DisplayNameAttribute), true) as DisplayNameAttribute;
                    var NoPrintAttribute = PropsCollection[j].GetCustomAttribute(typeof(NoPrintAttribute), true) as NoPrintAttribute;
                    if (PropAttribute != null && NoPrintAttribute == null)
                        if ((PropsCollection[j].GetValue(parametersArr) as IList) != null)
                        {
                            var tempItem = PropsCollection[j].GetValue(parametersArr);

                            ObjItem[PropsCollection[j].Name] = new
                            {
                                DisplayName = PropAttribute.DisplayName,
                                Value = ConvertToPrintObj(tempItem)
                            };
                        }
                        else
                        {
                            ObjItem[PropsCollection[j].Name] = new
                            {
                                DisplayName = PropAttribute.DisplayName,
                                Value = PropsCollection[j].GetValue(parametersArr)
                            };
                        }
                }
                result.Add(new
                {
                    Title = classAttribute.DisplayName,
                    data = ObjItem
                });
            }
            return result;
        }
        public static object ConvertToPrintList(object parametersArr)
        {
            var parameters = (IList)parametersArr;
            if (parameters == null)
                return null;
            var result = new List<object>();
            if (parameters != null && parameters.Count > 0)
            {
                for (int i = 0; i < parameters.Count; i++)
                {
                    object item = parameters[i];
                    var classAttribute = item.GetType().GetCustomAttribute(typeof(DisplayNameAttribute), true) as DisplayNameAttribute;
                    if (classAttribute != null && !string.IsNullOrEmpty(classAttribute.DisplayName))
                    {
                        var PropsCollection = parameters[i].GetType().GetProperties().ToList();
                        Dictionary<string, object> ObjItem = new Dictionary<string, object>();
                        for (int j = 0; j < PropsCollection.Count; j++)
                        {
                            var PropAttribute = PropsCollection[j].GetCustomAttribute(typeof(DisplayNameAttribute), true) as DisplayNameAttribute;
                            var NoPrintAttribute = PropsCollection[j].GetCustomAttribute(typeof(NoPrintAttribute), true) as NoPrintAttribute;
                            if (PropAttribute != null && NoPrintAttribute == null)
                                if ((PropsCollection[j].GetValue(item) as IList) != null)
                                {
                                    var tempItem = PropsCollection[j].GetValue(item);

                                    ObjItem[PropsCollection[j].Name] = new
                                    {
                                        DisplayName = PropAttribute.DisplayName,
                                        Value = ConvertToPrintObj(tempItem)
                                    };
                                }
                                else
                                {
                                    ObjItem[PropsCollection[j].Name] = new
                                    {
                                        DisplayName = PropAttribute.DisplayName,
                                        Value = PropsCollection[j].GetValue(item)
                                    };
                                }
                        }
                        result.Add(new
                        {
                            Title = classAttribute.DisplayName,
                            data = ObjItem
                        });
                    }
                }
            }
            return result;
        }
    }
}

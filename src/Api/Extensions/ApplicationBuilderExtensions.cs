﻿using Microsoft.AspNetCore.Builder;

namespace Cerberus.Api.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger(options =>
            {
                options.RouteTemplate = "docs/{documentName}/docs.json";
            });

            app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/docs/v0.1/docs.json", "Sejam api v0.1");
                    c.RoutePrefix = "v0.1/docs";
                    c.SupportedSubmitMethods(SubmitMethod.Delete, SubmitMethod.Get, SubmitMethod.Options, SubmitMethod.Put, SubmitMethod.Patch, SubmitMethod.Post);
                }
            );
        }

    }
}
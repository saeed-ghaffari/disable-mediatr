﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Api.Configuration
{
    public class EntryNodeConfiguration
    {
        public bool IgnoreIssuerOtp { get; set; }
        public List<int> IgnoresList { get; set; }
    }
}

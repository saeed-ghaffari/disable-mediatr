﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Api.Configuration
{
    public class LegalPersonsWhiteListConfiguration
    {
        public List<string> UniqueIdentifiers { get; set; }
    }
}

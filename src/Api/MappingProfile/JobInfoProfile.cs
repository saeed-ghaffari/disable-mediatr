﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Utility;
using Profile = AutoMapper.Profile;

namespace Cerberus.Api.MappingProfile
{
    public class JobInfoProfile:Profile
    {
        public JobInfoProfile()
        {
            CreateMap<JobInfo, AuthenticationOfficeJobInfoViewModel>()
                .ForMember(J => J.EmployeDate, d => d.MapFrom(j => j.EmploymentDate.ToPersianDate("yyyy/MM/dd")))
                .ForMember(J => J.jobDescription, d => d.MapFrom(j => j.Job.Title))
                .ForMember(J => J.CompanyFaxPrifix, d => d.MapFrom(j => j.CompanyFaxPrefix))
                .ForMember(J => J.CompanyTell, d => d.MapFrom(j => j.CompanyPhone))
                .ForMember(J => J.CompanyTellPrifix, d => d.MapFrom(j => j.CompanyCityPrefix))
                ;
        }
    }
}

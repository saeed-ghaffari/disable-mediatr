﻿using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Utility;
using System.Linq;
using Profile = AutoMapper.Profile;

namespace Cerberus.Api.MappingProfile
{
    public class FinancialInfoProfile : Profile
    {
        public FinancialInfoProfile()
        {
            CreateMap<FinancialInfo, AuthenticationOfficeFinancialInfoViewModel>()
                .ForMember(f => f.HowFamilyWithBourceDescription,
                    t => t.MapFrom(f =>
                        f.TradingKnowledgeLevel == TradingKnowledgeLevel.Low ? "بسیار کم" :
                        f.TradingKnowledgeLevel == TradingKnowledgeLevel.VeryLow ? "کم" :
                        f.TradingKnowledgeLevel == TradingKnowledgeLevel.Medium ? "متوسط" :
                        f.TradingKnowledgeLevel == TradingKnowledgeLevel.Good ? "خوب" :
                        f.TradingKnowledgeLevel == TradingKnowledgeLevel.Excellent ? "بسیار خوب" : ""))

                        .ForMember(f => f.AssesstValue, t => t.MapFrom(f => f.AssetsValue))

                        .ForMember(f => f.CExchange_Transactin, t => t.MapFrom(f => f.CExchangeTransaction))

                        .ForMember(f => f.FtransactionLevelDescription, t => t.MapFrom(f => f.TransactionLevel.GetEnumDescription()))

                        .ForMember(f => f.outExchange_Transactin, t => t.MapFrom(f => f.OutExchangeTransaction))

                        .ForMember(f => f.WorkingCompany, t => t.MapFrom(f => string.Join(",", f.FinancialBrokers.Select(v => v.Broker.Title))))

                        .ForMember(f => f.SExchange_Transactin, t => t.MapFrom(f => f.SExchangeTransaction));

        }
    }
}

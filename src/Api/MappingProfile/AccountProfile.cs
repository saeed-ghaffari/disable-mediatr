﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Utility;

namespace Cerberus.Api.MappingProfile
{
    public class AccountProfile:Profile
    {
        public AccountProfile()
        {
            CreateMap<Cerberus.Domain.Model.Profile, AuthenticationOfficeProfileViewModel>()
                //.ForMember(p => p.insertDateTime, v => v.MapFrom(p => p.CreationDate))
                .ForMember(p => p.InsertDate, v => v.MapFrom(p => p.CreationDate.ToPersianDate("yyyy/MM/dd")))
                .ForMember(p => p.PersonTypeStr, v => v.MapFrom(p => "حقیقی"))
                .ForMember(p => p.IranOrForeignStr, v => v.MapFrom(p => "ایرانی"))
                .ForMember(p => p.NationalCode, v => v.MapFrom(p => p.UniqueIdentifier))
                .ForMember(p => p.Mobile , v => v.MapFrom(p => p.Mobile.ToString()))

                ;

        }
    }
}

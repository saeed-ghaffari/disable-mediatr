﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Profile = AutoMapper.Profile;

namespace Cerberus.Api.MappingProfile
{
    public class AddressProfile : Profile
    {
        public AddressProfile()
        {
            CreateMap<Address, AuthenticationOfficeAddressViewModel>()
                .ForMember(a => a.CountryDescription, c => c.MapFrom(co => co.Country.Name))
                .ForMember(a => a.CityeDescription, c => c.MapFrom(ci => ci.City.Name))
                .ForMember(a => a.ProvinceDescription, p => p.MapFrom(pr => pr.Province.Name))
                .ForMember(a => a.Avenue, av => av.MapFrom(ave => ave.RemnantAddress))
                .ForMember(a => a.Alley, av => av.MapFrom(ave => ave.Alley))
                .ForMember(a => a.Plaque, av => av.MapFrom(ave => ave.Plaque))
                .ForMember(a => a.Telephone, t => t.MapFrom(tel => tel.Tel))
                .ForMember(a => a.CityPrifix, av => av.MapFrom(a => a.CityPrefix))
                .ForMember(a => a.CountryPrifix, av => av.MapFrom(a => a.CountryPrefix))
                .ForMember(a => a.EmergencyCountryPrifix, av => av.MapFrom(a => a.EmergencyTelCountryPrefix))
                .ForMember(a => a.EmergencyMobile, t => t.MapFrom(tel => tel.Mobile))
                .ForMember(a => a.SectionDescription, s => s.MapFrom(a => a.Section.Name))
                .ForMember(a => a.EmergencyCityPrifix, e => e.MapFrom(a => a.EmergencyTelCityPrefix))
                .ForMember(a => a.EmergencyTelephone, e => e.MapFrom(a => a.EmergencyTel))
                .ForMember(a => a.EmergencyCountryPrifix, e => e.MapFrom(a => a.EmergencyTelCountryPrefix));
        }
    }
}

﻿using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Utility;
using Profile = AutoMapper.Profile;

namespace Cerberus.Api.MappingProfile
{
    public class AgentProfile:Profile
    {
        public AgentProfile()
        {
            CreateMap<Agent, AuthenticationOfficeAgentViewModel>()
                .ForMember(a => a.PersonName, m => m.MapFrom(a => $"{a.AgentProfile.PrivatePerson.FirstName} {a.AgentProfile.PrivatePerson.LastName}"))
                .ForMember(a => a.ExpireDate, m => m.MapFrom(a => a.ExpirationDate.ToPersianDate("yyyy/MM/dd")))
                .ForMember(a => a.PersonCode, m => m.MapFrom(a => a.AgentProfile.UniqueIdentifier))
                .ForMember(a => a.TypeOfAttornyAvidenceDescription, m => m.MapFrom(a => a.Type.GetEnumDescription()))
                .ForMember(a => a.IsConfirmDescription, m => m.MapFrom(a => a.IsConfirmed));
        }
    }
}

﻿using Cerberus.Api.Model;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Api.MappingProfile
{
    public class PaymentProfile : AutoMapper.Profile
    {
        public PaymentProfile()
        {
            CreateMap<CreatePaymentRequest, Payment>()

                .ForMember(dis => dis.ClientReference, opt => opt.MapFrom(z => z.OrderId));

        }
    }
}
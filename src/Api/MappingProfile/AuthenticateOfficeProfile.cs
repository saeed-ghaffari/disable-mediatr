﻿using AutoMapper;
using Cerberus.Domain.Enum;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Utility;

namespace Cerberus.Api.MappingProfile
{
    public class AuthenticateOfficeProfile:Profile
    {
        public AuthenticateOfficeProfile()
        {
            CreateMap<Domain.Model.AuthenticationOffices, AuthenticateOfficeViewModel>()
                .ForMember(d => d.ProvinceName, s => s.MapFrom(sm => sm.Province.Name))
                .ForMember(d => d.CityName, s => s.MapFrom(sm => sm.City.Name))
                .ForMember(d => d.Type, s => s.MapFrom(x =>(OfficeType)x.Type));

        }
    }
}
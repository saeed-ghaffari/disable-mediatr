﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Utility;
using Profile = AutoMapper.Profile;

namespace Cerberus.Api.MappingProfile
{
    public class BankingAccountProfile:Profile
    {
        public BankingAccountProfile()
        {
            CreateMap<BankingAccount, AuthenticationOfficeBankingAccountViewModel>()
                .ForMember(b => b.Acc_No, v => v.MapFrom(b => b.AccountNumber))
                .ForMember(b => b.Acc_Owner, v => v.MapFrom(b => b.ProfileOwner.PrivatePerson.FirstName))
                .ForMember(b => b.Acc_Type, v => v.MapFrom(b => b.Type.GetEnumDescription()))
                .ForMember(b => b.BankName, v => v.MapFrom(b => b.Bank.Name))
                .ForMember(b => b.City, v => v.MapFrom(b => b.BranchCity.Name))
                .ForMember(b => b.BrancCode, v => v.MapFrom(b => b.BranchCode))
                .ForMember(b => b.IsDefaultDescription, v => v.MapFrom(b => b.IsDefault))
                ;

        }
    }
}

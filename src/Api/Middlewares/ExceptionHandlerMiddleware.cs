﻿using System;
using System.Threading.Tasks;
using Aspina.Model;
using Cerberus.Api.Model;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Gatekeeper.AspModules;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Cerberus.Api.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        private readonly ILogger<ExceptionHandlerMiddleware> _logger;
        private readonly RequestDelegate _next;

        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger<ExceptionHandlerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (RateLimitException exception)
            {
                context.Response.StatusCode = 429;
                context.Response.ContentType = "application/json";

                var response = new Envelop()
                {
                    Error = new EnvelopError()
                    {
                        Exception = exception.InnerException,
                        ErrorCode = (int)ErrorCode.ToManyRequest,
                        CustomMessage = exception.Message
                    },
                    Data = null
                };


                await context.Response.WriteAsync(JsonConvert.SerializeObject(response, JsonSerializerSettingsProvider.CreateSerializerSettings()));
            }
            catch (CerberusException exception)
            {
                context.Response.StatusCode = (int)exception.HttpStatusCode;
                context.Response.ContentType = "application/json";

                var response = new Envelop()
                {
                    Error = new EnvelopError()
                    {
                        Exception = exception.InnerException,
                        ErrorCode = (int)exception.ErrorCode,
                        CustomMessage = exception.CustomMessage
                    },
                    Data = exception.Value
                };


                await context.Response.WriteAsync(JsonConvert.SerializeObject(response, JsonSerializerSettingsProvider.CreateSerializerSettings()));
            }
            catch (Exception exception)
            {
                var logBody = $@"url : {context.Request.Path}{(context.Request.Headers.ContainsKey("User-Agent") ? ("user agent: " + context.Request.Headers["User-Agent"]) : "")}";

                _logger.LogCritical(exception, logBody);

                exception.Data.Add("HttpRequest", logBody);

                if (context.Response.HasStarted)
                    throw;

                context.Response.StatusCode = 500;
                context.Response.ContentType = "application/json";

                var response = new Envelop()
                {
                    Error = new EnvelopError()
                    {
                        ErrorCode = (int)ErrorCode.UnknownServerError,
                        //CustomMessage = exception.Message
                    },
                };

                var jsonSerializerSettings = JsonSerializerSettingsProvider.CreateSerializerSettings();
                jsonSerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

                await context.Response.WriteAsync(JsonConvert.SerializeObject(response, jsonSerializerSettings));
            }
        }
    }
}
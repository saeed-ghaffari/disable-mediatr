﻿using Aspina;
using Cerberus.Api.Extensions;
using Cerberus.Api.Middlewares;
using Cerberus.Utility.JsonConverters;
using Gatekeeper.AspModules.Middleware;
using GateKeeper.Auth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Cerberus.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCerberusServices(Configuration);

            services.AddGateKeeperAuthServices(Configuration);

            //background task
            services.AddHostedService<QueuedHostedService>();
            services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();

            services.AddMapper(Configuration);

            services.AddSwagger();

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.Converters.Add(new StandardDateTimeConverter());
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            services.AddApiVersioning();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (Configuration.GetSection("RequestLogConfiguration").GetValue<bool>("IsEnable"))
                app.UseMiddleware<LogRequestMiddleware>();

            app.UseMiddleware<CustomHeadersMiddleware>();

            app.UseMiddleware<ExceptionHandlerMiddleware>();

            app.UseSwagger();

            app.UseMvc();
        }
    }
}
﻿using System.Net;
using Aspina.Model;
using Cerberus.Api.Extensions;
using Cerberus.Api.Model;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Gatekeeper.AspModules.Interface;
using Gatekeeper.AspModules.Model;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Api.ApiLogic
{
    public class BaseController : Controller
    {
        public IssuerAccessTokenPayload CurrentService => Request.GetCurrentServiceAccessTokenPayload();

        public HttpStatusCode? ResponseHttpStatusCode { get; set; }
        public EnvelopMeta Meta { get; set; }

        public override void OnActionExecuting(Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext actionContext)
        {
            var feature = actionContext.HttpContext.Features.Get<IAspinaMetricFeatures>() ?? new AspinaMetricFeature();

            feature.AddTag("Controller", actionContext.ActionDescriptor.RouteValues["Controller"]);
            feature.AddTag("Action", actionContext.ActionDescriptor.RouteValues["Action"]);
            feature.AddTag("Scope", "Sejam-API");

            actionContext.HttpContext.Features.Set(feature);


            if (actionContext.ModelState.IsValid == false)
            {
                throw new CerberusException(ErrorCode.BadRequest, value: actionContext.ModelState.ToViewModel());
            }
        }

        public override void OnActionExecuted(Microsoft.AspNetCore.Mvc.Filters.ActionExecutedContext context)
        {
            var response = context.Result as ObjectResult;

            if (response?.Value == null)
                return;

            context.Result = StatusCode((int)(ResponseHttpStatusCode ?? HttpStatusCode.OK), new Envelop()
            {
                Data = response?.Value,
                Meta = Meta
            });
        }
    }
}
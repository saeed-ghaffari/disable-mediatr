﻿namespace Cerberus.Api.ApiLogic
{
    public enum OtpType
    {
        Kyc = 1,
        Authenticator = 2,
        EntryNode = 3,
        Kara=4,
        InternalService=5,
        EntryNodeStatus=6,
        PermanentKyc=7
    }
}
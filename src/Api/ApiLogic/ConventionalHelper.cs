﻿namespace Cerberus.Api.ApiLogic
{
    public static class ConventionalHelper
    {
        public static string CreateOtpKey(string msisdn, string uniqueIdentifier, OtpType type)
        {
            return $"api:{uniqueIdentifier}:{msisdn}:{type}";
        }

        public static string CreateIssuerOtpKey(string msisdn, string uniqueIdentifier, OtpType type)
        {
            return $"api:issuer:{uniqueIdentifier}:{msisdn}:{type}";
        }

        public static string CreateEntryNodeOtpKey(string msisdn)
        {
            return $"api:entryNode:{msisdn}";
        }
    }
}
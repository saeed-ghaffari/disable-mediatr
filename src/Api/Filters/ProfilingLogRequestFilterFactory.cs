﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace Cerberus.Api.Filters
{
    public class ProfilingLogRequestFilterFactory : Attribute, IFilterFactory
    {
        public int Order { get; set; }

        public IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
        {
            var filter = serviceProvider.GetService<ProfilingLogRequestAttribute>();
            filter.Order = Order;
            return filter;
        }

        public bool IsReusable => false;
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Gatekeeper.Core;
using Gatekeeper.Core.Interface;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Aspina;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Configuration;
using Cerberus.Api.Model;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Model.Log;
using Gatekeeper.AspModules;
using GateKeeper.Auth.Interface;
using GateKeeper.Auth.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Newtonsoft.Json;

namespace Cerberus.Api.Filters
{
    public class LogRequestAttribute : IAsyncActionFilter, IOrderedFilter
    {
        private readonly IBackgroundTaskQueue _queue;
        private readonly IApiRequestLogRepository _apiRequestLogRepository;
        private readonly RequestLogConfiguration _requestLogConfiguration;

        public int Order { get; set; }

        public LogRequestAttribute(
            IBackgroundTaskQueue queue,
            IApiRequestLogRepository apiRequestLogRepository,
            IOptions<RequestLogConfiguration> apiConfiguration)
        {
            _queue = queue;
            _apiRequestLogRepository = apiRequestLogRepository;
            _requestLogConfiguration = apiConfiguration.Value;
        }


        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var log = new ApiRequestLog()
            {
                Route = context.HttpContext.Request.Path,
                Method = context.HttpContext.Request.Method,
                ServiceId = long.Parse(context.HttpContext.Items["ServiceId"].ToString()),
            };

            _queue.QueueBackgroundWorkItem(token => _apiRequestLogRepository.AddAsync(log));

            //invoke next step
            await next.Invoke();
        }


    }
}
﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace Cerberus.Api.Filters
{
    public class LogRequestFilterFactory : Attribute, IFilterFactory
    {
        public int Order { get; set; }
        public IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
        {
            var filter = serviceProvider.GetService<LogRequestAttribute>();
            filter.Order = Order;
            return filter;

        }

        public bool IsReusable => false;
    }
}
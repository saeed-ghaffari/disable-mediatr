﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Aspina;
using Cerberus.Api.Configuration;
using Cerberus.Api.Extensions;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Model;
using Cerberus.Utility;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Cerberus.Api.Filters
{
    /// <summary>
    /// Attribute to log profiling request
    /// *** it can be used for any request***
    /// </summary>
    public class ProfilingLogRequestAttribute : IAsyncActionFilter, IOrderedFilter
    {
        private readonly IBackgroundTaskQueue _backgroundTaskQueue;
        private readonly IProfilingRequestLogRepository _logRepository;
        private readonly ApiConfiguration _apiConfiguration;
        public ProfilingLogRequestAttribute(IBackgroundTaskQueue backgroundTaskQueue,
            IProfilingRequestLogRepository logRepository, IOptions<ApiConfiguration> apiConfiguration)
        {
            _backgroundTaskQueue = backgroundTaskQueue;
            _logRepository = logRepository;
            _apiConfiguration = apiConfiguration.Value;
        }

        public int Order { get; set; }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!_apiConfiguration.EnableRequestLog)
            {
                await next.Invoke();
                return;
            }
            //get payload of access token if exists
            var accessToken = context.HttpContext.Request.GetCurrentServiceAccessTokenPayload();

            var requestLog = new ProfilingRequestLog
            {
                RequestDateTime = DateTime.Now,
                Route = context.HttpContext.Request.Path,
                Method = context.HttpContext.Request.Method,
                AccessTokenPayload = accessToken == null ? "{}" : JsonConvert.SerializeObject(accessToken),
                ServiceId = context.HttpContext.Items["ServiceId"] == null ? 0 : long.Parse(context.HttpContext.Items["ServiceId"].ToString()),
                SourceIp = Tools.GetUserIp(context.HttpContext.Request),
                Controller = context.RouteData.Values["Controller"].ToString(),
                Action = context.RouteData.Values["Action"].ToString(),
                QueryString = context.HttpContext.Request.QueryString.HasValue ? context.HttpContext.Request.QueryString.Value : null
            };


            //for enable to read body again because we can just read body one time in asp.net core 

            if (requestLog.Action != "UpdateProfileToSejamiAsync" && requestLog.Action != "UpdateProfileToSejamiPostAsync")
            {
                context.HttpContext.Request.EnableRewind();
                context.HttpContext.Request.Body.Position = 0;
                using (var stream = new StreamReader(context.HttpContext.Request.Body, Encoding.UTF8, true, 1024,
                       leaveOpen: true))
                {
                    var bodyAsText = stream.ReadToEnd();
                    requestLog.RequestBody = bodyAsText;
                }
                //set position of body to zero for read by action and model binding
                context.HttpContext.Request.Body.Position = 0;

            }



            //execute action and get response
            var actionExecuted = await next.Invoke();

            requestLog.ResponseDateTime = DateTime.Now;

            //means no exception thrown during executing action
            if (actionExecuted.Exception == null)
            {
                var result = actionExecuted.Result as ObjectResult;

                if (result?.Value != null)
                {
                    requestLog.Response = JsonConvert.SerializeObject(result.Value);
                    requestLog.ResponseHash = HashHelper.GenerateMd5String(requestLog.Response);
                }
            }
            //if there is a exception
            else
            {
                requestLog.HasException = true;

                if (actionExecuted.Exception is CerberusException cerberus)
                {
                    requestLog.ExceptionType = ExceptionType.CerberusException;
                    requestLog.ExceptionMessage =
                        $"{cerberus?.ErrorCode} -- {cerberus.CustomMessage} -- {cerberus.Message} -- {actionExecuted.Exception?.InnerException?.Message}";
                }
                else
                {
                    requestLog.ExceptionType = ExceptionType.Exception;
                    requestLog.ExceptionMessage =
                        $"{actionExecuted.Exception.Message} -- {actionExecuted.Exception?.InnerException?.Message}";
                }
            }

            //queue the log for asynchronous run and save in database
            _backgroundTaskQueue.QueueBackgroundWorkItem(log => _logRepository.AddAsync(requestLog));

        }


    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.ViewModel.Membership;

namespace Cerberus.Api.Model
{
    public class CreateSyncMembershipTaskRequest
    {
        [Required]
        public string CallbackUrl { get; set; }
        [Required]
        public IEnumerable<Member> Members { get; set; }
    }
}
﻿namespace Cerberus.Api.Model
{
    public class CreateAsyncTaskResponse
    {
        public string Reference { get; set; }
    }
}
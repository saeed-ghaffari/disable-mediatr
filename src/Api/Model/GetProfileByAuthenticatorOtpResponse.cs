﻿using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.Api.Model
{
    public class GetProfileByAuthenticatorOtpResponse
    {
        public ProfileViewModel Profile { get; set; }
        public string AuthenticatorOtp { get; set; }

        public string IssuerReference { get; set; }

    }
}
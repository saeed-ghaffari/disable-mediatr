﻿
using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.Enum;

namespace Cerberus.Api.Model
{
    public class CreateTradingCode
    {
        [Display(Name = "نام بورس")]
        [Range((int)(TradingType.Energy), (int)(TradingType.StockExchange), ErrorMessage = "مقدار نامعتبر است")]
        public TradingType Type { get; set; }

        [Display(Name = "کد سهام داری بخش اول")]
        [MaxLength(1, ErrorMessage = "نمی تواند بیشتر از 1 کارکتر باشد")]
        [RegularExpression(@"^\s*[مدصختک\s]+\s*$", ErrorMessage = "مجاز نمی باشد")]
        public string FirstPart { get; set; }

        [Display(Name = "کد سهام داری")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(7, ErrorMessage = "نمی تواند بیشتر از 7 کارکتر باشد")]
        public string SecondPart { get; set; }

        [Display(Name = "کد سهام داری")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(3, ErrorMessage = "{0} نمی تواند بیشتر از 3 کارکتر باشد")]
        public string ThirdPart { get; set; }
    }
}
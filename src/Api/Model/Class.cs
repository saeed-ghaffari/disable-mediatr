﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Api.Model
{
    public class GetSubscriberCountResponse
    {
        public int Count { get; set; }
    }
}

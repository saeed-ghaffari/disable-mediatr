﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Api.Model
{
    public class ProfileHistoryViewModel
    {
        public long Id { get; set; }

        public string ReferenceId { get; set; }

        public string PostId { get; set; }

        public DateTime CreationDate { get; set; }

        public ProfileCompactViewModel Profile { get; set; }
    }
}

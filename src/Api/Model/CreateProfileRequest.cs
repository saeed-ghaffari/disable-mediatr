﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Api.Model
{
    public class CreateProfileRequest
    {
        [Required]
        [MaxLength(50)]
        public string IssuerOtp { get; set; }

        [Required]
        public CreatePrivatePersonRequest PrivatePerson { get; set; }

        [Required]
        public CreateAddressRequest Address { get; set; }

        [Required]
        public CreatePaymentRequest Payment { get; set; }

        [Required]
        public CreateJobInfoRequest JobInfo { get; set; }

        [Required]
        public IList<CreateBankingAccountRequest> BankingAccounts { get; set; }

        [Required]
        public CreateFinancialInfoRequest FinancialInfo { get; set; }

        public IList<CreateTradingCode> TradingCodes { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Api.Model
{
    public class CreatePaymentRequest
    {
        [Display(Name = "مبلغ")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        public long Amount { get; set; }

        [Display(Name = "شماره مرجع")]
        [MaxLength(30, ErrorMessage = "{0}  معتبر نیست")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string ReferenceNumber { get; set; }

        [Display(Name = "شماره پیگیری")]
        [MaxLength(30, ErrorMessage = "{0}  معتبر نیست")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string SaleReferenceId { get; set; }

        public long? Discount { get; set; }

        public DateTime DateTime { get; set; }

        public string ServiceId { get; set; }

        public string OrderId { get; set; }

    }
}
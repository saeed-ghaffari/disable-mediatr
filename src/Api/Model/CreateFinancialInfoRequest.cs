﻿using System;
using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.Enum;

namespace Cerberus.Api.Model
{
    public class CreateFinancialInfoRequest
    {
        [RegularExpression(@"^\s*[,1234567890\s]+\s*$", ErrorMessage = "مقدار {0} نامعتبر است")]
        [Range(0, long.MaxValue, ErrorMessage = "{0} معتیر نیست")]
        public long? AssetsValue { get; set; }

        [RegularExpression(@"^\s*[,1234567890\s]+\s*$", ErrorMessage = "مقدار {0} نامعتبر است")]
        [Range(0, long.MaxValue, ErrorMessage = "{0} معتیر نیست")]
        public long? InComingAverage { get; set; }
        [RegularExpression(@"^\s*[,1234567890\s]+\s*$", ErrorMessage = "مقدار {0} نامعتبر است")]
        [Range(0, long.MaxValue, ErrorMessage = "{0} معتیر نیست")]
        public long? SExchangeTransaction { get; set; }
        [RegularExpression(@"^\s*[,1234567890\s]+\s*$", ErrorMessage = "مقدار {0} نامعتبر است")]
        [Range(0, long.MaxValue, ErrorMessage = "{0} معتیر نیست")]
        public long? CExchangeTransaction { get; set; }
        [RegularExpression(@"^\s*[,1234567890\s]+\s*$", ErrorMessage = "مقدار {0} نامعتبر است")]
        [Range(0, long.MaxValue, ErrorMessage = "{0} معتیر نیست")]
        public long? OutExchangeTransaction { get; set; }

        [Required(ErrorMessage = "{0}اجباری است ")]
        [Range((byte)(Domain.Enum.TransactionLevel.One), ((byte)(Domain.Enum.TransactionLevel.Five)), ErrorMessage = "مقدار {0} نامعتبر است")]
        public TransactionLevel? TransactionLevel { get; set; }

        [Required(ErrorMessage = "{0}اجباری است ")]
        [Range((byte)(Domain.Enum.TradingKnowledgeLevel.Excellent), ((byte)(Domain.Enum.TradingKnowledgeLevel.VeryLow)), ErrorMessage = "مقدار {0} نامعتبر است")]
        public TradingKnowledgeLevel? TradingKnowledgeLevel { get; set; }
    }
}
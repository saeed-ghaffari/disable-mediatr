﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Api.Model
{
    public class MobileResponseViewModel
    {
        public string Mobile { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Api.Model
{
    public class GetBatchInfoRequest
    {
        [Url]
        [Required]
        [MaxLength(200)]
        public string CallbackUrl { get; set; }

        [Required]
        public IEnumerable<string> UniqueIdentifiers { get; set; }
    }
}
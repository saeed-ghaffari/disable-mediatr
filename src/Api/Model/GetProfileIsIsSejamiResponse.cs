﻿namespace Cerberus.Api.Model
{
    public class GetProfileIsIsSejamiResponse
    {
        public bool IsSejami { get; set; }
    }
}
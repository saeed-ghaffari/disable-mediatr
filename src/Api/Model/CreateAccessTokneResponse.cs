﻿using System;

namespace Cerberus.Api.Model
{
    public class CreateAccessTokenResponse
    {
        public string AccessToken { get; set; }
        public TimeSpan Ttl { get; set; }
    }
}
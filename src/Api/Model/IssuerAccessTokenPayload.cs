﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Cerberus.Api.Model
{

    [DataContract]
    public class IssuerAccessTokenPayload
    {
        [DataMember(Order = 1)]
        [JsonProperty(PropertyName = "s")]
        public long ServiceId { get; set; }

        [DataMember(Order = 2)]
        [JsonProperty(PropertyName = "u")]
        public long UserId { get; set; }

    }
}
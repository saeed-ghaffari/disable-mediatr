﻿using System.ComponentModel.DataAnnotations;
using Cerberus.Utility;

namespace Cerberus.Api.Model
{
    public class GenerateEntryNodeStatusOtpRequest
    {
        [Required]
        [MinLength(10, ErrorMessage = "invalid mobile")]
        [MaxLength(10, ErrorMessage = "invalid mobile")]
        [RegularExpression(@"9(0[1-5]|1[0-9]|3[0-9]|2[0-9]|9[0-9])-?[0-9]{3}-?[0-9]{4}", ErrorMessage = "invalid mobile")]
        public string Mobile { get; set; }

        [Required]
        [UniqueIdentifierValidation(ErrorMessage = "invalid uniqueIdentifier")]
        public string UniqueIdentifier { get; set; }

    }
}
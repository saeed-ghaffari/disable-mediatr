﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.ViewModel;

namespace Cerberus.Api.Model
{
    public class GetBatchProfileRequest
    {
        [Url]
        [Required]
        public string CallbackUrl { get; set; }

        [Required]
        public IEnumerable<BatchProfileInfo> ProfileInfos { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace Cerberus.Api.Model
{
    public class CreateAccessTokenRequest
    {
        [Required,MaxLength(100)]
        public string Password { get; set; }
        [Required,MaxLength(50)]
        public string Username { get; set; }
    }
}
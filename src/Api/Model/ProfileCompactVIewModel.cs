﻿using Cerberus.Domain.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cerberus.Api.Model
{
    public class ProfileCompactViewModel
    {
        public long Mobile { get; set; }

        public string UniqueIdentifier { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public ProfileOwnerType Type { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public ProfileStatus Status { get; set; }
    }
}
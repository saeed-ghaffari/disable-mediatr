﻿namespace Cerberus.Api.Model
{
    public class CheckExistResponse
    {
        public bool Existence { get; set; }
        public string IssuerOtp { get; set; }
    }
}
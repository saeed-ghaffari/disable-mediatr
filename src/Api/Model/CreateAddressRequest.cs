﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Api.Model
{
    public class CreateAddressRequest
    {
        [Display(Name = "کد پستی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MinLength(10, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(10, ErrorMessage = "{0}  معتبر نیست")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        public string PostalCode { get; set; }

        [Display(Name = "کشور")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        [Range(Int32.MinValue, Int32.MaxValue, ErrorMessage = "{0}  معتبر نیست")]
        public long? CountryId { get; set; }

        [Display(Name = "استان")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        [Range(Int32.MinValue, Int32.MaxValue, ErrorMessage = "{0}  معتبر نیست")]
        public long? ProvinceId { get; set; }

        [Display(Name = "شهر")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        [Range(Int32.MinValue, Int32.MaxValue, ErrorMessage = "{0}  معتبر نیست")]
        public long? CityId { get; set; }

        [Display(Name = "بخش")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        [Range(Int32.MinValue, Int32.MaxValue, ErrorMessage = "{0}  معتبر نیست")]
        public long? SectionId { get; set; }


        [Display(Name = "پیش شماره شهر")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MinLength(3, ErrorMessage = "معتبر نیست")]
        [MaxLength(3, ErrorMessage = "معتبر نیست")]
        public string CityPrefix { get; set; }

        [Display(Name = "خیابان")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MinLength(1, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(100, ErrorMessage = "{0}  معتبر نیست")]
        [RegularExpression(@"^\s*[-،.چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ0123456789,\s]+\s*$", ErrorMessage = "فقط استفاده از اعداد و حروف فارسی در {0} مجاز است")]
        public string RemnantAddress { get; set; }

        [Display(Name = "کوچه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MinLength(1, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(50, ErrorMessage = "{0}  معتبر نیست")]
        [RegularExpression(@"^\s*[-،.چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ0123456789,\s]+\s*$", ErrorMessage = "فقط استفاده از اعداد و حروف فارسی مجاز است")]
        public string Alley { get; set; }

        [Display(Name = "پلاک")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MinLength(1, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(30, ErrorMessage = "{0}  معتبر نیست")]
        [RegularExpression(@"^\s*[-،.چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ0123456789,\s]+\s*$", ErrorMessage = "فقط استفاده از اعداد و حروف فارسی مجاز است")]
        public string Plaque { get; set; }

        [Display(Name = "شماره تلفن ثابت")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MinLength(8, ErrorMessage = "{0}  نمی تواند کمتر از 8 عدد باشد")]
        [MaxLength(8, ErrorMessage = "{0}  نمیتواند بیشتر از 8 عدد باشد")]
        public string Tel { get; set; }


        [Display(Name = " پیش شماره کشور تلفن همراه اضطراری")]
        [RegularExpression("^\\d*$", ErrorMessage = "{0}  معتبر نیست")]
        [MinLength(1, ErrorMessage = "معتبر نیست")]
        [MaxLength(4, ErrorMessage = "معتبر نیست")]
        public string CountryPrefix { get; set; }

        [Display(Name = "شماره موبایل اظطراری")]
        [RegularExpression(@"9(0[1-5]|1[0-9]|3[0-9]|2[0-9]|9[0-9])-?[0-9]{3}-?[0-9]{4}", ErrorMessage = "لطفا شماره تلفن همراه خود را صحیح وارد نمایید")]
        [MinLength(10, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(10, ErrorMessage = "{0}  معتبر نیست")]
        public string EmergencyMobile { get; set; }


        [Display(Name = "شماره تلفن ثابت اضطراری")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  فقط عدد مجاز است")]
        [MinLength(8, ErrorMessage = "{0}  نمی تواند کمتر از 8 عدد باشد")]
        [MaxLength(8, ErrorMessage = "{0}  نمی تواند بیشتر از 8 عدد باشد")]
        public string EmergencyTel { get; set; }


        [Display(Name = " پیش شماره تلفن ثابت اضطراری")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  فقط عدد مجاز است")]
        [MinLength(2, ErrorMessage = "معتبر نیست")]
        [MaxLength(3, ErrorMessage = "معتبر نیست")]
        public string EmergencyTelCityPrefix { get; set; }


        [Display(Name = " پیش شماره کشور  اضطراری")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  فقط عدد مجاز است")]
        [MinLength(2, ErrorMessage = "معتبر نیست")]
        [MaxLength(3, ErrorMessage = "معتبر نیست")]
        public string EmergencyTelCountryPrefix { get; set; }
        [Display(Name = " پیش شماره فکس")]
        [MaxLength(3, ErrorMessage = "معتبر نیست")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        public string FaxPrefix { get; set; }

        [Display(Name = " فکس")]
        [MaxLength(8, ErrorMessage = "معتبر نیست")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        public string Fax { get; set; }

        [Display(Name = " آدرس وب سایت")]
        [Url(ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(50, ErrorMessage = "معتبر نیست")]
        public string Website { get; set; }

        [Display(Name = " پست الکترونیکی")]
        [EmailAddress(ErrorMessage = "{0}  معتبر نیست")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,20}$", ErrorMessage = "{0} نامعتبر است")]
        [MaxLength(50, ErrorMessage = "{0}  معتبر نیست")]
        public string Email { get; set; }
    }
}
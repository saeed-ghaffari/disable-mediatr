﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Api.Model.AuthenticationOffice
{
    public class ConfirmDataRequest
    {
        [MaxLength(11 ,  ErrorMessage = "Invalid Parameters")]
        public string NationalCode { get; set; }
        [MaxLength(10, ErrorMessage = "Invalid Parameters")]
        public string TraceCode { get; set; }

        [StringLength(maximumLength:11,MinimumLength =11 ,ErrorMessage = "Invalid Parameters")]
        public string Token { get; set; }

        [MaxLength(10, ErrorMessage = "Invalid Parameters")]
        public string UserKey { get; set; }

        public string PersonImage { get; set; }
        public string SignatureImage { get; set; }

        [MaxLength(20, ErrorMessage = "Invalid Parameters")]
        public string RegisterDate { get; set; }
        
    }
}

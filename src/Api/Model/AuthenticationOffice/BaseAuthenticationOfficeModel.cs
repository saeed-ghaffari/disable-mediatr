﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Api.Model.AuthenticationOffice
{
    public class BaseAuthenticationOfficeModel
    {
        public BaseAuthenticationOfficeModel()
        {
            IsSuccess = false;
            Errors = new List<ErrorModel>();
        }
        public List<ErrorModel> Errors { get; set; }
        public bool IsSuccess { get; set; }
        public string recordsTotal { get; set; } = "0";
        public string recordsFiltered { get; set; } = "0";
        public string draw { get; set; } = "1";
        
    }
    public class ErrorModel
    {

        public string Code { get; set; }
        public string Message { get; set; }
    }
}

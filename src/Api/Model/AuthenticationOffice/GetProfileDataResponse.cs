﻿using System.Collections.Generic;

namespace Cerberus.Api.Model.AuthenticationOffice
{

    public class GetProfileDataResponse:BaseAuthenticationOfficeModel
    {
        public GetProfileDataResponse()
        {
            DataItem=new DataItem();
        }
        //public List<object> DataItem { get; set; }
        public DataItem DataItem { get; set; }
        public object[] data { get; set; } = new object[0];
    }
}

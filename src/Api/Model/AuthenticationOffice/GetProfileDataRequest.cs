﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Api.Model.AuthenticationOffice
{
    public class GetProfileDataRequest
    {
        [MaxLength(11, ErrorMessage = "Invalid Parameters")]
        public string NationalCode { get; set; }
        [MaxLength(10, ErrorMessage = "Invalid Parameters")]
        public string TraceCode { get; set; }
        [MaxLength(11, ErrorMessage = "Invalid Parameters") ,MinLength(11, ErrorMessage = "Invalid Parameters")]
        public string Token { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Api.Model.AuthenticationOffice
{
    public class CreateOtpTokenResponse:BaseAuthenticationOfficeModel
    {
        public long? DataItem { get; set; }

        public object[] data { get; set; } = new object[0];
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Cerberus.Api.Model
{
    public class CreateBankingAccountRequest
    {
        [Display(Name = "شماره حساب")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(20, ErrorMessage = "{0} مجاز نمی باشد")]
        public string AccountNumber { get; set; }

        [Display(Name = "نوع شماره حساب")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [Range((byte)(BankingAccountType.LongTermAccount), ((byte)BankingAccountType.SavingAccount), ErrorMessage = "مقدار {0} نامعتبر است")]
        public BankingAccountType? Type { get; set; }

        [Display(Name = "شماره شبا")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MaxLength(26, ErrorMessage = "{0} مجاز نمی باشد")]
        [MinLength(26, ErrorMessage = "{0} مجاز نمی باشد")]
        public string Sheba { get; set; }

        [Display(Name = "نام بانک")]
        [BindRequired]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0} معتبر نمی باشد")]
        public long? BankId { get; set; }

        [Display(Name = "کد شعبه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(8, ErrorMessage = "{0} مجاز نمی باشد")]
        public string BranchCode { get; set; }

        [Display(Name = "نام شعبه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(60, ErrorMessage = "{0} نمی تواند بیشتر از 60 کارکتر باشد")]
        public string BranchName { get; set; }

        [Display(Name = "شهر شعبه بانک")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [Range(Int32.MinValue,Int32.MaxValue,ErrorMessage = "{0} معتیر نیست")]
        public long? BranchCityId { get; set; }

        [Display(Name = "حساب پیش فرض")]
        [Required(ErrorMessage = "{0}اجباری است")]
        public bool IsDefault { get; set; }
    }
}
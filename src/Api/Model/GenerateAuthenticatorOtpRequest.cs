﻿using System.ComponentModel.DataAnnotations;

namespace Cerberus.Api.Model
{
    public class GenerateAuthenticatorOtpRequest : GenerateOtpRequest
    {
        [Required]
        [MaxLength(10)]
        public string TraceCode { get; set; }
    }
}
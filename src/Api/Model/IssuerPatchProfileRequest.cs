﻿using System.ComponentModel.DataAnnotations;

namespace Cerberus.Api.Model
{
    public class IssuerPatchProfileRequest
    {
        [Required]
        [MaxLength(80)]
        public string IssuerReference { get; set; }

        [Required]
        public string PrivatePersonImage { get; set; }

        [Required]
        public string PrivatePersonSignature { get; set; }
        
    }
}
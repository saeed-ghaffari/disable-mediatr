﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cerberus.Api.Model
{
    public class GetProfileStatusResponse
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ProfileStatus Status { get; set; }
    }
}

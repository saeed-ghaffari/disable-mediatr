﻿



using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NLog.Web;

namespace Cerberus.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json").Build();

            var legalPersonConfig = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("LegalPersonsWhiteListConfiguration.json").Build();

            var urls = config.GetSection("ApiConfiguration:Urls").GetChildren().Select(x => x.Value).ToArray();
            return WebHost.CreateDefaultBuilder(args)
                .UseKestrel(options =>
                {
                    options.Limits.MinResponseDataRate = null;
                    options.AddServerHeader = false;
                    options.Limits.KeepAliveTimeout = TimeSpan.FromSeconds(20);
                })
                .UseUrls(urls)
                .UseConfiguration(config)
                .UseConfiguration(legalPersonConfig)
                .CaptureStartupErrors(true)
                .ConfigureLogging(logging =>
                {
                    logging.SetMinimumLevel(LogLevel.Warning);
                    logging.AddConsole();
                    logging.AddDebug();
                })
                .UseNLog()
                .UseStartup<Startup>()
                .Build();
        }
    }
}

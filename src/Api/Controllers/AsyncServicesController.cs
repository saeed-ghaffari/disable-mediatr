﻿using System;
using System.Net;
using System.Threading.Tasks;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Filters;
using Cerberus.Api.Model;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class AsyncServicesController : BaseController
    {
        private readonly ITaskRepository _taskRepository;

        public AsyncServicesController(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        [HttpPost]
        [Route("v{version:apiVersion}/asyncServices/bankingAccounts")]
        [RateLimitFilterFactory(Order = 2, Limit = 20, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<CreateAsyncTaskResponse> GetBankingAccountsAsync(GetBatchInfoRequest request)
        {
            var task = new ScheduledTask()
            {
                DueTime = DateTime.Now,
                GroupId = CerberusTasksIdentifierHelper.GetBankingAccounts.Key,
                Params = JsonConvert.SerializeObject(new GetBankingAccountsTaskParams()
                {
                    UniqueIdentifiers = request.UniqueIdentifiers,
                    CallbackUrl = request.CallbackUrl,
                    ServiceId = CurrentService.ServiceId,
                }),
                RefId = CurrentService.ServiceId,
                Type = CerberusTasksIdentifierHelper.GetBankingAccounts.Value
            };

            task = await _taskRepository.AddAsync(task);

            //set 202 http status code
            ResponseHttpStatusCode = HttpStatusCode.Accepted;

            return new CreateAsyncTaskResponse()
            {
                Reference = task.Id.ToString()
            };
        }

        [HttpPost]
        [Route("v{version:apiVersion}/asyncServices/profiles/sejamStatus")]
        [RateLimitFilterFactory(Order = 2, Limit = 20, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<CreateAsyncTaskResponse> GetSejamStatusAsync(GetBatchInfoRequest request)
        {
            var task = new ScheduledTask()
            {
                DueTime = DateTime.Now,
                GroupId = CerberusTasksIdentifierHelper.GetProfileStatus.Key,
                Params = JsonConvert.SerializeObject(new GetSejamStatusTaskParam()
                {
                    UniqueIdentifiers = request.UniqueIdentifiers,
                    CallbackUrl = request.CallbackUrl,
                    ServiceId = CurrentService.ServiceId,
                }),
                RefId = CurrentService.ServiceId,
                Type = CerberusTasksIdentifierHelper.GetProfileStatus.Value
            };

            task = await _taskRepository.AddAsync(task);

            //set 202 http status code
            ResponseHttpStatusCode = HttpStatusCode.Accepted;

            return new CreateAsyncTaskResponse()
            {
                Reference = task.Id.ToString()
            };
        }

        [HttpPost]
        [Route("v{version:apiVersion}/asyncServices/profiles/isSejami")]
        [RateLimitFilterFactory(Order = 2, Limit = 20, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<CreateAsyncTaskResponse> GetIsSejamiAsync(GetBatchInfoRequest request)
        {
            var task = new ScheduledTask()
            {
                DueTime = DateTime.Now,
                GroupId = CerberusTasksIdentifierHelper.GetProfileIsSejami.Key,
                Params = JsonConvert.SerializeObject(new GetProfileTaskParam()
                {
                    UniqueIdentifiers = request.UniqueIdentifiers,
                    CallbackUrl = request.CallbackUrl,
                    ServiceId = CurrentService.ServiceId,
                }),
                RefId = CurrentService.ServiceId,
                Type = CerberusTasksIdentifierHelper.GetProfileIsSejami.Value
            };

            task = await _taskRepository.AddAsync(task);

            //set 202 http status code
            ResponseHttpStatusCode = HttpStatusCode.Accepted;

            return new CreateAsyncTaskResponse()
            {
                Reference = task.Id.ToString()
            };
        }

        [HttpPost]
        [Route("v{version:apiVersion}/asyncServices/privatePerson")]
        [RateLimitFilterFactory(Order = 2, Limit = 20, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<CreateAsyncTaskResponse> GetProfilesAsync(GetBatchInfoRequest request)
        {
            var task = new ScheduledTask()
            {
                DueTime = DateTime.Now,
                GroupId = CerberusTasksIdentifierHelper.GetPrivatePerson.Key,
                Params = JsonConvert.SerializeObject(new GetPrivatePersonTaskParam()
                {
                    UniqueIdentifiers = request.UniqueIdentifiers,
                    CallbackUrl = request.CallbackUrl,
                    ServiceId = CurrentService.ServiceId,
                    ProfileStatus = Domain.Enum.ProfileStatus.TraceCode
                }),
                RefId = CurrentService.ServiceId,
                Type = CerberusTasksIdentifierHelper.GetPrivatePerson.Value
            };

            task = await _taskRepository.AddAsync(task);

            //set 202 http status code
            ResponseHttpStatusCode = HttpStatusCode.Accepted;

            return new CreateAsyncTaskResponse()
            {
                Reference = task.Id.ToString()
            };
        }

        [HttpPost]
        [Route("v{version:apiVersion}/asyncServices/profile/traceCode")]
        [RateLimitFilterFactory(Order = 2, Limit = 20, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<CreateAsyncTaskResponse> GetTraceCodeAsync(GetBatchInfoRequest request)
        {
            var task = new ScheduledTask()
            {
                DueTime = DateTime.Now,
                GroupId = CerberusTasksIdentifierHelper.GetTraceCode.Key,
                Params = JsonConvert.SerializeObject(new GetProfileTaskParam()
                {
                    UniqueIdentifiers = request.UniqueIdentifiers,
                    CallbackUrl = request.CallbackUrl,
                    ServiceId = CurrentService.ServiceId,
                }),
                RefId = CurrentService.ServiceId,
                Type = CerberusTasksIdentifierHelper.GetTraceCode.Value
            };

            task = await _taskRepository.AddAsync(task);

            //set 202 http status code
            ResponseHttpStatusCode = HttpStatusCode.Accepted;

            return new CreateAsyncTaskResponse()
            {
                Reference = task.Id.ToString()
            };
        }


        [HttpPost]
        [Route("v{version:apiVersion}/asyncServices/compactProfiles")]
        [RateLimitFilterFactory(Order = 2, Limit = 20, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<CreateAsyncTaskResponse> GetCompactProfilesAsync(GetBatchInfoRequest request)
        {
            var task = new ScheduledTask()
            {
                DueTime = DateTime.Now,
                GroupId = CerberusTasksIdentifierHelper.GetCompactProfile.Key,
                Params = JsonConvert.SerializeObject(new GetProfileTaskParam()
                {
                    UniqueIdentifiers = request.UniqueIdentifiers,
                    CallbackUrl = request.CallbackUrl,
                    ServiceId = CurrentService.ServiceId,
                }),
                RefId = CurrentService.ServiceId,
                Type = CerberusTasksIdentifierHelper.GetCompactProfile.Value
            };

            task = await _taskRepository.AddAsync(task);

            //set 202 http status code
            ResponseHttpStatusCode = HttpStatusCode.Accepted;

            return new CreateAsyncTaskResponse()
            {
                Reference = task.Id.ToString()
            };
        }

        [HttpPost]
        [Route("v{version:apiVersion}/asyncServices/Profile/privateperson")]
        [RateLimitFilterFactory(Order = 2, Limit = 20, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<CreateAsyncTaskResponse> GetPrivatePersonsAsync(GetBatchProfileRequest request)
        {
            var task = new ScheduledTask()
            {
                DueTime = DateTime.Now,
                GroupId = CerberusTasksIdentifierHelper.GetProfilePrivatePerson.Key,
                Params = JsonConvert.SerializeObject(new GetProfileInfoTaskParam()
                {
                    ProfileInfos = request.ProfileInfos,
                    CallbackUrl = request.CallbackUrl,
                    ServiceId = CurrentService.ServiceId,
                }),
                RefId = CurrentService.ServiceId,
                Type = CerberusTasksIdentifierHelper.GetProfilePrivatePerson.Value
            };

            task = await _taskRepository.AddAsync(task);

            //set 202 http status code
            ResponseHttpStatusCode = HttpStatusCode.Accepted;

            return new CreateAsyncTaskResponse()
            {
                Reference = task.Id.ToString()
            };
        }

        [HttpPost]
        [Route("v{version:apiVersion}/asyncServices/sejamiPrivatePerson")]
        [RateLimitFilterFactory(Order = 2, Limit = 20, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<CreateAsyncTaskResponse> GetSejamiPrivatePersonAsync(GetBatchInfoRequest request)
        {
            var task = new ScheduledTask()
            {
                DueTime = DateTime.Now,
                GroupId = CerberusTasksIdentifierHelper.GetPrivatePerson.Key,
                Params = JsonConvert.SerializeObject(new GetPrivatePersonTaskParam()
                {
                    UniqueIdentifiers = request.UniqueIdentifiers,
                    CallbackUrl = request.CallbackUrl,
                    ServiceId = CurrentService.ServiceId,
                    ProfileStatus = Domain.Enum.ProfileStatus.Sejami
                }),
                RefId = CurrentService.ServiceId,
                Type = CerberusTasksIdentifierHelper.GetPrivatePerson.Value
            };

            task = await _taskRepository.AddAsync(task);

            //set 202 http status code
            ResponseHttpStatusCode = HttpStatusCode.Accepted;

            return new CreateAsyncTaskResponse()
            {
                Reference = task.Id.ToString()
            };
        }

    }
}
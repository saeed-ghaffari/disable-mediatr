﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Configuration;
using Cerberus.Api.Extensions;
using Cerberus.Api.Filters;
using Cerberus.Api.Model;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Service;
using Cerberus.Utility;
using Gatekeeper.AspModules.ActionFilter;
using Gatekeeper.AspModules.Interface;
using Gatekeeper.Core.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [Route("v{version:apiVersion}")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class OtpController : BaseController
    {
        private readonly IOtpService _otpService;
        private readonly IOptions<ApiConfiguration> _apiConfiguration;
        private readonly IMessagingService _messagingService;
        private readonly IOptions<MessagingConfiguration> _messagingConfiguration;
        private readonly IProfileApiService _profileApiService;
        private readonly AuthenticatorConfiguration _authenticatorConfiguration;
        private readonly LegalPersonsWhiteListConfiguration _legalPersonsWhiteListConfiguration;
        private readonly IRateLimitService _rateLimitService;
        private readonly IAgentService _agentService;
        private readonly IPrivatePersonService _privatePersonService;
        private readonly IProfileService _profileService;
        private readonly IMapper _mapper;
        private readonly IMsisdnService _msisdnService;
        private readonly IBankingAccountService _bankingAccountService;
        public OtpController(
            IOtpService otpService,
            IOptions<ApiConfiguration> apiConfiguration,
            IMessagingService messagingService, IOptions<MessagingConfiguration> messagingConfiguration,
            IProfileApiService profileApiService, IOptions<AuthenticatorConfiguration> authenticatorConfiguration,
           IOptions<LegalPersonsWhiteListConfiguration> legalPersonsWhiteListConfiguration, IRateLimitService rateLimitService, IAgentService agentService, IPrivatePersonService privatePersonService, IProfileService profileService, IMapper mapper, IMsisdnService msisdnService, IBankingAccountService bankingAccountService)
        {
            _otpService = otpService;
            _messagingService = messagingService;
            _messagingConfiguration = messagingConfiguration;
            _profileApiService = profileApiService;
            _rateLimitService = rateLimitService;
            _agentService = agentService;
            _privatePersonService = privatePersonService;
            _profileService = profileService;
            _mapper = mapper;
            _msisdnService = msisdnService;
            _bankingAccountService = bankingAccountService;
            _legalPersonsWhiteListConfiguration = legalPersonsWhiteListConfiguration.Value;
            _authenticatorConfiguration = authenticatorConfiguration.Value;
            _apiConfiguration = apiConfiguration;
        }

        [HttpPost]
        [Route("kycOtp")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<object> GenerateKycOtpAsync(GenerateOtpRequest request)
        {
            await CheckHasAccess(request.UniqueIdentifier, 2);
            var profile = await _profileApiService.GetByUniqueIdentifierAsync(request.UniqueIdentifier);
            if (profile == null)
                throw new CerberusException(ErrorCode.NotFound);

            if (profile.Type == ProfileOwnerType.IranianLegalPerson)
            {
                var whiteList = _legalPersonsWhiteListConfiguration.UniqueIdentifiers;

                if (profile.Status >= ProfileStatus.TraceCode && whiteList.Contains(profile.UniqueIdentifier))
                {
                    await GenerateAndSendMessageAsync(_messagingConfiguration.Value.KycOtpMessage, profile, OtpType.Kyc,
                        _apiConfiguration.Value.KycOtpTtl);
                    return NoContent();
                }
                throw new CerberusException(ErrorCode.InCompleteRegistrations);
            }

            if (profile.Status != ProfileStatus.Sejami)
                throw new CerberusException(ErrorCode.InCompleteRegistrations);

            await GenerateAndSendMessageAsync(_messagingConfiguration.Value.KycOtpMessage, profile, OtpType.Kyc,
                _apiConfiguration.Value.KycOtpTtl);

            return NoContent();

        }

        [HttpPost]
        [Route("kycPermanentOtp")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<object> GenerateKycPermanentOtpAsync(GenerateOtpRequest request)
        {
            await CheckHasAccess(request.UniqueIdentifier, 2);
            var profile = await _profileApiService.GetByUniqueIdentifierAsync(request.UniqueIdentifier);
            if (profile == null)
                throw new CerberusException(ErrorCode.NotFound);

            if (profile.Type == ProfileOwnerType.IranianLegalPerson)
            {
                var whiteList = _legalPersonsWhiteListConfiguration.UniqueIdentifiers;

                if (profile.Status >= ProfileStatus.TraceCode && whiteList.Contains(profile.UniqueIdentifier))
                {
                    await GenerateAndSendMessageAsync(_messagingConfiguration.Value.KycOtpMessage, profile, OtpType.PermanentKyc,
                        _apiConfiguration.Value.KycOtpTtl);
                    return NoContent();
                }
                throw new CerberusException(ErrorCode.InCompleteRegistrations);
            }

            if (profile.Status != ProfileStatus.Sejami)
                throw new CerberusException(ErrorCode.InCompleteRegistrations);

            await GenerateAndSendMessageAsync(_messagingConfiguration.Value.KycOtpMessage, profile, OtpType.PermanentKyc, _apiConfiguration.Value.OtpTtl);

            return NoContent();
        }

        [HttpPost]
        [Route("internalKycOtp")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<object> GenerateInternalKycOtpAsync(GenerateOtpRequest request)
        {
            await CheckHasAccess(request.UniqueIdentifier, 2);
            var profile = await _profileApiService.GetByUniqueIdentifierAsync(request.UniqueIdentifier);
            if (profile == null)
                throw new CerberusException(ErrorCode.NotFound);

            if (!profile.Status.IsSejami())
                throw new CerberusException(ErrorCode.InCompleteRegistrations);

            var otp = await _otpService.GetAsync(ConventionalHelper.CreateOtpKey(_apiConfiguration.Value.InternalServiceOtpMobile, profile.UniqueIdentifier, OtpType.InternalService),
                _apiConfiguration.Value.OtpTtl);

            await _messagingService.SendOtpSmsAsync(null, _apiConfiguration.Value.InternalServiceOtpMobile, null, string.Format(_messagingConfiguration.Value.KycOtpMessage, otp, Environment.NewLine));

            return NoContent();
        }

        [HttpPost]
        [Route("authenticatorOtp")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 120, UserIdentifier = "ServiceId")]
        public async Task<object> GenerateAuthenticatorOtpAsync(GenerateAuthenticatorOtpRequest request)
        {
            await CheckHasAccess(request.UniqueIdentifier, 2);
            var profile = await _profileApiService.GetByUniqueIdentifierAsync(request.UniqueIdentifier);
            if (profile == null)
                throw new CerberusException(ErrorCode.NotFound);

            if (!profile.Status.IsConfirmed())
            {
                profile = _mapper.Map<Domain.Model.Profile, ProfileViewModel>(await _profileService.GetByUniqueIdentifierWithNoCacheAsync(request.UniqueIdentifier));
                if (!profile.Status.IsConfirmed())
                    throw new CerberusException(ErrorCode.InCompleteRegistrations);

            }

            if (profile.Status == ProfileStatus.Dead)
                throw new CerberusException(ErrorCode.Conflicts, "profile has dead status");

            if (profile.TraceCode != request.TraceCode)
                throw new CerberusException(ErrorCode.InvalidTraceCode, "Invalid traceCode");

            if (profile.Status == ProfileStatus.Sejami)
                throw new CerberusException(ErrorCode.Conflicts, "profile already has sejami status");

            var profileAgent = await _agentService.GetByProfileIdWithNoLockAsync(profile.Id);

            if (profileAgent != null && profileAgent.IsConfirmed != true)
                throw new CerberusException(ErrorCode.InCompleteRegistrations, "the profile agent not Confirmed");

            var privatePerson = await _privatePersonService.GetByProfileIdWithNoLockAsync(profile.Id);

            if (profile.Type == ProfileOwnerType.IranianPrivatePerson && privatePerson != null &&
               Tools.IsUnder18Years(privatePerson.BirthDate) && profileAgent == null)
                throw new CerberusException(ErrorCode.InCompleteRegistrations, "the profile agent not specified");


            if (privatePerson == null || privatePerson.IsConfirmed != true)
                throw new CerberusException(ErrorCode.InCompleteRegistrations, "the privatePerson data is not confirmed");

            var bankingAccounts = await _bankingAccountService.GetListAsync(profile.Id);

            var accounts = bankingAccounts.ToList();
            if (!accounts.Any() || accounts.Any(x => x.IsConfirmed != true))
                throw new CerberusException(ErrorCode.InCompleteRegistrations, "the bankingAccount data is not confirmed");

            if (!Tools.IsUnder18Years(privatePerson.BirthDate) && profileAgent == null)
            {
                var msisdn = await _msisdnService.GetByProfileIdAsync(profile.Id);
                if (msisdn != null && msisdn.IsConfirmed != true && msisdn.Mobile == profile.Mobile)
                    throw new CerberusException(ErrorCode.InCompleteRegistrations, "the mobile is not confirmed");
            }
            if (CurrentService.ServiceId == _authenticatorConfiguration.PishkhanServiceId)
            {
                var otp = await _otpService.GetAsync(ConventionalHelper.CreateOtpKey(profile.Mobile.ToString(), profile.UniqueIdentifier, OtpType.Authenticator), _apiConfiguration.Value.OtpTtl);
                if (profileAgent == null)
                    await _messagingService.SendOtpSmsAsync(null, profile.Mobile.ToString(), null, string.Format(_messagingConfiguration.Value.PishkhanOtpWithNoAgentMessage, otp, Environment.NewLine, Environment.NewLine));
                else
                    await _messagingService.SendOtpSmsAsync(null, profile.Mobile.ToString(), null, string.Format(_messagingConfiguration.Value.PishkhanOtpWithAgentMessage, otp, Environment.NewLine, Environment.NewLine));

            }
            else
                await GenerateAndSendMessageAsync(_messagingConfiguration.Value.KycOtpMessage, profile, OtpType.Authenticator, _apiConfiguration.Value.OtpTtl);

            return NoContent();
        }

        [HttpPost]
        [Route("karaOtp")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<object> GenerateKaraOtpAsync(GenerateAuthenticatorOtpRequest request)
        {
            await CheckHasAccess(request.UniqueIdentifier, 2);
            var profile = await _profileApiService.GetByUniqueIdentifierAsync(request.UniqueIdentifier);
            if (profile == null)
                throw new CerberusException(ErrorCode.NotFound);

            if (!profile.Status.IsConfirmed())
                throw new CerberusException(ErrorCode.InCompleteRegistrations);

            if (profile.TraceCode != request.TraceCode)
                throw new CerberusException(ErrorCode.InvalidTraceCode, "Invalid traceCode");

            await GenerateAndSendMessageAsync(_messagingConfiguration.Value.KycOtpMessage, profile, OtpType.Kara, _apiConfiguration.Value.OtpTtl);

            return NoContent();
        }

        [HttpPost]
        [Route("entryNodeOtp")]
        [RateLimitFilterFactory(Order = 2, Limit = 300, PeriodInSec = 60, UserIdentifier = "ServiceId")]
        public async Task<object> GenerateEntryNodeOtpAsync(GenerateEntryNodeOtpRequest request)
        {
            await CheckHasAccess(request.Mobile, 2);

            var msisdn = request.Mobile;

            if (!ValidationHelper.IsMsisdnValid(ref msisdn, out var carrier) || carrier == null)
            {
                ModelState.AddModelError(nameof(CreatePrivatePersonRequest.Mobile), "invalid mobile number");
                throw new CerberusException(ErrorCode.BadRequest, value: ModelState.ToViewModel());
            }
            var otp = await _otpService.GetAsync(ConventionalHelper.CreateEntryNodeOtpKey(msisdn),
                _apiConfiguration.Value.OtpTtl);

            await _messagingService.SendOtpSmsAsync(null, msisdn, null, string.Format(_messagingConfiguration.Value.RegisterOtpMessage, otp, Environment.NewLine));

            return NoContent();
        }

        [HttpPost]
        [Route("entryNodeStatusOtp")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<object> GenerateEntryNodeStatusOtpAsync(GenerateEntryNodeStatusOtpRequest request)
        {

            var msisdn = request.Mobile;
            if (!ValidationHelper.IsMsisdnValid(ref msisdn, out var carrier) || carrier == null)
            {
                ModelState.AddModelError(nameof(CreatePrivatePersonRequest.Mobile), "invalid mobile number");
                throw new CerberusException(ErrorCode.BadRequest, value: ModelState.ToViewModel());
            }

            await CheckHasAccess(request.UniqueIdentifier, 2);

            var profile = await _profileService.GetByUniqueIdentifierFromCacheAsync(request.UniqueIdentifier);
            if (profile == null || profile.Mobile.ToString() != msisdn)
                throw new CerberusException(ErrorCode.NotFound);

            await GenerateAndSendMessageAsync(_messagingConfiguration.Value.EntryNodeStatusOtpMessage, _mapper.Map<Domain.Model.Profile, ProfileViewModel>(profile), OtpType.EntryNodeStatus, _apiConfiguration.Value.OtpTtl);

            return NoContent();
        }


        private async Task GenerateAndSendMessageAsync(string msgFormat, ProfileViewModel profile, OtpType otpType, TimeSpan ttl)
        {
            var otp = await _otpService.GetAsync(ConventionalHelper.CreateOtpKey(profile.Mobile.ToString(), profile.UniqueIdentifier, otpType), ttl);

            await _messagingService.SendOtpSmsAsync(null, profile.Mobile.ToString(), null, string.Format(msgFormat, otp, Environment.NewLine));

        }



        private async Task CheckHasAccess(string parameter, int rate)
        {

            var controller = ControllerContext.ActionDescriptor.ControllerName;
            var action = ControllerContext.ActionDescriptor.ActionName;
            var hasAccess = await _rateLimitService.HasAccessAsync($"{controller}:{action}:{parameter}", 1 * 60 * 5,
                rate);

            if (!hasAccess)
                throw new CerberusException(ErrorCode.TooManyRequest, "Rate limit exceeded");
        }
    }
}
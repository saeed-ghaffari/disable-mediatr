﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Configuration;
using Cerberus.Api.Filters;
using Cerberus.Domain;
using Cerberus.Domain.CustomValidation;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.ViewModel.Api;
using Gatekeeper.AspModules.ActionFilter;
using Gatekeeper.Core.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class BankingAccountController : BaseController
    {
        private readonly IBankingAccountApiService _bankingAccountService;
        private readonly IOtpService _otpService;
        private readonly IProfileApiService _profileService;
        private readonly IOptions<ApiConfiguration> _apiConfiguration;

        public BankingAccountController(
            IBankingAccountApiService legalPersonService,
            IOtpService otpService,
            IProfileApiService profileService,
            IOptions<ApiConfiguration> apiConfiguration)
        {
            _bankingAccountService = legalPersonService;
            _otpService = otpService;
            _profileService = profileService;
            _apiConfiguration = apiConfiguration;
        }

        [HttpGet]
        [Route("v{version:apiVersion}/profiles/{uniqueIdentifier}/bankingAccounts")]
        [RateLimitFilterFactory(Order = 2, Limit = 600, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 15, VaryByParamsPeriodInSec = 10 * 60 * 60)]
        public async Task<IEnumerable<BankingAccountViewModel>> GetAsync([Required]string uniqueIdentifier)
        {
           var targetProfile = await GetTargetProfileAsync(uniqueIdentifier);

            var result = await _bankingAccountService.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
            if (result == null)
                throw new CerberusException(ErrorCode.NotFound);

            return result;
        }

        [HttpGet]
        [Route("v{version:apiVersion}/servicesWithOtp/profiles/{uniqueIdentifier}/bankingAccounts")]
        [RateLimitFilterFactory(Order = 2, Limit = 600, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 15, VaryByParamsPeriodInSec = 10 * 60 * 60)]
        public async Task<IEnumerable<BankingAccountViewModel>> GetByOtpAsync([FromRoute,Required]string uniqueIdentifier, [FromQuery,Required,MaxLength(5)] string otp)
        {
            var targetProfile = await GetTargetProfileAsync(uniqueIdentifier);

            if (!await _otpService.IsValidAsync(
                ConventionalHelper.CreateOtpKey(targetProfile.Mobile.ToString(), uniqueIdentifier, OtpType.Kyc),
                otp,
                _apiConfiguration.Value.KillAfterValidation))
                throw new CerberusException(ErrorCode.InvalidOtp, "invalid otp");

            var result = await _bankingAccountService.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
            if (result == null)
                throw new CerberusException(ErrorCode.NotFound);

            return result;
        }

     

        private async Task<ProfileViewModel> GetTargetProfileAsync(string uniqueIdentifier)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await _profileService.GetByUniqueIdentifierAsync(uniqueIdentifier);
            if (profile == null || !profile.Status.IsConfirmed())
                throw new CerberusException(ErrorCode.NotFound);

            return profile;

        }
    }
}
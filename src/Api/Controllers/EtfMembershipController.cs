﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Filters;
using Cerberus.Api.Model;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [Route("v{version:apiVersion}/etfMembership")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class EtfMembershipController : BaseController
    {
        private readonly ITaskRepository _taskRepository;
        public EtfMembershipController(
            ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

   


        [HttpPost]
        [Route("")]
        [RateLimitFilterFactory(Order = 2, Limit = 10, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<CreateAsyncTaskResponse> CreateStoreEtfMembershipListAsync(CreateSyncEtfMembershipTaskRequest request)
        {
            if (!EnumerableExtensions.Any(request.Members))
                throw new CerberusException(ErrorCode.BadRequest, "membership list can not be null");
            if (request.Members.Count()>10000)
            {
                throw new CerberusException(ErrorCode.BadRequest, "membership list must be less than 10000");
            }

            var task = new ScheduledTask()
            {
                DueTime = DateTime.Now,
                GroupId = CerberusTasksIdentifierHelper.SyncEtfMembershipProcessing.Key,
                Params = JsonConvert.SerializeObject(new SyncEtfMembershipTaskParams()
                {
                    Members = request.Members,
                    ServiceId = CurrentService.ServiceId,
                }),
                RefId = CurrentService.ServiceId,
                Type = CerberusTasksIdentifierHelper.SyncEtfMembershipProcessing.Value
            };

            task = await _taskRepository.AddAsync(task);

            //set 202 http status code
            ResponseHttpStatusCode = HttpStatusCode.Accepted;

            return new CreateAsyncTaskResponse()
            {
                Reference = task.Id.ToString()
            };
        }
    }
}
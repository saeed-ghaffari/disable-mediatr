﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Filters;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class BankController : BaseController
    {

        private readonly IBankService _bankService;
        private readonly IMapper _mapper;

        public BankController(IBankService bankService, IMapper mapper)
        {
            _bankService = bankService;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("v{version:apiVersion}/banks")]
        [RateLimitFilterFactory(Order = 2, Limit = 30, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<IEnumerable<BankViewModel>> GetBanksListAsyncTask()
        {
            return _mapper.Map<IEnumerable<Bank>, IEnumerable<BankViewModel>>(await _bankService.GetListAsync());
        }
    }
}
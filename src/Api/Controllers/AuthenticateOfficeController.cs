﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Filters;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class AuthenticateOfficeController : BaseController
    {
        private readonly IAuthenticationOfficesService _authenticationOfficesService;
        private readonly IMapper _mapper;

        public AuthenticateOfficeController(IAuthenticationOfficesService authenticationOfficesService, IMapper mapper)
        {
            _authenticationOfficesService = authenticationOfficesService;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("v{version:apiVersion}/authenticateoffices")]
        [RateLimitFilterFactory(Order = 2, Limit = 30, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<IEnumerable<AuthenticateOfficeViewModel>> GetAllAuthenticateOffice()
        {
            return await _authenticationOfficesService.GetAllAsync();
        }

    }
}
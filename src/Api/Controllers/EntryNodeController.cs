﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Configuration;
using Cerberus.Api.Extensions;
using Cerberus.Api.Filters;
using Cerberus.Api.Model;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Interface.Services.FGM;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Domain.ViewModel.FGM;
using Cerberus.Utility;
using Gatekeeper.AspModules.ActionFilter;
using Gatekeeper.Core.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Profile = Cerberus.Domain.Model.Profile;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class EntryNodeController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IProfileService _profileService;
        private readonly IPaymentService _paymentService;
        private readonly IOfflineTaskService _offlineTaskService;
        private readonly IOtpService _otpService;
        private readonly ApiConfiguration _apiConfiguration;
        private readonly IThirdPartyServiceService _thirdPartyServiceService;
        private readonly IProvinceService _provinceService;
        private readonly ICityService _cityService;
        private readonly IAddressSectionService _addressSectionService;
        private readonly ICountryService _countryService;
        private readonly IFgmService _fgmService;
        private readonly IBankingAccountService _bankingAccountService;
        private readonly IJobInfoService _jobInfoService;
        private readonly IAddressService _addressService;
        private readonly IFinancialInfoService _financialInfoService;
        private readonly IPrivatePersonService _privatePersonService;
        private readonly ITradingCodeService _tradingCodeService;
        private readonly IJobService _jobService;
        private readonly IBankService _bankService;
        private readonly EntryNodeConfiguration _entryNodeConfiguration;
        public EntryNodeController(IMapper mapper, IProfileService profileService,
            IPaymentService paymentService, IOfflineTaskService offlineTaskService, IOtpService otpService,
            IOptions<ApiConfiguration> apiConfiguration, IThirdPartyServiceService thirdPartyServiceService,
            ICityService cityService, IProvinceService provinceService,
            IAddressSectionService addressSectionService, ICountryService countryService,
            IFgmService fgmService, IBankingAccountService bankingAccountService, IJobInfoService jobInfoService,
            IAddressService addressService, IFinancialInfoService financialInfoService,
            IPrivatePersonService privatePersonService, ITradingCodeService tradingCodeService, IJobService jobService, IBankService bankService, IOptions<EntryNodeConfiguration> entryNodeConfiguration)
        {
            _mapper = mapper;
            _profileService = profileService;
            _paymentService = paymentService;
            _offlineTaskService = offlineTaskService;
            _otpService = otpService;
            _apiConfiguration = apiConfiguration.Value;

            _thirdPartyServiceService = thirdPartyServiceService;
            _cityService = cityService;
            _provinceService = provinceService;
            _addressSectionService = addressSectionService;
            _countryService = countryService;
            _fgmService = fgmService;
            _bankingAccountService = bankingAccountService;
            _jobInfoService = jobInfoService;
            this._addressService = addressService;
            _financialInfoService = financialInfoService;
            _privatePersonService = privatePersonService;
            _tradingCodeService = tradingCodeService;
            this._jobService = jobService;
            _bankService = bankService;
            _entryNodeConfiguration = entryNodeConfiguration.Value;
        }


        [HttpPost]
        [Route("v{version:apiVersion}/entryNodeServices/profiles")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<ProfileViewModel> CreateProfileAsync(CreateProfileRequest request)
        {
            var service = await _thirdPartyServiceService.GetAsync(CurrentService.ServiceId);

            var msisdn = request.PrivatePerson.Mobile;

            if (!ValidationHelper.IsMsisdnValid(ref msisdn, out var carrier) || carrier == null)
            {
                ModelState.AddModelError(nameof(CreatePrivatePersonRequest.Mobile), "invalid mobile number");
                throw new CerberusException(ErrorCode.BadRequest, value: ModelState.ToViewModel());
            }

            if (!_entryNodeConfiguration.IgnoreIssuerOtp ||
                !_entryNodeConfiguration.IgnoresList.Contains(CurrentService.ServiceId.ToInt()))
            {
                if (!await _otpService.IsValidAsync(
                    ConventionalHelper.CreateIssuerOtpKey(msisdn, request.PrivatePerson.NationalCode,
                        OtpType.EntryNode),
                    request.IssuerOtp,
                    false))
                    throw new CerberusException(ErrorCode.Forbidden, "invalid issuerOtp");
            }
         

            await CheckValidData(request);


            var existProfile = await _profileService.GetByUniqueIdentifierWithNoCacheAsync(request.PrivatePerson.NationalCode);

            if (existProfile != null)
            {
                if (existProfile.Status >= ProfileStatus.SuccessPayment)
                    throw new CerberusException(ErrorCode.Conflicts, $"profile which identify by '{request.PrivatePerson.NationalCode}' national code, already exists in sejam DB");

                if (existProfile.Status == ProfileStatus.Init)
                    await DeleteProfile(existProfile);
            }

            //await CheckExistMobile(msisdn.ToLong());

            //if (service.HasFactor)
            //{
            //    var existPayment =
            //        await _paymentService.GetListAsync(x => x.SaleReferenceId == request.Payment.SaleReferenceId);
            //    var payments = existPayment.ToList();
            //    if (payments.Any(x =>
            //        x.SaleReferenceId == request.Payment.SaleReferenceId && x.Gateway == PaymentGateway.EntryNodeWithPayment))
            //    {
            //        throw new CerberusException(ErrorCode.Conflicts,
            //            $"this saleReferenceId : {request.Payment.SaleReferenceId} is already exists in sejam DB");
            //    }
            //}

            var profile = new Profile()
            {
                UniqueIdentifier = request.PrivatePerson.NationalCode,
                Type = ProfileOwnerType.IranianPrivatePerson,
                Carrier = carrier.Value,
                Status = ProfileStatus.Init,
                Mobile = long.Parse(msisdn),
            };

            try
            {
                await _profileService.CreateAsync(profile);
            }
            catch (Exception ex)
            {
                var sqlException = ex.InnerException as System.Data.SqlClient.SqlException;
                if (sqlException?.Number == 2601 || sqlException?.Number == 2627)
                {
                    throw new CerberusException(ErrorCode.Conflicts, $"profile which identify by '{request.PrivatePerson.NationalCode}' national code, already exists in sejam DB");
                }
            }


            var payment = _mapper.Map<CreatePaymentRequest, Payment>(request.Payment);
            payment.ProfileId = profile.Id;
            payment.ReferenceServiceId = CurrentService.ServiceId;
            payment.Status = PaymentStatus.Settle;
            payment.IsConfirmedReference = false;
            payment.Gateway = service.HasFactor ? PaymentGateway.EntryNodeWithPayment : PaymentGateway.EntryNodeWithNoPayment;
            if (!service.HasFactor) payment.Amount = 0;
            await _paymentService.CreateAsync(payment);

            var privatePerson = _mapper.Map<CreatePrivatePersonRequest, PrivatePerson>(request.PrivatePerson);
            privatePerson.ProfileId = profile.Id;
            await _privatePersonService.CreateAsync(privatePerson);

            var addresses = new List<Address>(1) { _mapper.Map<CreateAddressRequest, Address>(request.Address) };
            var address = addresses.FirstOrDefault();
            if (address != null)
            {
                address.ProfileId = profile.Id;
                await _addressService.CreateAsync(address);
            }

            var tradingCodes = _mapper.Map<IList<CreateTradingCode>, IList<TradingCode>>(request.TradingCodes);
            foreach (var tradingCode in tradingCodes)
            {
                tradingCode.ProfileId = profile.Id;
                await _tradingCodeService.CreateAsync(tradingCode);
            }
            var jobInfo = _mapper.Map<CreateJobInfoRequest, JobInfo>(request.JobInfo);
            if (request.JobInfo.JobId != 123159) jobInfo.JobDescription = null;
            jobInfo.ProfileId = profile.Id;
            await _jobInfoService.CreateAsync(jobInfo);


            var financialInfo = _mapper.Map<CreateFinancialInfoRequest, FinancialInfo>(request.FinancialInfo);
            financialInfo.ProfileId = profile.Id;
            await _financialInfoService.CreateAsync(financialInfo);


            var accounts =
                _mapper.Map<IList<CreateBankingAccountRequest>, IList<BankingAccount>>(request.BankingAccounts);

            foreach (var bankingAccount in accounts)
            {
                bankingAccount.ProfileId = profile.Id;
                await _bankingAccountService.CreateAsync(bankingAccount);
            }





            if (service.HasFactor)
            {
                await _offlineTaskService.ScheduleSetFakeFactorAsync(payment.Id, DateTime.Now.AddSeconds(60));
            }

            await _offlineTaskService.ScheduleOfflineTasksAsync(profile.Id, null, DateTime.Now.AddMinutes(3));


            //Confirm Data
            profile.Status = ProfileStatus.SuccessPayment;
            await _profileService.UpdateIsConfirmedEntryNodeAsync(profile, payment.Id);

            await _otpService.KillAsync(ConventionalHelper.CreateIssuerOtpKey(msisdn, request.PrivatePerson.NationalCode, OtpType.EntryNode));
            return _mapper.Map<Profile, ProfileViewModel>(profile);
        }

        [HttpPost]
        [Route("v{version:apiVersion}/entryNodeWithPaymentServices/profiles")]
        [RateLimitFilterFactory(Order = 2, Limit = 1000, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<ProfileViewModel> CreateProfileWithPaymentAsync(CreateProfileRequest request)
        {
            if (string.IsNullOrEmpty(request.Payment.ServiceId) || string.IsNullOrEmpty(request.Payment.OrderId) ||
                string.IsNullOrWhiteSpace(request.Payment.ServiceId) || string.IsNullOrWhiteSpace(request.Payment.OrderId))
                throw new CerberusException(ErrorCode.BadRequest, "serviceId or orderId can not be null or empty");


            var msisdn = request.PrivatePerson.Mobile;

            if (!ValidationHelper.IsMsisdnValid(ref msisdn, out var carrier) || carrier == null)
            {
                ModelState.AddModelError(nameof(CreatePrivatePersonRequest.Mobile), "invalid mobile number");
                throw new CerberusException(ErrorCode.BadRequest, value: ModelState.ToViewModel());
            }

            if (!await _otpService.IsValidAsync(
                ConventionalHelper.CreateIssuerOtpKey(msisdn, request.PrivatePerson.NationalCode, OtpType.EntryNode),
                request.IssuerOtp,
               false))
                throw new CerberusException(ErrorCode.Forbidden, "invalid issuerOtp");

            await CheckValidData(request);




            var existProfile = await _profileService.GetByUniqueIdentifierWithNoCacheAsync(request.PrivatePerson.NationalCode);

            if (existProfile != null)
            {
                if (existProfile.Status >= ProfileStatus.SuccessPayment)
                    throw new CerberusException(ErrorCode.Conflicts, $"profile which identify by '{request.PrivatePerson.NationalCode}' national code, already exists in sejam DB");

                if (existProfile.Status == ProfileStatus.Init)
                    await DeleteProfile(existProfile);
            }

            //   await CheckExistMobile(msisdn.ToLong());

            var existPayment =
                await _paymentService.GetListAsync(x => x.ClientReference == request.Payment.OrderId && x.ServiceId == request.Payment.ServiceId);
            var payments = existPayment.ToList();
            if (payments.Any())
                throw new CerberusException(ErrorCode.Conflicts, $"this OrderId : {request.Payment.OrderId} is already exists in sejam DB");


            var payResult = await CheckPaymentData(request.Payment);

            if (payResult.SaleReferenceId != request.Payment.SaleReferenceId || payResult.SaleReferenceNumber != request.Payment.ReferenceNumber || payResult.Amount != request.Payment.Amount.ToString())
                throw new CerberusException(ErrorCode.BadRequest, "invalid payment data");


            var profile = new Profile()
            {
                UniqueIdentifier = request.PrivatePerson.NationalCode,
                Type = ProfileOwnerType.IranianPrivatePerson,
                Carrier = carrier.Value,
                Status = ProfileStatus.Init,
                Mobile = long.Parse(msisdn),
            };
            try
            {
                await _profileService.CreateAsync(profile);
            }
            catch (Exception ex)
            {
                var sqlException = ex.InnerException as System.Data.SqlClient.SqlException;
                if (sqlException?.Number == 2601 || sqlException?.Number == 2627)
                    throw new CerberusException(ErrorCode.Conflicts, $"profile which identify by '{request.PrivatePerson.NationalCode}' national code, already exists in sejam DB");
            }

            var payment = _mapper.Map<CreatePaymentRequest, Payment>(request.Payment);
            payment.ReferenceServiceId = CurrentService.ServiceId;
            payment.Status = PaymentStatus.Settle;
            payment.IsConfirmedReference = false;
            payment.Gateway = PaymentGateway.FgmEntryNode;
            payment.ClientReference = request.Payment.OrderId;
            payment.ServiceId = request.Payment.ServiceId;
            payment.ProfileId = profile.Id;
            await _paymentService.CreateAsync(payment);

            var privatePerson = _mapper.Map<CreatePrivatePersonRequest, PrivatePerson>(request.PrivatePerson);
            privatePerson.ProfileId = profile.Id;
            await _privatePersonService.CreateAsync(privatePerson);

            var addresses = new List<Address>(1) { _mapper.Map<CreateAddressRequest, Address>(request.Address) };
            var address = addresses.FirstOrDefault();
            if (address != null)
            {
                address.ProfileId = profile.Id;
                await _addressService.CreateAsync(address);
            }

            var tradingCodes = _mapper.Map<IList<CreateTradingCode>, IList<TradingCode>>(request.TradingCodes);
            foreach (var tradingCode in tradingCodes)
            {
                tradingCode.ProfileId = profile.Id;
                await _tradingCodeService.CreateAsync(tradingCode);
            }
            var jobInfo = _mapper.Map<CreateJobInfoRequest, JobInfo>(request.JobInfo);
            if (request.JobInfo.JobId != 123159) jobInfo.JobDescription = null;
            jobInfo.ProfileId = profile.Id;
            await _jobInfoService.CreateAsync(jobInfo);


            var financialInfo = _mapper.Map<CreateFinancialInfoRequest, FinancialInfo>(request.FinancialInfo);
            financialInfo.ProfileId = profile.Id;
            await _financialInfoService.CreateAsync(financialInfo);


            var accounts =
                _mapper.Map<IList<CreateBankingAccountRequest>, IList<BankingAccount>>(request.BankingAccounts);

            foreach (var bankingAccount in accounts)
            {
                bankingAccount.ProfileId = profile.Id;
                await _bankingAccountService.CreateAsync(bankingAccount);
            }




            await _offlineTaskService.ScheduleSetFactorAsync(payment.Id, DateTime.Now.AddSeconds(30), true);


            await _offlineTaskService.ScheduleOfflineTasksAsync(profile.Id, null, DateTime.Now.AddMinutes(3));


            //Confirm Data
            profile.Status = ProfileStatus.SuccessPayment;
            await _profileService.UpdateIsConfirmedEntryNodeAsync(profile, payment.Id);

            await _otpService.KillAsync(ConventionalHelper.CreateIssuerOtpKey(msisdn, request.PrivatePerson.NationalCode, OtpType.EntryNode));

            return _mapper.Map<Profile, ProfileViewModel>(profile);
        }



        private async Task DeleteProfile(Profile profile)
        {
            var payments = await _paymentService.GetListAsync(x => x.ProfileId == profile.Id);
            var paymentList = payments.ToList();

            if (paymentList.Any())
            {

                if (paymentList.Any(x => x.Status == PaymentStatus.Settle || x.Status == PaymentStatus.Init || x.Status == PaymentStatus.Verify))
                {
                    throw new CerberusException(ErrorCode.Conflicts, $"profile which identify by '{profile.UniqueIdentifier}' national code, already exists in sejam DB");
                }

                foreach (var p in paymentList)
                {
                    await _paymentService.DeleteAsync(p);
                }
            }
            await _profileService.DeleteAsync(profile);
        }




        private async Task<CallbackResponseViewModel> CheckPaymentData(CreatePaymentRequest paymentRequest)
        {
            var statusResult = await _fgmService.CheckOtherPaymentStatus(paymentRequest.OrderId, paymentRequest.ServiceId.ToGuid());
            if (statusResult.Error != null)
                switch (statusResult.Error.ErrorCode)
                {
                    case (int)HttpStatusCode.RequestTimeout:
                        throw new CerberusException(ErrorCode.UndefinedService, "can not connect to check payment service");
                    case (int)HttpStatusCode.NotFound:
                        throw new CerberusException(ErrorCode.NotFound, "payment not found");
                    default:
                        throw new CerberusException(ErrorCode.FailedPayment, "payment was not successful");
                }
            return statusResult.Data;

        }




        private async Task CheckValidData(CreateProfileRequest request)
        {

            if (request.JobInfo.JobId == 123159 && string.IsNullOrEmpty(request.JobInfo.JobDescription))
                throw new CerberusException(ErrorCode.BadRequest, "the jobDescription can not be null when Job Id is 123159");

            if (!request.BankingAccounts.Any(x => x.IsDefault))
                throw new CerberusException(ErrorCode.BadRequest, "the one account must be the default");

            if (request.BankingAccounts.GroupBy(n => n.Sheba).Any(c => c.Count() > 1))
                throw new CerberusException(ErrorCode.BadRequest, "the account is duplicate");

            var country = await _countryService.GetListAsync();
            if (country.All(x => x.Id != request.Address.CountryId))
                throw new CerberusException(ErrorCode.BadRequest, "invalid countryId");

            var province = await _provinceService.GetListAsync(request.Address.CountryId.GetValueOrDefault());
            if (province.All(x => x.Id != request.Address.ProvinceId))
                throw new CerberusException(ErrorCode.BadRequest, "invalid provinceId");

            var city = await _cityService.GetListAsync(request.Address.ProvinceId.GetValueOrDefault());
            if (city.All(x => x.Id != request.Address.CityId))
                throw new CerberusException(ErrorCode.BadRequest, "invalid cityId");

            if (request.Address.SectionId != null)
            {
                var section = await _addressSectionService.GetListAsync(request.Address.CityId.GetValueOrDefault());
                if (section.All(x => x.Id != request.Address.SectionId))
                    throw new CerberusException(ErrorCode.BadRequest, "invalid sectionId");
            }

            var job = await _jobService.GetListAsync();
            if (job.All(x => x.Id != request.JobInfo.JobId))
                throw new CerberusException(ErrorCode.BadRequest, "invalid jobId");

            var bank = await _bankService.GetListAsync();
            var bankCity = await _cityService.GetListAsync();
            foreach (var bankingAccount in request.BankingAccounts)
            {
                if (bank.All(x => x.Id != bankingAccount.BankId))
                    throw new CerberusException(ErrorCode.BadRequest, "invalid bankId");

                if (bankCity.All(x => x.Id != bankingAccount.BranchCityId))
                    throw new CerberusException(ErrorCode.BadRequest, "invalid branchCityId");
            }
        }


        private async Task CheckExistMobile(long mobile)
        {
            var existMobile = await _profileService.RegisterMobileCount(mobile, ProfileStatus.SuccessPayment, _apiConfiguration.RegisterMobileCount);
            if (existMobile)
                throw new CerberusException(ErrorCode.Conflicts, "the mobile number already exists in sejam DB");
        }
    }
}
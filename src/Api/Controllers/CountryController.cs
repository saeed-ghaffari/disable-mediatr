﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Filters;
using Cerberus.Api.Model;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [Route("v{version:apiVersion}/countries")]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class CountryController : BaseController
    {
        private readonly ICountryService _countryService;
        private readonly IProvinceService _provinceService;
        private readonly ICityService _cityService;
        private readonly IAddressSectionService _addressSectionService;
        private readonly IMapper _mapper;

        public CountryController(ICountryService countryService, IProvinceService provinceService, ICityService cityService, IAddressSectionService addressSectionService, IMapper mapper)
        {
            _countryService = countryService;
            _provinceService = provinceService;
            _cityService = cityService;
            _addressSectionService = addressSectionService;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("")]
        [RateLimitFilterFactory(Order = 2, Limit = 20, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<IEnumerable<CountryViewModel>> GetCountriesAsync()
        {
            return _mapper.Map<IEnumerable<Country>, IEnumerable<CountryViewModel>>(await _countryService.GetListAsync());
        }

        [HttpGet]
        [Route("{countryId}/provinces")]
        [RateLimitFilterFactory(Order = 2, Limit = 30, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<IEnumerable<ProvinceViewModel>> GetProvincesAsync([FromRoute]long countryId)
        {
            return _mapper.Map<IEnumerable<Province>, IEnumerable<ProvinceViewModel>>(await _provinceService.GetListAsync(countryId));
        }


        [HttpGet]
        [Route("{countryId}/provinces/{provinceId}/cities")]
        [RateLimitFilterFactory(Order = 2, Limit = 30, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<IEnumerable<CityViewModel>> GetCitiesAsync([FromRoute]long countryId, [FromRoute] long provinceId)
        {
            return _mapper.Map<IEnumerable<City>, IEnumerable<CityViewModel>>(await _cityService.GetListAsync(provinceId));
        }

        [HttpGet]
        [Route("{countryId}/provinces/{provinceId}/cities/{cityId}/sections")]
         [RateLimitFilterFactory(Order = 2, Limit = 30, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<IEnumerable<AddressSectionViewModel>> GetSectionsAsync([FromRoute]long countryId, [FromRoute] long provinceId, [FromRoute] long cityId)
        {
            return _mapper.Map<IEnumerable<AddressSection>, IEnumerable<AddressSectionViewModel>>(await _addressSectionService.GetListAsync(cityId));
        }
    }
}
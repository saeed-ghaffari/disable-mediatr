﻿using System.Threading.Tasks;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Configuration;
using Cerberus.Api.Model;
using Cerberus.Domain;
using Cerberus.Domain.Interface.Services;
using Gatekeeper.AspModules.ActionFilter;
using Gatekeeper.AspModules.Interface;
using Gatekeeper.Core.Interface;
using GateKeeper.Auth;
using GateKeeper.Auth.Enum;
using GateKeeper.Auth.Interface;
using GateKeeper.Auth.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [Route("v{version:apiVersion}/accessToken")]
    [ApiController]
    public class AccessTokenController : BaseController
    {
        private readonly IUserService _service;
        private readonly ILogger<AccessTokenController> _logger;
        private readonly IThirdPartyServiceService _thirdPartyService;
        private readonly IJwtTokenService<IssuerAccessTokenPayload> _jwtTokenService;
        private readonly ApiConfiguration _apiConfiguration;
        private readonly IRateLimitService _rateLimitService;
        public AccessTokenController(IUserService service,
            ILogger<AccessTokenController> logger,
            IThirdPartyServiceService thirdPartyService, IJwtTokenService<IssuerAccessTokenPayload> jwtTokenService,
            IOptions<ApiConfiguration> apiConfiguration, IRateLimitService rateLimitService)
        {
            _service = service;
            _logger = logger;
            _thirdPartyService = thirdPartyService;
            _jwtTokenService = jwtTokenService;
            _rateLimitService = rateLimitService;
            _apiConfiguration = apiConfiguration.Value;
        }

        [HttpPost("")]
        [RateLimitFilterFactory(Order = 1, Limit = 60, PeriodInSec = 600)]
        public async Task<CreateAccessTokenResponse> CreateAccessTokenAsync(CreateAccessTokenRequest request)
        {
            await CheckHasAccess(request.Username, 10);
            User user;
            try
            {
                user = await _service.GetAsync(request.Username, request.Password);
            }
            catch (GateKeeperAuthException exception)
            {
                switch (exception.ErrorCode)
                {
                    case GateKeeper.Auth.Enum.ErrorCode.InCorrectUsernameOrPassword:
                        throw new CerberusException(Domain.Enum.ErrorCode.Unauthorized, "incorrect username or password");

                    case GateKeeper.Auth.Enum.ErrorCode.Locked:
                        throw new CerberusException(Domain.Enum.ErrorCode.LockedUser);

                    default:
                        _logger.LogDebug(exception, "GateKeeperAuthException.ErrorCode");
                        throw new CerberusException(Domain.Enum.ErrorCode.Unauthorized, "incorrect username or password");
                }
            }

            var service = await _thirdPartyService.GetAsync(user.RefId);
            if (service == null)
                throw new CerberusException(Domain.Enum.ErrorCode.UndefinedService, "incomplete service registration");

            if (!service.Enabled)
                throw new CerberusException(Domain.Enum.ErrorCode.Unauthorized, "service is disabled");


            var tokenPayload = new IssuerAccessTokenPayload()
            {
                ServiceId = service.Id,
                UserId = user.Id
            };

            var accessToken = await _jwtTokenService.CreateAsync(tokenPayload, _apiConfiguration.AccessTokenTtl);

            return new CreateAccessTokenResponse()
            {
                AccessToken = accessToken,
                Ttl = _apiConfiguration.AccessTokenTtl
            };
        }

        private async Task CheckHasAccess(string parameter, int rate)
        {

            var controller = ControllerContext.ActionDescriptor.ControllerName;
            var action = ControllerContext.ActionDescriptor.ActionName;
            var hasAccess = await _rateLimitService.HasAccessAsync($"{controller}:{action}:{parameter}", 1 * 60 * 10,
                rate);

            if (!hasAccess)
                throw new CerberusException(Domain.Enum.ErrorCode.TooManyRequest, "Rate limit exceeded");
        }
    }
}
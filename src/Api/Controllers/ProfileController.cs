﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Configuration;
using Cerberus.Api.Filters;
using Cerberus.Api.Model;
using Cerberus.Domain;
using Cerberus.Domain.CustomValidation;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Utility;
using Gatekeeper.AspModules.ActionFilter;
using Gatekeeper.Core.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [Route("v{version:apiVersion}/profiles")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class ProfileController : BaseController
    {
        private readonly IProfileApiService _profileApiService;
        private readonly IOtpService _otpService;
        private readonly IOptions<ApiConfiguration> _apiConfiguration;
        private readonly IPrivatePersonService _privatePersonService;
     private readonly IProfileHistoryService _profileHistoryService;
        private readonly IPermanentOtpService _permanentOtpService;
        private readonly IProfileService _profileService;
        private readonly LegalPersonsWhiteListConfiguration _legalPersonsWhiteListConfiguration;
        private readonly IMapper _mapper;
        private readonly IOptions<PermanentOtpConfiguration> _permanentOtpConfigurations;
        private readonly AuthenticatorConfiguration _authenticatorConfiguration;
        public ProfileController(
            IProfileApiService profileApiService,
            IOtpService otpService,
            IOptions<ApiConfiguration> apiConfiguration,
            IPrivatePersonService privatePersonService,
            IProfileHistoryService profileHistoryService,
            IPermanentOtpService permanentOtpService,
            IOptions<PermanentOtpConfiguration> permanentOtpConfigurations,
            IProfileService profileService,
            IOptions<LegalPersonsWhiteListConfiguration> legalPersonsWhiteListConfiguration,
            IMapper mapper, IOptions<AuthenticatorConfiguration> authenticatorConfiguration)
        {
            _profileApiService = profileApiService;
            _otpService = otpService;
            _apiConfiguration = apiConfiguration;
            _privatePersonService = privatePersonService;
            _profileHistoryService = profileHistoryService;
            _permanentOtpService = permanentOtpService;
            _permanentOtpConfigurations = permanentOtpConfigurations;
            _profileService = profileService;
            _mapper = mapper;
          _authenticatorConfiguration = authenticatorConfiguration.Value;
            _legalPersonsWhiteListConfiguration = legalPersonsWhiteListConfiguration.Value;
        }


        [HttpGet]
        [Route("{uniqueIdentifier}/privatePerson/signature")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<FileViewModel> GetSignatureAsync([Required]string uniqueIdentifier)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);
            var profile = await _profileService.GetByUniqueIdentifierFromCacheAsync(uniqueIdentifier);

            if (profile == null || profile.Status != ProfileStatus.Sejami)
                throw new CerberusException(ErrorCode.NotFound);

            var fileString = await _privatePersonService.GetIncludeImageAsync(profile.Id, ImageType.SignatureImage);

            if (fileString == null)
                throw new CerberusException(ErrorCode.NotFound);

            return _mapper.Map<FileViewModel>(fileString);
     

        }

        [HttpGet]
        [Route("{uniqueIdentifier}/privatePerson/image")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 100, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<FileViewModel> GetImageAsync([Required]string uniqueIdentifier)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await _profileService.GetByUniqueIdentifierFromCacheAsync(uniqueIdentifier);

            if (profile == null || profile.Status != ProfileStatus.Sejami)
                throw new CerberusException(ErrorCode.NotFound);

 
            var fileString = await _privatePersonService.GetIncludeImageAsync(profile.Id,ImageType.PersonImage);

            if (fileString == null)
                throw new CerberusException(ErrorCode.NotFound);

            return  _mapper.Map<FileViewModel>(fileString);
        }

        [HttpGet]
        [Route("{uniqueIdentifier}")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 10, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<ProfileViewModel> GetAsync([Required]string uniqueIdentifier)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await _profileApiService.GetForProfilingByUniqueIdentifierAsync(uniqueIdentifier, true);
            if (profile == null)
                throw new CerberusException(ErrorCode.NotFound);

            return profile;
        }

        [HttpGet]
        [Route("{uniqueIdentifier}/tracecode")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<ProfileViewModel> GetTraceCodeProfileAsync([Required]string uniqueIdentifier)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await _profileApiService.GetForProfilingByUniqueIdentifierAsync(uniqueIdentifier, true);
            if (profile == null || profile.Status < _apiConfiguration.Value.CompleteProfileStatus)
                throw new CerberusException(ErrorCode.NotFound);

            return profile;
        }

        [HttpGet]
        [Route("~/v{version:apiVersion}/profiles/{uniqueIdentifier}/sejamiprofile")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<ProfileViewModel> GetSejamiProfileAsync([Required]string uniqueIdentifier)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);
            var profile = await _profileApiService.GetForProfilingByUniqueIdentifierAsync(uniqueIdentifier, true);
            if (profile == null || profile.Status != ProfileStatus.Sejami)
                throw new CerberusException(ErrorCode.NotFound);

            return profile;
        }

        [HttpGet]
        [Route("{uniqueIdentifier}/{traceCode}")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<ProfileViewModel> GetUniqueIdentifierAndTraceCodeAsync(
            [FromRoute, Required]string uniqueIdentifier,
            [FromRoute, Required, MaxLength(10)]string traceCode,
            [FromQuery, Required, MaxLength(5)]string otp)
        {
            if (string.IsNullOrWhiteSpace(otp) || string.IsNullOrEmpty(otp))
                throw new CerberusException(ErrorCode.BadRequest, "otp can not be null");

            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);
            var profile = await _profileApiService.GetForProfilingByUniqueIdentifierAsync(uniqueIdentifier, true);
            if (profile == null || profile.Status < _apiConfiguration.Value.CompleteProfileStatus)
                throw new CerberusException(ErrorCode.NotFound);

            if (!profile.TraceCode.Equals(traceCode))
                throw new CerberusException(ErrorCode.InvalidTraceCode);

            if (!await _otpService.IsValidAsync(
                ConventionalHelper.CreateOtpKey(profile.Mobile.ToString(), uniqueIdentifier, OtpType.Kara),
                otp,
                _apiConfiguration.Value.KillAfterValidation))
                throw new CerberusException(ErrorCode.InvalidOtp, "invalid otp");

            if (!profile.Status.IsConfirmed())
                throw new CerberusException(ErrorCode.InCompleteRegistrations);

            return profile;
        }

        [HttpGet]
        [Route("~/v{version:apiVersion}/internalService/profiles/{uniqueIdentifier}/{traceCode}")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 30, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<ProfileViewModel> GetUniqueIdentifierAndTraceCodeAsync(
            [FromRoute, Required]string uniqueIdentifier,
            [FromRoute, Required, MaxLength(10)]string traceCode)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await GetProfileAsync(uniqueIdentifier, true);

            if (!profile.TraceCode.Equals(traceCode))
                throw new CerberusException(ErrorCode.InvalidTraceCode);

            if (profile.Type == ProfileOwnerType.IranianPrivatePerson)
            {
                var personImage = await _privatePersonService.GetIncludeImageAsync(profile.Id,ImageType.SignatureImage);
                if (personImage != null)
                    profile.PrivatePerson.SignatureFile = _mapper.Map<FileViewModel>(personImage);
            }

            return profile;
        }

        [HttpGet]
        [Route("{uniqueIdentifier}/sejamStatus")]
        [Route("~/v{version:apiVersion}/entryNodeServices/profiles/{uniqueIdentifier}/sejamStatus")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<GetProfileStatusResponse> GetProfileStatusAsync([Required]string uniqueIdentifier)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);
            var result = await _profileService.GetByUniqueIdentifierFromCacheAsync(uniqueIdentifier);
            if (result == null)
                throw new CerberusException(ErrorCode.NotFound);

            return new GetProfileStatusResponse()
            {
                Status = result.Status
            };
        }

        [HttpGet]
        [Route("~/v{version:apiVersion}/servicesWithOtp/profiles/{uniqueIdentifier}/sejamStatus")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<GetProfileStatusResponse> EntryNodeGetProfileStatusAsync([FromRoute, Required]string uniqueIdentifier, [FromQuery, Required, MaxLength(5)] string otp)
        {
            if (string.IsNullOrWhiteSpace(otp) || string.IsNullOrEmpty(otp))
                throw new CerberusException(ErrorCode.BadRequest, "otp can not be null");

            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await _profileService.GetByUniqueIdentifierFromCacheAsync(uniqueIdentifier);

            if (!await _otpService.IsValidAsync(
                ConventionalHelper.CreateOtpKey(profile?.Mobile.ToString(), uniqueIdentifier, OtpType.EntryNodeStatus),
                otp,
                _apiConfiguration.Value.KillAfterValidation))
                throw new CerberusException(ErrorCode.InvalidOtp, "invalid otp");

            if (profile == null) throw new CerberusException(ErrorCode.NotFound);

            return new GetProfileStatusResponse()
            {
                Status = profile.Status
            };
        }

        [HttpGet]
        [Route("~/v{version:apiVersion}/servicesWithOtp/profiles/{uniqueIdentifier}/kycSejamStatus")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<GetProfileStatusResponse> KycGetProfileStatusAsync([FromRoute, Required]string uniqueIdentifier, [FromQuery, Required, MaxLength(5)] string otp)
        {
            if (string.IsNullOrWhiteSpace(otp) || string.IsNullOrEmpty(otp))
                throw new CerberusException(ErrorCode.BadRequest, "otp can not be null");

            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await _profileApiService.GetByUniqueIdentifierAsync(uniqueIdentifier);

            if (!await _otpService.IsValidAsync(
                ConventionalHelper.CreateOtpKey(profile?.Mobile.ToString(), uniqueIdentifier, OtpType.Kyc),
                otp,
                _apiConfiguration.Value.KillAfterValidation))
                throw new CerberusException(ErrorCode.InvalidOtp, "invalid otp");

            if (profile == null) throw new CerberusException(ErrorCode.NotFound);

            return new GetProfileStatusResponse()
            {
                Status = profile.Status
            };
        }


        [HttpGet]
        [Route("{uniqueIdentifier}/isSejami")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 15, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<GetProfileIsIsSejamiResponse> GetProfileIsSejamiAsync([Required]string uniqueIdentifier)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var result = await _profileService.GetByUniqueIdentifierFromCacheAsync(uniqueIdentifier);
            if (result == null)
                return new GetProfileIsIsSejamiResponse();
            if (result.Type == ProfileOwnerType.IranianLegalPerson)
            {
                var whiteList = _legalPersonsWhiteListConfiguration.UniqueIdentifiers;

                if (result.Status == ProfileStatus.TraceCode && whiteList.Contains(result.UniqueIdentifier))
                {
                    return new GetProfileIsIsSejamiResponse
                    {
                        IsSejami = true
                    };
                }
            }

            return new GetProfileIsIsSejamiResponse()
            {
                IsSejami = result.Status == ProfileStatus.Sejami
            };
        }


        [HttpGet]
        [Route("{uniqueIdentifier}/isConfirmed")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<GetProfileIsIsSejamiResponse> GetProfileSejamiAsync([Required]string uniqueIdentifier)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var result = await _profileApiService.GetByUniqueIdentifierAsync(uniqueIdentifier);
            if (result == null)
                return new GetProfileIsIsSejamiResponse();

            if (result.Type == ProfileOwnerType.IranianLegalPerson && result.Status == ProfileStatus.TraceCode)
            {
                return new GetProfileIsIsSejamiResponse
                {
                    IsSejami = true
                };
            }
            return new GetProfileIsIsSejamiResponse()
            {
                IsSejami = result.Status == ProfileStatus.Sejami
            };
        }


        [HttpGet]
        [Route("~/v{version:apiVersion}/servicesWithOtp/profiles/{uniqueIdentifier}")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<ProfileViewModel> GetByKycOtpAsync([FromRoute][Required] string uniqueIdentifier, [FromQuery][Required, MaxLength(5)] string otp)
        {
            if (string.IsNullOrWhiteSpace(otp) || string.IsNullOrEmpty(otp))
                throw new CerberusException(ErrorCode.BadRequest, "otp can not be null");

            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await _profileApiService.GetForProfilingByUniqueIdentifierAsync(uniqueIdentifier, true);
            if (profile == null || profile.Status < _apiConfiguration.Value.CompleteProfileStatus)
                throw new CerberusException(ErrorCode.NotFound);

            if (!await _otpService.IsValidAsync(
                ConventionalHelper.CreateOtpKey(profile.Mobile.ToString(), uniqueIdentifier, OtpType.Kyc),
                otp,
                _apiConfiguration.Value.KillAfterValidation))
                throw new CerberusException(ErrorCode.InvalidOtp, "invalid otp");

            if (profile.Type == ProfileOwnerType.IranianLegalPerson && profile.Status >= ProfileStatus.TraceCode)
                return profile;

            if (profile.Status != ProfileStatus.Sejami)
                throw new CerberusException(ErrorCode.InCompleteRegistrations);

            if (profile.Type == ProfileOwnerType.IranianPrivatePerson && profile.PrivatePerson.ShNumber == "0") profile.PrivatePerson.ShNumber = profile.UniqueIdentifier;

            return profile;
        }


        [HttpGet]
        [Route("~/v{version:apiVersion}/servicesWithPermanentOtp/profiles/{uniqueIdentifier}")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 5, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<ProfileViewModel> GetByKycPermanentOtpAsync([FromRoute][Required]string uniqueIdentifier, [FromQuery, MaxLength(5)] string otp)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await _profileApiService.GetForProfilingByUniqueIdentifierAsync(uniqueIdentifier, true);
            if (profile == null || profile.Status < _apiConfiguration.Value.CompleteProfileStatus)
                throw new CerberusException(ErrorCode.NotFound);

            var permanentOtp = await _permanentOtpService.GetAsync(CurrentService.ServiceId, uniqueIdentifier, OtpTypes.PermanentKyc);
            if (permanentOtp == null || permanentOtp.ExpirationDate <= DateTime.Now)
            {
                if (string.IsNullOrWhiteSpace(otp) || string.IsNullOrEmpty(otp))
                    throw new CerberusException(ErrorCode.BadRequest, "otp can not be null");

                if (!await _otpService.IsValidAsync(
                    ConventionalHelper.CreateOtpKey(profile.Mobile.ToString(), uniqueIdentifier, OtpType.PermanentKyc),
                    otp,
                    _apiConfiguration.Value.KillAfterValidation))
                    throw new CerberusException(ErrorCode.InvalidOtp, "invalid otp");


                await _permanentOtpService.CreateAsync(new PermanentOtp
                {
                    Type = OtpTypes.PermanentKyc,
                    ServiceId = CurrentService.ServiceId,
                    UniqueIdentifier = uniqueIdentifier,
                    Otp = otp,
                    ExpirationDate = DateTime.Now.AddDays(_permanentOtpConfigurations.Value.ExpirationDays)
                });
            }

            if (profile.Type == ProfileOwnerType.IranianLegalPerson && profile.Status >= ProfileStatus.TraceCode)
                return profile;

            if (profile.Status != ProfileStatus.Sejami)
                throw new CerberusException(ErrorCode.InCompleteRegistrations);

            if (profile.Type == ProfileOwnerType.IranianPrivatePerson && profile.PrivatePerson.ShNumber == "0") profile.PrivatePerson.ShNumber = profile.UniqueIdentifier;

            return profile;
        }




        [HttpGet]
        [Route("~/v{version:apiVersion}/servicesWithOtp/internalService/profiles/{uniqueIdentifier}")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<ProfileViewModel> GetByInternalKycOtpAsync([FromRoute, Required]string uniqueIdentifier, [FromQuery, Required, MaxLength(5)] string otp)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);
            if (string.IsNullOrWhiteSpace(otp) || string.IsNullOrEmpty(otp))
                throw new CerberusException(ErrorCode.BadRequest, "otp can not be null");

            var profile = await _profileApiService.GetForProfilingByUniqueIdentifierAsync(uniqueIdentifier, true);
            if (profile == null || profile.Status < _apiConfiguration.Value.CompleteProfileStatus)
                throw new CerberusException(ErrorCode.NotFound);

            if (!await _otpService.IsValidAsync(
                ConventionalHelper.CreateOtpKey(_apiConfiguration.Value.InternalServiceOtpMobile, uniqueIdentifier, OtpType.InternalService),
                otp,
                _apiConfiguration.Value.KillAfterValidation))
                throw new CerberusException(ErrorCode.InvalidOtp, "invalid otp");

            return profile;
        }

        [HttpGet]
        [Route("~/v{version:apiVersion}/internalService/compactProfiles/{uniqueIdentifier}")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<CompactProfile> GetCompactProfileAsync([FromRoute, Required]string uniqueIdentifier)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await _profileApiService.GetByUniqueIdentifierAsync(uniqueIdentifier, true);
            if (profile == null || !profile.Status.IsConfirmed() || !profile.Accounts.Any() || profile.Accounts.Any(x => x.IsConfirmed != true))
                throw new CerberusException(ErrorCode.NotFound);

            return profile.ToCompactViewModel();
        }

        [HttpGet]
        [Route("~/v{version:apiVersion}/authenticator/profiles/{uniqueIdentifier}")]
        [RateLimitFilterFactory(Order = 2, Limit = 5000, PeriodInSec = 120, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<GetProfileByAuthenticatorOtpResponse> GetByAuthenticatorOtpAsync(
            [FromRoute, Required]string uniqueIdentifier,
            [FromQuery, Required, MaxLength(5)]string otp,
            [FromQuery, Required, MaxLength(10)]string traceCode)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);
            var profile = await _profileApiService.GetAuthenticatorProfileByUniqueIdentifierAsync(uniqueIdentifier);
            if (profile == null)
                throw new CerberusException(ErrorCode.NotFound);

            if (string.IsNullOrWhiteSpace(otp) || string.IsNullOrEmpty(otp))
                throw new CerberusException(ErrorCode.BadRequest, "otp can not be null");

            if (!await _otpService.IsValidAsync(
                ConventionalHelper.CreateOtpKey(profile.Mobile.ToString(), uniqueIdentifier, OtpType.Authenticator),
                otp,
                _apiConfiguration.Value.KillAfterValidation))
                throw new CerberusException(ErrorCode.InvalidOtp, "invalid otp");

            if (!profile.Status.IsConfirmed() || string.IsNullOrWhiteSpace(profile.TraceCode))
                throw new CerberusException(ErrorCode.InCompleteRegistrations);

            if (profile.Status == ProfileStatus.Sejami)
                throw new CerberusException(ErrorCode.Conflicts, "profile already has sejami status");


            if (!profile.TraceCode.Equals(traceCode))
                throw new CerberusException(ErrorCode.InvalidTraceCode);

            var list = _authenticatorConfiguration.LongsTermIssuerOtp;

            var issuerOtp = await _otpService.GetAsync(
                ConventionalHelper.CreateIssuerOtpKey(profile.Mobile.ToString(),$"{CurrentService.ServiceId}:{profile.UniqueIdentifier}", OtpType.Authenticator),
                list.Contains(CurrentService.ServiceId) ? _apiConfiguration.Value.LongsTermIssuerOtpTtl : _apiConfiguration.Value.IssuerOtpTtl,
                Guid.NewGuid().ToString());

            var response = new GetProfileByAuthenticatorOtpResponse
            {
                Profile = profile,
                AuthenticatorOtp = issuerOtp
            };
            if (profile.Status != ProfileStatus.SemiSejami && profile.Status != ProfileStatus.Suspend) return response;

            var lastHistory = await _profileHistoryService.GetTheLastHistoryAsync(profile.Id);

            response.IssuerReference = lastHistory?.ReferenceId;

            return response;
        }

        private async Task<ProfileViewModel> GetProfileAsync(string uniqueIdentifier, bool includeAllRelation = false)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await _profileApiService.GetByUniqueIdentifierAsync(uniqueIdentifier, includeAllRelation);
            if (profile == null || profile.Status < _apiConfiguration.Value.CompleteProfileStatus)
                throw new CerberusException(ErrorCode.NotFound);

            return profile;
        }


        [HttpGet]
        [Route("~/v{version:apiVersion}/entryNodeServices/profiles/{uniqueIdentifier}")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 60, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 3, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<CheckExistResponse> GetProfileExistanceAsync([FromRoute, Required] string uniqueIdentifier,
            [FromQuery, Required] string mobile, [FromQuery, Required, MaxLength(5)] string otp)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            if (!ValidationHelper.IsMsisdnValid(ref mobile, out var carrier) || carrier == null)
                throw new CerberusException(ErrorCode.BadRequest, "invalid msisdn");

            if (string.IsNullOrWhiteSpace(otp) || string.IsNullOrEmpty(otp))
                throw new CerberusException(ErrorCode.BadRequest, "otp can not be null");

            if (!await _otpService.IsValidAsync(
                ConventionalHelper.CreateEntryNodeOtpKey(mobile),
                otp,
                _apiConfiguration.Value.KillAfterValidation))
                throw new CerberusException(ErrorCode.InvalidOtp, "invalid otp");

            await CheckExistMobile(mobile.ToLong());

            var profile = await _profileService.GetByUniqueIdentifierFromCacheAsync(uniqueIdentifier);
            var response = new CheckExistResponse();
            if (profile != null && profile.Status >= ProfileStatus.SuccessPayment)
            {
                response.Existence = true;
                return response;
            }

            var issuerOtp = await _otpService.GetAsync(
                ConventionalHelper.CreateIssuerOtpKey(mobile, uniqueIdentifier, OtpType.EntryNode),
                _apiConfiguration.Value.IssuerOtpTtl,
                Guid.NewGuid().ToString("N"));
            response.Existence = false;
            response.IssuerOtp = issuerOtp;
            return response;
        }


        [HttpGet]
        [Route("{uniqueIdentifier}/underEighteenMobile")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId", VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 5, VaryByParamsPeriodInSec = 1 * 60 * 10)]
        public async Task<MobileResponseViewModel> GetMobileForUnderEighteenAsync([Required]string uniqueIdentifier)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await _profileService.GetByUniqueIdentifierFromCacheAsync(uniqueIdentifier);

            if (profile == null || profile.Status != ProfileStatus.Sejami)
                throw new CerberusException(ErrorCode.NotFound);


            var privatePerson = await _privatePersonService.GetByProfileIdWithNoLockAsync(profile.Id);
            if(!Tools.IsUnder18Years(privatePerson?.BirthDate))
                throw new CerberusException(ErrorCode.NotFound);

            return new MobileResponseViewModel
            {
                Mobile =  profile.Mobile.ToString()
            };
        }


        private async Task CheckExistMobile(long mobile)
        {
            var existMobile = await _profileService.RegisterMobileCount(mobile, ProfileStatus.SuccessPayment, _apiConfiguration.Value.RegisterMobileCount);
            if (existMobile)
                throw new CerberusException(ErrorCode.Conflicts, "the mobile number already exists in sejam DB");
        }



    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Cerberus.Api.Filters;
using Cerberus.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [Route("v{version:apiVersion}/files")]
    [ApiController]
    //[AuthorizeUserFilterFactory(Order = 1)]
    [ProfilingLogRequestFilterFactory(Order = 3)]

    public class FileController : ControllerBase
    {
       
        private readonly FileStorageConfiguration _fileStorageConfiguration;

        public FileController(IOptions<FileStorageConfiguration> fileStorageConfiguration)
        {
           
            _fileStorageConfiguration = fileStorageConfiguration.Value;
        }


    
        [HttpGet("serveFile/{filename}")]
        public async Task<ActionResult> GetFile([FromRoute, Required, MaxLength(300)] string filename)
        {
          
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", _fileStorageConfiguration.FileServiceToken);

            var response = await httpClient.GetAsync(string.Format(_fileStorageConfiguration.FileServiceDownloadUrl, filename));
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var fileStreamContent = await response.Content.ReadAsStreamAsync();
                return File(fileStreamContent, response.Content.Headers.ContentType.ToString());
            }

            return StatusCode((int)response.StatusCode);
        }
    }
}
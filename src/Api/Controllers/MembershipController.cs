﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Filters;
using Cerberus.Api.Model;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Gatekeeper.AspModules.ActionFilter;
using GateKeeper.Auth.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [Route("v{version:apiVersion}/membership")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class MembershipController : BaseController
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IPaymentService _paymentService;
        private readonly IThirdPartyServiceService _thirdPartyServiceService;
        private readonly IUserService _userService;

        public MembershipController(
            ITaskRepository taskRepository,
            IPaymentService paymentService, IThirdPartyServiceService thirdPartyServiceService, IUserService userService)
        {
            _taskRepository = taskRepository;
            _paymentService = paymentService;
            _thirdPartyServiceService = thirdPartyServiceService;
            _userService = userService;
        }

        [HttpPost]
        [Route("")]
        [RateLimitFilterFactory(Order = 2, Limit = 2, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<CreateAsyncTaskResponse> CreateStoreMembershipListAsync(CreateSyncMembershipTaskRequest request)
        {
            if (!request.Members.Any())
                throw new CerberusException(ErrorCode.BadRequest, "membership list can not be null");

            if (string.IsNullOrEmpty(request.CallbackUrl))
                throw new CerberusException(ErrorCode.BadRequest, "invalid callback url");

            var task = new ScheduledTask()
            {
                DueTime = DateTime.Now,
                GroupId = CerberusTasksIdentifierHelper.SyncMembershipProcessing.Key,
                Params = JsonConvert.SerializeObject(new SyncMembershipTaskParams()
                {
                    Members = request.Members,
                    CallbackUrl = request.CallbackUrl,
                    ServiceId = CurrentService.ServiceId,
                }),
                RefId = CurrentService.ServiceId,
                Type = CerberusTasksIdentifierHelper.SyncMembershipProcessing.Value
            };

            task = await _taskRepository.AddAsync(task);

            //set 202 http status code
            ResponseHttpStatusCode = HttpStatusCode.Accepted;

            return new CreateAsyncTaskResponse()
            {
                Reference = task.Id.ToString()
            };
        }

        [HttpGet]
        [Route("subscribersCount")]
        [RateLimitFilterFactory(Order = 2, Limit = 10, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<GetSubscriberCountResponse> GetSubscribersCountAsync(DateTime? from, DateTime? to)
        {
            int count = await _paymentService.GetCountByDiscountIssuerAsync(CurrentService.ServiceId, from, to);

            return new GetSubscriberCountResponse()
            {
                Count = count
            };
        }

        [HttpGet]
        [Route("{username}/subscribersCount")]
        [RateLimitFilterFactory(Order = 2, Limit = 20, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<GetSubscriberCountResponse> GetSubscribersCountAsync([Required, MaxLength(50)]string username, DateTime? from, DateTime? to) 
        {
            var user = await _userService.GetAsync(username);
            if (user == null)
                throw new CerberusException(ErrorCode.BadRequest, "invalid username");


            var service = await _thirdPartyServiceService.GetAsync(user.RefId);
            if (service == null)
                throw new CerberusException(ErrorCode.ThirdPartyServiceNotFound, $"service with title '{username}' not found");


            int count = await _paymentService.GetCountByDiscountIssuerAsync(service.Id, from, to);

            return new GetSubscriberCountResponse()
            {
                Count = count
            };
        }


 }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Configuration;
using Cerberus.Api.Filters;
using Cerberus.Api.Model;
using Cerberus.Domain;
using Cerberus.Domain.CustomValidation;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Service;
using Cerberus.Utility;
using Gatekeeper.AspModules.ActionFilter;
using Gatekeeper.Core.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [Route("v{version:apiVersion}/profiles")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class ProfileUpdatesController : BaseController
    {
        private readonly Domain.Interface.ApiServices.IProfileApiService _profileViewModelService;
        private readonly IProfileService _profileService;
        private readonly IOtpService _otpService;
        private readonly IPrivatePersonService _privatePersonService;
        private readonly IFileService _fileService;
        private readonly IOptions<FileStorageConfiguration> _fileStorageConfiguration;
        private readonly IProfileHistoryService _profileHistoryService;
        private readonly IMapper _mapper;
        private readonly IMessagingService _messagingService;
        private readonly MessagingConfiguration _messagingConfiguration;
        private readonly IIssuanceStockCodeRepository _issuanceStockCodeRepository;
        private readonly IBankingAccountService _bankingAccountService;
        private readonly AuthenticatorConfiguration _authenticatorConfiguration;
        private readonly IMsisdnService _msisdnService;
        private readonly IAgentService _agentService;

        public ProfileUpdatesController(
            Domain.Interface.ApiServices.IProfileApiService profileViewModelService,
            IProfileService profileService,
            IOtpService otpService,
            IPrivatePersonService privatePersonService,
            IFileService fileService,
            IOptions<FileStorageConfiguration> fileStorageConfiguration,
            IProfileHistoryService profileHistoryService,
            IMessagingService messagingService,
            IOptions<MessagingConfiguration> messagingConfiguration,
            IMapper mapper,
            IIssuanceStockCodeRepository issuanceStockCodeRepository, IBankingAccountService bankingAccountService,
            IOptions<AuthenticatorConfiguration> authenticatorConfiguration, IMsisdnService msisdnService, IAgentService agentService)
        {
            _messagingService = messagingService;
            _messagingConfiguration = messagingConfiguration.Value;
            _profileViewModelService = profileViewModelService;
            _profileService = profileService;
            _otpService = otpService;
            _privatePersonService = privatePersonService;
            _fileService = fileService;
            _fileStorageConfiguration = fileStorageConfiguration;
            _profileHistoryService = profileHistoryService;
            _mapper = mapper;
            _issuanceStockCodeRepository = issuanceStockCodeRepository;
            _bankingAccountService = bankingAccountService;
            _msisdnService = msisdnService;
            _agentService = agentService;
            _authenticatorConfiguration = authenticatorConfiguration.Value;
        }

        [HttpPatch]
        [Route("~/v{version:apiVersion}/authenticator/profiles/{uniqueIdentifier}/status")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId",
            VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 1, VaryByParamsPeriodInSec = 10)]
        public async Task<object> UpdateProfileToSejamiAsync(
            [FromRoute, Required] string uniqueIdentifier,
            [FromQuery, Required, MaxLength(40)] string issuerOtp,
            [FromQuery, Required, MaxLength(10)] string traceCode,
            [FromBody, Required] IssuerPatchProfileRequest request)
        {

            return await UpdateProfileToSejami(uniqueIdentifier, issuerOtp, traceCode, request);
        }


        [HttpPost]
        [Route("~/v{version:apiVersion}/authenticator/profiles/{uniqueIdentifier}/status")]
        [RateLimitFilterFactory(Order = 2, Limit = 10000, PeriodInSec = 300, UserIdentifier = "ServiceId",
            VaryByParams = "uniqueIdentifier", VaryByParamsLimit = 1, VaryByParamsPeriodInSec = 10)]
        public async Task<object> UpdateProfileToSejamiPostAsync(
            [FromRoute, Required] string uniqueIdentifier,
            [FromQuery, Required, MaxLength(40)] string issuerOtp,
            [FromQuery, Required, MaxLength(10)] string traceCode,
            [FromBody, Required] IssuerPatchProfileRequest request)
        {

            return await UpdateProfileToSejami(uniqueIdentifier, issuerOtp, traceCode, request);
        }









        [HttpPost]
        [Route("~/v{version:apiVersion}/authenticator/profiles/postIds")]
        [RateLimitFilterFactory(Order = 2, Limit = 600, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<object> UpdatePostIdAsync([FromBody, Required] IEnumerable<UpdatePostIdsRequest> request)
        {
            if (request == null)
                throw new CerberusException(ErrorCode.BadRequest);


            var updatePostIdsRequests = request.ToList();
            if (updatePostIdsRequests.Count > 100)
                throw new CerberusException(ErrorCode.BadRequest);

            foreach (var item in updatePostIdsRequests)
            {
                var history = await _profileHistoryService.GetAsync(item.ReferenceNumber);
                if (history != null)
                {
                    history.PostId = item.PostId;
                    await _profileHistoryService.UpdateAsync(history);
                }
            }

            return NoContent();
        }

        [HttpGet]
        [Route("~/v{version:apiVersion}/authenticator/profiles/stats/{referenceId}")]
        [RateLimitFilterFactory(Order = 2, Limit = 1000, PeriodInSec = 3000, UserIdentifier = "ServiceId",
            VaryByParams = "referenceId", VaryByParamsLimit = 15, VaryByParamsPeriodInSec = 1 * 60 * 1)]
        public async Task<IEnumerable<ProfileHistoryViewModel>> GetByAuthenticatorReferenceIdAsync(
            [FromRoute, Required, MaxLength(50)] string referenceId,
            [FromQuery, Required] DateTime from,
            [FromQuery, Required] DateTime to)
        {

            var result = await _profileHistoryService.GetByReferenceIdAsync(referenceId, from, to);

            return _mapper.Map<IEnumerable<ProfileHistory>, IEnumerable<ProfileHistoryViewModel>>(result.Where(x =>
                x.ChangedTo == ProfileStatus.Sejami));
        }

        private byte[] GetFileFromBase64Format(string base64, string imageName, out string mimeType, params string[] validFormat)
        {
            try
            {
                var splits = base64.Split(',');
                if (splits.Length < 2)
                    throw new CerberusException(ErrorCode.BadRequest, "invalid file format");

                var postedFileBytes = Convert.FromBase64String(splits[1]);
                var fileExtension = "." + splits[0].Split('/', ';')[1];
                if (validFormat.FirstOrDefault(x => x.Equals(fileExtension)) == null)
                    throw new CerberusException(ErrorCode.BadRequest,
                        $"invalid file format(valid formats:{string.Join(",", validFormat)})");

                mimeType = splits[0].Split(':', ';')[1];

                return Convert.FromBase64String(splits[1]);
            }
            catch(Exception c)
            {
                throw new CerberusException(ErrorCode.BadRequest, $"invalid {imageName}");
            }
        }




        private async Task<object> UpdateProfileToSejami(string uniqueIdentifier, string issuerOtp, string traceCode, IssuerPatchProfileRequest request)
        {
            UniqueIdentifierValidations.CheckValidUniqueIdentifier(uniqueIdentifier);

            var profile = await _profileService.GetByUniqueIdentifierAsync(uniqueIdentifier);
            if (profile == null)
                throw new CerberusException(ErrorCode.NotFound);

            if (!profile.Status.IsConfirmed())
            {
                profile = await _profileService.GetByUniqueIdentifierWithNoCacheAsync(uniqueIdentifier);

                if (!profile.Status.IsConfirmed())
                    throw new CerberusException(ErrorCode.InCompleteRegistrations);
            }

            if (profile.Status == ProfileStatus.Sejami)
                throw new CerberusException(ErrorCode.Conflicts, "profile already has sejami status");

            if ((CurrentService.ServiceId != _authenticatorConfiguration.PishkhanServiceId &&
                 !_authenticatorConfiguration.LongsTermIssuerOtp.Contains(CurrentService.ServiceId)) ||
                !_authenticatorConfiguration.IgnoreValidateOtp )
            {
                if (!await _otpService.IsValidAsync(
                    ConventionalHelper.CreateIssuerOtpKey(profile.Mobile.ToString(), $"{CurrentService.ServiceId}:{uniqueIdentifier}",
                        OtpType.Authenticator),
                    issuerOtp,
                    false))
                    throw new CerberusException(ErrorCode.Forbidden, "invalid issuerOtp");
            }


            if (!profile.TraceCode.Equals(traceCode))
                throw new CerberusException(ErrorCode.InvalidTraceCode, "invalid trace code");

            if (profile.Status == ProfileStatus.Dead)
                throw new CerberusException(ErrorCode.Conflicts, "profile has dead status");


            if (profile.Type == ProfileOwnerType.IranianPrivatePerson)
            {
                var privatePerson = await _privatePersonService.GetByProfileIdAsync(profile.Id);
                if (privatePerson == null)
                    throw new CerberusException(ErrorCode.NotFound, "private person, which related by selected profile not found");
                
                if (privatePerson.IsConfirmed != true)
                    throw new CerberusException(ErrorCode.InCompleteRegistrations, "the privatePerson data is not confirmed");

                var profileAgent = await _agentService.GetByProfileIdWithNoLockAsync(profile.Id);

                if (profileAgent != null && profileAgent.IsConfirmed != true)
                    throw new CerberusException(ErrorCode.InCompleteRegistrations, "the profile agent not Confirmed");

              

                if (profile.Type == ProfileOwnerType.IranianPrivatePerson && Tools.IsUnder18Years(privatePerson.BirthDate) && profileAgent == null)
                    throw new CerberusException(ErrorCode.InCompleteRegistrations, "the profile agent not specified");


                var bankingAccounts = await _bankingAccountService.GetListAsync(profile.Id);

                var accounts = bankingAccounts.ToList();
                if (!accounts.Any() || accounts.Any(x => x.IsConfirmed != true))
                    throw new CerberusException(ErrorCode.InCompleteRegistrations, "the bankingAccount data is not confirmed");
              
                if (!Tools.IsUnder18Years(privatePerson.BirthDate) && profileAgent == null)
                {
                    var msisdn = await _msisdnService.GetByProfileIdAsync(profile.Id);

                    if (msisdn != null && msisdn.IsConfirmed != true && msisdn.Mobile == profile.Mobile)
                        throw new CerberusException(ErrorCode.InCompleteRegistrations, "the mobile is not confirmed");
                }

                //save profile image
                var imageFileBytes = GetFileFromBase64Format(request.PrivatePersonImage, "person image", out var mimeTypeImage, ".jpeg", ".jpg", ".png", ".pdf");
                if (imageFileBytes.Length > 2000000)
                {
                    throw new CerberusException(ErrorCode.ImageTooLarge, "Person Image too large, you can upload files up to 2 MB");
                }
                var signatureFileBytes = GetFileFromBase64Format(request.PrivatePersonSignature, "signature image", out var mimeTypeSignature, ".jpeg", ".jpg", ".png", ".pdf");
                if (imageFileBytes.Length > 2000000)
                {
                    throw new CerberusException(ErrorCode.ImageTooLarge, "Signature Image too large, you can upload files up to 2 MB");
                }

                var cdnFileName = $"{Guid.NewGuid():N}.{mimeTypeImage.Split('/')[1]}";

                var privatePersonImage = new File()
                {
                    FileName = cdnFileName,
                    MimeType = mimeTypeImage,
                    Type = FileType.Image,
                    Title = cdnFileName

                };
                try
                {

                    await _fileService.CreateAsync(privatePersonImage, imageFileBytes,
                        _fileStorageConfiguration.Value.PrivatePersonImagePath);
                }

                catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnsupportedMediaType)
                {
                    throw new CerberusException(ErrorCode.BadRequest, "Person Image Format Not Supported");
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.ImageTooLarge)
                {
                    throw new CerberusException(ErrorCode.BadRequest, $" person file  size is not valid and too large. fileSize:{imageFileBytes.Length}");
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.ImageTooSmall)
                {
                    throw new CerberusException(ErrorCode.BadRequest, $"Person file size is too small. fileSize:{imageFileBytes.Length}");
                }

                catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnknownServerError)
                {
                    throw new CerberusException(ErrorCode.UnknownServerError, "Unable to Save Person Image");
                }
                catch (CerberusException)
                {
                    throw new CerberusException(ErrorCode.UnknownServerError, "Unable Save Person Image");
                }

                //save signature
                var cdnFileSignatureName = $"{Guid.NewGuid():N}.{mimeTypeSignature.Split('/')[1]}";
                var privatePersonSignature = new File()
                {
                    FileName = cdnFileSignatureName,
                    MimeType = mimeTypeSignature,
                    Type = FileType.Image,
                    Title = cdnFileSignatureName,
                };
                try
                {
                    await _fileService.CreateAsync(privatePersonSignature, signatureFileBytes,
                        _fileStorageConfiguration.Value.PrivatePersonSignaturePath);
                }

                catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnsupportedMediaType)
                {
                    throw new CerberusException(ErrorCode.BadRequest, "Signature Image Format Not Supported");
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.ImageTooLarge)
                {
                    throw new CerberusException(ErrorCode.BadRequest, $"signature file  size is not valid and too large. fileSize:{signatureFileBytes.Length}");
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.ImageTooSmall)
                {
                    throw new CerberusException(ErrorCode.BadRequest, $"signature file size is too small. fileSize:{signatureFileBytes.Length}");
                }

                catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnknownServerError)
                {
                    throw new CerberusException(ErrorCode.UnknownServerError, "Unable to Save Signature Image");
                }
                catch (CerberusException)
                {
                    throw new CerberusException(ErrorCode.UnknownServerError, "Unable Save Signature Image");
                }

                privatePerson.ImageId = privatePersonImage.Id;
                privatePerson.SignatureFileId = privatePersonSignature.Id;



                //update private person images
                await _privatePersonService.UpdateAsync(privatePerson);

                //update profile status


                var profileHistory = new ProfileHistory()
                {
                    ProfileId = profile.Id,
                    ReferenceId = request.IssuerReference,
                    ImageId = privatePersonImage.Id,
                    SignatureFileId = privatePersonSignature.Id,
                    ServiceId = CurrentService.ServiceId,
                    ChangeFrom = profile.Status,
                };
                await _profileHistoryService.CreateAsync(profileHistory, ProfileStatus.Sejami);



                //queue for issuance legacy code
                await _issuanceStockCodeRepository.CreateAsync(new IssuanceStockCode()
                {
                    ProfileId = profile.Id,
                    Mobile = profile.Mobile,
                    UniqueIdentifier = profile.UniqueIdentifier
                });

                profile.Status = ProfileStatus.Sejami;
                profile.StatusReasonType = null;
                await _profileService.UpdateStatusAndReasonAsync(profile);

                await _otpService.KillAsync(ConventionalHelper.CreateIssuerOtpKey(profile.Mobile.ToString(), $"{CurrentService.ServiceId}:{uniqueIdentifier}", OtpType.Authenticator));

                await _messagingService.SendSmsAsync(profile.Id, profile.Mobile.ToString(), null,
                    string.Format(_messagingConfiguration.OnSuccessAuthenticateMessage, $"{privatePerson.FirstName} {privatePerson.LastName}", Environment.NewLine)
                );

                await _messagingService.SendSmsAsync(profile.Id, profile.Mobile.ToString(), null,
                    _messagingConfiguration.AdvertiseDdnMessage, DateTime.Now.AddHours(1));

                return new { ReferenceNumber = profileHistory.Id };
            }

            return NoContent();
        }
    }
}
﻿namespace Cerberus.Web.Enum
{
    public enum PaymentResultCode
    {
        Failed = 0,
        Verify = 1,
        Success = 2,
        Duplicate = 3,
        Canceled = 4
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Utility;
using Cerberus.Web.Configuration;
using Cerberus.Web.Enum;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using StackExchange.Redis;

namespace Cerberus.Web.Filters
{
    public class ValidateCaptchaAttribute : ActionFilterAttribute
    {
        private readonly IDatabase _readDatabase;
        private readonly CaptchaConfiguration _captchaConfiguration;
        public const string ReCaptchaModelErrorKey = "Captcha";
        public ValidateCaptchaAttribute(IDatabase readDatabase, IOptions<CaptchaConfiguration> captchaConfiguration)
        {
            _readDatabase = readDatabase;
            _captchaConfiguration = captchaConfiguration.Value;
        }
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var cap = context.HttpContext.Request.Form["CaptchaCode"].ToString();
            if (string.IsNullOrEmpty(cap))
            {
                AddModelError(context, "لطفا کد امنیتی را وارد نمایید");
            }
            else
            {
                context.HttpContext.Request.Cookies.TryGetValue("_ck", out var clientKey);
                if (!await ValidateCaptchaCode(cap, clientKey))
                {
                    AddModelError(context, "کد امنیتی وارد شده اشتباه است.");

                }

            }
            await next.Invoke();

        }
        public async Task<bool> ValidateCaptchaCode(string userInputCaptcha, string clientKey)
        {
            var serverKey = HashHelper.GenerateMd5String(_captchaConfiguration.KeySalt + clientKey);
            var captchaCode = await _readDatabase.StringGetAsync($"{_captchaConfiguration.CaptchaPrefix}:{serverKey}");
            await _readDatabase.KeyDeleteAsync($"{_captchaConfiguration.CaptchaPrefix}:{serverKey}");
            return userInputCaptcha == captchaCode.ToString();
        }
        private static void AddModelError(ActionExecutingContext context, string error)
        {
            context.ModelState.AddModelError(ReCaptchaModelErrorKey, error.ToString());
        }
    }
}

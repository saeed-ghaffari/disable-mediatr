﻿using System;
using Cerberus.Web.Enum;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace Cerberus.Web.Filters
{
    public class AuthorizeUserFilterFactory : Attribute, IFilterFactory
    {
        public int Scope { get; set; }

        public IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
        {
            var filter = serviceProvider.GetService<AuthorizeUserAttribute>();
            if(Scope!=0)
                filter.Scope =(AvailableScope) Scope;
            return filter;

        }

        public bool IsReusable => false;
    }
}
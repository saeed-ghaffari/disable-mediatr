﻿using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Web.Configuration;
using Cerberus.Web.Enum;
using Cerberus.Web.Model;
using Cerberus.Web.WebLogic;
using Gatekeeper.Core;
using Gatekeeper.Core.Interface;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Gatekeeper.AspModules;

namespace Cerberus.Web.Filters
{
    public class AuthorizeUserAttribute : IAsyncActionFilter
    {

        private readonly WebConfiguration _apiConfiguration;
        private readonly IJwtTokenService<UserSessionInfo> _jwtTokenService;
        private readonly RequestLogConfiguration _requestLogConfiguration;

        public AvailableScope? Scope { get; set; }

        public AuthorizeUserAttribute(
            IOptions<WebConfiguration> apiConfiguration,
            IJwtTokenService<UserSessionInfo> sessionService,
            IOptions<RequestLogConfiguration> requestLogConfiguration)
        {
            _jwtTokenService = sessionService;
            _requestLogConfiguration = requestLogConfiguration.Value;
            _apiConfiguration = apiConfiguration.Value;
        }


        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!context.HttpContext.Request.Cookies.TryGetValue(ConventionalHelper.SessionCookieName, out var token))
                throw new CerberusException(ErrorCode.Unauthorized);

            try
            {
                //validate token and retrieve payload
                var tokenPayload = await _jwtTokenService.RetrievePayloadFromTokenAsync(token, true, _apiConfiguration.SessionTtl);

                if (Scope.HasValue && (tokenPayload.AvailableScope & Scope) != Scope)
                    throw new CerberusException(ErrorCode.Forbidden);

                //set payload in request items
                context.HttpContext.Items.Add("CurrentUser", tokenPayload);
                context.HttpContext.Items.Add("ProfileId", tokenPayload.ProfileId);
                context.HttpContext.Items.Add("UserId", tokenPayload.ProfileId);

                //invoke next step
                await next.Invoke();
            }
            catch (GateKeeperException)
            {
                throw new CerberusException(ErrorCode.Unauthorized);
            }
        }
    }
}
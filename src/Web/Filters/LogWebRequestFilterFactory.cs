﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace Cerberus.Web.Filters
{
    public class LogWebRequestFilterFactory : Attribute, IFilterFactory
    {
        public int Order { get; set; }
        public IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
        {
            var filter = serviceProvider.GetService<LogWebRequestAttribute>();
            filter.Order = Order;
            return filter;

        }

        public bool IsReusable => false;
    }
}
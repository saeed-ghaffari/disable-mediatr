﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Aspina;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Model.Log;
using Cerberus.Web.Configuration;
using Cerberus.Web.Model;
using Cerberus.Web.WebLogic;
using Gatekeeper.Core.Interface;

namespace Cerberus.Web.Filters
{
    public class LogWebRequestAttribute : IAsyncActionFilter, IOrderedFilter
    {
        private readonly IBackgroundTaskQueue _queue;
        private readonly IWebRequestLogRepository _webRequestLogRepository;
        private readonly IJwtTokenService<UserSessionInfo> _jwtTokenService;
        private readonly WebConfiguration _apiConfiguration;

        public int Order { get; set; }

        public LogWebRequestAttribute(
            IBackgroundTaskQueue queue,
          IWebRequestLogRepository webRequestLogRepository, IJwtTokenService<UserSessionInfo> jwtTokenService, IOptions<WebConfiguration> apiConfiguration)
        {
            _queue = queue;
            _webRequestLogRepository = webRequestLogRepository;
            _jwtTokenService = jwtTokenService;
            _apiConfiguration = apiConfiguration.Value;
        }


        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!_apiConfiguration.EnableRequestLog)
            {
                await next.Invoke();
                return;
            }
            context.HttpContext.Request.Cookies.TryGetValue(ConventionalHelper.SessionCookieName, out var token);
            var tokenPayload = await _jwtTokenService.RetrievePayloadFromTokenAsync(token, true, _apiConfiguration.SessionTtl);

            var log = new WebRequestLog()
            {
                Controller = context.RouteData.Values["Controller"].ToString(),
                Action = context.RouteData.Values["Action"].ToString(),
                Method = context.HttpContext.Request.Method,
                ProfileId = tokenPayload.ProfileId,
                ProfileStatus = tokenPayload.ProfileStatus,
                Ip = context.HttpContext.Request.GetUserIp().ToString(),
                RequestDateTime = DateTime.Now
            };

            _queue.QueueBackgroundWorkItem(x => _webRequestLogRepository.AddAsync(log));

            //invoke next step
            await next.Invoke();
        }


    }
}
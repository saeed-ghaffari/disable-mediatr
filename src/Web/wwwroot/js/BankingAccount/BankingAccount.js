﻿$(function () {
    $("#shebaNumber").mask('IR000000000000000000000000');

    $("#shebaNumber").keyup(function() {
        $("#BankId").val(returnBankId($("#shebaNumber").val()));
    });
});


function returnBankId(sheba) {

    var bban = sheba.substring(4, 8);

    if (bban.length < 3) {
        return "";
    }
    if (bban === "0600")
        return 123158;
    var str = sheba.substring(4, 7);
    switch (str) {
        case "069":
            return 123103;
        case "057":
            return 123104;
        case "018":
            return 123105;
        case "020":
            return 123106;
        case "065":
            return 123107;
        case "078":
            return 123108;
        case "056":
            return 123109;
        case "015":
            return 123110;
        case "061":
            return 123111;
        case "011":
            return 123112;
        case "014":
            return 123113;
        case "053":
            return 123114;
        case "016":
            return 123115;
        case "058":
            return 123116;
        case "060":
            return 123117;
        case "059":
            return 123118;
        case "064":
            return 123119;
        case "050":
            return 123120;
        case "070":
            return 123122;
        case "022":
            return 123123;
        case "021":
            return 123133;
        case "073":
            return 123159;
        case "075":
            return 123160;
        case "017":
            return 127;
        case "063":
            return 123102;
        case "055":
            return 123101;
        case "054":
            return 122795;
        case "013":
            return 122718;
        case "066":
            return 131;
        case "062":
            return 129;
        case "019":
            return 128;
        case "012":
            return 130;
        case "010":
            return 123161;
        default:
            return "";
    }
}

function DeleteAccount(id) {

    swal({
        title: "آیا از حذف حساب اطمینان دارید؟",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'بله',
        cancelButtonText: "خیر",
        closeOnConfirm: true,
        closeOnCancel: true,

    },
        function (isConfirm) {

            if (isConfirm) {
                var rowid = "#row_" + id;
                var data = $("#form-bankingAccount-delete").serialize();
                $.ajax({
                    type: "POST",
                    url: "/bankingAccounts/" + id,
                    data: data,
                    dataType: "json",
                    success: function (result) {
                        if (result.isSuccess) {
                            $(rowid).fadeOut(500,
                                function () {
                                    var urlString = window.location.href;
                                    var url = new URL(urlString);
                                    if (url.pathname.indexOf("bankingAccount") >= 0) {
                                        window.location.href = "/bankingAccounts";
                                    } else {
                                        document.reload();
                                    }

                                });
                        } else {
                            setTimeout(function () {
                                swal({
                                    html: true,
                                    title: 'توجه',
                                    text: result.message,
                                    type: "error",
                                    confirmButtonText: 'بستن'
                                })
                            }, 100);
                        }
                    },

                });

            }
        });

}
function DeleteBankingAccount(id) {

    swal({
        title: "آیا از حذف حساب اطمینان دارید؟",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'بله',
        cancelButtonText: "خیر",
        closeOnConfirm: true,
        closeOnCancel: true,

    },
        function (isConfirm) {

            if (isConfirm) {
                var rowid = "#row_" + id;
                var data = $("#form-bankingAccount-delete").serialize();
                $.ajax({
                    type: "POST",
                    url: "/bankingAccount/" + id,
                    data: data,
                    dataType: "json",
                    success: function (result) {
                        if (result.isSuccess) {
                            $(rowid).fadeOut(500,
                                function () {
                                    var urlString = window.location.href;
                                    var url = new URL(urlString);
                                    if (url.pathname.indexOf("bankingAccount") >= 0) {
                                        window.location.href = "/bankingAccounts";
                                    } else {
                                        document.reload();
                                    }

                                });
                        } else {
                            setTimeout(function () {
                                swal({
                                    html: true,
                                    title: 'توجه',
                                    text: result.message,
                                    type: "error",
                                    confirmButtonText: 'بستن'
                                })
                            }, 100);
                        }
                    },

                });

            }
        });

}
﻿function checkCodeMeli(code) {
    var L = code.length;
    if (L !== 10) return false;
    if (L < 8 || parseInt(code, 10) === 0) return false;
    code = ('0000' + code).substr(L + 4 - 10);
    if (parseInt(code.substr(3, 6), 10) === 0) return false;
    var c = parseInt(code.substr(9, 1), 10);
    var s = 0;
    for (var i = 0; i < 9; i++)
        s += parseInt(code.substr(i, 1), 10) * (10 - i);
    s = s % 11;
    return (s < 2 && c === s) || (s >= 2 && c === (11 - s));
};
function checkNationalCodeLegal(nationalCode) {
    var L = nationalCode.length;
    if (L < 11 || parseInt(nationalCode, 10) === 0) return false;
    if (parseInt(nationalCode.substr(3, 6), 10) === 0) return false;
    var c = parseInt(nationalCode.substr(10, 1), 10);
    var d = parseInt(nationalCode.substr(9, 1), 10) + 2;
    var z = new Array(29, 27, 23, 19, 17);
    var s = 0;
    for (var i = 0; i < 10; i++)
        s += (d + parseInt(nationalCode.substr(i, 1), 10)) * z[i % 5];
    s = s % 11;
    if (s === 10) s = 0;
    return (c === s);
}

$(function () {
    $("#resendCode").addClass("hidden");
    setTimeout(function () {
        $("#resendCode").removeClass("hidden");
    }, 60000);
    setUnqieueIdentifierMaxLentgh();

    $("#profileType").change(function () {
        setUnqieueIdentifierMaxLentgh();
    });

    $("#confirmBtn").click(function (event) {

        var tmp = $("#uniqueIdentifier").val().trim();
        if (tmp === "" || tmp === null) {

            swal({
                html: true,
                title: 'توجه',
                text: "لطفا کد ملی / شناسه ملی را وارد نمایید",
                type: "warning",
                confirmButtonText: 'بستن'
            });
        }
        else if ($("#profileType").val() === "1" && !checkCodeMeli(tmp)) {
            event.preventDefault();
            swal({
                html: true,
                title: 'توجه',
                text: "کد ملی وارد شده نامعتبر می باشد. ",
                type: "warning",
                confirmButtonText: 'بستن'
            });
        }
        else if ($("#profileType").val() === "2" && !checkNationalCodeLegal(tmp)) {
            event.preventDefault();
            swal({
                html: true,
                title: 'توجه',
                text: "شناسه ملی  وارد شده نامعتبر می باشد  . ",
                type: "warning",
                confirmButtonText: 'بستن'
            });
        }

    });

    $("#profileType").change(function () {

        if ($("#profileType").val() === "1")
            $("#Real").css('display', 'block');
        else {
            $("#Real").css('display', 'none');
        }
    });

    function setUnqieueIdentifierMaxLentgh() {

        var typeValue = $("#profileType").val();
        if (typeValue === "1") {
            $("#uniqueIdentifier").attr('maxlength', '10');
        }
        else if (typeValue === "2") {
            $("#uniqueIdentifier").attr('maxlength', '11');
        }
    }

    $("#resendbtn").click(function () {
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "api/resendOtp/" + $("#msisdn").val(),
            success: function (result) {
                if (result.isSuccess) {
                    swalMessage(result.message, "success");
                } else {
                   swalMessage(result.message, "warning");
                }
            },
            error: function (result) {
                if (result.status.toString() !== "429")
                    swalMessage(result.message, "warning");
                else
                    swalMessage("تعداد درخواست شما بیش از حد مجاز است.", "error");
            }
        });
        $("#resendCode").addClass("hidden");
        setTimeout(function () {
            $("#resendCode").removeClass("hidden");
        }, 60000);
    });
});
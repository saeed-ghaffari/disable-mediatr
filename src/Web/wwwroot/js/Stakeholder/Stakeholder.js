﻿
$(function () {

    var check = $('input:checkbox[name=IsOwnerSignature]');
    if (check.is(':checked')) {
        $("#signatureimage").removeClass("hidden");
    } else {
        $("#signatureimage").addClass("hidden");
    }
    $('input:checkbox[name=IsOwnerSignature]').click(function () {
        var $self = $(this);
        if ($self.is(':checked')) {
            $("#signatureimage").removeClass("hidden");
        } else {
            $("#signatureimage").addClass("hidden");
        }

    });

    $("#AddTakeAccess").click(function (event) {

        if ($("#FirstName").val().trim() === "" ||
            $("#LastName").val().trim() === "" ||
            $("#PositionType").val().trim() === "" ||
            $("#SignatureFile").val().trim() === "") {
            event.preventDefault();
            swalMessage("پر کردن تمامی مقادیر الزامی است", "warning");
        }
    });

    $(document).on("click", "#btncheckstatus", function () {

        var stakholdertype;
        var urlString = window.location.href;
        var url = new URL(urlString);
        if (url.pathname.indexOf("TakeAccess") >= 0) stakholdertype = "دارندگان حق برداشت";
        else if (url.pathname.indexOf("Manager") >= 0) stakholdertype = "اعضای هیئت مدیره";
        else if (url.pathname.indexOf("OrderAccess") >= 0) stakholdertype = "اشخاص مجاز به سفارش";

        if ($("#UniqueIdentifier").val().trim() === "" || $("#BirthDate").val().trim() === "") {

            swal({
                title: "توجه",
                text: "لطفا کد ملی و تاریخ تولد را وارد نمایید",
                type: "warning"

            });

        }
        else {
            $("#myModal").modal('show');
            var model = { UniqueIdentifier: $("#UniqueIdentifier").val(), BirthDate: $("#BirthDate").val(), type: stakholdertype };
            model.BirthDate = model.BirthDate.split('/').join('-');
            $.ajax({
                type: "post",
                url: "/api/checkstatus/" + model.UniqueIdentifier + "/" + model.BirthDate + "/" + model.type,
                data: {}, // JSON.stringify({ model: model }),
                contentType: "application/json",
                dataType: "json",
                success: function (result) {

                    if (result.isSuccess) {
                        $("#FirstName").val(result.firstName);
                        $("#LastName").val(result.lastName);
                        $("#myModal").modal('hide');

                    } else {
                        $("#FirstName").val("");
                        $("#LastName").val("");
                        $("#myModal").modal('hide');
                        swal({
                            title: "توجه",
                            text: result.message,
                            type: "warning"

                        });


                    }
                },

                error: function (result) {
                    if (result.status.toString() !== "429") {
                        swalMessage(result.message, "warning");
                        $("#myModal").modal('hide');
                    } else {
                        swalMessage("تعداد درخواست شما بیش از حد مجاز است.", "error");

                        $("#myModal").modal('hide');
                    }

                }

            });

        }
    });


    var types = ["application/pdf", "image/jpeg", "image/jpg", "image/png"];
    $("#SignatureFile").change(function () {
       
        if (this.files.length > 1) {
            swalMessage("حداکثر فایل انتخابی 1 فایل می باشد،لطفا تعداد فایل انتخابی را کمتر کنید", "error");
            this.value = "";
        } else {
            if (this.files[0].size > 2100000 || this.files[0].size<8100) {
                swalMessage("حجم فایل ارسالی باید بیشتر از 10 کیلوبایت و  کمتر از 2 مگابایت باشد،لطفا حجم فایل های خود را بررسی نمایید.", "error");
                this.value = "";
                return false;
            }
            if (!types.includes(this.files[0].type)) {
                swalMessage("فرمت فایل ارسالی باید از نوع  jpeg ، jpg ، png ، Pdf باشد، لطفا فرمت فایل خود را بررسی نمایید", "error");
                this.value = "";
                return false;
            }
        }
    });

});

function deleteManager(id) {

    swal({
        title: "آیا از حذف این شخص اطمینان دارید؟",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'بله',
        cancelButtonText: "خیر",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {


            if (isConfirm) {
                var data = $("#form-Manager-delete").serialize();
                var rowid = "#row_" + id;

                $.ajax({
                    type: "POST",
                    data: data,
                    url: "/managerAsync/" + id,
                    success: function (result) {
                        if (result.isSuccess)
                            $(rowid).fadeOut(500);
                        else
                            swalMessage(result.message, 'error');
                    },

                });

            }
        });



}


function deleteShareHolder(id) {

    swal({
        title: "آیا از حذف این سهامدار اطمینان دارید؟",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'بله',
        cancelButtonText: "خیر",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {


            if (isConfirm) {
                var rowid = "#row_" + id;
                var data = $("#form-Shareholder-delete").serialize();
                $.ajax({
                    type: "POST",
                    url: "/shareholderAsync/" + id,
                    data: data,
                    success: function (result) {
                        if (result.isSuccess)
                            $(rowid).fadeOut(500);
                        else
                            swalMessage(result.message, 'error');

                    },

                });

            }
        });

}


function deleteOrderAccess(id) {

    swal({
        title: "آیا از حذف این شخص اطمینان دارید؟",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'بله',
        cancelButtonText: "خیر",
        closeOnConfirm: true,
        closeOnCancel: true
    },

        function (isConfirm) {


            if (isConfirm) {
                var data = $("#form-OrderAccess-delete").serialize();
                var rowid = "#row_" + id;

                $.ajax({
                    type: "POST",
                    url: "/orderAccessAsync/" + id,
                    data: data,
                    success: function (result) {
                        if (result.isSuccess)
                            $(rowid).fadeOut(500);
                        else
                            swalMessage(result.message, 'error');
                    },

                });

            }
        });

}
function deleteTakeAccess(id) {

    swal({
        title: "آیا از حذف این شخص اطمینان دارید؟",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'بله',
        cancelButtonText: "خیر",
        closeOnConfirm: true,
        closeOnCancel: true
    },


        function (isConfirm) {

            if (isConfirm) {
                var rowid = "#row_" + id;
                var data = $("#form-TakeAccess-delete").serialize();
                $.ajax({
                    type: "POST",
                    url: "/takeAccessAsync/" + id,
                    data: data,
                    success: function (result) {
                        if (result.isSuccess)
                            $(rowid).fadeOut(500);
                        else
                            swalMessage(result.message, 'error')
                    },

                });

            }
        });



}



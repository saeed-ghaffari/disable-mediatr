﻿$(function () {
    $(".menu-bar-mobile").click(function () {
        $(".mask").addClass("show");
        $(".header-mobile .mask").css("width", "100%");
        $(".side-wrap").addClass("open");
        $("html").addClass("menu-mobile-open");
    });

    $(".mask").click(function () {
        $(".mask").removeClass("show");
        $(".header-mobile .mask").css("width", "auto");
        $(".side-wrap").removeClass("open");
        $("html").removeClass("menu-mobile-open");
    });

    $(".menu-bar").click(function () {
        if ($(".logo-wrapper.oppen").length > 0) {
            $(".logo-wrapper.oppen img").attr("src", "/Images/logo-min.png");
            $(".logo-wrapper.oppen").removeClass("oppen").addClass("cllose");
            $(".header-wrapper.oppen").removeClass("oppen").addClass("cllose");
            $(".rirgh-menu.oppen").removeClass("oppen").addClass("cllose");
            $(".rirgh-menu span.show").removeClass("show").addClass("hidden");
            $(".body-content.oppen").removeClass("oppen").addClass("cllose");
        } else {
            $(".logo-wrapper.cllose img").attr("src", "/Images/logo.png");
            $(".logo-wrapper.cllose").removeClass("cllose").addClass("oppen");
            $(".header-wrapper.cllose").removeClass("cllose").addClass("oppen");
            $(".rirgh-menu.cllose").removeClass("cllose").addClass("oppen");
            $(".rirgh-menu span.hidden").removeClass("hidden").addClass("show");
            $(".body-content.cllose").removeClass("cllose").addClass("oppen");
        }
    });

    initializeDatePicker();

    
});

function onBegin(data) {
}
function onFailed(data) {
    swalMessage('خطا در سرویس، سایت در حال بروز رسانی می باشد', 'error');
}
function onRegisterSuccess(data) {
    swal({
        title: "",
        text: data.description,
        type: data.type,
        showConfirmButton: true,
        confirmButtonText: "بستن",
        timer: 3000
    });
    setTimeout(function () {
        if (data.redirectUrl !== null) {
            $.get(data.redirectUrl, function (content) {
                $('#registercontent').html(content);
            });
        }
    }, 3000);
}
function onSuccess(data) {
    console.log(data);
    swal({
        title: "", text: data.description, type: data.type,
        showConfirmButton: false,
        timer: 3000
    });
    setTimeout(function () {
        if (data.redirectUrl !== null)
            window.location.href = data.redirectUrl;
    }, 3000);

}
function swalMessage(message, type) {
    swal({
        title: "",
        text: message,
        type: type,
        confirmButtonText: 'بستن'
    });
}

function validationInitialize() {
    $.validator.unobtrusive.parse("form");
}

function initializeDatePicker() {
  
    $(".datepicker").MdPersianDateTimePicker({
        Placement: "top",
        Trigger: "click",
        EnableTimePicker: false,
        TargetSelector: "#" + $(this).attr("id"),
        ToDate: false,
        FromDate: false,
        DisableBeforeToday: false,
        Disabled: false,
        Format: "yyyy/MM/dd",
        IsGregorian: false,
        EnglishNumber: false,
        InLine: false
    });
}








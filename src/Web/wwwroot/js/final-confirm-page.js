﻿$(function () {
    var $confirmableSectionsContainer = $('.confirmable-sections-container');
    var $sections = $confirmableSectionsContainer.find('section');
    var $buttonsContainer = $('.section-confirm-button-container')
        .data('section', 0)
        .css({ scrollTop: 0 });
    var numberOfSections = $sections.length;

    var $buttons = {
        prev: $('.section-confirm-button.prev-button'),
        next: $('.section-confirm-button.next-button'),
        final: $('.section-confirm-button.final-confirm-button')
    };

    var getTargetScrollHeight = function ($sections, targetSectionIndex) {
        var scrollHeight = 0;

        $sections.each(function(index, section) {
            if (index < targetSectionIndex) {
                scrollHeight += section.scrollHeight;
            }
        });

        return scrollHeight;
    };

    var setContainerClass = function () {
        var currentIndex = $buttonsContainer.data('section');

        if (currentIndex === 0) {
            $buttonsContainer
                .addClass('has-reached-start')
                .removeClass('has-reached-end')
                .removeClass('is-between');
        }
        else if (currentIndex < numberOfSections - 1) {
            $buttonsContainer
                .removeClass('has-reached-start')
                .removeClass('has-reached-end')
                .addClass('is-between');
        } else {
            $buttonsContainer
                .removeClass('has-reached-start')
                .addClass('has-reached-end')
                .removeClass('is-between');
        }
    }

    $buttons.next.on('click', function (e) {
        e.preventDefault();

        if ($buttonsContainer.hasClass('has-reached-end')) {
            return;
        }

        var targetSectionIndex = $buttonsContainer.data('section') + 1;

        $buttonsContainer.data('section', targetSectionIndex);

        setContainerClass();

        var totalScrollHeights = getTargetScrollHeight($sections, targetSectionIndex);

        $confirmableSectionsContainer
            .stop()
            .animate({
                scrollTop: totalScrollHeights
            });
    });

    $buttons.prev.on('click', function (e) {
        e.preventDefault();

        if ($buttonsContainer.hasClass('has-reached-start')) {
            return;
        }

        var targetSectionIndex = $buttonsContainer.data('section') - 1;

        $buttonsContainer.data('section', targetSectionIndex);

        setContainerClass();

        var totalScrollHeights = getTargetScrollHeight($sections, targetSectionIndex);

        $confirmableSectionsContainer
            .stop()
            .animate({
                scrollTop: totalScrollHeights
            });
    });

    // ----------------------------
    // form confirm button handler
    $('#btnConfirm').on('click', function(e) {
        var $this = $(this);
        var dependencySelector = $this.data('dependency');

        if (!$(dependencySelector).is(':checked')) {
            $('label[for="checkitem"]').addClass('text-danger');
            return false;
        }
    })
});
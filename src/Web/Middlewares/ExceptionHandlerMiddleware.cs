﻿using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Web.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Cerberus.Web.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        private readonly ILogger<ExceptionHandlerMiddleware> _logger;
        private readonly RequestDelegate _next;

        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger<ExceptionHandlerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (CerberusException exception) {
                context.Response.StatusCode = (int)exception.HttpStatusCode;
            }
            catch (Exception exception)
            {
                var logBody = $@"
url : {context.Request.Path}
{(context.Request.Headers.ContainsKey("User-Agent") ? ("user agent: " + context.Request.Headers["User-Agent"]) : "")}";

                _logger.LogCritical(exception, logBody);
                context.Response.StatusCode = 500;
            }
        }
    }
}
﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Cerberus.Web.Extensions
{
    public static class ModelStateExtensions
    {
        public static string ToErrorMessage(this ModelStateDictionary state) => string.Join("  ", state.Values.Select(x => string.Join(", ", x.Errors.Select(z => z.ErrorMessage))));
        public static object ToViewModel(this ModelStateDictionary state)
        {
            return new
            {
                ValidationErrors = state.Select(x => new
                {
                    PropertyName = x.Key,
                    Issues = x.Value.Errors.Select(v => v.ErrorMessage)
                })
            };
        }
    }
}

﻿using System;
using AutoMapper;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Repositories.Inquiries;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Interface.Repositories.Membership;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Interface.Services.FGM;
using Cerberus.Domain.Interface.Services.Membership;
using Cerberus.MockService;
using Cerberus.Repository;
using Cerberus.Repository.MsSql;
using Cerberus.Repository.MsSql.Log;
using Cerberus.Repository.MsSql.Membership;
using Cerberus.Service;
using Cerberus.Service.BankService.Configuration;
using Cerberus.Service.FGM;
using Cerberus.Service.Implementations;
using Cerberus.Service.Implementations.Inquiries;
using Cerberus.Service.Implementations.Membership;
using Cerberus.TasksManager.Core;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Repository;
using Cerberus.Web.Configuration;
using Cerberus.Web.Filters;
using Cerberus.Web.MappingProfile;
using Cerberus.Web.Model;
using Cerberus.Web.WebLogic;
using Gatekeeper.AspModules;
using Gatekeeper.AspModules.ActionFilter;
using Gatekeeper.AspModules.Interface;
using Gatekeeper.AspModules.Repository;
using Gatekeeper.AspModules.Service;
using Gatekeeper.Core.Configurations;
using Gatekeeper.Core.Implementation;
using Gatekeeper.Core.Interface;
using Komodo.Caching.Abstractions;
using Komodo.Caching.Redis;
using Komodo.Redis.StackExchange;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using StackExchange.Redis;

namespace Cerberus.Web.Extensions
{
    public static class ServicesExtensions
    {
        public static void AddCerberusServices(this IServiceCollection services, IConfiguration configuration)
        {
            #region Configurations

            var connectionSection = configuration.GetSection("ConnectionStrings");
            var connectionConfiguration = new ConnectionStringsConfiguration();
            connectionSection.Bind(connectionConfiguration);
            services.Configure<ConnectionStringsConfiguration>(connectionSection);
            Connection.SetConnectionString(connectionConfiguration.MasterDbConnection);
            Connection.SetConnectionString(connectionConfiguration.ReadConnection);
            services.Configure<MessagingConfiguration>(configuration.GetSection("MessagingConfiguration"));
            services.Configure<WebConfiguration>(configuration.GetSection("WebConfiguration"));
            services.Configure<OtpConfiguration>(configuration.GetSection("OtpConfiguration"));
            services.Configure<JwtTokenConfiguration>(configuration.GetSection("JwtTokenConfiguration"));
            services.Configure<AmountConfiguration>(configuration.GetSection("AmountConfiguration"));
            services.Configure<FileFtpConfiguration>(configuration.GetSection("FileFtpConfiguration"));
            services.Configure<RequestLogConfiguration>(configuration.GetSection("RequestLogConfiguration"));
            services.Configure<FileStorageConfiguration>(configuration.GetSection("FileStorageConfiguration"));
            services.Configure<RateLimitConfiguration>(configuration.GetSection("RateLimitConfiguration"));
            services.Configure<ObjectCacheConfiguration>(configuration.GetSection("ObjectCacheConfiguration"));
            services.Configure<CaptchaConfiguration>(configuration.GetSection("CaptchaConfiguration"));
            services.Configure<ShahkarServiceConfiguration>(configuration.GetSection("ShahkarServiceConfiguration"));
            services.Configure<ProfilePackageConfiguration>(configuration.GetSection("ProfilePackageConfiguration"));
            services.Configure<FgmConfiguration>(configuration.GetSection("FgmConfiguration"));
            services.Configure<InquiryConfiguration>(configuration.GetSection("InquiryConfiguration"));
            services.Configure<MasterCacheProviderConfiguration>(configuration.GetSection("MasterCacheProviderConfiguration"));
            Connection.SetConnectionString(connectionConfiguration.TaskManagerConnection);//for offline task



            #endregion

            services.AddSingleton<ISqlConnectionProvider, SqlConnectionProvider>();

            #region dbcontext

            services.AddDbContextPool<CerberusDbContext>(options =>
            {
                options.UseSqlServer(connectionConfiguration.MasterDbConnection);
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }, 8192);
         
            #endregion

            #region Redis

            //Redis
            var redisConfiguration = new ConfigurationOptions();
            foreach (var endpoint in connectionConfiguration.RedisWriteDbHost.Split(';'))
            {
                redisConfiguration.EndPoints.Add(endpoint);
            }
            redisConfiguration.Password = connectionConfiguration.RedisWriteDbPass;
            redisConfiguration.ConnectTimeout = 400;
            services.AddSingleton<ConfigurationOptions>(redisConfiguration);

            services.AddSingleton<IConnectionMultiplexerProvider>(new ConnectionMultiplexerProvider(redisConfiguration, Environment.ProcessorCount * 2));

            var dataProtectionMultiplexer = ConnectionMultiplexer.Connect(redisConfiguration);

            services.AddDataProtection()
                .SetApplicationName("sejam-web")
                .PersistKeysToStackExchangeRedis(dataProtectionMultiplexer, "web-DataProtection-Keys");

            services.AddSingleton<IDataBaseProvider, DatabaseProvider>();

            services.AddTransient<IDatabase>(provider => provider.GetService<IDataBaseProvider>().GetWriteDatabase());

            services.AddSingleton<ISerializer, BinarySerializer>();

            services.AddTransient<ICacheProvider>(provider =>
            {
                if (!provider.GetService<IOptions<WebConfiguration>>().Value.EnableCache)
                    return new NullCacheProvider();

                return new RedisCacheProvider(
                    provider.GetService<IDatabase>(),
                    provider.GetService<IDatabase>(),
                    provider.GetService<ISerializer>());
            });

            #endregion


            #region Repositories
            //gatekeeper 
            services.AddSingleton<IRequestLogRepository, LogRequestRepository>();
            services.AddSingleton<IWebRequestLogRepository, WebRequestLogRepository>();
            services.AddSingleton<ISmsLogRepository, SmsLogRepository>();
            services.AddScoped<IMessageRepository, MessageRepository>();
            services.AddScoped<IProfileRepository, ProfileRepository>();
            services.AddScoped<IAgentRepository, AgentRepository>();
            services.AddScoped<IAgentDocumentRepository, AgentDocumentRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IPrivatePersonRepository, PrivatePersonRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IBankingAccountRepository, BankingAccountRepository>();
            services.AddScoped<IBrokerRepository, BrokerRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IFinancialInfoRepository, FinancialInfoRepository>();
            services.AddScoped<IJobInfoRepository, JobInfoRepository>();
            services.AddScoped<IJobRepository, JobRepository>();
            services.AddScoped<IPrivatePersonRepository, PrivatePersonRepository>();
            services.AddScoped<ITradingCodeRepository, TradingCodeRepository>();
            services.AddScoped<IProvinceRepository, ProvinceRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IAddressSectionRepository, AddressSectionRepository>();
            services.AddScoped<IBankRepository, BankRepository>();
            services.AddScoped<IPaymentRepository, PaymentRepository>();
            services.AddScoped<ILegalPersonShareholderRepository, LegalPersonShareholderRepository>();
            services.AddScoped<ILegalPersonStakeholderRepository, LegalPersonStakeholderRepository>();
            services.AddScoped<ILegalPersonRepository, LegalPersonRepository>();
            services.AddScoped<IMembershipRepository, MembershipRepository>();
            services.AddScoped<IThirdPartyServiceRepository, ThirdPartyServiceRepository>();
            services.AddScoped<ITaskRepository, SqlTaskRepository>();
            services.AddScoped<IRecurringTaskRepository, SqlRecurringTaskRepository>();
            services.AddScoped<IAuthenticationOfficesRepository, AuthenticationOfficesRepository>();
            services.AddScoped<IProfilePackageRepository, ProfilePackageRepository>();
            services.AddScoped<IProfileHistoryRepository, ProfileHistoryRepository>();
            services.AddScoped<IAgentArchiveRepository, AgentArchiveRepository>();
            services.AddScoped<IAgentDocumentArchiveRepository, AgentDocumentArchiveRepository>();
            services.AddScoped<IPostInquiryRepository, PostInquiryRepository>();
            services.AddScoped<IPermanentOtpRepository, PermanentOtpRepository>();
            services.AddScoped<IMsisdnRepository, MsisdnRepository>();
            services.AddScoped<IShahkarInquiryRepository, ShahkarInquiryRepository>();
       


            #endregion

            #region ReadRepositories
            //read repositories
            services.AddCerberusReadRepositories(configuration);
            #endregion

            #region services
            //banking gateway
            services.AddScoped<MockGatewayService>();

            services.AddScoped<IPaymentService, PaymentService>(provider =>
                new PaymentService(
                    provider.GetService<IPaymentRepository>(), provider.GetService<IPaymentReadRepository>()

            ));


            //services.AddTransient<IPaymentService, PaymentService>(provider =>
            //    new PaymentService(
            //        provider.GetService<IPaymentRepository>(),
            //        provider.GetService<MockGatewayService>(),
            //        provider.GetService<MockGatewayService>(),
            //        provider.GetService<ILogger<PaymentService>>()));


            services.AddScoped<MessageService, MessageService>();
            services.AddScoped<IMessagingService, MessagingService>();
            services.AddScoped<IProfileService, ProfileService>();
            services.AddScoped<IAgentService, AgentService>();
            services.AddScoped<IAgentDocumentService, AgentDocumentService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IPrivatePersonService, PrivatePersonService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<IBankingAccountService, BankingAccountService>();
            services.AddScoped<IBrokerService, BrokerService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IFinancialInfoService, FinancialInfoService>();
            services.AddScoped<IJobInfoService, JobInfoService>();
            services.AddScoped<IJobService, JobService>();
            services.AddScoped<IPrivatePersonService, PrivatePersonService>();
            services.AddScoped<ITradingCodeService, TradingCodeService>();
            services.AddScoped<IBankService, BankService>();
            services.AddScoped<IProvinceService, ProvinceService>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IAddressSectionService, AddressSectionService>();
            services.AddScoped<IBankService, BankService>();
            services.AddScoped<ILegalPersonShareholderService, LegalPersonShareholderService>();
            services.AddScoped<ILegalPersonStakeholderService, LegalPersonStakeholderService>();
            services.AddScoped<ILegalPersonService, LegalPersonService>();
            services.AddScoped<IMembershipService, MembershipService>();
            services.AddScoped<IThirdPartyServiceService, ThirdPartyServiceService>();
            services.AddScoped<IOfflineTaskService, OfflineTaskService>();
            services.AddScoped<IAuthenticationOfficesService, AuthenticationOfficesService>();
            services.AddScoped<IProfilePackageService, ProfilePackageService>();
            services.AddScoped<IShahkarService, ShahkarService>();
            services.AddScoped<IProfileHistoryService, ProfileHistoryService>();
            services.AddScoped<IFgmService, FgmService>();
            services.AddScoped<IShahkarService, ShahkarService>();
            services.AddScoped<IProfileHistoryService, ProfileHistoryService>();
            services.AddScoped<IPostInquiryService, PostInquiryService>();
            services.AddScoped<IMasterCacheProvider, MasterCacheProvider>();
            services.AddScoped<IMsisdnService, MsisdnService>();

            // gatekeeper

            services.AddScoped<IOtpService, OtpService>();
            services.AddScoped<IJwtTokenService<UserSessionInfo>, JwtTokenService<UserSessionInfo>>();
            services.AddScoped<IRateLimitService, RateLimitService>();
            #endregion

            #region actionfilters

            services.AddScoped<CookieHelper>();
            services.AddScoped<AuthorizeUserAttribute>();
            services.AddScoped<RateLimitAttribute>();
            services.AddScoped<ValidateCaptchaAttribute>();
            services.AddScoped<LogWebRequestAttribute>();

            #endregion

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        public static void AddMapper(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(provider =>
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<UserProfileProfile>();
                    cfg.AddProfile<AddressProfile>();
                    cfg.AddProfile<BrokerProfile>();
                    cfg.AddProfile<FileProfile>();
                    cfg.AddProfile<AgentProfile>();
                    cfg.AddProfile<PrivatePersonProfile>();
                    cfg.AddProfile<CountryProfile>();
                    cfg.AddProfile<JobInfoProfile>();
                    cfg.AddProfile<JobProfile>();
                    cfg.AddProfile<FinancialInfoProfile>();
                    cfg.AddProfile<TradingCodeProfile>();
                    cfg.AddProfile<BankingAccountProfile>();
                    cfg.AddProfile<ProvinceProfile>();
                    cfg.AddProfile<CityProfile>();
                    cfg.AddProfile<AddressSectionProfile>();
                    cfg.AddProfile<PaymentProfile>();
                    cfg.AddProfile<BankProfile>();
                    cfg.AddProfile<LegalPersonShareholderProfile>();
                    cfg.AddProfile<LegalPersonProfile>();
                    cfg.AddProfile<LegalPersonStakeholderProfile>();
                    cfg.AddProfile<PersonNativeMapper>();
                    cfg.AddProfile<AddressTmpMapper>();
                    cfg.AddProfile<BankingAccountTmpMapper>();
                    cfg.AddProfile<AccountBankingMapper>();
                    cfg.AddProfile<AccountBankingListMapper>();
                    cfg.AddProfile<AccountBankingViewMapperMapper>();
                    cfg.AddProfile<PrivatePersonDashboardMapper>();
                    cfg.AddProfile<LegalPersonMappingProfile>();
                    cfg.AddProfile<AuthenticationOfficesMapper>();
                    cfg.AddProfile<AgentArchiveProfile>();
                    cfg.AddProfile<AgentDocumentArchiveProfile>();


                });
                return config.CreateMapper();
            });
        }
    }
}
﻿namespace Cerberus.Web.Configuration
{
    public class FileFtpConfiguration
    {
        public string FtpEndPoint { get; set; }
        public int FtpPort { get; set; }
        public string FtpUser { get; set; }
        public string FtpPass { get; set; }
        public string StakeholderFilePath  { get; set; }
    }
}

﻿using System;

namespace Cerberus.Web.Configuration
{
    public class WebConfiguration
    {
        public TimeSpan SessionTtl { get; set; }

        public TimeSpan OtpTtl { get; set; }
        public bool EnableCache { get; set; }
        public int RegisterMobileCount { get; set; }
        public bool EnableRequestLog { get; set; }
        public bool SecureCookie { get; set; }
    }
}

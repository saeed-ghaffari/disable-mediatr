﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Web.Configuration
{
    public class CaptchaConfiguration
    {
        public TimeSpan CaptchaTtl { get; set; }
        public int CaptchaImageHeight { get; set; }
        public int CaptchaImageWidth { get; set; }
        public string KeySalt { get; set; }
        public string CaptchaPrefix { get; set; }
        public int CaptchaLength { get; set; }

    }
}

﻿using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NLog.Web;

namespace Cerberus.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {

            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json").Build();

            return WebHost.CreateDefaultBuilder(args)
                .UseKestrel(options =>
                {
                    options.Limits.MinResponseDataRate = null;
                    options.Limits.KeepAliveTimeout = TimeSpan.FromSeconds(20);
                    options.Limits.MaxRequestBodySize = 20000000;
                })
                .UseConfiguration(config)
                .CaptureStartupErrors(true)
                .UseUrls($"http://0.0.0.0:{config.GetValue<int>("WebConfiguration:UseUrlPort")}")
                .ConfigureLogging(logging =>
                {
                    logging.SetMinimumLevel(LogLevel.Warning);
                    logging.AddConsole();
                    logging.AddDebug();
                })
                .UseNLog()
                .UseStartup<Startup>()
                .Build();
        }

    }
}

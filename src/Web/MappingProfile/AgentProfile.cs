﻿using Cerberus.Domain.Model;
using Cerberus.Utility;
using Cerberus.Web.ViewModel.Agent;

namespace Cerberus.Web.MappingProfile
{
    public class AgentProfile : AutoMapper.Profile
    {
        public AgentProfile()
        {
            CreateMap<CreateAgentViewModel, Agent>()
                .ForMember(dis => dis.ExpirationDate, opt => opt.MapFrom(s => s.ExpirationDate.ToGeorgianDate()));
        }
    }
}
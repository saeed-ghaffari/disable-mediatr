﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Web.MappingProfile
{
    public class AgentArchiveProfile:AutoMapper.Profile
    {
        public AgentArchiveProfile()
        {
            CreateMap<Agent, AgentArchive>()
                .ForMember(dis => dis.AgentCreationDate, opt => opt.MapFrom(s => s.CreationDate))
                .ForMember(dis => dis.AgentModifiedDate, opt => opt.MapFrom(s => s.ModifiedDate))
                .ForMember(dis => dis.AgentId, opt => opt.MapFrom(s => s.Id))
                .ForMember(dis => dis.Id, opt => opt.Ignore());
        }
    }
}

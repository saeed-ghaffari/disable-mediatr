﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Web.MappingProfile
{
    public class AgentDocumentArchiveProfile : AutoMapper.Profile
    {
        public AgentDocumentArchiveProfile()
        {
            CreateMap<AgentDocument, AgentDocumentArchive>()
                .ForMember(dis => dis.AgentDocumentCreationDate, opt => opt.MapFrom(s => s.CreationDate))
                .ForMember(dis => dis.AgentDocumentModifiedDate, opt => opt.MapFrom(s => s.ModifiedDate))
                .ForMember(dis => dis.AgentDocumentId, opt => opt.MapFrom(s => s.Id))
                .ForMember(dis => dis.Id, opt => opt.Ignore());
        }
    }
}

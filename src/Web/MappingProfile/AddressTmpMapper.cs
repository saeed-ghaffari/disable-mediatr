﻿using System.Collections.Generic;
using Cerberus.Domain.Model.Membership;
using Cerberus.Web.ViewModel.Address;

namespace Cerberus.Web.MappingProfile
{
    public class AddressTmpMapper : AutoMapper.Profile
    {
        public AddressTmpMapper()
        {
            CreateMap<AddressMembership, CreateAddressViewModel>()
                .ForMember(dis => dis.RemnantAddress, opt => opt.MapFrom(s => s.Address));
        }
    }
}


﻿using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.Payment;

namespace Cerberus.Web.MappingProfile
{
    public class PaymentProfile : AutoMapper.Profile
    {
        public PaymentProfile()
        {
            CreateMap<CreatePaymentViewModel, Payment>()
            
                .ForMember(dis => dis.Status, opt => opt.UseValue(PaymentStatus.Init));

        }
    }
}
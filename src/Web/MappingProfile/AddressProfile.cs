﻿using System;
using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.Address;

namespace Cerberus.Web.MappingProfile
{
    public class AddressProfile : AutoMapper.Profile
    {
        public AddressProfile()
        {

            CreateMap<CreateAddressViewModel, Address>();
               
            CreateMap<Address, CreateAddressViewModel>();

        }
    }
}
﻿using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Membership;

namespace Cerberus.Web.MappingProfile
{
    public class AccountBankingListMapper : AutoMapper.Profile
    {
        public AccountBankingListMapper()
        {
            CreateMap<BankingAccountMembership, BankingAccount>()
                 .ForMember(dest => dest.Id, opt => opt.Ignore())
                 .ForAllMembers(opts => opts.Condition(x => x != null));
        }
    }
}
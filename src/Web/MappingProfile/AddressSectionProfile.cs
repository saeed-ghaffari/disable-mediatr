﻿using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.ViewComponents;
using AutoMapperProfile = AutoMapper.Profile;

namespace Cerberus.Web.MappingProfile
{
    public class AddressSectionProfile : AutoMapperProfile
    {
        public AddressSectionProfile()
        {
            CreateMap<AddressSection, DropdownItemViewModel>()
                .ForMember(dis => dis.Text, opt => opt.MapFrom(s => s.Name))
                .ForMember(dis => dis.Value, opt => opt.MapFrom(s => s.Id));
        }
    }
}
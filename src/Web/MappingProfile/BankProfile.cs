﻿using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.ViewComponents;

namespace Cerberus.Web.MappingProfile
{
    public class BankProfile : AutoMapper.Profile
    {
        public BankProfile()
        {
            CreateMap<Bank, DropdownItemViewModel>()
                .ForMember(dis => dis.Text, opt => opt.MapFrom(s => s.Name))
                .ForMember(dis => dis.Value, opt => opt.MapFrom(s =>s.Id));
        }
    }
}
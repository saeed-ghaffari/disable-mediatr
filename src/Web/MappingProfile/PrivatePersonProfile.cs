﻿using Cerberus.Domain.Model;
using Cerberus.Utility;
using Cerberus.Web.ViewModel.PrivatePerson;

namespace Cerberus.Web.MappingProfile
{
    public class PrivatePersonProfile : AutoMapper.Profile
    {
        public PrivatePersonProfile()
        {
            CreateMap<CreatePrivatePersonNativeViewModel, PrivatePerson>()
                .ForMember(dis => dis.FirstName, opt => opt.MapFrom(s => s.FirstName.Unify()))
                .ForMember(dis => dis.LastName, x => x.MapFrom(s => s.LastName.Unify()))
                .ForMember(dis => dis.FatherName, opt => opt.MapFrom(s => s.FatherName.Unify()))
                .ForMember(dis => dis.PlaceOfBirth, x => x.MapFrom(s => s.PlaceOfBirth.Unify()))
                .ForMember(dis => dis.PlaceOfIssue, opt => opt.MapFrom(s => s.PlaceOfIssue.Unify()))
                .ForMember(dis => dis.ShNumber, x => x.MapFrom(s => s.ShNumber.Unify()))
                .ForMember(dis => dis.BirthDate, x => x.MapFrom(s => s.BirthDate.ToGeorgianDate()));



            CreateMap<CreatePrivatePersonForeignViewModel, PrivatePerson>()
                .ForMember(dis => dis.FirstName, opt => opt.MapFrom(s => s.FirstName.Unify()))
                .ForMember(dis => dis.LastName, x => x.MapFrom(s => s.LastName.Unify()))
                .ForMember(dis => dis.FatherName, opt => opt.MapFrom(s => s.FatherName.Unify()))
                .ForMember(dis => dis.EvidenceNumber, x => x.MapFrom(s => s.EvidenceNumber.Unify()))
                .ForMember(dis => dis.BirthDate, x => x.MapFrom(s => s.BirthDate.ToGeorgianDate()))
                .ForMember(dis => dis.EvidenceExpirationDate,
                    x => x.MapFrom(s => s.EvidenceExpirationDate.ToGeorgianDate()))
                .ForMember(dis => dis.LicenseExpirationDate,
                    x => x.MapFrom(s => s.LicenseExpirationDate.ToGeorgianDate()))
                .ForMember(dis => dis.LicenseIssueDate, x => x.MapFrom(s => s.LicenseIssueDate.ToGeorgianDate()));

            CreateMap<PrivatePerson, CreatePrivatePersonForeignViewModel>()
                .ForMember(dis => dis.BirthDate, x => x.MapFrom(s => s.BirthDate.ToPersianDate("yyyy/MM/dd")))
                .ForMember(dis => dis.EvidenceExpirationDate, x => x.MapFrom(s => s.EvidenceExpirationDate.ToPersianDate("yyyy/MM/dd")))
                .ForMember(dis => dis.LicenseExpirationDate,x => x.MapFrom(s => s.LicenseExpirationDate.ToPersianDate("yyyy/MM/dd")))
                .ForMember(dis => dis.LicenseIssueDate, x => x.MapFrom(s => s.LicenseIssueDate.ToPersianDate("yyyy/MM/dd")));


            CreateMap<PrivatePerson, CreatePrivatePersonNativeViewModel>()
                .ForMember(dis => dis.BirthDate, x => x.MapFrom(s => s.BirthDate.ToPersianDate("yyyy/MM/dd")));


        }
    }
}
﻿using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.ViewComponents;
using AutoMapperProfile = AutoMapper.Profile;

namespace Cerberus.Web.MappingProfile
{
    public class CityProfile : AutoMapperProfile
    {
        public CityProfile()
        {
            CreateMap<City, DropdownItemViewModel>()
                .ForMember(dis => dis.Text, opt => opt.MapFrom(s => s.Name))
                .ForMember(dis => dis.Value, opt => opt.MapFrom(s => s.Id));
        }
    }
}
﻿
using Cerberus.Domain.Model;
using Cerberus.Utility;
using Cerberus.Web.ViewModel.LegalPersonStakeholder;

namespace Cerberus.Web.MappingProfile
{
    public class LegalPersonStakeholderProfile : AutoMapper.Profile
    {
        public LegalPersonStakeholderProfile()
        {
            CreateMap<LegalPersonStakeholder, StakeholderManagerViewModel>()
                .ForMember(dis => dis.FirstName, opt => opt.MapFrom(s => s.StakeholderProfile.PrivatePerson.FirstName))
                .ForMember(dis => dis.LastName, opt => opt.MapFrom(s => s.StakeholderProfile.PrivatePerson.LastName))
                .ForMember(dis => dis.UniqueIdentifier, opt => opt.MapFrom(s => s.StakeholderProfile.UniqueIdentifier));


            CreateMap<LegalPersonStakeholder, StakeholderOrderAccessViewModel>()
                .ForMember(dis => dis.FirstName, opt => opt.MapFrom(s => s.StakeholderProfile.PrivatePerson.FirstName))
                .ForMember(dis => dis.LastName, opt => opt.MapFrom(s => s.StakeholderProfile.PrivatePerson.LastName))
                .ForMember(dis => dis.UniqueIdentifier, opt => opt.MapFrom(s => s.StakeholderProfile.UniqueIdentifier))
                .ForMember(dis => dis.StartAt, opt => opt.MapFrom(s => s.StartAt.ToPersianDate("yyyy/MM/dd")))
                .ForMember(dis => dis.EndAt, opt => opt.MapFrom(s => s.EndAt.ToPersianDate("yyyy/MM/dd")))
                .ForMember(dis => dis.SignatureFile, opt => opt.MapFrom(s => s.SignatureFile.Title));


            CreateMap<LegalPersonStakeholder, StakeholderTakeAccessViewModel>()
                .ForMember(dis => dis.FirstName, opt => opt.MapFrom(s => s.StakeholderProfile.PrivatePerson.FirstName))
                .ForMember(dis => dis.LastName, opt => opt.MapFrom(s => s.StakeholderProfile.PrivatePerson.LastName))
                .ForMember(dis => dis.UniqueIdentifier, opt => opt.MapFrom(s => s.StakeholderProfile.UniqueIdentifier))
                .ForMember(dis => dis.SignatureFile, opt => opt.MapFrom(s => s.SignatureFile.Title));


             

        }
    }
}
﻿using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.File;

namespace Cerberus.Web.MappingProfile
{
    public class FileProfile : AutoMapper.Profile
    {
        public FileProfile()
        {
            CreateMap<CreateFileViewModel, File>();
        }
    }
}
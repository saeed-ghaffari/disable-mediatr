﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace Cerberus.Web.MappingProfile
{
    public class LegalPersonMappingProfile:Profile
    {
        public LegalPersonMappingProfile()
        {
            CreateMap<Profile, LegalPersonProfile>();
        }
    }
}

﻿using Cerberus.Domain.Model.Membership;
using Cerberus.Web.ViewModel.BankingAccount;

namespace Cerberus.Web.MappingProfile
{
    public class BankingAccountTmpMapper : AutoMapper.Profile
    {
        public BankingAccountTmpMapper()
        {
            CreateMap<BankingAccountMembership, CreateBankingAccountViewModel>();
        }
    }
}
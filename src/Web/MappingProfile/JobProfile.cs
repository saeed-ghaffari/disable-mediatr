﻿using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.ViewComponents;

namespace Cerberus.Web.MappingProfile
{
    public class JobProfile : AutoMapper.Profile
    {
        public JobProfile()
        {
            CreateMap<Job, DropdownItemViewModel>()
                .ForMember(dis => dis.Text, opt => opt.MapFrom(s => s.Title))
                .ForMember(dis => dis.Value, opt => opt.MapFrom(s =>s.Id));
        }
    }
}
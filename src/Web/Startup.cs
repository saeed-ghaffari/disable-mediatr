﻿using Cerberus.Web.Extensions;
using Cerberus.Web.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json.Serialization;
using System.IO;
using Aspina;
using Cerberus.Utility.JsonConverters;
using Gatekeeper.AspModules.Middleware;
using Microsoft.AspNetCore.Http;

namespace Cerberus.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCerberusServices(Configuration);

            //background task
            services.AddHostedService<QueuedHostedService>();
            services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();

            services.AddResponseCompression();

            services.AddMapper(Configuration);

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.Converters.Add(new StandardDateTimeConverter());

            });

            //TODO: Upload File Directory
            services.AddSingleton<IFileProvider>(new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));

            services.AddMvc();

        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
                app.UseExceptionHandler("/error/{0}");

            app.UseMiddleware<CustomHeadersMiddleware>();

            app.UseResponseCompression();

            //static files
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    ctx.Context.Response.Headers.Append("Cache-Control", $"public, max-age=604800");
                }
            });

            if (Configuration.GetSection("RequestLogConfiguration").GetValue<bool>("IsEnable"))
                app.UseMiddleware<LogRequestMiddleware>();

            app.UseStatusCodePagesWithReExecute("/error/{0}");

            app.UseMiddleware<ExceptionHandlerMiddleware>();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
﻿using Cerberus.Domain.Enum;
using System.Runtime.Serialization;
using Cerberus.Web.Enum;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace Cerberus.Web.Model
{
    [DataContract]
    public class UserSessionInfo
    {
        [DataMember(Order = 1)]
        [JsonProperty(PropertyName = "pi")]
        public long ProfileId { get; set; }

        [DataMember(Order = 3)]
        [JsonProperty(PropertyName = "ps")]
        public ProfileStatus ProfileStatus { get; set; }

        [DataMember(Order = 4)]
        [JsonProperty(PropertyName = "t")]
        public ProfileOwnerType ProfileOwnerType { get; set; }


        [DataMember(Order = 5)]
        [JsonProperty(PropertyName = "cs")]
        public CurrentStep CurrentStep{ get; set; }


        [DataMember(Order = 6)]
        [JsonProperty(PropertyName = "as")]
        public AvailableScope AvailableScope { get; set; }

        [DataMember(Order = 7)]
        [JsonProperty(PropertyName = "si")]
        public long? ServiceId { get; set; }

        [DataMember(Order = 8)]
        [JsonProperty(PropertyName = "mo")]
        public long? Mobile { get; set; }

    }
}
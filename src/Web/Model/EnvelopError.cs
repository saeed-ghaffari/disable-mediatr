﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;

namespace Cerberus.Web.Model
{
    public class EnvelopError
    {
        public ErrorCode ErrorCode { get; set; }

        public string CustomMessage { get; set; }

        public Exception Exception { get; set; }
    }
}

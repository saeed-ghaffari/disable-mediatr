﻿namespace Cerberus.Web.Model
{
    public class CreateSessionResponse
    {
        public string SessionKey { get; set; }
    }
}
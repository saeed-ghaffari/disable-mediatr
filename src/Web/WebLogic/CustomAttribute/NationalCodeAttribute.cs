﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;

namespace Cerberus.Web.WebLogic.CustomAttribute
{
    public class NationalCodeAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            try
            {
                if (value == null) return true;
        
                var inputValue = value.ToString();
                if (!Regex.IsMatch(inputValue, @"^(?!(\d)\1{9})\d{10}$"))
                    return false;

                //if it's not able convert to int
                if (!int.TryParse(inputValue.Substring(9, 1), out var number)) return false;


                var check = Convert.ToInt32(inputValue.Substring(9, 1));
                var sum = Enumerable.Range(0, 9)
                              .Select(x => Convert.ToInt32(inputValue.Substring(x, 1)) * (10 - x))
                              .Sum() % 11;

                return sum < 2 && check == sum || sum >= 2 && check + sum == 11;
            }
            catch
            {
                return false;
            }

        }
    }
}

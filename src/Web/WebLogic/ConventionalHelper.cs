﻿namespace Cerberus.Web.WebLogic
{
    public static class ConventionalHelper
    {
        public static string CreateOtpKey(string msisdn, OtpType type)
        {
            return $"{msisdn}:{type}";
        }

        public static string SessionCookieName => "_sc";

    }
}
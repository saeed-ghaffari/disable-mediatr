﻿using System.IO;
using Cerberus.Web.Configuration;
using FluentFTP;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Cerberus.Web.WebLogic
{
    public static class FileHelper
    {
        public static async Task<bool> UploadAsync(byte[] file,string fileName, FileFtpConfiguration ftpConfiguration)
        {
            try
            {
                var client = new FtpClient(ftpConfiguration.FtpEndPoint) {Port = ftpConfiguration.FtpPort, Credentials = new NetworkCredential(ftpConfiguration.FtpUser, ftpConfiguration.FtpPass) };
                client.Connect();
                if (!client.DirectoryExists(ftpConfiguration.StakeholderFilePath))
                    client.CreateDirectory(ftpConfiguration.StakeholderFilePath);

               return await client.UploadAsync(file, $"{ftpConfiguration.StakeholderFilePath}/{fileName}");
            }
            catch
            {
                return false;
            }
        }

        public static byte[] ToByte(this IFormFile file)
        {
            if (file == null)
                return null;
            byte[] data;

            using (var memoryStream = new MemoryStream())
            {
                file.CopyTo(memoryStream);
                data = memoryStream.ToArray();
            }
            return data;
        }
    }
}

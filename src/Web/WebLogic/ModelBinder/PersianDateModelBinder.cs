﻿#region Using

using System;
using System.Globalization;
using System.Threading.Tasks;
using Cerberus.Utility;
using Microsoft.AspNetCore.Mvc.ModelBinding;

#endregion

namespace Cerberus.Web.WebLogic.ModelBinder
{
    public class PersianDateModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            ValueProviderResult valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            
            if (valueResult.FirstValue !="" && valueResult.Length>0)
            {

                var parts = valueResult.FirstValue.Split('/');
                if (parts.Length < 3) return null;
                var actualValue = new DateTime(parts[0].ToInt(), parts[1].ToInt(), parts[2].ToInt(),
                    new PersianCalendar());

                bindingContext.ModelState.SetModelValue(bindingContext.ModelName, actualValue, "");
                bindingContext.Result = ModelBindingResult.Success(actualValue);
            }

            return Task.CompletedTask;
        }
    }
}
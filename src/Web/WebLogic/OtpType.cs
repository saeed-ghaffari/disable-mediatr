﻿namespace Cerberus.Web.WebLogic
{
    public enum OtpType
    {
        Registration = 1,
        AgentValidator = 2,
        EditProfile = 3,
        RecoverTraceCode = 4,
        EditMobile = 5,
        EditMobileWithShahkarService = 6
    }
}
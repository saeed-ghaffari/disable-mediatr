﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Web.Controllers;
using Cerberus.Web.Enum;
using Cerberus.Web.Extensions;
using Cerberus.Web.Model;
using Cerberus.Web.WebLogic.Delegate;
using Gatekeeper.AspModules.Interface;
using Gatekeeper.AspModules.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Cerberus.Web.WebLogic
{
    public class BaseController : Controller
    {
        private static readonly List<RedirectRule> IranianPrivatePersonSteps = new List<RedirectRule>()
        {

            new RedirectRule(CurrentStep.Init,AvailableScope.Payment,"Index","Payment"),
            new RedirectRule(CurrentStep.Payment,AvailableScope.BaseInfo,"Index","BaseInfo"),
            new RedirectRule(CurrentStep.BaseInfo,AvailableScope.Agent,"Index","Agent"),
            new RedirectRule(CurrentStep.Agent,AvailableScope.Address,"Index","Address"),
            new RedirectRule(CurrentStep.AddressInfo,AvailableScope.TradingCode,"Index","TradingCode"),
            new RedirectRule(CurrentStep.TradingCode,AvailableScope.JobInfo,"Index","JobInfo"),
            new RedirectRule(CurrentStep.JobInfo,AvailableScope.FinancialInfo,"Index","FinancialInfo"),
            new RedirectRule(CurrentStep.FinancialInfo,AvailableScope.BankingAccount,"Index","BankingAccount"),
            new RedirectRule(CurrentStep.BankingAccount,AvailableScope.Confirm,"Index","Confirm"),
            new RedirectRule(CurrentStep.PolicyAccepted,AvailableScope.Factor,"Checkout","Factor")
        };

        private static readonly List<RedirectRule> IranianLegalPersonSteps = new List<RedirectRule>()
        {
            new RedirectRule(CurrentStep.Init,AvailableScope.Payment,"Index","Payment"),
            new RedirectRule(CurrentStep.Payment,AvailableScope.BaseInfo,"Index","BaseInfo"),
            new RedirectRule(CurrentStep.BaseInfo,AvailableScope.Address,"Index","Address"),
            new RedirectRule(CurrentStep.AddressInfo,AvailableScope.TradingCode,"Index","TradingCode"),
            new RedirectRule(CurrentStep.TradingCode,AvailableScope.StakeholderTakeAccess,"TakeAccess", "Stakeholder"),
            new RedirectRule(CurrentStep.AllowTake,AvailableScope.StakeholderTakeManager,"Manager","Stakeholder"),
            new RedirectRule(CurrentStep.Manager,AvailableScope.StakeholderShareholder,"Shareholder","Stakeholder"),
            new RedirectRule(CurrentStep.Shareholder,AvailableScope.StakeholderTakeOrder,"OrderAccess","Stakeholder"),
            new RedirectRule(CurrentStep.AllowOrder,AvailableScope.FinancialInfo,"Index","FinancialInfo"),
            new RedirectRule(CurrentStep.FinancialInfo,AvailableScope.BankingAccount,"Index","BankingAccount"),
            new RedirectRule(CurrentStep.BankingAccount,AvailableScope.Confirm,"Index","Confirm"),
            new RedirectRule(CurrentStep.PolicyAccepted ,AvailableScope.Factor,"Checkout","Factor")
        };

        public class RedirectRule
        {
            public RedirectRule(CurrentStep step, AvailableScope requiredScope, string action, string controller)
            {
                Step = step;
                RequiredScope = requiredScope;
                ActionName = action;
                ControllerName = controller;
            }
            public CurrentStep Step { get; set; }
            public AvailableScope RequiredScope { get; set; }
            public string ActionName { get; set; }
            public string ControllerName { get; set; }

        }

        public new UserSessionInfo User
        {
            get
            {
                return Request.GetCurrentUserData();
            }
        }

        public void Alert(string message, NotificationType notificationType)
        {
            TempData["Notification"] = $"<script> swal({{title:\'\',text:\"{message}\",type:\"{notificationType.ToString().ToLower()}\",showConfirmButton: true ,confirmButtonText: \"بستن\",timer: 40000}});</script>";
        }


        public RedirectToActionResult RedirectToNextStep(CurrentStep currentStep)
        {
            if (User.ProfileOwnerType == ProfileOwnerType.IranianPrivatePerson)
            {
                var rule = IranianPrivatePersonSteps
                    .First(x => x.Step >= currentStep && ((User.AvailableScope & x.RequiredScope) == x.RequiredScope));

                return RedirectToAction(rule.ActionName, rule.ControllerName);
            }

            if (User.ProfileOwnerType == ProfileOwnerType.IranianLegalPerson)
            {
                var rule = IranianLegalPersonSteps.First(x =>
                    x.Step >= currentStep && ((User.AvailableScope & x.RequiredScope) == x.RequiredScope));

                return RedirectToAction(rule.ActionName, rule.ControllerName);
            }


            throw new NotImplementedException();
        }

        public override void OnActionExecuting(Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext actionContext)
        {
            var feature = actionContext.HttpContext.Features.Get<IAspinaMetricFeatures>() ?? new AspinaMetricFeature();

            feature.AddTag("Controller", actionContext.ActionDescriptor.RouteValues["Controller"]);
            feature.AddTag("Action", actionContext.ActionDescriptor.RouteValues["Action"]);
            feature.AddTag("Scope", "Sejam-Web");
            actionContext.HttpContext.Features.Set(feature);

            this.ViewBag.Ver = "1.3";
        }

    }
}
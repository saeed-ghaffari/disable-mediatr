﻿using Cerberus.Domain.Enum;

namespace Cerberus.Web.ViewModel.File
{
    public class CreateFileViewModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public FileType Type { get; set; }
        public string MimeType { get; set; }
    }
}

﻿namespace Cerberus.Web.ViewModel.BankingAccount
{
    public class DeleteBankingAccountViewModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
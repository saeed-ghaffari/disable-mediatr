﻿using Cerberus.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Web.ViewModel.BankingAccount
{
    public class SejamiAccountViewModel
    {
        public string Status { get; set; }
        public PackageType PackageType { get; set; }
      
    }
}

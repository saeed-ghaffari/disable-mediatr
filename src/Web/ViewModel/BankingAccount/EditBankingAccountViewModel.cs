﻿using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.Enum;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Cerberus.Web.ViewModel.BankingAccount
{
    public class EditBankingAccountViewModel
    {
        public long Id { get; set; }

        [Display(Name = "شماره حساب")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(20, ErrorMessage = "{0} مجاز نمی باشد")]
        public string AccountNumber { get; set; }

        [Display(Name = "نوع شماره حساب")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [Range((byte)(BankingAccountType.LongTermAccount), ((byte)BankingAccountType.SavingAccount), ErrorMessage = "مقدار نامعتبر است")]
        public BankingAccountType? Type { get; set; }

        [Display(Name = "نام بانک")]
        [BindRequired]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0} معتبر نمی باشد")]
        public long? BankId { get; set; }

        [Display(Name = "کد شعبه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(6, ErrorMessage = "{0} مجاز نمی باشد")]
        public string BranchCode { get; set; }

        [Display(Name = "نام شعبه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(30, ErrorMessage = "{0} نمی تواند بیشتر از 30 کارکتر باشد")]
        public string BranchName { get; set; }

        [Display(Name = "شهر شعبه بانک")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public long? BranchCityId { get; set; }

    }
}
﻿using Cerberus.Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.CustomValidation;
using Cerberus.Web.WebLogic.ModelBinder;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Web.ViewModel.LegalPerson
{
    public class CreateLegalPersonViewModel
    {
        [Display(Name = "نام شخصیت حقوقی ")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(100, ErrorMessage = "{0} نمی تواند بیشتر از 100 کارکتر باشد")]
        public string CompanyName { get; set; }

        [Display(Name = "نوع شخصیت حقوقی ")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [Range((int)(LegalPersonType.Governmental),(int)(LegalPersonType.InvestmentFunds),ErrorMessage = "مقدار نامعتبر است")]
        public LegalPersonType Type { get; set; }

        [Display(Name = "تابعیت")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public long? CitizenshipCountryId { get; set; }

        [Display(Name = "شماره ثبت")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[-_/،.چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ0123456789,\s]+\s*$", ErrorMessage = "لطفا از اعداد و کاراکتر های مجاز استفاده نمایید")]
        [MaxLength(20, ErrorMessage = "{0} نمی تواند بیشتر از 20 کارکتر باشد")]
        public string RegisterNumber { get; set; }

        [Display(Name = "محل ثبت")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(50, ErrorMessage = "{0} نمی تواند بیشتر از 50 کارکتر باشد")]
        public string RegisterPlace { get; set; }

        [Display(Name = "تاریخ ثبت")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [PersianMinDatetimeValidation(ErrorMessage = "{0} معتبر نیست")]
        public string RegisterDate { get; set; }

        [Display(Name = "کد اقتصادی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(12, ErrorMessage = "{0} مجاز نمی باشد")]
        public string EconomicCode { get; set; }


        [Display(Name = "شناسه ملی")]
        public string UniqueIdentifier { get; set; }
    }
}
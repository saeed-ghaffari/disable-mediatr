﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Web.ViewModel.ProfilePackageViewModel
{
    public class ChangeMobileViewModel
    {
        public string Mobile { get; set; }
        public string NewMobile { get; set; }
        public PrimitivePackageType PrimitivePackageType{ get; set; }
    }
}

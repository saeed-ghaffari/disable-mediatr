﻿using System.ComponentModel.DataAnnotations;

namespace Cerberus.Web.ViewModel.PrivatePerson
{
    public class PlaceOfBirthViewModel
    {
        [Display(Name = "محل تولد")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(30, ErrorMessage = "{0} نمی تواند بیشتر از 30 کارکتر باشد")]
        public string PlaceOfBirth { get; set; }
    }
}
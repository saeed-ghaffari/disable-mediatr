﻿using Cerberus.Domain.Enum;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Web.ViewModel.LegalPersonShareholder
{
    public class ShareholderViewModel
    {
        public long Id { get; set; }

        [Display(Name = "کد ملی / کد فراگیر")]
        public string UniqueIdentifier { get; set; }

        [Display(Name = "نام")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }

        [Display(Name = "کد پستی")]
        public string PostalCode { get; set; }

        [Display(Name = "نشانی")]
        public string Address { get; set; }

        [Display(Name = "سمت")]
        public StakHolderPositionType PositionType { get; set; }

        [Display(Name = "درصد سهام داری یا حق رای")]
        public int PercentageVotingRight { get; set; }
    }
}
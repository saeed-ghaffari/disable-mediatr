﻿using Cerberus.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Utility.Enum;

namespace Cerberus.Web.ViewModel.Dashboard
{
    public interface IDashboardBaseViewModel
    {

        long Mobile { get; set; }

        Carrier Carrier { get; set; }

        string Email { get; set; }

        string UniqueIdentifier { get; set; }

        ProfileOwnerType Type { get; set; }

        ProfileStatus Status { get; set; }

        string TraceCode { get; set; }

        IEnumerable<Domain.Model.Payment> Payments { get; set; }

        IEnumerable<Domain.Model.Address> Addresses { get; set; }

        IEnumerable<Domain.Model.TradingCode> TradingCodes { get; set; }

        IEnumerable<Domain.Model.BankingAccount> Accounts { get; set; }

        Domain.Model.JobInfo JobInfo { get; set; }

        Domain.Model.FinancialInfo FinancialInfo { get; set; }

    }
}

﻿using Cerberus.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Utility.Enum;

namespace Cerberus.Web.ViewModel.Dashboard
{
    public class LegalPersonDashboardViewModel:IDashboardBaseViewModel
    {
        public Domain.Model.LegalPerson LegalPerson { get; set; }

        public IEnumerable<Domain.Model.Agent> Agents { get; set; }
        public IList<Domain.Model.LegalPersonStakeholder> LegalPersonStakeholders { get; set; }
        public IList<Domain.Model.LegalPersonShareholder> LegalPersonShareholders { get; set; }

        public long Mobile { get; set; }
        public Carrier Carrier { get; set; }
        public string Email { get; set; }
        public string UniqueIdentifier { get; set; }
        public ProfileOwnerType Type { get; set; }
        public ProfileStatus Status { get; set; }
        public string TraceCode { get; set; }
        public IEnumerable<Domain.Model.Payment> Payments { get; set; }
        public IEnumerable<Domain.Model.Address> Addresses { get; set; }
        public IEnumerable<Domain.Model.TradingCode> TradingCodes { get; set; }
        public IEnumerable<Domain.Model.BankingAccount> Accounts { get; set; }
        public Domain.Model.JobInfo JobInfo { get; set; }
        public Domain.Model.FinancialInfo FinancialInfo { get; set; }
    }
}

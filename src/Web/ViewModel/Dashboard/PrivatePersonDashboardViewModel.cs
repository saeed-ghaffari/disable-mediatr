﻿using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using Cerberus.Utility.Enum;

namespace Cerberus.Web.ViewModel.Dashboard
{
    public class PrivatePersonDashboardViewModel : IDashboardBaseViewModel
    {
        public Domain.Model.Agent Agent { get; set; }
        public Domain.Model.PrivatePerson PrivatePerson { get; set; }
        public long Mobile { get; set; }
        public Carrier Carrier { get; set; }
        public string Email { get; set; }
        public string UniqueIdentifier { get; set; }
        public ProfileOwnerType Type { get; set; }
        public ProfileStatus Status { get; set; }
        public string TraceCode { get; set; }
        public IEnumerable<Domain.Model.Payment> Payments { get; set; }
        public IEnumerable<Domain.Model.Address> Addresses { get; set; }
        public IEnumerable<Domain.Model.TradingCode> TradingCodes { get; set; }
        public IEnumerable<Domain.Model.BankingAccount> Accounts { get; set; }
        public Domain.Model.JobInfo JobInfo { get; set; }
        public Domain.Model.FinancialInfo FinancialInfo { get; set; }
        public IEnumerable<IssuanceStockCode> IssuanceStockCodes { get; set; }
    }
}

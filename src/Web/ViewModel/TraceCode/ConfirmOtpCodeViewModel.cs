﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Web.ViewModel.TraceCode
{
    public class ConfirmOtpCodeViewModel
    {
        //public long Mobile { get; set; }
        public string UniqueIdentifier { get; set; }

        [Display(Name = "کد تایید")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string OtpCode  { get; set; }
    }
}

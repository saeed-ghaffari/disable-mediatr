﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Web.ViewModel.Mobile
{
    public class CheckMobileViewModel
    {
        [Display(Name = "شماره تلفن همراه جدید ")]
        [Required(ErrorMessage = "{0} اجباری است ")]
        [RegularExpression(@"9(0[1-5]|1[0-9]|3[0-9]|2[0-9]|9[0-9])-?[0-9]{3}-?[0-9]{4}", ErrorMessage = "لطفا شماره تلفن همراه خود را صحیح وارد نمایید")]
        [MinLength(10, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(10, ErrorMessage = "{0}  معتبر نیست")]
        public string Msisdn { get; set; }


        [Display(Name = "کد ملی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [Utility.UniqueIdentifierValidation(ErrorMessage = "{0} اشتباه است")]
        public string UniqueIdentifier { get; set; }
    }
}

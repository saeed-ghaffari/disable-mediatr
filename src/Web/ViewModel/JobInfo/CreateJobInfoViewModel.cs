﻿using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.CustomValidation;

namespace Cerberus.Web.ViewModel.JobInfo
{
    public class CreateJobInfoViewModel
    {

        [Display(Name = "عنوان شغل")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public long? JobId { get; set; }


        [Display(Name = "نام شغل")]
        [MaxLength(60, ErrorMessage = "{0} معتبر نیست")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        public string JobDescription { get; set; }

        [Display(Name = "تاریخ شروع به کار")]
        [PersianMinDatetimeValidation(ErrorMessage = "{0} معتبر نیست")]
        public string EmploymentDate { get; set; }

        [Display(Name = "نام شرکت / موسسه")]
        [MaxLength(100, ErrorMessage = "{0} معتبر نیست")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        public string CompanyName { get; set; }

        [Display(Name = "آدرس شرکت / موسسه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MaxLength(300, ErrorMessage = "{0} نمی تواند بیشتر از 300 کارکتر باشد")]
        [RegularExpression(@"^\s*[-،.چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ0123456789,\s]+\s*$", ErrorMessage = "فقط استفاده از اعداد و حروف فارسی مجاز است")]
        public string CompanyAddress { get; set; }

        [Display(Name = "کد پستی شرکت / موسسه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MinLength(10, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(10, ErrorMessage = "{0}  معتبر نیست")]
        public string CompanyPostalCode { get; set; }

        [Required(ErrorMessage = "{0} اجباری است")]
        [Display(Name = "پست الکترونیکی شرکت / موسسه")]
        [EmailAddress(ErrorMessage = "{0}  معتبر نیست")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,20}$", ErrorMessage = "{0} نامعتبر است")]
        [MaxLength(100, ErrorMessage = "{0} معتبر نیست")]
        public string CompanyEmail { get; set; }

        [Display(Name = "آدرس وب سایت شرکت / موسسه")]
        [Url(ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(50, ErrorMessage = "{0} معتبر نیست")]
        public string CompanyWebSite { get; set; }

        [Display(Name = "پیش شماره ")]
        [Required(ErrorMessage = "اجباری است")]
        [RegularExpression(@"[0][1-9][1-9]$", ErrorMessage = "معتبر نیست")]
        [MinLength(3, ErrorMessage = " معتبر نیست")]
        [MaxLength(3, ErrorMessage = "معتبر نیست")]
        public string CompanyCityPrefix { get; set; }

        [Display(Name = "شماره تلفن شرکت / موسسه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MinLength(4, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(8, ErrorMessage = "{0}  معتبر نیست")]
        public string CompanyPhone { get; set; }

        [Display(Name = "سمت")]
        [MaxLength(50, ErrorMessage = "{0} معتبر نیست")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        public string Position { get; set; }

        [Display(Name = "پیش شماره دور نگار")]
        [RegularExpression(@"[0][1-9][1-9]$", ErrorMessage = "معتبر نیست")]
        [MinLength(3, ErrorMessage = "معتبر نیست")]
        [MaxLength(3, ErrorMessage = "معتبر نیست")]
        public string CompanyFaxPrefix { get; set; }


        [Display(Name = "دور نگار")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MinLength(8, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(8, ErrorMessage = "{0}  معتبر نیست")]
        public string CompanyFax { get; set; }

    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace Cerberus.Web.ViewModel.Profile
{
    public  class VerificationOtpAgentResponseViewModel
    {
        [Display(Name = "شماره موبایل")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MinLength(10)]
        public string Mobile { get; set; }

        [Display(Name = "کد تایید")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string Otp { get; set; }

        [Required]
        public string SessionKey { get; set; }
    }
}
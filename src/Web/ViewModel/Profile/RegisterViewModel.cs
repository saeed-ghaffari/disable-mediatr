﻿using System.ComponentModel.DataAnnotations;

namespace Cerberus.Web.ViewModel.Profile
{
    public class RegisterViewModel
    {
        [Display(Name ="شماره تلفن همراه")]
        [Required(ErrorMessage = "{0} اجباری است ")]
        [RegularExpression(@"9(0[1-5]|1[0-9]|3[0-9]|2[0-9]|9[0-9])-?[0-9]{3}-?[0-9]{4}", ErrorMessage ="لطفا شماره تلفن همراه خود را صحیح وارد نمایید")]
        [MinLength(10, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(10, ErrorMessage = "{0}  معتبر نیست")]
        public string Mobile { get; set; }

        public string CountryCode { get; set; } = "+98";
        public string LinkAddress { get; set; }
    }
}
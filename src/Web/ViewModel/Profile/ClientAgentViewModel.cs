﻿using Cerberus.Domain.Enum;

namespace Cerberus.Web.ViewModel.Profile
{
    public class ClientAgentViewModel
    {
        public string UniqueIdentifier { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Mobile { get; set; }
        public AgentType AgentType { get; set; }
        public long Id { get; set; }
    }
}
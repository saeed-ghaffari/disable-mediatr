﻿namespace Cerberus.Web.ViewModel.Profile
{
    public class VerificationOtpResponseViewModel
    {
        public string Token { get; set; }
    }
}
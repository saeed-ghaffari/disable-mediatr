﻿using System.ComponentModel.DataAnnotations;

namespace Cerberus.Web.ViewModel.Profile
{
    public class CheckAgentExistViewModel
    {
        [Display(Name = "کد ملی /شناسه ملی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string UniqueIdentifier { get; set; }
    }
}
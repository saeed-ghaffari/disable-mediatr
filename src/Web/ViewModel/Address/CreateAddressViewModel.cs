﻿using Cerberus.Web.ViewModel.ViewComponents;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Web.ViewModel.Address
{
    public class CreateAddressViewModel
    {

        [Display(Name = "کد پستی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MinLength(10, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(10, ErrorMessage = "{0}  معتبر نیست")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        public string PostalCode { get; set; }

        [Display(Name = "کشور")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public string CountryId { get; set; }

        [Display(Name = "استان")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public string ProvinceId { get; set; }

        [Display(Name = "شهر")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public string CityId { get; set; }

        [Display(Name = "بخش")]
        [MinLength(1)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public string SectionId { get; set; }

        [Display(Name = "خیابان")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MinLength(1, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(100, ErrorMessage = "{0}  معتبر نیست")]
        //[RegularExpression("[^0-9]*$", ErrorMessage = "استفاده از عدد مجاز نمی باشد")]
        [RegularExpression(@"^\s*[-،.چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀ ك آ0123456789,\s]+\s*$", ErrorMessage = "فقط استفاده از اعداد و حروف فارسی مجاز است")]
        public string RemnantAddress { get; set; }


        [Display(Name = "کوچه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MinLength(3, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(50, ErrorMessage = "{0}  معتبر نیست")]
        //[RegularExpression("[^0-9]*$", ErrorMessage = "استفاده از عدد مجاز نمی باشد")]
        [RegularExpression(@"^\s*[-،.چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀ ك آ0123456789,\s]+\s*$", ErrorMessage = "فقط استفاده از اعداد و حروف فارسی مجاز است")]
        public string Alley { get; set; }

        [Display(Name = "پلاک")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MinLength(1, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(30, ErrorMessage = "{0}  معتبر نیست")]
        //[RegularExpression("[^0-9]*$", ErrorMessage = "استفاده از عدد مجاز نمی باشد")]
        [RegularExpression(@"^\s*[-،.چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀ ك آ0123456789,\s]+\s*$", ErrorMessage = "فقط استفاده از اعداد و حروف فارسی مجاز است")]
        public string Plaque { get; set; }

        [Display(Name = "پیش شماره شهر")]
        [Required(ErrorMessage = "اجباری است")]
        [RegularExpression(@"[0][1-9][1-9]$", ErrorMessage = "معتبر نیست")]
        [MinLength(3, ErrorMessage = "معتبر نیست")]
        [MaxLength(3, ErrorMessage = "معتبر نیست")]
        public string CityPrefix { get; set; }

        [Display(Name = "شماره تلفن ثابت")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MinLength(8, ErrorMessage = "{0}  نمی تواند کمتر از 8 عدد باشد")]
        [MaxLength(8, ErrorMessage = "{0}  نمی تواند بیشتر از 8 عدد باشد")]
        public string Tel { get; set; }

        [Display(Name = "پیش شماره کشور تلفن همراه اضطراری")]
        [RegularExpression("^\\d*$", ErrorMessage = "{0}  معتبر نیست")]
        [MinLength(1, ErrorMessage = "معتبر نیست")]
        [MaxLength(4, ErrorMessage = "معتبر نیست")]
        public string CountryPrefix { get; set; }

        //TODO: Change To Emergency Mobile
        [Display(Name = "شماره موبایل")]
        [RegularExpression(@"9(0[1-5]|1[0-9]|3[0-9]|2[0-9]|9[0-9])-?[0-9]{3}-?[0-9]{4}", ErrorMessage = "لطفا شماره تلفن همراه خود را صحیح وارد نمایید")]
        [MinLength(10, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(10, ErrorMessage = "{0}  معتبر نیست")]
        public string Mobile { get; set; }

        [Display(Name = "شماره تلفن ثابت اضطراری")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  فقط عدد مجاز است")]
        [MinLength(8, ErrorMessage = "{0}  نمی تواند کمتر از 8 عدد باشد")]
        [MaxLength(8, ErrorMessage = "{0}  نمیتواند بیشتر از 8 عدد باشد")]
        public string EmergencyTel { get; set; }

        [Display(Name = " پیش شماره تلفن ثابت اضطراری")]
        [RegularExpression(@"[0][1-9][1-9]$", ErrorMessage = "معتبر نیست")]
        [MinLength(3, ErrorMessage = "معتبر نیست")]
        [MaxLength(3, ErrorMessage = "معتبر نیست")]
        public string EmergencyTelCityPrefix { get; set; }

        [Display(Name = " پیش شماره کشور اضطراری")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  فقط عدد مجاز است")]
        [MinLength(2, ErrorMessage = "معتبر نیست")]
        [MaxLength(4, ErrorMessage = "معتبر نیست")]
        public string EmergencyTelCountryPrefix { get; set; } //not used

        [Display(Name = " پیش شماره فکس")]
        [RegularExpression(@"[0][1-9][1-9]$", ErrorMessage = "معتبر نیست")]
        [MinLength(3, ErrorMessage = "معتبر نیست")]
        [MaxLength(3, ErrorMessage = "معتبر نیست")]
        public string FaxPrefix { get; set; }

        [Display(Name = "دورنگار")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MinLength(8, ErrorMessage = "{0}  نمی تواند کمتر از 8 عدد باشد")]
        [MaxLength(8, ErrorMessage = "{0}  نمی تواند بیشتر از 8 عدد باشد")]
        public string Fax { get; set; }

        [Display(Name = "تارنما")]
        [Url(ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(100, ErrorMessage = "{0}  معتبر نیست")]
        public string Website { get; set; }

        [Display(Name = " پست الکترونیکی")]
        [EmailAddress(ErrorMessage = "{0}  معتبر نیست")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,20}$", ErrorMessage = "{0} نامعتبر است")]
        [MaxLength(60, ErrorMessage = "{0}  معتبر نیست")]
        public string Email { get; set; }

        public string ServicePostalCode { get; set; }
        public bool? IsConfirmPostalCode { get; set; }

        public IEnumerable<DropdownItemViewModel> CountryItems { get; set; }

        public IEnumerable<DropdownItemViewModel> ProvinceItems { get; set; }

        public IEnumerable<DropdownItemViewModel> CityItems { get; set; }

        public IEnumerable<DropdownItemViewModel> SectionItems { get; set; }



        [Display(Name = "کد پستی")]
        [MinLength(10, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(10, ErrorMessage = "{0}  معتبر نیست")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        public string LegalPostalCode { get; set; }

        [Display(Name = "کشور")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public string LegalCountryId { get; set; }

        [Display(Name = "استان")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public string LegalProvinceId { get; set; }

        [Display(Name = "شهر")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public string LegalCityId { get; set; }

        [Display(Name = "بخش")]
        [MinLength(1)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public string LegalSectionId { get; set; }

        [Display(Name = "خیابان")]
        [MinLength(1, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(100, ErrorMessage = "{0}  معتبر نیست")]
        //[RegularExpression("[^0-9]*$", ErrorMessage = "استفاده از عدد مجاز نمی باشد")]
        [RegularExpression(@"^\s*[-،.چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ0123456789,\s]+\s*$", ErrorMessage = "فقط استفاده از اعداد و حروف فارسی مجاز است")]
        public string LegalRemnantAddress { get; set; }

        [Display(Name = "کوچه")]

        [MinLength(1, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(50, ErrorMessage = "{0}  معتبر نیست")]
        //[RegularExpression("[^0-9]*$", ErrorMessage = "استفاده از عدد مجاز نمی باشد")]
        [RegularExpression(@"^\s*[-،.چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ0123456789,\s]+\s*$", ErrorMessage = "فقط استفاده از اعداد و حروف فارسی مجاز است")]
        public string LegalAlley { get; set; }

        [Display(Name = "پلاک")]
        [MinLength(1, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(30, ErrorMessage = "{0}  معتبر نیست")]
        //[RegularExpression("[^0-9]*$", ErrorMessage = "استفاده از عدد مجاز نمی باشد")]
        [RegularExpression(@"^\s*[-،.چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ0123456789,\s]+\s*$", ErrorMessage = "فقط استفاده از اعداد و حروف فارسی مجاز است")]
        public string LegalPlaque { get; set; }

        [Display(Name = "پیش شماره شهر")]
        [RegularExpression(@"[0][1-9][1-9]$", ErrorMessage = "معتبر نیست")]
        [MinLength(3, ErrorMessage = "معتبر نیست")]
        [MaxLength(3, ErrorMessage = "معتبر نیست")]
        public string LegalCityPrefix { get; set; }

        [Display(Name = "شماره تلفن ثابت")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MinLength(8, ErrorMessage = "{0}  نمی تواند کمتر از 8 عدد باشد")]
        [MaxLength(8, ErrorMessage = "{0}  نمی تواند بیشتر از 8 عدد باشد")]
        public string LegalTel { get; set; }

        public IEnumerable<DropdownItemViewModel> LegalCountryItems { get; set; }

        public IEnumerable<DropdownItemViewModel> LegalProvinceItems { get; set; }

        public IEnumerable<DropdownItemViewModel> LegalCityItems { get; set; }

        public IEnumerable<DropdownItemViewModel> LegalSectionItems { get; set; }



    }
}
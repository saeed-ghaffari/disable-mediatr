﻿using System.ComponentModel.DataAnnotations;
using Cerberus.Web.WebLogic.CustomAttribute;

namespace Cerberus.Web.ViewModel.Agent
{
    public class CheckAgentViewModel
    {
        [Display(Name = "کد ملی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [NationalCode(ErrorMessage = "{0} معتبر نمی باشد")]
        [MaxLength(10, ErrorMessage = "{0} معتبر نیست")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public string UniqueIdentifier { get; set; }

        [Display(Name = "کد تایید")]
        [MaxLength(5, ErrorMessage = "{0} معتبر نیست")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public string OtpCode { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Web.ViewModel.Agent
{
    public class DeleteAgentViewModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}

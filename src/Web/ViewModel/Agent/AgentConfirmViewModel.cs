﻿using System.ComponentModel.DataAnnotations;

namespace Cerberus.Web.ViewModel.Agent
{
    public class AgentConfirmViewModel
    {
        [Required(ErrorMessage = "کد ملی اجباری است")]
        //TODO:Check UniqueIdentifier Validation
        public string UniqueIdentifier { get; set; }

        [Required(ErrorMessage = "کد تایید اجباری است")]
        public string Otp { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.Enum;

namespace Cerberus.Web.ViewModel.TradingCode
{
    public class CreateTradingCodeViewModel
    {
        [Display(Name = "نام بورس / بازار خارج از بورس")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [Range((int)(TradingType.Energy), (int)(TradingType.StockExchange), ErrorMessage = "مقدار نامعتبر است")]
        public TradingType Type { get; set; }

        [Display(Name = "کد سهام داری بخش اول")]
        [MaxLength(1, ErrorMessage = "معتبر نیست")]
        [RegularExpression(@"^\s*[مدصختک\s]+\s*$", ErrorMessage = "معتبر نیست")]
        public string FirstPart { get; set; }

        [Display(Name = "کد سهام داری")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        public string SecondPart { get; set; }

        [Display(Name = "کد سهامداری")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "معتبر نیست")]
        [MaxLength(3, ErrorMessage = "معتبر نیست")]
        public string ThirdPart { get; set; }

        public  List<TradingCodeViewModel> TradingCodes { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.Enum;
using Cerberus.Utility;

namespace Cerberus.Web.ViewModel.TradingCode
{
    public class TradingCodeViewModel
    {
        public long Id { get; set; }

        [Display(Name = "نام بورس")]
        [Required(ErrorMessage = "{0}اجباری است")]
        public TradingType Type { get; set; }

        public string TypeName => Type.GetEnumDescription();

        [Display(Name = "کد سهام داری")]
        public string FirstPart { get; set; }

        [Display(Name = "کد سهام داری")]
        public string SecondPart { get; set; }

        [Display(Name = "کد سهام داری")]
        public string ThirdPart { get; set; }
    }
}

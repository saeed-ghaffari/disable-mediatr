﻿using System;
using Cerberus.Domain.Enum;
using Cerberus.Web.Enum;

namespace Cerberus.Web.ViewModel.Payment
{
   public class PaymentResponse
    {
        public string OrderId { get; set; }
        public Guid ServiceId { get; set; }
        public PaymentResultCode ResultCode { get; set; }
        public string Token { get; set; }
    }
}

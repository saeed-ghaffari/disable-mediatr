﻿namespace Cerberus.Web.ViewModel.Payment
{
    public class MellatCallbackBody
    {
        public long SaleReferenceId { get; set; }
        public long SaleOrderId { get; set; }
        public string RefId { get; set; }
        public long ResCode { get; set; }
    }
}

    
﻿namespace Cerberus.Web.ViewModel.Payment
{
    public class DecryptAsanResponse
    {
        public long SaleReferenceId { get; set; }
        public long SaleOrderId { get; set; }
        public string RefId { get; set; }
        public string Description { get; set; }
        public bool IsSuccess { get; set; }
     
    }
}
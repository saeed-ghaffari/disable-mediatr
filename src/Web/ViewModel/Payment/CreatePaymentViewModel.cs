﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Cerberus.Domain.Enum;
using Cerberus.Utility;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Cerberus.Web.ViewModel.Payment
{
    public class CreatePaymentViewModel
    {

        public PaymentGateway Gateway { get; set; }
        public long Amount { get; set; }
        
        public string Message { get; set; }
        public long ServiceId { get; set; }

        [Required(ErrorMessage = "نوع پکیج اجباری است")]
        public PrimitivePackageType? PrimitivePackageType { get; set; }
        public string  UniqueIdentifier { get; set; }

    }
}

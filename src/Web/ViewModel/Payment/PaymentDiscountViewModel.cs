﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Web.ViewModel.Payment
{
    public class PaymentDiscountViewModel
    {
        public CreatePaymentViewModel CreatePaymentViewModel { get; set; }
        public long Amount { get; set; }
        public long Discount { get; set; }
        public long DefaultValuePay { get; set; }



    }
}

﻿using Cerberus.Domain.Enum;

namespace Cerberus.Web.ViewModel.Payment
{
    public class UpdatePaymentViewModel
    {
        public long Id { get; set; }
        public long ProfileId { get; set; }

        public PaymentGateway Gateway { get; set; }

        public PaymentStatus Status { get; set; }

        public string ReferenceNumber { get; set; }

        public long SaleReferenceId { get; set; }

    }
}
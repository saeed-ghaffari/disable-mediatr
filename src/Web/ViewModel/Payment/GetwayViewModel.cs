﻿using Cerberus.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Web.ViewModel.Payment
{
    public class GatewayViewModel

    {
        public string Url { get; set; }
        public string CallBackUrl { get; set; }
        public string Token { get; set; }
        public string RefId { get; set; }
        public PaymentGateway PaymentGateway { get; set; }
}
}

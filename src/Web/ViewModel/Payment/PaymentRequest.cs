﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Web.ViewModel.Payment
{
    public class PaymentRequest
    {
        public string Url{ get; set; }
        public string Token { get; set; }
    }
}

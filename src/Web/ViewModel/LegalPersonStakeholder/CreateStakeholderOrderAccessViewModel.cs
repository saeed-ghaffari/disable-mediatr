﻿using Cerberus.Domain.Enum;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cerberus.Web.WebLogic.ModelBinder;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Web.ViewModel.LegalPersonStakeholder
{
    public class CreateStakeholderOrderAccessViewModel
    {
        //TODO:Check UniqueIdentifier Is true

        [Display(Name = "کد ملی / کد فراگیر")]
        [Required(ErrorMessage = " {0} اجباری است")]
        public string UniqueIdentifier { get; set; }

        [Display(Name = "تاریخ تولد")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string BirthDate { get; set; } = null;

        [Display(Name = "نام")]
        [Required(ErrorMessage = " {0} اجباری است")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = " {0} اجباری است")]
        public string LastName { get; set; }

        [Display(Name = "شروع دوره تصدی")]
        [Required(ErrorMessage = " {0} اجباری است")]
       
        public string StartAt { get; set; }

        [Display(Name = "پایان دوره تصدی")]
        [Required(ErrorMessage = " {0} اجباری است")]
       
        public string EndAt { get; set; }

        [Display(Name = "سمت")]
        [Required(ErrorMessage = " {0} اجباری است")]
        [Range((byte)(StakHolderPositionType.Chairman), (byte)(StakHolderPositionType.Auditor), ErrorMessage = "مقدار نامعتبر است")]
        public StakHolderPositionType PositionType { get; set; }

        [Display(Name = "امضاء")]
        [Required(ErrorMessage = "لطفا فایل امضا را بارگزاری نمایید")]
        public IFormFile SignatureFile { get; set; }

        public IEnumerable<StakeholderOrderAccessViewModel> Stakeholders { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using Cerberus.Domain.Enum;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Cerberus.Web.ViewModel.LegalPersonStakeholder
{
    public class CreateStakeholderManagerViewModel
    {
        //TODO:Check UniqueIdentifier Is true

        [Display(Name = "کد ملی / کد فراگیر")]
        [Required(ErrorMessage = " {0} اجباری است")]
        public string UniqueIdentifier { get; set; }

        [Display(Name = "نام")]
        [Required(ErrorMessage = " {0} اجباری است")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = " {0} اجباری است")]
        public string LastName { get; set; }

        [Display(Name = "سمت")]
        [Required(ErrorMessage = " {0} اجباری است")]

        [Range((byte)(StakHolderPositionType.Chairman), (byte)(StakHolderPositionType.Auditor), ErrorMessage = "مقدار نامعتبر است")]
        public StakHolderPositionType PositionType { get; set; }

        [Display(Name = "صاحب امضاء")]
        public bool IsOwnerSignature { get; set; }
        [Display(Name = "تاریخ تولد")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string BirthDate { get; set; } = null;


        [Display(Name = "امضاء")]
        public IFormFile SignatureFile { get; set; }


        public IEnumerable<StakeholderManagerViewModel> Stakeholders { get; set; }
    }
}
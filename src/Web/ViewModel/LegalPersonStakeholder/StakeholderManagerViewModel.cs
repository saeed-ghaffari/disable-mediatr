﻿using Cerberus.Domain.Enum;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Web.ViewModel.LegalPersonStakeholder
{
    public class StakeholderManagerViewModel
    {
        public long Id { get; set; }

        [Display(Name = "کد ملی / کد فراگیر")]
        public string UniqueIdentifier { get; set; }

        [Display(Name = "نام")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }

        [Display(Name = "سمت")]
        public StakHolderPositionType PositionType { get; set; }

        [Display(Name = "صاحب امضاء است ؟")]
        public bool IsOwnerSignature { get; set; }

        public string SignatureFile { get; set; }


    }
}
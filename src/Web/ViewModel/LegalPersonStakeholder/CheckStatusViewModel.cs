﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;

namespace Cerberus.Web.ViewModel.LegalPersonStakeholder
{
    public class CheckStatusViewModel
    {
        public string UniqueIdentifier { get; set; }
        public string BirthDate { get; set; }
        public string Type { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services;
using Cerberus.Web.Filters;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;

namespace Cerberus.Web.Controllers
{
    [AuthorizeUserFilterFactory]
    public class CountryController : BaseController
    {
        private readonly ICountryService _countryService;

        public CountryController(ICountryService countryService)
        {
            _countryService = countryService;
        }

        [Route("api/countries")]
        [HttpGet]
        public async Task<IEnumerable<SelectListItem>> GetSelectListAsync()
        {
            return (await _countryService.GetListAsync()).Select(x => new SelectListItem(x.Name, x.Id.ToString()));
        }
    }
}
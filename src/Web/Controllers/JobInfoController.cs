﻿using System.Linq;
using AutoMapper;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Web.Enum;
using Cerberus.Web.Filters;
using Cerberus.Web.ViewModel.JobInfo;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Cerberus.Utility;
using Cerberus.Web.Extensions;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Cerberus.Web.Controllers
{
    [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.JobInfo)]
    public class JobInfoController : BaseController
    {
        private readonly IJobInfoService _jobInfoService;
        private readonly IJobService _jobService;
        private readonly IMapper _mapper;


        public JobInfoController(IJobInfoService jobInfoService, IJobService jobService, IMapper mapper)
        {
            _jobInfoService = jobInfoService;
            _jobService = jobService;
            _mapper = mapper;
        }

        [HttpGet]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Index()
        {
            var jobInfoModel = await _jobInfoService.GetByProfileIdAsync(User.ProfileId);

            var createJobInfoViewModel = _mapper.Map<CreateJobInfoViewModel>(jobInfoModel) ?? new CreateJobInfoViewModel();

            ViewBag.Jobs = (await _jobService.GetListAsync()).Select(x => new SelectListItem(x.Title, x.Id.ToString()));

            return View(createJobInfoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateAsync(CreateJobInfoViewModel jobInfoViewModel)
        {
            var joblist = (await _jobService.GetListAsync()).Select(x => new SelectListItem(x.Title, x.Id.ToString()));

            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                ViewBag.Jobs = joblist;
                return View("Index", jobInfoViewModel);
            }


            if (joblist.All(x => x.Value.ToLong() != jobInfoViewModel.JobId))
            {
                Alert("درخواست شما غیر مجاز است", NotificationType.Error);
                return RedirectToAction("Index");
            }

            if (jobInfoViewModel.JobId == 123159 && string.IsNullOrEmpty(jobInfoViewModel.JobDescription))
            {
                Alert("در صورت انتخاب شغل سایر، وارد کردن نام شغل اجباری می باشد.", NotificationType.Error);
                ViewBag.jobs = joblist;
                return View("Index", jobInfoViewModel);
            }

            if (jobInfoViewModel.JobId != 123159) jobInfoViewModel.JobDescription = null;

            var jobInfoModel = _mapper.Map<JobInfo>(jobInfoViewModel);

            var dbObject
                = await _jobInfoService.GetByProfileIdAsync(User.ProfileId);

            jobInfoModel.ProfileId = User.ProfileId;

            if (dbObject != null)
            {
                jobInfoModel.Id = dbObject.Id;
                jobInfoModel.CreationDate = dbObject.CreationDate;
                await _jobInfoService.UpdateAsync(jobInfoModel);
            }
            else
            {
                await _jobInfoService.CreateAsync(jobInfoModel);
            }


            return RedirectToNextStep(CurrentStep.JobInfo);
        }
    }
}
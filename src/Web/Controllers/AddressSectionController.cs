﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services;
using Cerberus.Web.Filters;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Cerberus.Web.Controllers
{
    public class AddressSectionController : BaseController
    {
        private readonly IAddressSectionService _addressSectionService;

        public AddressSectionController(IAddressSectionService addressSectionService)
        {
            _addressSectionService = addressSectionService;
        }


        [Route("api/addressSections/{cityId}")]
        [HttpGet]
        [AuthorizeUserFilterFactory]
        public async Task<IEnumerable<SelectListItem>> GetSelectListAsync(long cityId)
        {
            return (await _addressSectionService.GetListAsync(cityId)).Select(x => new SelectListItem(x.Name, x.Id.ToString()));
        }
    }
}
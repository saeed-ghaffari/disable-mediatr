﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Web.Enum;
using Cerberus.Web.Filters;
using Cerberus.Web.ViewModel.Dashboard;
using Cerberus.Web.WebLogic;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Web.Controllers
{
    public class DashboardController : BaseController
    {
        private readonly IProfileService _profileService;
        private readonly IMapper _mapper;

        public DashboardController(IProfileService profileService, IMapper mapper)
        {
            _profileService = profileService;
            _mapper = mapper;
        }

        [HttpGet("dashboard")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Dashboard)]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Index()
        {
            var result = await _profileService.GetRelatedDataForDashboard(User.ProfileId, User.ProfileOwnerType);
            switch (User.ProfileOwnerType)
            {
                case ProfileOwnerType.IranianPrivatePerson:

                    var privatePersonViewModel = _mapper.Map<Domain.Model.Profile, PrivatePersonDashboardViewModel>(result);
                    return View("PrivatePersonDashboard", privatePersonViewModel);

                case ProfileOwnerType.IranianLegalPerson:   

                    var legalPersonViewModel = _mapper.Map<Domain.Model.Profile, LegalPersonDashboardViewModel>(result);
                    return View("LegalPersonDashboard", legalPersonViewModel);

                case ProfileOwnerType.ForeignPrivatePerson:
                    break;
                case ProfileOwnerType.ForeignLegalPerson:
                    break;
            }

            
            throw new NotImplementedException();
        }
    }
}
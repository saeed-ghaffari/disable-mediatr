﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services;
using Cerberus.Web.Filters;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Cerberus.Web.Controllers
{
    public class ProvinceController : BaseController
    {
        private readonly IProvinceService _provinceService;

        public ProvinceController(IProvinceService provinceService)
        {
            _provinceService = provinceService;
        }


        [Route("api/provinces/{countryId}")]
        [HttpGet]
        [AuthorizeUserFilterFactory]
        public async Task<IEnumerable<SelectListItem>> GetSelectListAsync(long countryId)
        {
            return (await _provinceService.GetListAsync(countryId)).OrderBy(x=>x.Name).Select(x => new SelectListItem(x.Name, x.Id.ToString()));
        }
    }
}
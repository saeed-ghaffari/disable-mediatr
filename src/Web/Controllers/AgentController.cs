﻿using AutoMapper;
using Cerberus.Domain.Interface.Services;
using Cerberus.Web.ViewModel.Agent;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Cerberus.Service;
using Cerberus.Web.Configuration;
using Cerberus.Web.Enum;
using Cerberus.Web.Extensions;
using Cerberus.Web.Filters;
using Gatekeeper.Core.Interface;
using Microsoft.Extensions.Options;
using Cerberus.Domain.Model;
using System.IO;
using System.Linq;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Utility;
using Gatekeeper.AspModules.ActionFilter;
using File = Cerberus.Domain.Model.File;
using Profile = Cerberus.Domain.Model.Profile;

namespace Cerberus.Web.Controllers
{

    public class AgentController : BaseController
    {
        private readonly IProfileService _profileService;
        private readonly IAgentService _agentService;
        private readonly IAgentDocumentService _agentDocumentService;
        private readonly IOtpService _otpService;
        private readonly IMessagingService _messagingService;
        private readonly WebConfiguration _webConfiguration;
        private readonly MessagingConfiguration _messagingConfiguration;
        private readonly IMapper _mapper;
        private readonly FileStorageConfiguration _fileStorageConfiguration;
        private readonly IFileService _fileService;
        private readonly IProfileHistoryService _profileHistoryService;
        private readonly IOfflineTaskService _offlineTaskService;
        private readonly IPrivatePersonService _privatePersonService;


        public AgentController(
            IProfileService profileService,
            IAgentService agentService,
            IAgentDocumentService agentDocumentService,
            IOtpService otpService,
            IMessagingService messagingService,
            IOptions<WebConfiguration> webConfiguration,
            IOptions<MessagingConfiguration> messagingConfiguration,
            IMapper mapper,
            IFileService fileService,
            IOptions<FileStorageConfiguration> fileStorageConfiguration,
            IProfileHistoryService profileHistoryService,
            IOfflineTaskService offlineTaskService, IPrivatePersonService privatePersonService)

        {
            _profileService = profileService;
            _agentService = agentService;
            _agentDocumentService = agentDocumentService;
            _otpService = otpService;
            _messagingService = messagingService;
            _webConfiguration = webConfiguration.Value;
            _messagingConfiguration = messagingConfiguration.Value;
            _mapper = mapper;
            _fileStorageConfiguration = fileStorageConfiguration.Value;
            _fileService = fileService;
            _profileHistoryService = profileHistoryService;
            _offlineTaskService = offlineTaskService;
            _privatePersonService = privatePersonService;
        }

        [HttpGet]
        [Route("Agent/")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Agent)]
        [RateLimitFilterFactory(Order = 1, Limit = 30, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Index()
        {
            var tmp = await _agentService.GetByProfileIdAsync(User.ProfileId);
            if (tmp != null)
            {
                var agentViewModel = new CreateAgentViewModel()
                {
                    IsConfirmed = tmp.IsConfirmed,
                    Description = tmp.Description,
                    Id = tmp.Id,
                    Type = tmp.Type,
                    FirstName = tmp.AgentProfile.PrivatePerson.FirstName,
                    LastName = tmp.AgentProfile.PrivatePerson.LastName,
                    UniqueIdentifier = tmp.AgentProfile.UniqueIdentifier

                };
                return View(agentViewModel);
            }
            return View();

        }

        [HttpPost]
        [Route("api/createOtp")]
        [ValidateAntiForgeryToken]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Agent)]
        [RateLimitFilterFactory(Order = 1, Limit = 5, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<JsonResult> CreateOtpAsync(CheckAgentViewModel model)
        {

            if (!ModelState.IsValid)
                return Json(new { IsSuccess = false, Message = ModelState.ToErrorMessage() });


            var currentAgent = await _agentService.GetByAgentProfileIdAsync(User.ProfileId);
            if (currentAgent != null)
                return Json(new { IsSuccess = false, Message = "شما خودتان نماینده هستید و نمی توانید برای خود نماینده انتخاب کنید" });

            var profileModel = await _profileService.GetRelatedDataWithEntity(null, model.UniqueIdentifier, EntityType.Agent);

            if (profileModel == null || profileModel.Status < ProfileStatus.TraceCode)
                return Json(new { IsSuccess = false, Message = "در حال حاضر شخصی با این کد ملی کد پیگیری نگرفته است برای تکمیل فرم ابتدا باید شخص نماینده کد پیگیری گرفته باشد." });

            if (profileModel.Agent != null)
                return Json(new { IsSuccess = false, Message = "کد ملی انتخابی نمی تواند نماینده قانونی انتخاب شود، زیرا کد ملی مذکور دارای نماینده است." });
            var profile = await _profileService.GetAsync(User.ProfileId);
            if (profile.UniqueIdentifier == model.UniqueIdentifier)
                return Json(new { IsSuccess = false, Message = "کد ملی انتخابی نمی تواند نماینده قانونی انتخاب شود." });


            var key = ConventionalHelper.CreateOtpKey(profileModel.Mobile.ToString(), OtpType.AgentValidator);
            var otpCode = await _otpService.GetAsync(key, _webConfiguration.SessionTtl);


            await _messagingService.SendOtpSmsAsync(null,
                profileModel.Mobile.ToString(),
                profileModel.Carrier,
                string.Format(_messagingConfiguration.ValidateAgentOtpMessage, otpCode, Environment.NewLine));



            return Json(new { IsSuccess = true, Message = "کد تایید به تلفن همراه نماینده انتخابی، با  موفقیت ارسال شد ." });
        }


        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Agent)]
        [RateLimitFilterFactory(Order = 1, Limit = 30, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> GetAsync()
        {
            var tmp = await _agentService.GetByProfileIdAsync(User.ProfileId);
            if (tmp != null)
            {
                var agentViewModel = new CreateAgentViewModel()
                {
                    UniqueIdentifier = tmp.AgentProfile.UniqueIdentifier

                };
                return View("Edit", agentViewModel);
            }
            return View("Edit");
        }


        [Route("Agent")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Agent)]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateAsync(CreateAgentViewModel agentViewModel)
        {
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                ViewBag.error = "hasError";
                return View("Index", agentViewModel);
            }


            if (agentViewModel.Type != AgentType.Province && string.IsNullOrEmpty(agentViewModel.ExpirationDate))
            {
                Alert("تاریخ انقضاء نمایندگی اجباری است", NotificationType.Warning);
                ViewBag.error = "hasError";
                return View("Index", agentViewModel);
            }
            if (agentViewModel.Type == AgentType.Province)
            {
                agentViewModel.ExpirationDate = null;
            }
            if (agentViewModel.AgentFiles.Count() > 8)
            {
                Alert("تعداد فایل های ارسالی بیش از حد مجاز است . ", NotificationType.Warning);
                ViewBag.error = "hasError";
                return View("Index", agentViewModel);
            }


            var agentProfile = await _profileService.GetByUniqueIdentifierAsync(agentViewModel.UniqueIdentifier);
            if (agentProfile == null || agentProfile.Status < ProfileStatus.TraceCode)
            {
                Alert("در حال حاضر شخصی با این کد ملی کد پیگیری نگرفته است برای تکمیل فرم ابتدا باید شخص نماینده کد پیگیری گرفته باشد", NotificationType.Warning);
                ViewBag.error = "hasError";
                return View("Index", agentViewModel);
            }

            var validateOtpStatus = await _otpService.
               IsValidAsync(ConventionalHelper.CreateOtpKey(agentProfile.Mobile.ToString(), OtpType.AgentValidator),
                   agentViewModel.OtpCode, false);
            if (!validateOtpStatus)
            {
                Alert("کد تایید اشتباه است", NotificationType.Error);
                ViewBag.error = "hasError";
                return View("Index", agentViewModel);
            }


            var setDeleteDate = DateTime.Now;
            var profile = await _profileService.GetRelatedDataForAgentAsync(User.ProfileId);
            if (profile.Status > ProfileStatus.TraceCode)
            {
                Alert("بعد از سجامی شدن، شما نمی توانید نماینده اضافه نمایید .", NotificationType.Warning);
                ViewBag.error = "hasError";
                return View("Index", agentViewModel);
            }
            if (profile.Agent != null)
            {
                Alert("شما نمی توانید دو نماینده داشته باشید.", NotificationType.Warning);
                ViewBag.error = "hasError";
                return View("Index", agentViewModel);
            }

            if (agentProfile.UniqueIdentifier == profile.UniqueIdentifier)
            {
                Alert("شما نمی توانید نماینده خودتان شوید.", NotificationType.Warning);
                ViewBag.error = "hasError";
                return View("Index", agentViewModel);
            }

            var agentPrivatePerson = await _privatePersonService.GetByProfileIdAsync(agentProfile.Id);
            if (agentPrivatePerson.Gender == Gender.Female && agentViewModel.Type == AgentType.Province)
            {
                Alert("نماینده (ولی) شما معتبر نمی باشد", NotificationType.Warning);
                ViewBag.error = "hasError";
                return View("Index", agentViewModel);
            }
            if (Tools.IsUnder18Years(profile?.PrivatePerson?.BirthDate) && agentViewModel.Type == AgentType.Attorney)
            {
                Alert("بدلیل نرسیدن به سن قانونی، شما نمی توانید نوع نمایندگی را وکالت انتخاب کنید،لطفا گزینه های دیگر را انتخاب کنید .", NotificationType.Warning);
                ViewBag.error = "hasError";
                return View("Index", agentViewModel);
            }
            if (!Tools.IsUnder18Years(profile?.PrivatePerson?.BirthDate) && (agentViewModel.Type == AgentType.Province))
            {
                Alert("شما نمی توانید نوع نمایندگی را ولایت انتخاب کنید .", NotificationType.Warning);
                ViewBag.error = "hasError";
                return View("Index", agentViewModel);
            }

            var agent = _mapper.Map<CreateAgentViewModel, Agent>(agentViewModel);
            agent.ProfileId = User.ProfileId;
            agent.AgentProfileId = agentProfile.Id;
            agent.IsConfirmed = null;
            await _agentService.CreateAsync(agent);

            foreach (var item in agentViewModel.AgentFiles)
            {
                var cdnfileName = $"{Guid.NewGuid()}{Path.GetExtension(item.FileName)}";
                // convert to Domain File  ... 
                var file = new File
                {
                    CreationDate = DateTime.Now,
                    Type = FileType.Image,
                    Title = cdnfileName,
                    MimeType = item.ContentType,
                    FileName = cdnfileName

                };

                // the file path in this method will be replaced so empty string pass into
                try
                {
                    await _fileService.CreateAsync(file, item.ToByte(),
                        _fileStorageConfiguration.AgentFileSignaturePath);
                    await _agentDocumentService.CreateAsync(agent.Id, file);
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnsupportedMediaType)
                {
                    await DeleteAgentDocument(agent, setDeleteDate, null);

                    Alert("فایل نمایندگی دارای فرمت مناسب نمی باشد.", NotificationType.Warning);
                    ViewBag.error = "hasError";
                    return View("Index", agentViewModel);
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.ImageTooLarge)
                {
                    await DeleteAgentDocument(agent, setDeleteDate, null);
                    Alert("حجم هر فایل ارسالی باید بیشتر از 10 کیلوبایت و  کمتر از 2 مگابایت باشد،لطفا حجم فایل های خود را بررسی نمایید", NotificationType.Warning);
                    ViewBag.error = "hasError";
                    return View("Index", agentViewModel);
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.ImageTooSmall)
                {
                    await DeleteAgentDocument(agent, setDeleteDate, null);
                    Alert("حجم هر فایل ارسالی باید بیشتر از 10 کیلوبایت و  کمتر از 2 مگابایت باشد،لطفا حجم فایل های خود را بررسی نمایید", NotificationType.Warning);
                    ViewBag.error = "hasError";
                    return View("Index", agentViewModel);
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnknownServerError)
                {
                    await DeleteAgentDocument(agent, setDeleteDate, null);
                    Alert("در ذخیره فایل مشکلی بوجود آمده لطفا با پشتیبانی تماس بگیرید.", NotificationType.Warning);
                    ViewBag.error = "hasError";
                    return View("Index", agentViewModel);
                }

                catch (CerberusException)
                {
                    await DeleteAgentDocument(agent, setDeleteDate, null);
                    Alert("خطا در ذخیره اطلاعات", NotificationType.Warning);
                    ViewBag.error = "hasError";
                    return View("Index", agentViewModel);
                }
                catch (Exception)
                {
                    await DeleteAgentDocument(agent, setDeleteDate, null);
                    Alert("خطا در ذخیره اطلاعات", NotificationType.Warning);
                    ViewBag.error = "hasError";
                    return View("Index", agentViewModel);
                }

            }


            await _otpService.KillAsync(ConventionalHelper.CreateOtpKey(agentProfile.Mobile.ToString(), OtpType.AgentValidator));



            return RedirectToAction("Index");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Agent)]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> UpdateAsync(CreateAgentViewModel agentViewModel)
        {
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                return View("Edit", agentViewModel);
            }

            if (agentViewModel.Type != AgentType.Province && string.IsNullOrEmpty(agentViewModel.ExpirationDate))
            {
                Alert("تاریخ انقضاء نمایندگی اجباری است", NotificationType.Warning);
                return View("Edit", agentViewModel);
            }
            if (agentViewModel.Type == AgentType.Province)
            {
                agentViewModel.ExpirationDate = null;
            }
            if (agentViewModel.AgentFiles.Count() > 8)
            {
                Alert("تعداد فایل های ارسالی بیش از حد مجاز است . ", NotificationType.Warning);
                return View("Edit", agentViewModel);
            }
            var agentProfile = await _profileService.GetByUniqueIdentifierAsync(agentViewModel.UniqueIdentifier);
            if (agentProfile == null || agentProfile.Status < ProfileStatus.TraceCode)
            {
                Alert("در حال حاضر شخصی با این کد ملی کد پیگیری نگرفته است برای تکمیل فرم ابتدا باید شخص نماینده کد پیگیری گرفته باشد", NotificationType.Warning);
                return View("Edit", agentViewModel);
            }

            var validateOtpStatus = await _otpService.
               IsValidAsync(ConventionalHelper.CreateOtpKey(agentProfile.Mobile.ToString(), OtpType.AgentValidator),
                   agentViewModel.OtpCode, false);
            if (!validateOtpStatus)
            {
                Alert("کد تایید اشتباه است", NotificationType.Error);
                return View("Edit", agentViewModel);
            }

            var setDeleteDate = DateTime.Now;
            var profile = await _profileService.GetRelatedDataForAgentAsync(User.ProfileId);
            if (profile.Agent == null)
            {
                Alert("شما نمی توانید نماینده ثبت کنید", NotificationType.Error);
                return View("Edit", agentViewModel);
            }

            if (profile.Agent.IsConfirmed == null && profile.Status > ProfileStatus.PolicyAccepted)
            {
                Alert("تا مشخص نشدن وضعیت نماینده قبلی شما توسط شرکت سپرده گذاری، نمی توانید نماینده خود را ویرایش نمایید", NotificationType.Error);
                return View("Edit", agentViewModel);
            }

            if (profile.Status > ProfileStatus.TraceCode)
            {
                if (!profile.IsDeleteAndUpdateAgentSuspend())
                {
                    Alert("شما نمی توانید در حالت سجامی ناقص ، نماینده خود را وبرایش کنید", NotificationType.Error);
                    return View("Edit", agentViewModel);
                }
            }

            var agentPrivatePerson = await _privatePersonService.GetByProfileIdAsync(agentProfile.Id);
            if (agentPrivatePerson.Gender == Gender.Female && agentViewModel.Type == AgentType.Province)
            {
                Alert("نماینده (ولی) شما معتبر نمی باشد", NotificationType.Warning);
                return View("Edit", agentViewModel);
            }
            if (Tools.IsUnder18Years(profile?.PrivatePerson.BirthDate) && agentViewModel.Type == AgentType.Attorney)
            {
                Alert("به دلیل نرسیدن به سن قانونی، شما نمی توانید نوع نمایندگی را وکالت انتخاب کنید،لطفا گزینه های دیگر را انتخاب کنید .", NotificationType.Warning);
                return View("Edit", agentViewModel);
            }
            if (!Tools.IsUnder18Years(profile?.PrivatePerson.BirthDate) && (agentViewModel.Type == AgentType.Province))
            {
                Alert("شما نمی توانید نوع نمایندگی را ولایت انتخاب کنید .", NotificationType.Warning);
                return View("Edit", agentViewModel);
            }

            var agent = _mapper.Map<CreateAgentViewModel, Agent>(agentViewModel);
            agent.ProfileId = User.ProfileId;
            agent.AgentProfileId = agentProfile.Id;
            agent.IsConfirmed = null;
            agent.Id = profile.Agent.Id;

            foreach (var item in agentViewModel.AgentFiles)
            {
                var cdnfileName = $"{Guid.NewGuid()}{Path.GetExtension(item.FileName)}";
                // convert to Domain File  ... 
                var file = new File
                {
                    CreationDate = DateTime.Now,
                    Type = FileType.Image,
                    Title = cdnfileName,
                    MimeType = item.ContentType,
                    FileName = cdnfileName

                };

                // the file path in this method will be replaced so empty string pass into
                try
                {
                    await _fileService.CreateAsync(file, item.ToByte(),
                        _fileStorageConfiguration.AgentFileSignaturePath);
                    await _agentDocumentService.CreateAsync(agent.Id, file);
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnsupportedMediaType)
                {
                    await DeleteAgentDocument(agent, setDeleteDate, null);

                    Alert("فایل نمایندگی دارای فرمت مناسب نمی باشد.", NotificationType.Warning);
                    return View("Edit", agentViewModel);
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.ImageTooLarge)
                {
                    await DeleteAgentDocument(agent, setDeleteDate, null);
                    Alert("حجم هر فایل ارسالی باید بیشتر از 10 کیلوبایت و  کمتر از 2 مگابایت باشد،لطفا حجم فایل های خود را بررسی نمایید", NotificationType.Warning);
                    return View("Edit", agentViewModel);
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.ImageTooSmall)
                {
                    await DeleteAgentDocument(agent, setDeleteDate, null);
                    Alert("حجم هر فایل ارسالی باید بیشتر از 10 کیلوبایت و  کمتر از 2 مگابایت باشد،لطفا حجم فایل های خود را بررسی نمایید", NotificationType.Warning);
                    return View("Edit", agentViewModel);
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnknownServerError)
                {
                    await DeleteAgentDocument(agent, setDeleteDate, null);
                    Alert("در ذخیره فایل مشکلی بوجود آمده لطفا با پشتیبانی تماس بگیرید.", NotificationType.Warning);
                    return View("Edit", agentViewModel);
                }

                catch (CerberusException)
                {
                    await DeleteAgentDocument(agent, setDeleteDate, null);
                    Alert("خطا در ذخیره اطلاعات", NotificationType.Warning);
                    return View("Edit", agentViewModel);
                }
                catch (Exception)
                {
                    await DeleteAgentDocument(agent, setDeleteDate, null);
                    Alert("خطا در ذخیره اطلاعات", NotificationType.Warning);
                    return View("Edit", agentViewModel);
                }

            }

            if (profile.Status >= ProfileStatus.TraceCode)
            {
                await _agentService.AddToArchive(profile.Agent, setDeleteDate);
            }
            else
            {
                var agentDoc = await _agentDocumentService.GetListDocumentByAgentId(agent.Id);
                if (agentDoc != null)
                {
                    foreach (var doc in agentDoc.Where(x => x.ModifiedDate < setDeleteDate))
                    {
                        await _agentDocumentService.DeleteAsync(doc.Id);
                        await _fileService.DeleteAsync(doc.FileId);
                    }

                }
            }

            await _agentService.UpdateAsync(agent);
            await _otpService.KillAsync(ConventionalHelper.CreateOtpKey(agentProfile.Mobile.ToString(), OtpType.AgentValidator));


            if (profile.Status == ProfileStatus.Sejami)
            {
                profile.Status = ProfileStatus.Suspend;
                profile.StatusReasonType = StatusReasonType.EditAgent;
                await _profileService.UpdateStatusAndReasonAsync(profile);
                await _profileHistoryService.CreateAsync(new ProfileHistory
                {
                    ReferenceId = ProfileHistoryReferenceType.EditAgent.ToString(),
                    ProfileId = User.ProfileId,
                }, ProfileStatus.Suspend);
                await _messagingService.SendSmsAsync(profile.Id, profile.Mobile.ToString(),
                    profile.Carrier,
                    string.Format(_messagingConfiguration.SejamiProfileEditAgentMessage, profile.UniqueIdentifier, profile.TraceCode,
                        Environment.NewLine));
            }

            else if (profile.Status == ProfileStatus.TraceCode)
            {
                await _messagingService.SendSmsAsync(profile.Id, profile.Mobile.ToString(),
                    profile.Carrier,
                    string.Format(_messagingConfiguration.TraceCodeProfileEditAgentMessage, profile.UniqueIdentifier, profile.TraceCode,
                        Environment.NewLine));
            }

            if (profile.Status == ProfileStatus.InvalidInformation)
                Alert("  درصورت بروز رسانی نماینده،حتما به مرحله تعهدات رفته و گزینه می پذریم را کلیک کنید ، در غیر اینصورت مدارک شما بررسی نخواهد شد", NotificationType.Info);

            return RedirectToAction("Index");
        }

        private async Task DeleteAgentDocument(Agent agent, DateTime? startDate, DateTime? fromDate)
        {
            var agentDocuments = await _agentDocumentService.GetListDocumentByAgentId(agent.Id);
            if (agentDocuments.Any())
            {
                if (startDate != null)
                {
                    foreach (var doc in agentDocuments.Where(x => x.ModifiedDate >= startDate))
                    {
                        await _agentDocumentService.DeleteAsync(doc.Id);
                        await _fileService.DeleteAsync(doc.FileId);
                    }
                }
                else if (fromDate != null)
                {
                    foreach (var doc in agentDocuments.Where(x => x.ModifiedDate < fromDate))
                    {
                        await _agentDocumentService.DeleteAsync(doc.Id);
                        await _fileService.DeleteAsync(doc.FileId);
                    }
                }
                var agentDocumentList = await _agentDocumentService.GetListDocumentByAgentId(agent.Id);
                if (!agentDocumentList.Any())
                {
                    await _agentService.DeleteAsync(agent.Id);
                }

            }
            else
            {
                await _agentService.DeleteAsync(agent.Id);
            }
        }


        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Agent)]
        public async Task<IActionResult> Redirect()
        {
            var profile = await _profileService.GetRelatedDataForAgentAsync(User.ProfileId);
            if (profile.PrivatePerson != null && Tools.IsUnder18Years(profile?.PrivatePerson.BirthDate) && profile.Agent == null)
            {
                Alert("با توجه به اینکه سن شما کمتر از 18 سال است، لذا باید برای خود نماینده انتخاب کنید", NotificationType.Warning);
                return RedirectToAction("Index");
            }
            var currentStep = CurrentStep.Agent;
            if (User.ProfileStatus == ProfileStatus.InvalidInformation)
                currentStep = CurrentStep.BankingAccount;
            return RedirectToNextStep(currentStep);
        }


        [HttpPost("agents")]
        [ValidateAntiForgeryToken]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Agent)]
        [RateLimitFilterFactory(Order = 1, Limit = 2, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<JsonResult> DeleteAgentAsync()
        {
            var profile = await _profileService.GetRelatedDataForAgentAsync(User.ProfileId);

            if (Tools.IsUnder18Years(profile.PrivatePerson.BirthDate) && (profile.Agent.Type == AgentType.Province || profile.Agent.Type == AgentType.Conspiracy))

                return Json(new DeleteAgentViewModel
                {
                    Message = "به دلیل نرسیدن به سن قانونی ، شما نمی توانید نماینده خود را حذف کنید"
                });


            if (profile.Agent == null)
            {
                return Json(new DeleteAgentViewModel
                {
                    Message = "نماینده یافت نشد"
                });
            }

            if (profile.Agent.IsConfirmed == null && profile.Status >= ProfileStatus.PolicyAccepted)
            {
                return Json(new DeleteAgentViewModel
                {
                    Message = "به دلیل بررسی نماینده توسط شرکت سپرده گذاری، شما نمی توانید نماینده خود را حذف نمایید"
                });
            }
            if (profile.Status>ProfileStatus.TraceCode && !profile.IsDeleteAndUpdateAgentSuspend())
            {
                return Json(new DeleteAgentViewModel
                {
                    Message = "شما نمی توانید در حالت سجامی ناقص ، نماینده خود را حذف  نمایید"
                });
            }
            if (profile.Status < ProfileStatus.TraceCode)
            {

                if (profile.Agent.IsConfirmed == false && profile.Status > ProfileStatus.PolicyAccepted)
                    await _offlineTaskService.ScheduleOfflineTasksAsync(User.ProfileId,null, DateTime.Now);

                var agentDoc = await _agentDocumentService.GetListDocumentByAgentId(profile.Agent.Id);
                if (agentDoc != null)
                {
                    foreach (var doc in agentDoc)
                    {
                        await _agentDocumentService.DeleteAsync(doc.Id);
                        await _fileService.DeleteAsync(doc.FileId);
                    }
                }
                await _agentService.DeleteAsync(profile.Agent);
            }
            else
            {
                await _agentService.MoveToArchiveAsync(profile.Agent);
                await CheckStatusAndSendSms(profile);
            }
            return Json(new DeleteAgentViewModel
            {
                IsSuccess = true,
                Message = "نماینده با موفقیت حذف شد"
            });
        }

        private async Task CheckStatusAndSendSms(Profile profile)
        {
            if (profile.Status == ProfileStatus.Sejami)
            {
                profile.Status = ProfileStatus.Suspend;
                profile.StatusReasonType = StatusReasonType.DeleteAgent;
                await _profileService.UpdateStatusAndReasonAsync(profile);
                await _profileHistoryService.CreateAsync(new ProfileHistory
                {
                    ReferenceId = ProfileHistoryReferenceType.DeleteAgent.ToString(),
                    ProfileId = User.ProfileId,
                }, ProfileStatus.Suspend);
                await _messagingService.SendSmsAsync(profile.Id, profile.Mobile.ToString(),
                    profile.Carrier,
                    string.Format(_messagingConfiguration.SejamiProfileDeleteAgentMessage, profile.UniqueIdentifier, profile.TraceCode,
                        Environment.NewLine));
            }

            else if (profile.Status == ProfileStatus.TraceCode)
            {
                await _messagingService.SendSmsAsync(profile.Id, profile.Mobile.ToString(),
                    profile.Carrier,
                    string.Format(_messagingConfiguration.TraceCodeProfileDeleteAgentMessage, profile.UniqueIdentifier, profile.TraceCode,
                        Environment.NewLine));
            }
            else
            {
                await _messagingService.SendSmsAsync(profile.Id, profile.Mobile.ToString(),
                    profile.Carrier,
                    string.Format(_messagingConfiguration.InvalidInformationProfileDeleteAgentMessage, profile.UniqueIdentifier,
                        Environment.NewLine));
            }
        }
    }
}
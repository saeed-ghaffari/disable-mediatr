﻿using AutoMapper;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Web.Enum;
using Cerberus.Web.Extensions;
using Cerberus.Web.Filters;
using Cerberus.Web.ViewModel.TradingCode;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gatekeeper.AspModules.ActionFilter;

namespace Cerberus.Web.Controllers
{
    [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.TradingCode)]
    public class TradingCodeController : BaseController
    {
        private readonly ITradingCodeService _tradingCodeService;
        private readonly IMapper _mapper;

        public TradingCodeController(ITradingCodeService tradingCodeService, IMapper mapper)
        {
            _tradingCodeService = tradingCodeService;
            _mapper = mapper;
        }

        [HttpGet]
        //  [Route("tradingCode")]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 1, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Index()
        {
           var createTradingCodeViewModel = new CreateTradingCodeViewModel()
            {
                TradingCodes = _mapper.Map<List<TradingCodeViewModel>>(await _tradingCodeService.GetListAsync(User.ProfileId)),
            };
            return View(createTradingCodeViewModel);
        }

        [HttpGet]
        [Route("tradingCodes/nextStep")]
        public IActionResult Redirect()
        {
            return RedirectToNextStep(CurrentStep.TradingCode);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("tradingCodes")]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 1, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateAsync(CreateTradingCodeViewModel tradingCodeViewModel)
        {
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                return RedirectToAction("Index", tradingCodeViewModel);
            }
            var tradecodes = await _tradingCodeService.GetListAsync(User.ProfileId);
            if (tradecodes.Count > 11)
            {
                Alert("حداکثر تعداد کد های سهامداری وارد شده 10 عدد می تواند باشد", NotificationType.Warning);

                return RedirectToAction("Index");
            }
            if (tradecodes.Any(x => x.ProfileId == User.ProfileId && x.FirstPart == tradingCodeViewModel.FirstPart && x.SecondPart == tradingCodeViewModel.SecondPart && x.ThirdPart == tradingCodeViewModel.ThirdPart))
            {
                Alert("کد سهامداری تکراری می باشد", NotificationType.Warning);
                return RedirectToAction("Index");
            }
            var tradingCodeModel = _mapper.Map<TradingCode>(tradingCodeViewModel);
            tradingCodeModel.ProfileId = User.ProfileId;
            await _tradingCodeService.CreateAsync(tradingCodeModel);

            Alert("اطلاعات با موفقیت ثبت شد", NotificationType.Success);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [Route("tradingCodes/{id}")]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 1, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> DeleteAsync(DeleteTradeCodeViewModel model)
        {
            var deleteModel = new DeleteTradeCodeViewModel();
            var tradingCodeModel = await _tradingCodeService.GetAsync(model.Id);
            if (tradingCodeModel == null || tradingCodeModel.ProfileId != User.ProfileId)
            {
                deleteModel.IsSuccess = false;
                deleteModel.Message = "خطا در حذف اطلاعات";
                return Json(deleteModel);
            }
            await _tradingCodeService.DeleteAsync(tradingCodeModel);
            deleteModel.IsSuccess = true;
            return Json(deleteModel);
        }
    }
}
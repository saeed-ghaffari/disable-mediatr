﻿using System;
using AutoMapper;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Web.Enum;
using Cerberus.Web.Extensions;
using Cerberus.Web.Filters;
using Cerberus.Web.ViewModel.BankingAccount;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Services.Membership;
using Cerberus.Domain.Model.Membership;
using Cerberus.Utility;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Cerberus.Web.Controllers
{

    public class BankingAccountController : BaseController
    {
        private readonly IProfileService _profileService;
        private readonly IBankingAccountService _bankingAccountService;
        private readonly IBankService _bankService;
        private readonly ICityService _cityService;
        private readonly IMapper _mapper;
        private readonly IProvinceService _provinceService;
        private readonly IMembershipService _membershipService;
        private readonly IProfilePackageService _profilePackageService;
        private readonly IOfflineTaskService _offlineTaskService;
        private readonly IProfileHistoryService _profileHistoryService;
        public BankingAccountController(
               IProfileService profileService,
               IBankingAccountService bankingAccountService,
               IBankService bankService,
               ICityService cityService,
               IMapper mapper,
               IProvinceService provinceService,
               IMembershipService membershipService,
               IProfilePackageService profilePackageService,
               IOfflineTaskService offlineTaskService, IProfileHistoryService profileHistoryService)
        {
            _profileService = profileService;
            _bankingAccountService = bankingAccountService;
            _bankService = bankService;
            _cityService = cityService;
            _mapper = mapper;
            _provinceService = provinceService;
            _membershipService = membershipService;
            _profilePackageService = profilePackageService;
            _offlineTaskService = offlineTaskService;
            _profileHistoryService = profileHistoryService;
        }

        [HttpGet]
        [Route("bankingAccounts")]
        [AuthorizeUserFilterFactory(Scope = (int)(AvailableScope.BankingAccount))]
        [RateLimitFilterFactory(Order = 1, Limit = 30, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Index()
        {
            ViewBag.CanEdit = false;

            // Remove TempData
            if (TempData["dashboard"] as string == "ok") TempData.Remove("dashboard");


            var user = await _profileService.GetAsync(User.ProfileId);

            if (user.Status < ProfileStatus.PolicyAccepted)
                if (User.ServiceId.HasValue && User.ServiceId.Value > 0)
                {
                    var membership =
                        await _membershipService.GetPrivatePersonAsync(user.UniqueIdentifier, user.Mobile.ToString(), User.ServiceId.Value);

                    ViewBag.CanEdit = membership?.CanEditAccounts ?? false;
                }



            ViewBag.Provinces = (await _provinceService.GetListAsync()).Where(x => x.CountryId == 1).OrderBy(x => x.Name).Select(x => new SelectListItem(x.Name, x.Id.ToString()));
            ViewBag.Banks = (await _bankService.GetListAsync()).OrderBy(x => x.Name).Select(x => new SelectListItem(x.Name, x.Id.ToString()));


            var bankingAccounts = await _bankingAccountService.GetListAsync(User.ProfileId, true);
            var allAccounts = bankingAccounts as BankingAccount[] ?? bankingAccounts.ToArray();





            if (allAccounts.Any())
            {
                var createBankingAccountViewModel = new CreateBankingAccountViewModel
                {
                    BankingAccounts = allAccounts
                };
                return View(createBankingAccountViewModel);
            }


            if (User.ServiceId.HasValue && User?.ServiceId.Value > 0 && user.Status < ProfileStatus.PolicyAccepted)
            {
                var checkUser = await _membershipService.GetPrivatePersonAsync(user.UniqueIdentifier, user.Mobile.ToString(), User.ServiceId.Value);

                if (checkUser != null)
                {
                    var bankingAccountTmp = (await _membershipService.GetAccountAsync(user.UniqueIdentifier, User.ServiceId.Value));
                    if (bankingAccountTmp != null)
                    {

                        var bankingAccount = _mapper.Map<CreateBankingAccountViewModel>(bankingAccountTmp);

                        var bankingAccountViewModel = new CreateBankingAccountViewModel
                        {
                            AccountNumber = bankingAccount.AccountNumber,
                            IsDefault = bankingAccount.IsDefault,
                            Type = bankingAccount?.Type,
                            BankId = bankingAccount?.BankId,
                            BranchCityId = bankingAccount?.BranchCityId,
                            BranchCode = bankingAccount?.BranchCode,
                            BranchName = bankingAccount?.BranchName,
                            Sheba = bankingAccount?.Sheba,
                            BankingAccounts = new List<BankingAccount>(),
                            LockAccount = checkUser.CanEditAccounts
                        };
                        if (ViewBag.CanEdit == true)
                        {
                            Alert("به دلیل استفاده از لینک تخفیف ، شما نمی توانید شبا بانکی خود را تغییر دهید", NotificationType.Error);
                        }
                        return View(bankingAccountViewModel);
                    }
                }

            }

            var accountViewModel = new CreateBankingAccountViewModel
            {
                BankingAccounts = new List<BankingAccount>()
            };

            return View(accountViewModel);

        }

        [HttpGet]
        [AuthorizeUserFilterFactory]
        //  [Route("bankingAccounts/nextStep")]
        public async Task<IActionResult> Redirect()
        {

            var accounts = await _bankingAccountService.GetListAsync(User.ProfileId);
            var bankingAccounts = accounts as BankingAccount[] ?? accounts.ToArray();
            if (!bankingAccounts.Any())
            {
                Alert("ثبت حداقل یک شماره حساب پیش فرض برای ادامه ثبت نام/ویرایش اجباری است.", NotificationType.Warning);
                if (Request.GetCurrentUserData().AvailableScope.HasFlag(AvailableScope.BankingAccount))

                    return RedirectToAction("Index");

                return RedirectToAction("Edit");
            }

            if (!bankingAccounts.Any(x => x.IsDefault))
            {
                Alert("تعیین یک حساب پیشفرض الزامی می باشد .", NotificationType.Warning);
                return RedirectToAction(Request.GetCurrentUserData().AvailableScope.HasFlag(AvailableScope.BankingAccount)
                    ? "Index"
                    : "Edit");
            }
            var profile = await _profileService.GetAsync(User.ProfileId);
            if (profile.IsEditBankingAccountSemiSejami() || profile.IsEditPrivatePersonSemiSejami() || profile.IsEditBankingAccountAndDeleteAgentEighteen() || profile.IsEditPrivatePersonAndDeleteAgentEighteen())
            {
                var package = await _profilePackageService.GetUnusedPackagesAsync(profile.Id, PackageType.EditAllData, PackageUsedFlag.EditBankingAccount);
                if (package != null)
                {
                    if (profile.Status == ProfileStatus.SemiSejami && profile.StatusReasonType == StatusReasonType.EditBankingAccount && bankingAccounts.All(x => x.IsConfirmed == true))
                    {

                        await _profileHistoryService.CreateAsync(new ProfileHistory
                        {
                            ReferenceId = ProfileHistoryReferenceType.UpdateBankingAccount.ToString(),
                            ProfileId = User.ProfileId,
                        }, ProfileStatus.Sejami);

                        profile.Status = ProfileStatus.Sejami;
                        profile.StatusReasonType = null;
                        await _profileService.UpdateStatusAndReasonAsync(profile);

                        return RedirectToAction("Index", "Dashboard");
                    }
                    if (bankingAccounts.All(x => x.IsConfirmed == true))
                    {
                        return RedirectToAction("Index", "Dashboard");
                    }
                    if (bankingAccounts.Any(x => x.IsConfirmed == null))
                    {

                        await _offlineTaskService.ScheduleEditAfterSejamiTasksAsync(profile.Id);
                        return View("FinalEdit", package);
                    }

                    Alert("شما اطلاعات بانکی خود را ویرایش ننموده اید", NotificationType.Warning);
                    return RedirectToAction(nameof(Edit));
                }

                Alert("برای عملیات ویرایش باید عملیات پرداخت را انجام دهید", NotificationType.Warning);
                return RedirectToAction(nameof(Edit));

            }
            return RedirectToNextStep(CurrentStep.BankingAccount);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("bankingAccounts")]
        [AuthorizeUserFilterFactory(Scope = (int)(AvailableScope.BankingAccount))]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateAsync(CreateBankingAccountViewModel bankingAccountViewModel)
        {
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                return RedirectToAction("Index");
            }

            var user = await _profileService.GetWithNoCacheAsync(User.ProfileId);

            if (user.Status >= ProfileStatus.TraceCode)
            {
                Alert("بعد از گرفتن کد پیگیری امکان ثبت شماره حساب جدید امکان پذیر نمی باشد!", NotificationType.Error);
                return RedirectToAction(nameof(Index));
            }
            var accounts = await _bankingAccountService.GetListAsync(User.ProfileId);
            var bankingAccounts = accounts.ToList();

            PrivatePersonMembership personMembership = new PrivatePersonMembership();

            if (User.ServiceId.HasValue && User?.ServiceId.Value > 0 && user.Status < ProfileStatus.PolicyAccepted)
            {
                personMembership = await _membershipService.GetPrivatePersonAsync(user.UniqueIdentifier,
                    user.Mobile.ToString(), User.ServiceId.Value);

                if (personMembership != null && personMembership.CanEditAccounts)
                {

                    var accountMembership = (
                            await _membershipService.GetAccountAsync(user.UniqueIdentifier,
                                User.ServiceId.Value));


                    if (accountMembership != null)
                    {
                        bankingAccountViewModel.IsDefault = true;
                        bankingAccountViewModel.Sheba = accountMembership?.Sheba;
                    }
                }

            }

            if (bankingAccountViewModel.Id == 0)
            {

                if (bankingAccounts.Any() && personMembership?.CanEditAccounts == true && user.Status < ProfileStatus.TraceCode)
                {
                    Alert("به دلیل استفاده از لینک تخفیف، شما نمی توانید حساب جدید ثبت نمایید!",
                        NotificationType.Error);
                    return RedirectToAction(nameof(Index));

                }

                if (bankingAccounts.Count() > 1)
                {
                    Alert("حداکثر تعداد حساب بانکی که می توانید ثبت کنید 2 حساب می باشد.", NotificationType.Error);
                    return RedirectToAction(nameof(Index));
                }
                if (bankingAccounts.Any(x => x.ProfileId == User.ProfileId))
                {

                    if (bankingAccounts.Any(x => x.Sheba == bankingAccountViewModel.Sheba))
                    {
                        Alert("این شبا قبلا توسط این کاربر ثبت شده است.", NotificationType.Error);
                        return RedirectToAction(nameof(Index));
                    }
                    if (bankingAccounts.Any(x => x.IsDefault) && bankingAccountViewModel.IsDefault)
                    {
                        Alert("شما نمی توانید 2 حساب پیشفرض ثبت نمایید.", NotificationType.Error);
                        return RedirectToAction(nameof(Index));
                    }


                }

                var bankingAccountModel = _mapper.Map<BankingAccount>(bankingAccountViewModel);
                bankingAccountModel.ProfileId = User.ProfileId;
                await _bankingAccountService.CreateAsync(bankingAccountModel);
                Alert("اطلاعات با موفقیت ثبت شد", NotificationType.Success);


            }
            else
            {
                if (bankingAccounts.Any(x => x.ProfileId == User.ProfileId))
                {
                    if (bankingAccounts.Any(x => x.Sheba == bankingAccountViewModel.Sheba && x.Id != bankingAccountViewModel.Id))
                    {
                        Alert("این شبا قبلا توسط این کاربر ثبت شده است.", NotificationType.Error);
                        return RedirectToAction(nameof(Index));
                    }
                    if (bankingAccounts.Any(x => x.IsDefault && x.Id != bankingAccountViewModel.Id) && bankingAccountViewModel.IsDefault)
                    {
                        Alert("شما نمی توانید 2 حساب پیشفرض ثبت نمایید.", NotificationType.Error);
                        return RedirectToAction(nameof(Index));
                    }

                    if (bankingAccounts.Any(x => x.Id == bankingAccountViewModel.Id && x.Locked && user.Status != ProfileStatus.Sejami))
                    {
                        Alert("شما اجازه ویرایش این حساب را ندارید.", NotificationType.Error);
                        return RedirectToAction(nameof(Index));
                    }

                    var ccBankingAccountbankingAccountModel = _mapper.Map<BankingAccount>(bankingAccountViewModel);

                    ccBankingAccountbankingAccountModel.ProfileId = User.ProfileId;
                    await _bankingAccountService.UpdateAsync(ccBankingAccountbankingAccountModel);
                    Alert("اطلاعات با موفقیت بروز رسانی شد", NotificationType.Success);
                    return RedirectToAction("Index");
                }

                Alert("اطلاعات ارسالی مربوط به این پروفایل نمی باشد", NotificationType.Error);

            }

            return RedirectToAction("Index");
        }

        public async Task<bool> IsValidUserPackage(Domain.Model.Profile profile)
        {
            var package = await _profilePackageService.GetUnusedPackagesAsync(profile.Id, PackageType.EditAllData, PackageUsedFlag.EditBankingAccount);
            return package != null;
        }

        [HttpPost]
        [Route("bankingAccounts/{id}")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.BankingAccount)]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<JsonResult> DeleteAsync(long id)
        {
            var deleteModel = new DeleteBankingAccountViewModel();
            var user = await _profileService.GetAsync(User.ProfileId);

            if (User.ServiceId.HasValue && User?.ServiceId.Value > 0 && user.Status < ProfileStatus.PolicyAccepted)
            {
                var personMembership = await _membershipService.GetPrivatePersonAsync(user.UniqueIdentifier,
                      user.Mobile.ToString(), User.ServiceId.Value);

                if (personMembership != null && personMembership.CanEditAccounts)
                {
                    deleteModel.IsSuccess = false;
                    deleteModel.Message = "به دلیل استفاده از لینک تخفیف،شما نمی توانید حساب بانکی خود را حذف نمایید.";
                    return Json(deleteModel);
                }
            }
            var bankingAccountModel = await _bankingAccountService.GetAsync(id);


            if (bankingAccountModel == null || bankingAccountModel.ProfileId != User.ProfileId || bankingAccountModel.Locked)
            {
                deleteModel.IsSuccess = false;
                deleteModel.Message = "خطا در حذف اطلاعات دوباره تلاش کنید";
                return Json(deleteModel);
            }

            if (user.Status == ProfileStatus.Sejami && bankingAccountModel.IsDefault && bankingAccountModel.IsConfirmed == true)
            {
                deleteModel.IsSuccess = false;
                deleteModel.Message = "شما نمی توانید حساب پیشفرض خود را حذف نمایید.";
                return Json(deleteModel);
            }

            if (user.Status >= ProfileStatus.TraceCode)
            {
                deleteModel.Message = "بعد از گرفتن کد پیگیری امکان حذف حساب بانکی امکانپذیر  نمی باشد";
                return Json(deleteModel);
            }

            await _bankingAccountService.DeleteAsync(bankingAccountModel);
            deleteModel.IsSuccess = true;
            return Json(deleteModel);
        }


        [HttpPost]
        [Route("getBankingAccount")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.BankingAccount)]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> GetAsync(long id)
        {

            var user = await _profileService.GetAsync(User.ProfileId);
            ViewBag.CanEdit = false;

            if (user.Status < ProfileStatus.PolicyAccepted)
                if (User.ServiceId.HasValue && User.ServiceId.Value > 0)
                    ViewBag.CanEdit = (await _membershipService.GetPrivatePersonAsync(user.UniqueIdentifier, User.ServiceId.Value))?.CanEditAccounts ?? true;


            ViewBag.Provinces = (await _provinceService.GetListAsync()).Where(x => x.CountryId == 1).Select(x => new SelectListItem(x.Name, x.Id.ToString()));
            ViewBag.Banks = (await _bankService.GetListAsync()).Select(x => new SelectListItem(x.Name, x.Id.ToString()));


            var bankingAccountModel = await _bankingAccountService.GetAsync(id);
            if (bankingAccountModel == null || bankingAccountModel.ProfileId != User.ProfileId)
            {
                Alert("خطا در ویرایش اطلاعات، دوباره تلاش کنید", NotificationType.Warning);
                return RedirectToAction("Index");
            }

            if (bankingAccountModel.BranchCityId.HasValue)
            {
                City temp = (await _cityService.GetAsync(bankingAccountModel.BranchCityId.Value));
                SelectListItem tempp = new SelectListItem();

                tempp.Value = temp.Id.ToString();
                tempp.Disabled = false;
                tempp.Text = temp.Name;

                var listItems = new List<SelectListItem> { tempp };
                ViewBag.City = listItems;

            }

            var createBankingAccountViewModel = _mapper.Map<BankingAccount, CreateBankingAccountViewModel>(bankingAccountModel);
            createBankingAccountViewModel.BankingAccounts = await _bankingAccountService.GetListAsync(User.ProfileId, true);

            return View("Index", createBankingAccountViewModel);
        }


        [HttpPost]
        [Route("setDefault")]
        [AuthorizeUserFilterFactory]
        [RateLimitFilterFactory(Order = 1, Limit = 5, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> IsDefaultAsync()
        {

            var user = await _profileService.GetWithNoCacheAsync(User.ProfileId);
            if (User.ServiceId.HasValue && User.ServiceId.Value > 0 && user.Status < ProfileStatus.PolicyAccepted)
            {

                var editAccount = (await _membershipService.GetPrivatePersonAsync(user.UniqueIdentifier, user.Mobile.ToString(), User.ServiceId.Value))?.CanEditAccounts ?? false;
                if (editAccount)
                {
                    Alert("به دلیل استفاده از لینک تخفیف، شما نمی توانید پیشفرض بودن حساب خود را تغییر دهید",
                        NotificationType.Error);
                    return RedirectToAction(nameof(Index));
                }
            }

            var allAccount = await _bankingAccountService.GetListAsync(User.ProfileId);

            var bankingAccounts = allAccount.ToList();

            if (bankingAccounts.Any(x => x.IsDefault == false))
            {

                foreach (var item in bankingAccounts)
                {
                    item.IsDefault = !item.IsDefault;

                    await _bankingAccountService.UpdateAsync(item);
                }

                Alert("حساب با موفقیت پیشفرض شد", NotificationType.Success);


            }


            if (TempData["dashboard"] as string == "ok")
            {
                TempData["dashboard"] = null;
                TempData.Remove("dashboard");

                return RedirectToAction("Index", "Dashboard");
            }

            return RedirectToAction(Request.GetCurrentUserData().AvailableScope.HasFlag(AvailableScope.BankingAccount) ? "Index" : "Edit");
        }


        [HttpPost("getBankAccount")]
        [AuthorizeUserFilterFactory]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> EditAsync(long id)
        {
            ViewBag.Provinces = (await _provinceService.GetListAsync()).Where(x => x.CountryId == 1).Select(x => new SelectListItem(x.Name, x.Id.ToString()));
            ViewBag.Banks = (await _bankService.GetListAsync()).Select(x => new SelectListItem(x.Name, x.Id.ToString()));


            var bankingAccountModel = await _bankingAccountService.GetAsync(id);

            if (bankingAccountModel != null && bankingAccountModel.ProfileId == User.ProfileId)
            {
                EditBankingAccountViewModel bank = new EditBankingAccountViewModel
                {
                    AccountNumber = bankingAccountModel.AccountNumber,
                    BankId = bankingAccountModel.BankId,
                    BranchCityId = bankingAccountModel.BranchCityId,
                    BranchCode = bankingAccountModel.BranchCode,
                    BranchName = bankingAccountModel.BranchName,
                    Id = bankingAccountModel.Id,
                    Type = bankingAccountModel.Type,

                };
                if (bankingAccountModel.BranchCityId.HasValue)
                {
                    City temp = (await _cityService.GetAsync(bankingAccountModel.BranchCityId.Value));
                    SelectListItem tempp = new SelectListItem();

                    tempp.Value = temp.Id.ToString();
                    tempp.Disabled = false;
                    tempp.Text = temp.Name;

                    var listItems = new List<SelectListItem> { tempp };
                    ViewBag.City = listItems;

                }

                return View(bank);
            }

            return View();
        }

        [HttpPost("updateBankingAccount")]
        [ValidateAntiForgeryToken]
        [AuthorizeUserFilterFactory]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> UpdateAsync(EditBankingAccountViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                return View("EditAsync", vm);
            }


            var bankingAccountModel = await _bankingAccountService.GetAsync(vm.Id);
            if (bankingAccountModel != null && bankingAccountModel.ProfileId == User.ProfileId)
            {
                bankingAccountModel.AccountNumber = vm.AccountNumber;
                bankingAccountModel.BankId = vm.BankId.ToLong();
                bankingAccountModel.BranchCityId = vm.BranchCityId;
                bankingAccountModel.BranchName = vm.BranchName;
                bankingAccountModel.BranchCode = vm.BranchCode;
                // bankingAccountModel.Id = vm.Id;
                //bankingAccountModel.ProfileId = User.ProfileId;
                bankingAccountModel.Type = vm.Type;
                await _bankingAccountService.UpdateAsync(bankingAccountModel);
                Alert("اطلاعات باموفقیت بروز رسانی شد", NotificationType.Success);
                return RedirectToAction("Index", "Dashboard");
            }
            Alert("خطا در بروز رسانی", NotificationType.Error);
            return RedirectToAction("Index", "Dashboard");
        }



        [HttpGet]
        [Route("editBankingAccounts")]
        [AuthorizeUserFilterFactory(Scope = (int)(AvailableScope.EditBankingAccount))]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Edit()
        {
            var user = await _profileService.GetWithNoCacheAsync(User.ProfileId);
            if (!user.IsEditBankingAccountSemiSejami() && !user.IsEditBankingAccountAndDeleteAgentEighteen() && !user.IsEditPrivatePersonAndDeleteAgentEighteen())
            {
                return View("EditAccount", new SejamiAccountViewModel
                {
                    Status = "accessDenied",
                    PackageType = PackageType.EditAllData
                });
            }
            ViewBag.Provinces = (await _provinceService.GetListAsync()).Where(x => x.CountryId == 1).OrderBy(x => x.Name).Select(x => new SelectListItem(x.Name, x.Id.ToString()));
            ViewBag.Banks = (await _bankService.GetListAsync()).OrderBy(x => x.Name).Select(x => new SelectListItem(x.Name, x.Id.ToString()));

            var bankingAccounts = await _bankingAccountService.GetListAsync(User.ProfileId, true);
            var allAccounts = bankingAccounts.ToList();

            //Edit Banking Account For SejamiPerson
            if (!await IsValidUserPackage(user))
            {
                Alert("در صورت تغییر اطلاعات هویتی در سازمان ثبت احوال، توصیه میگردد قبل از بروز رسانی اطلاعات در سجام، تغییرات اطلاعات هویتی خود را  به بانک حساب  ثبت شده خود در سجام نیز اطلاع داده تا اطلاعات هویتی شما در بانک نیز بروز گردد. در غیر این صورت ویرایش حساب بانکی بعد از 30 روز مستلزم هزینه خواهد بود", NotificationType.Info);

                return View("EditAccount", new SejamiAccountViewModel
                {
                    Status = "payment",
                    PackageType = PackageType.EditAllData

                });
            }


            if (allAccounts.Any())
            {
                var createBankingAccountViewModel = new CreateBankingAccountViewModel
                {
                    BankingAccounts = allAccounts
                };

                return View(createBankingAccountViewModel);
            }


            var accountViewModel = new CreateBankingAccountViewModel
            {
                BankingAccounts = new List<BankingAccount>()
            };

            return View(accountViewModel);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("UpdateBankingAccounts")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.EditBankingAccount)]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateAfterSejamiAsync(CreateBankingAccountViewModel model)
        {
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                return RedirectToAction("Edit");
            }

            var user = await _profileService.GetWithNoCacheAsync(User.ProfileId);

            if (!user.IsEditBankingAccountSemiSejami() && !user.IsEditBankingAccountAndDeleteAgentEighteen() && !user.IsEditPrivatePersonAndDeleteAgentEighteen())
            {
                Alert("شما نمی توانید اطلاعات بانکی خود را ویرایش نمایید!", NotificationType.Error);
                return RedirectToAction(nameof(Edit));
            }

            var accounts = await _bankingAccountService.GetListAsync(User.ProfileId);
            var bankingAccounts = accounts.ToList();
            if (NotAllowEditing(user, bankingAccounts))
            {
                Alert("تا مشخص نشدن نتیجه استعلام،شما نمیتوانید حساب خود را ویرایش کنید!", NotificationType.Error);
                return RedirectToAction(nameof(Edit));
            }
            if (!await IsValidUserPackage(user))
            {
                Alert("شما باید عملیات پرداخت را انجام دهید!", NotificationType.Error);
                return RedirectToAction(nameof(Edit));
            }


            if (model.Id == 0)
            {

                if (bankingAccounts.Count() > 1)
                {
                    Alert("حداکثر تعداد حساب بانکی که می توانید ثبت کنید 2 حساب می باشد.", NotificationType.Error);
                    return RedirectToAction(nameof(Edit));
                }
                if (bankingAccounts.Any(x => x.ProfileId == User.ProfileId))
                {

                    if (bankingAccounts.Any(x => x.Sheba == model.Sheba))
                    {
                        Alert("این شبا قبلا توسط این کاربر ثبت شده است.", NotificationType.Error);
                        return RedirectToAction(nameof(Edit));
                    }
                    if (bankingAccounts.Any(x => x.IsDefault) && model.IsDefault)
                    {
                        Alert("شما نمی توانید 2 حساب پیشفرض ثبت نمایید.", NotificationType.Error);
                        return RedirectToAction(nameof(Edit));
                    }


                }

                var bankingAccountModel = _mapper.Map<BankingAccount>(model);
                bankingAccountModel.ProfileId = User.ProfileId;
                await _bankingAccountService.CreateAsync(bankingAccountModel);
                Alert("اطلاعات با موفقیت ثبت شد", NotificationType.Success);


            }
            else
            {
                if (bankingAccounts.Any(x => x.ProfileId == User.ProfileId))
                {
                    if (bankingAccounts.Any(x => x.Sheba == model.Sheba && x.Id != model.Id))
                    {
                        Alert("این شبا قبلا توسط این کاربر ثبت شده است.", NotificationType.Error);
                        return RedirectToAction(nameof(Edit));
                    }

                    if (bankingAccounts.Any(x => x.IsDefault && x.Id != model.Id) && model.IsDefault)
                    {
                        Alert("شما نمی توانید 2 حساب پیشفرض ثبت نمایید.", NotificationType.Error);
                        return RedirectToAction(nameof(Edit));
                    }

                    if (bankingAccounts.All(x => x.Id != model.Id))
                    {
                        Alert("اطلاعات ارسالی اشتباه می باشد", NotificationType.Error);
                        return RedirectToAction(nameof(Edit));
                    }

                    var ccBankingAccountbankingAccountModel = _mapper.Map<BankingAccount>(model);

                    ccBankingAccountbankingAccountModel.ProfileId = User.ProfileId;
                    await _bankingAccountService.UpdateAsync(ccBankingAccountbankingAccountModel);
                    Alert("اطلاعات با موفقیت بروز رسانی شد", NotificationType.Success);

                }
                else
                {
                    Alert("اطلاعات ارسالی مربوط به این پروفایل نمی باشد", NotificationType.Error);
                    return RedirectToAction("Edit");
                }
            }
            if (user.Status == ProfileStatus.Sejami)
            {
                var allAccounts = await _bankingAccountService.GetListAsync(User.ProfileId);
                var listAccounts = allAccounts.ToList();
                if (!listAccounts.Any() || listAccounts.Any(x => x.IsConfirmed != true))
                {
                    user.Status = ProfileStatus.SemiSejami;
                    user.StatusReasonType = StatusReasonType.EditBankingAccount;
                    await _profileService.UpdateStatusAndReasonAsync(user);
                    await _profileHistoryService.CreateAsync(new ProfileHistory
                    {
                        ReferenceId = ProfileHistoryReferenceType.ChangeBankingAccount.ToString(),
                        ProfileId = User.ProfileId,
                    }, ProfileStatus.SemiSejami);
                }
            }
            return RedirectToAction("Edit");
        }



        [HttpPost]
        [Route("bankingAccount/{id}")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.EditBankingAccount)]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<JsonResult> DeleteAccountAsync(long id)
        {
            var deleteModel = new DeleteBankingAccountViewModel();
            var user = await _profileService.GetWithNoCacheAsync(User.ProfileId);

            if (!user.IsEditBankingAccountSemiSejami() && !user.IsEditBankingAccountAndDeleteAgentEighteen() && !user.IsEditPrivatePersonAndDeleteAgentEighteen())
            {
                deleteModel.Message = $"شما نمی توانید اظلاعات بانکی خود را حذف کنید";
                return Json(deleteModel);
            }

            if (!await IsValidUserPackage(user))
            {
                deleteModel.Message = "شما باید قبلا عملیات پرداخت را انجام دهید.";
                return Json(deleteModel);
            }


            var bankingAccountModel = await _bankingAccountService.GetAsync(id);

            if (NotAllowEditing(user, bankingAccountModel))
            {
                deleteModel.Message = "تا مشخص نشدن نتیجه استعلام نمی توانید حساب بانکی را حذف نمایید";
                return Json(deleteModel);
            }

            if (bankingAccountModel == null || bankingAccountModel.ProfileId != User.ProfileId)
            {
                deleteModel.Message = "خطا در حذف اطلاعات دوباره تلاش کنید";
                return Json(deleteModel);
            }

            await _bankingAccountService.DeleteAsync(bankingAccountModel);
            var allAccount = await _bankingAccountService.GetListAsync(User.ProfileId);
            var listAccount = allAccount.ToList();
            if (listAccount.Any())
            {

                foreach (var item in listAccount)
                {
                    if (!item.IsDefault) item.IsDefault = true;

                    await _bankingAccountService.UpdateAsync(item);
                }

            }
            if (user.Status == ProfileStatus.Sejami)
            {
                if (!listAccount.Any() || listAccount.All(x => x.IsConfirmed != true))
                {
                    user.Status = ProfileStatus.SemiSejami;
                    user.StatusReasonType = StatusReasonType.EditBankingAccount;
                    await _profileService.UpdateStatusAndReasonAsync(user);
                    await _profileHistoryService.CreateAsync(new ProfileHistory
                    {
                        ReferenceId = ProfileHistoryReferenceType.ChangeBankingAccount.ToString(),
                        ProfileId = User.ProfileId,
                    }, ProfileStatus.SemiSejami);
                }
            }

            deleteModel.IsSuccess = true;
            return Json(deleteModel);
        }


        [HttpPost]
        [Route("getBankingAccounts")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.EditBankingAccount)]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> GetAfterSejamiAsync(long id)
        {

            ViewBag.Provinces = (await _provinceService.GetListAsync()).Where(x => x.CountryId == 1).Select(x => new SelectListItem(x.Name, x.Id.ToString()));
            ViewBag.Banks = (await _bankService.GetListAsync()).Select(x => new SelectListItem(x.Name, x.Id.ToString()));


            var bankingAccountModel = await _bankingAccountService.GetAsync(id);
            if (bankingAccountModel == null || bankingAccountModel.ProfileId != User.ProfileId)
            {
                Alert("خطا در ویرایش اطلاعات، دوباره تلاش کنید", NotificationType.Warning);
                return RedirectToAction("Edit");
            }


            if (bankingAccountModel.BranchCityId.HasValue)
            {
                City temp = (await _cityService.GetAsync(bankingAccountModel.BranchCityId.Value));
                SelectListItem tempp = new SelectListItem();

                tempp.Value = temp.Id.ToString();
                tempp.Disabled = false;
                tempp.Text = temp.Name;

                var listItems = new List<SelectListItem> { tempp };
                ViewBag.City = listItems;

            }

            var createBankingAccountViewModel = _mapper.Map<BankingAccount, CreateBankingAccountViewModel>(bankingAccountModel);
            createBankingAccountViewModel.BankingAccounts = await _bankingAccountService.GetListAsync(User.ProfileId, true);

            return View("Edit", createBankingAccountViewModel);
        }



        public IActionResult EditAccount(SejamiAccountViewModel model)
        {
            return View(model);
        }


        public bool NotAllowEditing(Domain.Model.Profile profile, BankingAccount account)
        {
            return profile.Status == ProfileStatus.SemiSejami && profile.StatusReasonType == StatusReasonType.EditBankingAccount && account != null && account.Locked;
        }

        public bool NotAllowEditing(Domain.Model.Profile profile, IList<BankingAccount> account)
        {
            return profile.Status == ProfileStatus.SemiSejami && profile.StatusReasonType == StatusReasonType.EditBankingAccount && account.Any() && (account.Count > 1 && account.All(x => x.Locked));
        }
    }
}
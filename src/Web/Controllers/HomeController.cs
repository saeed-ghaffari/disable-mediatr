﻿using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Web.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet("")]
        [RateLimitFilterFactory(Order = 1, Limit = 200, PeriodInSec = 1 * 60 * 2)]
        public IActionResult Index()
        {
            return View();
        }
    }

}
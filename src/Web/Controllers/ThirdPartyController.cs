﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Web.Controllers
{
    public class ThirdPartyController : Controller
    {
        private readonly IThirdPartyServiceService _thirdPartyService;

        public ThirdPartyController(IThirdPartyServiceService partyServiceService)
        {
            _thirdPartyService = partyServiceService;
        }

        [Route("Bourse/FromCompany/{link}")]//oldRoute
        public async Task<IActionResult> ThirdPartyAsync(string link)
        {
            if (string.IsNullOrEmpty(link) || string.IsNullOrWhiteSpace(link)) return RedirectToAction("Index", "Home");
            Response.Cookies.Delete("ServiceLink");
            var partyService = await _thirdPartyService.GetByLinkAsync(link);
            if (partyService != null && partyService.EnabledLink)
            {
                TempData["msg"] = $"شما از لینک  {partyService.Title} وارد شده اید. ";
         
                Response.Cookies.Append("ServiceLink", link, new CookieOptions
                {
                    Expires = DateTimeOffset.Now.AddHours(2)
                });
            }
         
            return RedirectToAction("Index", "Home");
        }
    }
}
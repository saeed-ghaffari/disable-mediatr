﻿using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Web.Controllers
{
    public class StatusCodeController : ControllerBase
    {
        [HttpGet("error/{code}")]
        public IActionResult Index(int code)
        {
            return File($"~/{code}.html", "text/html");
        }

        [HttpPost("error/{code}")]
        public IActionResult GoToErrorPage(int code)
        {
            return File($"~/{code}.html", "text/html");
        }
    }
}
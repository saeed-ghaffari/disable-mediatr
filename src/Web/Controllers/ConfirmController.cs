﻿using System;
using AutoMapper;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Web.Enum;
using Cerberus.Web.Filters;
using Cerberus.Web.ViewModel.Dashboard;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Model;
using Cerberus.Web.Configuration;
using Cerberus.Web.Extensions;
using Cerberus.Web.Model;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.Extensions.Options;

namespace Cerberus.Web.Controllers
{
    [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Confirm)]
    public class ConfirmController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IProfileService _profileService;
        private readonly IOfflineTaskService _offlineTaskService;
        private readonly CookieHelper _cookieHelper;
        private readonly AmountConfiguration _amountConfiguration;
        private readonly IThirdPartyServiceService _thirdPartyServiceService;

        public ConfirmController(IMapper mapper, IProfileService profileService,
            IOfflineTaskService offlineTaskService, CookieHelper cookieHelper,
            IOptions<AmountConfiguration> amountConfiguration, IThirdPartyServiceService thirdPartyServiceService)
        {
            _mapper = mapper;
            _profileService = profileService;
            _offlineTaskService = offlineTaskService;
            _cookieHelper = cookieHelper;
            _amountConfiguration = amountConfiguration.Value;
            _thirdPartyServiceService = thirdPartyServiceService;
        }


        [RateLimitFilterFactory(Order = 1, Limit = 15, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Index()
        {
            var result = await _profileService.GetRelatedDataForDashboard(User.ProfileId, User.ProfileOwnerType);
            switch (User.ProfileOwnerType)
            {

                case ProfileOwnerType.IranianPrivatePerson:

                    var privatePersonViewModel = _mapper.Map<Domain.Model.Profile, PrivatePersonDashboardViewModel>(result);
                    return View("PrivatePersonInfo", privatePersonViewModel);

                case ProfileOwnerType.IranianLegalPerson:

                    var legalPersonViewModel = _mapper.Map<Domain.Model.Profile, LegalPersonDashboardViewModel>(result);
                    return View("LegalPersonInfo", legalPersonViewModel);

                case ProfileOwnerType.ForeignPrivatePerson:
                    break;
                case ProfileOwnerType.ForeignLegalPerson:
                    break;
            }


            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 15, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateAsync(bool isConfirm)
        {

            var profile = await _profileService.GetAsync(User.ProfileId, true);
            var payment = profile.Payments
                ?.FirstOrDefault(x => x.ProfilePackageId == null && x.Status == PaymentStatus.Settle);
            if (payment == null)
            {
                Alert("شما عملیات پرداخت را انجام نداده اید", NotificationType.Error);
                return RedirectToAction("SignOut","Session");
            }

            var profileScope = profile.GetAvailableScopeForConfirm(User.ProfileOwnerType);
            if (profileScope.availableScope == AvailableScope.Non)
            {
                if (profile.Status < ProfileStatus.TraceCode)
                {
                    if (profile.Status==ProfileStatus.PendingValidation)
                    {
                        Alert("تا مشخص نشدن نتیجه استعلام، نمی توانید وارد سامانه شوید",
                            NotificationType.Warning);
                        return RedirectToAction("SignOut", "Session");
                    }
                    ThirdPartyService service = null;

                    if (payment.ReferenceServiceId != null)
                        service = await _thirdPartyServiceService.GetAsync(payment.ReferenceServiceId.GetValueOrDefault());


                    if (
                        ((service != null && !service.HasFactor) ||
                         payment.Discount >= _amountConfiguration.IranianPrivatePerson))
                    {
                    }

                    else
                    {
                        if (profile.Status < ProfileStatus.PolicyAccepted && payment.FactorId == null)
                        {
                            await _offlineTaskService.ScheduleSetFactorAsync(payment.Id, DateTime.Now);
                        }
                    }

                    profile.Status = ProfileStatus.PolicyAccepted;
                    await _profileService.UpdateStatusAsync(profile);
                    await _offlineTaskService.ScheduleOfflineTasksAsync(User.ProfileId,null, DateTime.Now);
                }

                var sessionInfo = new UserSessionInfo
                {
                    ProfileId = profile.Id,
                    ProfileOwnerType = profile.Type,
                    ProfileStatus = profile.Status,
                    AvailableScope = profileScope.availableScope | AvailableScope.Factor,
                    ServiceId = User.ServiceId ?? 0
                };

                await _cookieHelper.CreateCookie(sessionInfo);
                return RedirectToAction("Checkout", "Factor");

            }

            profileScope.availableScope |= AvailableScope.Confirm;
            var tokenPayload = new UserSessionInfo
            {
                ProfileId = profile.Id,
                ProfileOwnerType = profile.Type,
                ProfileStatus = profile.Status,
                AvailableScope = profileScope.availableScope,
                ServiceId = User.ServiceId ?? 0
            };


            await _cookieHelper.CreateCookie(tokenPayload);


            Alert($"اطلاعات {profileScope.missingDataScopeTxt} به درستی وارد نشده است، لطفا تصحیح فرمایید.",
                NotificationType.Error);
            return RedirectToNextStep(tokenPayload.CurrentStep);

        }

    }
}
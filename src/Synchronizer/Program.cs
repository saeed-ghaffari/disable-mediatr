﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Synchronizer.Implementations;
using Cerberus.Synchronizer.Interface;
using Cerberus.Synchronizer.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Options;
using NLog.Extensions.Logging;

namespace Cerberus.Synchronizer
{
    class Program
    {
        static void Main(string[] args)
        {
            var services = new ServiceCollection();
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();

            services.AddCerberusServices(configuration);
            services.AddCdcConsumers();
            services.AddMapper(configuration);
            services.AddSingleton<ILoggerFactory>(provider =>
            {
                var factory = new LoggerFactory();
                factory.AddConsole();
                factory.AddNLog();

                return factory;
            });
            services.AddLogging();

            services.AddSingleton<Implementations.Synchronizer>();

            var serviceProvider = services.BuildServiceProvider();

            var synchronizer = serviceProvider.GetService<Implementations.Synchronizer>();

            synchronizer.Start(1000);

            Console.WriteLine("Start");

            Console.ReadLine();
        }

        private static Task ConsumerOnChangeReceivedAsync(IConsumer<Profile> sender, ChangeData<Profile> data)
        {
            Console.WriteLine(data.Data.UniqueIdentifier);

            return Task.CompletedTask;
        }
    }
}

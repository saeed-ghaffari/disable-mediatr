﻿namespace Cerberus.Synchronizer
{
    public class SynchronizerConfiguration
    {
        public string MaserDbConnection { get; set; }
        public string TasksDbConnection { get; set; }

        public string RedisReadDbHost { get; set; }
        public string RedisReadDbPass { get; set; }
        public string ReadConnection { get; set; }
        public string StatusReportDbConnection { get; set; }

    }
}
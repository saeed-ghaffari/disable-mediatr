﻿using System.Data;
using System.Data.SqlClient;
using Cerberus.Domain.Interface.Repositories;
using Microsoft.Extensions.Options;

namespace Cerberus.Synchronizer.Implementations
{
    public class SqlConnectionProvider : ISqlConnectionProvider
    {
        private readonly IOptions<SynchronizerConfiguration> _options;

        public SqlConnectionProvider(IOptions<SynchronizerConfiguration> options)
        {
            _options = options;
        }

        public IDbConnection GetConnection()
        {
            return new SqlConnection(_options.Value.MaserDbConnection);
        }

        public IDbConnection GetReadConnection()
        {
            return new SqlConnection(_options.Value.ReadConnection);
        }

        public IDbConnection GetRequestLogConnection()
        {
            throw new System.NotImplementedException();
        }
        public IDbConnection GetStatusReportDbConnection()
        {
            return new SqlConnection(_options.Value.StatusReportDbConnection);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Synchronizer.Delegates;
using Cerberus.Synchronizer.Enum;
using Cerberus.Synchronizer.Interface;
using Cerberus.Synchronizer.Model;
using Cerberus.Synchronizer.TypeMappers;
using Dapper;
using Microsoft.Extensions.Logging;

namespace Cerberus.Synchronizer.Implementations
{
    public abstract class Consumer
    {
        static Consumer()
        {
            Dapper.SqlMapper.SetTypeMap(
                typeof(ChangeMetadata),
                new ColumnAttributeTypeMapper<ChangeMetadata>());
        }
    }

    public class Consumer<TEntity> : Consumer, IConsumer<TEntity>, IDisposable
    {
        private readonly ConsumerSettings _settings;
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<Consumer<TEntity>> _logger;
        private readonly ICdcLsnManager _lsnManager;

        private byte[] _startLsn;
        private bool _initialized;
        private System.Threading.Timer _timer;
        private bool _isConsumingPeriodically;
        private bool _isConsuming;

        public Consumer(ConsumerSettings settings, IConnectionProvider connectionProvider, ILogger<Consumer<TEntity>> logger, ICdcLsnManager lsnManager)
        {
            _settings = settings;
            _connectionProvider = connectionProvider;
            _logger = logger;
            _lsnManager = lsnManager;
        }

        protected async Task ConsumeAsync(
            bool internalCall)
        {
            if (_isConsuming)
                throw new Exception("Consume cannot be called while another operation is running");

            if (!internalCall && _isConsumingPeriodically)
                throw new Exception("Cannot consume while a periodic operation is ongoing");

            byte[] lastLsn = _startLsn;
            try
            {
                _isConsuming = true;

                if (!_initialized)
                    Init();

                var offset = 0;

                try
                {
                    // fetch change data
                    // We fetch `fetchLimit` + 1 rows from db to check for update after rows
                    IEnumerable<ChangeData<TEntity>> data = await FetchChangeData(_startLsn, offset);

                    if (data == null || !data.Any())
                        return;

                    using (IEnumerator<ChangeData<TEntity>> iter = data.GetEnumerator())
                    {
                        iter.MoveNext();
                        for (; iter.Current != null; iter.MoveNext())
                        {
                            // merge update data and raise events
                            ChangeData<TEntity> changeData = iter.Current;

                            if (changeData.Metadata.Operation == ChangeDataOperation.UpdateBefore)
                            {
                                if (!iter.MoveNext())
                                {
                                    await OnChangeReceivedAsync(changeData);

                                    lastLsn = changeData.Metadata.StartLsn;
                                    _logger.LogDebug($"Found an UpdateBefore operation without its corresponding UpdateAfter (Row didn't exist) [OF:{offset}, FL:{_settings.FetchLimit}]");

                                    continue;
                                }


                                iter.Current.BeforeData = changeData.Data;
                                changeData = iter.Current;

                                if (changeData.Metadata.Operation != ChangeDataOperation.UpdateAfter)
                                    throw new Exception(
                                        $"Found an UpdateBefore operation without its corresponding UpdateAfter [OF:{offset}, FL:{_settings.FetchLimit}]");
                            }

                            await OnChangeReceivedAsync(changeData);

                            lastLsn = changeData.Metadata.StartLsn;

                            UpdateLsn(lastLsn);
                        }
                    }

                    _startLsn = lastLsn;

                }
                catch (Exception ex) when (OnException != null)
                {
                    ExceptionHandlingMode mode = OnException.Invoke(this, ex);
                    switch (mode)
                    {
                        case ExceptionHandlingMode.Throw:
                            throw;
                        case ExceptionHandlingMode.Stop:
                            this.StopConsuming();
                            return;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }


            }
            finally
            {
                _isConsuming = false;
            }

        }

        private void UpdateLsn(byte[] lsn)
        {
            _lsnManager.UpdateLsn(typeof(TEntity), lsn);
        }

        public void StartConsuming(int interval)
        {
            _isConsumingPeriodically = true;

            if (_timer == null)
                _timer = new System.Threading.Timer(DoConsume, null, 0, interval);
            else
                _timer.Change(0, interval);
        }

        public void StopConsuming()
        {
            _isConsumingPeriodically = false;
            _timer.Change(int.MaxValue, int.MaxValue);
        }

        private void DoConsume(object state)
        {
            try
            {
                if (!_isConsuming)
                    ConsumeAsync(internalCall: true).Wait();
                else
                {
                    Console.WriteLine("already do consuming");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _logger.LogCritical(e, "throw exception in consume CDC");
            }
        }

        public event ChangeReceivedAsyncEventHandler<TEntity> ChangeReceivedAsync;
        public event ConsumerExceptionEventHandler OnException;


        protected virtual async Task OnChangeReceivedAsync(ChangeData<TEntity> data)
        {
            var handler = ChangeReceivedAsync;
            if (handler == null)
                return;

            var invocationList = handler.GetInvocationList();

            foreach (var del in invocationList)
            {
                await ((ChangeReceivedAsyncEventHandler<TEntity>)del)(this, data);
            }
        }

        private void Init()
        {
            if (_initialized)
                return;
            _initialized = true;

            _startLsn = _lsnManager.GetStartingLsn(typeof(TEntity));
        }

        private async Task<IEnumerable<ChangeData<TEntity>>> FetchChangeData(byte[] startLsn,
            int offset)
        {
            using (var con = _connectionProvider.GetConnection())
            {
                return await con.QueryAsync<ChangeMetadata, TEntity, ChangeData<TEntity>>(
                    $@"select sys.fn_cdc_map_lsn_to_time (__$start_lsn) as ModifyDate ,* from {_settings.ChangeTable}
{(startLsn != null ? "where __$start_lsn > @startLsn" : "")}
order by __$start_lsn asc
offset {offset} rows fetch next {_settings.FetchLimit + 1} rows only",
                    (metadata, entity) => new ChangeData<TEntity>
                    {
                        Metadata = metadata,
                        Data = entity
                    }, new
                    {
                        startLsn
                    }
                );
            }
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
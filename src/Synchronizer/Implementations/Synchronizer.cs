﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Membership;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.Synchronizer.Enum;
using Cerberus.Synchronizer.Interface;
using Cerberus.Synchronizer.Model;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Komodo.Redis.StackExchange;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Profile = Cerberus.Domain.Model.Profile;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.Synchronizer.Implementations
{
    public class Synchronizer : IDisposable
    {
        private readonly IConsumer<Profile> _profileConsumer;
        private readonly IConsumer<PrivatePerson> _privatePersonConsumer;
        private readonly IConsumer<LegalPerson> _legalPersonConsumer;
        private readonly IConsumer<Agent> _agentConsumer;
        private readonly IConsumer<AgentDocument> _agentDocumentConsumer;
        private readonly IConsumer<Address> _addressConsumer;
        private readonly IConsumer<TradingCode> _tradingCodeConsumer;
        private readonly IConsumer<BankingAccount> _bankingAccountConsumer;
        private readonly IConsumer<FinancialInfo> _financialInfoConsumer;
        private readonly IConsumer<FinancialBroker> _financialBrokerConsumer;
        private readonly IConsumer<JobInfo> _jobInfoConsumer;
        private readonly IConsumer<LegalPersonShareholder> _legalPersonShareHolderConsumer;
        private readonly IConsumer<LegalPersonStakeholder> _legalPersonStakeHolderConsumer;
        private readonly IConsumer<ThirdPartyService> _thirdPartyServiceConsumer;
        private readonly IConsumer<PrivatePersonMembership> _privatePersonMembershipConsumer;
        private readonly IConsumer<BankingAccountMembership> _bankingAccountMembershipConsumer;
        private readonly IConsumer<AddressMembership> _addressMembershipConsumer;
        private readonly IConsumer<PermanentOtp> _permanentOtpConsumer;
        private readonly List<IConsumer> _consumers;
        private readonly ITaskRepository _taskRepository;
        private readonly IFinancialInfoRepository _financialInfoRepository;
        private readonly IAgentRepository _agentRepository;
        private readonly ILogger<Synchronizer> _logger;
        private readonly ISerializer _serializer;
        private readonly IMapper _mapper;

        public Synchronizer(
            IConsumer<ThirdPartyService> thirdPartyServiceConsumer,
            IConsumer<LegalPersonStakeholder> legalPersonStakeHolderConsumer,
            IConsumer<LegalPersonShareholder> legalPersonShareHolderConsumer,
            IConsumer<JobInfo> jobInfoConsumer,
            IConsumer<FinancialBroker> financialBrokerConsumer,
            IConsumer<FinancialInfo> financialInfoConsumer,
            IConsumer<BankingAccount> bankingAccountConsumer,
            IConsumer<TradingCode> tradingCodeConsumer,
            IConsumer<Address> addressConsumer,
            IConsumer<AgentDocument> agentDocumentConsumer,
            IConsumer<Agent> agentConsumer,
            IConsumer<LegalPerson> legalPersonConsumer,
            IConsumer<PrivatePerson> privatePersonConsumer,
            IConsumer<Profile> profileConsumer,
            IConsumer<PrivatePersonMembership> privatePersonMembershipConsumer,
            IConsumer<BankingAccountMembership> bankingAccountMembershipConsumer, IConsumer<AddressMembership> addressMembershipConsumer, IConsumer<PermanentOtp> permanentOtpConsumer,
            ITaskRepository taskRepository, IFinancialInfoRepository financialInfoRepository, IAgentRepository agentRepository, ILogger<Synchronizer> logger, IMapper mapper, ISerializer serializer)
        {
            _consumers = new List<IConsumer>();

            //third party
            _consumers.Add(thirdPartyServiceConsumer);
            _thirdPartyServiceConsumer = thirdPartyServiceConsumer;
            _thirdPartyServiceConsumer.ChangeReceivedAsync += ThirdPartyServiceConsumerOnChangeReceivedAsync;
            _thirdPartyServiceConsumer.OnException += ConsumerOnOnException;


            //legal stakeholder
            _legalPersonStakeHolderConsumer = legalPersonStakeHolderConsumer;
            _consumers.Add(legalPersonStakeHolderConsumer);
            _legalPersonStakeHolderConsumer.ChangeReceivedAsync += LegalPersonStakeHolderConsumerOnChangeReceivedAsync;
            _legalPersonStakeHolderConsumer.OnException += ConsumerOnOnException;

            //legal shareholder
            _consumers.Add(legalPersonShareHolderConsumer);
            _legalPersonShareHolderConsumer = legalPersonShareHolderConsumer;
            _legalPersonShareHolderConsumer.ChangeReceivedAsync += LegalPersonShareHolderConsumerOnChangeReceivedAsync;
            _legalPersonShareHolderConsumer.OnException += ConsumerOnOnException;


            //job info
            _jobInfoConsumer = jobInfoConsumer;
            _consumers.Add(jobInfoConsumer);
            _jobInfoConsumer.ChangeReceivedAsync += JobInfoConsumerOnChangeReceivedAsync;
            _jobInfoConsumer.OnException += ConsumerOnOnException;

            //financial broker
            _financialBrokerConsumer = financialBrokerConsumer;
            _consumers.Add(financialBrokerConsumer);
            _financialBrokerConsumer.ChangeReceivedAsync += FinancialBrokerConsumerOnChangeReceivedAsync;
            _financialBrokerConsumer.OnException += ConsumerOnOnException;


            //financial info
            _financialInfoConsumer = financialInfoConsumer;
            _consumers.Add(financialInfoConsumer);
            _financialInfoConsumer.ChangeReceivedAsync += FinancialInfoConsumerOnChangeReceivedAsync;
            _financialInfoConsumer.OnException += ConsumerOnOnException;

            //banking account
            _bankingAccountConsumer = bankingAccountConsumer;
            _consumers.Add(bankingAccountConsumer);
            _bankingAccountConsumer.ChangeReceivedAsync += BankingAccountConsumerOnChangeReceivedAsync;
            _bankingAccountConsumer.OnException += ConsumerOnOnException;

            //trading code
            _tradingCodeConsumer = tradingCodeConsumer;
            _consumers.Add(tradingCodeConsumer);
            _tradingCodeConsumer.ChangeReceivedAsync += TaradingCodeConsumerOnChangeReceivedAsync;
            _tradingCodeConsumer.OnException += ConsumerOnOnException;

            //addresses
            _addressConsumer = addressConsumer;
            _consumers.Add(addressConsumer);
            _addressConsumer.ChangeReceivedAsync += AddressConsumerOnChangeReceivedAsync;
            _addressConsumer.OnException += ConsumerOnOnException;

            //agent document
            _agentDocumentConsumer = agentDocumentConsumer;
            _consumers.Add(agentDocumentConsumer);
            _agentDocumentConsumer.ChangeReceivedAsync += AgentDocumentConsumerOnChangeReceivedAsync;
            _agentDocumentConsumer.OnException += ConsumerOnOnException;

            //agent
            _agentConsumer = agentConsumer;
            _consumers.Add(agentConsumer);
            _agentConsumer.ChangeReceivedAsync += AgentConsumerOnChangeReceivedAsync;
            _agentConsumer.OnException += ConsumerOnOnException;


            //legal person
            _legalPersonConsumer = legalPersonConsumer;
            _consumers.Add(legalPersonConsumer);
            _legalPersonConsumer.ChangeReceivedAsync += LegalPersonConsumerOnChangeReceivedAsync;
            _legalPersonConsumer.OnException += ConsumerOnOnException;

            //private person
            _privatePersonConsumer = privatePersonConsumer;
            _consumers.Add(privatePersonConsumer);
            _privatePersonConsumer.ChangeReceivedAsync += PrivatePersonConsumerOnChangeReceivedAsync;
            _privatePersonConsumer.OnException += ConsumerOnOnException;

            //private person membership
            _privatePersonMembershipConsumer = privatePersonMembershipConsumer;
            _consumers.Add(privatePersonMembershipConsumer);
            _privatePersonMembershipConsumer.ChangeReceivedAsync += PrivatePersonMembershipOnChangeReceivedAsync;
            _privatePersonMembershipConsumer.OnException += ConsumerOnOnException;

            //address membership
            _addressMembershipConsumer = addressMembershipConsumer;
            _addressMembershipConsumer.ChangeReceivedAsync += AddressMembershipConsumerOnChangeReceivedAsync;
            _addressMembershipConsumer.OnException += ConsumerOnOnException;
            _consumers.Add(addressMembershipConsumer);


            //banking accounts membership
            _bankingAccountMembershipConsumer = bankingAccountMembershipConsumer;
            _bankingAccountMembershipConsumer.ChangeReceivedAsync += BankingAccountMembershipOnChangeReceivedAsync;
            _bankingAccountMembershipConsumer.OnException += ConsumerOnOnException;
            _consumers.Add(bankingAccountMembershipConsumer);

            //PermanentOtp
            _permanentOtpConsumer = permanentOtpConsumer;
            _consumers.Add(permanentOtpConsumer);
            _permanentOtpConsumer.ChangeReceivedAsync += PermanentOtpOnChangeReceivedAsync;
            _permanentOtpConsumer.OnException += ConsumerOnOnException;

            //profile
            _profileConsumer = profileConsumer;
            _taskRepository = taskRepository;
            _financialInfoRepository = financialInfoRepository;
            _agentRepository = agentRepository;
            _logger = logger;
            _mapper = mapper;
            _serializer = serializer;
            _consumers.Add(profileConsumer);
            _profileConsumer.ChangeReceivedAsync += ProfileConsumerOnChangeReceivedAsync;
            _profileConsumer.OnException += ConsumerOnOnException;



        }

        private Task AddressMembershipConsumerOnChangeReceivedAsync(IConsumer<AddressMembership> sender, ChangeData<AddressMembership> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.Id}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.AddressMembership,
                ServiceId = data.Data.ServiceId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
                UniqueIdentifier = data.Data.UniqueIdentifier,
                DataSerialized = _serializer.Serialize(data.Data)
            }, data.Data.Id);
        }

        private Task BankingAccountMembershipOnChangeReceivedAsync(IConsumer<BankingAccountMembership> sender, ChangeData<BankingAccountMembership> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.Id}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.BankingAccountMembership,
                ServiceId = data.Data.ServiceId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
                UniqueIdentifier = data.Data.UniqueIdentifier,
                DataSerialized = _serializer.Serialize(data.Data)
            }, data.Data.Id);
        }

        private Task PrivatePersonMembershipOnChangeReceivedAsync(IConsumer<PrivatePersonMembership> sender, ChangeData<PrivatePersonMembership> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.Id}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.PrivatePersonMembership,
                ServiceId = data.Data.ServiceId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
                UniqueIdentifier = data.Data.UniqueIdentifier,
                DataSerialized = _serializer.Serialize(data.Data)

            }, data.Data.Id);
        }

        private Task AddSynchronizerTask(SynchronizeTaskParams param, long refId)
        {
            return _taskRepository.AddAsync(new ScheduledTask()
            {
                Type = CerberusTasksIdentifierHelper.Synchronizer.Value,
                DueTime = DateTime.Now,
                Status = TaskStatus.Pending,
                GroupId = CerberusTasksIdentifierHelper.Synchronizer.Key,
                Params = JsonConvert.SerializeObject(param),
                RefId = refId
            });
        }

        public async Task FinancialBrokerConsumerOnChangeReceivedAsync(IConsumer<FinancialBroker> sender, ChangeData<FinancialBroker> data)
        {
            var financialInfo = await _financialInfoRepository.GetAsync(data.Data.FinancialInfoId);
            if (financialInfo == null)
                return;

            await AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.FinancialInfoBroker,
                ProfileId = financialInfo.ProfileId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
            }, financialInfo.Id);

            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.Id}");

        }

        public Task FinancialInfoConsumerOnChangeReceivedAsync(IConsumer<FinancialInfo> sender, ChangeData<FinancialInfo> data)
        {

            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.Id}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.FinancialInfo,
                ProfileId = data.Data.ProfileId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
            }, data.Data.Id);

        }

        public Task BankingAccountConsumerOnChangeReceivedAsync(IConsumer<BankingAccount> sender, ChangeData<BankingAccount> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.ProfileId}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.BankingAccount,
                ProfileId = data.Data.ProfileId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
            }, data.Data.Id);
        }

        public Task TaradingCodeConsumerOnChangeReceivedAsync(IConsumer<TradingCode> sender, ChangeData<TradingCode> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.ProfileId}");
            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.TradingCode,
                ProfileId = data.Data.ProfileId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
            }, data.Data.Id);
        }

        public Task AddressConsumerOnChangeReceivedAsync(IConsumer<Address> sender, ChangeData<Address> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.ProfileId}");
            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.Address,
                ProfileId = data.Data.ProfileId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
            }, data.Data.Id);
        }

        public async Task AgentDocumentConsumerOnChangeReceivedAsync(IConsumer<AgentDocument> sender, ChangeData<AgentDocument> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.AgentId}");
            var agent = await _agentRepository.GetAsync(data.Data.AgentId);
            if (agent == null)
                return;

            await AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.AgentDocument,
                ProfileId = agent.ProfileId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
            }, agent.Id);
        }

        public Task AgentConsumerOnChangeReceivedAsync(IConsumer<Agent> sender, ChangeData<Agent> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.ProfileId}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.Agent,
                ProfileId = data.Data.ProfileId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
            }, data.Data.Id);
        }

        public Task LegalPersonConsumerOnChangeReceivedAsync(IConsumer<LegalPerson> sender, ChangeData<LegalPerson> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.ProfileId}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.LegalPerson,
                ProfileId = data.Data.ProfileId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
                // DataSerialized = _serializer.Serialize(_mapper.Map<LegalPerson, LegalPersonViewModel>(data.Data))
            }, data.Data.Id);
        }

        public Task PrivatePersonConsumerOnChangeReceivedAsync(IConsumer<PrivatePerson> sender, ChangeData<PrivatePerson> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.ProfileId}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.PrivatePerson,
                ProfileId = data.Data.ProfileId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
                // DataSerialized = _serializer.Serialize(_mapper.Map<PrivatePerson, PrivatePersonViewModel>(data.Data))
            }, data.Data.Id);
        }

        public Task ProfileConsumerOnChangeReceivedAsync(IConsumer<Profile> sender, ChangeData<Profile> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.UniqueIdentifier}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.Profile,
                ProfileId = data.Data.Id,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
                UniqueIdentifier = data.Data.UniqueIdentifier,
                DataSerialized = _serializer.Serialize(_mapper.Map<Profile, ProfileViewModel>(data.Data))
            }, data.Data.Id);
        }

        public Task JobInfoConsumerOnChangeReceivedAsync(IConsumer<JobInfo> sender, ChangeData<JobInfo> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.ProfileId}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.JobInfo,
                ProfileId = data.Data.ProfileId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
            }, data.Data.Id);
        }

        private ExceptionHandlingMode ConsumerOnOnException(IConsumer sender, Exception exception)
        {
            _logger.LogCritical(exception, "ConsumerOnOnException");

            return ExceptionHandlingMode.Throw;
        }

        public Task LegalPersonShareHolderConsumerOnChangeReceivedAsync(IConsumer<LegalPersonShareholder> sender, ChangeData<LegalPersonShareholder> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.ProfileId}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.LegalPersonShareholder,
                ProfileId = data.Data.ProfileId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
            }, data.Data.Id);
        }

        public Task LegalPersonStakeHolderConsumerOnChangeReceivedAsync(IConsumer<LegalPersonStakeholder> sender, ChangeData<LegalPersonStakeholder> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.ProfileId}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.LegalPersonStakeholder,
                ProfileId = data.Data.ProfileId,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
            }, data.Data.Id);
        }

        public Task ThirdPartyServiceConsumerOnChangeReceivedAsync(IConsumer<ThirdPartyService> sender, ChangeData<ThirdPartyService> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.Id}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.ThirdPartyService,
                RefId = data.Data.Id,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
            }, data.Data.Id);
        }

        private Task PermanentOtpOnChangeReceivedAsync(IConsumer<PermanentOtp> sender, ChangeData<PermanentOtp> data)
        {
            _logger.LogInformation($"{data.Metadata.ModifyDate} {data.Data.GetType().Name}  {data.Data.Id}");

            return AddSynchronizerTask(new SynchronizeTaskParams()
            {
                Target = SynchronizerTarget.PermanentOtp,
                IsDeleted = data.Metadata.Operation == ChangeDataOperation.Delete,
                DataSerialized = _serializer.Serialize(data.Data)

            }, data.Data.Id);
        }


        public void Start(int interval)
        {
            _consumers.ForEach(x => x.StartConsuming(interval));
        }

        public void Stop()
        {
            _consumers.ForEach(x => x.StopConsuming());
        }

        public void Dispose()
        {
            Stop();
        }
    }
}
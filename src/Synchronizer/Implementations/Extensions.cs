﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Synchronizer.Implementations
{
    public static class ByteArrayExtensions
    {
        public static string ToHexString(this byte[] bytes)
        {
            return "0x" + BitConverter.ToString(bytes).Replace("-", "");
        }
    }
}

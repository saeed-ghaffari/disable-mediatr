﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Timers;
using Cerberus.Synchronizer.Interface;
using Newtonsoft.Json;

namespace Cerberus.Synchronizer.Implementations
{
    public class CdcLsnManager : ICdcLsnManager
    {
        private readonly ConcurrentDictionary<string, string> _dic;
        private readonly string _filename = "lsn.config";
        private object _obj = new object();

        public CdcLsnManager()
        {
            _dic = new ConcurrentDictionary<string, string>();

            if (File.Exists(_filename))
                _dic = JsonConvert.DeserializeObject<ConcurrentDictionary<string, string>>(File.ReadAllText(_filename));

            var timer = new Timer(10000);

            timer.Elapsed += delegate (object sender, ElapsedEventArgs args)
             {
                 lock (_obj)
                 {
                     if (_dic.Count != 0)
                         File.WriteAllText(_filename, JsonConvert.SerializeObject(_dic, Formatting.Indented));
                 }

             };

            timer.Start();
        }



        public byte[] GetStartingLsn(Type type)
        {
            if (!_dic.TryGetValue(type.FullName ?? throw new InvalidOperationException("invalid type to sync last LSN"), out var lsn))
                return null;

            return StringToByteArray(lsn);
        }

        public void UpdateLsn(Type type, byte[] lsn)
        {
            string lsnStr = $"0x{BitConverter.ToString(lsn)}".Replace("-", "");

            _dic.AddOrUpdate(type.FullName ?? throw new InvalidOperationException("invalid type to sync last LSN"), lsnStr, (key, value) => lsnStr);
        }

        public byte[] StringToByteArray(string hex)
        {
            if (hex.StartsWith("0x"))
                hex = hex.Substring(2, hex.Length - 2);

            byte[] bytes = new byte[hex.Length / 2];

            for (int i = 0; i < hex.Length; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}
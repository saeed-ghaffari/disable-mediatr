﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Cerberus.Synchronizer.Interface;
using Microsoft.Extensions.Options;

namespace Cerberus.Synchronizer.Implementations
{
    public class ConnectionProvider : IConnectionProvider
    {
        private readonly IOptions<SynchronizerConfiguration> _options;

        public ConnectionProvider(IOptions<SynchronizerConfiguration> options)
        {
            _options = options;
        }

        public IDbConnection GetConnection()
        {
            return new SqlConnection(_options.Value.MaserDbConnection);
        }
    }
}

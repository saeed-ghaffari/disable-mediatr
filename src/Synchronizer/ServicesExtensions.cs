﻿using System;
using AutoMapper;
using Cerberus.ApiService.Implementations;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Membership;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Repository;
using Cerberus.Repository.MsSql;
using Cerberus.Service.Implementations;
using Cerberus.Synchronizer.Implementations;
using Cerberus.Synchronizer.Interface;
using Cerberus.Synchronizer.Model;
using Cerberus.TasksManager.Core;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Repository;
using Komodo.Caching.Abstractions;
using Komodo.Caching.Redis;
using Komodo.Redis.StackExchange;
using Komodo.Redis.StackExchange.ProtoBuf;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using Profile = AutoMapper.Profile;

namespace Cerberus.Synchronizer
{
    public static class ServicesExtensions
    {
        public static void AddCerberusServices(this IServiceCollection services, IConfiguration configuration)
        {
            //Configurations

            services.Configure<SynchronizerConfiguration>(configuration.GetSection("SynchronizerConfiguration"));

            //dbcontext
            services.AddDbContext<CerberusDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetValue<string>("SynchronizerConfiguration:MaserDbConnection"));
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

            }, ServiceLifetime.Transient);


            services.AddTransient<IConnectionProvider, ConnectionProvider>();
            services.AddTransient<ISqlConnectionProvider, SqlConnectionProvider>();

            //Redis
            var redisConfiguration = new ConfigurationOptions();
            foreach (var endpoint in configuration.GetValue<string>("SynchronizerConfiguration:RedisWriteDbHost").Split(';'))
            {
                redisConfiguration.EndPoints.Add(endpoint);
            }
            redisConfiguration.Password = configuration.GetValue<string>("SynchronizerConfiguration:RedisWriteDbPass");
            redisConfiguration.ConnectTimeout = 400;
            services.AddSingleton<ConfigurationOptions>(redisConfiguration);

            services.AddSingleton<IConnectionMultiplexerProvider>(new ConnectionMultiplexerProvider(redisConfiguration, Environment.ProcessorCount * 2));

            services.AddSingleton<IDataBaseProvider, DatabaseProvider>();

            services.AddTransient<IDatabase>(provider => provider.GetService<IDataBaseProvider>().GetWriteDatabase());

            services.AddSingleton<ISerializer, ProtobufSerializer>();

            services.AddTransient<ICacheProvider>(provider => new RedisCacheProvider(
                provider.GetService<IDatabase>(),
                provider.GetService<IDatabase>(),
                provider.GetService<ISerializer>()));

            //Repositories

            //---gatekeeper 

            services.AddTransient<IThirdPartyServiceRepository, ThirdPartyServiceRepository>();
            services.AddTransient<IProfileRepository, ProfileRepository>();
            services.AddTransient<IAgentRepository, AgentRepository>();
            services.AddTransient<IAgentDocumentRepository, AgentDocumentRepository>();
            services.AddTransient<IFileRepository, FileRepository>();
            services.AddTransient<IPrivatePersonRepository, PrivatePersonRepository>();
            services.AddTransient<IAddressRepository, AddressRepository>();
            services.AddTransient<IBankingAccountRepository, BankingAccountRepository>();
            services.AddTransient<IBrokerRepository, BrokerRepository>();
            services.AddTransient<ICountryRepository, CountryRepository>();
            services.AddTransient<IFinancialInfoRepository, FinancialInfoRepository>();
            services.AddTransient<IJobInfoRepository, JobInfoRepository>();
            services.AddTransient<IJobRepository, JobRepository>();
            services.AddTransient<IPrivatePersonRepository, PrivatePersonRepository>();
            services.AddTransient<ITradingCodeRepository, TradingCodeRepository>();
            services.AddTransient<IProvinceRepository, ProvinceRepository>();
            services.AddTransient<ICityRepository, CityRepository>();
            services.AddTransient<IAddressSectionRepository, AddressSectionRepository>();
            services.AddTransient<IBankRepository, BankRepository>();
            services.AddTransient<IPaymentRepository, PaymentRepository>();
            services.AddTransient<ILegalPersonShareholderRepository, LegalPersonShareholderRepository>();
            services.AddTransient<ILegalPersonStakeholderRepository, LegalPersonStakeholderRepository>();
            services.AddTransient<ILegalPersonRepository, LegalPersonRepository>();
            services.AddTransient<IPermanentOtpRepository, PermanentOtpRepository>();

            services.AddSingleton<IApiRequestLogRepository, ApiRequestLogRepository>();

            //todo:inject connection string
            Connection.SetConnectionString(configuration.GetValue<string>("SynchronizerConfiguration:TasksDbConnection"));
            services.AddTransient<ITaskRepository, SqlTaskRepository>();

            //services
            //----Api Services
            services.AddTransient<IAddressApiService, AddressApiService>();
            services.AddTransient<IProfileApiService, ProfileApiService>();
            services.AddTransient<IBankingAccountApiService, BankingAccountApiService>();
            services.AddTransient<IFinancialInfoApiService, FinancialInfoApiService>();
            services.AddTransient<IJobInfoApiService, JobInfoApiService>();
            services.AddTransient<ILegalPersonApiService, LegalPersonApiService>();
            services.AddTransient<IPrivatePersonApiService, PrivatePersonApiService>();
            services.AddTransient<ITradingCodeApiService, TradingCodesApiService>();

            services.AddTransient<IThirdPartyServiceService, ThirdPartyServiceService>();
            services.AddTransient<IProfileService, ProfileService>();
            services.AddTransient<IAgentService, AgentService>();
            services.AddTransient<IAgentDocumentService, AgentDocumentService>();
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<IPrivatePersonService, PrivatePersonService>();
            services.AddTransient<IAddressService, AddressService>();
            services.AddTransient<IBankingAccountService, BankingAccountService>();
            services.AddTransient<IBrokerService, BrokerService>();
            services.AddTransient<ICountryService, CountryService>();
            services.AddTransient<IFinancialInfoService, FinancialInfoService>();
            services.AddTransient<IJobInfoService, JobInfoService>();
            services.AddTransient<IJobService, JobService>();
            services.AddTransient<IPrivatePersonService, PrivatePersonService>();
            services.AddTransient<ITradingCodeService, TradingCodeService>();
            services.AddTransient<IBankService, BankService>();
            services.AddTransient<IProvinceService, ProvinceService>();
            services.AddTransient<ICityService, CityService>();
            services.AddTransient<IAddressSectionService, AddressSectionService>();
            services.AddTransient<IBankService, BankService>();
            services.AddTransient<ILegalPersonShareholderService, LegalPersonShareholderService>();
            services.AddTransient<ILegalPersonStakeholderService, LegalPersonStakeholderService>();
            services.AddTransient<ILegalPersonService, LegalPersonService>();
            services.AddTransient<IPermanentOtpService, PermanentOtpService>();

            services.AddSingleton<ICdcLsnManager, CdcLsnManager>();

        }

        public static void AddCdcConsumers(this IServiceCollection services)
        {
            services.AddSingleton<IConsumer<Domain.Model.Profile>>(provider => new Consumer<Domain.Model.Profile>(new ConsumerSettings()
            {
                CaptureInstance = nameof(Domain.Model.Profile),
                ChangeTable = "cdc.dbo_Profile_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.Profile>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<PrivatePerson>>(provider => new Consumer<PrivatePerson>(new ConsumerSettings()
            {
                CaptureInstance = nameof(PrivatePerson),
                ChangeTable = "cdc.dbo_PrivatePerson_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.PrivatePerson>>>(), provider.GetService<ICdcLsnManager>()));


            services.AddSingleton<IConsumer<ThirdPartyService>>(provider => new Consumer<ThirdPartyService>(new ConsumerSettings()
            {
                CaptureInstance = nameof(ThirdPartyService),
                ChangeTable = "cdc.dbo_ThirdPartyService_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.ThirdPartyService>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<LegalPersonStakeholder>>(provider => new Consumer<LegalPersonStakeholder>(new ConsumerSettings()
            {
                CaptureInstance = nameof(LegalPersonStakeholder),
                ChangeTable = "cdc.dbo_LegalPersonStakeholder_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.LegalPersonStakeholder>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<LegalPersonShareholder>>(provider => new Consumer<LegalPersonShareholder>(new ConsumerSettings()
            {
                CaptureInstance = nameof(LegalPersonShareholder),
                ChangeTable = "cdc.dbo_LegalPersonShareholder_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.LegalPersonShareholder>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<JobInfo>>(provider => new Consumer<JobInfo>(new ConsumerSettings()
            {
                CaptureInstance = nameof(JobInfo),
                ChangeTable = "cdc.dbo_JobInfo_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.JobInfo>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<FinancialBroker>>(provider => new Consumer<FinancialBroker>(new ConsumerSettings()
            {
                CaptureInstance = nameof(FinancialBroker),
                ChangeTable = "cdc.dbo_FinancialBroker_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.FinancialBroker>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<FinancialInfo>>(provider => new Consumer<FinancialInfo>(new ConsumerSettings()
            {
                CaptureInstance = nameof(FinancialInfo),
                ChangeTable = "cdc.dbo_FinancialInfo_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.FinancialInfo>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<BankingAccount>>(provider => new Consumer<BankingAccount>(new ConsumerSettings()
            {
                CaptureInstance = nameof(BankingAccount),
                ChangeTable = "cdc.dbo_BankingAccount_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.BankingAccount>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<TradingCode>>(provider => new Consumer<TradingCode>(new ConsumerSettings()
            {
                CaptureInstance = nameof(TradingCode),
                ChangeTable = "cdc.dbo_TradingCode_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.TradingCode>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<Address>>(provider => new Consumer<Address>(new ConsumerSettings()
            {
                CaptureInstance = nameof(Address),
                ChangeTable = "cdc.dbo_Address_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.Address>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<AgentDocument>>(provider => new Consumer<AgentDocument>(new ConsumerSettings()
            {
                CaptureInstance = nameof(AgentDocument),
                ChangeTable = "cdc.dbo_AgentDocument_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.AgentDocument>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<Agent>>(provider => new Consumer<Agent>(new ConsumerSettings()
            {
                CaptureInstance = nameof(Agent),
                ChangeTable = "cdc.dbo_Agent_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.Agent>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<LegalPerson>>(provider => new Consumer<LegalPerson>(new ConsumerSettings()
            {
                CaptureInstance = nameof(LegalPerson),
                ChangeTable = "cdc.dbo_LegalPerson_CT"
            }, provider.GetService<IConnectionProvider>(), provider.GetService<ILogger<Consumer<Domain.Model.LegalPerson>>>(), provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<PrivatePersonMembership>>(provider =>
                new Consumer<Domain.Model.Membership.PrivatePersonMembership>(new ConsumerSettings()
                {
                    CaptureInstance = nameof(PrivatePersonMembership),
                    ChangeTable = "cdc.mem_PrivatePersonMembership_CT"
                }, provider.GetService<IConnectionProvider>(),
                    provider.GetService<ILogger<Consumer<Domain.Model.Membership.PrivatePersonMembership>>>(),
                    provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<BankingAccountMembership>>(provider =>
                new Consumer<Domain.Model.Membership.BankingAccountMembership>(new ConsumerSettings()
                {
                    CaptureInstance = nameof(BankingAccountMembership),
                    ChangeTable = "cdc.mem_BankingAccountMembership_CT"
                }, provider.GetService<IConnectionProvider>(),
                    provider.GetService<ILogger<Consumer<Domain.Model.Membership.BankingAccountMembership>>>(),
                    provider.GetService<ICdcLsnManager>()));

            services.AddSingleton<IConsumer<AddressMembership>>(provider =>
                new Consumer<Domain.Model.Membership.AddressMembership>(new ConsumerSettings()
                {
                    CaptureInstance = nameof(AddressMembership),
                    ChangeTable = "cdc.mem_AddressMembership_CT"
                }, provider.GetService<IConnectionProvider>(),
                    provider.GetService<ILogger<Consumer<Domain.Model.Membership.AddressMembership>>>(),
                    provider.GetService<ICdcLsnManager>()));


            services.AddSingleton<IConsumer<PermanentOtp>>(provider =>
                new Consumer<PermanentOtp>(new ConsumerSettings()
                    {
                        CaptureInstance = nameof(PermanentOtp),
                        ChangeTable = "cdc.dbo_PermanentOtp_CT"
                }, provider.GetService<IConnectionProvider>(),
                    provider.GetService<ILogger<Consumer<PermanentOtp>>>(),
                    provider.GetService<ICdcLsnManager>()));
        }

        public static void AddMapper(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(provider =>
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<Domain.Model.Profile, ProfileViewModel>();
                    cfg.CreateMap<PrivatePerson, PrivatePersonViewModel>();
                    cfg.CreateMap<LegalPerson, LegalPersonViewModel>();
                    cfg.CreateMap<Address, AddressViewModel>();
                    cfg.CreateMap<TradingCode, TradingCodeViewModel>();
                    cfg.CreateMap<Agent, AgentViewModel>();
                    cfg.CreateMap<BankingAccount, BankingAccountViewModel>();
                    cfg.CreateMap<JobInfo, JobInfoViewModel>();
                    cfg.CreateMap<FinancialInfo, FinancialInfoViewModel>();
                    cfg.CreateMap<LegalPersonShareholder, LegalPersonShareholderViewModel>();
                    cfg.CreateMap<Country, CountryViewModel>();
                    cfg.CreateMap<Province, ProvinceViewModel>();
                    cfg.CreateMap<City, CityViewModel>();
                    cfg.CreateMap<AddressSection, AddressSection>();
                    cfg.CreateMap<Broker, BrokerViewModel>();
                    cfg.CreateMap<FinancialBroker, FinancialBrokerViewModel>();
                    cfg.CreateMap<Bank, BankViewModel>();
                    cfg.CreateMap<Job, JobViewModel>();

                    cfg.CreateMap<File, FileViewModel>()
                        .ForMember(file => file.FileName,
                            exp => exp.ResolveUsing(fileViewModel =>
                                fileViewModel.FileName
                                    = $"{configuration.GetValue<string>("FileStorageConfiguration:CdnFilePath")}/{fileViewModel.FileName}"));

                });
                return config.CreateMapper();
            });
        }

    }
}
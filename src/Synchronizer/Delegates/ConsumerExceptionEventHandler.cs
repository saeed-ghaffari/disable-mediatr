﻿using System;
using Cerberus.Synchronizer.Enum;
using Cerberus.Synchronizer.Interface;

namespace Cerberus.Synchronizer.Delegates
{
    public delegate ExceptionHandlingMode ConsumerExceptionEventHandler(IConsumer sender, Exception exception);
}
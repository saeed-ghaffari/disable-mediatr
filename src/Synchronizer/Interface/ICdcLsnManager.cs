﻿using System;

namespace Cerberus.Synchronizer.Interface
{
    public interface ICdcLsnManager
    {
        byte[] GetStartingLsn(Type type);

        void UpdateLsn(Type type, byte[] lsn);

        byte[] StringToByteArray(string hex);
    }
}
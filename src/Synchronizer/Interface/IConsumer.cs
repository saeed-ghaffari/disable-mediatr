﻿using Cerberus.Synchronizer.Delegates;

namespace Cerberus.Synchronizer.Interface
{
    public interface IConsumer
    {
        /// <summary>
        /// Periodically checks for changes in the database. 
        /// Calling Consume while the consumer is working will result in an exception.
        /// </summary>
        /// <param name="interval">The time interval between invocations of Consume, in milliseconds</param>
        void StartConsuming(int interval);

        void StopConsuming();
    }

    public interface IConsumer<TEntity> : IConsumer
    {
        event ChangeReceivedAsyncEventHandler<TEntity> ChangeReceivedAsync;
        event ConsumerExceptionEventHandler OnException;
    }
}
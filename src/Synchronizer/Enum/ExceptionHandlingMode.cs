﻿namespace Cerberus.Synchronizer.Enum
{
    public enum ExceptionHandlingMode
    {
        Retry = 1,
        Throw = 2,
        Stop = 3
    }
}
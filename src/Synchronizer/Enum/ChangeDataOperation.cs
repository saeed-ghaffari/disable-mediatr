﻿namespace Cerberus.Synchronizer.Enum
{
    public enum ChangeDataOperation
    {
        Delete = 1,
        Insert = 2,
        /// <summary>
        /// Captured column values are those before the update operation
        /// </summary>
        UpdateBefore = 3,
        /// <summary>
        /// captured column values are those after the update operation
        /// </summary>
        UpdateAfter = 4
    }
}
﻿namespace Cerberus.Synchronizer.Enum
{
    public enum MembershipDataType
    {
        PrivatePerson,
        BankingAccount,
        Address
    }
}
﻿namespace Cerberus.Synchronizer.Model
{
    public class CdcStartLsn
    {
        public string Instance { get; set; }

        public byte[] Lsn { get; set; }
    }
}
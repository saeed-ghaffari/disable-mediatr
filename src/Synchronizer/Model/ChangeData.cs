﻿namespace Cerberus.Synchronizer.Model
{
    public class ChangeData<T>
    {
        public ChangeMetadata Metadata { get; set; }

        public T Data { get; set; }

        public T BeforeData { get; set; }

        public override string ToString()
        {
            return $"Meta:{Metadata}\nData:{Data}\nBeforeData:{BeforeData}";
        }
    }
}
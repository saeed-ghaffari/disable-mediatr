﻿using System;

namespace Aspina.Model
{
    public class EnvelopError
    {
        public int ErrorCode { get; set; }

        public string CustomMessage { get; set; }

        public Exception Exception { get; set; }
    }
}

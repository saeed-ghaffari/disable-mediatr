﻿using Aspina.Model;

namespace Cerberus.Api.Model
{
    public class Envelop
    {
        public object Data { get; set; }
        public EnvelopMeta Meta { get; set; }
        public EnvelopError Error { get; set; }

    }
}
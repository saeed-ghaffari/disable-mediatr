﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.Domain.Interface.ApiServices
{
    public interface IPrivatePersonApiService : IObjectCacheService<PrivatePerson, PrivatePersonViewModel>
    {
        Task<PrivatePerson> GetIncludeImageAsync(string uniqueIdentifier);
    }
}
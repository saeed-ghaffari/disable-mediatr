﻿using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.Domain.Interface.ApiServices
{
    public interface IPermanentOtpApiService
    {
        Task UpdateCacheAsync(PermanentOtp permanentOtp);

        Task RemoveAsync(PermanentOtp permanentOtp);

        Task<PermanentOtp> GetAsync(long serviceId, string uniqueIdentifier, OtpTypes type);

        Task<PermanentOtp> GetAsync(PermanentOtp permanentOtp);

  }
}
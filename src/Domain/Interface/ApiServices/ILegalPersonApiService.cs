﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.Domain.Interface.ApiServices
{
    public interface ILegalPersonApiService : IObjectCacheService<LegalPerson, LegalPersonViewModel>
    {
    }
}
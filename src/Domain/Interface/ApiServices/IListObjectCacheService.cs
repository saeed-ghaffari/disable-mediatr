﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.Domain.Interface.ApiServices
{
    public interface IListObjectCacheService<TModel, TViewModel>
        where TModel : BaseModel
        where TViewModel : new()
    {
        /// <summary>
        /// store or update one of object from list of them in cache provider which get from master database 
        /// </summary>
        /// <param name="uniqueIdentifier"></param>
        /// <returns></returns>
        Task StoreAsync(string uniqueIdentifier,ProfileOwnerType? type);

        /// <summary>
        /// remove object from cache provider
        /// </summary>
        /// <param name="uniqueIdentifier"></param>
        /// <returns></returns>
        Task RemoveAsync(string uniqueIdentifier);


        /// <summary>
        /// get specific list of objects from cache provider
        /// </summary>
        /// <param name="uniqueIdentifier"></param>
        /// <returns></returns>
        Task<IEnumerable<TViewModel>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);

        /// <summary>
        /// update specific profile child objects which store in cache provider 
        /// </summary>
        /// <param name="uniqueIdentifier"></param>
        /// <param name="childObject"></param>
        /// <returns></returns>
        Task UpdateProfileChildObjectAsync(string uniqueIdentifier, IEnumerable<TViewModel> childObject,ProfileOwnerType? type);
    }
}
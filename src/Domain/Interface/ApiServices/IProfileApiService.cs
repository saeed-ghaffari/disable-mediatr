﻿using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.Domain.Interface.ApiServices
{
    public interface IProfileApiService
    {
        Task<ProfileViewModel> GetByUniqueIdentifierAsync(string uniqueIdentifier, bool includeAllRelations = false, ProfileOwnerType? type = null);
        Task<ProfileViewModel> GetForProfilingByUniqueIdentifierAsync(string uniqueIdentifier, bool includeAllRelations = false, ProfileOwnerType? type = null);
        Task<ProfileViewModel> GetAuthenticatorProfileByUniqueIdentifierAsync(string uniqueIdentifier);
        Task StoreAsync(string uniqueIdentifier, ProfileOwnerType? type = null);

        Task StoreAsync(ProfileViewModel profile, bool updateWholeObject = true, ProfileOwnerType? type = null);

        /// <summary>
        /// store just profile entity in cache
        /// </summary>
        /// <param name="profile">profile object</param>
        /// <returns></returns>
        Task StoreProfileAsync(Profile profile);

        Task RemoveAsync(string uniqueIdentifier);
    }
}
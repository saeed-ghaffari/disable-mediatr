﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.Domain.Interface.ApiServices
{
    public interface IObjectCacheService<TModel, TViewModel>
        where TModel : BaseModel
        where TViewModel : new()
    {
        /// <summary>
        /// store or update object in cache provider which get from master database 
        /// </summary>
        /// <param name="uniqueIdentifier"></param>
        /// <returns></returns>
        Task StoreAsync(string uniqueIdentifier, ProfileOwnerType? type);

        /// <summary>
        /// store or update object after map to related view model in cache provider, which given in args method
        /// </summary>
        /// <param name="value"></param>
        /// <param name="uniqueIdentifier"></param>
        /// <returns></returns>
        Task StoreAsync(TModel value, string uniqueIdentifier, ProfileOwnerType? type);


        /// <summary>
        /// store or update object  in cache provider, which given in args method
        /// </summary>
        /// <param name="viewModelValue"></param>
        /// <param name="uniqueIdentifier"></param>
        /// <returns></returns>
        Task StoreAsync(TViewModel viewModelValue, string uniqueIdentifier, ProfileOwnerType? type);

        /// <summary>
        /// remove object from cache provider
        /// </summary>
        /// <param name="uniqueIdentifier"></param>
        /// <returns></returns>
        Task RemoveAsync(string uniqueIdentifier);


        /// <summary>
        /// get specific object from cache provider
        /// </summary>
        /// <param name="uniqueIdentifier"></param>
        /// <returns></returns>
        Task<TViewModel> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);

        /// <summary>
        /// update specific profile child object which store in cache provider 
        /// </summary>
        /// <param name="uniqueIdentifier"></param>
        /// <param name="childObject"></param>
        /// <returns></returns>
        Task UpdateProfileChildObjectAsync(string uniqueIdentifier, TViewModel childObject, ProfileOwnerType? type);
    }
}
﻿using System.Data;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface ISqlConnectionProvider
    {
        IDbConnection GetConnection();
        IDbConnection GetReadConnection();
        IDbConnection GetRequestLogConnection();
        IDbConnection GetStatusReportDbConnection();
    }
}
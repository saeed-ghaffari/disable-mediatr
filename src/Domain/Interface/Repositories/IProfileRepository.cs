﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IProfileRepository : IBaseRepository<Profile>
    {
        Task<Profile> GetAsync(long id, bool getAllRelations);

        Task<Profile> GetByUniqueIdentifierAsync(string uniqueIdentifier, bool getAllRelations,
            bool includeAllChildRelations, ProfileOwnerType? type = null);

        Task UpdateStatusAsync(Profile profile);
        Task UpdateStatusAndReasonAsync(Profile profile);
        Task<string> GetFullName(long profileId);
        Task CreateInTransactionAsync(Profile profile);
        Task<IEnumerable<Profile>> GetByMobileAsync(long mobile, ProfileStatus status);
        Task<Profile> GetDataForFactorAsync(long profileId);
        Task<Profile> GetDataForFgmFactorAsync(long profileId);
        Task<Profile> GetRelatedDataWithEntityAsync(long? profileId, string uniqueIdentifier, EntityType entity);
        Task<Profile> GetRelatedDataForDashboardAsync(long profileId, ProfileOwnerType type);

        Task<Profile> GetRelatedDataForScopeAsync(string uniqueIdentifier, ProfileOwnerType type);
       
        Task<IEnumerable<Profile>> GetProfileListAsync(long fromRow, long toRow, string uniqueIdentifier,
            ProfileOwnerType? type = null, ProfileStatus status = ProfileStatus.TraceCode);

        Task<int> CountAsync(ProfileOwnerType? type = null, ProfileStatus? status = null);

        Task<Profile> GetRelatedDataForAgentAsync(long profileId);

        Task ChangeProfileDataAsync(long profileId,ProfileField field, string value);

        Task<List<Profile>> GetForDeadAsync(string uniqueIdentifier, DateTime birthDate, ProfileStatus status = ProfileStatus.PolicyAccepted);
        Task<Profile> GetForAuthenticatorOtpByUniqueIdentifierAsync(string uniqueIdentifier);

    
        Task<Profile> GetSignatureFileByUniqueIdentifierAsync(string uniqueIdentifier);
         Task<Profile> GetWithNoLockAsync(string uniqueIdentifier);

         Task UpdateIsConfirmedEntryNodeAsync(long profileId,ProfileStatus status, long paymentId);
    }
}
﻿using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IPermanentOtpRepository : IBaseRepository<PermanentOtp>
    {
        Task<PermanentOtp> GetAsync(long serviceId, string uniqueIdentifier, OtpTypes type);

        Task AddAsync(PermanentOtp permanentOtp);
    }
}
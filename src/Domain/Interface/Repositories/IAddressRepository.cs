﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IAddressRepository : IBaseRepository<Address>
    {
        Task<Address> GetByProfileIdAsync(long profileId,bool getRelatedData = false);

        Task<List<Address>> GetListByProfileIdAsync(long profileId);
        Task<List<Address>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface ITradingCodeRepository : IBaseRepository<TradingCode>
    {
        Task<List<TradingCode>> GetListAsync(long profileId);
        Task<IEnumerable<TradingCode>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}
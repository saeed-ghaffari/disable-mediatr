﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IFinancialInfoRepository : IBaseRepository<FinancialInfo>
    {
        Task<FinancialInfo> GetByProfileIdAsync(long profileId, bool includeBrokers);
        Task UpdateAsync(FinancialInfo financialInfoModel, IEnumerable<Broker> brokers);
        Task CreateAsync(FinancialInfo financialInfoModel, IEnumerable<Broker> brokers);
        Task<FinancialInfo> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}
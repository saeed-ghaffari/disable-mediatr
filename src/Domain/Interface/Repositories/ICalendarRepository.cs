﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface ICalendarRepository : IBaseRepository<Calendar>
    {
        Task<bool> TodayIsHoliday();
    }
}
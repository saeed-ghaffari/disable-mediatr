﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model.Membership;

namespace Cerberus.Domain.Interface.Repositories.Membership
{
    public interface IEtfMembershipRepository: IBaseRepository<EtfMembership>
    {
        Task<bool> CheckExistAsync(string uniqueIdentifier);
        Task BulkCreateAsync(List<EtfMembership> memberships);
    }
}

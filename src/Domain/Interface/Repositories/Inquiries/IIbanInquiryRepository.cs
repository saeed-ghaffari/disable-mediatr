﻿using Cerberus.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.Repositories
{
  public  interface IIbanInquiryRepository:IBaseRepository<IbanInquiry>
    {
        //IEnumerable<IbanInquiry> GetExistsIbanInquiry(Expression<Func<IbanInquiry, bool>> predicate);
    }
}

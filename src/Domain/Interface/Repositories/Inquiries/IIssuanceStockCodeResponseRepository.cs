﻿using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface ILegacyCodeResponseRepository : IBaseRepository<LegacyCodeResponse>
    {
        
    }
}
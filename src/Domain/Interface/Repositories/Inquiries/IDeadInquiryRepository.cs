﻿using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.Repositories.Inquiries
{
    public interface IDeadInquiryRepository:IBaseRepository<DeadInquiry>
    {
    }
}

﻿using Cerberus.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface ILegacyCodeInquiryRepository : IBaseRepository<LegacyCodeInquiry>
    {

    }
}

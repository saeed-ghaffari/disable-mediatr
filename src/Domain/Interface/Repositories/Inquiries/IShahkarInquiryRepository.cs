﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.Repositories.Inquiries
{
   public interface IShahkarInquiryRepository : IBaseRepository<ShahkarInquiry>
   {
       Task <List<ShahkarInquiry>> GetBySsnAsync(string uniqueIdentifier);
   }
}

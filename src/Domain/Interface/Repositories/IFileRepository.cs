﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IFileRepository : IBaseRepository<File>
    {
        Task DeleteAsync(long id);
    }
}
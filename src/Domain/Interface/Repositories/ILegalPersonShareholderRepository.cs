﻿using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface ILegalPersonShareholderRepository : IBaseRepository<LegalPersonShareholder>
    {
        Task<List<LegalPersonShareholder>> GetListAsync(long profileId);

     

    }
}
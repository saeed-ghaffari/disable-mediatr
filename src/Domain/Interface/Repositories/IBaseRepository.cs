﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IBaseRepository<T> where T : BaseModel
    {
        Task CreateAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        Task<T> GetAsync(long id);
        Task CreateRangeAsync(IEnumerable<T> entity);
        Task UpdateRangeAsync(IEnumerable<T> entity);
        Task<IEnumerable<T>> GetListAsync(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> GetConditionalListAsync(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> GetGenericAsync(Expression<Func<T, bool>> predicate,
            Func<IQueryable<T>, IQueryable<T>> include, Func<IQueryable<T>, IOrderedQueryable<T>> order);
    }
}
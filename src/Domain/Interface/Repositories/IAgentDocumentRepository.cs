﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IAgentDocumentRepository : IBaseRepository<AgentDocument>
    {
        Task<List<AgentDocument>> GetListDocumentByAgentId(long id);
        Task DeleteAsync(long id);
    }
}
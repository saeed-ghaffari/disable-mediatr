﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IPaymentRepository : IBaseRepository<Payment>
    {
        Task SetFactorIdRepository(long paymentId, long factorId, string serialNumber);
        Task<int> GetCountByDiscountIssuerAsync(long serviceId, DateTime? from, DateTime? to);
        Task<Payment> GetPaymentAsync(long id);
        Task UpdateIsConfirmedReferenceAsync(long paymentId);
        Task<Payment> GetByIdAsync(long id);
        Task<List<Payment>> GetSuspiciousPayments();
        Task UpdateSettledPaymentsAsync(long Id, string SaleReferenceId, string ReferenceNumber);
        Task UpdateSettleFailedPaymentsAsync(long Id);

    }
}
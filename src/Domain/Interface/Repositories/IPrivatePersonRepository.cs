﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IPrivatePersonRepository : IBaseRepository<PrivatePerson>
    {
        Task<PrivatePerson> GetByProfileIdAsync(long profileId);
        Task<PrivatePerson> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
        Task<IEnumerable<PrivatePerson>> GetByDaysLeftToEighteenAsync(int days,int status);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface ICountryRepository : IBaseRepository<Country>
    {
      Task<List<Country>> GetListAsync();
    }
}
﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories.Log
{
    public interface IProfilingRequestLogRepository
    {
        Task AddAsync(ProfilingRequestLog log);
    }
}
﻿using Cerberus.Domain.Model.Log;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cerberus.Domain.Interface.Repositories.Log
{
  public  interface ISmsLogRepository
    {
        Task AddAsync(SmsLog log);

    }
}

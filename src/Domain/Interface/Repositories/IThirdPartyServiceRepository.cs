﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IThirdPartyServiceRepository : IBaseRepository<ThirdPartyService>
    {
        Task<ThirdPartyService> GetByLinkAsync(string link);

        Task<string> GetThirdPartyName(long? id);
        Task<ThirdPartyService> GetAsync(string title);
    }
}
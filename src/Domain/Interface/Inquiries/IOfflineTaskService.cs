﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.Interface.Inquiries
{
    public interface IOfflineTaskService
    {
        Task CreateTaskAsync(KeyValuePair<long, string> taskGroup, long profileId, object taskParams);
        Task ScheduleOfflineTasksAsync(long profileId,long? mobile, DateTime dueTime);
        Task ScheduleEditAfterSejamiTasksAsync(long profileId);
        Task ScheduleSetFactorAsync(long paymentId,DateTime dueTime,bool setFactorForOtherClients=false);
        Task ScheduleSetFakeFactorAsync(long paymentId, DateTime dueTime);
        Task ScheduleOfflineTasksShahkarAsync(long profileId, long mobile, long newMobile, string uniqueIdentifier,
            DateTime dueTime, ProfileStatus profileStatus);

    }
} 
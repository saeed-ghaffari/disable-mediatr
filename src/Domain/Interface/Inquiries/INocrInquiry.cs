﻿using System.Threading.Tasks;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.Inquiries
{
    public interface INocrInquiryService
    {
        Task<NocrInquiry> GetAsync(string ssn,string birthDate);
        Task<NocrInquiry> GetExistInquiryAsync(string ssn, string birthDate);
        Task UpdateAsync(NocrInquiry nocrInquiry);
    }
}

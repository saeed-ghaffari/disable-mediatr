﻿using System.Threading.Tasks;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.Inquiries
{
    public interface ILegacyCodeInquiryService
    {
        Task<LegacyCodeInquiry> GetAsync(string ninOrSsn);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories.Inquiries;
using Cerberus.Domain.ViewModel.Inquiries;

namespace Cerberus.Domain.Interface.Inquiries
{
    public interface IDeadInquiryService
    {
        Task<DeadListInquiryVm> SendAsync(string fromDate, string toDate);
        Task<DeadListResponseVm> GetAsync(Guid id);
    }
}

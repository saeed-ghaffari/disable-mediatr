﻿using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.Inquiries
{
    public interface IIrenexInquiryService
    {
        Task<LegacyCodeInquiry> GetAsync(string ninOrSsn);
    }
}

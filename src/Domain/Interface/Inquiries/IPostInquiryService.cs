﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.Inquiries
{
    public interface IPostInquiryService
    {
        Task<PostInquiry> GetAsync(string ssn, string birthDate);
    }
}
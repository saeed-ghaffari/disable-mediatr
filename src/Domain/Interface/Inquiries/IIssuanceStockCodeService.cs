﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Cerberus.Domain.ViewModel.Inquiries;

namespace Cerberus.Domain.Interface.Inquiries
{
    public interface IIssuanceStockCodeService
    {
        Task<(IEnumerable<IssuanceStockCodeModel> codeResponseModel, HttpStatusCode StatusCode)> GetStockCodeAsync(IEnumerable<IssuanceStokeCodeModel> model);
    }
}
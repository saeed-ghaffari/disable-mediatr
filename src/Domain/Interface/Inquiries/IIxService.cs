﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Utility.Model;

namespace Cerberus.Domain.Interface.Inquiries
{
    public interface IIxService
    {
        Task<HttpResponseResult<IxReturnType>> GetAsync(string uniqueIdentifier, TimeSpan ixAccessTokenTtl);
        Task<LegacyCodeInquiry> GetLegacyCodeAsync(string uniqueIdentifier);
        Task<string> GetTokenAsync(TimeSpan accessTokenTtl, bool getNewToken = false);

    }
}
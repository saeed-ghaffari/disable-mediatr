﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface ILegalPersonStakeholderService
    {
        Task<List<LegalPersonStakeholder>> GetListAsync(long profileId, StakeHolderType type);

        Task<LegalPersonStakeholder> GetAsync(long id);

        Task CreateAsync(LegalPersonStakeholder model);

        Task DeleteAsync(LegalPersonStakeholder model);
        Task<List<LegalPersonStakeholder>> GetListAsync(long profileId);


    }
}
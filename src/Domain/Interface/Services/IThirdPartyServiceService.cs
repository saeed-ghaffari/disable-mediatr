﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IThirdPartyServiceService
    {
        Task<ThirdPartyService> GetAsync(long id);
        Task<Model.ThirdPartyService> GetAsync(string title);
        Task<ThirdPartyService> GetByLinkAsync(string link);
        Task<string> GetThirdPartyName(long? id);
    }
}

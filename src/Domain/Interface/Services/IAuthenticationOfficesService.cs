﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.Domain.Interface.Services
{
    public interface IAuthenticationOfficesService
    {
        Task<(List<AuthenticationOffices> authenticationOfficeses, long recordCount)> GetListAsync(AuthenticationOffices offices, int skip, int take);
        Task<List<AuthenticateOfficeViewModel>> GetAllAsync();

    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface ITradingCodeService
    {
        Task CreateAsync(TradingCode tradingCode);
        Task UpdateAsync(TradingCode tradingCode);
        Task DeleteAsync(TradingCode tradingCode);

        Task<TradingCode> GetAsync(long id);

        Task<List<TradingCode>> GetListAsync(long profileId);
      
        Task<IEnumerable<TradingCode>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}
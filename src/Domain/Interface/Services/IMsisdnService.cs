﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
  public  interface IMsisdnService
    {
        Task CreateAsync(Msisdn msisdn);

        Task UpdateAsync(Msisdn msisdn);

        Task<Msisdn> GetAsync(long id);

        Task<Msisdn> GetByProfileIdAsync(long profileId);

       
    }
}

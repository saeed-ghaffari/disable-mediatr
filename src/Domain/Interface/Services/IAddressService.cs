﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IAddressService
    {
        Task CreateAsync(Address address);

        Task UpdateAsync(Address address);

        Task<Address> GetByProfileIdAsync(long profileId, bool getRelatedData = false);

        Task<List<Address>> GetListByProfileIdAsync(long profileId);

        Task<IEnumerable<Address>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}
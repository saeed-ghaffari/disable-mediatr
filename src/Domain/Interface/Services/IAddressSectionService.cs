﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IAddressSectionService
    {
        Task<IEnumerable<AddressSection>> GetListAsync(long cityId);
       
    }
}
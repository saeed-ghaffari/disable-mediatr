﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Membership;

namespace Cerberus.Domain.Interface.Services.Membership
{
    public interface IMembershipService
    {
        Task CreateAsync(PrivatePersonMembership model);
       Task<PrivatePersonMembership> GetPrivatePersonAsync(string uniqueIdentifier, string mobile, long serviceId);

        Task<AddressMembership> GetAddressAsync(string uniqueIdentifier, long serviceId);

        Task<BankingAccountMembership> GetAccountAsync(string uniqueIdentifier, long serviceId);

        Task AddAsync(long serviceId, string uniqueIdentifier, PrivatePersonMembership privatePerson,
            AddressMembership address, List<BankingAccountMembership> accounts);

        Task<PrivatePersonMembership> GetPrivatePersonAsync(string uniqueIdentifier, long serviceId);

        Task UpdateCacheAsync<T>(long serviceId, string uniqueIdentifier, T data,MembershipDataType type);

        Task RemoveAsync(long serviceId, string uniqueIdentifier, MembershipDataType type );

        Task<IEnumerable<PrivatePersonMembership>> GetPrivatePeronListAsync(long fromRow, long toRow, string uniqueIdentifier);
        Task<IEnumerable<AddressMembership>> GetAddressListAsync(long fromRow, long toRow, string uniqueIdentifier);
        Task<IEnumerable<BankingAccountMembership>> GetBankingAccountListAsync(long fromRow, long toRow, string uniqueIdentifier);

        Task<int> CountAsync(MembershipDataType type);
        Task UpdateAsync(PrivatePersonMembership model);

    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IJobInfoService
    {
        Task CreateAsync(JobInfo jobInfo);

        Task UpdateAsync(JobInfo jobInfo);

        Task<JobInfo> GetAsync(long id);

        Task<JobInfo> GetByProfileIdAsync(long profileId, bool getAllRelated = false);

        Task<JobInfo> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}
﻿using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IPermanentOtpService
    {
        Task<PermanentOtp> GetAsync(long serviceId, string uniqueIdentifier,OtpTypes type);
        Task CreateAsync(PermanentOtp permanentOtp);
    }
}
﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IMessageService
    {
        Task<Message> CreateAsync(Message message, bool sendSms = true, Profile profile = null,DateTime? dueTime=null);
        
        Task<Message> GetListAsync(long profileId);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Model.Membership;

namespace Cerberus.Domain.Interface.Services
{
    public interface IUserBrokerInCerberusService
    {
        Task<PrivatePersonMembership> CheckUserExsist(string nationalCode, string mobile);
    }
}

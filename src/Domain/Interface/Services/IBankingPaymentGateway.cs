﻿using System.Threading.Tasks;

namespace Cerberus.Domain.Interface.Services
{
    public interface IBankingPaymentGateway
    {
        Task<string> InitialPaymentAsync(long localInvoiceId, long amount);
        Task<bool> VerifyPaymentAsync(long saleReferenceId, long? saleOrderId, string refId);
        Task<bool> SettlePaymentAsync(long saleReferenceId, long? saleOrderId, string refId);
        

    }
}
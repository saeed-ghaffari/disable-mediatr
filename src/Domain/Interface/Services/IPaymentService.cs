﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Cerberus.Domain.Model;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.ViewModel;


namespace Cerberus.Domain.Interface.Services
{
    public interface IPaymentService
    {
        Task<Payment> CreatePendingAsync(long profileId, PaymentGateway gateway, long amount, long organizationId, long discount, long? profilePackageId);
        Task UpdateAsync(Payment updatePayment);
        Task<Payment> GetAsync(long id);
        Task<int> GetCountByDiscountIssuerAsync(long serviceId, DateTime? from, DateTime? to);
        Task<IEnumerable<Payment>> GetListAsync(Expression<Func<Payment, bool>> predicate);
        Task DeleteAsync(Payment entity);
        Task UpdateIsConfirmedReferenceAsync(long paymentId);
        Task CreateAsync(Payment payment);
        Task<List<Payment>> GetSuspiciousPayments();
        Task UpdateSettledPaymentsAsync(long Id, string SaleReferenceId, string ReferenceNumber);
        Task UpdateSettleFailedPaymentsAsync(long Id);
        Task SetFactorIdRepository(long paymentId, long factorId, string serialNumber);
        Task<Payment> GetByIdAsync(long id);
    }
}

﻿using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cerberus.Domain.Interface.Services
{
    public interface IBankService
    {
        Task<List<Bank>> GetListAsync();
        Task<List<Bank>> GetListForAuthenticationOfficeAsync();
    }
}
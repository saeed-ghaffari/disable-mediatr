﻿using System.Threading.Tasks;
using Microsoft.Extensions.ObjectPool;

namespace Cerberus.Domain.Interface.Services
{
    public interface IShahkarService
    {
        Task<bool> IsValidMobile(string mobile, string nationalCode);
        Task<bool> IsMobileForNationalCode(string mobile, string nationalCode);
    }
}
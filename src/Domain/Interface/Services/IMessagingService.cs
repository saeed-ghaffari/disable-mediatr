﻿using System;
using Cerberus.Domain.Enum;
using Cerberus.Domain.ViewModel;
using System.Threading.Tasks;
using Cerberus.Utility.Enum;

namespace Cerberus.Domain.Interface.Services
{
    public interface IMessagingService
    {
        Task SendSmsAsync(long? profileId, string msisdn, Carrier? carrier, string message, DateTime? dueTime=null);
        Task SendOtpSmsAsync(long? profileId, string msisdn, Carrier? carrier, string message, DateTime? dueTime=null);
    }
}
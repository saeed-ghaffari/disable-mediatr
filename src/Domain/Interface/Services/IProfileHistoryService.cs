﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IProfileHistoryService
    {
        Task<ProfileHistory> GetAsync(long id);

        Task<ProfileHistory> GetByUniqueIdentifierAsync(string uniqueIdentifier);

        Task CreateAsync(ProfileHistory history, ProfileStatus changedTo);
        Task CreateAsync(ProfileHistory history);


        Task UpdateAsync(ProfileHistory history);

        Task<IEnumerable<ProfileHistory>> GetByReferenceIdAsync(string referenceId, DateTime from, DateTime to);

        Task<ProfileHistory> GetTheLastHistoryAsync(long profileId);
    }
}
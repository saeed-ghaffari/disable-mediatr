﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IBrokerService
    {
        Task<IEnumerable<Broker>> GetListAsync();

        Task<Broker> GetAsync(long id);
    }
}
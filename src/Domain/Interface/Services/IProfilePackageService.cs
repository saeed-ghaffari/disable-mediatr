﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IProfilePackageService
    {
        Task CreateAsync(ProfilePackage profilePackage);

        Task UpdateAsync(ProfilePackage profilePackage);

        Task<ProfilePackage> GetAsync(long id);

        Task<ProfilePackage> GetUnusedPackagesAsync(long profileId, PackageType type,PackageUsedFlag usePackage);

        // Task<bool> CheckExistProfilePackage(long profileId, PackageType type);
        Task<ProfilePackage> GetSuspiciousPackageAsync(long profileId, PackageType type);

    }
}

﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface ILegalPersonService
    {
        Task CreateAsync(LegalPerson legalPerson);
        Task<LegalPerson> GetAsync(long id);

        Task UpdateAsync(LegalPerson legalPerson);

        Task<LegalPerson> GetByProfileIdAsync(long profileId);

        Task<LegalPerson> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}
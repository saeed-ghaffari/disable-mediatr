﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface ILegalPersonReadRepository : IBaseReadRepository<LegalPerson>
    {
        Task<LegalPerson> GetByProfileIdAsync(long profileId);
        Task<LegalPerson> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.StatusData;
using Cerberus.Domain.ViewModel.StatusData;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IProfileReadRepository : IBaseReadRepository<Profile>
    {
        Task<Profile> GetAsync(long id, bool getAllRelations);

        Task<Profile> GetByUniqueIdentifierAsync(string uniqueIdentifier, bool getAllRelations,
            bool includeAllChildRelations, ProfileOwnerType? type = null);

        Task<string> GetFullName(long profileId);
        Task<IEnumerable<Profile>> GetByMobileAsync(long mobile, ProfileStatus status);
        Task<Profile> GetDataForFactorAsync(long profileId);
        Task<Profile> GetDataForFgmFactorAsync(long profileId);
        Task<Profile> GetRelatedDataWithEntityAsync(long? profileId, string uniqueIdentifier, EntityType entity);
        Task<Profile> GetRelatedDataForDashboardAsync(long profileId, ProfileOwnerType type);
       
        Task<Profile> GetRelatedDataForScopeAsync(long profileId, ProfileOwnerType type);


        Task<IEnumerable<Profile>> GetProfileListAsync(long fromRow, long toRow, string uniqueIdentifier,
            ProfileOwnerType? type = null, ProfileStatus status = ProfileStatus.SuccessPayment);

        Task<int> CountAsync(ProfileOwnerType? type = null, ProfileStatus? status = null);

        Task<Profile> GetRelatedDataForAgentAsync(long profileId);

        Task<List<Profile>> GetForDeadAsync(string uniqueIdentifier, DateTime birthDate, ProfileStatus status = ProfileStatus.PolicyAccepted);
       

        Task<Profile> GetSignatureFileByUniqueIdentifierAsync(string uniqueIdentifier);
        Task<IEnumerable<BatchSejamStatusResultViewModel>> GetSejamStatus(string referenceId, List<StatusData> validUniqueIdentifiers);
        Task<IEnumerable<BatchPrivatePersonResultViewModel>> GetBatchPrivatePerson(string referenceId, List<StatusData> validUniqueIdentifiers);

        Task<Profile> GetAuthenticatorProfileByUniqueIdentifierAsync(string uniqueIdentifier);
    }
}
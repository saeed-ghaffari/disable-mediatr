﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IJobReadRepository : IBaseReadRepository<Job>
    {
      Task<List<Job>> GetListAsync();
    }
}
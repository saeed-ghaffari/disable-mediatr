﻿using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface ILegalPersonShareholderReadRepository : IBaseReadRepository<LegalPersonShareholder>
    {
        Task<List<LegalPersonShareholder>> GetListAsync(long profileId);

     

    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface ICityReadRepository : IBaseReadRepository<City>
    {
        Task<List<City>> GetListAsync(long provinceId);
        Task<List<City>> GetListAsync();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IPaymentReadRepository : IBaseReadRepository<Payment>
    {
        Task<int> GetCountByDiscountIssuerAsync(long serviceId, DateTime? from, DateTime? to);
        Task<Payment> GetByIdAsync(long id);
        Task<List<Payment>> GetSuspiciousPayments();
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IAuthenticationOfficesReadRepository : IBaseReadRepository<AuthenticationOffices>
    {
       Task<(List<AuthenticationOffices> authenticationOfficeses, long recordCount)> GetListAsync(AuthenticationOffices offices, int skip, int take);
       Task<List<AuthenticationOffices>>GetAllAsync();
    }
}

﻿using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.ReadRepositories.ReadInquiries
{
    public interface IIbanInquiryReadRepository : IBaseReadRepository<IbanInquiry>
    {
        //IEnumerable<IbanInquiry> GetExistsIbanInquiry(Expression<Func<IbanInquiry, bool>> predicate);
    }
}

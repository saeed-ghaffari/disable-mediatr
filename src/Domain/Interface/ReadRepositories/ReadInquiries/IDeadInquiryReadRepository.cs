﻿using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.ReadRepositories.ReadInquiries
{
    public interface IDeadInquiryReadRepository : IBaseReadRepository<DeadInquiry>
    {
    }
}

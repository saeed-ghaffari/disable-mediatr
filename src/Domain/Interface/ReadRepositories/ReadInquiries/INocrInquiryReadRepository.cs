﻿using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.ReadRepositories.ReadInquiries
{
    public interface INocrInquiryReadRepository : IBaseReadRepository<NocrInquiry>
    {
    }
}

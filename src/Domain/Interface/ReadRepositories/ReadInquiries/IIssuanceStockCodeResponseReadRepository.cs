﻿using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories.ReadInquiries
{
    public interface IIssuanceStockCodeResponseReadRepository : IBaseReadRepository<IssuanceStockCodeResponse>
    {
    }
}
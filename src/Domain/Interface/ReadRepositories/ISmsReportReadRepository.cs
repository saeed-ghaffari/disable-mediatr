﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model.Report;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface ISmsReportReadRepository
    {
        Task<ReportModel> GetReportAsync(DateTime fromDate , DateTime toDate);
        Task<IEnumerable<GroupMember>> GetGroupMemberAsync(int? groupId = null);
    }
}

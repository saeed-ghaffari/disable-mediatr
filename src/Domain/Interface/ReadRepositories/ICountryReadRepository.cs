﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface ICountryReadRepository : IBaseReadRepository<Country>
    {
      Task<List<Country>> GetListAsync();
    }
}
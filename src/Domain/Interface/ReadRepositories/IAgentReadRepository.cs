﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IAgentReadRepository : IBaseReadRepository<Agent>
    {
        Task<Agent> GetByProfileIdAsync(long profileId);
        Task<Agent> GetByAgentProfileIdAsync(long agentProfileId);
        Task<List<Agent>> GetListByAgentProfileIdAsync(long agentProfileId);
        Task<Agent> GetByProfileIdWithNoLockAsync(long profileId);
    }
}
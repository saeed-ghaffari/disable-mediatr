﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model.Membership;

namespace Cerberus.Domain.Interface.ReadRepositories.Membership
{
    public interface IMembershipReadRepository : IBaseReadRepository<PrivatePersonMembership>
    {
        Task<PrivatePersonMembership> GetPrivatePersonAsync(string uniqueIdentifier, long serviceId);

        Task<AddressMembership> GetAddressAsync(string uniqueIdentifier, long serviceId);

        Task<BankingAccountMembership> GetAccountAsync(string uniqueIdentifier, long serviceId);

        Task AddAsync(PrivatePersonMembership privatePerson, AddressMembership address,
            List<BankingAccountMembership> accounts);

        Task<IEnumerable<PrivatePersonMembership>> GetPrivatePeronListAsync(long fromRow, long toRow, string uniqueIdentifier);
        Task<IEnumerable<AddressMembership>> GetAddressListListAsync(long fromRow, long toRow, string uniqueIdentifier);
        Task<IEnumerable<BankingAccountMembership>> GetBankingAccountListAsync(long fromRow, long toRow, string uniqueIdentifier);

        Task<int> CountAsync(MembershipDataType type);
    }
}

﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IJobInfoReadRepository : IBaseReadRepository<JobInfo>
    {
        Task<JobInfo> GetByProfileIdAsync(long profileId, bool getAllRelated);
        Task<JobInfo> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}
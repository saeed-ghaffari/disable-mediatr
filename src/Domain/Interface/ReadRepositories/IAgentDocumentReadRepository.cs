﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IAgentDocumentReadRepository : IBaseReadRepository<AgentDocument>
    {
        Task<List<AgentDocument>> GetListDocumentByAgentId(long id);
    }
}
﻿using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IPermanentOtpReadRepository : IBaseReadRepository<PermanentOtp>
    {
        Task<PermanentOtp> GetAsync(long serviceId, string uniqueIdentifier, OtpTypes type);
        Task<bool> IsExistAsync(long serviceId, string uniqueIdentifier);
    }
}
﻿using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IAddressSectionReadRepository : IBaseReadRepository<AddressSection>
    {
    }
}
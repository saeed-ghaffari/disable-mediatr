﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IFinancialInfoReadRepository : IBaseReadRepository<FinancialInfo>
    {
        Task<FinancialInfo> GetByProfileIdAsync(long profileId, bool includeBrokers);
        Task<FinancialInfo> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}
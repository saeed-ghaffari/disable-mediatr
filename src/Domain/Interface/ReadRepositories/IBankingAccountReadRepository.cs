﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IBankingAccountReadRepository : IBaseReadRepository<BankingAccount>
    {
        Task<IEnumerable<BankingAccount>> GetListAsync(long profileId, bool includeRelatedData = false);
     
        Task<IEnumerable<BankingAccount>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);

    }
}
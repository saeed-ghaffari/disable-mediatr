﻿using System;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface ILegalPersonStakeholderReadRepository : IBaseReadRepository<LegalPersonStakeholder>
    {
        Task<List<LegalPersonStakeholder>> GetListAsync(long profileId, StakeHolderType type);

        Task<List<LegalPersonStakeholder>> GetListAsync(long profileId);
    }
}
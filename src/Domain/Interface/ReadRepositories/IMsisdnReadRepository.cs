﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IMsisdnReadRepository : IBaseReadRepository<Msisdn>
    {
        Task<Msisdn> GetByProfileIdAsync(long profileId);
    }
}

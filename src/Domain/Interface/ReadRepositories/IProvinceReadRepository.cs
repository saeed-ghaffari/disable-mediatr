﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IProvinceReadRepository : IBaseReadRepository<Province>
    {
        Task<IEnumerable<Province>> GetListAsync(long countryId);
        Task<IEnumerable<Province>> GetListAsync();
    }
}
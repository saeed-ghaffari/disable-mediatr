﻿using Cerberus.Domain.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Cerberus.Domain.ViewModel.StatusData
{
    public class BatchPrivatePersonResultViewModel
    {
        public string NationalCode { get; set; }
        public string UniqueIdentifier { get; set; }
        public int Status { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FatherName { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Gender Gender { get; set; }

        public string SeriShChar { get; set; }

        public string SeriSh { get; set; }

        public string Serial { get; set; }

        public string ShNumber { get; set; }

        public DateTime BirthDate { get; set; }

        public string PlaceOfIssue { get; set; }

        public string PlaceOfBirth { get; set; }

        [JsonIgnore]
        public bool? IsConfirmed { get; set; }
    }
}

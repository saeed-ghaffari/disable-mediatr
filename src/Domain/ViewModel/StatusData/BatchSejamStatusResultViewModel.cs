﻿using Cerberus.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.ViewModel.StatusData
{
    public class BatchSejamStatusResultViewModel
    {
        public string NationalCode { get; set; }
        public string UniqueIdentifier { get; set; }
        public int Status { get; set; }
    }
}

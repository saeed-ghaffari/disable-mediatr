﻿namespace Cerberus.Domain.ViewModel
{
    public class CheckPaymentViewModel
    {
        public bool Successful { get; set; }
        public bool SettleExist { get; set; }

    }
}
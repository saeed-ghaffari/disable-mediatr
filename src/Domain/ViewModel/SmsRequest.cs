﻿using Newtonsoft.Json;

namespace Cerberus.Domain.ViewModel
{
    public class SmsRequest
    {
        public string Body { get; set; }
        public string Receptor { get; set; }
    }

    /// <summary>
    /// to use the origin server for send sms
    /// </summary>
    public class OriginSmsRequest
    {
        /// <summary>
        /// list of messages
        /// </summary>
        [JsonProperty(PropertyName = "messages")]
        public string[] Messages { get; set; }

        /// <summary>
        /// list of mobile numbers
        /// </summary>
        [JsonProperty(PropertyName = "mobiles")]
        public string[] Mobiles { get; set; }

        /// <summary>
        /// list of message ids (the values are not important just fill it ;))
        /// </summary>
        [JsonProperty(PropertyName = "msgIds")]
        public long[] MsgIds { get; set; }    
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Cerberus.Domain.ViewModel.Membership
{
    public class MemberAddressViewModel
    {
        [MaxLength(10, ErrorMessage = "Maximum of 10 digits is allowed in {0}")]
        public string PostalCode { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Only the number is allowed in {0}")]
        public string CountryId { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Only the number is allowed in {0}")]
        public string ProvinceId { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Only the number is allowed in {0}")]
        public string CityId { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Only the number is allowed in {0}")]
        public string SectionId { get; set; }

        [Required(ErrorMessage = "{0} can not be null")]
        [RegularExpression(@"^\s*[-،.چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ0123456789,\s]+\s*$", ErrorMessage = "Only the number and Persian Character is allowed in {0}")]
        public string Address { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Only the number is allowed in {0}")]
        [MaxLength(8, ErrorMessage = "Maximum of 8 digits is allowed in {0}")]
        public string Tel { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Domain.ViewModel.Membership
{
    public class Member
    {
        [Required(ErrorMessage = "{0} can not be null")]
        [MinLength(10, ErrorMessage = "invalid {0}")]
        public string UniqueIdentifier { get; set; }

        [Required(ErrorMessage = "{0} can not be null")]
        public MemberPrivatePerson PrivatePerson { get; set; }

        public IEnumerable<MemberBankingAccountViewModel> BankingAccounts { get; set; }

        public MemberAddressViewModel Address { get; set; }
    }
}
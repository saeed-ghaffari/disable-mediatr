﻿using System;
using System.ComponentModel.DataAnnotations;
using Cerberus.Web.WebLogic.CustomAttribute;

namespace Cerberus.Domain.ViewModel.Membership
{
    public class EtfMemberShipViewModel
    {

        [MaxLength(10)]
        [Required(ErrorMessage = "{0} is Required")]
        [Utility.UniqueIdentifierValidation(ErrorMessage = "Invalid {0}")]
        public string UniqueIdentifier { get; set; }

        [MaxLength(12)]
        [Required(ErrorMessage = "{0} is Required")]
        [RegularExpression(@"9(0[1-5]|1[0-9]|3[0-9]|2[0-9]|9[0-9])-?[0-9]{3}-?[0-9]{4}", ErrorMessage = "Invalid {0}")]
        public string Mobile { get; set; }

        [MaxLength(100)]
        [Required(ErrorMessage = "{0} is Required")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "Only the Persian Character is allowed in {0}")]
        public string FirstName { get; set; }

        [MaxLength(150)]
        [Required(ErrorMessage = "{0} is Required")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "Only the Persian Character is allowed in {0}")]
        public string LastName { get; set; }

        [MaxLength(100)]
        [Required(ErrorMessage = "{0} is Required")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "Only the Persian Character is allowed in {0}")]
        public string FatherName { get; set; }

        [Required(ErrorMessage = "{0} is Required")]
        public DateTime? BirthDate { get; set; }

        [MaxLength(26)]
       // [Sheba(ErrorMessage = "Invalid {0}")]
        public string Sheba { get; set; }

        [MaxLength(10)]
        [MinLength(10)]
        [Required(ErrorMessage = "{0} is Required")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only  Number is allowed in {0}")]
        public string PostalCode { get; set; }

       
        [Required(ErrorMessage = "{0} is Required")]
        public long Amount { get; set; }

        [Required(ErrorMessage = "{0} is Required")]
        public DateTime PaymentDate { get; set; }
    }
}

﻿namespace Cerberus.Domain.ViewModel
{
    public class Error
    {
        public int ErrorCode { get; set; }
        public string CustomMessage { get; set; }
        public string Exception { get; set; }


    }
}
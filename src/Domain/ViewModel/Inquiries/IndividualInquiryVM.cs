﻿using Cerberus.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.ViewModel
{
    public class IndividualInquiryVM : BaseInquiryParams
    {
        public string PrivateCode { get; set; }
    }
}

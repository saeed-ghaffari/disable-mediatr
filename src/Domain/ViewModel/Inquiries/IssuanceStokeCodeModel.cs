﻿namespace Cerberus.Domain.ViewModel.Inquiries
{
    public class IssuanceStokeCodeModel
    {
        public string Nin { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FatherName { get; set; }
        public string SeriesNumber { get; set; }
        public string SerialNumber { get; set; }
        public string IdentificationNumber { get; set; }
        public bool IsMale { get; set; }
        public string PersianBirthDate { get; set; }
        public string IssuePlace { get; set; }
        public bool IsAlive { get; set; }
        public long IssuanceStockCodeId { get; set; }
        public string Mobile { get; set; }
        public long ProfileId { get; set; }
        public int TryCount { get; set; }
    }
}
﻿using Cerberus.Domain.Model;

namespace Cerberus.Domain.ViewModel.Inquiries
{
    public class IbanInquiryVM: BaseInquiryParams
    {
        public string Iban { get; set; }
    }
}

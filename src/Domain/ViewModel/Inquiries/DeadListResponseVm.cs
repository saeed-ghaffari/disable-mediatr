﻿using System;
using System.Collections.Generic;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.ViewModel.Inquiries
{
    public class DeadListResponseVm
    {
        
        public string Data { get; set; }
        public Guid? FileName { get; set; }
        public List<string> Message { get; set; }
        public bool DataNotReady { get; set; }
        public bool IsOk { get; set; }
        public bool HasException { get; set; }
        public bool Successful { get; set; }
        public List<DeadInquiry> DeadInquiries { get; set; }

       
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.ViewModel.Inquiries
{
    public class DeadListInquiryVm
    {
        public Guid Id { get; set; }
        public List<string> Message { get; set; }
        public bool Successful { get; set; }
        public bool HasException { get; set; }

    }
}

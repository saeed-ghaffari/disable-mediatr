﻿using System.ComponentModel;

namespace Cerberus.Domain.ViewModel.Api
{
    [DisplayName("اطلاعات ارتباطی")]
    public class AuthenticationOfficeAddressViewModel
    {
        [DisplayName("کشور")]
        public string CountryDescription { get; set; }

        [DisplayName("استان")]
        public string ProvinceDescription { get; set; }

        [DisplayName("شهر")]
        public string CityeDescription { get; set; }

        [DisplayName("خیابان")]
        public string Avenue { get; set; }

        [DisplayName("کوچه")]
        public string Alley { get; set; }

        [DisplayName("پلاک")]
        public string Plaque { get; set; }

        [DisplayName("پیش شماره شهر ")]
        public string CityPrifix { get; set; }

        [DisplayName("تلفن")]
        public string Telephone { get; set; }

        [DisplayName("ایمیل")]
        public string Email { get; set; }

        [DisplayName("پیش شماره کشور")]
        public string CountryPrifix { get; set; }

        [DisplayName("شماره موبایل")]
        public string SelphoneNumbrer { get; set; }

        [DisplayName("بخش")]
        public string SectionDescription { get; set; }

        [DisplayName("دهستان")]
        public string VillageDescription { get; set; }

        [DisplayName("آبادی")]
        public string SubVillageDescription { get; set; }

        [DisplayName("پیش شماره شهر اضطراری")]
        public string EmergencyCityPrifix { get; set; }

        [DisplayName("تلفن ثابت(تماس اضطراری)")]
        public string EmergencyTelephone { get; set; }

        [DisplayName("پیش شماره کشور اضطراری")]
        public string EmergencyCountryPrifix { get; set; }

        [DisplayName("تلفن همراه(تماس اضطراری) ")]
        public string EmergencyMobile { get; set; }
    }
}

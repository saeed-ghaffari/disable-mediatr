﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Cerberus.Domain.ViewModel.Api
{
    [DisplayName("اطلاعات نماینده")]
    public class AuthenticationOfficeAgentViewModel
    {
        [DisplayName("کد ملی نماینده حقیقی")]
        public string PersonCode { get; set; }

        [DisplayName("تاریخ انقضای نمایندگی")]
        public string ExpireDate { get; set; }
        
        [DisplayName("نام نماینده حقوقی")]
        public string CompanyName { get; set; }

        [DisplayName("نام نماینده حقیقی")]
        public string PersonName { get; set; }

        [DisplayName("نوع مشتری")]
        public string RealLegalDescription { get; set; }

        [DisplayName("وضعیت مدرک ارسال شده")]
        public bool  IsConfirmDescription { get; set; }
        [DisplayName("نوع مدرک اثبات کننده سمت")]
        public string TypeOfAttornyAvidenceDescription { get; set; }
    }
}

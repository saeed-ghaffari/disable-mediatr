﻿using System;
using ProtoBuf;

namespace Cerberus.Api.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class AddressSectionViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
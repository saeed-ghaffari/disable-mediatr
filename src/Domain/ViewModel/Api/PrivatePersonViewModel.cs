﻿using System;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PrivatePersonViewModel
    {

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FatherName { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Gender Gender { get; set; }

        public string SeriShChar { get; set; }

        public string SeriSh { get; set; }

        public string Serial { get; set; }

        public string ShNumber { get; set; }

        public DateTime BirthDate { get; set; }

        public string PlaceOfIssue { get; set; }

        public string PlaceOfBirth { get; set; }

        public FileViewModel SignatureFile { get; set; }

        [JsonIgnore]
        public bool? IsConfirmed { get; set; }

    }
}
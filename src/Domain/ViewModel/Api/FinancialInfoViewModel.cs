﻿using System;
using System.Collections.Generic;
using Cerberus.Domain.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class FinancialInfoViewModel
    {
        public long? AssetsValue { get; set; }
        public long? InComingAverage { get; set; }
        public long? SExchangeTransaction { get; set; }
        public long? CExchangeTransaction { get; set; }
        public long? OutExchangeTransaction { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public TransactionLevel TransactionLevel { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public TradingKnowledgeLevel TradingKnowledgeLevel { get; set; }
        public string CompanyPurpose { get; set; }
        public string ReferenceRateCompany { get; set; }
        public DateTime? RateDate { get; set; }
        public long? Rate { get; set; }


        public IEnumerable<FinancialBrokerViewModel> FinancialBrokers { get; set; }
    }
}
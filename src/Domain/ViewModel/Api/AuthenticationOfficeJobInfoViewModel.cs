﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.ViewModel.Api
{
    [DisplayName("اطلاعات شغلی")]
    public class AuthenticationOfficeJobInfoViewModel
    {
        [DisplayName("تاریخ اشتغال")]
        public string EmployeDate { get; set; }

        [DisplayName("نام شرکت")]
        public string CompanyName { get; set; }

        [DisplayName("نشانی شرکت")]
        public string CompanyAdress { get; set; }

        [DisplayName("کد پستی شرکت")]
        public string CompanyPostalCode { get; set; }

        [DisplayName("سایت شرکت")]
        public string CompanyWebSite { get; set; }

        [DisplayName("پیش شماره شرکت")]
        public string CompanyTellPrifix { get; set; }

        [DisplayName("شماره شرکت")]
        public string CompanyTell { get; set; }

        [DisplayName("سمت")]
        public string Position { get; set; }

        [DisplayName("پیش شماره دورنگار شرکت")]
        public string CompanyFaxPrifix { get; set; }

        [DisplayName("شماره دور نگار شرکت")]
        public string CompanyFax { get; set; }

        [DisplayName("عنوان شغل")]
        public string jobDescription { get; set; }

    }
}

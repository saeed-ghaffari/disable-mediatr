﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;

namespace Cerberus.Api.ViewModel
{
    [DisplayName("اشخاص حقیقی ایرانی")]
    public class AuthenticationOfficePrivatePersonViewModel
    {
        [DisplayName("کد ملی")]
        public string NationalCode { get; set; }
        [DisplayName("تاریخ تولد")] 
        public string BirthDate { get; set; }
        [DisplayName("بخش حروفی سری شناسنامه")]
        public string SeriChar { get; set; }
        [DisplayName("بخش عددی سری شناسنامه")]
        public string Seri { get; set; }
        [DisplayName("سریال شناسنامه")]
        public string Serial { get; set; }
        [DisplayName("کد پستی")]
        public string PostalCode { get; set; }
        [DisplayName("نام")]
        public string FirstName { get; set; }
        [DisplayName("نام خانوادگی")]
        public string LastName { get; set; }
        [DisplayName("نام پدر")]
        public string FatherName { get; set; }
        [DisplayName("جنسیت")]
        public string GenderDescription { get; set; }
        [DisplayName("شماره شناسنامه")]
        public string ShenasnameNumber { get; set; }
        [DisplayName("محل صدور شناسنامه")]
        public string PlaceOfIssue { get; set; }
        [DisplayName("محل تولد")]
        public string PlaceOfBirth { get; set; }
    }
}

﻿using System;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class IssuanceStokeCodeResponseViewModel
    {
        public long IssuanceStockCodeId { get; set; }
        public string LegacyCode { get; set; }
    }
}

﻿using System;
using Cerberus.Domain.Model;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class LegalPersonViewModel
    {
        public string CompanyName { get; set; }

        public Country CitizenshipCountry { get; set; }

        public string RegisterNumber { get; set; }

        public string RegisterPlace { get; set; }

        public DateTime RegisterDate { get; set; }

        public string EconomicCode { get; set; }

        public string EvidenceReleaseCompany { get; set; }

        public DateTime? EvidenceReleaseDate { get; set; }

        public DateTime? EvidenceExpirationDate { get; set; }
    }
}
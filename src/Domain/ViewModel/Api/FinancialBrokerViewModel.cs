﻿using System;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class FinancialBrokerViewModel
    {
        public BrokerViewModel Broker { get; set; }
    }
}
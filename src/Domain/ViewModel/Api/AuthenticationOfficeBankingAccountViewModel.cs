﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Cerberus.Domain.ViewModel.Api
{
    [DisplayName("حساب بانکی")]
    public class AuthenticationOfficeBankingAccountViewModel
    {
        [DisplayName("شماره حساب")]
        public string Acc_No { get; set; }

        [DisplayName("کد شعبه")]
        public string BrancCode { get; set; }

        [DisplayName("نوع حساب")]
        public string Acc_Type { get; set; }

        [DisplayName("کد شبا")]
        public string Sheba { get; set; }

        [DisplayName("نام صاحب حساب")]
        public string Acc_Owner { get; set; }

        [DisplayName("نام شعبه")]
        public string BranchName { get; set; }

        [DisplayName("نام شهرستان")]
        public string City { get; set; }

        [DisplayName("حساب پیش فرض")]
        public bool IsDefaultDescription { get; set; }

        [DisplayName("نام بانک")]
        public string BankName { get; set; }
    }
}

﻿using System;
using Cerberus.Domain.Enum;
using Newtonsoft.Json;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class AuthenticateOfficeViewModel
    {
        [JsonIgnore]
        public long Id { get; set; }
        public long ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public long CityId { get; set; }
        public string CityName { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string ResponsibleName { get; set; }
        public OfficeType Type { get; set; }
        //public string TypeDescription { get; set; }
    }
}
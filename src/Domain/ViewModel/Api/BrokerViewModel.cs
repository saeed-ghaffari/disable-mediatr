﻿using System;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class BrokerViewModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
    }
}
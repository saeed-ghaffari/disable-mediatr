﻿using System;
using Cerberus.Domain.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PrivatePersonWithStatusViewModel
    {
        public string Mobile { get; set; }


        [JsonConverter(typeof(StringEnumConverter))]
        public ProfileStatus Status { get; set; }

        public PrivatePersonViewModel PrivatePerson { get; set; }
    }
}
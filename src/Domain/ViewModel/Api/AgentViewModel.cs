﻿using System;
using Cerberus.Domain.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class AgentViewModel
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public AgentType Type { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string Description { get; set; }

        public string UniqueIdentifier { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
        public bool? IsConfirmed { get; set; }

    }
}
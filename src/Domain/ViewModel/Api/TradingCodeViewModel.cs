﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class TradingCodeViewModel
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public TradingType Type { get; set; }

        public string FirstPart { get; set; }

        public string SecondPart { get; set; }

        public string ThirdPart { get; set; }

        [ProtoIgnore]
        public string Code => $"{FirstPart}{SecondPart}{ThirdPart}";
    }
}
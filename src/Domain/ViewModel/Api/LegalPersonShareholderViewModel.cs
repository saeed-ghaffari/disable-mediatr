﻿using System;
using Cerberus.Domain.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class LegalPersonShareholderViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UniqueIdentifier { get; set; }

        public string PostalCode { get; set; }

        public string Address { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public StakHolderPositionType PositionType { get; set; }

        public int PercentageVotingRight { get; set; }
    }
}
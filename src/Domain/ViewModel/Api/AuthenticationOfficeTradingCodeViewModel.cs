﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Cerberus.Domain.ViewModel.Api
{
    [DisplayName(" کد  های معاملاتی ")]
    public class AuthenticationOfficeTradingCodeViewModel
    {
        [DisplayName("عنوان بورس")]
        public string BurseDescription { get; set; }

        [DisplayName("کد معاملاتی بخش اول")]
        public string BurseCode1 { get; set; }

        [DisplayName("کد معاملاتی بخش دوم")]
        public string BurseCode2 { get; set; }

        [DisplayName("کد معاملاتی بخش سوم")]
        public string BurseCode3 { get; set; }

    }
}

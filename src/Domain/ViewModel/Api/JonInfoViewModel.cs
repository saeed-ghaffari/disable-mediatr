﻿using System;
using Cerberus.Domain.Model;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class JobInfoViewModel
    {
        public DateTime? EmploymentDate { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPostalCode { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyWebSite { get; set; }
        public string CompanyCityPrefix { get; set; }
        public string CompanyPhone { get; set; }
        public string Position { get; set; }
        public string CompanyFaxPrefix { get; set; }
        public string CompanyFax { get; set; }
        public JobViewModel Job { get; set; }
        public string JobDescription { get; set; }

    }
}
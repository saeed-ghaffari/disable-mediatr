﻿using Cerberus.Domain.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cerberus.Domain.ViewModel
{
    public class CompactProfile
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ProfileOwnerType Type { get; set; }

        public CompactPrivatePerson PrivatePerson { get; set; }

        public CompactLegalPerson LegalPerson { get; set; }

        public string Iban { get; set; }
    }
}
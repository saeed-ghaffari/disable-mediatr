﻿namespace Cerberus.Domain.ViewModel
{
    public class Meta
    {
        public int OperationResult { get; set; }
        public int OperationResultCode { get; set; }
        public string Message { get; set; }
    }
}

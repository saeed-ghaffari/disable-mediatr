﻿using System.Runtime.Serialization;

namespace Cerberus.Domain.ViewModel
{
    [DataContract]
    public class TokenClaim
    {
        [DataMember(Order = 1)]
        public string Mobile { get; set; }

        [DataMember(Order = 2)]
        public string SessionKey { get; set; }

        [DataMember(Order = 3)]
        public string UniqueIdentifier { get; set; }
    }
}
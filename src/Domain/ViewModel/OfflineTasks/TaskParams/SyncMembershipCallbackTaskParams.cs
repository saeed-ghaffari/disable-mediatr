﻿namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class CallbackThirdPartyServicesTaskParams
    {
        public long ServiceId { get; set; }
        public string CallbackUrl { get; set; }
        public object Payload { get; set; }
    }
}
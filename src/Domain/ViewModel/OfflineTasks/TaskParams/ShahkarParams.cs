﻿using Cerberus.Domain.Enum;

namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class ShahkarParams
    {
        public long Mobile { get; set; }
        public string UniqueIdentifier { get; set; }
        public long? NewMobile { get; set; }
        public ProfileStatus Status { get; set; }
    }
}

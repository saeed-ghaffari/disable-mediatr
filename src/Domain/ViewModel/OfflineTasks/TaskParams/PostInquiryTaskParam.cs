﻿namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class PostInquiryTaskParam
    {
        public string Ssn { get; set; }
        public string BirthDate { get; set; }
        public long ReferenceId { get; set; }
        public string Mobile { get; set; }
    }
}
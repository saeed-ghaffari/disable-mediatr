﻿namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class ChangeProfileDataTaskParam
    {
        public long ProfileId { get; set; }
        public ProfileField ProfileField { get; set; }
        public string Value { get; set; }
    }

    public enum ProfileField
    {
        None = 0,
        Mobile = 1,
        UniqueIdentifier = 2
    }
}
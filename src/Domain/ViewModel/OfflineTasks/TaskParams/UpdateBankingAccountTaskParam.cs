﻿using Cerberus.Domain.Enum;

namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class UpdateBankingAccountTaskParam
    {
        public string Iban { get; set; }
        public string Mobile { get; set; }
        public long ReferenceId { get; set; }
        public TaskCreatorType TaskCreator { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
   public class SendSmsTaskParam
    {
        public long ProfileId { get; set; }
        public string SmsText { get; set; }
        public string Msisdn { get; set; }
    }
}

﻿namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class DeleteAgentEighteenTaskParam
    {
        public int DaysLeftToEighteen { get; set; }

    }
}
﻿using System.Collections.Generic;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class GetPrivatePersonTaskParam
    {
        public long ServiceId { get; set; }
        public ProfileStatus ProfileStatus { get; set; }
        public string CallbackUrl { get; set; }
        public IEnumerable<string> UniqueIdentifiers { get; set; }
    }
}
﻿namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class SendReportBySmsTaskParam
    {
        public int? GroupId { get; set; }
    }
}
﻿using System.Collections.Generic;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class UpdateIssuanceCodeTaskParams
    {
        public List<long> Id { get; set; }
        public IssuanceStockCodeStatus Status { get; set; }
        public bool Successful { get; set; }
    }
}

﻿using System.Collections.Generic;
using Cerberus.Domain.ViewModel.Membership;

namespace Cerberus.Domain.ViewModel.OfflineTasks
{
    public class SyncEtfMembershipTaskParams
    {
        public long ServiceId { get; set; }
       public IEnumerable<EtfMemberShipViewModel> Members { get; set; }
    }
}
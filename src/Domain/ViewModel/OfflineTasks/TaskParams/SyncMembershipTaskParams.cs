﻿using System.Collections.Generic;
using Cerberus.Domain.ViewModel.Membership;

namespace Cerberus.Domain.ViewModel.OfflineTasks
{
    public class SyncMembershipTaskParams
    {
        public long ServiceId { get; set; }
        public string CallbackUrl { get; set; }
        public bool FromAdmin { get; set; }
        public IEnumerable<Member> Members { get; set; }
    }
}
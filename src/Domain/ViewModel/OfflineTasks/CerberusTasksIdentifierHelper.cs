﻿using System.Collections.Generic;

namespace Cerberus.Domain.ViewModel.OfflineTasks
{
    public static class CerberusTasksIdentifierHelper
    {
        public static KeyValuePair<long, string> IbanInquiry => new KeyValuePair<long, string>(1, "iban-inquiry");
        public static KeyValuePair<long, string> IlencInquiry => new KeyValuePair<long, string>(2, "ilenc-inquiry");
        public static KeyValuePair<long, string> NocrInquiry => new KeyValuePair<long, string>(3, "nocr-inquiry");
        public static KeyValuePair<long, string> LegacyInquiry => new KeyValuePair<long, string>(4, "legacy-inquiry");
        public static KeyValuePair<long, string> IrenexInquiry => new KeyValuePair<long, string>(5, "irenex-inquiry");
        public static KeyValuePair<long, string> IndividualInquiry => new KeyValuePair<long, string>(6, "individual-inquiry");
        public static KeyValuePair<long, string> InstitutionInquiry => new KeyValuePair<long, string>(7, "institution-inquiry");
        public static KeyValuePair<long, string> InquiryChecker => new KeyValuePair<long, string>(8, "inquiry-checker");
        public static KeyValuePair<long, string> AgentInquiry => new KeyValuePair<long, string>(9, "agent-inquiry");
        public static KeyValuePair<long, string> SyncMembershipProcessing => new KeyValuePair<long, string>(10, "sync-membership-processing");
        public static KeyValuePair<long, string> CallbackThirdPartyServices => new KeyValuePair<long, string>(11, "callback-third-party-services");
        public static KeyValuePair<long, string> Synchronizer => new KeyValuePair<long, string>(12, "synchronizer");
        public static KeyValuePair<long, string> CreateInquiriesTasks => new KeyValuePair<long, string>(13, "create-inquiries-tasks");
        public static KeyValuePair<long, string> GetBankingAccounts => new KeyValuePair<long, string>(14, "get-banking-accounts");
        public static KeyValuePair<long, string> GetProfileStatus => new KeyValuePair<long, string>(15, "get-profile-status");
        public static KeyValuePair<long, string> GetProfileIsSejami => new KeyValuePair<long, string>(16, "get-profile-is-sejami");
        public static KeyValuePair<long, string> GetPrivatePerson => new KeyValuePair<long, string>(17, "get-profile-privatePerson");
        public static KeyValuePair<long, string> GetTraceCode => new KeyValuePair<long, string>(18, "get-profile-trace-code");
        public static KeyValuePair<long, string> SendSms => new KeyValuePair<long, string>(19, "send-sms");
        public static KeyValuePair<long, string> GetCompactProfile => new KeyValuePair<long, string>(20, "get-compact-profile");
        public static KeyValuePair<long, string> UpdatePrivatePerson => new KeyValuePair<long, string>(22, "update-private-person");
        public static KeyValuePair<long, string> UpdateBanckingAccount => new KeyValuePair<long, string>(23, "update-banking-account");
        public static KeyValuePair<long, string> ChangeProfileToSemiSejami => new KeyValuePair<long, string>(24, "change-profile-to-semi-sejami");
        public static KeyValuePair<long, string> SendReportBySms => new KeyValuePair<long, string>(25, "send-report-by-sms");
        public static KeyValuePair<long, string> GetProfilePrivatePerson => new KeyValuePair<long, string>(26, "get-profile-privatePerson-status");
        public static KeyValuePair<long, string> CreateEditAfterSejamiTasks => new KeyValuePair<long, string>(27, "create-edit-after-sejami-tasks");
        public static KeyValuePair<long, string> DeleteAgentEighteen => new KeyValuePair<long, string>(28, "delete-agent-eighteen");
        public static KeyValuePair<long, string> ChangeProfileStatus => new KeyValuePair<long, string>(29, "change-profile-status");
        public static KeyValuePair<long, string> IssuanceStockCodeOne => new KeyValuePair<long, string>(30, "issuance-stock-code-one");
        public static KeyValuePair<long, string> ChangeProfileData => new KeyValuePair<long, string>(31, "change-profile-data");
        public static KeyValuePair<long, string> PostInquiry => new KeyValuePair<long, string>(32, "post-inquiry");
        public static KeyValuePair<long, string> DeadRequestInquiry => new KeyValuePair<long, string>(33, "dead-request-inquiry");
        public static KeyValuePair<long, string> DeadDownloadInquiry => new KeyValuePair<long, string>(34, "dead-download-inquiry");
        public static KeyValuePair<long, string> SetFactor => new KeyValuePair<long, string>(35, "set-factor");
        public static KeyValuePair<long, string> SetFakeFactor => new KeyValuePair<long, string>(36, "set-fake-factor");
        public static KeyValuePair<long, string> UpdateIssuanceStockCode => new KeyValuePair<long, string>(37, "update-issuance-stock-code");
        public static KeyValuePair<long, string> SyncStockCodeWithIx => new KeyValuePair<long, string>(38, "sync-stock-code-with-ix");
        public static KeyValuePair<long, string> OldIssuanceStockCodeOne => new KeyValuePair<long, string>(39, "old-issuance-stock-code-one");
        public static KeyValuePair<long, string> SendSms1 => new KeyValuePair<long, string>(42, "send-sms-1");
        public static KeyValuePair<long, string> SendSms2 => new KeyValuePair<long, string>(43, "send-sms-2");
        public static KeyValuePair<long, string> SendSms3 => new KeyValuePair<long, string>(44, "send-sms-3");
        public static KeyValuePair<long, string> OldIssuanceStockCodeTwo => new KeyValuePair<long, string>(45, "old-issuance-stock-code-two");
        public static KeyValuePair<long, string> SyncEtfMembershipProcessing => new KeyValuePair<long, string>(46, "sync-etf-membership-processing");
        public static KeyValuePair<long, string> OldIssuanceStockCodeThree => new KeyValuePair<long, string>(47, "old-issuance-stock-code-three");

        public static KeyValuePair<long, string> IssuanceStockCodeTwo => new KeyValuePair<long, string>(48, "issuance-stock-code-two");
        public static KeyValuePair<long, string> IssuanceStockCodeThree => new KeyValuePair<long, string>(49, "issuance-stock-code-three");
      
        public static KeyValuePair<long, string> InquirySuspiciousPayment => new KeyValuePair<long, string>(50, "inquiry-suspicious-payment");

        public static KeyValuePair<long, string> OldIssuanceStockCodeFour => new KeyValuePair<long, string>(53, "old-issuance-stock-code-four");
        public static KeyValuePair<long, string> OldIssuanceStockCodeFive => new KeyValuePair<long, string>(54, "old-issuance-stock-code-five");
        public static KeyValuePair<long, string> OldIssuanceStockCodeSix => new KeyValuePair<long, string>(55, "old-issuance-stock-code-six");
        public static KeyValuePair<long, string> OldIssuanceStockCodeSeven => new KeyValuePair<long, string>(56, "old-issuance-stock-code-seven");
        public static KeyValuePair<long, string> OldIssuanceStockCodeEight => new KeyValuePair<long, string>(57, "old-issuance-stock-code-eight");
        public static KeyValuePair<long, string> OldIssuanceStockCodeNine => new KeyValuePair<long, string>(58, "old-issuance-stock-code-nine");
        public static KeyValuePair<long, string> OldIssuanceStockCodeTen => new KeyValuePair<long, string>(59, "old-issuance-stock-code-ten");
        public static KeyValuePair<long, string> OldIssuanceStockCodeEleven => new KeyValuePair<long, string>(60, "old-issuance-stock-code-eleven");
        public static KeyValuePair<long, string> OldIssuanceStockCodeTwelve => new KeyValuePair<long, string>(61, "old-issuance-stock-code-twelve");
        public static KeyValuePair<long, string> OldIssuanceStockCodeThirteen => new KeyValuePair<long, string>(62, "old-issuance-stock-code-thirteen");
        public static KeyValuePair<long, string> OldIssuanceStockCodeFourteen => new KeyValuePair<long, string>(63, "old-issuance-stock-code-fourteen");
        public static KeyValuePair<long, string> OldIssuanceStockCodeFifteen => new KeyValuePair<long, string>(64, "old-issuance-stock-code-fifteen");
        public static KeyValuePair<long, string> OldIssuanceStockCodeSixteen => new KeyValuePair<long, string>(65, "old-issuance-stock-code-sixteen");
        public static KeyValuePair<long, string> OldIssuanceStockCodeSeventeen => new KeyValuePair<long, string>(66, "old-issuance-stock-code-seventeen");
        public static KeyValuePair<long, string> OldIssuanceStockCodeEighteen => new KeyValuePair<long, string>(67, "old-issuance-stock-code-eighteen");
        public static KeyValuePair<long, string> OldIssuanceStockCodeNinteen => new KeyValuePair<long, string>(68, "old-issuance-stock-code-ninteen");
        public static KeyValuePair<long, string> OldIssuanceStockCodeTwenty => new KeyValuePair<long, string>(69, "old-issuance-stock-code-twenty");
        public static KeyValuePair<long, string> ShahkarInquiry => new KeyValuePair<long, string>(51, "shahkar-inquiry");
        public static KeyValuePair<long, string> UpdateProfileCache => new KeyValuePair<long, string>(52, "update-profile-cache");

    }
}
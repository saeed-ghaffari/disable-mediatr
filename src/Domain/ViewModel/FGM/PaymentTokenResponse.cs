﻿using System;

namespace Cerberus.Domain.ViewModel.FGM
{
  public  class PaymentTokenResponse
    {
        public string PaymentToken { get; set; }
        public TimeSpan Ttl { get; set; }
    }
}

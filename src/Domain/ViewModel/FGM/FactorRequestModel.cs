﻿using System;
using System.Collections.Generic;
using System.Text;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.ViewModel.FGM
{
    public class FactorRequestModel
    {
        public string OrderId { get; set; }
        public string ServiceId { get; set; }
        public string FullName { get; set; }
        public string UniqueIdentifier { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public ProfileOwnerType ProfileOwnerType { get; set; }
    }
}

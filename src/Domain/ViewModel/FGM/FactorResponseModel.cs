﻿using System;
using System.Collections.Generic;
using System.Text;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.ViewModel.FGM
{
    public class FactorResponseModel
    {
        public string InvoiceNumber { get; set; }
        public string SaleReferenceId { get; set; }
        public DateTimeOffset IssueDate { get; set; }
        public string OrderId { get; set; }
        public int Amount { get; set; }
        public long FactorId { get; set; }
    }
}

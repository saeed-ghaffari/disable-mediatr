﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Domain.ViewModel.FGM
{
    public class PaymentRequest
    {
        public int Discount { get; set; }
        [Required]
        public string CreditId { get; set; }
        [Required]
        public int Amount { get; set; }
        [Required]
        public Guid ServiceId { get; set; }
        [Required]
        public string ClientRefId { get; set; }
        [Required]
        public string ReferenceNumber { get; set; }
        [Required]
        public string SaleReferenceId { get; set; }

    }
}

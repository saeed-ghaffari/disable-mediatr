﻿namespace Cerberus.Domain.ViewModel.FGM
{
    public class VerifyPaymentResponse
    {
        public bool Successful { get; set; }
    }
}
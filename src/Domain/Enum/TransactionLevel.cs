﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum TransactionLevel : byte
    {
        [Description("کمتر از 250 میلیون ریال")]
        One = 1,
        [Description("بین ۲۵۰ تا ۱۰۰۰ میلیون ریال")]
        Two = 2,
        [Description("بین 1000 تا 5000 میلیون ریال")]
        Three = 3,
        [Description("بین 5000 تا 10000 میلیون ریال")]
        Four = 4,
        [Description("بیش از  10000 میلیون ریال")]
        Five = 5,


        [Description("کمتر از 500 میلیون ریال")]
        Eleven = 11,
        [Description("بین 500 تا 1000 میلیون ریال")]
        Twelve = 12,
        [Description("بین 1000 تا 5000 میلیون ریال")]
        Thirteen = 13,
        [Description("بین 5000 تا 10000 میلیون ریال")]
        Fourteen = 14,
        [Description("بیش از  10000 میلیون ریال")]
        Fifteen = 15
    }
}
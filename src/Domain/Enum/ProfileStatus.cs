﻿namespace Cerberus.Domain.Enum
{
    public enum ProfileStatus : byte
    {
        Init = 1,

        SuccessPayment = 2,

        PolicyAccepted = 3,

        PendingValidation = 4,

        InvalidInformation = 5,

        TraceCode = 6,

        Sejami = 7,

        Suspend = 8,

        Dead = 9,

        SemiSejami = 10
    }


}
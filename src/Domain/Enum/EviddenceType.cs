﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum EvidenceType
    {
        [Description("گذرنامه معتبر")]
        Passport = 1,

        [Description("کارت هویت")]
        IdentityCard = 2,

        [Description("برگ آمایش اتباع غیر ایرانی")]
        NonIranianCitizensMeeting = 3
    }
}
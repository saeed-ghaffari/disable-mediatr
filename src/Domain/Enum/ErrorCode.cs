﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum ErrorCode
    {
        Successful = 2001,

        NotFound = 4041,

        UndefinedService = 4042,

        ThirdPartyServiceNotFound = 4043,

        //invalid request
        BadRequest = 400,

        [Description("")]
        InvalidMsisdn = 4001,

        InvalidOtpTime = 4002,

        InCompleteRegistrations = 4004,

        InvalidTraceCode = 4005,

        Unauthorized = 4010,

        LockedUser = 4011,

        Forbidden = 403,

        InvalidOtp = 4031,

        Conflicts = 4090,

        UnknownServerError = 500,

        FailedSendSms = 600,

        FailedPayment = 601,
        SuspiciousPayment = 602,

        ToManyRequest = 4290,

        UnsupportedMediaType = 4150,

        ImageTooLarge=4008,
        ImageTooSmall = 4009,

        TooManyRequest=429

    }
}
﻿namespace Cerberus.Domain.Enum
{
    public enum MembershipDataType
    {
        PrivatePerson,
        BankingAccount,
        Address
    }
}
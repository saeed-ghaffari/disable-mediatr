﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum AgentType : byte
    {
        [Description("وکالت")]
        Attorney = 1,
        [Description("ولایت")]
        Province = 2,
        [Description("قیومیت")]
        Conspiracy = 3,
        [Description("وصایت")]
        Prescriptive = 4
    }
}
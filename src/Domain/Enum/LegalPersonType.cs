﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum LegalPersonType
    {
        [Description("دولتی")]
        Governmental = 1,
        [Description("غیر دولتی")]
        NonGovernmental = 2,
        [Description("موسسات غیر تجاری")]
        NonCommercial = 3,
        [Description("سهامی خاص")]
        PrivateCompany = 4,
        [Description("با مسئولیت محدود")]
        LimitedCompany = 5,
        [Description("سهامی عام")]
        PublicCompany = 6,
        [Description("تعاونی")]
        CooperativeCompany = 7,
        [Description("تضامنی")]
        GeneralPartnership = 8,
        [Description("نسبی")]
        ProportionalLiabilityPartnership = 9,
        [Description("مختلط سهامی")]
        JointMixedCompany = 10,
        [Description("صندوق های سرمایه گذاری")]
        InvestmentFunds = 11,
    }
}
﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Domain.Enum
{
    public enum OfficeType
    {
        [Description("نامشخص")]
        Unknown =0,
        [Description("پیشخوان")]
        Authentication = 1,
        [Description( "کارگزاری")]
        Broker = 2
    }
}
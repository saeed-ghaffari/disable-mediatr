﻿namespace Cerberus.Domain.Enum
{
    public enum PaymentGateway : byte
    {
        Mellat = 1,
        AsanPardakht = 2,
        Meli = 3,
        CompleteDiscount = 4,
        EntryNodeWithPayment = 5,
        EntryNodeWithNoPayment = 6,
        Fgm = 7,
        FgmEntryNode=8,
   
    }
}
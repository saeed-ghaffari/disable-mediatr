﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum PrimitivePackageType : byte
    {

        [Description("ویرایش تلفن همراه")]
        EditMobileNumber = 1,


        [Description("ویرایش تلفن همراه با سرویس شاهکار")]
        EditMobileNumberWithShahkar = 2,


        [Description("ویرایش اطلاعات هویتی و بانکی")]
        EditPrivatePersonInfo = 3,


        [Description("ویرایش اطلاعات حساب بانکی")]
        EditBankingAccount = 4,
    }
}
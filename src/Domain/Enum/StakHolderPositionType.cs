﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum StakHolderPositionType: byte
    {
        [Description("رئیس هیئت مدیره")]
        Chairman = 1,
        [Description(" معاون رئیس هیئت مدیره")]
        DeputyChairman = 2,
        [Description("مدیر عامل")]
        Ceo = 3,
        [Description(" عضو هیئت مدیره")]
        Member = 4,
        [Description("سایر")]
        Others = 5,
        [Description("عضو هیئت امنا")]
        Trustee = 6,
        [Description("عضو ارکان مشابه")]
        Pillars = 7,
        [Description("بازرس")]
        Inspector = 8,
        [Description("حسابرس")]
        Auditor = 9,

    }
}
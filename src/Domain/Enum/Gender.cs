﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum Gender:byte
    {
        [Description("مرد")]
        Male=1,
        [Description("زن")]
        Female =2
    }
}
﻿namespace Cerberus.Domain.Enum
{
    public enum SynchronizerTarget
    {
        FinancialInfo,
        BankingAccount,
        TradingCode,
        Address,
        Agent,
        LegalPerson,
        PrivatePerson,
        Profile,
        JobInfo,
        LegalPersonShareholder,
        LegalPersonStakeholder,
        ThirdPartyService,
        FinancialInfoBroker,
        AgentDocument,
        PrivatePersonMembership,
        AddressMembership,
        BankingAccountMembership,
        PermanentOtp
    }
}
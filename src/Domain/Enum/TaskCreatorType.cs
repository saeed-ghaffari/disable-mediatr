﻿namespace Cerberus.Domain.Enum
{
    public enum TaskCreatorType
    {
        UpdatePrivatePerson=1,
        UpdateBankingAccount=2
    }
}
﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum TradingType : byte
    {
        [Description("انرژی - برق")]
        Energy = 1,
        [Description("کالا")]
        Product = 2,
        [Description("بورس - فرابورس")]
        StockExchange = 3,
        [Description("نامشخص")]
        None = 4

    }
}
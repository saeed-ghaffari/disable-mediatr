﻿using System;
using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum PackageType : byte
    {
        [Description("ثبت نام")]
        Registration = 1,


        [Description("ویرایش تلفن همراه")]
        EditMobileNumber = 2,


        [Description("ویرایش اطلاعات هویتی و بانکی")]
        EditPrivatePersonInfo = 4,


        [Description("ویرایش اطلاعات حساب بانکی")]
        EditBankingAccount = 8,


        [Description("ویرایش اطلاعات")]
        EditAllData = 16,


    }
}
﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum ProfileOwnerType : byte
    {
        [Description("حقیقی ایرانی")]
        IranianPrivatePerson = 1,

        [Description("حقوقی ایرانی")]
        IranianLegalPerson = 2,

        [Description("حقیقی خارجی")]
        ForeignPrivatePerson = 3,

        [Description("حقوقی خارجی")]
        ForeignLegalPerson = 4
    }
}
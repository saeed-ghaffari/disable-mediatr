﻿namespace Cerberus.Domain.Enum
{
    public enum EntityType
    {
        PrivatePerson,
        LegalPerson,
        Payments,
        Addresses,
        TradingCodes,
        Agent,
        Accounts,
        JobInfo,
        ProfileHistory,
        FinancialInfo,
        LegalPersonStakeholders,
        LegalPersonShareholders,
        NocrInquiry,
        Packages,
        Messages,
    }
}
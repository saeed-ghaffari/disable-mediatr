﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum StakeHolderType : byte
    {
        [Description("هیئت مدیره")]
        Manager = 1,
        [Description("دارندگان حق برداشت")]
        TakeAccess = 2,
        [Description("دارندگان حق سفارش")]
        OrderAccess = 3
    }
}
﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum TradingKnowledgeLevel : byte
    {
        [Description("عالی")]
        Excellent = 1,
        [Description("خوب")]
        Good = 2,
        [Description("متوسط")]
        Medium = 3,
        [Description("کم")]
        Low = 4,
        [Description("بسیار کم")]
        VeryLow = 5
    }
}
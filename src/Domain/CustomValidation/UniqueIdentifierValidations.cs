﻿using Cerberus.Domain.Enum;
using Cerberus.Utility;

namespace Cerberus.Domain.CustomValidation
{
    public static class UniqueIdentifierValidations
    {
        public static void CheckValidUniqueIdentifier(string uniqueIdentifier)
        {
            if (!uniqueIdentifier.IsValidUniqueIdentifier())
                throw new CerberusException(ErrorCode.BadRequest, "invalid uniqueIdentifier");
        }
    }
}

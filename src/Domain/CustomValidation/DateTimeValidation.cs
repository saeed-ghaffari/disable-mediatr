﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Domain.CustomValidation
{
    public class DateTimeValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var str = value != null ? value.ToString() : string.Empty;
            if (!string.IsNullOrWhiteSpace(str))
            {
                if (!DateTime.TryParse(str, out var date))
                    return new ValidationResult("invalid DateTime");

                if (date.Year < 1890)
                    return new ValidationResult("invalid DateTime");
            }

            return ValidationResult.Success;
        }
    }
}
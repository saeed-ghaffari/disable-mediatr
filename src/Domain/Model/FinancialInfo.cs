﻿using System;
using Cerberus.Domain.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("FinancialInfo")]
    public class FinancialInfo : BaseModel
    {
        [ForeignKey(nameof(ProfileOwner))]
        public long ProfileId { get; set; }
        public long? AssetsValue { get; set; }
        public long? InComingAverage { get; set; }
        public long? SExchangeTransaction { get; set; }//trading exchange 
        public long? CExchangeTransaction { get; set; }//commodity
        public long? OutExchangeTransaction { get; set; }//aboard exchange
        public TransactionLevel? TransactionLevel { get; set; }
        public TradingKnowledgeLevel? TradingKnowledgeLevel { get; set; }

        public string CompanyPurpose { get; set; }
        public string ReferenceRateCompany { get; set; }
        public DateTime? RateDate { get; set; }
        public long? Rate { get; set; }

        public Profile ProfileOwner { get; set; }
        public ICollection<FinancialBroker> FinancialBrokers { get; set; }
    }
}

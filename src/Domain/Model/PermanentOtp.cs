﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("PermanentOtp")]
    public class PermanentOtp : BaseModel
    {
        public long ServiceId  { get; set; }
        [MaxLength(15)]
        public string UniqueIdentifier { get; set; }
        public DateTime ExpirationDate { get; set; }
        [MaxLength(15)]
        public string Otp { get; set; }
        public OtpTypes Type { get; set; } 
    }
}

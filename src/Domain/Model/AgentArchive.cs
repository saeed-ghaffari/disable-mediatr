﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.Model
{
    [Table("AgentArchive")]
    public class AgentArchive: InquiryDataModel
    {
        public long AgentId { get; set; }

        public long ProfileId { get; set; }

        public long AgentProfileId { get; set; }

        public AgentType Type { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string Description { get; set; }

        public DateTime AgentCreationDate { get; set; }

        public DateTime AgentModifiedDate { get; set; }

    }
}
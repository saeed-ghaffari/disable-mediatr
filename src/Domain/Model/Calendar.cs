﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cerberus.Domain.Model
{
    [Table("Calendar")]
    public class Calendar : BaseModel
    {
        public string PersianDate { get; set; }
        public DateTime GregorianDate { get; set; }
        public bool IsHoliday { get; set; }
        public string Events { get; set; }
    }
}
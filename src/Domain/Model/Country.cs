﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [Table("Country")]
    public class Country : BaseModel
    {
        public string Name { get; set; }

        public string Prefix { get; set; }

        public IEnumerable<Province> Provinces { get; set; }

        public IEnumerable<Address> Addresses { get; set; }
    }
}
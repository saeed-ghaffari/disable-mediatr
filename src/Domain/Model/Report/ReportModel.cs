﻿namespace Cerberus.Domain.Model.Report
{
    public class ReportModel
    {
        public int RegistrationCount { get; set; }
        public int AgentCount { get; set; }
        public int LegalCount { get; set; }
        public int VerifiedCount { get; set; }
        public int SejamiCount { get; set; }
        public string Link { get; set; }
        public string Province { get; set; }
        public string EntryNode { get; set; }
        public string Club { get; set; }
        public string SejamiPishkhan { get; set; }
        public string SejamiBroker { get; set; }
        public string SejamiBank { get; set; }
        public string SejamiGheirHozuri { get; set; }
        public string SejamiPishkhan2 { get; set; }

    }
}
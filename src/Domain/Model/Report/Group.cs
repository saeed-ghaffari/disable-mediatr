﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cerberus.Domain.Model.Report
{
    public class Group
    {
        public long Id { get; set; }

        public string GroupName { get; set; }
        public IList<GroupMember> GroupMembers { get; set; }

    }
}
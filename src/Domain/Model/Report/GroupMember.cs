﻿namespace Cerberus.Domain.Model.Report
{
    public class GroupMember
    {
        public int Id { get; set; }

        public long GroupId { get; set; }

        public string FullName { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public bool IsActive { get; set; }

        public Group Group { get; set; }

    }
}
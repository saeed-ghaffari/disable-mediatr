﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("PrivatePerson")]
    public class PrivatePerson : InquiryDataModel
    {
        [ForeignKey(nameof(ProfileOwner))]
        public long ProfileId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FatherName { get; set; }

        public Gender Gender { get; set; }

        public string SeriShChar { get; set; }

        public string SeriSh { get; set; }

        public string Serial { get; set; }

        public string ShNumber { get; set; }

        public DateTime BirthDate { get; set; }

        public string PlaceOfIssue { get; set; }

        public string PlaceOfBirth { get; set; }

        [ForeignKey(nameof(BornCountry))]
        public long? BornCountryId { get; set; }

        public Country BornCountry { get; set; }

        [ForeignKey(nameof(EvidenceCountry))]
        public long? EvidenceCountryId { get; set; }

        public Country EvidenceCountry { get; set; }

        public EvidenceType? EvidenceType { get; set; }

        public DateTime? EvidenceExpirationDate { get; set; }

        public string EvidenceNumber { get; set; }

        public string PassportNumber { get; set; }

        [ForeignKey(nameof(CitizenCountry))]
        public long? CitizenCountryId { get; set; }

        public Country CitizenCountry { get; set; }

        public string SpecialNumber { get; set; }

        public string LicenseNumber { get; set; }

        public DateTime? LicenseIssueDate { get; set; }

        public DateTime? LicenseExpirationDate { get; set; }

        [ForeignKey(nameof(ImageFile))]
        public long? ImageId { get; set; }

        public File ImageFile { get; set; }

        [ForeignKey(nameof(SignatureFile))]
        public long? SignatureFileId { get; set; }

        public File SignatureFile { get; set; }


        public string Description { get; set; }



        public Profile ProfileOwner { get; set; }

    }
}
﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cerberus.Domain.Model
{
    [Table("IssuanceStockCodeResponse")]
    public class IssuanceStockCodeResponse : BaseModel
    {
        [ForeignKey(nameof(IssuanceStockCode))]
        public long IssuanceStockCodeId { get; set; }
        public string LegacyCode { get; set; }

        public string InvestorCode { get; set; }
        public string Message { get; set; }
        public IssuanceStockCode IssuanceStockCode { get; set; }

        //by default it's true because any response from AS400 is already active code
        public bool IsActive { get; set; }
    }
}
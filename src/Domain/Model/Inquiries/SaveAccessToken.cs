﻿using System;

namespace Cerberus.Domain.Model.Inquiries
{
    public class SaveAccessToken
    {
        public DateTime Ttl { get; set; }
        public RefreshTokenVM TokenModel { get; set; }
    }
}
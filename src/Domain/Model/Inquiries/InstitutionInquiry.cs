﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Cerberus.Domain.Model
{
    [Table("InstitutionInquiry")]
    public class InstitutionInquiry:BaseModel
    {
        //[ForeignKey(nameof(Profile))]
        //public long ProfileId { get; set; }

        //public Profile Profile { get; set; }

        //[ForeignKey(nameof(LegalPerson))]
        //public long LegalPersonId { get; set; }

        //public LegalPerson LegalPerson { get; set; }
        public string PrivateCode { get; set; }
        public string ActivityType { get; set; }

        public string LatinName { get; set; }

        public string OrganizationType { get; set; }

        public string OwnerType { get; set; }

        public string PersianName { get; set; }

        public string RegistrationDate { get; set; }

        public string RegistrationNumber { get; set; }

        public int RegistrationCountryId { get; set; }

        public string RegistrationCountry { get; set; }

        public string ShortName { get; set; }

        public bool Successful { get; set; }
        /// <summary>
        /// For avoid get more inquiry if it's true
        /// </summary>
        public bool SuccessInquiry { get; set; }

        public string Message { get; set; }

        public bool HasException { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Cerberus.Domain.Model.Inquiries
{
    [Table("IlencInquiry")]
   public class IlencInquiry:BaseModel
    {
        //[ForeignKey(nameof(Profile))]
        //public long ProfileId { get; set; }

        //public Profile Profile { get; set; }

        //[ForeignKey(nameof(LegalPerson))]
        //public long LegalPersonId { get; set; }

        //public LegalPerson LegalPerson { get; set; }
        [JsonProperty(PropertyName = "FollowUpNo")]
        public string FollowUpNo { get; set; }

        [JsonProperty(PropertyName = "NationalCode")]
        public string NationalCode { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "State")]
        public string State { get; set; }

        [JsonProperty(PropertyName = "EstablishmentDate")]
        public string EstablishmentDate { get; set; }

        [JsonProperty(PropertyName = "RegisterDate")]
        public string RegisterDate { get; set; }

        [JsonProperty(PropertyName = "RegisterNumber")]
        public string RegisterNumber { get; set; }

        [JsonProperty(PropertyName = "Address")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "PostCode")]
        public string PostCode { get; set; }

        [JsonProperty(PropertyName = "LastChangeDate")]
        public string LastChangeDate { get; set; }

        [JsonProperty(PropertyName = "Successful")]
        public bool Successful { get; set; }
        /// <summary>
        /// For avoid get more inquiry if it's true
        /// </summary>
        [JsonProperty(PropertyName = "SuccessInquiry")]
        public bool SuccessInquiry { get; set; }

        [JsonProperty(PropertyName = "Message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "HasException")]
        public bool HasException { get; set; }
    }
}

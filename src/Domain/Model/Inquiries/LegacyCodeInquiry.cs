﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Cerberus.Domain.Model.Inquiries
{
    [Table("LegacyCodeInquiry")]
    public class LegacyCodeInquiry : BaseModel
    {
        [JsonProperty(PropertyName = "NinOrSsn")]
        public string NinOrSsn { get; set; }

        [JsonProperty(PropertyName = "Message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "Successful")]
        public bool Successful { get; set; }

        [JsonProperty(PropertyName = "SuccessInquiry")]
        public bool SuccessInquiry { get; set; }

        [JsonProperty(PropertyName = "HasException")]
        public bool HasException { get; set; }

        public IEnumerable<LegacyCodeResponse> LegacyCodeResponseList { get; set; }

    }
}

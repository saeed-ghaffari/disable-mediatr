﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Cerberus.Domain.Model.Inquiries
{
    [Table("PostInquiry")]
    public class PostInquiry : BaseModel
    {
        public string Ssn { get; set; }
        public string BirthDate { get; set; }
        public string EndUsername { get; set; }
        public string EndIp { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string Message { get; set; }
        public bool Successful { get; set; }
        public bool SuccessInquiry { get; set; }
        public bool HasException { get; set; }

    }
}
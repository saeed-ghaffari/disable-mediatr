﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Cerberus.Domain.Model.Inquiries
{
    [Table("DeadInquiry")]
    public class DeadInquiry:BaseModel
    {
        public string UniqueIdentifier  { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string PersianBirthDate { get; set; }
        public string Message { get; set; }
        public bool IsOk { get; set; }
        public bool SuccessInquiry { get; set; }
        public bool HasException { get; set; }
        public Guid FileName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime DeadDate { get; set; }
    }
}

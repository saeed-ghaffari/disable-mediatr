﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.Model.Inquiries
{
    public class Item
    {
        public string attribute { get; set; }
        public string @operator { get; set; }
        public string value { get; set; }
    }

    public class Filter
    {
        public Filter()
        {
            items = new List<Item>();
        }

        public string _operator { get; set; }
        public List<Item> items { get; set; }
    }

    public class AwaUserCredentials
    {
        public string userID { get; set; }
        public string userIP { get; set; }
        public string applicationName { get; set; }
        public string context { get; set; }
    }

    public class AwaRequestObject
    {
        public AwaRequestObject()
        {
            filter = new Filter();
            awaUserCredentials = new AwaUserCredentials();

        }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public Filter filter { get; set; }
        public AwaUserCredentials awaUserCredentials { get; set; }
    }
}

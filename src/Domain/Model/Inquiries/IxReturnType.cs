﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Cerberus.Domain.Model.Inquiries
{
    public class Properties
    {
        public string CIFNAME { get; set; }
        public string CILNAME { get; set; }
        public string CISNAM { get; set; }
        public string CIBPHO { get; set; }
        public string CISHSERIE { get; set; }
        public string CISHCODE { get; set; }
        public string CIRPHO { get; set; }
        public string CIGENDER { get; set; }
        public string CIFAX { get; set; }
        public string CIBORN { get; set; }
        public string CIBPRO { get; set; }
        public string CIBCOU { get; set; }
        public string CINSPO { get; set; }
        public int CIISPO { get; set; }
        public string CINPRI { get; set; }
        public int CIIPRI { get; set; }
        public string CINNXT { get; set; }
        public int CIINXT { get; set; }
        public string CIACTV { get; set; }
        public string CIIDAT { get; set; }
        public string CIIRSN { get; set; }
        public string CISIN { get; set; }
        public int CICDAT { get; set; }
        public string CIHSCL { get; set; }
        public string CIGRAC { get; set; }
        public string CIINAC { get; set; }
        public string CIITYP { get; set; }
        public string CITYPE { get; set; }
        public string CISUBTYPECODE { get; set; }
        public string STypeCaption { get; set; }
        public string CIINDV { get; set; }
        public string CIINDV_Original { get; set; }
        public string CIINDVUnicode { get; set; }
    }

    public class Record
    {
        public Properties properties { get; set; }
    }
    public class Fault
    {
        [JsonProperty("code")]
        public long Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
    public class IxReturnType
    {
        public List<Record> records { get; set; }
        [JsonProperty("fault")]
        public Fault Fault { get; set; }
        public int fromDate { get; set; }
        public int toDate { get; set; }
    }
}

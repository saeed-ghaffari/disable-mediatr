﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.Model.Inquiries
{
    public class RefreshTokenVM

    {
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public string scope { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }

    }
    public class TokenVM

    {
        public string grant_type { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}

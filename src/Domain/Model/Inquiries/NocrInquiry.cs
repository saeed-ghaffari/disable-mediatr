﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Cerberus.Domain.Model.Inquiries
{
    [Table("NocrInquiry")]
    public class NocrInquiry : BaseModel
    {
        //[ForeignKey(nameof(Profile))]
        //public long ProfileId { get; set; }

        //public Profile Profile { get; set; }

        //[ForeignKey(nameof(PrivatePerson))]
        //public long PrivatePersonId { get; set; }

        //public PrivatePerson PrivatePerson { get; set; }
        [JsonProperty(PropertyName = "FirstName")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "LastName")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "FatherName")]
        public string FatherName { get; set; }

        [JsonProperty(PropertyName = "SeriesNumber")]
        public string SeriesNumber { get; set; }

        [JsonProperty(PropertyName = "SerialNumber")]
        public string SerialNumber { get; set; }

        [JsonProperty(PropertyName = "IdentificationNumber")]
        public string IdentificationNumber { get; set; }

        [JsonProperty(PropertyName = "Gender")]
        public string Gender { get; set; }

        [JsonProperty(PropertyName = "Ssn")]
        public string Ssn { get; set; }

        [JsonProperty(PropertyName = "IsAlive")]
        public string IsAlive { get; set; }

        [JsonProperty(PropertyName = "BirthDate")]
        public string BirthDate { get; set; }

        [JsonProperty(PropertyName = "PersianBirthDate")]
        public string PersianBirthDate { get; set; }

        [JsonProperty(PropertyName = "Message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "Successful")]
        public bool Successful { get; set; }
        /// <summary>
        /// For avoid get more inquiry if it's true
        /// </summary>
        [JsonProperty(PropertyName = "SuccessInquiry")]
        public bool SuccessInquiry { get; set; }

        [JsonProperty(PropertyName = "HasException")]
        public bool HasException { get; set; }
    }
}

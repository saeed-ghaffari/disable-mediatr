﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using ProtoBuf;

namespace Cerberus.Domain.Model.Inquiries
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("ShahkarInquiry")]
    public class ShahkarInquiry : BaseModel
    {
        [JsonProperty(PropertyName = "Ssn")]
        public string Ssn { get; set; }

        [JsonProperty(PropertyName = "Mobile")]
        public long Mobile { get; set; }

        [JsonProperty(PropertyName = "Message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "Successful")]
        public bool Successful { get; set; }

        [JsonProperty(PropertyName = "SuccessInquiry")]
        public bool SuccessInquiry { get; set; }

        [JsonProperty(PropertyName = "HasException")]
        public bool HasException { get; set; }
    }
}

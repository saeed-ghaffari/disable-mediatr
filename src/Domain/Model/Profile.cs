﻿using System;
using Cerberus.Domain.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Utility.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("Profile")]
    public class Profile : BaseModel
    {
        public long Mobile { get; set; }

        public Carrier Carrier { get; set; }

        public string Email { get; set; }

        public string UniqueIdentifier { get; set; }

        public ProfileOwnerType Type { get; set; }

        public ProfileStatus Status { get; set; }

        public string TraceCode { get; set; }

        public StatusReasonType? StatusReasonType { get; set; }

        public PrivatePerson PrivatePerson { get; set; }

        public LegalPerson LegalPerson { get; set; }

        public IList<Payment> Payments { get; set; }

        public IList<Address> Addresses { get; set; }

        public IList<TradingCode> TradingCodes { get; set; }

        [InverseProperty(nameof(Model.Agent.ProfileOwner))]
        public Agent Agent { get; set; }

        [InverseProperty(nameof(Model.Agent.AgentProfile))]
        public IList<Agent> Agents { get; set; }

        public IList<BankingAccount> Accounts { get; set; }

        public JobInfo JobInfo { get; set; }

        public IList<ProfileHistory> ProfileHistory { get; set; }

        public FinancialInfo FinancialInfo { get; set; }

        [InverseProperty(nameof(LegalPersonStakeholder.ProfileOwner))]
        public IList<LegalPersonStakeholder> LegalPersonStakeholders { get; set; }

        [InverseProperty(nameof(LegalPersonShareholder.ProfileOwner))]
        public IList<LegalPersonShareholder> LegalPersonShareholders { get; set; }
        public IEnumerable<ProfilePackage> Packages { get; set; }
        public IEnumerable<Message> Messages { get; set; }
        public IEnumerable<IssuanceStockCode> IssuanceStockCodes { get; set; }
        public Msisdn Mobiles { get; set; }


    }
}
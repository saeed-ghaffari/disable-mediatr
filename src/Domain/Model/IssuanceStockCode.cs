﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.Model
{
    [Table("IssuanceStockCode")]
    public class IssuanceStockCode : BaseModel
    {
        [ForeignKey(nameof(Profile))]
        public long ProfileId { get; set; }

        public string UniqueIdentifier { get; set; }

        public long? Mobile { get; set; }

        public bool Successful { get; set; }

        public string Description { get; set; }

        public IssuanceStockCodeStatus Status { get; set; }
        public int TryCount { get; set; }

        public Profile Profile { get; set; }

        public IEnumerable<IssuanceStockCodeResponse> IssuanceStockCodeResponses { get; set; }
    }
}
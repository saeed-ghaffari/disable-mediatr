﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("File")]
    public class File : BaseModel
    {
        public string Title { get; set; }

        public string FileName { get; set; }

        public FileType Type { get; set; }

        public string MimeType { get; set; }

        public string LocalFileName { get; set; }

        public IEnumerable<AgentDocument> AgentDocuments { get; set; }

    }
}
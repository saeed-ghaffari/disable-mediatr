﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.Model
{
    [Table("ProfilePackage")]
    public class ProfilePackage : BaseModel
    {
        public long ProfileId { get; set; }

        public PackageType Type { get; set; }

        [NotMapped]
        public bool Used
        {
            get
            {
                switch (Type)
                {
                    case PackageType.Registration:
                        return UsedFlag.HasFlag(PackageUsedFlag.Registration);

                    case PackageType.EditMobileNumber:
                        return UsedFlag.HasFlag(PackageUsedFlag.EditMobileNumber);

                    case PackageType.EditPrivatePersonInfo:
                        return UsedFlag.HasFlag(PackageUsedFlag.EditPrivatePersonInfo) && UsedFlag.HasFlag(PackageUsedFlag.EditBankingAccount);

                    case PackageType.EditBankingAccount:
                        return UsedFlag.HasFlag(PackageUsedFlag.EditBankingAccount);

                    case PackageType.EditAllData:
                        return UsedFlag.HasFlag(PackageUsedFlag.EditPrivatePersonInfo) && UsedFlag.HasFlag(PackageUsedFlag.EditBankingAccount) && UsedFlag.HasFlag(PackageUsedFlag.EditMobileNumber);

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private PackageUsedFlag _usedFlag;

        public PackageUsedFlag UsedFlag
        {
            get => _usedFlag;

            set
            {
                switch (Type)
                {
                    case PackageType.Registration:
                        if (value != PackageUsedFlag.Registration)
                            throw new ArgumentOutOfRangeException(nameof(UsedFlag));

                        break;
                    case PackageType.EditMobileNumber:
                        if (value != PackageUsedFlag.EditMobileNumber)
                            throw new ArgumentOutOfRangeException(nameof(UsedFlag));
                        break;
                    case PackageType.EditPrivatePersonInfo:
                        if (!value.HasFlag(PackageUsedFlag.EditPrivatePersonInfo) && !value.HasFlag(PackageUsedFlag.EditBankingAccount))
                            throw new ArgumentOutOfRangeException(nameof(UsedFlag));
                        break;
                    case PackageType.EditBankingAccount:
                        if (value != PackageUsedFlag.EditBankingAccount)
                            throw new ArgumentOutOfRangeException(nameof(UsedFlag));
                        break;
                    case PackageType.EditAllData: break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                _usedFlag |= value;
            }
        }

        public bool HasPermission(PackageUsedFlag usedFlag)
        {
            return !UsedFlag.HasFlag(usedFlag);
        }

        public PrimitivePackageType PrimitivePackageType { get; set; }

        public DateTime ExpirationDate { get; set; }

        [ForeignKey(nameof(ProfileId))]
        public Profile Profile { get; set; }

        public IEnumerable<Payment> Payments { get; set; }

    }
}
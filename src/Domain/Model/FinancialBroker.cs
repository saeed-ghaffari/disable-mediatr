﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("FinancialBroker")]
    public class FinancialBroker : BaseModel
    {
        [ForeignKey(nameof(FinancialInfo))]
        public long FinancialInfoId { get; set; }

        [ForeignKey(nameof(Broker))]
        public long BrokerId { get; set; }

        public Broker Broker { get; set; }

        public FinancialInfo FinancialInfo { get; set; }

    }
}

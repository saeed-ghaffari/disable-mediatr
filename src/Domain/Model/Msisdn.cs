﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("Msisdn")]
    public class Msisdn : InquiryDataModel
    {
        [ForeignKey(nameof(ProfileOwner))]
        public long ProfileId { get; set; }
        public Profile ProfileOwner { get; set; }

        public long Mobile { get; set; }

        public int ChangeMobileCount { get; set; }
    }
}

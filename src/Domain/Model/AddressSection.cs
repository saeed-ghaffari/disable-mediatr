﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [Table("AddressSection")]
    public class AddressSection : BaseModel
    {
        public string Name { get; set; }

        [ForeignKey(nameof(City))]
        public long CityId { get; set; }

        public City City { get; set; }

        public IEnumerable<Address> Addresses { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("JobInfo")]
    public class JobInfo : BaseModel
    {
        [ForeignKey(nameof(ProfileOwner))]
        public long ProfileId { get; set; }
        [ForeignKey(nameof(Job))]
        public long JobId { get; set; }
        public string JobDescription { get; set; }
        public DateTime? EmploymentDate { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPostalCode { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyWebSite { get; set; }
        public string CompanyCityPrefix { get; set; }
        public string CompanyPhone { get; set; }
        public string Position { get; set; }
        public string CompanyFaxPrefix { get; set; }
        public string CompanyFax { get; set; }
        public Profile ProfileOwner { get; set; }
        public Job Job { get; set; }
    }
}

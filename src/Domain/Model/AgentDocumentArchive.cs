﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cerberus.Domain.Model
{
    [Table("AgentDocumentArchive")]
    public class AgentDocumentArchive:InquiryDataModel
    {
        public long AgentDocumentId { get; set; }

        public long AgentId { get; set; }

        public long FileId { get; set; }

        public DateTime AgentDocumentCreationDate { get; set; }

        public DateTime AgentDocumentModifiedDate { get; set; }

    }
}
﻿using System;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class InquiryDataModel : BaseModel
    {
        public bool Locked { get; set; }

        public bool? IsConfirmed { get; set; }
    }
}
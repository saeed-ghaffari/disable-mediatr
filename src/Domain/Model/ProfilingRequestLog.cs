﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.Model
{
    /// <summary>
    /// Model for log all profiling request
    /// *** it can be use for any request ***
    /// </summary>
    [Table("ProfilingRequestLog", Schema = "log")]
    public class ProfilingRequestLog
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        /// <summary>
        /// Consider which service request
        /// </summary>
        
        public long ServiceId { get; set; }

        /// <summary>
        /// Access token payload in json format
        /// </summary>
        [MaxLength(1024)]
        public string AccessTokenPayload { get; set; }

        /// <summary>
        /// Request route that include all data in route like uniqueIdentifier
        /// </summary>
        [MaxLength(1024)]
        public string Route { get; set; }

        [MaxLength(200)]
        public string Controller { get; set; }

        [MaxLength(200)]
        public string Action { get; set; }
        /// <summary>
        /// Request method like post or get
        /// </summary>
        [MaxLength(10)]
        public string Method { get; set; }

        /// <summary>
        /// Request body include all data that posted to server
        /// </summary>
        public string RequestBody { get; set; }


        /// <summary>
        /// Request QueryString  data that posted to server
        /// </summary>

        [MaxLength(512)]
        public string QueryString { get; set; }


        /// <summary>
        /// Request date time
        /// </summary>
        public DateTime RequestDateTime { get; set; }

        /// <summary>
        /// Response in json format
        /// </summary>
        public string Response { get; set; }

        /// <summary>
        /// Response date time
        /// </summary>
        public DateTime? ResponseDateTime { get; set; }

        /// <summary>
        /// A hash of response that used to confirm integrity and accuracy of response
        /// </summary>
        [MaxLength(1024)]
        public string ResponseHash { get; set; }

        /// <summary>
        /// Request ip
        /// </summary>
        [MaxLength(100)]
        public string SourceIp { get; set; }

        /// <summary>
        /// Consider during execute action a exception thrown or not
        /// </summary>
        public bool HasException { get; set; }

        /// <summary>
        /// Message of exception
        /// </summary>
        public string ExceptionMessage { get; set; }

        public ExceptionType? ExceptionType { get; set; }

        /// <summary>
        /// Creation date of record in database
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreationDate { get; set; }
    }
}
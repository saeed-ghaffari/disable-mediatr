﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("LegalPerson")]
    public class LegalPerson : InquiryDataModel
    {
        [ForeignKey(nameof(ProfileOwner))]
        public long ProfileId { get; set; }

        public string CompanyName { get; set; }

        public LegalPersonType Type { get; set; }

        [ForeignKey(nameof(CitizenshipCountry))]
        public long CitizenshipCountryId { get; set; }

        public Country CitizenshipCountry { get; set; }

        public string RegisterNumber { get; set; }

        public string RegisterPlace { get; set; }

        public DateTime RegisterDate { get; set; }

        public string EconomicCode { get; set; }

        public string EvidenceReleaseCompany { get; set; }

        public DateTime? EvidenceReleaseDate { get; set; }

        public DateTime? EvidenceExpirationDate { get; set; }

        //  public LegalPersonType Type { get; set; }

        public Profile ProfileOwner { get; set; }

    }
}
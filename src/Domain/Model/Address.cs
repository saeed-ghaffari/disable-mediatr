﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("Address")]
    public class Address : BaseModel
    {
        [ForeignKey(nameof(ProfileOwner))]
        public long ProfileId { get; set; }

        public string PostalCode { get; set; }

        [ForeignKey(nameof(Country))]
        public long? CountryId { get; set; }

        public Country Country { get; set; }

        [ForeignKey(nameof(Province))]
        public long? ProvinceId { get; set; }

        public Province Province { get; set; }

        [ForeignKey(nameof(City))]
        public long? CityId { get; set; }

        public City City { get; set; }

        [ForeignKey(nameof(Section))]
        public long? SectionId { get; set; }

        public AddressSection Section { get; set; }

        public string CityPrefix { get; set; }

        [Column("Address")]
        public string RemnantAddress { get; set; }

        public string Alley { get; set; }
        public string Plaque { get; set; }
        public string Tel { get; set; }

        public string CountryPrefix { get; set; }

        public string Mobile { get; set; }

        public string EmergencyTel { get; set; }

        public string EmergencyTelCityPrefix { get; set; }

        public string EmergencyTelCountryPrefix { get; set; }

        public string FaxPrefix { get; set; }

        public string Fax { get; set; }

        public string Website { get; set; }

        public string Email { get; set; }
        public bool? IsConfirmPostalCode { get; set; }
        public Profile ProfileOwner { get; set; }

    }
}
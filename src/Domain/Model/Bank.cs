﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [Table("Bank")]
    public class Bank : BaseModel
    {
        public string Name { get; set; }
    }
}
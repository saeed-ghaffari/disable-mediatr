﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("Payment")]
    public class Payment : BaseModel
    {
        [ForeignKey(nameof(ProfileOwner))]
        public long ProfileId { get; set; }

        public PaymentGateway Gateway { get; set; }

        public PaymentStatus Status { get; set; }

        public long Amount { get; set; }

        public string ReferenceNumber { get; set; }

        public string SaleReferenceId { get; set; }

        public long Discount { get; set; }

        public long? DiscountIssuer { get; set; }

        public long? ReferenceServiceId { get; set; }

        [ForeignKey(nameof(ProfileId))]
        public Profile ProfileOwner { get; set; }

        [ForeignKey(nameof(DiscountIssuer))]
        public ThirdPartyService IssuerService { get; set; }

        public long? FactorId { get; set; }

        public long? ProfilePackageId { get; set; }

        public bool? IsConfirmedReference  { get; set; }

        [ForeignKey(nameof(ProfilePackageId))]
        public ProfilePackage Package { get; set; }

        public string ClientReference { get; set; }
        public string ServiceId { get; set; }
        [MaxLength(100)]
        public string SerialNumber { get; set; }

    }
}
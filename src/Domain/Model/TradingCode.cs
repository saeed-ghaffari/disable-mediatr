﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("TradingCode")]
    public class TradingCode : InquiryDataModel
    {
        [ForeignKey(nameof(ProfileOwner))]
        public long ProfileId { get; set; }

        public Profile ProfileOwner { get; set; }

        public TradingType Type { get; set; }

        public string FirstPart { get; set; }

        public string SecondPart { get; set; }

        public string ThirdPart { get; set; }

        /// <summary>
        /// when insert a record by task(ix) this will be true
        /// </summary>
        //public bool IsOriginal { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("Agent")]
    public class Agent : InquiryDataModel
    {
        [ForeignKey(nameof(ProfileOwner))]
        public long ProfileId { get; set; }

        [ForeignKey(nameof(AgentProfile))]
        public long AgentProfileId { get; set; }

        public AgentType Type { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string Description { get; set; }

        public Profile ProfileOwner { get; set; }

        public Profile AgentProfile { get; set; }

        [NotMapped]
        public string UniqueIdentifier { get; set; }

        [NotMapped]
        public string FirstName { get; set; }

        [NotMapped]
        public string LastName { get; set; }

        public IEnumerable<AgentDocument> AgentDocuments { get; set; }
    }
}
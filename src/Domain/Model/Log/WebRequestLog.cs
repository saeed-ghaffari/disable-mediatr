﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.Model.Log
{
    [Table("WebRequestLog", Schema = "log")]
    public class WebRequestLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Ip { get; set; }
        public long ProfileId { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Method { get; set; }
        public ProfileStatus ProfileStatus { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreationDate { get; set; }

        public DateTime RequestDateTime { get; set; }
    }
}
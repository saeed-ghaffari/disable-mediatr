﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model.Membership
{
    [Table("AddressMembership", Schema = "mem")]
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class AddressMembership : BaseModel
    {
        [ForeignKey(nameof(ThirdPartyService))]
        public long ServiceId { get; set; }

        public ThirdPartyService ThirdPartyService { get; set; }

        public string UniqueIdentifier { get; set; }

        public string PostalCode { get; set; }

        [ForeignKey(nameof(Country))]
        public long? CountryId { get; set; }

        public Country Country { get; set; }

        [ForeignKey(nameof(Province))]
        public long? ProvinceId { get; set; }

        public Province Province { get; set; }

        [ForeignKey(nameof(City))]
        public long? CityId { get; set; }

        public City City { get; set; }

        [ForeignKey(nameof(Section))]
        public long? SectionId { get; set; }

        public AddressSection Section { get; set; }

        public string Address { get; set; }

        public string Tel { get; set; }

        public MembershipFlag Flag { get; set; }
    }
}
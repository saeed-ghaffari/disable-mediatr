﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model.Membership
{
    [Table("PrivatePersonMembership", Schema = "mem")]
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PrivatePersonMembership : BaseModel
    {
        public long Discount { get; set; }

        [ForeignKey(nameof(ThirdPartyService))]
        public long ServiceId { get; set; }

        public string UniqueIdentifier { get; set; }

        public string Mobile { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FatherName { get; set; }

        public Gender? Gender { get; set; }

        public string SeriShChar { get; set; }

        public string SeriSh { get; set; }

        public string Serial { get; set; }

        public string ShNumber { get; set; }

        public DateTime? BirthDate { get; set; }

        public string PlaceOfIssue { get; set; }

        public string PlaceOfBirth { get; set; }

        public bool CanEditAccounts { get; set; }

        public MembershipFlag Flag { get; set; }

        public ThirdPartyService ThirdPartyService { get; set; }

    }
}
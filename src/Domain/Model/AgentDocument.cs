﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("AgentDocument")]
    public class AgentDocument : InquiryDataModel
    {
        [ForeignKey(nameof(Agent))]
        public long AgentId { get; set; }

        [ForeignKey(nameof(File))]
        public long FileId { get; set; }

        public Agent Agent { get; set; }
        public File File { get; set; }
    }
}
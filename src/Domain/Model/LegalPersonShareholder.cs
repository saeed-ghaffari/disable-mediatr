﻿using System;
using Cerberus.Domain.Enum;
using System.ComponentModel.DataAnnotations.Schema;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("LegalPersonShareholder")]
    public class LegalPersonShareholder : BaseModel
    {
        [ForeignKey(nameof(ProfileOwner))]
        public long ProfileId { get; set; }

        public Profile ProfileOwner { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UniqueIdentifier { get; set; }

        public string PostalCode { get; set; }

        public string Address { get; set; }

        public StakHolderPositionType PositionType { get; set; }

        public int? PercentageVotingRight { get; set; }
    }
}
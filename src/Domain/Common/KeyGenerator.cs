﻿namespace Cerberus.Domain.Common
{
    public static class KeyGenerator
    {
        private static string _prefix = "master";
        public static class Profile
        {
            public static string GetMasterCacheKey(string uniqueIdentifier) => $"{_prefix}-p-u-{uniqueIdentifier}";
            public static string GetMasterCacheKeyById(long profileId) => $"{_prefix}-p-i-{profileId}";
        }
    }
}
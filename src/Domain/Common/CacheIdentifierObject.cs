﻿namespace Cerberus.Domain.Common
{
    public class CacheIdentifierObject
    {
        public string UniqueIdentifier { get; set; }
    }
}
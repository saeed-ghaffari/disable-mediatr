﻿using System;
using System.Collections.Generic;
using System.Text;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.Domain.Extensions
{
    public static class ProfileExtensions
    {
        public static bool IsSejami(this ProfileStatus profileStatus)
        {
            return
                    profileStatus == ProfileStatus.Sejami ||
                    profileStatus == ProfileStatus.SemiSejami;
        }


        public static bool IsConfirmed(this ProfileStatus profileStatus)
        {
            return profileStatus.IsSejami() ||
                   profileStatus == ProfileStatus.TraceCode ||
                   profileStatus == ProfileStatus.Suspend;
        }

        public static bool IsEditPrivatePersonSemiSejami(this Profile profile)
        {
            return profile.Status == ProfileStatus.Sejami || (profile.Status == ProfileStatus.SemiSejami && profile.StatusReasonType == StatusReasonType.EditPrivatePerson);
        }

        public static bool IsEditBankingAccountSemiSejami(this Profile profile)
        {
            return profile.Status == ProfileStatus.Sejami || (profile.Status== ProfileStatus.SemiSejami && profile.StatusReasonType == StatusReasonType.EditBankingAccount);
        }

        public static bool IsDeleteAndUpdateAgentSuspend(this Profile profile)
        {
            return profile.Status == ProfileStatus.Sejami || (profile.Status == ProfileStatus.Suspend && (profile.StatusReasonType == StatusReasonType.DeleteAgent || profile.StatusReasonType== StatusReasonType.EditAgent));
        }
        public static bool IsEditPrivatePersonAndDeleteAgentEighteen(this Profile profile)
        {
            return (profile.Status == ProfileStatus.Suspend && profile.StatusReasonType == StatusReasonType.DeleteAgentEighteenPrivatePersonSemiSejami);
        }
        public static bool IsEditBankingAccountAndDeleteAgentEighteen(this Profile profile)
        {
            return (profile.Status == ProfileStatus.Suspend && profile.StatusReasonType == StatusReasonType.DeleteAgentEighteenBankingAccountSemiSejami);
        }
    }
}

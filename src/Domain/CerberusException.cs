﻿using System;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain
{
    public class CerberusException : Exception
    {
        public CerberusException()
        {
        }
        public CerberusException(
                ErrorCode errorCode,
                string customMessage = null,
                object value = null, 
                Exception innerException = null)
            : base(customMessage, innerException)
        {
            ErrorCode = errorCode;
            CustomMessage = customMessage;
            Value = value;
        }

        public ErrorCode ErrorCode { get; set; }

        public string CustomMessage { get; set; }

        public object Value { get; set; }

        public int HttpStatusCode => int.Parse(ErrorCode.ToString("D").Substring(0, 3));
    }
}
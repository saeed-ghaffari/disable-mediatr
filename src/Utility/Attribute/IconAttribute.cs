﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Utility.Attribute
{
    public class IconAttribute : System.Attribute
    {
        public string Name { get; }
        public IconAttribute(string name) => Name = name;
    }
}

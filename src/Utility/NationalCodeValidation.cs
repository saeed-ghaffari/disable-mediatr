﻿using System.ComponentModel.DataAnnotations;

namespace Cerberus.Utility
{
    public class UniqueIdentifierValidation : ValidationAttribute
    {

        public override bool IsValid(object value)
        {
            var inputValue = value.ToString();
            return inputValue.IsValidUniqueIdentifier();
        }

    }
}

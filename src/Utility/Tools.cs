﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using Cerberus.Utility.Attribute;
using Microsoft.AspNetCore.Http;

namespace Cerberus.Utility
{
    public static class Tools
    {
        private static readonly Random Random = new Random();

        public static int GenerateRandomNumber(int min, int max)
        {
            return Random.Next(min, max);
        }

        public static string GetMimeType(this string path)
        {
            var mimeTypes = new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
            var extension = Path.GetExtension(path)?.ToLowerInvariant();
            return mimeTypes[extension ?? string.Empty];

        }

        public static string GetFileExtension(this string base64String)
        {
            var data = base64String.Substring(0, 5);

            switch (data.ToUpper())
            {
                case "IVBOR":
                    return "image/png";
                case "/9J/4":
                    return "image/jpg";
                case "/9J//":
                    return "image/jpg";
                case "AAAAF":
                    return "mp4";
                case "JVBER":
                    return "application/pdf";
                case "AAABA":
                    return "ico";
                case "UMFYI":
                    return "rar";
                case "E1XYD":
                    return "rtf";
                case "U1PKC":
                    return "txt";
                case "MQOWM":
                case "77U/M":
                    return "srt";
                default:
                    return "invalidFile";
            }
        }
        public static SelectList ToSelectList<T>()
        {
            return new SelectList(System.Enum.GetValues(typeof(T)).Cast<System.Enum>().Select(
                x => new SelectListItem
                {
                    Text = x.GetEnumDescription(),
                    Value = (Convert.ToInt32(x)).ToString()
                }), "Value", "Text");
        }

        public static SelectList ToSelectList<TEnum>(this TEnum obj) where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            return new SelectList(System.Enum.GetValues(typeof(TEnum)).OfType<System.Enum>()
            .Select(x => new SelectListItem
            {
                Text = x.GetEnumDescription(),
                Value = (Convert.ToInt32(x)).ToString()
            }), "Value", "Text");
        }

        public static string GetEnumDescription<TEnum>(this TEnum value)
        {
            var attributes = (DescriptionAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

        public static string GetEnumCategory<TEnum>(this TEnum value)
        {
            var attributes = (CategoryAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(CategoryAttribute), false);
            return attributes.Length > 0 ? attributes[0].Category : value.ToString();
        }
        public static string GetEnumIcon<TEnum>(this TEnum value)
        {
            var attributes = (IconAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(CategoryAttribute), false);
            return attributes.Length > 0 ? attributes[0].Name : value.ToString();
        }
        public static string GenerateRandomNumber(int length)
        {
            string result = "";
            Random r = new Random();
            for (int i = 0; i < length; i++)
            {
                result += r.Next(0, 9) + "";
            }
            return result;
        }

        public static bool CheckUniqueIdentifier(string uniqueIdentifier, int profileOwnerType)
        {
            if (profileOwnerType == 1)
            {
                if (!System.Text.RegularExpressions.Regex.IsMatch(uniqueIdentifier, @"^(?!(\d)\1{9})\d{10}$"))
                    return false;
                var check = Convert.ToInt32(uniqueIdentifier.Substring(9, 1));
                var sum = Enumerable.Range(0, 9)
                              .Select(x => Convert.ToInt32(uniqueIdentifier.Substring(x, 1)) * (10 - x))
                              .Sum() % 11;

                return sum < 2 && check == sum || sum >= 2 && check + sum == 11;
            }

            if (profileOwnerType == 2)
            {

                if (string.IsNullOrWhiteSpace(uniqueIdentifier) || !Regex.IsMatch(uniqueIdentifier, "^\\d{11}$"))
                    return false;

                //one is the last digit char which is the signature of the identifier
                if (!int.TryParse(uniqueIdentifier.Substring(10, 1), out var one))
                    return false;


                if (!int.TryParse(uniqueIdentifier.Substring(9, 1), out var d))
                    return false;

                d += 2;

                var z = new int[] { 29, 27, 23, 19, 17 };

                var s = 0;

                for (var i = 0; i < 10; i++)
                    s += (d + int.Parse(uniqueIdentifier.Substring(i, 1)) * z[i % 5]);

                s = s % 11;

                if (s == 10)
                    s = 0;
                return one == s;
            }
            return true;
        }

        public static string GetDigitsFromString(string text)
        {
            return new String(text.Where(Char.IsDigit).ToArray());
        }

        public static bool IsTraceCodeValid(string traceCode)
        {
            return Regex.IsMatch(traceCode, @"\d{10}");
        }

        public static bool IsValidImageFormat(string extension)
        {
            switch (extension)
            {
                case "image/png":
                case "image/jpg":
                case "application/pdf":
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsUnder18Years(DateTime? birthday) => birthday != null && DateTime.Now.Date < birthday.Value.AddYears(18);

        public static string NormalizeMobile(this string mobile)
        {
            return $"0{mobile.Substring(2, 10)}";
        }
        public static string NormalizeMobile(this long mobile)
        {
            return $"0{mobile.ToString().Substring(2, 10)}";
        }

        public static string NormalMobile(this long mobile)
        {
            return $"{mobile.ToString().Substring(2, 10)}";
        }

        public static string ConcatAddress(string province, string city, string street, string alley, string plaque)
        {
            return
                $"استان {province} شهر {city} خیابان {street.Replace("خیابان", "").Replace("خیابون", "")} کوچه {alley.Replace("کوچه", "")} پلاک {plaque.Replace("پلاک", "")}";
        }

        public static string ConcatDate(this string date)
        {
            var year = date.Substring(0, 4);
            var month = date.Substring(4, 2);
            var day = date.Substring(6, 2);
            return $"{year}/{month}/{day}";

        }
        public static string ConcatSecondDate(this string date)
        {
            var year = date.Substring(8, 4);
            var month = date.Substring(12, 2);
            var day = date.Substring(14, 2);
            return $"{year}/{month}/{day}";

        }
        public static bool IsAllDigits(this string s)
        {
            return long.TryParse(s, out _);

        }



        public static string GetUserIp(this HttpRequest request)
        {
            //todo : fixed get user ip order by aspina
            if (request.Headers["X-Forwarded-For"].FirstOrDefault() != null)
                return request.Headers["X-Forwarded-For"].ToString();

            return request.HttpContext.Connection.RemoteIpAddress.ToString();
        }

    }
}
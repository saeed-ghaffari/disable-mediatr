﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Cerberus.Utility.Enum;

namespace Cerberus.Utility
{
    public static class ValidationHelper
    {
        public static bool IsMsisdnValid(ref string phone, out Carrier? carrier)
        {
            carrier = null;

            if (string.IsNullOrWhiteSpace(phone))
                return false;

            if (Regex.IsMatch(phone, @"^09\d{9}$"))
                phone = $"98{phone.Substring(1, 10)}";
            else if (Regex.IsMatch(phone, @"^9\d{9}$"))
                phone = $"98{phone}";
            else if (!Regex.IsMatch(phone, @"989\d{9}") && phone != "01234567890")
                return false;

            if (phone.Length != 12)
                return false;

            return ValidatePhonePrefix(phone.Substring(0, 5), out carrier, phone);

        }

        private static bool ValidatePhonePrefix(string prefix, out Carrier? carrier, string phoneNumber)
        {
            switch (prefix)
            {
                case "98910":
                case "98911":
                case "98912":
                case "98913":
                case "98914":
                case "98915":
                case "98916":
                case "98917":
                case "98918":
                case "98919":
                case "98990":
                case "98991":
                case "98992":
                case "98993":
                case "98994":
                    carrier = Carrier.Mci;
                    return true;
                case "98930":
                case "98933":
                case "98935":
                case "98936":
                case "98937":
                case "98938":
                case "98939":
                case "98901":
                case "98902":
                case "98903":
                case "98904":
                case "98905":
                    carrier = Carrier.Mtn;
                    return true;
                case "98920":
                case "98921":
                case "98922":
                    carrier = Carrier.Rightel;
                    return true;
                case "98931":
                case "98932":
                    var slice = phoneNumber.Substring(0, 6);
                    if (slice == "989324")
                    {
                        carrier = Carrier.Spadan;
                        return true;
                    }
                    else if (slice == "989329")
                    {
                        carrier = Carrier.Talia;
                        return true;
                    }

                    carrier = null;
                    return false;

                case "98934":
                    carrier = Carrier.Tkc;
                    return true;

                case "98998":
                    carrier = Carrier.Shatel;
                    return true;

                case "98999":
                    carrier = Carrier.MVNO;
                    return true;

                default:
                    carrier = null;
                    return false;
            }
        }

        public static string UnifyText(string input)
        {
            if (string.IsNullOrEmpty(input))
                return null;

            char[][] numbers = new char[][]
            {
                "12345678900123456789یکهه".ToCharArray(),"١٢٣٤٥٦٧٨٩٠۰۱۲۳۴۵۶۷۸۹يكةۀ".ToCharArray()
            };
            for (int i = 0; i < numbers[0].Length; i++)
            {
                input = input.Replace(numbers[1][i], numbers[0][i]);
            }

            input = input.Replace("ڇ", "چ").Replace(((char)1609).ToString(), "ی");
            return input.Normalize(NormalizationForm.FormKC);
        }

        public static string Unify(this string input)
        {
            return UnifyText(input);
        }


        public static bool IsPrivatePersonUniqueIdentifierValid(this string inputValue)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(inputValue, @"^(?!(\d)\1{9})\d{10}$"))
                return false;

            var check = Convert.ToInt32(inputValue.Substring(9, 1));
            var sum = Enumerable.Range(0, 9)
                          .Select(x => Convert.ToInt32(inputValue.Substring(x, 1)) * (10 - x))
                          .Sum() % 11;

            return sum < 2 && check == sum || sum >= 2 && check + sum == 11;
        }

        public static bool IsLegalPersonUniqueIdentifierValid(this string nationalCode)
        {
            if (string.IsNullOrWhiteSpace(nationalCode) || !Regex.IsMatch(nationalCode, "^\\d{11}$"))
                return false;

            //one is the last digit char which is the signature of the identifier
            if (!int.TryParse(nationalCode.Substring(10, 1), out var one))
                return false;


            if (!int.TryParse(nationalCode.Substring(9, 1), out var d))
                return false;

            d += 2;

            var z = new int[] { 29, 27, 23, 19, 17 };

            var s = 0;

            for (var i = 0; i < 10; i++)
                s += (d + int.Parse(nationalCode.Substring(i, 1)) * z[i % 5]);

            s = s % 11;

            if (s == 10)
                s = 0;
            return one == s;
        }


        public static bool IsValidUniqueIdentifier(this string uniqueIdentifier)
        {
            if (string.IsNullOrWhiteSpace(uniqueIdentifier)||string.IsNullOrEmpty(uniqueIdentifier))
                return true;

            if (uniqueIdentifier.Length > 11)
                return false;
            if (uniqueIdentifier.Length == 11)
            {
                if (string.IsNullOrWhiteSpace(uniqueIdentifier) || !Regex.IsMatch(uniqueIdentifier, "^\\d{11}$"))
                    return false;

                //one is the last digit char which is the signature of the identifier
                if (!int.TryParse(uniqueIdentifier.Substring(10, 1), out var one))
                    return false;


                if (!int.TryParse(uniqueIdentifier.Substring(9, 1), out var d))
                    return false;

                d += 2;

                var z = new int[] { 29, 27, 23, 19, 17 };

                var s = 0;

                for (var i = 0; i < 10; i++)
                    s += (d + int.Parse(uniqueIdentifier.Substring(i, 1)) * z[i % 5]);

                s %= 11;

                if (s == 10)
                    s = 0;
                return one == s;
            }
            if (uniqueIdentifier.Length == 10)
            {
                try
                {
                  
                    if (!Regex.IsMatch(uniqueIdentifier, @"^(?!(\d)\1{9})\d{10}$"))
                        return false;

                    //if it's not able convert to int
                    if (!int.TryParse(uniqueIdentifier.Substring(9, 1), out var number)) return false;


                    var check = Convert.ToInt32(uniqueIdentifier.Substring(9, 1));
                    var sum = Enumerable.Range(0, 9)
                                  .Select(x => Convert.ToInt32(uniqueIdentifier.Substring(x, 1)) * (10 - x))
                                  .Sum() % 11;

                    return sum < 2 && check == sum || sum >= 2 && check + sum == 11;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }
    }
}
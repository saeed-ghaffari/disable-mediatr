﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Cerberus.Utility
{
    public static class HashHelper
    {
        public static string GenerateMd5String(string value)
        {
            var sb = new StringBuilder();

            foreach (Byte b in GenerateMd5(value))
                sb.Append(b.ToString("x2"));

            return sb.ToString();
        }

        public static byte[] GenerateMd5(string value)
        {
            using (var hash = MD5.Create())
            {
                var enc = Encoding.UTF8;
                return hash.ComputeHash(enc.GetBytes(value));
            }
        }
    }
}
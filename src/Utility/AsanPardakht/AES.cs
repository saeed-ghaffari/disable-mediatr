﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Cerberus.Utility
{
    public class AES
    {

        string AES_Key = string.Empty;
        string AES_IV = string.Empty;
        public AES(string AES_Key, string AES_IV)
        {
            this.AES_Key = AES_Key;
            this.AES_IV = AES_IV;
        }
        public string Encrypt(string input)
        {
            string encryptedString = "";

            var aes = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 256,
                Padding = PaddingMode.PKCS7,
                Key = Convert.FromBase64String(this.AES_Key),
                IV = Convert.FromBase64String(this.AES_IV)
            };
          
            var encrypt = aes.CreateEncryptor(aes.Key, aes.IV); byte[] xBuff = null; using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write))
                {
                    byte[] xXml = Encoding.UTF8.GetBytes(input);
                    cs.Write(xXml, 0, xXml.Length);
                }
                xBuff = ms.ToArray();
            }

            encryptedString = Convert.ToBase64String(xBuff);
            return encryptedString;
        }
        public bool Decrypt(String Input, out string decodedString)
        {
            try
            {
                RijndaelManaged aes = new RijndaelManaged(); aes.KeySize = 256; aes.BlockSize = 256; aes.Mode = CipherMode.CBC; aes.Padding = PaddingMode.PKCS7; aes.Key = Convert.FromBase64String(this.AES_Key); aes.IV = Convert.FromBase64String(this.AES_IV);
                var decrypt = aes.CreateDecryptor(); byte[] xBuff = null; using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write)) { byte[] xXml = Convert.FromBase64String(Input); cs.Write(xXml, 0, xXml.Length); }
                    xBuff = ms.ToArray();
                }
                decodedString = Encoding.UTF8.GetString(xBuff); return true;

            }
            catch { decodedString = string.Empty; return false; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Cerberus.Utility
{
    public static class PersianChars
    {
        /// <summary>
        /// حذف اعراب از کلمات
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
        public static string ConvertArabicToPersianChar(string text)
        {
            return text.Replace("ي", "ی").Replace("ك", "ک").Replace(((char)1609).ToString(), "ی").Replace(((char)1749).ToString(), ((char)1607).ToString()/*یکسان سازی حرف ه */);
        }
        /// <summary>
        /// حذف کاراکتر های مثل اسپیس و نیم اسپیس و تب
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string RemoveSpecialChar(string text)
        {
            text = text.Replace(" ", "").Replace("\u2009", "").Replace("\u200A", "").Replace("\u0009", "").Replace("ـ", "").Replace("ڇ", "چ")
                .Replace(((char)8204).ToString(), "")/*نیم اسپیس*/
                .Replace(((char)8205).ToString(), "")/*نیم اسپیس*/
                .Replace("ة", "ه").Replace(".", "").Replace(((char)160).ToString(), "")/*نیم اسپیس*/;

            return text;
        }
        public static string ReplaceSpecialWord(string text)
        {
            return text.Replace("كوپاءي", "کوپایی").Replace("الله", "اله").Replace("شرکت", "").Replace("موسسه", "").Replace("شركت", "").Replace("سهامی خاص", "");
        }

        public static string GetClearChars(string text)
        {
            return RemoveSpecialChar(ReplaceSpecialWord(RemoveSpecialChar(ReplaceSpecialWord(ConvertArabicToPersianChar(RemoveDiacritics(ConvertArabicToPersianChar(ReplaceSpecialWord(text))))))));
        }
    }
}

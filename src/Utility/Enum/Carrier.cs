﻿namespace Cerberus.Utility.Enum
{
    public enum Carrier : byte
    {
        Mci = 1,
        Mtn = 2,
        Rightel = 3,
        Spadan = 4,
        Talia = 5,
        Tkc = 6,
        Shatel = 7,
        MVNO=8
    }
}
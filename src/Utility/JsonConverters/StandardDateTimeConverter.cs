﻿using Newtonsoft.Json.Converters;

namespace Cerberus.Utility.JsonConverters
{
    public class StandardDateTimeConverter : IsoDateTimeConverter
    {
        public StandardDateTimeConverter(string format = null)
        {
            if (string.IsNullOrEmpty(format))
                base.DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss";
            else
                DateTimeFormat = format;
        }
    }
}
﻿#region Using

using System;
using System.Globalization;
using System.Reflection;

#endregion

namespace Cerberus.Utility
{
    public static class PersianDateExtension
    {
        #region  Fields

        private static CultureInfo _culture;

        #endregion

        #region Methods

        #region Get Persian Culture

        public static CultureInfo GetPersianCulture()
        {
            if (_culture != null) return _culture;
            _culture = new CultureInfo("fa-IR");
            var formatInfo = _culture.DateTimeFormat;
            var monthNames = new[] { "فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند", "" };

            formatInfo.AbbreviatedDayNames = new[] { "ی", "د", "س", "چ", "پ", "ج", "ش" };
            formatInfo.DayNames = new[] { "یکشنبه", "دوشنبه", "سه شنبه", "چهار شنبه", "پنجشنبه", "جمعه", "شنبه" };
            formatInfo.AbbreviatedMonthNames = formatInfo.MonthNames = formatInfo.MonthGenitiveNames = formatInfo.AbbreviatedMonthGenitiveNames = monthNames;
            formatInfo.AMDesignator = "ق.ظ";
            formatInfo.PMDesignator = "ب.ظ";
            formatInfo.ShortDatePattern = "yyyy/MM/dd";
            formatInfo.LongDatePattern = "dddd, dd MMMM,yyyy";
            formatInfo.FirstDayOfWeek = DayOfWeek.Saturday;
            var persianCalendar = new PersianCalendar();

            var fieldInfo = _culture.GetType().GetField("calendar", BindingFlags.NonPublic | BindingFlags.Instance);
            if (fieldInfo != null)
                fieldInfo.SetValue(_culture, persianCalendar);

            var info = formatInfo.GetType().GetField("calendar", BindingFlags.NonPublic | BindingFlags.Instance);
            if (info != null)
                info.SetValue(formatInfo, persianCalendar);

            _culture.NumberFormat.NumberDecimalSeparator = "/";
            _culture.NumberFormat.DigitSubstitution = DigitShapes.NativeNational;
            _culture.NumberFormat.NumberNegativePattern = 0;
            return _culture;
        }

        #endregion

        #region To Persian Date

        public static string ToPersianDate(this DateTime date, string format = "yyyy/MM/dd") => date.ToString(format, GetPersianCulture());
        public static string ToPersianDate(this DateTime? date, string format = "yyyy/MM/dd") => date?.ToString(format, GetPersianCulture());
        public static DateTime ToGeorgianDate(this string persianTime){
            PersianCalendar pc = new PersianCalendar();
            int y = Convert.ToInt32(persianTime.Substring(0, 4));
            int m = Convert.ToInt32(persianTime.Substring(5, 2));
            int d = Convert.ToInt32(persianTime.Substring(8, 2));
            DateTime t = pc.ToDateTime(y, m, d, 0, 0, 0, 0);
            return t;

        }
        #endregion

        #region Get Persian Year

        public static int GetPersianYear(this DateTime date) => new PersianCalendar().GetYear(date);

        #endregion

        #region Get Month Name

        public static string GetMonthName(this int month)
        {
            var dateTime = new DateTime(DateTime.Now.Year, month, DateTime.Now.Day);
            var persianCalendar = new PersianCalendar();

            var pdate = $"{persianCalendar.GetYear(dateTime):0000}/{persianCalendar.GetMonth(dateTime):00}/{persianCalendar.GetDayOfMonth(dateTime):00}";

            var dates = pdate.Split('/');

            switch (Convert.ToInt32(dates[1]))
            {
                case 1: return "فررودين";
                case 2: return "ارديبهشت";
                case 3: return "خرداد";
                case 4: return "تير‏";
                case 5: return "مرداد";
                case 6: return "شهريور";
                case 7: return "مهر";
                case 8: return "آبان";
                case 9: return "آذر";
                case 10: return "دي";
                case 11: return "بهمن";
                case 12: return "اسفند";
                default: return "";
            }
        }

        #endregion

        #endregion
    }
}
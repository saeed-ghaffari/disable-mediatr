﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Interface.Services.Membership;
using Microsoft.Extensions.DependencyInjection;

namespace Cerberus.Cli
{
    public static class TaskPool
    {
        public static async Task UpdateProfilesAsync(ServiceProvider service, int pageSize, int profileCount, string uniqueIdentifier = null)
        {
            int fromRow = 1;
            int toRow = pageSize;
            Console.WriteLine("Start Update profile " + profileCount);

            while (true)
            {
                using (var scope = service.CreateScope())
                {
                    var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                    var list = await profileService.GetProfileListAsync(fromRow, toRow, uniqueIdentifier);


                    DrawTextProgressBar(toRow, profileCount);

                    var enumerable = list.ToList();

                    if (!enumerable.Any())
                        break;

                    var tasks = new List<Task>();

                    foreach (var profile in enumerable)
                    {
                        tasks.Add(Task.Run(async () =>
                        {
                            using (var localScope = service.CreateScope())
                            {
                                await localScope.ServiceProvider.GetRequiredService<IProfileApiService>()
                                    .StoreProfileAsync(profile);
                            }
                        }));
                    }

                    Task.WaitAll(tasks.ToArray());

                    if(pageSize==1)
                         break;

                    if (enumerable.Count() == pageSize)
                    {
                        fromRow += pageSize;
                        toRow += pageSize;
                    }
                    else
                        break;

                }


            }
        }
        public static async Task UpdatePrivatePersonsAsync(ServiceProvider service, int pageSize, int profileCount, string uniqueIdentifier = null)
        {

            int fromRow = 1;
            int toRow = pageSize;

            Console.WriteLine("Start Update PrivatePerson: " + profileCount);

            while (true)
            {
                using (var scope = service.CreateScope())
                {

                    var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                    var list = await profileService.GetProfileListAsync(fromRow, toRow, uniqueIdentifier);

                    DrawTextProgressBar(toRow, profileCount);

                    var enumerable = list.ToList();

                    if (!enumerable.Any())
                        break;

                    var tasks = new List<Task>();

                    foreach (var profile in enumerable)
                    {
                        tasks.Add(Task.Run(async () =>
                        {
                            using (var localScope = service.CreateScope())
                            {
                                await localScope.ServiceProvider.GetRequiredService<IPrivatePersonApiService>()
                                    .StoreAsync(profile.UniqueIdentifier, profile.Type);
                            }
                        }));
                    }

                    Task.WaitAll(tasks.ToArray());

                    if (pageSize == 1)
                        break;

                    if (enumerable.Count() == pageSize)
                    {
                        fromRow += pageSize;
                        toRow += pageSize;
                    }
                    else
                        break;

                }


            }
        }
        public static async Task UpdateBankingAccountsAsync(ServiceProvider service, int pageSize, int profileCount, string uniqueIdentifier = null)
        {

            int fromRow = 1;
            int toRow = pageSize;
            Console.WriteLine("Start Update bankingAccount: " + profileCount);

            while (true)
            {
                using (var scope = service.CreateScope())
                {

                    var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                    var list = await profileService.GetProfileListAsync(fromRow, toRow, uniqueIdentifier);

                    DrawTextProgressBar(toRow, profileCount);

                    var enumerable = list.ToList();

                    if (!enumerable.Any())
                        break;

                    var tasks = new List<Task>();

                    foreach (var profile in enumerable)
                    {
                        tasks.Add(Task.Run(async () =>
                        {
                            using (var localScope = service.CreateScope())
                            {
                                await localScope.ServiceProvider.GetRequiredService<IBankingAccountApiService>()
                                    .StoreAsync(profile.UniqueIdentifier, profile.Type);
                            }
                        }));
                    }

                    Task.WaitAll(tasks.ToArray());

                    if (pageSize == 1)
                        break;

                    if (enumerable.Count() == pageSize)
                    {
                        fromRow += pageSize;
                        toRow += pageSize;
                    }
                    else
                        break;

                }


            }
        }
        public static async Task UpdateFinancialInfosAsync(ServiceProvider service, int pageSize, int profileCount, string uniqueIdentifier = null)
        {
            int fromRow = 1;
            int toRow = pageSize;
            Console.WriteLine("Start Update FinancialInfo: " + profileCount);

            while (true)
            {
                using (var scope = service.CreateScope())
                {

                    var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                    var list = await profileService.GetProfileListAsync(fromRow, toRow, uniqueIdentifier);

                    DrawTextProgressBar(toRow, profileCount);

                    var enumerable = list.ToList();

                    if (!enumerable.Any())
                        break;

                    var tasks = new List<Task>();

                    foreach (var profile in enumerable)
                    {
                        tasks.Add(Task.Run(async () =>
                        {
                            using (var localScope = service.CreateScope())
                            {
                                await localScope.ServiceProvider.GetRequiredService<IFinancialInfoApiService>()
                                    .StoreAsync(profile.UniqueIdentifier, profile.Type);
                            }
                        }));
                    }

                    Task.WaitAll(tasks.ToArray());

                    if (pageSize == 1)
                        break;

                    if (enumerable.Count() == pageSize)
                    {
                        fromRow += pageSize;
                        toRow += pageSize;
                    }
                    else
                        break;

                }




            }
        }
        public static async Task UpdateJobInfosAsync(ServiceProvider service, int pageSize, int profileCount, string uniqueIdentifier = null)
        {

            int fromRow = 1;
            int toRow = pageSize;

            Console.WriteLine("Start Update JobInfo: " + profileCount);

            while (true)
            {
                using (var scope = service.CreateScope())
                {

                    var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                    var list = await profileService.GetProfileListAsync(fromRow, toRow, uniqueIdentifier);

                    DrawTextProgressBar(toRow, profileCount);

                    var enumerable = list.ToList();

                    if (!enumerable.Any())
                        break;

                    var tasks = new List<Task>();

                    foreach (var profile in enumerable)
                    {
                        tasks.Add(Task.Run(async () =>
                        {
                            using (var localScope = service.CreateScope())
                            {
                                await localScope.ServiceProvider.GetRequiredService<IJobInfoApiService>()
                                    .StoreAsync(profile.UniqueIdentifier, profile.Type);
                            }
                        }));
                    }

                    Task.WaitAll(tasks.ToArray());

                    if (pageSize == 1)
                        break;

                    if (enumerable.Count() == pageSize)
                    {
                        fromRow += pageSize;
                        toRow += pageSize;
                    }
                    else
                        break;

                }

            }
        }
        public static async Task UpdateAddressesAsync(ServiceProvider service, int pageSize, int profileCount, string uniqueIdentifier = null)
        {

            int fromRow = 1;
            int toRow = pageSize;
            Console.WriteLine("Start Update Address: " + profileCount);

            while (true)
            {
                using (var scope = service.CreateScope())
                {

                    var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                    var list = await profileService.GetProfileListAsync(fromRow, toRow, uniqueIdentifier);

                    DrawTextProgressBar(toRow, profileCount);

                    var enumerable = list.ToList();

                    if (!enumerable.Any())
                        break;

                    var tasks = new List<Task>();

                    foreach (var profile in enumerable)
                    {
                        tasks.Add(Task.Run(async () =>
                        {
                            using (var localScope = service.CreateScope())
                            {
                                await localScope.ServiceProvider.GetRequiredService<IAddressApiService>()
                                    .StoreAsync(profile.UniqueIdentifier, profile.Type);
                            }
                        }));
                    }

                    Task.WaitAll(tasks.ToArray());

                    if (pageSize == 1)
                        break;

                    if (enumerable.Count() == pageSize)
                    {
                        fromRow += pageSize;
                        toRow += pageSize;
                    }
                    else
                        break;

                }

            }
        }
        public static async Task UpdateTradingCodesAsync(ServiceProvider service, int pageSize, int profileCount, string uniqueIdentifier = null)
        {

            int fromRow = 1;
            int toRow = pageSize;

            Console.WriteLine("Start Update TradingCode: " + profileCount);

            while (true)
            {
                using (var scope = service.CreateScope())
                {

                    var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                    var list = await profileService.GetProfileListAsync(fromRow, toRow, uniqueIdentifier);

                    DrawTextProgressBar(toRow, profileCount);

                    var enumerable = list.ToList();

                    if (!enumerable.Any())
                        break;

                    var tasks = new List<Task>();

                    foreach (var profile in enumerable)
                    {
                        tasks.Add(Task.Run(async () =>
                        {
                            using (var localScope = service.CreateScope())
                            {
                                await localScope.ServiceProvider.GetRequiredService<ITradingCodeApiService>()
                                    .StoreAsync(profile.UniqueIdentifier, profile.Type);
                            }
                        }));
                    }

                    Task.WaitAll(tasks.ToArray());

                    if (pageSize == 1)
                        break;

                    if (enumerable.Count() == pageSize)
                    {
                        fromRow += pageSize;
                        toRow += pageSize;
                    }
                    else
                        break;

                }

            }
        }
        public static async Task UpdateLegalPersonsAsync(ServiceProvider service, int pageSize, int profileCount, string uniqueIdentifier = null)
        {

            int fromRow = 1;
            int toRow = pageSize;
            var task = new List<Task>();

            Console.WriteLine("Start Update Legalperson: " + profileCount);

            while (true)
            {
                using (var scope = service.CreateScope())
                {

                    var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                    var list = await profileService.GetProfileListAsync(fromRow, toRow, uniqueIdentifier, ProfileOwnerType.IranianLegalPerson);

                    DrawTextProgressBar(toRow, profileCount);

                    var enumerable = list.ToList();

                    if (!enumerable.Any())
                        break;

                    var tasks = new List<Task>();

                    foreach (var profile in enumerable)
                    {
                        tasks.Add(Task.Run(async () =>
                        {
                            using (var localScope = service.CreateScope())
                            {
                                await localScope.ServiceProvider.GetRequiredService<ILegalPersonApiService>()
                                    .StoreAsync(profile.UniqueIdentifier, profile.Type);
                            }
                        }));
                    }

                    Task.WaitAll(tasks.ToArray());

                    if (pageSize == 1)
                        break;

                    if (enumerable.Count() == pageSize)
                    {
                        fromRow += pageSize;
                        toRow += pageSize;
                    }
                    else
                        break;

                }

            }
        }

        public static async Task UpdatePrivatePersonsMembershipAsync(ServiceProvider service, int pageSize, int profileCount, string uniqueIdentifier = null)
        {

            int fromRow = 1;
            int toRow = pageSize;

            Console.WriteLine("Start Update PrivatePersonMembership: " + profileCount);

            while (true)
            {
                using (var scope = service.CreateScope())
                {

                    var privatePerson = scope.ServiceProvider.GetRequiredService<IMembershipService>();
                    var list = await privatePerson.GetPrivatePeronListAsync(fromRow, toRow, uniqueIdentifier);

                    DrawTextProgressBar(toRow, profileCount);

                    var enumerable = list.ToList();

                    if (!enumerable.Any())
                        break;

                    var tasks = new List<Task>();

                    foreach (var profile in enumerable)
                    {
                        tasks.Add(Task.Run(async () =>
                        {
                            using (var localScope = service.CreateScope())
                            {
                                await localScope.ServiceProvider.GetRequiredService<IMembershipService>()
                                    .UpdateCacheAsync(profile.ServiceId, profile.UniqueIdentifier,profile,MembershipDataType.PrivatePerson);
                            }
                        }));
                    }

                    Task.WaitAll(tasks.ToArray());

                    if (pageSize == 1)
                        break;

                    if (enumerable.Count() == pageSize)
                    {
                        fromRow += pageSize;
                        toRow += pageSize;
                    }
                    else
                        break;

                }


            }
        }

        public static async Task UpdateAddressMembershipAsync(ServiceProvider service, int pageSize, int profileCount, string uniqueIdentifier = null)
        {

            int fromRow = 1;
            int toRow = pageSize;

            Console.WriteLine("Start Update AddressMembership: " + profileCount);

            while (true)
            {
                using (var scope = service.CreateScope())
                {

                    var privatePerson = scope.ServiceProvider.GetRequiredService<IMembershipService>();
                    var list = await privatePerson.GetAddressListAsync(fromRow, toRow, uniqueIdentifier);

                    DrawTextProgressBar(toRow, profileCount);

                    var enumerable = list.ToList();

                    if (!enumerable.Any())
                        break;

                    var tasks = new List<Task>();

                    foreach (var profile in enumerable)
                    {
                        tasks.Add(Task.Run(async () =>
                        {
                            using (var localScope = service.CreateScope())
                            {
                                await localScope.ServiceProvider.GetRequiredService<IMembershipService>()
                                    .UpdateCacheAsync(profile.ServiceId, profile.UniqueIdentifier, profile, MembershipDataType.Address);
                            }
                        }));
                    }

                    Task.WaitAll(tasks.ToArray());

                    if (pageSize == 1)
                        break;

                    if (enumerable.Count() == pageSize)
                    {
                        fromRow += pageSize;
                        toRow += pageSize;
                    }
                    else
                        break;

                }


            }
        }

        public static async Task UpdateBankingAccountMembershipAsync(ServiceProvider service, int pageSize, int profileCount, string uniqueIdentifier = null)
        {

            int fromRow = 1;
            int toRow = pageSize;

            Console.WriteLine("Start Update BankingAccountMembership: " + profileCount);

            while (true)
            {
                using (var scope = service.CreateScope())
                {

                    var privatePerson = scope.ServiceProvider.GetRequiredService<IMembershipService>();
                    var list = await privatePerson.GetBankingAccountListAsync(fromRow, toRow, uniqueIdentifier);

                    DrawTextProgressBar(toRow, profileCount);

                    var enumerable = list.ToList();

                    if (!enumerable.Any())
                        break;

                    var tasks = new List<Task>();

                    foreach (var profile in enumerable)
                    {
                        tasks.Add(Task.Run(async () =>
                        {
                            using (var localScope = service.CreateScope())
                            {
                                await localScope.ServiceProvider.GetRequiredService<IMembershipService>()
                                    .UpdateCacheAsync(profile.ServiceId, profile.UniqueIdentifier, profile, MembershipDataType.BankingAccount);
                            }
                        }));
                    }

                    Task.WaitAll(tasks.ToArray());

                    if (pageSize == 1)
                        break;

                    if (enumerable.Count() == pageSize)
                    {
                        fromRow += pageSize;
                        toRow += pageSize;
                    }
                    else
                        break;

                }


            }
        }
        private static void DrawTextProgressBar(int progress, int total)
        {
            //draw empty progress bar
            Console.CursorLeft = 0;
            Console.Write("("); //start
            Console.CursorLeft = 32;
            Console.Write(")"); //end
            Console.CursorLeft = 1;
            float onechunk = 30.0f / total;

            //draw filled part
            int position = 1;
            for (int i = 0; i < onechunk * progress; i++)
            {
                Console.CursorLeft = position++;
                Console.Write("|");
            }

            //draw unfilled part
            for (int i = position; i <= 31; i++)
            {
                Console.CursorLeft = position++;
                Console.Write("-");
            }

            if (progress > total)
                progress = total;

            var totalm = total * 1.0;
            //draw totals
            Console.CursorLeft = 35;

            Console.Write($"Processed Count: {progress} -- {(int)((progress / totalm) * 100)}  %                                "); //blanks at the end remove any excess
        }
    }
}
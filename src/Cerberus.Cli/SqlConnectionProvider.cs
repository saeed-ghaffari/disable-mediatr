﻿using System.Data;
using System.Data.SqlClient;
using Cerberus.Cli.Configuration;
using Cerberus.Domain.Interface.Repositories;
using Microsoft.Extensions.Options;

namespace Cerberus.Cli
{
    public class SqlConnectionProvider : ISqlConnectionProvider
    {
        private readonly IOptions<ConnectionStringsConfiguration> _options;

        public SqlConnectionProvider(IOptions<ConnectionStringsConfiguration> options)
        {
            _options = options;
        }

        public IDbConnection GetConnection()
        {
            return new SqlConnection(_options.Value.LogDbConnection);
        }
        public IDbConnection GetReadConnection()
        {
            return new SqlConnection(_options.Value.ReadConnection);
        }
        public IDbConnection GetRequestLogConnection()
        {
            throw new System.NotImplementedException();
        }
        public IDbConnection GetStatusReportDbConnection()
        {
            return new SqlConnection(_options.Value.StatusReportDbConnection);
        }
    }
}
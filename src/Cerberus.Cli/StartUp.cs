﻿using System;
using System.Net;
using System.Net.Http;
using AutoMapper;
using Cerberus.ApiService;
using Cerberus.ApiService.Implementations;
using Cerberus.Cli.Configuration;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Interface.Repositories.Membership;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Interface.Services.FGM;
using Cerberus.Domain.Interface.Services.Membership;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Repository;
using Cerberus.Repository.MsSql;
using Cerberus.Repository.MsSql.Log;
using Cerberus.Repository.MsSql.Membership;
using Cerberus.Service;
using Cerberus.Service.BankService.Configuration;
using Cerberus.Service.FGM;
using Cerberus.Service.Implementations;
using Cerberus.Service.Implementations.Inquiries;
using Cerberus.Service.Implementations.Membership;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Repository;
using Gatekeeper.Core.Configurations;
using Gatekeeper.Core.Implementation;
using Gatekeeper.Core.Interface;
using Komodo.Caching.Abstractions;
using Komodo.Caching.Redis;
using Komodo.Redis.StackExchange;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using File = Cerberus.Domain.Model.File;


namespace Cerberus.Cli
{
    public static class Startup
    {
        public static ServiceCollection Init(IConfiguration configuration)
        {
            ServicePointManager.DefaultConnectionLimit = int.MaxValue;
            var services = new ServiceCollection();
            services.AddCerberusServices(configuration);
            services.AddMapper(configuration);
            return services;

        }
        public static void AddCerberusServices(this IServiceCollection services, IConfiguration configuration)
        {
            //Configurations

            //Configurations

            var connectionSection = configuration.GetSection("ConnectionStrings");
            var connectionConfiguration = new ConnectionStringsConfiguration();
            connectionSection.Bind(connectionConfiguration);
            services.Configure<ConnectionStringsConfiguration>(connectionSection);

            services.Configure<MessagingConfiguration>(configuration.GetSection("MessagingConfiguration"));
            services.Configure<OtpConfiguration>(configuration.GetSection("OtpConfiguration"));
            services.Configure<JwtTokenConfiguration>(configuration.GetSection("JwtTokenConfiguration"));
            services.Configure<ApiConfiguration>(configuration.GetSection("ApiConfiguration"));
            services.Configure<FileStorageConfiguration>(configuration.GetSection("FileStorageConfiguration"));
            services.Configure<ObjectCacheServicesConfiguration>(configuration.GetSection("ObjectCacheServicesConfiguration"));
            services.Configure<IxServiceConfiguration>(configuration.GetSection("IxServiceConfiguration"));
            services.Configure<IssuanceStockCodeConfiguration>(configuration.GetSection("IssuanceStockCodeConfiguration"));
            services.Configure<FgmConfiguration>(configuration.GetSection("FgmConfiguration"));
            services.Configure<FgmConfiguration>(configuration.GetSection("FgmConfiguration"));

            services.Configure<MasterCacheProviderConfiguration>(configuration.GetSection("MasterCacheProviderConfiguration"));


            services.AddSingleton<ISqlConnectionProvider, SqlConnectionProvider>();

            //dbcontext

            services.AddDbContextPool<CerberusDbContext>(options =>
            {
                options.UseSqlServer(connectionConfiguration.MasterDbConnection);
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

            }, 8192);

            services.AddDbContextPool<CerberusLogDbContext>(options =>
            {
                options.UseSqlServer(connectionConfiguration.LogDbConnection);
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

            }, 4096);


            //Redis
            var redisConfiguration = new ConfigurationOptions();
            foreach (var endpoint in connectionConfiguration.RedisWriteDbHost.Split(';'))
            {
                redisConfiguration.EndPoints.Add(endpoint);
            }
            redisConfiguration.Password = connectionConfiguration.RedisWriteDbPass;
            redisConfiguration.ConnectTimeout = 400;
            services.AddSingleton<ConfigurationOptions>(redisConfiguration);

            services.AddSingleton<IConnectionMultiplexerProvider>(new ConnectionMultiplexerProvider(redisConfiguration, Environment.ProcessorCount * 2));

            services.AddSingleton<IDataBaseProvider, DatabaseProvider>();

            services.AddTransient<IDatabase>(provider => provider.GetService<IDataBaseProvider>().GetWriteDatabase());

            services.AddSingleton<ISerializer, BinarySerializer>();

            services.AddTransient<ICacheProvider>(provider =>
            {
                if (!provider.GetService<IOptions<ApiConfiguration>>().Value.EnableCache)
                    return new NullCacheProvider();

                return new RedisCacheProvider(
                    provider.GetService<IDatabase>(),
                    provider.GetService<IDatabase>(),
                    provider.GetService<ISerializer>());
            });

            //Repositories
            services.AddCerberusReadRepositories(configuration);



            services.AddScoped<IThirdPartyServiceRepository, ThirdPartyServiceRepository>();
            services.AddScoped<IProfileRepository, ProfileRepository>();
            services.AddScoped<IAgentRepository, AgentRepository>();
            services.AddScoped<IAgentDocumentRepository, AgentDocumentRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IPrivatePersonRepository, PrivatePersonRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IBankingAccountRepository, BankingAccountRepository>();
            services.AddScoped<IBrokerRepository, BrokerRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IFinancialInfoRepository, FinancialInfoRepository>();
            services.AddScoped<IJobInfoRepository, JobInfoRepository>();
            services.AddScoped<IJobRepository, JobRepository>();
            services.AddScoped<IPrivatePersonRepository, PrivatePersonRepository>();
            services.AddScoped<ITradingCodeRepository, TradingCodeRepository>();
            services.AddScoped<IProvinceRepository, ProvinceRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IAddressSectionRepository, AddressSectionRepository>();
            services.AddScoped<IBankRepository, BankRepository>();
            services.AddScoped<IPaymentRepository, PaymentRepository>();
            services.AddScoped<ILegalPersonShareholderRepository, LegalPersonShareholderRepository>();
            services.AddScoped<ILegalPersonStakeholderRepository, LegalPersonStakeholderRepository>();
            services.AddScoped<ILegalPersonRepository, LegalPersonRepository>();
            services.AddScoped<IProfileHistoryRepository, ProfileHistoryRepository>();
            services.AddScoped<IAuthenticationOfficesRepository, AuthenticationOfficesRepository>();
            services.AddScoped<IPermanentOtpRepository, PermanentOtpRepository>();

            services.AddSingleton<IApiRequestLogRepository, ApiRequestLogRepository>();
            services.AddSingleton<ISmsLogRepository, SmsLogRepository>();
            services.AddScoped<IAgentArchiveRepository, AgentArchiveRepository>();
            services.AddScoped<IAgentDocumentArchiveRepository, AgentDocumentArchiveRepository>();
            services.AddScoped<IIssuanceStockCodeRepository, IssuanceStockCodeRepository>();
            services.AddScoped<IIssuanceStockCodeResponseRepository, IssuanceStockCodeResponseRepository>();
            services.AddScoped<IProfilingRequestLogRepository, ProfilingRequestLogRepository>();
            services.AddScoped<IEtfMembershipRepository, EtfMembershipRepository>();
            services.AddScoped<IMembershipRepository, MembershipRepository>();


            services.AddScoped<ITaskRepository, SqlTaskRepository>();


            //----Api Services
            services.AddScoped<IAddressApiService, AddressApiService>();
            services.AddScoped<IProfileApiService, ProfileApiService>();
            services.AddScoped<IBankingAccountApiService, BankingAccountApiService>();
            services.AddScoped<IFinancialInfoApiService, FinancialInfoApiService>();
            services.AddScoped<IJobInfoApiService, JobInfoApiService>();
            services.AddScoped<ILegalPersonApiService, LegalPersonApiService>();
            services.AddScoped<IPrivatePersonApiService, PrivatePersonApiService>();
            services.AddScoped<ITradingCodeApiService, TradingCodesApiService>();

            services.AddScoped<IThirdPartyServiceService, ThirdPartyServiceService>();
            services.AddScoped<IMessagingService, MessagingService>();
            services.AddScoped<IProfileService, ProfileService>();
            services.AddScoped<IAgentService, AgentService>();
            services.AddScoped<IAgentDocumentService, AgentDocumentService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IPrivatePersonService, PrivatePersonService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<IBankingAccountService, BankingAccountService>();
            services.AddScoped<IBrokerService, BrokerService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IFinancialInfoService, FinancialInfoService>();
            services.AddScoped<IJobInfoService, JobInfoService>();
            services.AddScoped<IJobService, JobService>();
            services.AddScoped<IPrivatePersonService, PrivatePersonService>();
            services.AddScoped<ITradingCodeService, TradingCodeService>();
            services.AddScoped<IBankService, BankService>();
            services.AddScoped<IProvinceService, ProvinceService>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IAddressSectionService, AddressSectionService>();
            services.AddScoped<IMembershipService, MembershipService>();

            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IBankService, BankService>();
            services.AddScoped<ILegalPersonShareholderService, LegalPersonShareholderService>();
            services.AddScoped<ILegalPersonStakeholderService, LegalPersonStakeholderService>();
            services.AddScoped<ILegalPersonService, LegalPersonService>();
            services.AddScoped<IOfflineTaskService, OfflineTaskService>();
            services.AddScoped<ITaskRepository, SqlTaskRepository>();
            services.AddScoped<IRecurringTaskRepository, SqlRecurringTaskRepository>();
            services.AddScoped<IProfileHistoryService, ProfileHistoryService>();
            services.AddScoped<IAuthenticationOfficesService, AuthenticationOfficesService>();
            services.AddScoped<IPermanentOtpService, PermanentOtpService>();
            services.AddScoped<IPermanentOtpApiService, PermanentOtpApiService>();
            services.AddScoped<ILegacyCodeInquiryService, LegacyCodeInquiryService>();
            services.AddScoped<IIxService, IxService>();
            services.AddScoped<IFgmService, FgmService>();
            services.AddScoped<IMasterCacheProvider, MasterCacheProvider>();
            // gatekeeper
            services.AddScoped<IOtpService, OtpService>();

            //action filters

            //http clients
            services.AddCerberusHttpClients(configuration);

        }


        public static void AddMapper(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(provider =>
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<AutoMapper.Profile, ProfileViewModel>();
                    cfg.CreateMap<PrivatePerson, PrivatePersonViewModel>();
                    cfg.CreateMap<LegalPerson, LegalPersonViewModel>();
                    cfg.CreateMap<Address, AddressViewModel>();
                    cfg.CreateMap<TradingCode, TradingCodeViewModel>();
                    cfg.CreateMap<Agent, AgentViewModel>();
                    cfg.CreateMap<BankingAccount, BankingAccountViewModel>();
                    cfg.CreateMap<JobInfo, JobInfoViewModel>();
                    cfg.CreateMap<FinancialInfo, FinancialInfoViewModel>();
                    cfg.CreateMap<LegalPersonShareholder, LegalPersonShareholderViewModel>();
                    cfg.CreateMap<LegalPersonStakeholder, LegalPersonStakeholderViewModel>();
                    cfg.CreateMap<Country, CountryViewModel>();
                    cfg.CreateMap<Province, ProvinceViewModel>();
                    cfg.CreateMap<City, CityViewModel>();
                    cfg.CreateMap<AddressSection, AddressSection>();
                    cfg.CreateMap<Broker, BrokerViewModel>();
                    cfg.CreateMap<FinancialBroker, FinancialBrokerViewModel>();
                    cfg.CreateMap<Bank, BankViewModel>();
                    cfg.CreateMap<Job, JobViewModel>();


                    cfg.CreateMap<File, FileViewModel>()
                        .ForMember(file => file.FileName,
                        exp => exp.ResolveUsing(fileViewModel =>
                            fileViewModel.FileName
                                    = $"{configuration.GetValue<string>("FileStorageConfiguration:SejamServeFileUrl")}/{fileViewModel.FileName}"));

                    cfg.CreateMap<AuthenticationOffices, AuthenticateOfficeViewModel>();
                });
                return config.CreateMapper();
            });
        }

        public static void AddCerberusHttpClients(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpClient<IIssuanceStockCodeService, IssuanceStockCodeService>(client => { client.BaseAddress = new Uri(configuration.GetSection("IssuanceStockCodeConfiguration:Url").Value); })
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true });

            services.AddHttpClient("IxService", client => { client.BaseAddress = new Uri(configuration.GetSection("IxServiceConfiguration:IxAwaGetTradeCodeUrl").Value); })
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true });

            services.AddHttpClient("IxServiceAccessToken", client => { client.BaseAddress = new Uri(configuration.GetSection("IxServiceConfiguration:IxAwaGetAccessTokenUrl").Value); })
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true });

            services.AddHttpClient("IxServiceRefreshAccessToken", client => { client.BaseAddress = new Uri(configuration.GetSection("IxServiceConfiguration:IxAwaRefreshAccessTokenUrl").Value); })
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true });
        }
    }
}
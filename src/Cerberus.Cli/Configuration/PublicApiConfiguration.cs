﻿using System;
using Cerberus.Domain.Enum;

namespace Cerberus.Cli.Configuration
{
    public class ApiConfiguration
    {
        public TimeSpan AccessTokenTtl { get; set; }
        public TimeSpan OtpTtl { get; set; }
        public TimeSpan IssuerOtpTtl { get; set; }
        public bool KillAfterValidation { get; set; }
        public ProfileStatus CompleteProfileStatus { get; set; }
        public bool EnableCache { get; set; }
        public string InternalServiceOtpMobile { get; set; }
    }
}

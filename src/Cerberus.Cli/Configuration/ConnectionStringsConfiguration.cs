﻿namespace Cerberus.Cli.Configuration
{
    public class ConnectionStringsConfiguration
    {
        public string MasterDbConnection { get; set; }
        public string LogDbConnection { get; set; }
        public string RedisReadDbHost { get; set; }
        public string RedisReadDbPass { get; set; }
        public string RedisWriteDbHost { get; set; }
        public string RedisWriteDbPass { get; set; }
        public string ReadConnection { get; set; }
        public string StatusReportDbConnection { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Interface.Services.Membership;
using Cerberus.Domain.Model;
using CommandLine;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Cerberus.Cli
{
    class Program
    {

        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();

            var services = Startup.Init(configuration).BuildServiceProvider();

            

            Parser.Default.ParseArguments<CommandOptions>(args)
                .WithParsed(async options =>
                {
                    if (options != null) await DoCommand(options, services);
                });


            Console.ReadKey();
        }

        private static async Task DoCommand(CommandOptions opts, ServiceProvider services)
        {
            if (opts.UniqueIdentifier != null)
                opts.PageSize = 1;

            Console.WriteLine($"Cli started at {DateTime.Now:G}");
            using (var scope = services.CreateScope())
            {
                var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                var membershipService=scope.ServiceProvider.GetRequiredService<IMembershipService>();

                var profileCount = await profileService.CountAsync(null,ProfileStatus.SuccessPayment);
                
                foreach (var ent in opts.Entities)
                {
                    switch (ent.ToLower())
                    {
                        case "profile":
                        {
                            try
                            {
                                await TaskPool.UpdateProfilesAsync(services, opts.PageSize, profileCount,
                                    opts.UniqueIdentifier);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            break;
                        }
                        case "privateperson":
                        {
                            try
                            {
                                profileCount = await profileService.CountAsync(ProfileOwnerType.IranianLegalPerson, ProfileStatus.TraceCode);
                                    await TaskPool.UpdatePrivatePersonsAsync(services, opts.PageSize, profileCount,
                                    opts.UniqueIdentifier);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            break;
                        }
                        case "banckingaccount":
                        {
                            try
                            {
                                await TaskPool.UpdateBankingAccountsAsync(services, opts.PageSize, profileCount,
                                    opts.UniqueIdentifier);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            break;
                        }
                        case "financialinfo":
                        {
                            try
                            {
                                await TaskPool.UpdateFinancialInfosAsync(services, opts.PageSize, profileCount,
                                    opts.UniqueIdentifier);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            break;
                        }
                        case "address":
                        {
                            try
                            {
                                await TaskPool.UpdateAddressesAsync(services, opts.PageSize, profileCount,
                                    opts.UniqueIdentifier);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            break;
                        }
                        case "jobinfo":
                        {
                            try
                            {
                                await TaskPool.UpdateJobInfosAsync(services, opts.PageSize, profileCount,
                                    opts.UniqueIdentifier);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            break;
                        }
                        case "tradingcode":
                        {
                            try
                            {
                                await TaskPool.UpdateTradingCodesAsync(services, opts.PageSize, profileCount,
                                    opts.UniqueIdentifier);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            break;
                        }
                        case "legalperson":
                        {
                            try
                            {
                                profileCount = await profileService.CountAsync(ProfileOwnerType.IranianLegalPerson);

                                await TaskPool.UpdateLegalPersonsAsync(services, opts.PageSize, profileCount,
                                    opts.UniqueIdentifier);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            break;
                        }
                        case "privatepersonmembership":
                        {
                            try
                            {
                                var privatePersonMembershipCount = await membershipService.CountAsync(MembershipDataType.PrivatePerson);
                                    await TaskPool.UpdatePrivatePersonsMembershipAsync(services, opts.PageSize, privatePersonMembershipCount,
                                    opts.UniqueIdentifier);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            break;
                        }
                        case "addressmembership":
                        {
                            try
                            {
                                var addressMembershipCount = await membershipService.CountAsync(MembershipDataType.Address);
                                    await TaskPool.UpdateAddressMembershipAsync(services, opts.PageSize, addressMembershipCount,
                                    opts.UniqueIdentifier);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            break;
                        }
                        case "bankingaccountmembership":
                        {
                            try
                            {
                                var bankingAccountMembershipCount = await membershipService.CountAsync(MembershipDataType.BankingAccount);
                                    await TaskPool.UpdateBankingAccountMembershipAsync(services, opts.PageSize, bankingAccountMembershipCount,
                                    opts.UniqueIdentifier);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            break;
                        }
                    }
                }
            }

            Console.WriteLine($"Cli Ended at {DateTime.Now:G}");

            Console.Read();
        }

       
    }
}

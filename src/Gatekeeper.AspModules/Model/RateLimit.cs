﻿using System;

namespace Gatekeeper.AspModules.Model
{
    public struct RateLimit
    {
        public TimeSpan Range { get; set; }
        public int Count { get; set; }
    }
}
﻿using System;
using System.Threading.Tasks;
using Aspina;
using Gatekeeper.AspModules.Interface;
using Komodo.Redis.StackExchange;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;

namespace Gatekeeper.AspModules.Service
{
    public class RateLimitService : IRateLimitService
    {
        private readonly IDataBaseProvider _databaseProvider;
        private readonly IBackgroundTaskQueue _backgroundTaskQueue;
        private readonly ILogger<RateLimitService> _logger;

        public RateLimitService(IDataBaseProvider dataBaseProvider, IBackgroundTaskQueue backgroundTaskQueue, ILogger<RateLimitService> logger)
        {
            _databaseProvider = dataBaseProvider;
            _backgroundTaskQueue = backgroundTaskQueue;
            _logger = logger;
        }

        public async Task<bool> HasAccessAsync(string resourceKey, int periodInSec, int limit)
        {
            var counts = await _databaseProvider.GetReadDatabase().SortedSetLengthAsync(resourceKey, DateTime.Now.AddSeconds(-periodInSec).ToUtcTimestamp(),
                DateTime.Now.AddSeconds(periodInSec).ToUtcTimestamp());

            if (counts >= limit)
            {
                _logger.LogCritical($"Rate limit : key :{resourceKey} - count:{counts}");
                return false;
            }

            _backgroundTaskQueue.QueueBackgroundWorkItem(token => _databaseProvider.GetWriteDatabase().SortedSetAddAsync(resourceKey, DateTime.Now.Ticks, DateTime.Now.ToUtcTimestamp(), CommandFlags.FireAndForget));

            _backgroundTaskQueue.QueueBackgroundWorkItem(token => _databaseProvider.GetWriteDatabase().KeyExpireAsync(resourceKey, TimeSpan.FromSeconds(periodInSec), CommandFlags.FireAndForget));


            return true;
        }


    }
}
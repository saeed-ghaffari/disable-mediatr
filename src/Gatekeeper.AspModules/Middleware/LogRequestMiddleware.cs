﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Aspina;
using Cerberus.Utility;
using Gatekeeper.AspModules.Interface;
using Gatekeeper.AspModules.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace Gatekeeper.AspModules.Middleware
{
    public class LogRequestMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IRequestLogRepository _requestLogRepository;
        private readonly IOptions<RequestLogConfiguration> _configuration;
        private readonly IBackgroundTaskQueue _queue;

        public LogRequestMiddleware(RequestDelegate next,
            IBackgroundTaskQueue queue,
            IOptions<RequestLogConfiguration> configuration,
            IRequestLogRepository requestLogRepository)
        {
            _queue = queue;
            _next = next;
            _requestLogRepository = requestLogRepository;
            _configuration = configuration;
        }



        public async Task InvokeAsync(HttpContext context)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
                await _next.Invoke(context);
            }
            finally
            {
                stopWatch.Stop();

                //create log object
                try
                {

                    //create log object
                    var log = new RequestLog()
                    {
                        Headers = _configuration.Value.TargetHeaders == null || _configuration.Value.TargetHeaders.Length == 0
                            ? null
                            : context
                                .Request?
                                .Headers?
                                .Where(x => _configuration.Value
                                                .TargetHeaders
                                                .FirstOrDefault(header => header.Equals(x.Key.ToLower())) != null)
                                .Select(x => new KeyValuePair<string, string>(x.Key, x.Value)).ToList(),
                        HttpResponseCode = context.Response?.StatusCode,
                        Method = context.Request?.Method,
                        Query = context.Request?.QueryString.Value,
                        ExecutionTime = stopWatch.ElapsedMilliseconds,
                        CreationDate = DateTime.Now,
                        Ip =Tools.GetUserIp(context.Request)
                    };

                    var features = context.Features.Get<IAspinaMetricFeatures>();
                    if (features != null)
                    {
                        log.Tags = new List<KeyValuePair<string, string>>();

                        log.Tags.AddRange(features.GetAllTags());
                    }

                    //queue to background task queue
                    _queue.QueueBackgroundWorkItem(token => _requestLogRepository.AddAsync(log));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }
    }
}

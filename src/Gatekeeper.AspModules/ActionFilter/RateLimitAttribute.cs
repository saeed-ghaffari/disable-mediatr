﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Aspina.Model;
using Cerberus.Api.Model;
using Cerberus.Utility;
using Gatekeeper.AspModules.Interface;
using Gatekeeper.AspModules.Model;
using GateKeeper.Auth.Enum;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Gatekeeper.AspModules.ActionFilter
{
    public class RateLimitAttribute : IAsyncActionFilter, IOrderedFilter
    {
        private readonly ILogger<RateLimitAttribute> _logger;
        private readonly IRateLimitService _rateLimitService;
        private readonly IOptions<RateLimitConfiguration> _options;

        public RateLimitAttribute(ILogger<RateLimitAttribute> logger, IRateLimitService rateLimitService, IOptions<RateLimitConfiguration> options)
        {
            _logger = logger;
            _rateLimitService = rateLimitService;
            _options = options;
        }

        public int Order { get; set; }
        public string UserIdentifier { get; set; }
        public int PeriodInSec { get; set; }
        public int Limit { get; set; }
        public string VaryByParams { get; set; }
        public int VaryByParamsPeriodInSec { get; set; }
        public int VaryByParamsLimit { get; set; }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            try
            {
                if (!_options.Value.Enable)
                {
                    await next.Invoke();
                    return;
                }

                var userKey = string.IsNullOrWhiteSpace(UserIdentifier)
                    ?context.HttpContext.Request.GetUserIp()
                    : context.HttpContext.Items[UserIdentifier].ToString();

                var controller = context.ActionDescriptor.RouteValues["Controller"];
                var action = context.ActionDescriptor.RouteValues["Action"];


                bool hasAccess = await _rateLimitService.HasAccessAsync($"{userKey}:{controller}:{action}", PeriodInSec, Limit);

                if (hasAccess && !string.IsNullOrWhiteSpace(VaryByParams))
                {
                    var parameters = VaryByParams.Split(',');

                    var paramsKey = "";

                    foreach (var parameter in parameters)
                    {
                        if (context.ActionArguments.ContainsKey(parameter))
                            paramsKey += $"{context.ActionArguments[parameter]}:";
                    }

                    if (!string.IsNullOrWhiteSpace(paramsKey) &&
                        !await _rateLimitService.HasAccessAsync($"{userKey}:{controller}:{action}:{paramsKey}", VaryByParamsPeriodInSec, VaryByParamsLimit))
                        hasAccess = false;//Rate limit exceeded
                }

                if (!hasAccess)
                {
                    context.HttpContext.Response.StatusCode = 429;

                    if (_options.Value.ReturnErrorPage)
                    {
                        if (IsAjaxRequest(context.HttpContext.Request))
                            await context.HttpContext.Response.WriteAsync("429");
                        return;
                    }

                    context.HttpContext.Response.ContentType = "application/json";

                    var response = new Envelop()
                    {
                        Error = new EnvelopError()
                        {
                            ErrorCode = 429,
                            CustomMessage = "Rate limit exceeded"
                        },
                        Data = null
                    };

                    await context.HttpContext.Response.WriteAsync(JsonConvert.SerializeObject(response, JsonSerializerSettingsProvider.CreateSerializerSettings()));
                }
                else
                    await next.Invoke();
            }
            catch (RateLimitException)
            {
                throw;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, e.Message);
                await next.Invoke();
            }
        }

        private const string RequestedWithHeader = "X-Requested-With";
        private const string XmlHttpRequest = "XMLHttpRequest";

        private bool IsAjaxRequest(HttpRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            if (request.Headers != null)
            {
                return request.Headers[RequestedWithHeader] == XmlHttpRequest;
            }

            return false;
        }
    }
}

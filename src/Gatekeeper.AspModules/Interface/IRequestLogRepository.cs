﻿using System.Threading.Tasks;
using Gatekeeper.AspModules.Model;

namespace Gatekeeper.AspModules.Interface
{
    public interface IRequestLogRepository
    {
        Task AddAsync(RequestLog log);
    }
}
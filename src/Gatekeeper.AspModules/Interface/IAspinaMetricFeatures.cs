﻿using System.Collections.Generic;

namespace Gatekeeper.AspModules.Interface
{
    public interface IAspinaMetricFeatures
    {
        void AddTag(string key, string value);
        IEnumerable<KeyValuePair<string, string>> GetAllTags();
    }
}
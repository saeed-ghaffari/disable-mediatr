﻿using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Http;

namespace Gatekeeper.AspModules
{
    internal static class Utility
    {
        public static string GetUserAgent(this HttpRequest request)
        {
            return request.Headers["User-Agent"].ToString();
        }

        public static long ToUtcTimestamp(this DateTime dateTime)
        {
            return (long)dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
}
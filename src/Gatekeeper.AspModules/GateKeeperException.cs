﻿using System;

namespace Gatekeeper.AspModules
{
    public class RateLimitException : Exception
    {
        public RateLimitException(string message) : base(message)
        {

        }
        public RateLimitException()
        {

        }
    }
}
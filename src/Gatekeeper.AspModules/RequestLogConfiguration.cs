﻿namespace Gatekeeper.AspModules
{
    public class RequestLogConfiguration
    {
        public bool IsEnable { get; set; }
        public string[] TargetHeaders { get; set; }
        public string DbEndPoint { get; set; }
        public int DbTimeout { get; set; }
        public string DbUsername { get; set; }
        public string DbPass { get; set; }
    }
}
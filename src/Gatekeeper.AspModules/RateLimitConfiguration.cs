﻿namespace Gatekeeper.AspModules
{
    public class RateLimitConfiguration
    {
        public bool Enable { get; set; }
        public bool ReturnErrorPage { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Gatekeeper.AspModules.Interface;
using Gatekeeper.AspModules.Model;
using KairosDbClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Timer = System.Timers.Timer;

namespace Gatekeeper.AspModules.Repository
{
    public class LogRequestRepository : IRequestLogRepository
    {
        private readonly ILogger<LogRequestRepository> _logger;
        private readonly List<Metric> _metricsList = new List<Metric>();
        private readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1);

        static LogRequestRepository()
        {
            ServicePointManager.DefaultConnectionLimit = Int32.MaxValue;
            ServicePointManager.FindServicePoint(new Uri("http://94.182.163.118:8080/api/v1/datapoints"))
                .ConnectionLeaseTimeout = (int)TimeSpan.FromSeconds(5).TotalMilliseconds;
        }

        public class DataPoint
        {
            [JsonProperty("name")] public string Name { get; set; }

            [JsonProperty("value")] public int Value { get; set; }

            [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
            public string Type { get; set; }

            [JsonProperty("timestamp", NullValueHandling = NullValueHandling.Ignore)]
            public long? Timestamp { get; set; }

            [JsonProperty("tags", NullValueHandling = NullValueHandling.Ignore)]
            public Dictionary<string, string> Tags { get; set; }
        }


        public LogRequestRepository(ILogger<LogRequestRepository> logger,
            IOptions<RequestLogConfiguration> configuration)
        {
            _logger = logger;

            var restClient = new RestClient(configuration.Value.DbEndPoint);

            var timer = new Timer(1000);
            timer.Elapsed += async (sender, args) =>

            {
                try
                {

                    var listToSend = new List<Metric>();
                    _semaphoreSlim.Wait();
                    listToSend.AddRange(_metricsList);
                    _metricsList.Clear();
                    _semaphoreSlim.Release();

                    if (listToSend.Count == 0)
                        return;

                    await restClient.AddMetricsAsync(listToSend);
                }
                catch (Exception e)
                {
                    _logger.LogCritical(e, e.Message);
                }

            };
            timer.Enabled = true;
            timer.Start();
        }

        private List<DataPoint> CreateRequest(RequestLog log)
        {
            var tags = new Dictionary<string, string>
            {
                {"Method", log.Method},
                {"ServerName", Environment.MachineName},
            };

            if (log.Tags != null && log.Tags.Count != 0)
            {
                log.Tags.ForEach(x => tags.Add(x.Key, x.Value));
            }

            if (log.HttpResponseCode.HasValue)
                tags.Add("HttpResponseCode", log.HttpResponseCode.Value.ToString());

            log.Headers?.ToList().ForEach(x => tags.Add(x.Key, x.Value));

            var dataPoints = new List<DataPoint>
            {
                new DataPoint()
                {
                    Value = (int) log.ExecutionTime,
                    Name = "gatekeeper.metrics.executionTime",
                    Tags = tags,
                    Timestamp = ToLocalTimestamp(log.CreationDate)
                }
            };


            return dataPoints;
        }

        public async Task AddAsync(RequestLog log)
        {
            //await HttpWebRequestAsync(CreateRequest(log));
            var metrics = new List<Metric>();
            var logs = CreateRequest(log);
            foreach (var item in logs)
            {
                var metric = new Metric(item.Name);
                if (item.Timestamp != null)
                    metric.AddDataPoint(new KairosDbClient.DataPoint(item.Timestamp.Value, item.Value));
                metric.AddTags(item.Tags);
                metrics.Add(metric);
            }

            await _semaphoreSlim.WaitAsync();
            _metricsList.AddRange(metrics);
            _semaphoreSlim.Release();
        }


    
        public static long ToLocalTimestamp(DateTime dateTime)
        {
            return (long)dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using Gatekeeper.AspModules.Interface;
using Gatekeeper.AspModules.Model;
using Newtonsoft.Json;

namespace Gatekeeper.AspModules.Repository
{
    public class MockLogRequestRepository : IRequestLogRepository
    {
        public Task AddAsync(RequestLog log)
        {
            Console.WriteLine(JsonConvert.SerializeObject(log, Formatting.Indented));

            return Task.CompletedTask;
        }

        public void Add(RequestLog log)
        {
            Console.WriteLine(JsonConvert.SerializeObject(log, Formatting.Indented));
        }
    }
}
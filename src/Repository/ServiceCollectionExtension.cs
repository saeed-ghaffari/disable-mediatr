﻿using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.ReadRepositories.Membership;
using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Repository.MsSql;
using Cerberus.Repository.ReadRepositories;
using Cerberus.Repository.ReadRepositories.Membership;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Cerberus.Repository
{
    public static class ServiceCollectionExtension
    {
        public static void AddCerberusReadRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContextPool<ReadCerberusDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetValue<string>("ConnectionStrings:ReadConnection"));
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

            }, 8192);

            services.AddScoped<IProfileReadRepository, ProfileReadRepository>();
            services.AddScoped<IAgentReadRepository, AgentReadRepository>();
            services.AddScoped<IAgentDocumentReadRepository, AgentDocumentReadRepository>();
            services.AddScoped<IPrivatePersonReadRepository, PrivatePersonReadRepository>();
            services.AddScoped<IAddressReadRepository, AddressReadRepository>();
            services.AddScoped<IBankingAccountReadRepository, BankingAccountReadRepository>();
            services.AddScoped<IBrokerReadRepository, BrokerReadRepository>();
            services.AddScoped<ICountryReadRepository, CountryReadRepository>();
            services.AddScoped<IFinancialInfoReadRepository, FinancialInfoReadRepository>();
            services.AddScoped<IJobInfoReadRepository, JobInfoReadRepository>();
            services.AddScoped<IJobReadRepository, JobReadRepository>();
            services.AddScoped<IPrivatePersonReadRepository, PrivatePersonReadRepository>();
            services.AddScoped<ITradingCodeReadRepository, TradingCodeReadRepository>();
            services.AddScoped<IProvinceReadRepository, ProvinceReadRepository>();
            services.AddScoped<ICityReadRepository, CityReadRepository>();
            services.AddScoped<IAddressSectionReadRepository, AddressSectionReadRepository>();
            services.AddScoped<IBankReadRepository, BankReadRepository>();
            services.AddScoped<IPaymentReadRepository, PaymentReadRepository>();
            services.AddScoped<ILegalPersonShareholderReadRepository, LegalPersonShareholderReadRepository>();
            services.AddScoped<ILegalPersonStakeholderReadRepository, LegalPersonStakeholderReadRepository>();
            services.AddScoped<ILegalPersonReadRepository, LegalPersonReadRepository>();
            services.AddScoped<IMembershipReadRepository, MembershipReadRepository>();
            services.AddScoped<IThirdPartyServiceReadRepository, ThirdPartyServiceReadRepository>();
            services.AddScoped<IAuthenticationOfficesReadRepository, AuthenticationOfficesReadRepository>();
            services.AddScoped<IProfilePackageReadRepository, ProfilePackageReadRepository>();
            services.AddScoped<IProfileHistoryReadRepository, ProfileHistoryReadRepository>();
            services.AddScoped<IPostInquiryReadRepository, PostInquiryReadRepository>();
            services.AddScoped<IPermanentOtpReadRepository, PermanentOtpReadRepository>();
            services.AddScoped<IMsisdnReadRepository, MsisdnReadRepository>();
        }
    }
}
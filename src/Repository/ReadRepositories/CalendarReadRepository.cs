﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class CalendarReadRepository : BaseReadRepository<Calendar>, ICalendarReadRepository
    {
        private readonly ReadCerberusDbContext _db;
        public CalendarReadRepository(ReadCerberusDbContext db) : base(db)
        {
            _db = db;
        }

        public async Task<bool> TodayIsHoliday()
        {
            return await _db.Calendars.AsNoTracking().AnyAsync(x => x.IsHoliday && x.GregorianDate.Date == DateTime.Now.Date);
        }
    }
}
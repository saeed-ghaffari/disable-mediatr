﻿using Cerberus.Domain.Model;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Repository.MsSql;

namespace Cerberus.Repository.ReadRepositories
{
    public class AddressSectionReadRepository : BaseReadRepository<AddressSection>, IAddressSectionReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;

        public AddressSectionReadRepository(ReadCerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

       
    }
}
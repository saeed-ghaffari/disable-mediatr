﻿using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Repository.MsSql;

namespace Cerberus.Repository.ReadRepositories
{
    public class CityReadRepository : BaseReadRepository<City>, ICityReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;

        public CityReadRepository(ReadCerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<List<City>> GetListAsync(long provinceId)
        {
            return await _dbContext.Cities.AsNoTracking().Where(x => x.ProvinceId == provinceId).ToListAsync();
        }

        public async Task<List<City>> GetListAsync()
        {
            return await _dbContext.Cities.AsNoTracking().ToListAsync();
        }

    }
}
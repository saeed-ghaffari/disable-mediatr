﻿using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Repository.MsSql;

namespace Cerberus.Repository.ReadRepositories
{
    public class BankReadRepository : BaseReadRepository<Bank>, IBankReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;
        public BankReadRepository(ReadCerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<List<Bank>> GetListAsync()
        {
            return await _dbContext.Banks.AsNoTracking().ToListAsync();
        }

        public async Task<List<Bank>> GetListForAuthenticationOfficeAsync()
        {
            var c = from b in _dbContext.Banks
                join au in _dbContext.AuthenticationOfficeses
                    on b.Id equals au.BankId
                select b;

            return await c.AsNoTracking().Distinct().ToListAsync();
        }
    }
}
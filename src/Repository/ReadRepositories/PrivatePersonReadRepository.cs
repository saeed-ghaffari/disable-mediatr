﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Dapper;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class PrivatePersonReadRepository : BaseReadRepository<PrivatePerson>, IPrivatePersonReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;

        public PrivatePersonReadRepository(ReadCerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) :
            base(db)
        {
            this._dbContext = db;
            _sqlConnectionProvider = sqlConnectionProvider;
        }

        public async Task<PrivatePerson> GetByProfileIdAsync(long profileId)
        {
            return await _dbContext.PrivatePeople.AsNoTracking().FirstOrDefaultAsync(row => row.ProfileId == profileId);
        }

        public async Task<PrivatePerson> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {

            using (var db = _sqlConnectionProvider.GetReadConnection())
            {
                var cmd =
                    $"declare @profileId bigint select @profileId=Id from Profile (nolock) where UniqueIdentifier='{uniqueIdentifier}' "
                    + " SELECT * FROM dbo.PrivatePerson (NOLOCK) j LEFT JOIN dbo.[File] b ON b.Id = j.SignatureFileId LEFT JOIN dbo.[File] img (nolock) ON img.Id=j.ImageId WHERE j.ProfileId = @profileId";
                return (await db.QueryAsync<PrivatePerson, File, File, PrivatePerson>(cmd, (person, signature, image) =>
                {
                    person.SignatureFile = signature;
                    person.ImageFile = image;
                    return person;
                })).FirstOrDefault();
            }

        }

        public async Task<IEnumerable<PrivatePerson>> GetByDaysLeftToEighteenAsync(int days, int status)
        {
            using (var connection = _sqlConnectionProvider.GetReadConnection())
            {
                if (days == 1 || days == 7)
                {
                    return await connection.QueryAsync<PrivatePerson, Profile, Agent, PrivatePerson>
                    ($"SELECT * FROM dbo.PrivatePerson (NOLOCK) p JOIN dbo.Profile (NOLOCK) pf ON pf.Id = p.ProfileId " +
                     $"JOIN dbo.Agent (NOLOCK) a ON a.ProfileId=pf.id "
                     + $"WHERE DATEDIFF(DAY, GETDATE(), DATEADD(YEAR, 18, BirthDate)) = {days} AND pf.Status >= {status}  AND a.type = 2" ,
                        (prv, p, a) =>
                        {
                            prv.ProfileOwner = p;
                            prv.ProfileOwner.Agent = a;
                            return prv;
                        });
                }

                return await connection.QueryAsync<PrivatePerson, Profile, Agent, PrivatePerson>
                ($"SELECT * FROM dbo.PrivatePerson (NOLOCK) p JOIN dbo.Profile (NOLOCK) pf ON pf.Id = p.ProfileId " +
                 $"JOIN dbo.Agent (NOLOCK) a ON a.ProfileId=pf.id "
                 + $"WHERE DATEDIFF(DAY, GETDATE(), DATEADD(YEAR, 18, BirthDate)) <= {days} AND pf.Status >= {status} AND a.type = 2",
                    (prv, p, a) =>
                    {
                        prv.ProfileOwner = p;
                        prv.ProfileOwner.Agent = a;
                        return prv;
                    });
            }

        }

        public async Task<PrivatePerson> GetByProfileIdWithNoLockAsync(long profileId)
        {
            using (var con = _sqlConnectionProvider.GetConnection())
            {
                var result =
                    await con.QueryMultipleAsync(
                        $" select  top 1 * from privatePerson (nolock)  where ProfileId = {profileId}");

                var privatePerson = await result.ReadFirstOrDefaultAsync<PrivatePerson>();

                return privatePerson ?? null;
            }
        }

        public async Task<string> GetIncludeImageAsync(long profileId, ImageType imageType)
        {
            using (var db = _sqlConnectionProvider.GetReadConnection())
            {
                var sql = imageType.Equals(ImageType.PersonImage)
                    ? "select f.FileName from PrivatePerson (nolock) p  left join [File] (nolock)  f on p.ImageId=f.Id where p.ProfileId=@profileId"
                    : "select f.FileName from PrivatePerson (nolock) p  left join [File] (nolock)  f on p.SignatureFileId=f.Id where p.ProfileId=@profileId";

                db.Open();

                var result = await db.QueryFirstOrDefaultAsync<string>(sql, new {profileId = profileId});

                return result;
            }
        }
    }
}
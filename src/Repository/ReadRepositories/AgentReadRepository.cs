﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Dapper;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class AgentReadRepository : BaseReadRepository<Agent>, IAgentReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;

        public AgentReadRepository(ReadCerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) : base(db)
        {
            this._dbContext = db;
            _sqlConnectionProvider = sqlConnectionProvider;
        }

        public async Task<Agent> GetByProfileIdAsync(long profileId)
        {
            return await _dbContext.Agents.Include(x => x.AgentProfile).ThenInclude(x => x.PrivatePerson)
                .AsNoTracking()
                .FirstOrDefaultAsync(agent => agent.ProfileId == profileId);
        }

        public async Task<Agent> GetByAgentProfileIdAsync(long agentProfileId)
        {
            return await _dbContext.Agents.AsNoTracking()
                .FirstOrDefaultAsync(agent => agent.AgentProfileId == agentProfileId);
        }

        public async Task<List<Agent>> GetListByAgentProfileIdAsync(long agentProfileId)
        {
            return await _dbContext.Agents.Include(x => x.ProfileOwner).ThenInclude(x => x.PrivatePerson)
            .Where(agent => agent.AgentProfileId == agentProfileId).AsNoTracking().ToListAsync();
        }

        public async Task<Agent> GetByProfileIdWithNoLockAsync(long profileId)
        {
            using (var con = _sqlConnectionProvider.GetConnection())
            {
                var result = await con.QueryMultipleAsync($" select  top 1 * from Agent (nolock)  where ProfileId = {profileId}");

                var agent = await result.ReadFirstOrDefaultAsync<Agent>();

                return agent ?? null;
            }
        }
    }
}
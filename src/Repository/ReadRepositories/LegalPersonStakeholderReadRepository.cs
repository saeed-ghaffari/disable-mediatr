﻿using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Repository.MsSql;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class LegalPersonStakeholderReadRepository : BaseReadRepository<LegalPersonStakeholder>, ILegalPersonStakeholderReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;

        public LegalPersonStakeholderReadRepository(ReadCerberusDbContext db) : base(db)
        {
            _dbContext = db;
        }

        public async Task<List<LegalPersonStakeholder>> GetListAsync(long profileId, StakeHolderType type)
        {
            return
                await _dbContext.LegalPersonStakeholders
                            .AsNoTracking()
                            .Include(x => x.StakeholderProfile)
                                .ThenInclude(x => x.PrivatePerson)
                            .Include(x => x.SignatureFile)
                            .Where(row => row.ProfileId == profileId && row.Type == type)
                            .ToListAsync();
        }

        public async Task<List<LegalPersonStakeholder>> GetListAsync(long profileId)
        {
            return
                await _dbContext.LegalPersonStakeholders
                            .AsNoTracking()
                            .Include(x => x.StakeholderProfile)
                            .Where(row => row.ProfileId == profileId)
                            .ToListAsync();
        }
    }
}
﻿using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Repository.MsSql;

namespace Cerberus.Repository.ReadRepositories
{
    public class IlencInquiryReadRepository : BaseReadRepository<IlencInquiry>, IIlencInquiryReadRepository    
    {
        public IlencInquiryReadRepository(ReadCerberusDbContext dbContext) : base(dbContext)
        {
        }

    }
}

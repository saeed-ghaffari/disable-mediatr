﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class ProfilePackageReadRepository : BaseReadRepository<ProfilePackage>, IProfilePackageReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;

        public ProfilePackageReadRepository(ReadCerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<ProfilePackage> GetUnusedPackageAsync(long profileId, PackageType type, PackageUsedFlag usePackage)
        {

            var pack = await _dbContext
                .Payments
                .Join(
                    _dbContext.ProfilePackages,
                    payment => payment.ProfilePackageId,
                    package => package.Id,
                    ((payment, package) => new { package, payment })
                )
                .Where(x =>
                    x.payment.Status == PaymentStatus.Settle &&
                    x.payment.ProfileId == profileId &&
                    (
                        x.package.Type == type)
                    && (x.package.ExpirationDate > DateTime.Now)
                )
                .Select(x => x.package)
                .ToListAsync();
            return pack
                ?.Where(x => !x.Used && x.ProfileId == profileId &&  x.HasPermission(usePackage))
                .FirstOrDefault();
        }

        public async Task<ProfilePackage> GetSuspiciousPackageAsync(long profileId, PackageType type)
        {
            var pack = await _dbContext
                .Payments
                .Join(
                    _dbContext.ProfilePackages,
                    payment => payment.ProfilePackageId,
                    package => package.Id,
                    ((payment, package) => new { package, payment })
                )
                .Where(x =>
                    x.payment.Status == PaymentStatus.Init || x.payment.Status == PaymentStatus.Verify &&
                    x.payment.ProfileId == profileId &&
                    (
                        x.package.Type == type)
                   )
                .Select(x => x.package)
                .ToListAsync();
            return pack
                ?.Where(x => !x.Used && x.ProfileId==profileId)
                .FirstOrDefault();
        }


        public async Task<IEnumerable<ProfilePackage>> AllPackagesByProfileId(long profileId, PackageType type)
        {
            return await _dbContext
                .Payments
                .Join(
                    _dbContext.ProfilePackages,
                    payment => payment.ProfilePackageId,
                    package => package.Id,
                    ((payment, package) => new { package, payment })
                )
                .Where(x =>
                    x.payment.Status == PaymentStatus.Settle &&
                    x.payment.ProfileId == profileId &&
                    (
                        x.package.Type == type)
                 )
                .Select(x => x.package)
                .ToListAsync();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class BaseReadRepository<T> : IBaseReadRepository<T> where T : BaseModel
    {
        private readonly ReadCerberusDbContext _dbContext;

        public BaseReadRepository(ReadCerberusDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<T> GetAsync(long id)
        {
            var response = await _dbContext.FindAsync<T>(id);
            if (response == null || response.IsDeleted)
                return null;

            return response;

        }

        public async Task<IEnumerable<T>> GetListAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbContext.Set<T>().Where(predicate).ToListAsync();

        }
        public async Task<IEnumerable<T>> GetConditionalListAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbContext.Set<T>().Where(predicate).Where(x => !x.IsDeleted).ToListAsync();

        }
        public async Task<IEnumerable<T>> GetGenericAsync(Expression<Func<T, bool>> predicate,
            Func<IQueryable<T>, IQueryable<T>> include = null, Func<IQueryable<T>, IOrderedQueryable<T>> order = null)
        {
            IQueryable<T> query = _dbContext.Set<T>();
            if (predicate == null)
                return null;

            query = query.Where(predicate);
            if (include != null)
                query = include(query);
            if (order != null)
                query = order(query);

            return await query.ToListAsync();

        }
    }
}
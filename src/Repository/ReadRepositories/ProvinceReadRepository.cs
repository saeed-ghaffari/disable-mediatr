﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class ProvinceReadRepository : BaseReadRepository<Province>, IProvinceReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;

        public ProvinceReadRepository(ReadCerberusDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Province>> GetListAsync(long countryId)
        {
            return await _dbContext.Provinces.AsNoTracking().Where(x => x.CountryId == countryId).ToListAsync();
        }

        public async Task<IEnumerable<Province>> GetListAsync()
        {
            return await _dbContext.Provinces.AsNoTracking().ToListAsync();
        }
    }
}
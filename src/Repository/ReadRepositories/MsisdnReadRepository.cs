﻿using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Dapper;

namespace Cerberus.Repository.ReadRepositories
{
    public class MsisdnReadRepository : BaseReadRepository<Msisdn>, IMsisdnReadRepository
    {
        private readonly ISqlConnectionProvider _sqlConnectionProvider;

        public MsisdnReadRepository(ReadCerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) : base(db)
        {
            _sqlConnectionProvider = sqlConnectionProvider;
        }

        public async Task<Msisdn> GetByProfileIdAsync(long profileId)
        {
            using (var db = _sqlConnectionProvider.GetReadConnection())
            {
                return await db.QueryFirstOrDefaultAsync<Msisdn>($"select top 1 * from  msisdn where profileId ={profileId}");
            }

        }
    }
}

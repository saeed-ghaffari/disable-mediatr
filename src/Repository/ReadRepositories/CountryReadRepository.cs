﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class CountryReadRepository : BaseReadRepository<Country>, ICountryReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;

        public CountryReadRepository(ReadCerberusDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<Country>> GetListAsync()
        {
            return await _dbContext.Countries.AsNoTracking().ToListAsync();
        }
    }
}
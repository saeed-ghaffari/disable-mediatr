﻿using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class ThirdPartyServiceReadRepository : BaseReadRepository<ThirdPartyService>, IThirdPartyServiceReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;
        public ThirdPartyServiceReadRepository(ReadCerberusDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ThirdPartyService> GetByLinkAsync(string link)
        {
            return await _dbContext.ThirdPartyServices.AsNoTracking().FirstOrDefaultAsync(x => x.LinkRefId == link);
        }

        public async Task<string> GetThirdPartyName(long? id)
        {
            var result = await _dbContext.ThirdPartyServices.AsNoTracking().FirstOrDefaultAsync(p => p.Id == id);
            return result?.Title;
        }

        public async Task<ThirdPartyService> GetAsync(string title)
        {
            var result = await _dbContext.ThirdPartyServices.AsNoTracking().FirstOrDefaultAsync(p => p.Title.Equals(title));
            return result;
        }
    }
}
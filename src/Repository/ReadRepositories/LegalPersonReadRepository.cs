﻿using System.Linq;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Repository.MsSql;
using Dapper;

namespace Cerberus.Repository.ReadRepositories
{
    public class LegalPersonReadRepository : BaseReadRepository<LegalPerson>, ILegalPersonReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;

        public LegalPersonReadRepository(ReadCerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) : base(db)
        {
            this._dbContext = db;
            _sqlConnectionProvider = sqlConnectionProvider;
        }

        public async Task<LegalPerson> GetByProfileIdAsync(long profileId)
        {
            return await _dbContext.LegalPeople.AsNoTracking().FirstOrDefaultAsync(row => row.ProfileId == profileId && !row.IsDeleted);
        }

        public async Task<LegalPerson> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            using (var db = _sqlConnectionProvider.GetReadConnection())
            {
                var cmd =
                    $"declare @profileId bigint select @profileId=Id from Profile (nolock) where UniqueIdentifier='{uniqueIdentifier}' "
                    + " SELECT * FROM dbo.LegalPerson (nolock) j LEFT JOIN dbo.Country b ON b.Id = j.CitizenshipCountryId WHERE j.ProfileId = @profileId";
                return (await db.QueryAsync< LegalPerson, Country, LegalPerson>(cmd, (person, country) =>
                    {
                        person.CitizenshipCountry = country;
                        return person;
                    } )).FirstOrDefault();
            }

        }
    }
}
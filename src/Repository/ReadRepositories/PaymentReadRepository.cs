﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Dapper;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class PaymentReadRepository : BaseReadRepository<Payment>, IPaymentReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;

        public PaymentReadRepository(ReadCerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) : base(db)
        {
            this._dbContext = db;
            _sqlConnectionProvider = sqlConnectionProvider;
        }

        public async Task<int> GetCountByDiscountIssuerAsync(long serviceId, DateTime? @from, DateTime? to)
        {
            to = to?.AddDays(1);

            return await
                _dbContext.Payments
                    .AsNoTracking().CountAsync(x => x.Status == PaymentStatus.Settle &&
                                     x.DiscountIssuer == serviceId &&
                                     (!from.HasValue || x.CreationDate >= from) &&
                                     (!to.HasValue || x.CreationDate <= to));

        }

   
        public async Task<Payment> GetByIdAsync(long id)
        {
            using (var con = _sqlConnectionProvider.GetReadConnection())
            {
                return await con.QueryFirstOrDefaultAsync<Payment>("Select * From payment where Id = @Id", new { id });
            }

        }
        public async Task<List<Payment>> GetSuspiciousPayments()
        {
            var date = DateTime.Now.AddMinutes(-15);
            var query = @"Select * 
                          From payment with(nolock)
                          where ([Status] = 2 OR  [Status] = 3) ";
            using (var con = _sqlConnectionProvider.GetReadConnection())
            {
                var res = await con.QueryAsync<Payment>(query);
                return res.Where(x => x.CreationDate <= date).ToList();
            }
        }
    }
}

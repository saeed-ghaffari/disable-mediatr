﻿using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Repository.MsSql;

namespace Cerberus.Repository.ReadRepositories
{
    public class NocrInquiryReadRepository : BaseReadRepository<NocrInquiry>, INocrInquiryReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;
        public NocrInquiryReadRepository(ReadCerberusDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
    }
}

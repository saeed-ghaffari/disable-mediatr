﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Inquiries;
using Cerberus.Repository.MsSql;
using Dapper;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class IssuanceStockCodeReadRepository : BaseReadRepository<IssuanceStockCode>, IIssuanceStockCodeReadRepository
    {
        private readonly ReadCerberusDbContext _db;
        private readonly ISqlConnectionProvider _connectionProvider;
        public IssuanceStockCodeReadRepository(ReadCerberusDbContext db, ISqlConnectionProvider connectionProvider) : base(db)
        {
            _db = db;
            _connectionProvider = connectionProvider;
        }

        /// <summary>
        /// get by profileId that refer to sejam profile
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="successful"></param>
        /// <returns></returns>
        public async Task<IssuanceStockCode> GetByProfileIdAsync(long profileId, bool successful)
        {
            return await _db.IssuanceStockCodes.AsNoTracking().Include(x => x.IssuanceStockCodeResponses)
                .FirstOrDefaultAsync(x => x.Successful == successful && x.ProfileId == profileId);
        }

        /// <summary>
        /// get by uniqueIdentifier that refer to national code
        /// </summary>
        /// <param name="uniqueIdentifier"></param>
        /// <param name="successful"></param>
        /// <returns></returns>
        public async Task<IssuanceStockCode> GetByUniqueIdentifierAsync(string uniqueIdentifier, bool successful)
        {
            return await _db.IssuanceStockCodes.AsNoTracking().Include(x => x.IssuanceStockCodeResponses)
                .FirstOrDefaultAsync(x => x.Successful == successful && x.UniqueIdentifier == uniqueIdentifier);
        }

        /// <summary>
        /// get profile for issuance stoke code that not successful before
        /// </summary>
        /// <param name="getCurrentDay"> is current day or previous days</param>
        /// <param name="mode"></param>
        /// <param name="divisor"></param>
        /// <returns></returns>
        public async Task<IEnumerable<IssuanceStokeCodeModel>> GetBatchAsync(bool getCurrentDay, byte mode, byte divisor)
        {
            //return await _db.IssuanceStockCodes.AsNoTracking().Where(x => !x.Successful).OrderBy(x => x.Id).Take(count).ToListAsync();
            using (var con = _connectionProvider.GetReadConnection())
            {
                var query = $"EXEC [sp_GetStockCodeUnsent]  @GroupCount, @GroupId, @IsToday  ";

                return await con.QueryAsync<IssuanceStokeCodeModel>(query, new
                {
                    IsToday = getCurrentDay,
                    GroupId = mode,
                    GroupCount = divisor
                });
            }
        }


        /// <summary>
        /// get info about person that need in issuance stoke code request
        /// </summary>
        /// <param name="uniqueIdentifier"></param>
        /// <returns></returns>
        public async Task<IssuanceStokeCodeModel> GetAllInfoAsync(string uniqueIdentifier)
        {
            using (var con = _connectionProvider.GetReadConnection())
            {
                var query =
                    $"SELECT [FirstName],[LastName],[FatherName],[SeriesNumber],[SerialNumber],[IdentificationNumber],[Gender],[Ssn],[IsAlive],[BirthDate],replace([PersianBirthDate],'/','') [PersianBirthDate],Row_number()OVER (PARTITION BY [Ssn] ORDER BY id DESC) AS [Rank]"
                    + $" INTO   #NocrInquery FROM   NocrInquiry (nolock) WHERE  Ssn='{uniqueIdentifier}' and SuccessInquiry = 1 AND IsAlive = 'True' "
                    + " SELECT p.UniqueIdentifier Nin, nc.FirstName,nc.LastName,nc.FatherName,nc.SeriesNumber,nc.SerialNumber,nc.IdentificationNumber,cast(Iif(nc.Gender = 1, 1, 0) as bit) [IsMale],nc.PersianBirthDate,pp.PlaceOfIssue [IssuePlace], "
                    + $"cast(Iif(nc.IsAlive = 'True', 1, 0) as bit) IsAlive FROM   Profile p (nolock) INNER JOIN PrivatePerson pp (nolock) ON p.id = pp.ProfileId INNER JOIN #NocrInquery nc ON p.UniqueIdentifier = nc.Ssn WHERE  p.[UniqueIdentifier] = '{uniqueIdentifier}' "
                    + " AND p.Status >= 6 AND nc.[Rank] = 1 DROP TABLE IF EXISTS #NocrInquery";
                    
                return (await con.QueryAsync<IssuanceStokeCodeModel>(query)).FirstOrDefault();
            }
        }

        public async Task<List<IssuanceStockCode>> GetListWithListIdAsync(List<long> id)
        {
            return await _db.IssuanceStockCodes.Where(x => id.Contains(x.Id)).ToListAsync();
        }
    }
}
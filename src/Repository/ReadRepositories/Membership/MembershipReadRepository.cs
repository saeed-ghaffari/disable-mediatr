﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories.Membership;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model.Membership;
using Cerberus.Repository.MsSql;
using Dapper;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories.Membership
{
    public class MembershipReadRepository : BaseReadRepository<PrivatePersonMembership>, IMembershipReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;
        public MembershipReadRepository(ReadCerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) : base(db)
        {
            this._dbContext = db;
            _sqlConnectionProvider = sqlConnectionProvider;
        }


        public async Task<PrivatePersonMembership> GetPrivatePersonAsync(string uniqueIdentifier, long serviceId)
        {

            return await _dbContext
                .PrivatePersonMembership
                .FromSql($"select top 1 * from mem.PrivatePersonMembership WITH (NOLOCK) where UniqueIdentifier=@UniqueIdentifier  and ServiceId=@ServiceId ",
                    new SqlParameter("@UniqueIdentifier", uniqueIdentifier),
                    new SqlParameter("@ServiceId", serviceId))
                .FirstOrDefaultAsync();
            // return await _dbContext.PrivatePersonMembership.FirstOrDefaultAsync(x => x.UniqueIdentifier == uniqueidentifier && x.Mobile == mobile && x.ServiceId == serviceId);
        }

        public async Task<AddressMembership> GetAddressAsync(string uniqueIdentifier, long serviceId)
        {
            return await _dbContext.AddressMembership.FromSql($"select top 1 * from mem.AddressMembership WITH (NOLOCK) where UniqueIdentifier=@UniqueIdentifier  and ServiceId=@ServiceId ", new SqlParameter("@UniqueIdentifier", uniqueIdentifier),
               new SqlParameter("@ServiceId", serviceId)).FirstOrDefaultAsync();
            // return await _dbContext.AddressMembership.FirstOrDefaultAsync(x => x.UniqueIdentifier == uniqueidentifier && x.ServiceId == serviceId);
        }

        public async Task<BankingAccountMembership> GetAccountAsync(string uniqueIdentifier, long serviceId)
        {
            return await _dbContext
                .BankingAccountMembership
                .FromSql($"select top 1 * from mem.BankingAccountMembership WITH (NOLOCK) where UniqueIdentifier=@UniqueIdentifier  and ServiceId=@ServiceId ",
                    new SqlParameter("@UniqueIdentifier", uniqueIdentifier),
                    new SqlParameter("@ServiceId", serviceId))
                .FirstOrDefaultAsync();
        }

        public async Task AddAsync(PrivatePersonMembership privatePerson, AddressMembership address, List<BankingAccountMembership> accounts)
        {
            await _dbContext.PrivatePersonMembership.AddAsync(privatePerson);
            if (address != null)
                await _dbContext.AddressMembership.AddAsync(address);
            if (accounts.Count != 0)
                await _dbContext.BankingAccountMembership.AddRangeAsync(accounts.ToArray());

            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<PrivatePersonMembership>> GetPrivatePeronListAsync(long fromRow, long toRow, string uniqueIdentifier)
        {

            using (var con = _sqlConnectionProvider.GetReadConnection())
            {
                var query =
                    "SELECT x.Id ,x.[UniqueIdentifier] ,x.[Mobile] ,x.[Discount] ,x.[ServiceId] ,x.[FirstName] ,x.[LastName] ,x.[FatherName] ,x.[Gender] ,x.[SeriShChar] ,x.[SeriSh] ,x.[Serial] ,x.[ShNumber] ,x.[BirthDate] ,x.[PlaceOfIssue] ,x.[PlaceOfBirth] ,x.[CreationDate] ,x.[IsDeleted] ,x.[ModifiedDate] ,x.[CanEditAccounts]  FROM ("
                    + $"SELECT ROW_NUMBER() OVER(ORDER BY Id) rw, * FROM mem.privatePersonMembership WITH(NOLOCK) where UniqueIdentifier is not null";

                if (uniqueIdentifier != null)
                    query += $" and UniqueIdentifier='{uniqueIdentifier}'";

                query += $" ) x WHERE x.rw>= {fromRow} AND x.rw<= {toRow}";

                return await con.QueryAsync<PrivatePersonMembership>(query);
            }
        }

        public async Task<IEnumerable<AddressMembership>> GetAddressListListAsync(long fromRow, long toRow, string uniqueIdentifier)
        {
            using (var con = _sqlConnectionProvider.GetReadConnection())
            {
                var query =
                    "SELECT x.[Id] ,x.[ServiceId] ,x.[UniqueIdentifier] ,x.[PostalCode] ,x.[CountryId] ,x.[ProvinceId] ,x.[CityId] ,x.[SectionId] ,x.[Address] ,x.[Tel] ,x.[CreationDate] ,x.[IsDeleted] ,x.[ModifiedDate]  FROM ("
                    + $"SELECT ROW_NUMBER() OVER(ORDER BY Id) rw, * FROM mem.AddressMembership WITH(NOLOCK) where UniqueIdentifier is not null";

                if (uniqueIdentifier != null)
                    query += $" and UniqueIdentifier='{uniqueIdentifier}'";

                query += $" ) x WHERE x.rw>= {fromRow} AND x.rw<= {toRow}";

                return await con.QueryAsync<AddressMembership>(query);
            }
        }

        public async Task<IEnumerable<BankingAccountMembership>> GetBankingAccountListAsync(long fromRow, long toRow, string uniqueIdentifier)
        {
            using (var con = _sqlConnectionProvider.GetReadConnection())
            {
                var query =
                    "SELECT x.[Id] ,x.[UniqueIdentifier] ,x.[AccountNumber] ,x.[ServiceId] ,x.[Type] ,x.[Sheba] ,x.[BankId] ,x.[BranchCode] ,x.[BranchName] ,x.[BranchCityId] ,x.[IsDefault] ,x.[IsDeleted] ,x.[CreationDate] ,x.[ModifiedDate]  FROM ("
                    + $"SELECT ROW_NUMBER() OVER(ORDER BY Id) rw, * FROM mem.BankingAccountMembership WITH(NOLOCK) where UniqueIdentifier is not null";

                if (uniqueIdentifier != null)
                    query += $" and UniqueIdentifier='{uniqueIdentifier}'";

                query += $" ) x WHERE x.rw>= {fromRow} AND x.rw<= {toRow}";

                return await con.QueryAsync<BankingAccountMembership>(query);
            }
        }

        public async Task<int> CountAsync(MembershipDataType type)
        {
            switch (type)
            {
                case MembershipDataType.PrivatePerson:
                    return await _dbContext.PrivatePersonMembership.CountAsync();
                case MembershipDataType.BankingAccount:
                    return await _dbContext.BankingAccountMembership.CountAsync();
                case MembershipDataType.Address:
                    return await _dbContext.AddressMembership.CountAsync();
                default:
                    return 0;
            }
        }
    }


}


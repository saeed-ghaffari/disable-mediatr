﻿using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Repository.MsSql;

namespace Cerberus.Repository.ReadRepositories
{
    public class DeadInquiryReadRepository : BaseReadRepository<DeadInquiry>, IDeadInquiryReadRepository
    {
        public DeadInquiryReadRepository(ReadCerberusDbContext db) : base(db)
        {
        }

      
    }
}

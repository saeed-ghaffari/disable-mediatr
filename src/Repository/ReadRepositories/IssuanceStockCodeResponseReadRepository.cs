﻿using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;

namespace Cerberus.Repository.ReadRepositories
{
    public class IssuanceStockCodeResponseReadRepository : BaseReadRepository<IssuanceStockCodeResponse>, IIssuanceStockCodeResponseReadRepository
    {
        public IssuanceStockCodeResponseReadRepository(ReadCerberusDbContext db) : base(db)
        {
        }
    }
}
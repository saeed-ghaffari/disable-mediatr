﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Dapper;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class AddressRepository : BaseRepository<Address>, IAddressRepository
    {
        private readonly CerberusDbContext _dbContext;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;
        public AddressRepository(CerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) : base(db)
        {
            this._dbContext = db;
            _sqlConnectionProvider = sqlConnectionProvider;
        }

        public async Task<Address> GetByProfileIdAsync(long profileId, bool getRelatedData = false)
        {
            if (getRelatedData)
            {
                using (var con = _sqlConnectionProvider.GetConnection())
                {
                    var result = await con.QueryMultipleAsync($"select ad.*,ad.Address as RemnantAddress,Country.*,Province.*,City.*,AddressSection.*" +
                                                              $" from Address ad (nolock) left outer join Country (nolock) on ad.CountryId=Country.Id left outer join Province (nolock) on ad.ProvinceId=Province.Id left outer join City (nolock) on ad.CityId=City.Id " +
                                                              $" left outer join AddressSection (nolock) on ad.SectionId=AddressSection.Id where ProfileId={profileId}");

                    var addresses = result.Read<Address, Country, Province, City, AddressSection, Address>(
                        (address, country, province, city, section) =>
                        {
                            if (address == null)
                                return null;
                            address.Country = country;
                            address.Province = province;
                            address.City = city;
                            address.Section = section;

                            return address;
                        })?.FirstOrDefault();
                    return addresses;
                }
            }

            return await _dbContext
                .Addresses
                .AsNoTracking()
                .FirstOrDefaultAsync(row => row.ProfileId == profileId);
        }

        public async Task<List<Address>> GetListByProfileIdAsync(long profileId)
        {

            using (var con = _sqlConnectionProvider.GetConnection())
            {
                var result = await con.QueryMultipleAsync($"select ad.*,ad.Address as RemnantAddress,Country.*,Province.*,City.*,AddressSection.*" +
                                                          $" from Address ad (nolock) left outer join Country (nolock) on ad.CountryId=Country.Id left outer join Province (nolock) on ad.ProvinceId=Province.Id left outer join City (nolock) on ad.CityId=City.Id " +
                                                          $" left outer join AddressSection (nolock) on ad.SectionId=AddressSection.Id where ProfileId={profileId}");

                var addresses = result.Read<Address, Country, Province, City, AddressSection, Address>(
                      (address, country, province, city, section) =>
                      {
                          if (address == null)
                              return null;
                          address.Country = country;
                          address.Province = province;
                          address.City = city;
                          address.Section = section;

                          return address;
                      })?.ToList();
                return addresses;
            }

        }

        public async Task<List<Address>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return await _dbContext
                .Addresses
                .Include(x => x.Country)
                .Include(x => x.Province)
                .Include(x => x.City)
                .Include(x => x.Section)
                .AsNoTracking()
                .Where(x => x.ProfileOwner.UniqueIdentifier.Equals(uniqueIdentifier)).ToListAsync();
        }
    }
}
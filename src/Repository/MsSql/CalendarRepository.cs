﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class CalendarRepository : BaseRepository<Calendar>, ICalendarRepository
    {
        private readonly CerberusDbContext _db;
        public CalendarRepository(CerberusDbContext db) : base(db)
        {
            _db = db;
        }

        public async Task<bool> TodayIsHoliday()
        {
            return await _db.Calendars.AsNoTracking().AnyAsync(x => x.IsHoliday && x.GregorianDate.Date == DateTime.Now.Date);
        }
    }
}
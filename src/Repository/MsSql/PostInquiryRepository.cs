﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Repository.MsSql
{
    public class PostInquiryRepository : BaseRepository<PostInquiry>, IPostInquiryRepository
    {
        public PostInquiryRepository(CerberusDbContext db) : base(db)
        {
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class ProvinceRepository : BaseRepository<Province>, IProvinceRepository
    {
        private readonly CerberusDbContext _dbContext;

        public ProvinceRepository(CerberusDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Province>> GetListAsync(long countryId)
        {
            return await _dbContext.Provinces.AsNoTracking().Where(x => x.CountryId == countryId).ToListAsync();
        }

        public async Task<IEnumerable<Province>> GetListAsync()
        {
            return await _dbContext.Provinces.AsNoTracking().ToListAsync();
        }
    }
}
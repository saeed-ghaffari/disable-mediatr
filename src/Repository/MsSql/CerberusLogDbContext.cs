﻿using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Log;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class CerberusLogDbContext : DbContext
    {
        public CerberusLogDbContext(DbContextOptions<CerberusLogDbContext> options) : base(options)
        {

        }

        public DbSet<ApiRequestLog> ApiRequestLogs { get; set; }

        public DbSet<CallbackThirdPartyServicesLog> CallbackThirdPartyServicesLogs { get; set; }
        public DbSet<WebRequestLog> WebRequestLogs { get; set; }

    }
}
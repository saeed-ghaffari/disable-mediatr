﻿using System;
using System.Collections.Generic;
using System.Text;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;

namespace Cerberus.Repository.MsSql
{
    public class AgentDocumentArchiveRepository:BaseRepository<AgentDocumentArchive>,IAgentDocumentArchiveRepository
    {
        public AgentDocumentArchiveRepository(CerberusDbContext db) : base(db)
        {
        }
    }
}

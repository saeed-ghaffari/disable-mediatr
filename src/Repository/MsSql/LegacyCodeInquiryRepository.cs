﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Repository.MsSql
{
    public class LegacyCodeInquiryRepository : BaseRepository<LegacyCodeInquiry>, ILegacyCodeInquiryRepository
    {
        public LegacyCodeInquiryRepository(CerberusDbContext dbContext) : base(dbContext)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Dapper;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class PrivatePersonRepository : BaseRepository<PrivatePerson>, IPrivatePersonRepository
    {
        private readonly CerberusDbContext _dbContext;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;
        public PrivatePersonRepository(CerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) : base(db)
        {
            this._dbContext = db;
            _sqlConnectionProvider = sqlConnectionProvider;
        }

        public async Task<PrivatePerson> GetByProfileIdAsync(long profileId)
        {
            return await _dbContext.PrivatePeople.AsNoTracking().FirstOrDefaultAsync(row => row.ProfileId == profileId);
        }

        public async Task<PrivatePerson> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {

            using (var db = _sqlConnectionProvider.GetConnection())
            {
                var cmd =
                    $"declare @profileId bigint select @profileId=Id from Profile (nolock) where UniqueIdentifier='{uniqueIdentifier}' "
                    + " SELECT * FROM dbo.PrivatePerson (NOLOCK) j LEFT JOIN dbo.[File] b ON b.Id = j.SignatureFileId LEFT JOIN dbo.[File] img (nolock) ON img.Id=j.ImageId WHERE j.ProfileId = @profileId";
                return (await db.QueryAsync<PrivatePerson, File, File, PrivatePerson>(cmd, (person, signature,image) =>
                {
                    person.SignatureFile = signature;
                    person.ImageFile = image;
                    return person;
                })).FirstOrDefault();
            }
            
        }

        public async Task<IEnumerable<PrivatePerson>> GetByDaysLeftToEighteenAsync(int days,int status)
        {
            using (var connection=_sqlConnectionProvider.GetConnection())
            {
                return await connection.QueryAsync<PrivatePerson, Profile, Agent, PrivatePerson>
                ($"SELECT * FROM dbo.PrivatePerson (NOLOCK) p JOIN dbo.Profile (NOLOCK) pf ON pf.Id = p.ProfileId " +
                  $"JOIN dbo.Agent a ON a.ProfileId=pf.id "
                +$"WHERE DATEDIFF(DAY, GETDATE(), DATEADD(YEAR, 18, BirthDate)) = {days} AND pf.Status >= {status}",
                    (prv, p, a) =>
                    {
                        prv.ProfileOwner = p;
                        prv.ProfileOwner.Agent = a;
                        return prv;
                    });
            }
        }
    }
}
﻿using System.Data.SqlClient;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class FileRepository : BaseRepository<File>, IFileRepository
    {
        private readonly CerberusDbContext _dbContext;

        public FileRepository(CerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }
        public async Task DeleteAsync(long id)
        {
            await _dbContext.Database.ExecuteSqlCommandAsync($"delete [File] where id=@id", new SqlParameter("@id", id));
        }
    }
}
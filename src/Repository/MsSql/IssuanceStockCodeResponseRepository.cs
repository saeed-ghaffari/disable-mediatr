﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;

namespace Cerberus.Repository.MsSql
{
    public class IssuanceStockCodeResponseRepository : BaseRepository<IssuanceStockCodeResponse>, IIssuanceStockCodeResponseRepository
    {
        public IssuanceStockCodeResponseRepository(CerberusDbContext db) : base(db)
        {
        }
    }
}
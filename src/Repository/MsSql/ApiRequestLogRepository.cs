﻿using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Model.Log;
using Dapper.FastCrud;

namespace Cerberus.Repository.MsSql
{
    public class ApiRequestLogRepository : IApiRequestLogRepository
    {
        private readonly ISqlConnectionProvider _connectionProvider;

        public ApiRequestLogRepository(ISqlConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public async Task AddAsync(ApiRequestLog log)
        {
            using (var con=_connectionProvider.GetConnection())
            {
                await con.InsertAsync(log);
            }
        }
    }
}
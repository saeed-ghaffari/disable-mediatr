﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;

namespace Cerberus.Repository.MsSql
{
    public class MsisdnRepository : BaseRepository<Msisdn>, IMsisdnRepository
    {
     
        public MsisdnRepository(CerberusDbContext dbContext) : base(dbContext)
        {
        }

     
    }
}

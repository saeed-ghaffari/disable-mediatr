﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class BrokerRepository : BaseRepository<Broker>, IBrokerRepository
    {
        private readonly CerberusDbContext _dbContext;

        public BrokerRepository(CerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<IEnumerable<Broker>> GetListAsync()
        {
            return await _dbContext.Brokers.AsNoTracking().ToListAsync();
        }
    }
}
﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Repository.MsSql
{
    public class IbanInquiryRepository : BaseRepository<IbanInquiry>, IIbanInquiryRepository
    {
        public IbanInquiryRepository(CerberusDbContext dbContext) : base(dbContext)
        {
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class ProfilePackageRepository : BaseRepository<ProfilePackage>, IProfilePackageRepository
    {
        private readonly CerberusDbContext _dbContext;

        public ProfilePackageRepository(CerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<ProfilePackage> GetUnusedPackageAsync(long profileId, PackageType type,PackageUsedFlag usePackage)
        {
           
            var pack = await _dbContext
                .Payments
                .Join(
                    _dbContext.ProfilePackages,
                    payment => payment.ProfilePackageId,
                    package => package.Id,
                    ((payment, package) => new {package, payment})
                )
                .Where(x =>
                    x.payment.Status == PaymentStatus.Settle &&
                    x.payment.ProfileId == profileId &&
                    (
                        x.package.Type == type)
                    && (x.package.ExpirationDate > DateTime.Now)
                )
                .Select(x => x.package)
                .ToListAsync();
            return pack
                ?.Where(x => !x.Used && x.HasPermission(usePackage))
                .FirstOrDefault();
        }


        public async Task<IEnumerable<ProfilePackage>> AllPackagesByProfileId(long profileId, PackageType type)
        {
            return await _dbContext
                .Payments
                .Join(
                    _dbContext.ProfilePackages,
                    payment => payment.ProfilePackageId,
                    package => package.Id,
                    ((payment, package) => new { package, payment })
                )
                .Where(x =>
                    x.payment.Status == PaymentStatus.Settle &&
                    x.payment.ProfileId == profileId &&
                    (
                        x.package.Type == type)
                 )
                .Select(x => x.package)
                .ToListAsync();
        
        }
    }
}

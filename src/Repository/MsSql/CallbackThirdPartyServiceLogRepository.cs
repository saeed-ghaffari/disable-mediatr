﻿using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Model.Log;

namespace Cerberus.Repository.MsSql
{
    public class CallbackThirdPartyServiceLogRepository : ICallbackThirdPartyServiceLogRepository
    {
        private readonly CerberusLogDbContext _dbContext;

        public CallbackThirdPartyServiceLogRepository(CerberusLogDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddAsync(CallbackThirdPartyServicesLog log)
        {
            await _dbContext.CallbackThirdPartyServicesLogs.AddAsync(log);
            await _dbContext.SaveChangesAsync();
        }
    }
}
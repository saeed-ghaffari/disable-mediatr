﻿using System;
using System.Collections.Generic;
using System.Text;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;

namespace Cerberus.Repository.MsSql
{
   public class AgentArchiveRepository:BaseRepository<AgentArchive>,IAgentArchiveRepository
    {
        public AgentArchiveRepository(CerberusDbContext db) : base(db)
        {
        }
    }
}

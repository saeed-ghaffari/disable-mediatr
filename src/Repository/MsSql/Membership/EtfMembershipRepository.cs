﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories.Membership;
using Cerberus.Domain.Model.Membership;
using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql.Membership
{
  public  class EtfMembershipRepository:BaseRepository<EtfMembership>,IEtfMembershipRepository
    {
        private readonly CerberusDbContext _dbContext;

        public EtfMembershipRepository(CerberusDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> CheckExistAsync(string uniqueIdentifier)
        {
            return await _dbContext.EtfMembership.AnyAsync(x => x.UniqueIdentifier == uniqueIdentifier);
        }

        public async Task BulkCreateAsync(List<EtfMembership> memberships)
        {
             await _dbContext.BulkInsertAsync(memberships);
        }
    }
}

﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Repository.MsSql
{
    public class BankRepository : BaseRepository<Bank>, IBankRepository
    {
        private readonly CerberusDbContext _dbContext;
        public BankRepository(CerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<List<Bank>> GetListAsync()
        {
            return await _dbContext.Banks.AsNoTracking().ToListAsync();
        }

        public async Task<List<Bank>> GetListForAuthenticationOfficeAsync()
        {
            var c = from b in _dbContext.Banks
                join au in _dbContext.AuthenticationOfficeses
                    on b.Id equals au.BankId
                select b;

            return await c.AsNoTracking().Distinct().ToListAsync();
        }
    }
}
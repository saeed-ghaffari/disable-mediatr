﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class CountryRepository : BaseRepository<Country>, ICountryRepository
    {
        private readonly CerberusDbContext _dbContext;

        public CountryRepository(CerberusDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<Country>> GetListAsync()
        {
            return await _dbContext.Countries.AsNoTracking().ToListAsync();
        }
    }
}
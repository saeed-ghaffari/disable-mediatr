﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Dapper;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class PaymentRepository : BaseRepository<Payment>, IPaymentRepository
    {
        private readonly CerberusDbContext _dbContext;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;

        public PaymentRepository(CerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) : base(db)
        {
            this._dbContext = db;
            _sqlConnectionProvider = sqlConnectionProvider;
        }


        public async Task SetFactorIdRepository(long paymentId, long factorId, string serialNumber)
        {
            using (var con = _sqlConnectionProvider.GetConnection())
            {
                await con.ExecuteAsync($" update payment set FactorId={factorId} , SerialNumber='{serialNumber}' , ModifiedDate=getDate() where id={paymentId}");
            }
            //await _dbContext.Database.ExecuteSqlCommandAsync($" update payment set FactorId={factorId} , SerialNumber={serialNumber} , ModifiedDate=getDate() where id={paymentId}");

        }

        public async Task<int> GetCountByDiscountIssuerAsync(long serviceId, DateTime? @from, DateTime? to)
        {
            to = to?.AddDays(1);

            return await
                _dbContext.Payments
                    .AsNoTracking().CountAsync(x => x.Status == PaymentStatus.Settle &&
                                     x.DiscountIssuer == serviceId &&
                                     (!from.HasValue || x.CreationDate >= from) &&
                                     (!to.HasValue || x.CreationDate <= to));

        }

        public async Task<Payment> GetPaymentAsync(long id)
        {
            return await _dbContext.Payments.AsNoTracking().Include(x => x.Package).SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task UpdateIsConfirmedReferenceAsync(long paymentId)
        {
            using (var con = _sqlConnectionProvider.GetConnection())
            {
                await con.ExecuteAsync($" update payment set IsConfirmedReference=1,ModifiedDate=getDate() where id={paymentId}");
            }
        }

        public async Task<Payment> GetByIdAsync(long id)
        {
            using (var con = _sqlConnectionProvider.GetConnection())
            {
                return await con.QueryFirstOrDefaultAsync<Payment>("Select * From payment where Id = @Id", new { id });
            }

        }
        public async Task<List<Payment>> GetSuspiciousPayments()
        {
            var date = DateTime.Now.AddMinutes(-15);
            var query = @"Select * 
                          From payment with(nolock)
                          where ([Status] = 2 OR  [Status] = 3) ";
            using (var con = _sqlConnectionProvider.GetConnection())
            {
                var res = await con.QueryAsync<Payment>(query);
                return res.Where(x => x.CreationDate <= date).ToList();
            }
        }
        public async Task UpdateSettledPaymentsAsync(long Id, string SaleReferenceId, string ReferenceNumber)
        {

            using (var con = _sqlConnectionProvider.GetConnection())
            {
                await con.ExecuteAsync($@"Update [dbo].Payment 
                                Set [Status] = {(byte)PaymentStatus.Settle},
                                [SaleReferenceId] = N'{SaleReferenceId}',
                                [ReferenceNumber] = '{ReferenceNumber}'
                            Where Id = {Id}");
            }

        }
        public async Task UpdateSettleFailedPaymentsAsync(long Id)
        {
            using (var con = _sqlConnectionProvider.GetConnection())
            {
                await con.ExecuteAsync($"Update [dbo].Payment Set [Status] = {(byte)PaymentStatus.SettleFailed}  Where Id = {Id}");
            }

        }
    }
}

﻿using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Model;
using Dapper.FastCrud;

namespace Cerberus.Repository.MsSql
{
    public class ProfilingRequestLogRepository : IProfilingRequestLogRepository
    {
        private readonly ISqlConnectionProvider _connectionProvider;

        public ProfilingRequestLogRepository(ISqlConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public async Task AddAsync(ProfilingRequestLog log)
        {
            using (var con = _connectionProvider.GetRequestLogConnection())
            {
                await con.InsertAsync(log);
            }
        }
    }
}
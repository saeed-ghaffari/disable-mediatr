﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Repository.MsSql
{
    public class AddressSectionRepository : BaseRepository<AddressSection>, IAddressSectionRepository
    {
        private readonly CerberusDbContext _dbContext;

        public AddressSectionRepository(CerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

       
    }
}
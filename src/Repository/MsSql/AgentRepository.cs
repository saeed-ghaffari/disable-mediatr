﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class AgentRepository : BaseRepository<Agent>, IAgentRepository
    {
        private readonly CerberusDbContext _dbContext;

        public AgentRepository(CerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<Agent> GetByProfileIdAsync(long profileId)
        {
            return await _dbContext.Agents.Include(x=>x.AgentProfile).ThenInclude(x=>x.PrivatePerson)
                .AsNoTracking()
                .FirstOrDefaultAsync(agent => agent.ProfileId == profileId);
        }

        public async Task<Agent> GetByAgentProfileIdAsync(long agentProfileId)
        {
            return await _dbContext.Agents.AsNoTracking()
                .FirstOrDefaultAsync(agent => agent.AgentProfileId == agentProfileId);
        }

        public async Task DeleteAsync(long id)
        {
            await _dbContext.Database.ExecuteSqlCommandAsync($"delete Agent where id=@id", new SqlParameter("@id", id));
        }

        public async Task<List<Agent>> GetListByAgentProfileIdAsync(long agentProfileId)
        {
                return await _dbContext.Agents.AsNoTracking().Include(x=>x.ProfileOwner).ThenInclude(x=>x.PrivatePerson)
                .Where(agent => agent.AgentProfileId == agentProfileId).ToListAsync(); 
        }
    }
}
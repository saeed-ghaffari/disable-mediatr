﻿using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Model.Log;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Dapper.FastCrud;

namespace Cerberus.Repository.MsSql.Log
{
    public class SmsLogRepository : ISmsLogRepository
    {
        private readonly ISqlConnectionProvider _sqlConnectionProvider;

        public SmsLogRepository(ISqlConnectionProvider sqlConnectionProvider)
        {
            _sqlConnectionProvider = sqlConnectionProvider;
        }


        public async Task AddAsync(SmsLog log)
        {
            using (var con = _sqlConnectionProvider.GetConnection())
            {
                await con.InsertAsync(log);
            }
        }
    }
}

﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Repository.MsSql
{
    public class CityRepository : BaseRepository<City>, ICityRepository
    {
        private readonly CerberusDbContext _dbContext;

        public CityRepository(CerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<List<City>> GetListAsync(long provinceId)
        {
            return await _dbContext.Cities.AsNoTracking().Where(x => x.ProvinceId == provinceId).ToListAsync();
        }

        public async Task<List<City>> GetListAsync()
        {
            return await _dbContext.Cities.AsNoTracking().ToListAsync();
        }

    }
}
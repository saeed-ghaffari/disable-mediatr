﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Repository.MsSql
{
    public class LegacyCodeResponseRepository : BaseRepository<LegacyCodeResponse>, ILegacyCodeResponseRepository
    {
        public LegacyCodeResponseRepository(CerberusDbContext dbContext) : base(dbContext)
        {
        }
    }
}
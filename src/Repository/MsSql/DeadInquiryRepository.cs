﻿using System;
using System.Collections.Generic;
using System.Text;
using Cerberus.Domain.Interface.Repositories.Inquiries;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Repository.MsSql
{
    public class DeadInquiryRepository : BaseRepository<DeadInquiry>, IDeadInquiryRepository
    {
        public DeadInquiryRepository(CerberusDbContext db) : base(db)
        {
        }
    }
}

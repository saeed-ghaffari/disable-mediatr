﻿using System;
using Cerberus.TasksManager.Core.Enum;

namespace Cerberus.TasksManager.Core.Model
{
    public class ScheduledTask
    {
        public long Id { get; set; }

        public long GroupId { get; set; }

        public long? RefId { get; set; }

        public string Type { get; set; }

        public TaskStatus Status { get; set; }

        public string Params { get; set; }

        public DateTime DueTime { get; set; }

        /// <summary>
        /// Execution time in milliseconds
        /// </summary>
        public long ExecuteTime { get; set; }

        public DateTime FinishTime { get; set; }

        public DateTime CreationTime { get; set; }

        public DateTime? LastRun { get; set; }

        public long? RecurringTaskId { get; set; }

        public TaskGroup Group { get; set; }

        /// <summary>
        /// This is filled in case of a recovered task
        /// </summary>
        public long? OriginalTaskId { get; set; }

        public int RecoverySequence { get; set; }

        public override string ToString()
        {
            return $"{Type}(Id={Id}) in group {Group}";
        }
    }
}
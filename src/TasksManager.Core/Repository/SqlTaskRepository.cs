﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Dapper;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.TasksManager.Core.Repository
{
    public class SqlTaskRepository : ITaskRepository
    {
        public void CleanupTasks(int timeoutWindowSeconds)
        {
            using (var con = Connection.GetConnection())
            {
                con.Execute("EXEC tsk.SP_CleanupTasks @Since",
                    new { Since = DateTime.Now.AddSeconds(-timeoutWindowSeconds) });
            }
        }

        public IEnumerable<ScheduledTask> GetPendingTasks(long groupId, int count)
        {
            IList<TaskGroup> taskGroups;
            IList<ScheduledTask> scheduledTasks;

            using (var con = Connection.GetConnection())
            using (var multi = con.QueryMultiple($"EXEC tsk.SP_GetPendingTasks {groupId},{count}"))
            {
                var tasks = multi.Read<ScheduledTask>();
                var groups = multi.Read<TaskGroup>();

                taskGroups = groups as IList<TaskGroup> ?? groups.ToList();
                scheduledTasks = tasks as IList<ScheduledTask> ?? tasks.ToList();
            }

            foreach (var task in scheduledTasks)
            {
                task.Group = taskGroups.First(g => g.Id == task.GroupId);
            }
            return scheduledTasks;
        }

        public IEnumerable<TaskGroup> GetTaskGroups(int runnerId)
        {
            using (var con = Connection.GetConnection())
            {
                return con.Query<TaskGroup>($"EXEC tsk.SP_GetTaskGroups {runnerId}");
            }
        }

        public void Update(ScheduledTask task)
        {
            using (var con = Connection.GetConnection())
            {
                con.Execute("EXEC tsk.SP_UpdateTaskStatus @Id,@Status,@ExecuteTime,@LastRun",
                    new
                    {
                        task.Id,
                        task.Status,
                        task.ExecuteTime,
                        task.LastRun
                    });
            }
        }

        public async Task<ScheduledTask> AddAsync(ScheduledTask task, bool dontAddDuplicates = false)
        {
            using (var con = Connection.GetConnection())
            using (var multi = await con.QueryMultipleAsync("EXEC tsk.SP_AddTask @GroupId,@Type,@Params,@DueTime,@NoDuplicates,@RefId,@OriginalTaskId,@RecoverySequence,@RecurringTaskId",
                    new
                    {
                        task.GroupId,
                        task.Type,
                        task.Params,
                        task.DueTime,
                        NoDuplicates = dontAddDuplicates,
                        task.RefId,
                        task.OriginalTaskId,
                        task.RecoverySequence,
                        task.RecurringTaskId
                    }))
            {
                var result = multi.Read<ScheduledTask>().FirstOrDefault();

                if (result == null)
                    return null;

                result.Group = multi.Read<TaskGroup>().FirstOrDefault();

                return result;
            }
        }

        public async Task<ScheduledTask> GetAsync(long taskId)
        {
            using (var con = Connection.GetConnection())
            using (var multi = await con.QueryMultipleAsync("EXEC tsk.SP_GetTaskById " + taskId))
            {
                var result = multi.Read<ScheduledTask>().FirstOrDefault();

                if (result == null)
                    return null;

                result.Group = multi.Read<TaskGroup>().FirstOrDefault();

                return result;
            }
        }


        public async Task<IEnumerable<ScheduledTask>> GetAsync(string type, string param)
        {
            IList<TaskGroup> taskGroups;
            IList<ScheduledTask> scheduledTasks;

            using (var con = Connection.GetConnection())
            using (var multi = await con.QueryMultipleAsync("EXEC tsk.SP_GetTasksByParams @type,@param",
                    new
                    {
                        type,
                        param
                    }))
            {
                var tasks = multi.Read<ScheduledTask>();
                var groups = multi.Read<TaskGroup>();

                taskGroups = groups as IList<TaskGroup> ?? groups.ToList();
                scheduledTasks = tasks as IList<ScheduledTask> ?? tasks.ToList();
            }

            foreach (var task in scheduledTasks)
            {
                task.Group = taskGroups.First(g => g.Id == task.GroupId);
            }
            return scheduledTasks;
        }

        public async Task DeleteAsync(long taskId)
        {
            using (var con = Connection.GetConnection())
            {
                await con.ExecuteAsync("EXEC tsk.SP_DeleteTask " + taskId);
            }
        }

        public async Task<ScheduledTask> RecoverTask(ScheduledTask task, TimeSpan recoveryDelay)
        {
            task.Status = TaskStatus.Recovered;
            return await AddAsync(new ScheduledTask
            {
                GroupId = task.GroupId,
                Type = task.Type,
                Params = task.Params,
                DueTime = DateTime.Now.Add(recoveryDelay),
                OriginalTaskId = task.Id,
                RecoverySequence = task.RecoverySequence + 1
            });
        }

        public async Task<IEnumerable<ScheduledTask>> GetTaskListAsync(long refId, long groupId, int status)
        {
            using (var con = Connection.GetConnection())
            {
             return  await con.QueryAsync<ScheduledTask>("EXEC tsk.SP_GetTaskList @refId,@status,@groupId", new {refId,status,groupId});
            }
            
        }
    }
}
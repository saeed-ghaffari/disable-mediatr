﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Dapper;

namespace Cerberus.TasksManager.Core.Repository
{
    public class SqlRecurringTaskRepository : IRecurringTaskRepository
    {
        public async Task<RecurringTask> AddAsync(RecurringTask task)
        {
            using (var con = Connection.GetConnection())
            {
                return await con.QuerySingleOrDefaultAsync<RecurringTask>(
                    "EXEC tsk.SP_AddRecurringTask @GroupId,@Type,@Params,@Interval,@RefId,@NextExecution,@IntervalPeriod",
                    new
                    {
                        task.GroupId,
                        task.Type,
                        task.Params,
                        task.Interval,
                        task.IntervalPeriod,
                        task.RefId,
                        task.NextExecution
                    });
            }
        }

        public async Task<RecurringTask> GetAsync(long taskId)
        {
            using (var con = Connection.GetConnection())
            {
                return await con.QuerySingleOrDefaultAsync<RecurringTask>("EXEC tsk.SP_GetRecurringTaskById " + taskId);
            }
        }

        public RecurringTask Get(long taskId)
        {
            using (var con = Connection.GetConnection())
            {
                return con.QuerySingleOrDefault<RecurringTask>("EXEC tsk.SP_GetRecurringTaskById " + taskId);
            }
        }

        public async Task<RecurringTask> UpdateAsync(RecurringTask task)
        {
            using (var con = Connection.GetConnection())
            {
                return await con.QuerySingleOrDefaultAsync<RecurringTask>(
                    @"EXEC tsk.SP_UpdateRecurringTask @Id,@IsEnabled,@Status,@Interval,
@LastTaskId,@LastTaskStatus,@LastExecution,@NextExecution,@IntervalPeriod",
                    new
                    {
                        task.Id,
                        task.IsEnabled,
                        task.Status,
                        task.Interval,
                        task.IntervalPeriod,
                        task.LastTaskId,
                        task.LastTaskStatus,
                        task.LastExecution,
                        task.NextExecution
                    });
            }
        }

        public RecurringTask Update(RecurringTask task)
        {
            using (var con = Connection.GetConnection())
            {
                return con.QuerySingleOrDefault<RecurringTask>(
                    @"EXEC tsk.SP_UpdateRecurringTask @Id,@IsEnabled,@Status,@Interval,
@LastTaskId,@LastTaskStatus,@LastExecution,@NextExecution,@IntervalPeriod",
                    new
                    {
                        task.Id,
                        task.IsEnabled,
                        task.Status,
                        task.Interval,
                        task.IntervalPeriod,
                        task.LastTaskId,
                        task.LastTaskStatus,
                        task.LastExecution,
                        task.NextExecution
                    });
            }
        }
        public async Task DeleteAsync(long taskId)
        {
            using (var con = Connection.GetConnection())
            {
                await con.ExecuteAsync("EXEC tsk.SP_DeleteRecurringTask " + taskId);
            }
        }

        public async Task<IEnumerable<RecurringTask>> GetActiveTasks(int runnerId)
        {
            using (var con = Connection.GetConnection())
            {
                return await con.QueryAsync<RecurringTask>("EXEC tsk.SP_GetActiveRecurringTasks @runnerId", new { runnerId });
            }
        }

        public async Task<IEnumerable<RecurringTask>> GetAsync(string type, long refId, string param = null)
        {
            using (var con = Connection.GetConnection())
            {
                return await con.QueryAsync<RecurringTask>("EXEC tsk.SP_GetRecurringTaskByRef @type,@refId,@param",
                    new
                    {
                        type,
                        refId,
                        param
                    });
            }
        }

#if COMPILING_ON_NETSTANDARD
        public Task<IEnumerable<RecurringTask>> GetAsync(string type, IEnumerable<long> refIds)
        {
            // todo: not implemented on netstandard
            throw new NotSupportedException();
        }
#else

        public Task<IEnumerable<RecurringTask>> GetAsync(string type, IEnumerable<long> refIds)
        {
            //var dt = new DataTable();

            //dt.Columns.Add("Id", typeof(long));

            //foreach (var refId in refIds)
            //{
            //    dt.Rows.Add(refId);
            //}

            //using (var con = Connection.GetConnection())
            //{
            //    return await con.QueryAsync<RecurringTask>("EXEC tsk.SP_GetRecurringTasksByRefs @type,@refIds",
            //        new
            //        {
            //            type,
            //            refIds = dt.AsTableValuedParameter("tsk.RefId")
            //        });
            //}
            throw new NotImplementedException();

        }

#endif


        public async Task EnableTaskAsync(long taskId, bool value)
        {
            using (var con = Connection.GetConnection())
            {
                await con.ExecuteAsync(string.Format("EXEC tsk.SP_EnableTask {0},{1}", taskId, value));
            }
        }
    }
}

﻿using System;
using Cerberus.TasksManager.Core.Enum;
using Cerberus.TasksManager.Core.Model;

namespace Cerberus.TasksManager.Core
{
    public class TaskOperationEventArgs : EventArgs
    {
        public ScheduledTask Task { get; private set; }

        public TaskStatus TaskStatus { get; private set; }

        public Exception Exception { get; private set; }

        public long Duration { get; private set; }

        public TaskOperationEventArgs(ScheduledTask task, TaskStatus taskStatus)
            : this(task, taskStatus, null, 0)
        { }

        public TaskOperationEventArgs(ScheduledTask task, TaskStatus taskStatus, Exception exception, long duration = 0)
        {
            Task = task;
            Exception = exception;
            TaskStatus = taskStatus;
            Duration = duration;
        }
    }
}
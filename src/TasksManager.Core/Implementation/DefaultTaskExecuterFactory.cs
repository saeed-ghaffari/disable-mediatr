﻿using System;
using System.Collections.Concurrent;
using Cerberus.TasksManager.Core.Delegate;
using Cerberus.TasksManager.Core.Interface;

namespace Cerberus.TasksManager.Core.Implementation
{
    public class PooledTaskExecuterFactory : ITaskExecuterFactory
    {
        private readonly ConcurrentDictionary<string, CreateTaskExecuter> _constructors;
        private readonly ConcurrentDictionary<string, ITaskExecuter> _instances;

        public PooledTaskExecuterFactory()
        {
            _constructors = new ConcurrentDictionary<string, CreateTaskExecuter>();
            _instances = new ConcurrentDictionary<string, ITaskExecuter>();
        }

        public void RegisterConstructor(string type, CreateTaskExecuter ctor)
        {
            type = type.ToLowerInvariant();
            if (_constructors.ContainsKey(type))
                throw new Exception("Constructor is already registered.");
            _constructors.TryAdd(type, ctor);
        }

        public ITaskExecuter Create(string type)
        {
            type = type.ToLowerInvariant();

            if (!_constructors.ContainsKey(type))
                throw new Exception("Constructor not registered.");

            // return an instance from the pool if exists
            if (_instances.ContainsKey(type))
                return _instances[type];

            var executer = _constructors[type](type);

            // put the instance in the pool if it's reusable
            if (executer.IsReusable)
                _instances.TryAdd(type, executer);

            return executer;
        }
    }
}
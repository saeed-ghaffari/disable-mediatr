﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Cerberus.TasksManager.Core.Delegate;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.TasksManager.Core.Implementation
{
    public class TaskGroupManager : IDisposable
    {
        private class TaskState
        {
            public ScheduledTask ScheduledTask { get; set; }

            public Stopwatch Stopwatch { get; set; }
        }

        private static readonly NLog.ILogger Logger = NLog.LogManager.GetCurrentClassLogger();

        private readonly ITaskExecuterFactory _executerFactory;
        private Thread _queueManagerThread;

        private BlockingCollection<Task> _tasks;
        private Semaphore _semaphore;
        private long _queuedTasks;
        private long _runningTasks;
        private long _releasedResources;
        private object _concurrencyLock;

        public TaskGroup Group { get; private set; }
        public int ConcurrencyLevel { get; private set; }

        public event TaskEventHandler OnTaskQueued;
        public event TaskEventHandler OnTaskExecuting;
        public event TaskEventHandler OnTaskExecuted;
        public event TaskEventHandler OnTaskException;

        public TaskGroupManager(TaskGroup taskGroup, int concurrencyLevel, ITaskExecuterFactory executerFactory)
        {
            Group = taskGroup;
            _executerFactory = executerFactory;
            ConcurrencyLevel = concurrencyLevel;

            if (!Init())
                Logger.Error("TaskGroup {0}({1}) failed to initialize.", Group.Title, Group.Id);
        }

        public void Dispose()
        {
            try
            {
                _tasks.CompleteAdding();
                _tasks.Dispose();
            }
            catch
            {
                // ignore
            }
        }

        public void Enqueue(ScheduledTask job)
        {
            var executer = _executerFactory.Create(job.Type);
            if (!executer.CanHandleTask(job.Params))
            {
                // bad params
                RaiseOnTaskException(job, TaskStatus.BadParams, new ArgumentException("Bad params: " + job.Params));
                return;
            }
            var task = GetExtendedTask(executer, job);


            RaiseOnTaskQueued(job); // :(
            _tasks.Add(task);
            Interlocked.Increment(ref _queuedTasks);

            ReleaseResource();
        }

        public int GetQueueLength()
        {
            return _tasks.Count;
        }

        private bool Init()
        {
            _tasks = new BlockingCollection<Task>();
            _semaphore = new Semaphore(0, Group.ConcurrencyLevel);
            _queuedTasks = 0;
            _runningTasks = 0;
            _releasedResources = 0;
            _concurrencyLock = new object();

            // init main thread
            _queueManagerThread = new Thread(DoWork)
            {
                Name = string.Format("Group {0}[{1}]", Group.Title, Group.Id)
            };
            _queueManagerThread.Start();

            return true;
        }

        private void DoWork()
        {
            Logger.Info("Thread started: " + Thread.CurrentThread.Name);

            while (_semaphore.WaitOne())
            {
                Task task;
                if (!_tasks.TryTake(out task))
                {
                    Interlocked.Decrement(ref _releasedResources);
                    continue;
                }

                // a null task is a termination signal
                // note: we don't want to terminate a task in the middle of the work
                if (_tasks == null)
                {
                    break;
                }

                // process task
                try
                {
                    Interlocked.Increment(ref _runningTasks);
                    var state = task.AsyncState as TaskState;
                    if (state != null)
                    {
                        RaiseOnTaskExecuting(state.ScheduledTask);
                        state.Stopwatch = new Stopwatch();
                    }

                    task.Start();
                }
                catch (Exception ex)
                {
                    Logger.Error("Exception raised while processing task " + task);
                    Logger.Error(ex);
                }
                finally
                {
                    Interlocked.Decrement(ref _queuedTasks);
                }
            }

            Logger.Info("Idle thread exited: " + Thread.CurrentThread.Name);
        }

        private Task<TaskStatus> GetExtendedTask(ITaskExecuter taskExecuter, ScheduledTask scheduledTask)
        {
            var task = new Task<TaskStatus>((state) =>
            {
                var taskState = state as TaskState;
                if (taskState == null)
                    throw new Exception("Invalid task state");

                taskState.Stopwatch.Start();

                var t = taskExecuter.ExecuteTask(scheduledTask);

                return t.Result;
            }, new TaskState
            {
                Stopwatch = new Stopwatch(),
                ScheduledTask = scheduledTask
            });

            // completed
            task.ContinueWith(act =>
            {
                var taskState = act.AsyncState as TaskState;
                if (taskState == null)
                    throw new Exception("Invalid task state");

                RaiseOnTaskExecuted(taskState.ScheduledTask, act.Result
                    , duration: taskState.Stopwatch.ElapsedMilliseconds);
            }, TaskContinuationOptions.OnlyOnRanToCompletion);

            // canceled
            task.ContinueWith(act =>
            {
                var taskState = act.AsyncState as TaskState;
                if (taskState == null)
                    throw new Exception("Invalid task state");

                RaiseOnTaskExecuted(taskState.ScheduledTask, TaskStatus.Canceled);
            }, TaskContinuationOptions.OnlyOnCanceled);

            // exception
            task.ContinueWith(act =>
            {
                var taskState = act.AsyncState as TaskState;
                if (taskState == null)
                    throw new Exception("Invalid task state");

                RaiseOnTaskExecuted(taskState.ScheduledTask, TaskStatus.Failed, act.Exception);
            }, TaskContinuationOptions.OnlyOnFaulted);

            // general/cleanup
            task.ContinueWith(act =>
            {
                Interlocked.Decrement(ref _runningTasks);
                Interlocked.Decrement(ref _releasedResources);

                ReleaseResource();
            });

            return task;
        }

        private void RaiseOnTaskQueued(ScheduledTask task)
        {
            if (OnTaskQueued == null)
                return;
            try
            {
                OnTaskQueued(this, new TaskOperationEventArgs(task, TaskStatus.Queued));
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void RaiseOnTaskException(ScheduledTask task, TaskStatus status, Exception exception)
        {
            if (OnTaskException == null)
                return;
            try
            {
                OnTaskException(this, new TaskOperationEventArgs(task, status, exception));
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void RaiseOnTaskExecuting(ScheduledTask task)
        {
            if (OnTaskExecuting == null)
                return;
            try
            {
                OnTaskExecuting(this, new TaskOperationEventArgs(task, TaskStatus.InProgress));
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void RaiseOnTaskExecuted(ScheduledTask task, TaskStatus status, Exception exception = null, long duration = 0)
        {
            if (OnTaskExecuted == null)
                return;

            RaiseOnTaskExecuted(new TaskOperationEventArgs(task, status, exception, duration));
        }

        private void RaiseOnTaskExecuted(TaskOperationEventArgs e)
        {
            if (OnTaskExecuted == null)
                return;
            try
            {
                OnTaskExecuted(this, e);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void ReleaseResource()
        {
            bool shouldStartTaskAfter;
            lock (_concurrencyLock)
            {
                shouldStartTaskAfter = Interlocked.Read(ref _releasedResources) < ConcurrencyLevel;
            }
            if (!shouldStartTaskAfter) return;

            Interlocked.Increment(ref _releasedResources);
            _semaphore.Release();
        }

    }
}
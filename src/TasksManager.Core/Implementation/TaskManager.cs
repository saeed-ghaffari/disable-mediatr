﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.TasksManager.Core.Enum;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.TasksManager.Core.Implementation
{
    public class TaskManager
    {
        #region Models
        private class GroupManagerStats
        {
            public TaskGroupManager TaskGroupManager { get; set; }

            public int QueuedTasks { get; set; }

            public int CurrentFetchCount { get; set; }
        }
        #endregion

        private const int DefaultFetchCount = 100;
        private const int MinFetchCount = 10;
        private const int MaxFetchCount = 10000;
        private const int CleanupTimeout = 120;
        private static readonly NLog.ILogger Logger = NLog.LogManager.GetCurrentClassLogger();

        // settings
        private readonly int _runnerId;
        private readonly int _interval;
        private readonly ITaskExecuterFactory _executerFactory;
        private readonly ITaskRepository _taskRepository;
        private readonly IRecurringTaskRepository _recurringTaskRepository;

        // group managers
        private ConcurrentDictionary<long, GroupManagerStats> _groupManagers;

        // timers
        private System.Threading.Timer _fetchTimer;
        private bool _isFetching;
        private bool _isFetchingRecurringTasks;

        public TaskManager(int runnerId, int interval, ITaskExecuterFactory executerFactory,
            ITaskRepository taskRepository, IRecurringTaskRepository recurringTaskRepository)
        {
            _interval = interval;
            _executerFactory = executerFactory;
            _taskRepository = taskRepository;
            _recurringTaskRepository = recurringTaskRepository;
            _runnerId = runnerId;

            Init();
        }

        private void Init()
        {
            Logger.Info("Initializing Task Manager...");
            _groupManagers = new ConcurrentDictionary<long, GroupManagerStats>();
            _fetchTimer = new System.Threading.Timer(FetchNewTasks, null, int.MaxValue, _interval);
        }

        private void FetchNewTasks(object param)
        {
            try
            {
                Task.Run(() =>
                {
                    FetchRecurringTasks().Wait();
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            try
            {
                FetchScheduledTasks();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private async Task FetchRecurringTasks()
        {
            if (_isFetchingRecurringTasks)
            {
                Logger.Debug("Currently Fetching recurring tasks");
                return;
            }

            _isFetchingRecurringTasks = true;

            try
            {
                var recurringTasks = await _recurringTaskRepository.GetActiveTasks(_runnerId);

                foreach (var rtask in recurringTasks)
                {
                    // update recurring task
                    rtask.Status = RecurringTaskStatus.Running;
                    rtask.LastExecution = DateTime.Now;
                    await _recurringTaskRepository.UpdateAsync(rtask);

                    // add a new scheduled task
                    var task = await _taskRepository.AddAsync(new ScheduledTask
                    {
                        GroupId = rtask.GroupId,
                        DueTime = rtask.NextExecution,
                        Type = rtask.Type,
                        Params = rtask.Params,
                        RecurringTaskId = rtask.Id,
                        RefId = rtask.RefId
                    });

                    // update recurring task
                    rtask.LastTaskId = task.Id;
                    rtask.LastTaskStatus = TaskStatus.Pending;
                    await _recurringTaskRepository.UpdateAsync(rtask);
                }
            }
            finally
            {
                _isFetchingRecurringTasks = false;
            }
        }

        private void FetchScheduledTasks()
        {
            if (_isFetching)
            {
                return;
            }

            _isFetching = true;

            try
            {
                var taskGroups = _taskRepository.GetTaskGroups(_runnerId);

                var tasks = taskGroups.Select(@group => Task.Run(() =>
                {
                    FetchGroupTasks(@group);
                })).ToArray();

                Task.WaitAll(tasks);
            }
            finally
            {
                _isFetching = false;
            }
        }

        private void FetchGroupTasks(TaskGroup taskGroup)
        {
            try
            {
                var groupManagerStats = GetGroupManager(taskGroup);

                // Optimize the number of tasks fetched
                groupManagerStats.CurrentFetchCount = CalculateFetchCount(groupManagerStats);

                var newTasks = _taskRepository.GetPendingTasks(taskGroup.Id, groupManagerStats.CurrentFetchCount).ToList();

                // Set the current queueLength
                // this is important to set this number before queueing tasks
                groupManagerStats.QueuedTasks = groupManagerStats.TaskGroupManager.GetQueueLength()
                    + newTasks.Count();

                EnqueueRange(groupManagerStats.TaskGroupManager, newTasks);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public void Start(bool cleanup = true)
        {
            Logger.Info("Starting task manager...");
            // first cleanup abandoned tasks
            if (cleanup)
                CleanupTasks();

            _fetchTimer.Change(0, _interval);

            Logger.Info("Waiting for tasks...");
        }

        public void Stop()
        {
            _fetchTimer.Change(int.MaxValue, _interval);
            Logger.Info("Task manager stopped.");
        }

        public void EnqueueRange(TaskGroupManager taskGroupManager, IEnumerable<ScheduledTask> range)
        {
            foreach (var task in range)
            {
                taskGroupManager.Enqueue(task);
            }
        }

        private int CalculateFetchCount(GroupManagerStats groupManagerStats)
        {
            var lastQueueLength = groupManagerStats.QueuedTasks;
            var currentQueueLength = groupManagerStats.TaskGroupManager.GetQueueLength();

            var queueDiff = currentQueueLength - lastQueueLength;

            var fetchCount = groupManagerStats.CurrentFetchCount;

            if (queueDiff == 0)
            {
                // One of these cases happened:
                // 1- No tasks were queued to be executed so we don't have any data to work on
                // 2- Tasks are taking exactly the same time as a fetch cycle
                // so no need to change the fetch count
                fetchCount = groupManagerStats.CurrentFetchCount;
            }
            else if (queueDiff < 0 && -queueDiff >= fetchCount)
            {
                // queueDiff <0 
                // This means tasks are getting executed more than queued
                // we can increase the fetch count

                fetchCount -= queueDiff;
            }
            else if (queueDiff < 0 && -queueDiff < fetchCount)
            {
                // Tasks are getting executed less than queued
                // we decrease the fetch count
                fetchCount = -queueDiff;
            }
            else if (queueDiff > 0)
            {
                // Tasks are getting executed less than queued
                // we decrease the fetch count
                fetchCount -= queueDiff;
            }

            if (fetchCount < MinFetchCount)
                fetchCount = MinFetchCount;
            else if (fetchCount > MaxFetchCount)
                fetchCount = MaxFetchCount;

            if (fetchCount != groupManagerStats.CurrentFetchCount)
                Logger.Info("Changing fetch count from {0} to {1} ({2}) \n lastQueueLength: {3} \n currentQueueLength: {4} \n queueDiff: {5}",
                    groupManagerStats.CurrentFetchCount, fetchCount,
                    groupManagerStats.TaskGroupManager.Group, lastQueueLength, currentQueueLength, queueDiff);

            return fetchCount;
        }

        /// <summary>
        /// sets abandoned tasks' status to pending
        /// </summary>
        private void CleanupTasks()
        {
            try
            {
                _taskRepository.CleanupTasks(CleanupTimeout);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private GroupManagerStats GetGroupManager(TaskGroup group)
        {
            if (group == null)
                throw new Exception("Invalid Group");
            if (group.ConcurrencyLevel < 1)
                throw new Exception("Invalid concurrency level");

            if (_groupManagers.ContainsKey(group.Id))
                return _groupManagers[group.Id];

            var groupManager = new TaskGroupManager(@group, @group.ConcurrencyLevel, _executerFactory);
            var groupManagerStats = new GroupManagerStats
            {
                TaskGroupManager = groupManager,
                CurrentFetchCount = DefaultFetchCount
            };

            if (!_groupManagers.TryAdd(group.Id, groupManagerStats))
            {
                throw new Exception($"unable to add group {group.Title} to dic");
            };


            groupManager.OnTaskQueued += groupManager_OnTaskQueued;
            groupManager.OnTaskExecuting += groupManager_OnTaskExecuting;
            groupManager.OnTaskExecuted += groupManager_OnTaskExecuted;
            groupManager.OnTaskException += groupManager_OnTaskException;

            return groupManagerStats;
        }

        private void groupManager_OnTaskException(TaskGroupManager manager, TaskOperationEventArgs e)
        {
            Logger.Info("Manager: " + e.Exception.Message + " " + e.Task);

            e.Task.Status = e.TaskStatus;
            e.Task.ExecuteTime = e.Duration;
            _taskRepository.Update(e.Task);
        }

        private void groupManager_OnTaskQueued(TaskGroupManager manager, TaskOperationEventArgs e)
        {
            Logger.Trace("Manager: task queued " + e.Task);

            e.Task.Status = e.TaskStatus;
            e.Task.ExecuteTime = -1;
            _taskRepository.Update(e.Task);
        }

        private void groupManager_OnTaskExecuting(TaskGroupManager manager, TaskOperationEventArgs e)
        {
            Logger.Trace("Manager: task executing " + e.Task);

            e.Task.Status = e.TaskStatus;
            e.Task.ExecuteTime = -1;
            e.Task.LastRun = DateTime.Now;
            _taskRepository.Update(e.Task);
        }

        private void groupManager_OnTaskExecuted(TaskGroupManager manager, TaskOperationEventArgs e)
        {
            if (e.Exception == null)
                Logger.Trace("Manager: task completed {0} in {1}ms", e.Task, e.Duration);
            else
                Logger.Error("Manager: task exited with exceptions " + e.Exception);

            e.Task.Status = e.TaskStatus;
            e.Task.ExecuteTime = e.Duration;
            _taskRepository.Update(e.Task);

            if (!e.Task.RecurringTaskId.HasValue) return;

            // set recurring task status to pending and update next execution
            var recurringTask = _recurringTaskRepository.Get(e.Task.RecurringTaskId.Value);
            if (recurringTask == null)
                return;

            recurringTask.Status = RecurringTaskStatus.Idle;
            recurringTask.NextExecution = GetRecurringNextExecution(recurringTask);
            recurringTask.LastTaskStatus = e.TaskStatus;
            _recurringTaskRepository.Update(recurringTask);
        }

        private DateTime GetRecurringNextExecution(RecurringTask rtask)
        {
            switch (rtask.IntervalPeriod)
            {
                case RecurringIntervalPeriod.Day:
                    return DateTime.Now.AddDays(rtask.Interval);

                case RecurringIntervalPeriod.Hour:
                    return DateTime.Now.AddHours(rtask.Interval);

                case RecurringIntervalPeriod.Minute:
                    return DateTime.Now.AddMinutes(rtask.Interval);

                case RecurringIntervalPeriod.Second:
                    return DateTime.Now.AddSeconds(rtask.Interval);

                default:
                    throw new ArgumentException("Invalid IntervalPeriod", nameof(RecurringTask.IntervalPeriod));
            }
        }
    }
}
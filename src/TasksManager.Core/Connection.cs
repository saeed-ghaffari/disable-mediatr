﻿using System.Data;
using System.Data.SqlClient;

namespace Cerberus.TasksManager.Core
{
    public static class Connection
    {
        private static string _connectionString;
        
        public static void SetConnectionString(string connectionString)
        {
            _connectionString = connectionString;
        }

        public static IDbConnection GetConnection()
        {
            return new SqlConnection(_connectionString);
        }
    }
}
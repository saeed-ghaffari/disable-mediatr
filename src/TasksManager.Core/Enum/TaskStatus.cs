﻿namespace Cerberus.TasksManager.Core.Enum
{
    public enum TaskStatus
    {
        Pending = 1,
        Fetched = 2,
        Queued = 3,
        InProgress = 4,
        Done = 5,
        Recovered = 6,

        Failed = 11,
        Canceled = 12,
        BadParams = 13,
        Disabled = 14
    }
}
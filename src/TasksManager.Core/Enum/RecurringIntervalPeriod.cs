﻿namespace Cerberus.TasksManager.Core.Enum
{
    public enum RecurringIntervalPeriod
    {
        Second = 1,
        Minute = 2,
        Hour = 3,
        Day = 4
    }
}
﻿using Cerberus.TasksManager.Core.Interface;

namespace Cerberus.TasksManager.Core.Delegate
{
    public delegate ITaskExecuter CreateTaskExecuter(string type);
}
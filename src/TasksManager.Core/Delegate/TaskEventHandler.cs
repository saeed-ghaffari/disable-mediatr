﻿using Cerberus.TasksManager.Core.Implementation;

namespace Cerberus.TasksManager.Core.Delegate
{
    public delegate void TaskEventHandler(TaskGroupManager manager, TaskOperationEventArgs e);

}
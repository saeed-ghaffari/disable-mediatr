﻿using System.Threading.Tasks;
using Cerberus.TasksManager.Core.Model;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.TasksManager.Core.Interface
{
    public interface ITaskExecuter
    {
        bool IsReusable { get; }

        /// <summary>
        /// Returns true if the executer can process the task
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        bool CanHandleTask(string param);

        Task<TaskStatus> ExecuteTask(ScheduledTask task);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.TasksManager.Core.Model;

namespace Cerberus.TasksManager.Core.Interface
{
    public interface IRecurringTaskRepository
    {
        Task<RecurringTask> AddAsync(RecurringTask task);

        Task<RecurringTask> GetAsync(long taskId);

        RecurringTask Get(long taskId);

        Task<RecurringTask> UpdateAsync(RecurringTask task);

        RecurringTask Update(RecurringTask task);
        
        Task DeleteAsync(long taskId);

        Task<IEnumerable<RecurringTask>> GetActiveTasks(int runnerId);

        Task<IEnumerable<RecurringTask>> GetAsync(string type, long refId, string param = null);

        Task<IEnumerable<RecurringTask>> GetAsync(string type, IEnumerable<long> refIds); 

        Task EnableTaskAsync(long taskId, bool value);
    }
}
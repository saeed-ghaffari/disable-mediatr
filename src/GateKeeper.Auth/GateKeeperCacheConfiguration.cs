﻿using System;

namespace GateKeeper.Auth
{
    public class GateKeeperCacheConfiguration
    {
        public TimeSpan ReconstructTime { get; set; }
        public TimeSpan Ttl { get; set; }
        public TimeSpan UserExpiration { get; set; }
        public bool EnableCache { get; set; }
    }
}
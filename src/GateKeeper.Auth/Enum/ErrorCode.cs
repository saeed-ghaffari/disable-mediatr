﻿namespace GateKeeper.Auth.Enum
{
    public enum ErrorCode
    {
        InCorrectUsernameOrPassword = 4010,
        Locked = 4011
    }
}
﻿using System;
using System.Threading.Tasks;

namespace GateKeeper.Auth.Interface
{
    /// <summary>
    /// for cache all objects --> Be careful that expiration time should not be very high
    /// </summary>
    public interface IMasterCacheProvider
    {
        /// <summary>
        /// Fetch data from cache if not exist fetch data from given func and save to cache then return it
        /// </summary>
        /// <typeparam name="T"> object type</typeparam>
        /// <param name="key">key to get data from cache</param>
        /// <param name="getFromDatabase">get data from database when data does not exist in cache</param>
        /// <param name="expirationTime">cache expiration time</param>
        /// <returns></returns>
        Task<T> FetchAsync<T>(string key, Func<Task<T>> getFromDatabase, TimeSpan expirationTime);

        /// <summary>
        /// Store data in cache
        /// </summary>
        /// <typeparam name="T"> object type</typeparam>
        /// <param name="key">cache key</param>
        /// <param name="value">data</param>
        /// <param name="expirationTime">cache expiration time</param>
        /// <returns></returns>
        Task StoreAsync<T>(string key, T value, TimeSpan expirationTime);



    }
}
﻿using System.Threading.Tasks;
using GateKeeper.Auth.Model;

namespace GateKeeper.Auth.Interface
{
    public interface IUserService
    {
        Task CreateAsync(User user);

        Task<User> GetAsync(long id, bool includeAllRelatedData);

        Task<User> GetAsync(string userName, string password);

        Task<User> GetAsync(string userName);

    }
}
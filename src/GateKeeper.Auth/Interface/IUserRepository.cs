﻿using System;
using System.Threading.Tasks;
using GateKeeper.Auth.Model;

namespace GateKeeper.Auth.Interface
{
    public interface IUserRepository
    {
        Task CreateAsync(User user);

        Task<User> GetAsync(long id, bool includeAllRelatedData);

        Task<User> GetAsync(string userName);
        Task UpdateAsync(User user);
        Task UpdateAsync(long id, DateTime lastLogIn);
        Task UpdateAsync(long id,int attempts, DateTime? lockedUntil);
    }
    
}
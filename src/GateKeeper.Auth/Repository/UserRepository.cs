﻿using System;
using System.Threading.Tasks;
using GateKeeper.Auth.Interface;
using GateKeeper.Auth.Model;
using Microsoft.EntityFrameworkCore;

namespace GateKeeper.Auth.Repository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private readonly AuthDbContext _dbContext;

        public UserRepository(AuthDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<User> GetAsync(long id, bool includeAllRelatedData)
        {
            if (includeAllRelatedData)
                return _dbContext.Users
                    .Include(x => x.UserRoles)
                    .ThenInclude(x => x.Role)
                    .ThenInclude(x => x.RoleResources)
                    .ThenInclude(x => x.Resource)
                    .FirstOrDefaultAsync(x => x.Id == id);

            return _dbContext.Users
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<User> GetAsync(string userName)
        {
            return _dbContext.Users.AsNoTracking().FirstOrDefaultAsync(x => x.UserName.Equals(userName));

        }

        public async Task UpdateAsync(long id, DateTime lastLogIn)
        {
            await _dbContext.Database.ExecuteSqlCommandAsync($"update [auth].[user] set lastLogIn={lastLogIn} , lockedUntil = null ,attempts=0  where id={id}");
        }

        public async Task UpdateAsync(long id, int attempts, DateTime? lockedUntil)
        {
            if (lockedUntil == null)
                await _dbContext.Database.ExecuteSqlCommandAsync($"update [auth].[user] set attempts={attempts} where id={id}");
            else
                await _dbContext.Database.ExecuteSqlCommandAsync($"update [auth].[user] set attempts={attempts} , lockedUntil ={lockedUntil} where id={id}");

        }
    }
}
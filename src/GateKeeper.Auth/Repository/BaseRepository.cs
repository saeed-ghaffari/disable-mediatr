﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GateKeeper.Auth.Model;
using Microsoft.EntityFrameworkCore;

namespace GateKeeper.Auth.Repository
{
    public class BaseRepository<T> where T : BaseModel
    {
        private readonly AuthDbContext _dbContext;

        public BaseRepository(AuthDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task CreateAsync(T entity)
        {
            await _dbContext.AddAsync<T>(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            _dbContext.Update(entity);
            await _dbContext.SaveChangesAsync();
        }


        public async Task DeleteAsync(T entity)
        {
            _dbContext.Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<T> GetAsync(long id)
        {
            return await _dbContext.FindAsync<T>(id);
        }

        public async Task CreateRangeAsync(IEnumerable<T> entity)
        {
            await _dbContext.AddRangeAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateRangeAsync(IEnumerable<T> entity)
        {
            _dbContext.UpdateRange(entity);
            await _dbContext.SaveChangesAsync();
        }
        public async Task<IEnumerable<T>> GetListAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbContext.Set<T>().Where(predicate).ToListAsync();
        }
        public async Task<IEnumerable<T>> GetConditionalListAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbContext.Set<T>().Where(predicate).ToListAsync();
        }
    }
}
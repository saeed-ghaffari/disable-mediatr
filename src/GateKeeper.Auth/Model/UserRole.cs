﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ProtoBuf;

namespace GateKeeper.Auth.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("UserRole", Schema = "auth")]
    public class UserRole : BaseModel
    {
        [ForeignKey(nameof(User))]
        public long UserId { get; set; }

        [ForeignKey(nameof(Role))]
        public long RoleId { get; set; }

        [ProtoIgnore]
        public User User { get; set; }

        public Role Role { get; set; }
    }
}
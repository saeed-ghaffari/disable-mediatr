﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GateKeeper.Auth.Model
{
    [Table("User", Schema = "auth")]
    [Serializable]
    public class User : BaseModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime? LastLogIn { get; set; }
        public int Attempts { get; set; }
        public DateTime? LockedUntil { get; set; }

        public long RefId { get; set; }

        public IEnumerable<UserRole> UserRoles { get; set; }
    }
}

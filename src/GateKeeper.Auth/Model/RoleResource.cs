﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ProtoBuf;

namespace GateKeeper.Auth.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("RoleResource", Schema = "auth")]
    public class RoleResource : BaseModel
    {
        [ForeignKey(nameof(Role))]
        public long RoleId { get; set; }

        [ForeignKey(nameof(Resource))]
        public long ResourceId { get; set; }

        [ProtoIgnore]
        public Role Role { get; set; }
        public Resource Resource { get; set; }
    }
}
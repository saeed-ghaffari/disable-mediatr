﻿using System;

namespace GateKeeper.Auth
{
    internal static class CacheKeyHelper
    {
        private const string Prefix = "auth";

        internal static string GetUserKey(long userId, bool includeAllRelatedData) =>
            $"{Prefix}:{userId}:{includeAllRelatedData}";

        internal static string GetUserKey(string userName) => $"{Prefix}:{userName}";
    }
}
﻿using System;
using System.Threading.Tasks;
using GateKeeper.Auth.Interface;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace GateKeeper.Auth.Service
{
    public class MasterCacheProvider : IMasterCacheProvider
    {
        private readonly ILogger<MasterCacheProvider> _logger;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;
        private readonly IDatabase _readDatabase;
        private readonly IDatabase _writeDatabase;
        public MasterCacheProvider(ILogger<MasterCacheProvider> logger, IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration, IDatabase readDatabase, IDatabase writeDatabase)
        {
            _logger = logger;
            _readDatabase = readDatabase;
            _writeDatabase = writeDatabase;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }

        public async Task<T> FetchAsync<T>(string key, Func<Task<T>> getFromDatabase, TimeSpan expirationTime)
        {
            if (!_masterCacheProviderConfiguration.IsActive)
                return await getFromDatabase();

            try
            {
                var value = await _readDatabase.StringGetAsync(key);
                if (!value.IsNull && value.HasValue) return JsonConvert.DeserializeObject<T>(value);
                var result = await getFromDatabase();

                if (result == null)
                    return default(T);

                await StoreAsync(key, result, expirationTime);

                return result;

            }
            catch (Exception e)
            {
                _logger.LogCritical(e, e.Message);
                throw;
            }

        }

        public async Task StoreAsync<T>(string key, T value, TimeSpan expirationTime)
        {
            try
            {
                var valueBytes = JsonConvert.SerializeObject(value,Formatting.None,new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
                await _writeDatabase.StringSetAsync(key, valueBytes, expirationTime);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "StoreAsync Has Exception");
                throw;
            }


        }

    }
}
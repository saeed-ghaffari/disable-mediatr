﻿using System;
using GateKeeper.Auth.Enum;

namespace GateKeeper.Auth
{
    public class GateKeeperAuthException : Exception
    {
        public ErrorCode ErrorCode { get; set; }

        public GateKeeperAuthException()
        {

        }

        public GateKeeperAuthException(ErrorCode code)
        {
            ErrorCode = code;
        }
    }
}
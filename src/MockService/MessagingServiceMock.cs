﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services;
using Cerberus.Utility.Enum;

namespace Cerberus.MockService
{
    public class MessagingServiceMock : IMessagingService
    {
        public Task SendSmsAsync(long? profileId, string msisdn, Carrier? carrier, string message, DateTime? douTime = null)
        {
            Console.WriteLine($@"
sending sms....
    message:
{message}

");

            return Task.CompletedTask;
        }

        public Task SendOtpSmsAsync(long? profileId, string msisdn, Carrier? carrier, string message, DateTime? dueTime = null)
        {
            Console.WriteLine($@"
sending sms....
    message:
{message}

");

            return Task.CompletedTask;
        }
    }
}
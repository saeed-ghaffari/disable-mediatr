﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services;

namespace Cerberus.MockService
{
    public class MockGatewayService : IBankingPaymentGateway
    {
        public Task<string> InitialPaymentAsync(long localInvoiceId, long amount)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        

        public Task<bool> VerifyPaymentAsync(long saleReferenceId, long? saleOrderId, string refId)
        {
            return Task.FromResult(true);
        }

        public Task<bool> SettlePaymentAsync(long saleReferenceId, long? saleOrderId, string refId)
        {
            return Task.FromResult(true);
        }
    }
}
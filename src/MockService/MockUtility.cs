﻿using Newtonsoft.Json;

namespace Cerberus.MockService
{
    public static class MockUtility
    {
        public static string ToLogText(this object o)
        {
            return JsonConvert.SerializeObject(o, Formatting.Indented);
        }
    }
}
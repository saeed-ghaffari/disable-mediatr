﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.ViewModel;

namespace Cerberus.MockService
{
    public class MockMelliGatewayService : IBankMelliPaymentGateway
    {
        public Task<string> InitialPaymentAsync(long localInvoiceId, long amount)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<MelliVerifyResult> MelliVerifyPaymentAsync(string orderId, string token)
        {
            return  Task.FromResult(new MelliVerifyResult
            {
                ResCode = "1",
                Description = "",
                Amount = "100000",
                OrderId = "1",
                RetrivalRefNo = "test",
                Succeed = true,
                SystemTraceNo = "123"
            });
        }


       
    }
}
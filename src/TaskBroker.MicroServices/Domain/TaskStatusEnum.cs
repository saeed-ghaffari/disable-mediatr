﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskBroker.MicroServices.Domain
{
    public enum TaskStatusEnum
    {
        Pending = 1,
        Executing = 2,
        Succesfull = 3,
        Failed = 4
    }
}

﻿using System.Data;

namespace TaskBroker.MicroServices.Domain.Interfaces
{
    public interface IConnectionManager
    {
        IDbConnection GetConnection();

        void FreeConnection();
    }
}

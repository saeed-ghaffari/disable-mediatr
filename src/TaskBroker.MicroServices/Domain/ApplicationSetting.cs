﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskBroker.MicroServices.Domain
{
    public class ApplicationSetting
    {
        public string ConnectionString { get; set; }

        public string SmsConnectionString { get; set; }
    }
}

﻿using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using TaskBroker.MicroServices.Domain.Interfaces;

namespace TaskBroker.MicroServices.Domain
{
    public class ConnectionManager : IConnectionManager
    {
        private class ConnectionInfo
        {
            private ConnectionInfo() 
            {
                ClientsCount = 1;
            }

            public ConnectionInfo(IDbConnection cnn): this()
            {
                Connection = cnn;
            }

            public IDbConnection Connection { get; set; }

            public int ClientsCount { get; set; }

            public IDbConnection GetConnection()
            {
                ClientsCount++;
                return Connection;
            }

            public void FreeConnection()
            {
                ClientsCount--;

                if(ClientsCount <= 0)
                {
                    if (Connection.State == ConnectionState.Open)
                        Connection.Close();

                    ClientsCount = 0;
                }
            }
        }


        private readonly string _connectionString;
        private Dictionary<Thread, ConnectionInfo> _connections = new Dictionary<Thread, ConnectionInfo>();


        public ConnectionManager(IOptions<ApplicationSetting> appSetting)
        {
            _connectionString = appSetting.Value.ConnectionString;
        }


        public IDbConnection GetConnection()
        {
            if (!_connections.ContainsKey(Thread.CurrentThread))
            {
                IDbConnection cnn = new SqlConnection(_connectionString);
                cnn.Open();

                _connections.Add(Thread.CurrentThread, new ConnectionInfo(cnn));
            }

            return _connections[Thread.CurrentThread].GetConnection();
        }

        public void FreeConnection()
        {
            if (_connections.ContainsKey(Thread.CurrentThread))
                _connections[Thread.CurrentThread].FreeConnection();
        }
    }
}

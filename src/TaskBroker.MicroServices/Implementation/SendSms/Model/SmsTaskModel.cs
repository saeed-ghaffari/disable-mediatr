﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskBroker.MicroServices.Implementation.SendSms.Models
{
    public class SmsTaskModel
    {
        public Guid GId { get; set; }

        public string Target { get; set; }

        public string Message { get; set; }

        public string CreateDate { get; set; }

        public int Status { get; set; }

        public int ParentId { get; set; }
    }
}

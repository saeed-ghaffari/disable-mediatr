﻿using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using System;

namespace TaskBroker.MicroServices.Implementation.SendSms.Infrastructure
{
    public interface ISmsService
    {
        void Enqueue(SendSmsTaskParam sms);

    }
}

﻿using System;
using System.Threading.Tasks;
using TaskBroker.MicroServices.Domain;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;

namespace TaskBroker.MicroServices.Implementation.SendSms.Infrastructure
{
    public interface ISmsRepository
    {
        Task Insert(SendSmsTaskParam sms);

        Task ChangeStatus(Guid gid, TaskStatusEnum status, string message = "");
    }
}

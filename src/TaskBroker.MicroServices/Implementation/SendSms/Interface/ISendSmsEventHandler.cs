﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskBroker.Domain.Bus;
using TaskBroker.Domain.Events;

namespace TaskBroker.MicroServices.Implementation.SendSms.Infrastructure
{
    public interface ISendSmsEventHandler: IEventHandler
    {
    }
}

﻿using System;
using Newtonsoft.Json;
using TaskBroker.Domain.Bus;
using TaskBroker.Domain.Commands;
using TaskBroker.Domain.Common;
using TaskBroker.Domain.Events;
using TaskBroker.MicroServices.Implementation.SendSms.Infrastructure;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;

namespace TaskBroker.MicroServices.Implementation.SendSms.Services
{
    public class SmsService : ISmsService
    {
        private readonly ISmsRepository _smsRepository;
        private readonly IEventBus _eventBus;
        private readonly IEnqueueCommandHandler _command;

        public SmsService(IEventBus eventBus, ISmsRepository smsRepository, IEnqueueCommandHandler command)
        {
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
            _smsRepository = smsRepository ?? throw new ArgumentNullException(nameof(smsRepository));
            _command = command;
        }

        public void Enqueue(SendSmsTaskParam sms)
        {
            _smsRepository.Insert(sms);

            _eventBus.Publish(new Event
            {
                Message = new Message
                {
                    Id = Guid.Empty, //sms.GId,
                    Data = JsonConvert.SerializeObject(sms)
                },
                RouteKey = "Sejam.SendSMS"
            }); ;
        }
    }
}

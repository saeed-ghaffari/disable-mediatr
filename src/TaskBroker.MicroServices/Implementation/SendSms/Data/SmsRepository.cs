﻿using System;
using System.Data;
using System.Threading.Tasks;
using TaskBroker.MicroServices.Domain;
using TaskBroker.MicroServices.Domain.Interfaces;
using TaskBroker.MicroServices.Implementation.SendSms.Infrastructure;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;

namespace TaskBroker.MicroServices.Implementation.SendSms.Data
{
    public class SmsRepository: ISmsRepository
    {
        private readonly IConnectionManager _connectionManager;

        public SmsRepository(IConnectionManager connectionManager)
        {
            _connectionManager = connectionManager;
        }

        public Task Insert(SendSmsTaskParam sms)
        {
            using(var cmd = _connectionManager.GetConnection().CreateCommand())
            {
                cmd.CommandText = "sp_SMS_Insert";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                IDataParameter p;

                p = cmd.CreateParameter();
                p.ParameterName = "@target";
                p.DbType = DbType.String;
                p.Value = sms.Msisdn;
                cmd.Parameters.Add(p);

                p = cmd.CreateParameter();
                p.ParameterName = "@message";
                p.DbType = DbType.String;
                p.Value = sms.SmsText;
                cmd.Parameters.Add(p);

                p = cmd.CreateParameter();
                p.ParameterName = "@newID";
                p.DbType = DbType.Guid;
                p.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();

                //if (p != null && p.Value != null)
                //    sms.GId = Guid.Parse(p.Value.ToString());
            }

            return Task.FromResult(true);
        }

        public Task ChangeStatus(Guid gid, TaskStatusEnum status, string message = "")
        {
            using (var cmd = _connectionManager.GetConnection().CreateCommand())
            {
                cmd.CommandText = "sp_SMS_UpdateStatus";
                cmd.CommandType = CommandType.StoredProcedure;

                IDataParameter p;

                p = cmd.CreateParameter();
                p.ParameterName = "@gid";
                p.DbType = DbType.Guid;
                p.Value = gid;
                cmd.Parameters.Add(p);

                p = cmd.CreateParameter();
                p.ParameterName = "@statusId";
                p.DbType = DbType.Int32;
                p.Value = (int)status;
                cmd.Parameters.Add(p);

                if(!string.IsNullOrEmpty(message))
                {
                    p = cmd.CreateParameter();
                    p.ParameterName = "@message";
                    p.DbType = DbType.String;
                    p.Value = message;
                    cmd.Parameters.Add(p);
                }

                cmd.ExecuteNonQuery();
            }

            return Task.FromResult(true);
        }
    }
}

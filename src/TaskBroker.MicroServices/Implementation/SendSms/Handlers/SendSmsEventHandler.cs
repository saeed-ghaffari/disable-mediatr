﻿using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskBroker.Domain.Bus;
using TaskBroker.Domain.Commands;
using TaskBroker.Domain.Common;
using TaskBroker.Domain.Events;
using TaskBroker.MicroServices.Domain;
using TaskBroker.MicroServices.Implementation.SendSms.Infrastructure;
using TaskBroker.MicroServices.Implementation.SendSms.Models;

namespace TaskBroker.MicroServices.Implementation.SendSms.Handlers
{
    public class SendSmsEventHandler : ISendSmsEventHandler
    {
        private readonly ISmsRepository _smsRepository;
        private ApplicationSetting _config;
        

        public SendSmsEventHandler(ISmsRepository smsRepository, IOptions<ApplicationSetting> config)
        {
            _smsRepository = smsRepository ?? throw new ArgumentNullException(nameof(smsRepository));
            _config = config.Value;
        }


        public async Task Handle(Event @event)
        {
            try
            {
                var sms = JsonConvert.DeserializeObject<SmsTaskModel>(@event.Message.Data);

                if(sms != null)
                {
                    await SendSMS(sms.Target, sms.Message);
                    await _smsRepository.ChangeStatus(sms.GId, TaskStatusEnum.Succesfull);
                }
            }
            catch(Exception exp)
            {
                await _smsRepository.ChangeStatus(@event.Message.Id, TaskStatusEnum.Failed, exp.Message);
            }
        }

        public async Task SendSMS(string number, string message)
        {
            var insertData = "insert into outgoing_message(from_mobile_number,dest_mobile_number,message_body,due_date,creation_date," +
                             "Priority) values (@from_mobile, @dest_mobile_number, @message_body, @due_date, @creation_date, @Priority)";

            using (var connection = new MySqlConnection(_config.SmsConnectionString))
            {
                var command = new MySqlCommand(insertData, connection);

                command.Parameters.AddWithValue("@from_mobile", "9810000510");
                command.Parameters.AddWithValue("@dest_mobile_number", number);
                command.Parameters.AddWithValue("@message_body", message);
                command.Parameters.AddWithValue("@due_date", DateTime.Now);
                command.Parameters.AddWithValue("@creation_date", DateTime.Now);
                command.Parameters.AddWithValue("@Priority", 1);
                connection.Open();
                command.CommandTimeout = 5;
                await command.ExecuteNonQueryAsync();
                await connection.CloseAsync();
            }
        }
    }
}

﻿using System;
using Cerberus.Repository.MsSql;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Cerberus.Migrations
{
    public class CerberusDbContextBuilder : IDesignTimeDbContextFactory<CerberusDbContext>
    {
        public CerberusDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CerberusDbContext>();
            optionsBuilder.UseSqlServer("Data Source=172.16.170.45;initial catalog=Cerberus;user ID=sa;password=123456;Connection Timeout=6;",
                db =>
                {
                    db.MigrationsHistoryTable("__EFMigrationsHistory", "dbo");
                    db.MigrationsAssembly(this.GetType().Assembly.FullName);
                });

            return new CerberusDbContext(optionsBuilder.Options);
        }
    }
}
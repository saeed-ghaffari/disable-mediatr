﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addmsisdnandshahkarInquiryentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Msisdn",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "GetDate()"),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    IsConfirmed = table.Column<bool>(nullable: true),
                    ProfileId = table.Column<long>(nullable: false),
                    Mobile = table.Column<long>(nullable: false),
                    ChangeMobileCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Msisdn", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Msisdn_Profile_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ShahkarInquiry",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "GetDate()"),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Ssn = table.Column<string>(nullable: true),
                    Mobile = table.Column<long>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    Successful = table.Column<bool>(nullable: false),
                    SuccessInquiry = table.Column<bool>(nullable: false),
                    HasException = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShahkarInquiry", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Msisdn__ProfileId",
                table: "Msisdn",
                column: "ProfileId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ShahkarInquiry_Ssn_SuccessInquiry",
                table: "ShahkarInquiry",
                columns: new[] { "Ssn", "SuccessInquiry" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Msisdn");

            migrationBuilder.DropTable(
                name: "ShahkarInquiry");
        }
    }
}

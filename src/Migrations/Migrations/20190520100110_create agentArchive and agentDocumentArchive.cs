﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class createagentArchiveandagentDocumentArchive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AgentArchive",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "GetDate()"),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    IsConfirmed = table.Column<bool>(nullable: true),
                    AgentId = table.Column<long>(nullable: false),
                    ProfileId = table.Column<long>(nullable: false),
                    AgentProfileId = table.Column<long>(nullable: false),
                    Type = table.Column<byte>(nullable: false),
                    ExpirationDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    AgentCreationDate = table.Column<DateTime>(nullable: false),
                    AgentModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentArchive", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AgentDocumentArchive",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "GetDate()"),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    IsConfirmed = table.Column<bool>(nullable: true),
                    AgentDocumentId = table.Column<long>(nullable: false),
                    AgentId = table.Column<long>(nullable: false),
                    FileId = table.Column<long>(nullable: false),
                    AgentDocumentCreationDate = table.Column<DateTime>(nullable: false),
                    AgentDocumentModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentDocumentArchive", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AgentArchive");

            migrationBuilder.DropTable(
                name: "AgentDocumentArchive");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class ProfilePackage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ProfilePackageId",
                table: "Payment",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ProfilePackage",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "GetDate()"),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ProfileId = table.Column<long>(nullable: false),
                    Type = table.Column<byte>(nullable: false),
                    Used = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfilePackage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProfilePackage_Profile_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Payment_ProfilePackageId",
                table: "Payment",
                column: "ProfilePackageId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfilePackage_ProfileId",
                table: "ProfilePackage",
                column: "ProfileId");

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_ProfilePackage_ProfilePackageId",
                table: "Payment",
                column: "ProfilePackageId",
                principalTable: "ProfilePackage",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payment_ProfilePackage_ProfilePackageId",
                table: "Payment");

            migrationBuilder.DropTable(
                name: "ProfilePackage");

            migrationBuilder.DropIndex(
                name: "IX_Payment_ProfilePackageId",
                table: "Payment");

            migrationBuilder.DropColumn(
                name: "ProfilePackageId",
                table: "Payment");
        }
    }
}

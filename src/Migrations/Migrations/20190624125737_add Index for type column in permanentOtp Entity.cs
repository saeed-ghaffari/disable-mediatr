﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addIndexfortypecolumninpermanentOtpEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PermanentOtp_ServiceId_UniqueIdentifier",
                table: "PermanentOtp");

            migrationBuilder.CreateIndex(
                name: "IX_PermanentOtp_ServiceId_UniqueIdentifier_type",
                table: "PermanentOtp",
                columns: new[] { "ServiceId", "UniqueIdentifier", "Type" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PermanentOtp_ServiceId_UniqueIdentifier_type",
                table: "PermanentOtp");

            migrationBuilder.CreateIndex(
                name: "IX_PermanentOtp_ServiceId_UniqueIdentifier",
                table: "PermanentOtp",
                columns: new[] { "ServiceId", "UniqueIdentifier" });
        }
    }
}

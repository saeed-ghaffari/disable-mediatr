﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addenableDiscountinthirdPartyService : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EnabledDiscount",
                table: "ThirdPartyService",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "EnabledLink",
                table: "ThirdPartyService",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EnabledDiscount",
                table: "ThirdPartyService");

            migrationBuilder.DropColumn(
                name: "EnabledLink",
                table: "ThirdPartyService");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class AddingFlagtomembershipdatamodels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "Flag",
                schema: "mem",
                table: "PrivatePersonMembership",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "Flag",
                schema: "mem",
                table: "BankingAccountMembership",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "Flag",
                schema: "mem",
                table: "AddressMembership",
                nullable: false,
                defaultValue: (byte)0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Flag",
                schema: "mem",
                table: "PrivatePersonMembership");

            migrationBuilder.DropColumn(
                name: "Flag",
                schema: "mem",
                table: "BankingAccountMembership");

            migrationBuilder.DropColumn(
                name: "Flag",
                schema: "mem",
                table: "AddressMembership");
        }
    }
}

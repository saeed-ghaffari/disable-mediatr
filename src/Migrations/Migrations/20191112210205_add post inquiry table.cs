﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addpostinquirytable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PostInquiry",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "GetDate()"),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Ssn = table.Column<string>(nullable: true),
                    BirthDate = table.Column<string>(nullable: true),
                    EndUsername = table.Column<string>(nullable: true),
                    EndIp = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    Successful = table.Column<bool>(nullable: false),
                    SuccessInquiry = table.Column<bool>(nullable: false),
                    HasException = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostInquiry", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Ssn_SuccessInquiry_CreationDate",
                table: "PostInquiry",
                columns: new[] { "Ssn", "SuccessInquiry", "CreationDate" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PostInquiry");
        }
    }
}

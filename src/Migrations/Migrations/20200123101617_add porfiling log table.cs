﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addporfilinglogtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProfilingRequestLog",
                schema: "log",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ServiceId = table.Column<long>(nullable: false),
                    AccessTokenPayload = table.Column<string>(maxLength: 1024, nullable: true),
                    Route = table.Column<string>(maxLength: 1024, nullable: true),
                    Method = table.Column<string>(maxLength: 10, nullable: true),
                    RequestBody = table.Column<string>(nullable: true),
                    RequestDateTime = table.Column<DateTime>(nullable: false),
                    Response = table.Column<string>(nullable: true),
                    ResponseDateTime = table.Column<DateTime>(nullable: true),
                    ResponseHash = table.Column<string>(maxLength: 1024, nullable: true),
                    SourceIp = table.Column<string>(maxLength: 100, nullable: true),
                    HasException = table.Column<bool>(nullable: false),
                    ExceptionMessage = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "GetDate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfilingRequestLog", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProfilingRequestLog",
                schema: "log");
        }
    }
}

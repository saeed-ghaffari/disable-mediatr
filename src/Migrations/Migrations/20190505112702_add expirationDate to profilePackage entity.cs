﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addexpirationDatetoprofilePackageentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ExpirationDate",
                table: "ProfilePackage",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExpirationDate",
                table: "ProfilePackage");
        }
    }
}

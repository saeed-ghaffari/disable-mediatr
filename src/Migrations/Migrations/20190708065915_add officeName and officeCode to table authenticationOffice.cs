﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addofficeNameandofficeCodetotableauthenticationOffice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OfficeCode",
                table: "AuthenticationOffices",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OfficeName",
                table: "AuthenticationOffices",
                maxLength: 500,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OfficeCode",
                table: "AuthenticationOffices");

            migrationBuilder.DropColumn(
                name: "OfficeName",
                table: "AuthenticationOffices");
        }
    }
}

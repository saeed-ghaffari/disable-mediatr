﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addindexibansuccessdatecreationdatetoibaninquirytable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Iban_SuccessInquiry",
                table: "IbanInquiry");

            migrationBuilder.CreateIndex(
                name: "IX_Iban_SuccessInquiry",
                table: "IbanInquiry",
                columns: new[] { "Iban", "SuccessInquiry", "CreationDate" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Iban_SuccessInquiry",
                table: "IbanInquiry");

            migrationBuilder.CreateIndex(
                name: "IX_Iban_SuccessInquiry",
                table: "IbanInquiry",
                columns: new[] { "Iban", "SuccessInquiry" });
        }
    }
}

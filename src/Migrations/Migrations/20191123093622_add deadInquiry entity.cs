﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class adddeadInquiryentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DeadInquiry",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "GetDate()"),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    UniqueIdentifier = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: false),
                    PersianBirthDate = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    IsOk = table.Column<bool>(nullable: false),
                    SuccessInquiry = table.Column<bool>(nullable: false),
                    HasException = table.Column<bool>(nullable: false),
                    FileName = table.Column<Guid>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeadInquiry", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Dead_UniqueIdentifier_BirthDate_LastName_FromDate_ToDate_SuccessInquiry",
                table: "DeadInquiry",
                columns: new[] { "UniqueIdentifier", "BirthDate", "LastName", "FromDate", "ToDate", "SuccessInquiry" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DeadInquiry");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addedusedFlagtoProfilePackageandmarkUsedpropertyasreadonly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Used",
                table: "ProfilePackage");

            migrationBuilder.AlterColumn<byte>(
                name: "Type",
                table: "ProfilePackage",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<byte>(
                name: "UsedFlag",
                table: "ProfilePackage",
                nullable: false,
                defaultValue: (byte)0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UsedFlag",
                table: "ProfilePackage");

            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "ProfilePackage",
                nullable: false,
                oldClrType: typeof(byte));

            migrationBuilder.AddColumn<bool>(
                name: "Used",
                table: "ProfilePackage",
                nullable: false,
                defaultValue: false);
        }
    }
}

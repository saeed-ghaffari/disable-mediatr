﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addDescriptioninissuancecodeandinvestorCodeinresponse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "InvestorCode",
                table: "IssuanceStockCodeResponse",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "IssuanceStockCode",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "IssuanceStockCode",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvestorCode",
                table: "IssuanceStockCodeResponse");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "IssuanceStockCode");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "IssuanceStockCode");
        }
    }
}

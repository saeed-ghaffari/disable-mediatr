﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class ReanmeMobileAndStatusIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameIndex(
                name: "IX_Mobile_Status",
                table: "Profile",
                newName: "IX_Profile_Mobile_Status");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameIndex(
                name: "IX_Profile_Mobile_Status",
                table: "Profile",
                newName: "IX_Mobile_Status");
        }
    }
}

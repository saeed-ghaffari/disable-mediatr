﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addPermanentOtpEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PermanentOtp",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "GetDate()"),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ServiceId = table.Column<long>(nullable: false),
                    UniqueIdentifier = table.Column<string>(maxLength: 15, nullable: true),
                    ExpirationDate = table.Column<DateTime>(nullable: false),
                    Otp = table.Column<string>(maxLength: 15, nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermanentOtp", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PermanentOtp_ServiceId_UniqueIdentifier",
                table: "PermanentOtp",
                columns: new[] { "ServiceId", "UniqueIdentifier" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PermanentOtp");
        }
    }
}

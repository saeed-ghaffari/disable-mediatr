﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addissuancestockcodeandresponsetable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.CreateTable(
                name: "IssuanceStockCode",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ProfileId = table.Column<long>(nullable: false),
                    UniqueIdentifier = table.Column<string>(nullable: true),
                    Successful = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IssuanceStockCode", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IssuanceStockCode_Profile_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IssuanceStockCodeResponse",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    IssuanceStockCodeId = table.Column<long>(nullable: false),
                    LegacyCode = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IssuanceStockCodeResponse", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IssuanceStockCodeResponse_IssuanceStockCode_IssuanceStockCodeId",
                        column: x => x.IssuanceStockCodeId,
                        principalTable: "IssuanceStockCode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IssuanceStockCode_ProfileId",
                table: "IssuanceStockCode",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_IssuanceStockCodeResponse_IssuanceStockCodeId",
                table: "IssuanceStockCodeResponse",
                column: "IssuanceStockCodeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IssuanceStockCodeResponse");

            migrationBuilder.DropTable(
                name: "IssuanceStockCode");

          
        }
    }
}

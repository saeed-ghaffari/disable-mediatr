﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addlegacyOriginalandlegacyUnicodetolegacyiquirytable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LagecyCodeUnicode",
                table: "LegacyCodeResponse",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LegacyCodeOrginal",
                table: "LegacyCodeResponse",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LagecyCodeUnicode",
                table: "LegacyCodeResponse");

            migrationBuilder.DropColumn(
                name: "LegacyCodeOrginal",
                table: "LegacyCodeResponse");
        }
    }
}

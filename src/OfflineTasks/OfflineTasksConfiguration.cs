﻿namespace Cerberus.OfflineTasks
{
    public class OfflineTasksConfiguration
    {
        public long EdalatClubServiceId { get; set; }
        public int RunnerId { get; set; }
        public bool EnableCache { get; set; }
    }
}
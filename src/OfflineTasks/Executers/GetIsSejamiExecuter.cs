﻿using System;
using System.Collections.Generic;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.OfflineTasks.Model;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Domain.Enum;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;
namespace Cerberus.OfflineTasks.Executers
{
    public class GetIsSejamiExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;



        public GetIsSejamiExecuter(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IProfileApiService profileApiService = scope.ServiceProvider.GetRequiredService<IProfileApiService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();


                GetProfileTaskParam taskParam;

                try
                {
                    taskParam = JsonConvert.DeserializeObject<GetProfileTaskParam>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                var callBackResponse = new GetIsSejamCallbackData()
                {
                    Success = new List<KeyValuePair<string, IsSejamiViewModel>>(),
                    Failed = new List<CallbackErrorDataModel>(),
                    ReferenceId = task.Id
                };

                foreach (var uniqueIdentifier in taskParam.UniqueIdentifiers)
                {
                    if (!uniqueIdentifier.IsPrivatePersonUniqueIdentifierValid() && !uniqueIdentifier.IsLegalPersonUniqueIdentifierValid())
                    {
                        callBackResponse.Failed.Add(new CallbackErrorDataModel()
                        {
                            UniqueIdentifier = uniqueIdentifier,
                            Meta = new[] { "invalid uniqueIdentifier" }
                        });

                        continue;
                    }

                    var profile = await profileApiService.GetByUniqueIdentifierAsync(uniqueIdentifier);
                    if (profile == null)
                    {
                        callBackResponse.Failed.Add(new CallbackErrorDataModel()
                        {
                            UniqueIdentifier = uniqueIdentifier,
                            Meta = new[] { "profile not found" }
                        });

                        continue;
                    }

                    callBackResponse.Success.Add(new KeyValuePair<string, IsSejamiViewModel>(uniqueIdentifier,
                        new IsSejamiViewModel { IsSejami = profile.Status == ProfileStatus.Sejami }));
                }

                //create callback task
                await taskRepository.AddAsync(new ScheduledTask()
                {
                    Type = CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Value,
                    DueTime = DateTime.Now,
                    GroupId = CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Key,
                    RefId = taskParam.ServiceId,
                    OriginalTaskId = task.Id,
                    Params = JsonConvert.SerializeObject(new CallbackThirdPartyServicesTaskParams()
                    {
                        CallbackUrl = taskParam.CallbackUrl,
                        Payload = callBackResponse,
                        ServiceId = taskParam.ServiceId
                    }),
                });

                return TaskStatus.Done;
            }
        }
    }
}
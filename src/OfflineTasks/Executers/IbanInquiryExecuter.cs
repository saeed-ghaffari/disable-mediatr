﻿using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using System;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.ViewModel.Inquiries;
using Cerberus.Service;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class IbanInquiryExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }
        public IbanInquiryExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<TasksManager.Core.Enum.TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IIbanInquiryService ibanInquiry = scope.ServiceProvider.GetRequiredService<IIbanInquiryService>();
                var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                var bankingAccountService = scope.ServiceProvider.GetRequiredService<IBankingAccountService>();
                IIbanInquiryService ibanInquiryService = scope.ServiceProvider.GetRequiredService<IIbanInquiryService>();
                var privatePersonService = scope.ServiceProvider.GetRequiredService<IPrivatePersonService>();
                var legalPersonService = scope.ServiceProvider.GetRequiredService<ILegalPersonService>();
                IOptions<InquiryConfiguration> config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();

                Console.WriteLine("IbanInquiryExecuter....");

                IbanInquiryVM param;
                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<IbanInquiryVM>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;

                var mobile = param.Mobile;

                var isMsisdnValid = ValidationHelper.IsMsisdnValid(ref mobile, out var carrier);

                if (!isMsisdnValid || carrier == null)
                    return TaskStatus.BadParams;


                var account = await bankingAccountService.GetAsync(param.ReferenceId);
                if (account == null || account.IsDeleted || account.IsConfirmed == true)
                    return TaskStatus.Canceled;

                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                {
                    account.IsConfirmed = false;
                    account.Locked = false;
                    await bankingAccountService.UpdateAsync(account);

                    return TaskStatus.Done;
                }

                var privatePerson = await privatePersonService.GetByProfileIdAsync(task.RefId.GetValueOrDefault());

                if (privatePerson != null && privatePerson.IsConfirmed == null)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.IbanInquiryTimeToLater);
                    task.Status = TaskStatus.Pending;
                    task.RecoverySequence++;
                    await taskRepository.AddAsync(task);

                    return TaskStatus.Recovered;
                }

                if (privatePerson != null && privatePerson.IsConfirmed == false)
                {
                    //account.IsConfirmed = false;
                    //account.Locked = false;
                    //await bankingAccountRepository.UpdateAsync(account);
                    return TaskStatus.Done;
                }

                var legalPerson = await legalPersonService.GetByProfileIdAsync(task.RefId.GetValueOrDefault());

                if (legalPerson != null && legalPerson.IsConfirmed == null)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.IbanInquiryTimeToLater);
                    task.Status = TaskStatus.Pending;
                    task.RecoverySequence++;
                    await taskRepository.AddAsync(task);

                    return TaskStatus.Recovered;
                }

                if (legalPerson != null && legalPerson.IsConfirmed == false)
                {
                    //account.IsConfirmed = false;
                    //account.Locked = false;
                    //await bankingAccountRepository.UpdateAsync(account);
                    return TaskStatus.Done;
                }

                var inquiry = await ibanInquiry.GetAsync(param.Iban);

                if (inquiry == null || inquiry.HasException)
                {

                    task.DueTime = DateTime.Now.AddMinutes(config.Value.InquiryCheckNextTime);
                    task.Status = TaskStatus.Pending;
                    task.RecoverySequence++;
                    await taskRepository.AddAsync(task);

                    return TaskStatus.Recovered;
                }

                if (inquiry.Successful == false)
                {
                    account.IsConfirmed = false;
                    account.Locked = false;
                    await bankingAccountService.UpdateAsync(account);
                    return TaskStatus.Done;
                }

                var name = await profileService.GetFullName(task.RefId.GetValueOrDefault());

                var userProfile = await profileService.GetAsync(task.RefId.GetValueOrDefault());
                string inquirySide = PersianChars.GetClearChars(
                    (inquiry.FirstName ?? "").Replace("0", "").Trim() + " " +
                    (inquiry.LastName ?? "").Replace("0", "").Trim());
                if ((userProfile.Type == ProfileOwnerType.IranianLegalPerson ||
                     userProfile.Type == ProfileOwnerType.ForeignLegalPerson)
                    && inquiry.FirstName == inquiry.LastName)
                    inquirySide = PersianChars.GetClearChars((inquiry.FirstName ?? "").Trim());

                string personSide = PersianChars.GetClearChars(name);
                if (personSide == inquirySide)
                {
                    account.IsConfirmed = true;
                    account.Locked = true;
                    await bankingAccountService.UpdateAsync(account);
                    return TaskStatus.Done;
                }

                inquiry.Successful = false;
                await ibanInquiryService.UpdateAsync(inquiry);
                account.IsConfirmed = false;
                account.Locked = false;
                await bankingAccountService.UpdateAsync(account);

                return TaskStatus.Done;
            }


        }
    }
}

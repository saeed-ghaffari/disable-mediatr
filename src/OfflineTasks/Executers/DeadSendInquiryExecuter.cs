﻿using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;
using System;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Service;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Cerberus.OfflineTasks.Executers
{
    class DeadSendInquiryExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public DeadSendInquiryExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var deadListInquiryService = scope.ServiceProvider.GetRequiredService<IDeadInquiryService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
               
                IOptions<InquiryConfiguration> config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();
                var messageConfig = scope.ServiceProvider.GetRequiredService<IOptions<IssuanceStockCodeConfiguration>>();
                var messageService = scope.ServiceProvider.GetRequiredService<IMessagingService>();
                //if (task.RefId.GetValueOrDefault() <= 0)
                //    return TaskStatus.BadParams;


                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                {

                    var mobiles = messageConfig.Value.SendErrorMobiles;

                    foreach (var m in mobiles)
                    {
                        await messageService.SendSmsAsync(
                            null,
                            m,
                            null,
                            string.Format(
                                $"دیشب نخواستم بیدارت کنم ، سرویس فوتی های ثبت احوال خطا داد،یادت نره یه بررسی بکن "),DateTime.Now.AddHours(6));
                    }
                    return TaskStatus.Canceled;
                }

                var fromDate = DateTime.Now.AddDays(-config.Value.DeadInquiryFromAddDay).ToPersianDate("yyyyMMdd");
                var toDate = DateTime.Now.AddDays(-config.Value.DeadInquiryToAddDay).ToPersianDate("yyyyMMdd");
                var inquiry = await deadListInquiryService.SendAsync(fromDate,toDate);

                if (inquiry == null || inquiry.HasException || inquiry.Successful == false)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.DeadAddTimeToDownloadRequest);
                    task.RecoverySequence++;
                    task.Status = TaskStatus.Pending;
                    await taskRepository.AddAsync(task);
                    return TaskStatus.Recovered;

                }
            

                await taskRepository.AddAsync(new ScheduledTask
                {
                    Type = CerberusTasksIdentifierHelper.DeadDownloadInquiry.Value,
                    DueTime = DateTime.Now.AddMinutes(config.Value.DeadAddTimeToDownloadRequest),
                    GroupId = CerberusTasksIdentifierHelper.DeadDownloadInquiry.Key,
                    Params = inquiry.Id.ToString(),
                    RefId = task.Id,
                    Status = TaskStatus.Pending
                });

                return TaskStatus.Done;
            }
        }
    }
}

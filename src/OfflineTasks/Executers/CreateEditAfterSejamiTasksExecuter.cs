﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.Service;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class CreateEditAfterSejamiTasksExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;


        public CreateEditAfterSejamiTasksExecuter(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IProfileService profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                IBankingAccountService bankingAccountService = scope.ServiceProvider.GetRequiredService<IBankingAccountService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                IOptions<InquiryConfiguration> config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();
                IProfilePackageService profilePackageService =
                    scope.ServiceProvider.GetRequiredService<IProfilePackageService>();
                IProfileHistoryRepository profileHistoryRepository = scope.ServiceProvider.GetService<IProfileHistoryRepository>();


                if (!long.TryParse(task.Params, out var profileId))
                    return TaskStatus.BadParams;


                var profile = (await profileService.GetGenericAsync(x => x.Id == task.RefId,
                    inc => { return inc.Include(x => x.PrivatePerson).Include(c => c.Accounts); }, null)).FirstOrDefault();

                if (profile == null)
                    return TaskStatus.BadParams;


                if (profile.IsEditBankingAccountSemiSejami()||profile.IsEditPrivatePersonSemiSejami()||profile.IsEditBankingAccountAndDeleteAgentEighteen() ||profile.IsEditPrivatePersonAndDeleteAgentEighteen())
                {

                    var package = await profilePackageService.GetUnusedPackagesAsync(profile.Id,
                        PackageType.EditAllData, PackageUsedFlag.EditPrivatePersonInfo);
                    if (package != null)
                    {
                        if (profile.PrivatePerson.IsConfirmed == null)
                        {
                            await profileHistoryRepository.CreateAsync(new ProfileHistory()
                            {
                                ChangedTo = ProfileStatus.SemiSejami,
                                ReferenceId = ProfileHistoryReferenceType.ChangePrivatePerson.ToString(),
                                ProfileId = profile.Id,
                            });


                            profile.Status = ProfileStatus.SemiSejami;
                            await profileService.UpdateAsync(profile);
                            //await profileService.UpdateStatusAsync(profile.Id, ProfileStatus.SemiSejami);

                            var p = new UpdatePrivatePersonTaskParam()
                            {
                                Mobile = profile.Mobile.ToString(),
                                ReferenceId = profile.PrivatePerson.Id,
                                Ssn = profile.UniqueIdentifier,
                                BirthDate = profile.PrivatePerson?.BirthDate.ToPersianDate()
                            };
                            await CreateTaskAsync(CerberusTasksIdentifierHelper.UpdatePrivatePerson, profileId, p,
                                taskRepository);

                            foreach (var profileAccount in profile.Accounts)
                            {
                                var taskparam = new UpdateBankingAccountTaskParam()
                                {
                                    Mobile = profile.Mobile.ToString(),
                                    ReferenceId = profileAccount.Id,
                                    Iban = profileAccount.Sheba,
                                    TaskCreator = TaskCreatorType.UpdatePrivatePerson
                                };
                                await CreateTaskAsync(CerberusTasksIdentifierHelper.UpdateBanckingAccount, profileId,
                                    taskparam,
                                    taskRepository, DateTime.Now.AddMinutes(config.Value.IbanInquiryTimeToLater));

                                profileAccount.IsConfirmed = null;
                                profileAccount.Locked = true;
                                await bankingAccountService.UpdateAsync(profileAccount);

                            }
                            return TaskStatus.Done;
                        }
                      
                    }
                   
                    var packageAccount = await profilePackageService.GetUnusedPackagesAsync(profile.Id,
                        PackageType.EditAllData, PackageUsedFlag.EditBankingAccount);
                    if (packageAccount != null)
                    {
                        if (profile.Accounts.Any(x => x.IsConfirmed == null))
                        {
                            foreach (var profileAccount in profile.Accounts
                                .Where(x => x.IsConfirmed == null).ToList())
                            {
                                //await profileHistoryRepository.CreateAsync(new ProfileHistory()
                                //{
                                //    ChangedTo = ProfileStatus.SemiSejami,
                                //    ReferenceId = ProfileHistoryReferenceType.ChangeBankingAccount.ToString(),
                                //    ProfileId = profile.Id,
                                //});

                                //profile.Status = ProfileStatus.SemiSejami;
                                //await profileService.UpdateAsync(profile);

                                var p = new UpdateBankingAccountTaskParam()
                                {
                                    Mobile = profile.Mobile.ToString(),
                                    ReferenceId = profileAccount.Id,
                                    Iban = profileAccount.Sheba,
                                    TaskCreator = TaskCreatorType.UpdateBankingAccount
                                };
                                await CreateTaskAsync(CerberusTasksIdentifierHelper.UpdateBanckingAccount, profileId, p,
                                    taskRepository, DateTime.Now.AddMinutes(config.Value.IbanInquiryTimeToLater));

                                profileAccount.Locked = true;
                                await bankingAccountService.UpdateAsync(profileAccount);

                            }
                        }
                        else if (profile.Accounts.All(x => x.IsConfirmed == true))
                        {
                            await profileHistoryRepository.CreateAsync(new ProfileHistory()
                            {
                                ChangedTo = ProfileStatus.Sejami,
                                ReferenceId = ProfileHistoryReferenceType.UpdateBankingAccount.ToString(),
                                ProfileId = profile.Id,
                            });

                            profile.Status = ProfileStatus.Sejami;
                            await profileService.UpdateAsync(profile);

                        }

                        return TaskStatus.Done;

                    }
                }


                return TaskStatus.Done;

            }
        }

        public async Task CreateTaskAsync(KeyValuePair<long, string> taskGroup, long profileId, object taskParams, ITaskRepository taskRepository, DateTime? dueTime = null)
        {

            var param = JsonConvert.SerializeObject(taskParams);
            await taskRepository.AddAsync(new ScheduledTask()
            {
                RefId = profileId,
                DueTime = dueTime ?? DateTime.Now,
                GroupId = taskGroup.Key,
                Type = taskGroup.Value,
                Params = param,
                Status = TaskStatus.Pending,
                RecoverySequence = 0
            });
        }


    }
}
﻿using System;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model.Report;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;
using Microsoft.Extensions.Options;

namespace Cerberus.OfflineTasks.Executers
{
    public class SendReportBySmsExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public SendReportBySmsExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => true;
        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {

            using (var scope = _serviceProvider.CreateScope())
            {
                var messagingService = scope.ServiceProvider.GetRequiredService<IMessagingService>();
                var smsReportRepository = scope.ServiceProvider.GetRequiredService<ISmsReportRepository>();
                var adminReportOptions = scope.ServiceProvider.GetRequiredService<IOptions<AdminReportConfiguration>>().Value;
                SendReportBySmsTaskParam param;
                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<SendReportBySmsTaskParam>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                var fromDate = DateTime.Now.AddDays(adminReportOptions.PreDayForFromDate);


                var result = await smsReportRepository.GetReportAsync(
                    fromDate,
                    DateTime.Now.AddDays(adminReportOptions.PreDayForToDate));

                string textDetail = MakeSmsText(adminReportOptions, fromDate, result);
                string textSummary = MakeSummarySmsText(adminReportOptions, fromDate, result);

                var groupMembers = await smsReportRepository.GetGroupMemberAsync();

                foreach (var groupMember in groupMembers)
                {
                    if (groupMember.GroupId == 1)
                        await messagingService.SendSmsAsync(null, groupMember.Mobile, null, textDetail, DateTime.Now.AddHours(2));
                    if (groupMember.GroupId == 2)
                        await messagingService.SendSmsAsync(null, groupMember.Mobile, null, textSummary, DateTime.Now.AddHours(4));
                }

                return TaskStatus.Done;
            }
        }

        private static string MakeSmsText(AdminReportConfiguration adminReportOptions, DateTime fromDate, ReportModel result)
        {
            var text = string.Format($"{adminReportOptions.HeaderMessage}", fromDate.ToPersianDate(), Environment.NewLine);

            text += string.Format($"{adminReportOptions.CountMessage}", result.RegistrationCount, Environment.NewLine,
                result.AgentCount, result.LegalCount, result.VerifiedCount, result.SejamiCount);



            text += string.Format($"{adminReportOptions.LinkMessage}", result.Link, Environment.NewLine);

            text += string.Format($"{adminReportOptions.ProvinceMessage}", result.Province, Environment.NewLine);

            text += string.Format($"{adminReportOptions.EntryNodeMessage}", result.EntryNode, Environment.NewLine);

            text += string.Format($"{adminReportOptions.ClubMessage}", result.Club, Environment.NewLine);

            text += string.Format($"{adminReportOptions.SejamiBrokerMessage}", result.SejamiBroker, Environment.NewLine);

            text += string.Format($"{adminReportOptions.SejamiPishkhanMessage}", result.SejamiPishkhan, Environment.NewLine);

            text += string.Format($"{adminReportOptions.SejamiPishkhanMessage2}", result.SejamiPishkhan2, Environment.NewLine);

            text += string.Format($"{adminReportOptions.SejamiBankMessage}", result.SejamiBank, Environment.NewLine);

            text += string.Format($"{adminReportOptions.SejamiGheirHozuriMessage}", result.SejamiGheirHozuri, Environment.NewLine);


            return text.Replace("*", Environment.NewLine);
        }
        private static string MakeSummarySmsText(AdminReportConfiguration adminReportOptions, DateTime fromDate, ReportModel result)
        {
            var text = string.Format($"{adminReportOptions.HeaderMessage}", fromDate.ToPersianDate(), Environment.NewLine);

            text += string.Format($"{adminReportOptions.SummaryCountMessage}", result.RegistrationCount, Environment.NewLine, result.SejamiCount);

            return text;
        }
    }
}
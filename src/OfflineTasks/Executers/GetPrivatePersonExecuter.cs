﻿using System; using System.Collections.Generic; using AutoMapper; using Cerberus.Domain.ViewModel.Api; using Cerberus.Domain.ViewModel.OfflineTasks; using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams; using Cerberus.OfflineTasks.Model; using Cerberus.TasksManager.Core.Interface; using Cerberus.TasksManager.Core.Model; using Cerberus.Domain.Interface.Inquiries; using Cerberus.Domain.Interface.Services; using Cerberus.Domain.Model.Inquiries; using Cerberus.Utility; using Microsoft.Extensions.DependencyInjection; using Newtonsoft.Json; using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus; using Cerberus.Domain.Model.StatusData;

namespace Cerberus.OfflineTasks.Executers {     public class GetPrivatePersonExecuter : ITaskExecuter     {         private readonly IServiceProvider _serviceProvider;          public GetPrivatePersonExecuter(             IServiceProvider serviceProvider)         {             _serviceProvider = serviceProvider;         }          public bool IsReusable => false;          public bool CanHandleTask(string param)         {             return true;         }          public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)         {             using (var scope = _serviceProvider.CreateScope())             {                  var taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();                 var nocrInquiryService = scope.ServiceProvider.GetRequiredService<INocrInquiryService>();                 var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();                 var mapper = scope.ServiceProvider.GetRequiredService<IMapper>();                  List<StatusData> validUniqueIdentifiers = new List<StatusData>();                 GetPrivatePersonTaskParam taskParam;                  try                 {                     taskParam = JsonConvert.DeserializeObject<GetPrivatePersonTaskParam>(task.Params);                 }                 catch                 {                     return TaskStatus.BadParams;                 }                  var callBackResponse = new GetPrivatePersonCallbackData()                 {                     Success = new List<KeyValuePair<string, PrivatePersonViewModel>>(),                     Failed = new List<CallbackErrorDataModel>(),                     ReferenceId = task.Id                 };                  foreach (var uniqueIdentifier in taskParam.UniqueIdentifiers)                 {                     if (!uniqueIdentifier.IsValidNationalCode())                     {                         callBackResponse.Failed.Add(new CallbackErrorDataModel()                         {                             UniqueIdentifier = uniqueIdentifier,                             Meta = new[] { "invalid uniqueIdentifier" }                         });                          continue;                     }

                    validUniqueIdentifiers.Add(new StatusData
                    {
                        NationalCode = uniqueIdentifier,
                        RefrenceNumber = task.Id.ToString()
                    });
                 }

                var batchPrivatePersonResult = await profileService.GetBatchPrivatePerson(task.Id.ToString(), validUniqueIdentifiers);
                foreach (var item in batchPrivatePersonResult)
                {
                    if (string.IsNullOrEmpty(item.UniqueIdentifier) || item.Status < (byte)taskParam.ProfileStatus)
                    {
                        callBackResponse.Failed.Add(new CallbackErrorDataModel()
                        {
                            UniqueIdentifier = item.NationalCode,
                            Meta = new[] { "profile not found" }
                        });
                    }
                    else
                    {
                        var privatePersonViewModel = mapper.Map<PrivatePersonViewModel>(item);
                        if (item.ShNumber == item.NationalCode)
                            privatePersonViewModel.ShNumber = "0";
                        callBackResponse.Success.Add(
                            new KeyValuePair<string, PrivatePersonViewModel>(item.NationalCode, privatePersonViewModel));
                    }
                }


                //create callback task
                await taskRepository.AddAsync(new ScheduledTask()                 {                     Type = CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Value,                     DueTime = DateTime.Now,                     GroupId = CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Key,                     RefId = taskParam.ServiceId,                     OriginalTaskId = task.Id,                     Params = JsonConvert.SerializeObject(new CallbackThirdPartyServicesTaskParams()                     {                         CallbackUrl = taskParam.CallbackUrl,                         Payload = callBackResponse,                         ServiceId = taskParam.ServiceId                     }),                 });                  return TaskStatus.Done;             }         }       } } 
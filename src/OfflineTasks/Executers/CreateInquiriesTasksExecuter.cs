﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.ViewModel;
using Cerberus.Domain.ViewModel.Inquiries;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.Service;
using Cerberus.TasksManager.Core.Enum;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class CreateInquiriesTasksExecuter : ITaskExecuter
    {
        private readonly IServiceScopeFactory _serviceProvider;

        public CreateInquiriesTasksExecuter(
            IServiceScopeFactory serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                var bankingAccountService = scope.ServiceProvider.GetRequiredService<IBankingAccountService>();
                var privatePersonService = scope.ServiceProvider.GetRequiredService<IPrivatePersonService>();
                var agentService = scope.ServiceProvider.GetRequiredService<IAgentService>();
                var paymentService = scope.ServiceProvider.GetRequiredService<IPaymentService>();

                CreateInquiryParams taskParam;

                try
                {
                    taskParam = JsonConvert.DeserializeObject<CreateInquiryParams>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }



                var profile = await profileService.GetWithNoCacheAsync(taskParam.ProfileId);
                if (profile == null)
                    return TaskStatus.BadParams;

                if (profile.Status >= ProfileStatus.TraceCode)
                    return TaskStatus.Done;





                var payments = await paymentService.GetListAsync(x => x.ProfileId == profile.Id);
                if (payments.Any(x => x.ReferenceServiceId != null && x.IsConfirmedReference == false))
                    return TaskStatus.Canceled;

                profile.Status = ProfileStatus.PendingValidation;
                await profileService.UpdateStatusAsync(profile);


                var accounts = await bankingAccountService.GetListAsync(profile.Id);
                var bankingAccounts = accounts.ToList();

                if (profile.Type == ProfileOwnerType.IranianPrivatePerson)
                {
                    //insert NocrInquiry
                    var privatePerson = await privatePersonService.GetByProfileIdAsync(profile.Id);
                    if (privatePerson != null)
                    {
                        if (privatePerson.IsConfirmed!=true)
                        {
                            //lock privatePerson to edit
                            privatePerson.Locked = true;
                            privatePerson.IsConfirmed = null;
                            await privatePersonService.UpdateAsync(privatePerson);
                            var nocrInquiryParam = new NocrInquiryVM()
                            {
                                Mobile = profile.Mobile.ToString(),
                                ReferenceId = privatePerson.Id,
                                BirthDate = privatePerson.BirthDate.ToPersianDate(),
                                Ssn = profile.UniqueIdentifier
                            };

                            await CreateTaskAsync(CerberusTasksIdentifierHelper.NocrInquiry, taskParam.ProfileId,
                                nocrInquiryParam, _serviceProvider);

                            // if private Person's lock is false we should insert banking accounts tasks


                            //foreach (var bankingAccount in bankingAccounts)
                            //{

                            //    bankingAccount.Locked = true;
                            //    bankingAccount.IsConfirmed = null;
                            //    await bankingAccountService.UpdateAsync(bankingAccount);

                            //    var ibanInquiryParam = new IbanInquiryVM()
                            //    {
                            //        Mobile = profile.Mobile.ToString(),
                            //        Iban = bankingAccount.Sheba,
                            //        ReferenceId = bankingAccount.Id

                            //    };

                            //    await CreateTaskAsync(CerberusTasksIdentifierHelper.IbanInquiry, taskParam.ProfileId,

                            //        ibanInquiryParam, _serviceProvider, DateTime.Now.AddSeconds(30));

                            //}
                        }

                    }
                    var agent = await agentService.GetByProfileIdAsync(profile.Id);

                    //insert AgentInquiry
                    if (agent != null)
                    {


                        if (!agent.Locked)
                        {
                            //lock agent to edit
                            agent.Locked = true;
                            await agentService.UpdateAsync(agent);
                            var agentInquiryParam = new AgentInquiryVm()
                            {
                                Mobile = profile.Mobile.ToString(),
                                ReferenceId = agent.Id,
                                SendSmsVerificationAgent = false,
                                AgentProfileId = agent.AgentProfileId
                            };

                            await CreateTaskAsync(CerberusTasksIdentifierHelper.AgentInquiry, taskParam.ProfileId,
                                agentInquiryParam, _serviceProvider, DateTime.Now.AddMinutes(2));
                        }
                    }

                    var msisdnService = scope.ServiceProvider.GetRequiredService<IMsisdnService>();
                    var msisdn = await msisdnService.GetByProfileIdAsync(taskParam.ProfileId);
                    if (msisdn == null && agent == null || taskParam.Mobile != null || (!Tools.IsUnder18Years(privatePerson?.BirthDate) && msisdn?.IsConfirmed != true))
                    {
                        var p = new ShahkarParams
                        {
                            Mobile = profile.Mobile,
                            UniqueIdentifier = profile.UniqueIdentifier,
                            NewMobile = taskParam.Mobile,
                            Status = profile.Status
                        };
                        await CreateTaskAsync(CerberusTasksIdentifierHelper.ShahkarInquiry, taskParam.ProfileId, p, _serviceProvider, DateTime.Now.AddSeconds(30));
                    }



                }
                else if (profile.Type == ProfileOwnerType.IranianLegalPerson)
                {
                    ILegalPersonService legalPersonService = scope.ServiceProvider.GetRequiredService<ILegalPersonService>();

                    var legalPerson = await legalPersonService.GetByProfileIdAsync(profile.Id);

                    if (legalPerson != null)
                    {
                        if (legalPerson.IsConfirmed!=true)
                        {
                            //lock legalPerson to edit
                            legalPerson.Locked = true;
                            legalPerson.IsConfirmed = null;
                            await legalPersonService.UpdateAsync(legalPerson);
                            //insert ilenc inquiry
                            var ilencInquiryParam = new IlencInquiryVM()
                            {
                                Mobile = profile.Mobile.ToString(),
                                ReferenceId = legalPerson.Id,
                                NationalCode = profile.UniqueIdentifier
                            };

                            await CreateTaskAsync(CerberusTasksIdentifierHelper.IlencInquiry, taskParam.ProfileId,
                                ilencInquiryParam, _serviceProvider);

                            // if legal Person's lock is false we should insert banking accounts tasks

                            //foreach (var bankingAccount in bankingAccounts)
                            //{
                            //    bankingAccount.Locked = true;
                            //    bankingAccount.IsConfirmed = null;
                            //    await bankingAccountService.UpdateAsync(bankingAccount);

                            //    var ibanInquiryParam = new IbanInquiryVM()
                            //    {
                            //        Mobile = profile.Mobile.ToString(),
                            //        Iban = bankingAccount.Sheba,
                            //        ReferenceId = bankingAccount.Id

                            //    };

                            //    await CreateTaskAsync(CerberusTasksIdentifierHelper.IbanInquiry, taskParam.ProfileId,
                            //        ibanInquiryParam, _serviceProvider, DateTime.Now.AddSeconds(30));

                            //}
                        }
                    }
                }

                IOptions<InquiryConfiguration> config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();

                // banking accounts

                foreach (var bankingAccount in bankingAccounts)
                {
                    if (bankingAccount.IsConfirmed!=true)
                    {
                        //lock bankingAccount to edit s

                        bankingAccount.Locked = true;
                        bankingAccount.IsConfirmed = null;
                        await bankingAccountService.UpdateAsync(bankingAccount);

                        var ibanInquiryParam = new IbanInquiryVM()
                        {
                            Mobile = profile.Mobile.ToString(),
                            Iban = bankingAccount.Sheba,
                            ReferenceId = bankingAccount.Id

                        };

                        await CreateTaskAsync(CerberusTasksIdentifierHelper.IbanInquiry, taskParam.ProfileId,
                            ibanInquiryParam, _serviceProvider, DateTime.Now.AddMinutes(1));
                    }
                }


                IRecurringTaskRepository recurringTaskRepository = scope.ServiceProvider.GetRequiredService<IRecurringTaskRepository>();


                var exisTask = await recurringTaskRepository.GetAsync(CerberusTasksIdentifierHelper.InquiryChecker.Value,
                    taskParam.ProfileId);
                var recurringTasks = exisTask.ToList();
                if (recurringTasks.Any())
                {
                    foreach (var recurringTask in recurringTasks)
                    {
                        await recurringTaskRepository.EnableTaskAsync(recurringTask.Id, false);
                    }

                }

                await CreateRecuringTaskAsync(CerberusTasksIdentifierHelper.InquiryChecker, taskParam.ProfileId, config.Value, _serviceProvider);
                return TaskStatus.Done;

            }
        }

        public async Task CreateTaskAsync(KeyValuePair<long, string> taskGroup, long profileId, object taskParams, IServiceScopeFactory scopeFactory, DateTime? dueTime = null)
        {
            using (var scope = scopeFactory.CreateScope())
            {
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();

                var param = JsonConvert.SerializeObject(taskParams);
                await taskRepository.AddAsync(new ScheduledTask()
                {
                    RefId = profileId,
                    DueTime = dueTime ?? DateTime.Now,
                    GroupId = taskGroup.Key,
                    Type = taskGroup.Value,
                    Params = param,
                    Status = TaskStatus.Pending,
                    RecoverySequence = 0
                });
            }

        }
        public async Task CreateRecuringTaskAsync(KeyValuePair<long, string> taskGroup, long profileId, InquiryConfiguration config,
            IServiceScopeFactory scopeFactory)
        {
            using (var scope = scopeFactory.CreateScope())
            {
                IRecurringTaskRepository recurringTaskRepository = scope.ServiceProvider.GetRequiredService<IRecurringTaskRepository>();
                await recurringTaskRepository.AddAsync(new RecurringTask()
                {
                    RefId = profileId,
                    NextExecution = DateTime.Now.AddMinutes(config.CheckRecuringTaskInterval),
                    GroupId = taskGroup.Key,
                    Type = taskGroup.Value,
                    Params = null,
                    Status = RecurringTaskStatus.Idle,
                    IntervalPeriod = RecurringIntervalPeriod.Minute,
                    Interval = config.CheckRecuringTaskInterval
                });
            }
        }

    }
}
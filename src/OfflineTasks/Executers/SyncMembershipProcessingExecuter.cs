﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories.Membership;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Membership;
using Cerberus.Domain.ViewModel.Membership;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.OfflineTasks.Model;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;     
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;
using ValidationContext = System.ComponentModel.DataAnnotations.ValidationContext;
using IMembershipService = Cerberus.Domain.Interface.Services.Membership.IMembershipService;

namespace Cerberus.OfflineTasks.Executers
{
    public class SyncMembershipProcessingExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IOptions<OfflineTasksConfiguration> _configuration;
        private IList<Bank> _banks;

            

        public SyncMembershipProcessingExecuter(
            IServiceProvider serviceProvider, IOptions<OfflineTasksConfiguration> configuration)
        {
            _serviceProvider = serviceProvider;
            _configuration = configuration;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                ILogger<SyncMembershipProcessingExecuter> logger =
                    scope.ServiceProvider.GetRequiredService<ILogger<SyncMembershipProcessingExecuter>>();
                IThirdPartyServiceService thirdPartyServiceService =
                    scope.ServiceProvider.GetRequiredService<IThirdPartyServiceService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                IMembershipService membershipService = scope.ServiceProvider.GetRequiredService<IMembershipService>();
                //IMembershipRepository membershipRepository =
                //    scope.ServiceProvider.GetRequiredService<IMembershipRepository>();
                 IProfileService profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                IMapper mapper = scope.ServiceProvider.GetRequiredService<IMapper>();
                IBankService bankService = scope.ServiceProvider.GetRequiredService<IBankService>();
                _banks = await bankService.GetListAsync();

                SyncMembershipTaskParams taskParams;
                try
                {
                    taskParams = JsonConvert.DeserializeObject<SyncMembershipTaskParams>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                var thirdPartyService = await thirdPartyServiceService.GetAsync(taskParams.ServiceId);
                if (thirdPartyService == null)
                {
                    logger.LogDebug(
                        $"bad params;third party services which specified in task params, not found.(taskId:{task.Id})");
                    return TaskStatus.BadParams;
                }

                if (taskParams.FromAdmin)// import from excel in admin panel
                {
                    foreach (var member in taskParams.Members)
                    {
                        var privatePerson = await membershipService.GetPrivatePersonAsync(member.UniqueIdentifier, taskParams.ServiceId);
                        if (privatePerson == null)
                            await membershipService.CreateAsync(new PrivatePersonMembership()
                            {
                                UniqueIdentifier = member.UniqueIdentifier,
                                Discount = member.PrivatePerson.Discount,
                                Mobile = member.PrivatePerson.Mobile,
                                CanEditAccounts = false,
                                ServiceId = taskParams.ServiceId,
                                Flag = MembershipFlag.None
                            });
                        else
                        {
                            privatePerson.Mobile = member.PrivatePerson.Mobile;
                            privatePerson.Discount = member.PrivatePerson.Discount;
                            await membershipService.UpdateAsync(privatePerson);
                        }

                        await membershipService.UpdateCacheAsync(taskParams.ServiceId, member.UniqueIdentifier, privatePerson,
                            MembershipDataType.PrivatePerson);
                    }

                    return TaskStatus.Done;
                }
                var result = new CreateMemberCallbackData()
                {
                    Success = new List<string>(),
                    Failed = new List<CreateMemberCallbackError>(),
                    ReferenceId = task.Id
                };


                var validMembers = new List<Member>();



                //validate 
                foreach (var member in taskParams.Members)
                {
                    var validationResult = await ValidateAndGetErrors(member, taskParams.ServiceId, profileService, membershipService);
                    if (validationResult == null)
                    {
                        result.Success.Add(member.UniqueIdentifier);
                        validMembers.Add(member);
                    }
                    else
                        result.Failed.Add(validationResult);
                }

                //add into cerberus
                if (validMembers.Count > 0)
                    foreach (var x in validMembers)
                    {
                        await membershipService.AddAsync(
                            serviceId: thirdPartyService.Id,
                            uniqueIdentifier: x.UniqueIdentifier,
                            privatePerson: MapPrivatePersonList(x, taskParams.ServiceId, mapper),
                            address: MapAddressesList(x, thirdPartyService.Id, mapper),
                            accounts: MapBankingAccount(x, thirdPartyService.Id, mapper));
                    }

                //create callback task
                await taskRepository.AddAsync(new ScheduledTask()
                {
                    Type = CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Value,
                    DueTime = DateTime.Now,
                    GroupId = CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Key,
                    RefId = taskParams.ServiceId,
                    OriginalTaskId = task.Id,
                    Params = JsonConvert.SerializeObject(new CallbackThirdPartyServicesTaskParams()
                    {
                        CallbackUrl = taskParams.CallbackUrl,
                        Payload = result,
                        ServiceId = taskParams.ServiceId
                    }),
                });

                return TaskStatus.Done;

            }
        }

        private List<BankingAccountMembership> MapBankingAccount(Member validMembers, long serviceId, IMapper mapper)
        {

            var result =
                mapper.Map<IEnumerable<MemberBankingAccountViewModel>, IEnumerable<BankingAccountMembership>>(validMembers.BankingAccounts).ToList();

            result.ForEach(x =>
            {
                x.UniqueIdentifier = validMembers.UniqueIdentifier;
                x.ServiceId = serviceId;

                if (_configuration.Value.EdalatClubServiceId == serviceId)
                    x.Flag = MembershipFlag.EdalatClub;

            });

            return result;
        }

        private AddressMembership MapAddressesList(Member validMember, long serviceId, IMapper mapper)
        {
            var result = mapper.Map<MemberAddressViewModel, AddressMembership>(validMember.Address);
            result.ServiceId = serviceId;
            result.UniqueIdentifier = validMember.UniqueIdentifier;
            if (_configuration.Value.EdalatClubServiceId == serviceId)
                result.Flag = MembershipFlag.EdalatClub;

            return result;
        }

        private PrivatePersonMembership MapPrivatePersonList(Member validMember, long serviceId, IMapper mapper)
        {

            var result = mapper.Map<MemberPrivatePerson, PrivatePersonMembership>(validMember.PrivatePerson);
            result.ServiceId = serviceId;
            result.UniqueIdentifier = validMember.UniqueIdentifier;

            if (validMember.PrivatePerson.BirthDate != null &&
                (validMember.PrivatePerson.BirthDate.Length < 10 ||
                 !DateTime.TryParse(validMember.PrivatePerson.BirthDate, out var dt)))
                result.BirthDate = null;

            if (_configuration.Value.EdalatClubServiceId == serviceId)
                result.Flag = MembershipFlag.EdalatClub;

            if (validMember.PrivatePerson.Discount < 50000)
                result.CanEditAccounts = false;


            return result;
        }


        private async Task<CreateMemberCallbackError> ValidateAndGetErrors(Member member, long serviceId, IProfileService profileService, IMembershipService membershipService)
        {
            //validate data model
            var validationContext = new ValidationContext(member);
            var validationResult = new List<ValidationResult>();
            if (!Validator.TryValidateObject(member, validationContext, validationResult, true))
            {
                return new CreateMemberCallbackError()
                {
                    UniqueIdentifier = member.UniqueIdentifier,
                    Meta = Array.ConvertAll(validationResult.ToArray(), o => o.ErrorMessage)
                };
            }

            //check profile existing with the same unique identifier
            var profile = await profileService.GetByUniqueIdentifierAsync(member.UniqueIdentifier);
            if (profile != null && profile.Status >= ProfileStatus.SuccessPayment)
            {
                return new CreateMemberCallbackError()
                {
                    UniqueIdentifier = member.UniqueIdentifier,
                    Meta = new[] { $"profile which specified by '{member.UniqueIdentifier}',already exists as active profile, in SEJAM DB." }
                };
            }

            //prevent to store duplicate member
            var memberPrivatePerson =
                await membershipService.GetPrivatePersonAsync(member.UniqueIdentifier, serviceId);

            if (memberPrivatePerson != null)
                return new CreateMemberCallbackError()
                {
                    UniqueIdentifier = member.UniqueIdentifier,
                    Meta = new[] { $"profile which specified by '{member.UniqueIdentifier}',already exists, in SEJAM DB." }
                };


            foreach (var item in member.BankingAccounts)
            {
                if (_banks.All(x => x.Id != item.BankId))
                {
                    return new CreateMemberCallbackError()
                    {
                        UniqueIdentifier = member.UniqueIdentifier,
                        Meta = new[] { $" value '{item.BankId}',is not valid for bankId" }
                    };
                }
            }
            //valid situation :)
            return null;

        }
    }
}
﻿using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.ViewModel;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using System;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Service;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using static Newtonsoft.Json.JsonConvert;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    class LegacyCodeInquiryExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }
        public LegacyCodeInquiryExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public async Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                ILegacyCodeInquiryService legacyCodeInquiry =
                    scope.ServiceProvider.GetRequiredService<ILegacyCodeInquiryService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                IOptions<InquiryConfiguration> config =
                    scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();

                LegacyCodeInquiryVM param;
                try
                {
                    param = DeserializeObject<LegacyCodeInquiryVM>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;

                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                    return TaskStatus.Canceled;

                var inquiry = await legacyCodeInquiry.GetAsync(param.NinOrSsn);
                if (inquiry == null || inquiry.HasException)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.InquiryCheckNextTime);
                    task.RecoverySequence++;
                    task.Status = TaskStatus.Pending;
                    await taskRepository.AddAsync(task);
                    return TaskStatus.Recovered;

                }
                

                return TaskStatus.Done;
            }

        }
    }
}

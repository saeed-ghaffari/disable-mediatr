﻿using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.ViewModel;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using System;
using System.Threading.Tasks;
using Cerberus.Service;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class IrenexInquiryExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }
        public IrenexInquiryExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public async Task<TasksManager.Core.Enum.TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IIrenexInquiryService irenexInquiry =
                    scope.ServiceProvider.GetRequiredService<IIrenexInquiryService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                IOptions<InquiryConfiguration> config =
                    scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();

                LegacyCodeInquiryVM param;
                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<LegacyCodeInquiryVM>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;

                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                    return TaskStatus.Canceled;

                var inquiry = await irenexInquiry.GetAsync(param.NinOrSsn);

                if (inquiry == null || inquiry.HasException)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.InquiryCheckNextTime);
                    task.Status = TaskStatus.Pending;
                    await taskRepository.AddAsync(task);
                    return TaskStatus.Recovered;

                }

                return TaskStatus.Done;

            }
        }
    }
}

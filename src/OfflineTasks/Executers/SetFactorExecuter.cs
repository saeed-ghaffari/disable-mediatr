﻿using System;
using System.Linq;
using System.Net;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Interface.Services.FGM;
using Cerberus.Domain.ViewModel;
using Cerberus.Domain.ViewModel.FGM;
using Cerberus.Service;
using Cerberus.Service.BankService.Configuration;
using Cerberus.TasksManager.Core.Enum;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Cerberus.OfflineTasks.Executers
{
    public class SetFactorExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public SetFactorExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param) => true;

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var fgmService = scope.ServiceProvider.GetRequiredService<IFgmService>();
                var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                var taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                var paymentService = scope.ServiceProvider.GetRequiredService<IPaymentService>();
                var config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();
                var fgmConfig = scope.ServiceProvider.GetRequiredService<IOptions<FgmConfiguration>>();
                bool setFactorForOtherClients;

                try
                {
                    setFactorForOtherClients = Newtonsoft.Json.JsonConvert.DeserializeObject<bool>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;


                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                    return TaskStatus.Failed;

                var payment = await paymentService.GetByIdAsync(task.RefId.GetValueOrDefault());

                if (payment == null || payment.Status != PaymentStatus.Settle)
                    return TaskStatus.Canceled;

                if (payment.ReferenceServiceId != null && payment.IsConfirmedReference != true)
                    return TaskStatus.Canceled;


                if (payment.FactorId != null)
                    return TaskStatus.Done;

                var saveProfile = await profileService.GetDataForFgmFactorAsync(payment.ProfileId);
                var address = saveProfile.Addresses.FirstOrDefault();
                string fullName;

                if (saveProfile.Type == ProfileOwnerType.IranianPrivatePerson)
                    fullName = saveProfile.PrivatePerson != null ? ($"{saveProfile.PrivatePerson?.FirstName} {saveProfile?.PrivatePerson?.LastName}") : "فاقد نام";
                else
                    fullName = saveProfile.LegalPerson != null ? saveProfile.LegalPerson?.CompanyName : "فاقد نام";

                var factor = new FactorRequestModel
                {
                    UniqueIdentifier = saveProfile.UniqueIdentifier,
                    ProfileOwnerType = saveProfile.Type,
                    Address = saveProfile.Addresses.Any() ? Tools.ConcatAddress(address?.Province?.Name, address?.City?.Name,
                        address?.RemnantAddress, address?.Alley, address?.Plaque) : "فاقد آدرس",
                    PostalCode = address?.PostalCode ?? "0000000000",
                    FullName = fullName,
                    Mobile = saveProfile.Mobile.NormalMobile(),


                };
                Envelop<FactorResponseModel> factorResult;
                if (setFactorForOtherClients)
                {
                    factor.OrderId = payment.ClientReference;
                    factor.ServiceId = payment.ServiceId;
                    factorResult = await fgmService.SetFactorForOtherPayment(factor);
                }
                else
                {
                    factor.OrderId = payment.Id.ToString();
                    factor.ServiceId = payment.ProfilePackageId != null ? fgmConfig.Value.EditServiceId : fgmConfig.Value.RegisterServiceId;
                    factorResult = await fgmService.SetFactorNumber(factor);
                }

                if (factorResult.Error == null)
                {
                    if (payment.FactorId != null) return TaskStatus.Done;

                    if (factorResult.Data.FactorId > 0 && !string.IsNullOrEmpty(factorResult.Data.InvoiceNumber))
                    {

                        //payment.FactorId = factorResult.Data.FactorId;
                        //payment.SerialNumber = factorResult.Data.InvoiceNumber;
                        //await paymentRepository.UpdateAsync(payment);
                          await paymentService.SetFactorIdRepository(payment.Id, factorResult.Data.FactorId, factorResult.Data.InvoiceNumber);
                        return TaskStatus.Done;
                    }
                }

                task.DueTime = DateTime.Now.AddMinutes(config.Value.SetFactorNextExecuteTime);
                task.RecoverySequence++;
                task.Status = TaskStatus.Pending;
                await taskRepository.AddAsync(task);
                return TaskStatus.Recovered;

            }



        }

    }
}

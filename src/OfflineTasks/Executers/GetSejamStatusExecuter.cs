﻿using System;
using System.Collections.Generic;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model.StatusData;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.OfflineTasks.Model;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class GetSejamStatusExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public GetSejamStatusExecuter(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IProfileApiService profileApiService = scope.ServiceProvider.GetRequiredService<IProfileApiService>();
                IProfileService profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();

                List<StatusData> validUniqueIdentifiers  =  new List<StatusData>();

                GetSejamStatusTaskParam taskParam;

                try
                {
                    taskParam = JsonConvert.DeserializeObject<GetSejamStatusTaskParam>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                var callBackResponse = new GetSejamStatusCallbackData()
                {
                    Success = new List<KeyValuePair<string, SejamStatusViewModel>>(),
                    Failed = new List<GetSejamStatusCallbackError>(),
                    ReferenceId = task.Id
                };

                foreach (var uniqueIdentifier in taskParam.UniqueIdentifiers)
                {

                    if (!uniqueIdentifier.IsPrivatePersonUniqueIdentifierValid() && !uniqueIdentifier.IsLegalPersonUniqueIdentifierValid())
                    {
                        callBackResponse.Failed.Add(new GetSejamStatusCallbackError()
                        {
                            UniqueIdentifier = uniqueIdentifier,
                            Meta = new[] { "invalid uniqueIdentifier" }
                        });

                        continue;
                    }

                    validUniqueIdentifiers.Add(new StatusData
                    {
                        NationalCode = uniqueIdentifier,
                        RefrenceNumber = task.Id.ToString()
                    });
                }

                var sejamStatusResult = await profileService.GetSejamStatus(task.Id.ToString(), validUniqueIdentifiers);

                foreach(var item in sejamStatusResult)
                {
                    if(item.Status > 0)
                    {
                        callBackResponse.Success.Add(new KeyValuePair<string, SejamStatusViewModel>(item.UniqueIdentifier,
                        new SejamStatusViewModel { ProfileStatus = ((ProfileStatus)item.Status).ToString() }));
                    }
                    else
                    {
                        callBackResponse.Failed.Add(new GetSejamStatusCallbackError()
                        {
                            UniqueIdentifier = item.NationalCode,
                            Meta = new[] { "profile not found" }
                        });
                    }
                }

                //create callback task
                await taskRepository.AddAsync(new ScheduledTask()
                {
                    Type = CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Value,
                    DueTime = DateTime.Now,
                    GroupId = CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Key,
                    RefId = taskParam.ServiceId,
                    OriginalTaskId = task.Id,
                    Params = JsonConvert.SerializeObject(new CallbackThirdPartyServicesTaskParams()
                    {
                        CallbackUrl = taskParam.CallbackUrl,
                        Payload = callBackResponse,
                        ServiceId = taskParam.ServiceId
                    }),
                });

                return TaskStatus.Done;
            }
        }

    }
}
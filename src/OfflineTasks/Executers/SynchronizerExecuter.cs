﻿using System;
using System.Diagnostics;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Interface.Services.Membership;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Membership;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Komodo.Redis.StackExchange;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class SynchronizerExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public SynchronizerExecuter(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IProfileService profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                IFinancialInfoApiService financialInfoApiService =
                    scope.ServiceProvider.GetRequiredService<IFinancialInfoApiService>();
                IProfileApiService profileApiService = scope.ServiceProvider.GetRequiredService<IProfileApiService>();
                IBankingAccountApiService bankingAccountApiService =
                    scope.ServiceProvider.GetRequiredService<IBankingAccountApiService>();
                ITradingCodeApiService tradingCodeApiService =
                    scope.ServiceProvider.GetRequiredService<ITradingCodeApiService>();
                IAddressApiService addressApiService = scope.ServiceProvider.GetRequiredService<IAddressApiService>();
                ILegalPersonApiService legalPersonApiService =
                    scope.ServiceProvider.GetRequiredService<ILegalPersonApiService>();
                IPrivatePersonApiService privatePersonApiService =
                    scope.ServiceProvider.GetRequiredService<IPrivatePersonApiService>();
                IJobInfoApiService jobInfoApiService = scope.ServiceProvider.GetRequiredService<IJobInfoApiService>();
                ISerializer serializer = scope.ServiceProvider.GetRequiredService<ISerializer>();
                IMembershipService membershipService = scope.ServiceProvider.GetService<IMembershipService>();
                IPermanentOtpApiService permanentOtpService = scope.ServiceProvider.GetService<IPermanentOtpApiService>();

                SynchronizeTaskParams taskParams;

                try
                {
                    taskParams = JsonConvert.DeserializeObject<SynchronizeTaskParams>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                Profile profile = new Profile();

                if (!(taskParams.Target == SynchronizerTarget.AddressMembership
                    || taskParams.Target == SynchronizerTarget.BankingAccountMembership
                    || taskParams.Target == SynchronizerTarget.PrivatePersonMembership
                    || taskParams.Target == SynchronizerTarget.PermanentOtp))
                {
                    var stopWatch = new Stopwatch();

                    stopWatch.Start();
                    profile = await profileService.GetAsync(taskParams.ProfileId);
                    stopWatch.Stop();

                    Console.WriteLine($"Get UniqueIdentifier : {stopWatch.ElapsedMilliseconds}");

                    if (profile == null)
                    {
                        if (!string.IsNullOrWhiteSpace(taskParams.UniqueIdentifier))
                            await profileApiService.RemoveAsync(taskParams.UniqueIdentifier);
                    }

                    if (profile == null || string.IsNullOrWhiteSpace(profile.UniqueIdentifier))
                        return TaskStatus.Canceled;
                }

                switch (taskParams.Target)
                {
                    case SynchronizerTarget.FinancialInfo:

                        if (taskParams.IsDeleted)
                            await financialInfoApiService.RemoveAsync(profile.UniqueIdentifier);
                        else
                            await financialInfoApiService.StoreAsync(profile.UniqueIdentifier, profile.Type);

                        break;

                    case SynchronizerTarget.FinancialInfoBroker:

                        if (taskParams.IsDeleted)
                            await financialInfoApiService.RemoveAsync(profile.UniqueIdentifier);
                        else

                            await financialInfoApiService.StoreAsync(profile.UniqueIdentifier, profile.Type);

                        break;

                    case SynchronizerTarget.BankingAccount:

                        if (taskParams.IsDeleted)
                            await bankingAccountApiService.RemoveAsync(profile.UniqueIdentifier);
                        else
                            await bankingAccountApiService.StoreAsync(profile.UniqueIdentifier, profile.Type);

                        break;
                    case SynchronizerTarget.TradingCode:

                        if (taskParams.IsDeleted)
                            await tradingCodeApiService.RemoveAsync(profile.UniqueIdentifier);
                        else
                            await tradingCodeApiService.StoreAsync(profile.UniqueIdentifier, profile.Type);
                        break;
                    case SynchronizerTarget.Address:
                        if (taskParams.IsDeleted)
                            await addressApiService.RemoveAsync(profile.UniqueIdentifier);
                        else
                            await addressApiService.StoreAsync(profile.UniqueIdentifier, profile.Type);


                        break;


                    case SynchronizerTarget.LegalPerson:

                        if (taskParams.IsDeleted)
                            await legalPersonApiService.RemoveAsync(profile.UniqueIdentifier);
                        else
                        {
                            await legalPersonApiService.StoreAsync(profile.UniqueIdentifier, profile.Type);
                        }

                        break;
                    case SynchronizerTarget.PrivatePerson:

                        if (taskParams.IsDeleted)
                            await privatePersonApiService.RemoveAsync(profile.UniqueIdentifier);
                        else
                        {
                            await privatePersonApiService.StoreAsync(profile.UniqueIdentifier, profile.Type);
                        }


                        break;
                    case SynchronizerTarget.Profile:
                        if (taskParams.IsDeleted)
                            await profileApiService.RemoveAsync(profile.UniqueIdentifier);
                        else
                        {
                            if (taskParams.DataSerialized == null)
                                await profileApiService.StoreAsync(profile.UniqueIdentifier, profile.Type);
                            else
                                await profileApiService.StoreAsync(serializer.Deserialize<ProfileViewModel>(taskParams.DataSerialized), false, profile.Type);
                        }


                        break;
                    case SynchronizerTarget.JobInfo:
                        if (taskParams.IsDeleted)
                            await jobInfoApiService.RemoveAsync(profile.UniqueIdentifier);
                        else
                            await jobInfoApiService.StoreAsync(profile.UniqueIdentifier, profile.Type);
                        break;
                    case SynchronizerTarget.Agent:
                    case SynchronizerTarget.LegalPersonShareholder:
                    case SynchronizerTarget.LegalPersonStakeholder:
                        await profileApiService.StoreAsync(profile.UniqueIdentifier, profile.Type);


                        break;
                    case SynchronizerTarget.ThirdPartyService:

                        break;
                    case SynchronizerTarget.PrivatePersonMembership:
                        {

                            if (taskParams.IsDeleted)
                                await membershipService.RemoveAsync(taskParams.ServiceId, taskParams.UniqueIdentifier,
                                    MembershipDataType.PrivatePerson);
                            else
                                await membershipService.UpdateCacheAsync(taskParams.ServiceId, taskParams.UniqueIdentifier, serializer.Deserialize<PrivatePersonMembership>(taskParams.DataSerialized)
                                    , MembershipDataType.PrivatePerson);
                            break;
                        }
                    case SynchronizerTarget.BankingAccountMembership:
                        {
                            if (taskParams.IsDeleted)
                                await membershipService.RemoveAsync(taskParams.ServiceId, taskParams.UniqueIdentifier,
                                    MembershipDataType.BankingAccount);
                            else
                                await membershipService.UpdateCacheAsync(taskParams.ServiceId, taskParams.UniqueIdentifier, serializer.Deserialize<BankingAccountMembership>(taskParams.DataSerialized)
                                    , MembershipDataType.BankingAccount);
                            break;
                        }
                    case SynchronizerTarget.AddressMembership:
                        {
                            if (taskParams.IsDeleted)
                                await membershipService.RemoveAsync(taskParams.ServiceId, taskParams.UniqueIdentifier,
                                    MembershipDataType.Address);
                            else
                                await membershipService.UpdateCacheAsync(taskParams.ServiceId, taskParams.UniqueIdentifier, serializer.Deserialize<AddressMembership>(taskParams.DataSerialized)
                                    , MembershipDataType.Address);
                            break;
                        }

                    case SynchronizerTarget.PermanentOtp:
                        {
                            if (taskParams.IsDeleted)
                                await permanentOtpService.RemoveAsync(serializer.Deserialize<PermanentOtp>(taskParams.DataSerialized));
                            else
                                await permanentOtpService.UpdateCacheAsync(serializer.Deserialize<PermanentOtp>(taskParams.DataSerialized));
                            break;
                        }
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                return TaskStatus.Done;
            }
        }
    }
}
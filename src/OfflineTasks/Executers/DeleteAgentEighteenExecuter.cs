﻿using System;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.TasksManager.Core.Enum;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Cerberus.OfflineTasks.Executers
{
    public class DeleteAgentEighteenExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public DeleteAgentEighteenExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var privatePersonService = scope.ServiceProvider.GetRequiredService<IPrivatePersonService>();
                var messagingService = scope.ServiceProvider.GetRequiredService<IMessagingService>();
                var config = scope.ServiceProvider.GetRequiredService<IOptions<DeleteAgentEighteenConfiguration>>();
                var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                var profileHistoryRepository = scope.ServiceProvider.GetRequiredService<IProfileHistoryRepository>();
                var agentService = scope.ServiceProvider.GetRequiredService<IAgentService>();

                DeleteAgentEighteenTaskParam param;
                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<DeleteAgentEighteenTaskParam>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                foreach (var privatePerson in await privatePersonService.GetByDaysLeftToEighteenAsync(param.DaysLeftToEighteen, (int)config.Value.IntProfileStatus))
                {
                    switch (param.DaysLeftToEighteen)
                    {
                        case 7:
                            {
                                if (privatePerson.ProfileOwner.Agent != null)
                                {
                                    if (privatePerson.ProfileOwner.Status == ProfileStatus.Sejami || privatePerson.ProfileOwner.Status == ProfileStatus.SemiSejami)
                                        await messagingService.SendSmsAsync(privatePerson.ProfileId,
                                            privatePerson.ProfileOwner.Mobile.ToString(),
                                            privatePerson.ProfileOwner.Carrier,
                                            string.Format(config.Value.MessageSevenDaysSejami, privatePerson.ProfileOwner.UniqueIdentifier, privatePerson.ProfileOwner.TraceCode, Environment.NewLine), DateTime.Now.AddHours(4));

                                    else if (privatePerson.ProfileOwner.Status == ProfileStatus.TraceCode)
                                        await messagingService.SendSmsAsync(privatePerson.ProfileId,
                                            privatePerson.ProfileOwner.Mobile.ToString(),
                                            privatePerson.ProfileOwner.Carrier,
                                            string.Format(config.Value.MessageSevenDaysTraceCode, privatePerson.ProfileOwner.UniqueIdentifier, privatePerson.ProfileOwner.TraceCode, Environment.NewLine), DateTime.Now.AddHours(4));
                                    else
                                        await messagingService.SendSmsAsync(privatePerson.ProfileId,
                                            privatePerson.ProfileOwner.Mobile.ToString(),
                                            privatePerson.ProfileOwner.Carrier,
                                            string.Format(config.Value.MessageSevenDays, privatePerson.ProfileOwner.UniqueIdentifier, Environment.NewLine), DateTime.Now.AddHours(4));
                                }

                                break;
                            }
                        case 1:
                            {
                                if (privatePerson.ProfileOwner.Agent != null)
                                {
                                    if (privatePerson.ProfileOwner.Status == ProfileStatus.Sejami || privatePerson.ProfileOwner.Status == ProfileStatus.SemiSejami)
                                        await messagingService.SendSmsAsync(privatePerson.ProfileId,
                                            privatePerson.ProfileOwner.Mobile.ToString(),
                                            privatePerson.ProfileOwner.Carrier,
                                            string.Format(config.Value.MessageOneDaysSejami, privatePerson.ProfileOwner.UniqueIdentifier, privatePerson.ProfileOwner.TraceCode, Environment.NewLine), DateTime.Now.AddHours(4));

                                    else if (privatePerson.ProfileOwner.Status == ProfileStatus.TraceCode)
                                        await messagingService.SendSmsAsync(privatePerson.ProfileId,
                                            privatePerson.ProfileOwner.Mobile.ToString(),
                                            privatePerson.ProfileOwner.Carrier,
                                            string.Format(config.Value.MessageOneDaysTraceCode, privatePerson.ProfileOwner.UniqueIdentifier, privatePerson.ProfileOwner.TraceCode, Environment.NewLine), DateTime.Now.AddHours(4));

                                    else
                                        await messagingService.SendSmsAsync(privatePerson.ProfileId,
                                            privatePerson.ProfileOwner.Mobile.ToString(),
                                            privatePerson.ProfileOwner.Carrier,
                                            string.Format(config.Value.MessageOneDays, privatePerson.ProfileOwner.UniqueIdentifier, Environment.NewLine), DateTime.Now.AddHours(4));
                                }

                                break;
                            }
                        case 0:
                            {
                                if (privatePerson.ProfileOwner.Agent != null)
                                {
                                    await agentService.MoveToArchiveAsync(privatePerson.ProfileOwner.Agent);
                                }

                                if (privatePerson.ProfileOwner.Status == ProfileStatus.Sejami)
                                {

                                    privatePerson.ProfileOwner.Status = ProfileStatus.Suspend;
                                    privatePerson.ProfileOwner.StatusReasonType = StatusReasonType.DeleteAgentEighteen;
                                    await profileService.UpdateStatusAndReasonAsync(privatePerson.ProfileOwner);

                                    await profileHistoryRepository.CreateAsync(new ProfileHistory()
                                    {
                                        ChangedTo = ProfileStatus.Suspend,
                                        ReferenceId = ProfileHistoryReferenceType.RemoveAgentEighteen.ToString(),
                                        ProfileId = privatePerson.ProfileId,
                                    });
                                }
                                else if (privatePerson.ProfileOwner.IsEditPrivatePersonSemiSejami())
                                {
                                    privatePerson.ProfileOwner.Status = ProfileStatus.Suspend;
                                    privatePerson.ProfileOwner.StatusReasonType =
                                        StatusReasonType.DeleteAgentEighteenPrivatePersonSemiSejami;
                                    await profileService.UpdateStatusAndReasonAsync(privatePerson.ProfileOwner);

                                    await profileHistoryRepository.CreateAsync(new ProfileHistory()
                                    {
                                        ChangedTo = ProfileStatus.Suspend,
                                        ReferenceId = ProfileHistoryReferenceType.RemoveAgentEighteen.ToString(),
                                        ProfileId = privatePerson.ProfileId,
                                    });

                                }

                                else if (privatePerson.ProfileOwner.IsEditBankingAccountSemiSejami())
                                {
                                    privatePerson.ProfileOwner.Status = ProfileStatus.Suspend;
                                    privatePerson.ProfileOwner.StatusReasonType =
                                        StatusReasonType.DeleteAgentEighteenBankingAccountSemiSejami;
                                    await profileService.UpdateStatusAndReasonAsync(privatePerson.ProfileOwner);

                                    await profileHistoryRepository.CreateAsync(new ProfileHistory()
                                    {
                                        ChangedTo = ProfileStatus.Suspend,
                                        ReferenceId = ProfileHistoryReferenceType.RemoveAgentEighteen.ToString(),
                                        ProfileId = privatePerson.ProfileId,
                                    });
                                }

                                break;
                            }
                    }
                }
                return TaskStatus.Done;
            }
        }
    }
}
﻿using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Interface.Services.FGM;
using Cerberus.TasksManager.Core.Enum;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Service.BankService.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;

namespace Cerberus.OfflineTasks.Executers
{
    class InquirySuspiciousPaymentExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;
        private static readonly NLog.ILogger Logger = NLog.LogManager.GetCurrentClassLogger();

        public InquirySuspiciousPaymentExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public  async Task<TasksManager.Core.Enum.TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var fgmService = scope.ServiceProvider.GetRequiredService<IFgmService>();
                var paymentService = scope.ServiceProvider.GetRequiredService<IPaymentService>();
                var offlineTaskService = scope.ServiceProvider.GetRequiredService<IOfflineTaskService>();
                var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                var fgmConfig = scope.ServiceProvider.GetRequiredService<IOptions<FgmConfiguration>>();

                try
                {
                    var suspiciousPayments = await paymentService.GetSuspiciousPayments();
                    foreach (var payment in suspiciousPayments)
                    {
                        var statusResult = await fgmService.CheckPaymentStatus(payment.Id.ToString(), Guid.Parse(payment.ProfilePackageId != null ? fgmConfig.Value.EditServiceId : fgmConfig.Value.RegisterServiceId)); 
                        if (statusResult.Error == null)
                        {
                            await paymentService.UpdateSettledPaymentsAsync(payment.Id, statusResult?.Data.SaleReferenceId, statusResult?.Data.SaleReferenceNumber);

                            if (payment.ProfilePackageId == null)
                            {
                                var profile = await profileService.GetAsync(payment.ProfileId);
                                profile.Status = ProfileStatus.SuccessPayment;
                                await profileService.UpdateStatusAsync(profile);
                            }
                            else
                            {
                                if (payment.FactorId == null)
                                    await offlineTaskService.ScheduleSetFactorAsync(payment.Id, DateTime.Now.AddSeconds(2));
                            }
                        }
                        else if (statusResult.Error.ErrorCode == (int)ErrorCode.FailedPayment)
                        {
                            await paymentService.UpdateSettleFailedPaymentsAsync(payment.Id);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"InquirySuspiciousPayment exception : {ex.Message}");
                    return TasksManager.Core.Enum.TaskStatus.Failed;

                }
                return TasksManager.Core.Enum.TaskStatus.Done;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.TasksManager.Core.Enum;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Cerberus.OfflineTasks.Executers
{
    public class SyncStockCodeWithIxExecuter : ITaskExecuter
    {
        private readonly IServiceScopeFactory _scopeFactory;

        public SyncStockCodeWithIxExecuter(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param) => true;

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                LegacyCodeInquiry ixResponse;
                try
                {
                    ixResponse = JsonConvert.DeserializeObject<LegacyCodeInquiry>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }


                if (string.IsNullOrEmpty(ixResponse?.NinOrSsn))
                    return TaskStatus.BadParams;

                if (ixResponse.HasException)
                    return TaskStatus.Canceled;

                if (ixResponse.LegacyCodeResponseList == null || !ixResponse.LegacyCodeResponseList.Any())
                    return TaskStatus.Done;

                var profileRepository = scope.ServiceProvider.GetRequiredService<IProfileService>();

                var profile = await profileRepository.GetAsync(task.RefId ?? 0);

                if (profile == null)
                    return TaskStatus.Canceled;

                var issuanceCodeRepository = scope.ServiceProvider.GetRequiredService<IIssuanceStockCodeRepository>();

                var legacyCodeFromDb = await issuanceCodeRepository.GetByUniqueIdentifierAsync(ixResponse.NinOrSsn, true);

                //if not exist legacy code in db for this person then insert new code from ix
                if (legacyCodeFromDb == null)
                {
                    var issuance = new IssuanceStockCode()
                    {
                        Description = "Insert from offline task",
                        ProfileId = profile.Id,
                        Mobile = profile.Mobile,
                        UniqueIdentifier = profile.UniqueIdentifier,
                        Status = Domain.Enum.IssuanceStockCodeStatus.Done,
                        Successful = true,
                        
                        IssuanceStockCodeResponses = new List<IssuanceStockCodeResponse>()
                    };

                    issuance.IssuanceStockCodeResponses =
                        ixResponse.LegacyCodeResponseList.Select(x => new IssuanceStockCodeResponse
                        {
                            IsActive = x.IsActive ?? false,
                            LegacyCode = x.LegacyCode,
                            Message = "Insert from offline task",
                            InvestorCode = profile.Id.ToString()
                        }).ToList();

                    await issuanceCodeRepository.CreateAsync(issuance);

                    return TaskStatus.Done;
                }

                var issuanceCodeResponseRepository =
                    scope.ServiceProvider.GetRequiredService<IIssuanceStockCodeResponseRepository>();

                //check ix response with database 
                foreach (var ixLegacyCodeResponse in ixResponse.LegacyCodeResponseList)
                {
                    var dbCode =
                        legacyCodeFromDb.IssuanceStockCodeResponses.LastOrDefault(x =>
                            x.LegacyCode == ixLegacyCodeResponse.LegacyCode);

                    //if find a match then update isActive with ix response
                    if (dbCode != null)
                    {
                        if (dbCode.IsActive != ixLegacyCodeResponse.IsActive)
                        {
                            dbCode.IsActive = ixLegacyCodeResponse.IsActive ?? false;

                            await issuanceCodeResponseRepository.UpdateAsync(dbCode);
                        }
                    }
                    //if not found means it's a new record and not exist in database then we should insert it
                    else
                    {
                        await issuanceCodeResponseRepository.CreateAsync(new IssuanceStockCodeResponse
                        {
                            IsActive = ixLegacyCodeResponse.IsActive ?? false,
                            LegacyCode = ixLegacyCodeResponse.LegacyCode,
                            InvestorCode = legacyCodeFromDb.ProfileId.ToString(),
                            Message = "Insert from offline task",
                            IssuanceStockCodeId = legacyCodeFromDb.Id
                        });
                    }
                }


                return TaskStatus.Done;
            }
        }
    }
}
﻿using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.ViewModel;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using System;
using System.Threading.Tasks;
using Cerberus.Service;
using Cerberus.Utility.Enum;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class IndividualInquiryExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }
        public IndividualInquiryExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<TasksManager.Core.Enum.TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IIndividualInquiryService individualInquiry = scope.ServiceProvider.GetRequiredService<IIndividualInquiryService>();
                IIndividualInquiryRepository individualInquiryRepository = scope.ServiceProvider.GetRequiredService<IIndividualInquiryRepository>();
                IPrivatePersonRepository privatePersonRepository = scope.ServiceProvider.GetRequiredService<IPrivatePersonRepository>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                IMessagingService messagingService = scope.ServiceProvider.GetRequiredService<IMessagingService>();
                IProfileRepository profileRepository = scope.ServiceProvider.GetRequiredService<IProfileRepository>();
                IOptions<InquiryConfiguration> config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();


                IndividualInquiryVM param;

                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<IndividualInquiryVM>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;
                var mobile = param.Mobile;
                var isMsisdnValid = ValidationHelper.IsMsisdnValid(ref mobile, out var carrier);
                if (!isMsisdnValid)
                    return TaskStatus.BadParams;

                var inquiry = await individualInquiry.GetAsync(param.PrivateCode);
                if (inquiry == null || inquiry.HasException)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.InquiryCheckNextTime);
                    task.RecoverySequence++;
                    task.Status = TaskStatus.Pending;
                    await taskRepository.AddAsync(task);
                    return TaskStatus.Done;

                }

                var person = await privatePersonRepository.GetAsync(param.ReferenceId);
                if (person == null || person.IsDeleted)
                    return TaskStatus.Canceled;


                var name = await profileRepository.GetFullName(task.RefId.GetValueOrDefault());
                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                {
                    await SendSms(task.RefId.GetValueOrDefault(), param.Mobile, carrier.GetValueOrDefault(), name, messagingService, config.Value);

                    return TaskStatus.Canceled;
                }


                if (inquiry.Successful == false)
                {
                    person.IsConfirmed = false;
                    person.Locked = false;
                    await privatePersonRepository.UpdateAsync(person);
                    await SendSms(task.RefId.GetValueOrDefault(), param.Mobile, carrier.GetValueOrDefault(), name, messagingService, config.Value);

                    return TaskStatus.Failed;
                }

                string inquiryPersianSide = PersianChars.GetClearChars(
                    (inquiry.PersianFirstName ?? "").Trim() + " " + (inquiry.PersianLastName ?? "").Trim() + " " +
                    (inquiry.PersianFatherName ?? "").Trim());
                string inquiryLatinSide = PersianChars.GetClearChars(
                    (inquiry.LatinFirstName ?? "").Trim() + " " + (inquiry.LatinLastName ?? "").Trim() + " " +
                    (inquiry.LatinFatherName ?? "").Trim());
                string personSide = PersianChars.GetClearChars(
                    (person.ProfileOwner.PrivatePerson.FirstName ?? "").Trim() + " " +
                    (person.ProfileOwner.PrivatePerson.LastName ?? "").Trim());
                if ((personSide == inquiryLatinSide || personSide == inquiryPersianSide) &&
                    person.BirthDate.ToPersianDate() == inquiry.BirthDate)
                {
                    person.IsConfirmed = true;
                    person.Locked = true;
                    await privatePersonRepository.UpdateAsync(person);
                    return TaskStatus.Done;
                }
                else
                {
                    inquiry.Successful = false;
                    await individualInquiryRepository.UpdateAsync(inquiry);
                    person.IsConfirmed = false;
                    person.Locked = false;
                    await privatePersonRepository.UpdateAsync(person);
                    await SendSms(task.RefId.GetValueOrDefault(), param.Mobile, carrier.GetValueOrDefault(), name, messagingService, config.Value);
                    return TaskStatus.Done;
                }

            }
        }

        private async Task SendSms(long refId, string mobile, Carrier carrier, string name, IMessagingService messagingService, InquiryConfiguration config)
        {
            await messagingService.SendSmsAsync(refId, mobile, carrier, string.Format(config.InquiryNotConfirmedMessage,
                name, " استعلام اطلاعات هویتی ", Environment.NewLine));
        }
    }
}

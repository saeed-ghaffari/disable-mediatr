﻿using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;
using System;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel;
using Cerberus.Service;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Cerberus.OfflineTasks.Executers
{
    class UpdatePrivatePersonExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public UpdatePrivatePersonExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                INocrInquiryService nocrInquiry = scope.ServiceProvider.GetRequiredService<INocrInquiryService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                IPrivatePersonService privatePersonService = scope.ServiceProvider.GetRequiredService<IPrivatePersonService>();
                IOptions<InquiryConfiguration> config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();
                IProfileService profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                IProfilePackageService profilePackageService = scope.ServiceProvider.GetRequiredService<IProfilePackageService>();
                IMessagingService messagingService = scope.ServiceProvider.GetRequiredService<IMessagingService>();

                UpdatePrivatePersonTaskParam param;
                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<UpdatePrivatePersonTaskParam>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }
                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;

                var mobile = param.Mobile;
                var isMsisdnValid = ValidationHelper.IsMsisdnValid(ref mobile, out var carrier);

                if (!isMsisdnValid)
                    return TaskStatus.BadParams;


                PrivatePerson privatePerson = await privatePersonService.GetAsync(param.ReferenceId);
                var profile = await profileService.GetWithNoCacheAsync(privatePerson.ProfileId);

                var profilePackage =
                    await profilePackageService.GetUnusedPackagesAsync(profile.Id, PackageType.EditAllData, PackageUsedFlag.EditPrivatePersonInfo);
                if (profilePackage == null)
                {
                    return TaskStatus.Canceled;
                }

                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                {
                    await messagingService.SendSmsAsync(task.RefId, mobile, carrier.GetValueOrDefault(),
                    string.Format(config.Value.NotConfirmUpdatePrivatePerson, $"{privatePerson.FirstName} {privatePerson.LastName}", Environment.NewLine));
                    return TaskStatus.Done;
                }


                var inquiry = await nocrInquiry.GetAsync(param.Ssn, param.BirthDate);

                if (inquiry == null || inquiry.HasException || inquiry.Successful == false)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.InquiryCheckNextTime);
                    task.RecoverySequence++;
                    task.Status = TaskStatus.Pending;
                    await taskRepository.AddAsync(task);
                    return TaskStatus.Recovered;

                }

                //Replace Date When Profile Is Sejami 
                if (profile.Status.IsSejami())
                {

                    privatePerson.IsConfirmed = true;
                    privatePerson.Locked = true;

                   
                        privatePerson.FirstName = inquiry.FirstName.Unify();
                        privatePerson.LastName = inquiry.LastName.Unify();
                        privatePerson.FatherName = inquiry.FatherName.Unify();

                        privatePerson.BirthDate = DateTime.TryParse(inquiry.BirthDate, out var dt) ? dt : inquiry.PersianBirthDate.ToGeorgianDate();

                        privatePerson.Gender = inquiry.Gender == "1" ? Gender.Male : Gender.Female;
                        privatePerson.Serial = inquiry.SerialNumber;

                        var seriNumber = Tools.GetDigitsFromString(inquiry.SeriesNumber ?? string.Empty);

                        privatePerson.SeriSh = seriNumber;


                        if (seriNumber.Length > 0)
                        {
                            privatePerson.SeriShChar = inquiry.SeriesNumber == null
                                ? string.Empty
                                : inquiry.SeriesNumber.Replace(seriNumber, ""); //get characters from seriesNumber
                        }
                        else
                        {
                            privatePerson.SeriShChar = inquiry.SeriesNumber;
                        }
                        privatePerson.ShNumber = inquiry.IdentificationNumber;
                    

                    await privatePersonService.UpdateAsync(privatePerson);
                    await messagingService.SendSmsAsync(task.RefId, mobile, carrier.GetValueOrDefault(),
                        string.Format(config.Value.ConfirmUpdatePrivatePerson, $"{privatePerson.FirstName} {privatePerson.LastName}", Environment.NewLine));

                    profilePackage.UsedFlag |= PackageUsedFlag.EditPrivatePersonInfo;
                    await profilePackageService.UpdateAsync(profilePackage);


                }



                return TaskStatus.Done;

            }
        }
    }
}

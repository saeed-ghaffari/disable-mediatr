﻿using System;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.Service;
using Cerberus.TasksManager.Core.Enum;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Cerberus.OfflineTasks.Executers
{
    public class PostInquiryExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public PostInquiryExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param) => true;

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var postInquiryService = scope.ServiceProvider.GetRequiredService<IPostInquiryService>();
                var taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                var config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();

                PostInquiryTaskParam param;

                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<PostInquiryTaskParam>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;

                var mobile = param.Mobile;
                var isMsisdnValid = ValidationHelper.IsMsisdnValid(ref mobile, out var carrier);

                if (!isMsisdnValid)
                    return TaskStatus.BadParams;

                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                {
                    //todo:update entity if required like private person
                    return TaskStatus.Done;
                }

                var inquiry = await postInquiryService.GetAsync(param.Ssn, param.BirthDate);

                if (inquiry == null || inquiry.HasException)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.InquiryCheckNextTime);
                    task.RecoverySequence++;
                    task.Status = TaskStatus.Pending;
                    await taskRepository.AddAsync(task);
                    return TaskStatus.Recovered;

                }
                if (inquiry.Successful == false)
                {
                    //todo:update entity if required like private person
                    return TaskStatus.Done;
                }

                //todo: compare information that received from post

                return TaskStatus.Done;
            }
        }
    }
}
﻿using System;
using System.Linq;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.TasksManager.Core.Enum;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Microsoft.Extensions.DependencyInjection;

namespace Cerberus.OfflineTasks.Executers
{
    public class UpdateIssuanceStockCodeExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public UpdateIssuanceStockCodeExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param) => true;

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var issuanceStockCodeRepository = scope.ServiceProvider.GetRequiredService<IIssuanceStockCodeRepository>();

                UpdateIssuanceCodeTaskParams param;

                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<UpdateIssuanceCodeTaskParams>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }


                var issuanceStockCodes = await issuanceStockCodeRepository.GetListWithListIdAsync(param.Id);
                if (issuanceStockCodes.Any())
                {
                    issuanceStockCodes.ForEach(x =>
                    {
                        x.Status = param.Status;
                        x.Successful = param.Successful;
                        x.TryCount = param.Status == IssuanceStockCodeStatus.None ? x.TryCount++ : 0;
                    });
                    await issuanceStockCodeRepository.UpdateRangeAsync(issuanceStockCodes);

                }

                return TaskStatus.Done;
            }
        }
    }
}
﻿using System;
using Cerberus.Domain.Common;
using Cerberus.Domain.Interface.Services;
using Cerberus.Service;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class UpdateProfileCacheExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;


        public UpdateProfileCacheExecuter(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

        }

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var masterCacheProvider = scope.ServiceProvider.GetRequiredService<IMasterCacheProvider>();
                var config = scope.ServiceProvider.GetRequiredService<IOptions<MasterCacheProviderConfiguration>>();
                var profileService= scope.ServiceProvider.GetRequiredService<IProfileService>();
                long profileId;
                

                try
                {
                    profileId = JsonConvert.DeserializeObject<long>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                var profile = await profileService.GetWithNoCacheAsync(profileId);
                if (profile==null)
                    return TaskStatus.BadParams;

                await masterCacheProvider.StoreAsync(KeyGenerator.Profile.GetMasterCacheKey(profile.UniqueIdentifier), profile, config.Value.ProfileExpirationTime);

                return TaskStatus.Done;

            }
        }
    }
}
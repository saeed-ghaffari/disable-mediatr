﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.Service;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class ChangeToSemiSejamiExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public ChangeToSemiSejamiExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            ChangeToSemiSejamiTaskParams taskParams;

            try
            {
                taskParams = JsonConvert.DeserializeObject<ChangeToSemiSejamiTaskParams>(task.Params);
            }
            catch
            {
                return TaskStatus.BadParams;
            }

            using (var scope = _serviceProvider.CreateScope())
            {
                var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                var taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                var messageService = scope.ServiceProvider.GetRequiredService<IMessageService>();
                var profile = await profileService.GetWithNoCacheAsync(taskParams.ProfileId);
                var configurations = scope.ServiceProvider.GetRequiredService<IOptions<SemiSejamiConfiguration>>();
                var messagingConfigurations = scope.ServiceProvider.GetRequiredService<IOptions<MessagingConfiguration>>();
                var profileHistoryService = scope.ServiceProvider.GetRequiredService<IProfileHistoryService>();
                if (profile == null)
                    return TaskStatus.BadParams;

                switch (taskParams.Tier)
                {
                    case 1:
                        if (profile.Status != ProfileStatus.Sejami)
                            return TaskStatus.BadParams;


                        //create reminder tasks
                        //second tier

                        var taskList = await taskRepository.GetTaskListAsync(taskParams.ProfileId,
                            CerberusTasksIdentifierHelper.ChangeProfileToSemiSejami.Key, 1);
                        var allSemiSejamTask = taskList.ToList();
                        if (allSemiSejamTask.Any())
                        {
                            foreach (var scheduledTask in allSemiSejamTask)
                            {
                                scheduledTask.Status = TaskStatus.Canceled;
                                taskRepository.Update(scheduledTask);
                            }
                        }


                        var allMessage = await taskRepository.GetTaskListAsync(taskParams.ProfileId,
                            CerberusTasksIdentifierHelper.SendSms.Key, 1);
                        var scheduledTasks = allMessage.ToList();
                        if (scheduledTasks.Any())
                        {
                            foreach (var scheduledTask in scheduledTasks)
                            {
                                scheduledTask.Status = TaskStatus.Canceled;
                                taskRepository.Update(scheduledTask);
                            }
                        }


                        await CreateReminderTasks(taskRepository, new ChangeToSemiSejamiTaskParams()
                        {
                            Tier = 2,
                            ProfileId = taskParams.ProfileId
                        }, DateTime.Now.AddDays(configurations.Value.SecondTier));

                        //////third tier
                        await CreateReminderTasks(taskRepository, new ChangeToSemiSejamiTaskParams()
                        {
                            Tier = 3,
                            ProfileId = taskParams.ProfileId
                        }, DateTime.Now.AddDays(configurations.Value.ThirdTier));

                        var lastProfileHistory = await profileHistoryService.GetTheLastHistoryAsync(profile.Id);

                        //update status 
                        profile.Status = ProfileStatus.SemiSejami;
                        profile.StatusReasonType = StatusReasonType.Admin;
                        await profileService.UpdateStatusAndReasonAsync(profile);
                        await profileHistoryService.CreateAsync(new ProfileHistory()
                        {
                            ProfileId = profile.Id,
                            RejectType = taskParams.RejectType,
                            Description = taskParams.Description,
                            ReferenceId = lastProfileHistory.ReferenceId

                        }, ProfileStatus.SemiSejami);
                        //send notification

                        
                        if (lastProfileHistory.ReferenceId.ToLower().StartsWith("kara"))
                        {
                            await messageService.CreateAsync(new Message()
                            {
                                ProfileId = taskParams.ProfileId,
                                Body = string.Format(configurations.Value.FirstTierKaraMessageFormat, profile.UniqueIdentifier, Environment.NewLine),


                            }, true, null, DateTime.Now.AddHours(messagingConfigurations.Value.SendMessageDueTime));

                        }
                        else
                        {
                            await messageService.CreateAsync(new Message()
                            {
                                ProfileId = taskParams.ProfileId,
                                Body = string.Format(configurations.Value.FirstTierPishkhanMessageFormat, profile.UniqueIdentifier, lastProfileHistory.ReferenceId, Environment.NewLine),


                            }, true, null, DateTime.Now.AddHours(messagingConfigurations.Value.SendMessageDueTime));
                        }


                    

                        return TaskStatus.Done;

                    case 2:
                        if (profile.Status == ProfileStatus.Sejami)
                            return TaskStatus.Canceled;

                        if (profile.Status != ProfileStatus.SemiSejami)
                            return TaskStatus.BadParams;

                        var lastHistory = await profileHistoryService.GetTheLastHistoryAsync(profile.Id);

                        if (!lastHistory.ReferenceId.ToLower().StartsWith("kara"))
                        {
                            await messageService.CreateAsync(new Message()
                            {
                                ProfileId = taskParams.ProfileId,
                                Body = string.Format(configurations.Value.SecondTierMessageFormat, profile.UniqueIdentifier, Environment.NewLine)
                            });
                        } //send notification

                    


                        return TaskStatus.Done;

                    case 3:
                        if (profile.Status == ProfileStatus.Sejami)
                            return TaskStatus.Canceled;

                        if (profile.Status != ProfileStatus.SemiSejami)
                            return TaskStatus.BadParams;

                        //send notification
                        await messageService.CreateAsync(new Message()
                        {
                            ProfileId = taskParams.ProfileId,
                            Body = string.Format(configurations.Value.ThirdTierMessageFormat, profile.UniqueIdentifier, Environment.NewLine)
                        });

                        //update status 
                        profile.Status = ProfileStatus.Suspend;
                        profile.StatusReasonType = StatusReasonType.Admin;
                        await profileService.UpdateStatusAndReasonAsync(profile);
                        await profileHistoryService.CreateAsync(new ProfileHistory()
                        {
                            ProfileId = profile.Id,
                        }, ProfileStatus.Suspend);

                        return TaskStatus.Done;


                    case 4:

                        if (profile.Status == ProfileStatus.Sejami)
                            return TaskStatus.Canceled;

                        var allTask = await taskRepository.GetTaskListAsync(taskParams.ProfileId,
                            CerberusTasksIdentifierHelper.ChangeProfileToSemiSejami.Key, 1);
                        var tasks = allTask.ToList();
                        if (tasks.Any())
                        {
                            foreach (var scheduledTask in tasks)
                            {
                                scheduledTask.Status = TaskStatus.Canceled;
                                taskRepository.Update(scheduledTask);
                            }
                        }


                        var messageList = await taskRepository.GetTaskListAsync(taskParams.ProfileId,
                            CerberusTasksIdentifierHelper.SendSms.Key, 1);
                        var messages = messageList.ToList();
                        if (messages.Any())
                        {
                            foreach (var scheduledTask in messages)
                            {
                                scheduledTask.Status = TaskStatus.Canceled;
                                taskRepository.Update(scheduledTask);
                            }
                        }

                        await profileHistoryService.CreateAsync(new ProfileHistory()
                        {
                            ReferenceId = "Admin",
                            ProfileId = profile.Id,
                        }, ProfileStatus.Sejami);

                        profile.Status = ProfileStatus.Sejami;
                        profile.StatusReasonType = null;
                        await profileService.UpdateStatusAndReasonAsync(profile);
                        return TaskStatus.Done;


                    default:
                        return TaskStatus.BadParams;
                }

            }
        }

        public Task CreateReminderTasks(ITaskRepository repository, ChangeToSemiSejamiTaskParams taskParams, DateTime dueTime)
        {
            return repository.AddAsync(new ScheduledTask()
            {
                Status = TaskStatus.Pending,
                Params = JsonConvert.SerializeObject(taskParams),
                Type = CerberusTasksIdentifierHelper.ChangeProfileToSemiSejami.Value,
                GroupId = CerberusTasksIdentifierHelper.ChangeProfileToSemiSejami.Key,
                DueTime = dueTime,
                RefId = taskParams.ProfileId
            });
        }
    }
}
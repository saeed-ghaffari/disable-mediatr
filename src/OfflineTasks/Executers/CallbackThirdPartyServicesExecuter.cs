﻿using System;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Model.Log;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class CallbackThirdPartyServicesExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;


        public CallbackThirdPartyServicesExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {

                ICallbackThirdPartyServiceLogRepository repository =
                    scope.ServiceProvider.GetRequiredService<ICallbackThirdPartyServiceLogRepository>();

                CallbackThirdPartyServicesTaskParams taskParams;

                try
                {
                    taskParams = JsonConvert.DeserializeObject<CallbackThirdPartyServicesTaskParams>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                var body = JsonConvert.SerializeObject(taskParams.Payload);
                try
                {


                    var response = await Utility.HttpHepler.PostAsync(taskParams.CallbackUrl, body, "application/json");

                    await repository.AddAsync(new CallbackThirdPartyServicesLog()
                    {
                        ServiceId = taskParams.ServiceId,
                        Response = response.Body,
                        HttpStatusCode = (int)response.StatusCode,
                        Payload = body,
                        Url = taskParams.CallbackUrl
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    
                    await repository.AddAsync(new CallbackThirdPartyServicesLog()
                    {
                        ServiceId = taskParams.ServiceId,
                        Response = e.Message,
                        HttpStatusCode = 500,
                        Payload = body,
                        Url = taskParams.CallbackUrl
                    });

                    throw;
                }


                return TaskStatus.Done;
            }

        }
    }
}
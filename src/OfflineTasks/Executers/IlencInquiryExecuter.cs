﻿using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.ViewModel;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using System;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services;
using Cerberus.Service;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class IlencInquiryExecuter : ITaskExecuter
    {

        private readonly IServiceProvider _serviceProvider;


        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public IlencInquiryExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IIlencInquiryService ilencInquiry = scope.ServiceProvider.GetRequiredService<IIlencInquiryService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                var laLegalPersonService =scope.ServiceProvider.GetRequiredService<ILegalPersonService>();
                IOptions<InquiryConfiguration> config =scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();


                IlencInquiryVM param;
                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<IlencInquiryVM>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;
                var mobile = param.Mobile;
                var isMsisdnValid = ValidationHelper.IsMsisdnValid(ref mobile, out var carrier);
                if (!isMsisdnValid)
                    return TaskStatus.BadParams;


                var legalPerson = await laLegalPersonService.GetAsync(param.ReferenceId);


                if (legalPerson == null || legalPerson.IsDeleted || legalPerson.IsConfirmed == true)
                    return TaskStatus.Canceled;


                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                {
                    legalPerson.IsConfirmed = false;
                    legalPerson.Locked = false;
                    await laLegalPersonService.UpdateAsync(legalPerson);
                    return TaskStatus.Done;
                }

                var inquiry = await ilencInquiry.GetAsync(param.NationalCode);

                if (inquiry == null || inquiry.HasException)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.InquiryCheckNextTime);
                    task.RecoverySequence++;
                    task.Status = TaskStatus.Pending;
                    await taskRepository.AddAsync(task);
                    return TaskStatus.Recovered;
                }


                if (!inquiry.Successful)
                {
                    legalPerson.IsConfirmed = false;
                    legalPerson.Locked = false;
                    await laLegalPersonService.UpdateAsync(legalPerson);
                    return TaskStatus.Done;
                }

                string inquiryName = PersianChars.GetClearChars((inquiry.Name ?? "").Trim());
                string legalPersonName = PersianChars.GetClearChars((legalPerson.CompanyName ?? "").Trim());

                bool isValid = IsValidLegalPerson(legalPerson, inquiry, inquiryName, legalPersonName);

                if (!isValid)
                {
                    inquiry.Successful = false;
                    await ilencInquiry.UpdateAsync(inquiry);
                    legalPerson.IsConfirmed = false;
                    legalPerson.Locked = false;
                    await laLegalPersonService.UpdateAsync(legalPerson);
                }
                else
                {

                    legalPerson.IsConfirmed = true;
                    legalPerson.Locked = true;
                    await laLegalPersonService.UpdateAsync(legalPerson);
                }

                return TaskStatus.Done;


            }


        }

        private static bool IsValidLegalPerson(Domain.Model.LegalPerson legalPerson, Domain.Model.Inquiries.IlencInquiry inquiry, string name1, string name2)
        {
            return (inquiry.State == "فعال" || inquiry.State == "منحل شده") && name1 == name2 &&
                   PersianChars.GetClearChars((inquiry.RegisterNumber ?? "").Trim().Replace("/", "")) ==
                   PersianChars.GetClearChars((legalPerson.RegisterNumber ?? "").Trim().Replace("/", "")) &&
                        inquiry.RegisterDate.Trim() == legalPerson.RegisterDate.ToPersianDate();
        }
    }
}

﻿using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;
using System;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.Service;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Cerberus.OfflineTasks.Executers
{
    class ShahkarInquiryExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public ShahkarInquiryExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                var config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();

                var shahkarService = scope.ServiceProvider.GetRequiredService<IShahkarService>();
                var msisdnService = scope.ServiceProvider.GetRequiredService<IMsisdnService>();
                var messagingService = scope.ServiceProvider.GetRequiredService<IMessagingService>();
                ShahkarParams param;
                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<ShahkarParams>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;

                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                {
                    return TaskStatus.Disabled;
                }

                var msisdn = await msisdnService.GetByProfileIdAsync(task.RefId.GetValueOrDefault());
                if (msisdn == null)
                {

                    msisdn = new Msisdn
                    {
                        Mobile = param.Mobile,
                        ChangeMobileCount = 0,
                        ProfileId = task.RefId.GetValueOrDefault(),
                    };
                    await msisdnService.CreateAsync(msisdn);

                }
                else if (param.NewMobile != null)
                {
                    msisdn.IsConfirmed = null;
                    await msisdnService.UpdateAsync(msisdn);
                }

                var mobile = param.NewMobile ?? param.Mobile;

                try
                {
                    var existShahkar = await shahkarService.IsMobileForNationalCode(mobile.ToString(), param.UniqueIdentifier);
                    msisdn.ChangeMobileCount += 1;
                    msisdn.Mobile = mobile;
                    if (!existShahkar)
                    {
                        msisdn.Locked = false;
                        msisdn.IsConfirmed = false;
                        await msisdnService.UpdateAsync(msisdn);

                        if (param.Status == ProfileStatus.SuccessPayment || param.Status >= ProfileStatus.TraceCode)
                        {
                            var privatePersonService = scope.ServiceProvider.GetRequiredService<IPrivatePersonService>();
                            var privatePerson = await privatePersonService.GetByProfileIdAsync(task.RefId.GetValueOrDefault());
                            var fullName = $"{privatePerson?.FirstName} {privatePerson?.LastName}";
                            fullName = string.IsNullOrWhiteSpace(fullName) ? "کاربر" : fullName;
                            await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(),
                                mobile.ToString(), null,
                                string.Format(config.Value.ShahkarInquiryNotConfirmedMessage, fullName,
                                    Environment.NewLine));
                        }
                    }
                    else
                    {
                        msisdn.IsConfirmed = true;
                        msisdn.Locked = true;
                        if (param.NewMobile != null)
                        {
                            var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();

                            var profile = await profileService.GetWithNoCacheAsync(task.RefId.GetValueOrDefault());
                            profile.Mobile = param.NewMobile.GetValueOrDefault();
                            await profileService.UpdateAsync(profile);


                            await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(), param.NewMobile.ToString(), null,
                                string.Format(config.Value.RegisterNewMobileMessage, $"{param.UniqueIdentifier}",
                                    Environment.NewLine));

                            //send sms to previous  mobile
                            await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(),
                                param.Mobile.ToString(), null,
                                string.Format(config.Value.RemoveOldMobileMessage,
                                    $"{param.UniqueIdentifier}", Environment.NewLine));
                        }
                        await msisdnService.UpdateAsync(msisdn);
                    }



                }
                catch (Exception)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.InquiryCheckNextTime);
                    task.RecoverySequence++;
                    task.Status = TaskStatus.Pending;
                    await taskRepository.AddAsync(task);
                    return TaskStatus.Recovered;
                }

                return TaskStatus.Done;

            }
        }
    }
}

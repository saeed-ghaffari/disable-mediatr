﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Cerberus.Domain.Interface.Repositories.Membership;
using Cerberus.Domain.Model.Membership;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class SyncEtfMembershipProcessingExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;


        public SyncEtfMembershipProcessingExecuter(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

        }

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var etfMembershipRepository =
                    scope.ServiceProvider.GetRequiredService<IEtfMembershipRepository>();
                var mapper = scope.ServiceProvider.GetRequiredService<IMapper>();

                SyncEtfMembershipTaskParams taskParams;
                try
                {
                    taskParams = JsonConvert.DeserializeObject<SyncEtfMembershipTaskParams>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }
              
                var memberships = new List<EtfMembership>();

                mapper.Map(taskParams.Members, memberships);
                foreach (var model in memberships)
                {
                    var mobile = model.Mobile;
                    ValidationHelper.IsMsisdnValid(ref mobile, out var carrier);
                    model.Mobile = mobile;
                    model.Carrier = carrier.GetValueOrDefault();
                    model.ServiceId = taskParams.ServiceId;
                }
                await etfMembershipRepository.BulkCreateAsync(memberships);
    
                return TaskStatus.Done;

            }
        }
    }
}
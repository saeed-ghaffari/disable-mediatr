﻿using System;
using System.Threading.Tasks;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Microsoft.Extensions.DependencyInjection;

namespace Cerberus.OfflineTasks.Executers
{
    public class ChangeProfileStatusExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;


        public bool IsReusable => false;
        public bool CanHandleTask(string param)
        {
            return true;
        }

        public ChangeProfileStatusExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public async Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var profileHistoryService = scope.ServiceProvider.GetRequiredService<IProfileHistoryService>();
                var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                var privatePersonService = scope.ServiceProvider.GetRequiredService<IPrivatePersonService>();
                var bankingAccountService = scope.ServiceProvider.GetRequiredService<IBankingAccountService>();

                ChangeProfileStatusTaskParam param;
                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<ChangeProfileStatusTaskParam>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;

                var profile = await profileService.GetWithNoCacheAsync(param.ProfileId);

                //if user had Sejami status it can update to Dead status
                if (profile.Status == ProfileStatus.Sejami && param.ChangeToStatus == ProfileStatus.Dead)
                {
                    profile.Status = param.ChangeToStatus;
                    await profileService.UpdateStatusAsync(profile);
                    await profileHistoryService.CreateAsync(new ProfileHistory()
                    {
                        ChangedTo = param.ChangeToStatus,
                        ProfileId = param.ProfileId,
                        ReferenceId = param.ReferenceId,
                        Description = param.Description
                    });

                    return TaskStatus.Done;
                }

                //if user had TraceCode or InvalidInformation status it can update to InvalidInformation status
                if (
                    (profile.Status == ProfileStatus.TraceCode || profile.Status == ProfileStatus.InvalidInformation)
                    && param.ChangeToStatus == ProfileStatus.InvalidInformation)
                {
                    profile.Status = param.ChangeToStatus;
                    await profileService.UpdateStatusAsync(profile);
                    await profileHistoryService.CreateAsync(new ProfileHistory()
                    {
                        ProfileId = param.ProfileId,
                        ReferenceId = param.ReferenceId,
                        Description = param.Description
                    }, param.ChangeToStatus);

                    //in case of InvalidInformation we need to update private person and banking account and then user can edit information
                    if (param.ChangeToStatus == ProfileStatus.InvalidInformation)
                    {
                        var person = await privatePersonService.GetByProfileIdAsync(param.ProfileId);
                        if (person != null)
                        {
                            person.Locked = false;
                            person.IsConfirmed = null;
                            await privatePersonService.UpdateAsync(person);
                        }

                        var accountList =
                            await bankingAccountService.GetListAsync(param.ProfileId);
                        foreach (var bankingAccount in accountList)
                        {
                            bankingAccount.Locked = false;
                            bankingAccount.IsConfirmed = null;
                            await bankingAccountService.UpdateAsync(bankingAccount);
                        }
                    }
                    return TaskStatus.Done;

                }

                return TaskStatus.Failed;
            }

        }
    }
}

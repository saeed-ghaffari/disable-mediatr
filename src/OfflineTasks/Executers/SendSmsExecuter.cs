﻿using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Model.Log;
using Cerberus.Domain.ViewModel;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.Service;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class SendSmsExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public SendSmsExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IOptions<MessagingConfiguration> messagingConfiguration = scope.ServiceProvider.GetRequiredService<IOptions<MessagingConfiguration>>();
                ISmsLogRepository smsLogRepository = scope.ServiceProvider.GetRequiredService<ISmsLogRepository>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();

                SendSmsTaskParam param;
                try
                {
                    param = JsonConvert.DeserializeObject<SendSmsTaskParam>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }


                if (task.RecoverySequence > 5)
                {
                    return TaskStatus.Failed;
                }

                if (messagingConfiguration.Value.UseRahyab)
                {
                    var insertData = "insert into outgoing_message(from_mobile_number,dest_mobile_number,message_body,due_date,creation_date," +
                                     "Priority) values (@from_mobile, @dest_mobile_number, @message_body, @due_date, @creation_date, @Priority)";
                    using (var connection = new MySqlConnection(messagingConfiguration.Value.RahyabSmsConectionString))
                    {
                        try
                        {
                            var command = new MySqlCommand(insertData, connection);
                            command.Parameters.AddWithValue("@from_mobile", messagingConfiguration.Value.RahyabSmsFromMobile);
                            command.Parameters.AddWithValue("@dest_mobile_number", param.Msisdn);
                            command.Parameters.AddWithValue("@message_body", param.SmsText);
                            command.Parameters.AddWithValue("@due_date", DateTime.Now);
                            command.Parameters.AddWithValue("@creation_date", DateTime.Now);
                            command.Parameters.AddWithValue("@Priority", 1);
                            connection.Open();
                            command.CommandTimeout = 5;
                            await command.ExecuteNonQueryAsync();
                            connection.Close();
                        }
                        catch (Exception)
                        {
                            task.DueTime = DateTime.Now.AddMinutes(1);
                            task.RecoverySequence++;
                            task.Status = TaskStatus.Pending;
                            await taskRepository.AddAsync(task);
                            return TaskStatus.Recovered;
                        }
                    }
                }
                else
                {
                    //consider that use the original server to send sms or not
                    if (messagingConfiguration.Value.UseOriginalServer)
                        await HttpHepler.PostAsync(messagingConfiguration.Value.OriginalSmsServerUrl, JsonConvert.SerializeObject(new OriginSmsRequest()
                        {
                            Messages = new[] { param.SmsText },
                            Mobiles = new[] { param.Msisdn },
                            MsgIds = new[] { param.ProfileId }
                        }), "application/json", messagingConfiguration.Value.ServiceProviderTimeout);
                    else
                        await HttpHepler.PostAsync(messagingConfiguration.Value.SmsApiUrl, JsonConvert.SerializeObject(new SmsRequest
                        {
                            Body = param.SmsText,
                            Receptor = param.Msisdn,
                        }), "application/json", messagingConfiguration.Value.ServiceProviderTimeout);
                }
                var smsLog = new SmsLog() { ProfileId = param.ProfileId, SmsText = param.SmsText, Msisdn = param.Msisdn };
                await smsLogRepository.AddAsync(smsLog);

                return TaskStatus.Done;
            }
        }
    }
}

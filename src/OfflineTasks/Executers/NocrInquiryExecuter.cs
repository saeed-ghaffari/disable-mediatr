﻿using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.ViewModel;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services;
using Cerberus.Service;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class NocrInquiryExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }
        public NocrInquiryExecuter(IServiceProvider serviceProvider)
        {

            _serviceProvider = serviceProvider;

        }

        private static int _errorCount;
        public async Task<TasksManager.Core.Enum.TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                INocrInquiryService nocrInquiry = scope.ServiceProvider.GetRequiredService<INocrInquiryService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                IPrivatePersonService privatePersonService = scope.ServiceProvider.GetRequiredService<IPrivatePersonService>();
                INocrInquiryService nocrInquiryService = scope.ServiceProvider.GetRequiredService<INocrInquiryService>();
                IOptions<InquiryConfiguration> config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();
                var messageConfig = scope.ServiceProvider.GetRequiredService<IOptions<IssuanceStockCodeConfiguration>>();
                var messageService = scope.ServiceProvider.GetRequiredService<IMessagingService>();

                NocrInquiryVM param;
                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<NocrInquiryVM>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }
                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;

                var mobile = param.Mobile;
                var isMsisdnValid = ValidationHelper.IsMsisdnValid(ref mobile, out var carrier);

                if (!isMsisdnValid)
                    return TaskStatus.BadParams;


                var privatePerson = await privatePersonService.GetAsync(param.ReferenceId);
                if (privatePerson == null || privatePerson.IsDeleted || privatePerson.IsConfirmed == true)
                    return TaskStatus.Canceled;

                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                {
                    privatePerson.IsConfirmed = false;
                    privatePerson.Locked = false;
                    await privatePersonService.UpdateAsync(privatePerson);

                    return TaskStatus.Done;
                }

                var inquiry = await nocrInquiry.GetAsync(param.Ssn, param.BirthDate);

                if (inquiry==null|| (string.IsNullOrEmpty(inquiry.FirstName) && (string.IsNullOrEmpty(inquiry.LastName))) || inquiry.HasException || !string.IsNullOrEmpty(inquiry.Message))
                    _errorCount++;

                if (_errorCount == 100)
                {
                    var mobiles = messageConfig.Value.SendErrorMobiles;

                    foreach (var m in mobiles)
                    {
                        await messageService.SendSmsAsync(
                            null,
                            m,
                            null,
                            string.Format(
                                $"خطا در سرویس ثبت احوال در ساعت: {DateTime.Now.Hour}:{DateTime.Now.Minute}:{DateTime.Now.Second}  \r\n احتمالا سرویس قطع شده، لطفا بررسی کنید. "));
                    }

                    _errorCount = 0;
                }

                if (inquiry==null || inquiry.HasException)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.InquiryCheckNextTime);
                    task.RecoverySequence++;
                    task.Status = TaskStatus.Pending;
                    await taskRepository.AddAsync(task);
                    return TaskStatus.Recovered;

                }

                if (_errorCount>0) _errorCount = 0;

                if (inquiry.Successful == false)
                {
                    privatePerson.IsConfirmed = false;
                    privatePerson.Locked = false;
                    await privatePersonService.UpdateAsync(privatePerson);

                    return TaskStatus.Done;
                }


                string inquirySide = PersianChars.GetClearChars((inquiry.FirstName ?? "").Trim() + " " + (inquiry.LastName ?? "").Trim() + " " + (inquiry.FatherName ?? "").Trim());
                string personSide = PersianChars.GetClearChars((privatePerson.FirstName ?? "").Trim() + " " + (privatePerson.LastName ?? "").Trim() + " " + (privatePerson.FatherName ?? "").Trim());
                var valid = inquirySide == personSide
                    && (inquiry.PersianBirthDate ?? "").Trim() == (privatePerson.BirthDate.ToPersianDate()).Trim()
                    && (privatePerson.Gender == Gender.Male ? inquiry.Gender == "1" : inquiry.Gender == "0")
                    && inquiry.IsAlive == "True";
                if (long.Parse(inquiry.IdentificationNumber ?? "0") > 0)
                {
                    if (inquiry.IdentificationNumber != null)
                        valid = valid && privatePerson.ShNumber.Trim() ==
                                inquiry.IdentificationNumber.Trim();
                }
                else
                {
                    valid = valid && (privatePerson.ShNumber.Trim() == inquiry.Ssn.Trim()|| privatePerson.ShNumber.Trim()=="0");
                }
                if (valid)
                {
                    privatePerson.IsConfirmed = true;
                    privatePerson.Locked = true;
                    
                    privatePerson.Serial =  inquiry.SerialNumber.Trim();

                    var seriNumber = Tools.GetDigitsFromString(inquiry.SeriesNumber ?? string.Empty);

                    privatePerson.SeriSh = seriNumber.Trim();


                    if (seriNumber.Length > 0)
                    {
                        privatePerson.SeriShChar = inquiry.SeriesNumber == null
                            ? string.Empty
                            : inquiry.SeriesNumber.Replace(seriNumber, "").Trim(); //get characters from seriesNumber
                    }
                    else
                    {
                        privatePerson.SeriShChar = inquiry.SeriesNumber?.Trim();
                    }
                    
                    await privatePersonService.UpdateAsync(privatePerson);
                    return TaskStatus.Done;
                }

                inquiry.Successful = false;
                await nocrInquiryService.UpdateAsync(inquiry);
                privatePerson.IsConfirmed = false;
                privatePerson.Locked = false;
                await privatePersonService.UpdateAsync(privatePerson);

                return TaskStatus.Done;

            }
        }

        private async Task CreateTaskAsync(KeyValuePair<long, string> taskGroup, long profileId, object taskParams, ITaskRepository _taskRepository)
        {

            var param = JsonConvert.SerializeObject(taskParams);

            await _taskRepository.AddAsync(new ScheduledTask()
            {
                RefId = profileId,
                DueTime = DateTime.Now,
                GroupId = taskGroup.Key,
                Type = taskGroup.Value,
                Params = param,
                Status = TaskStatus.Pending,
                RecoverySequence = 0
            });
        }

    }
}

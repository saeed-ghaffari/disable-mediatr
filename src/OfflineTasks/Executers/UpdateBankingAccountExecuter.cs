﻿using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using System;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.Service;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class UpdateBankingAccountExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }
        public UpdateBankingAccountExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<TasksManager.Core.Enum.TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IIbanInquiryService ibanInquiry = scope.ServiceProvider.GetRequiredService<IIbanInquiryService>();
                IProfileService profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                IBankingAccountService bankingAccountService = scope.ServiceProvider.GetRequiredService<IBankingAccountService>();
                IIbanInquiryRepository ibanInquiryRepository = scope.ServiceProvider.GetRequiredService<IIbanInquiryRepository>();
                IPrivatePersonService privatePersonService = scope.ServiceProvider.GetRequiredService<IPrivatePersonService>();
                ILegalPersonService legalPersonService = scope.ServiceProvider.GetRequiredService<ILegalPersonService>();
                IProfilePackageService profilePackageService = scope.ServiceProvider.GetRequiredService<IProfilePackageService>();
                IOptions<InquiryConfiguration> config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();
                IMessagingService messagingService = scope.ServiceProvider.GetRequiredService<IMessagingService>();
                IProfileHistoryService profileHistoryService =
                    scope.ServiceProvider.GetService<IProfileHistoryService>();



                UpdateBankingAccountTaskParam param;
                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<UpdateBankingAccountTaskParam>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;

                var mobile = param.Mobile;

                var isMsisdnValid = ValidationHelper.IsMsisdnValid(ref mobile, out var carrier);

                if (!isMsisdnValid || carrier == null)
                    return TaskStatus.BadParams;


                var account = await bankingAccountService.GetAsync(param.ReferenceId);
                if (account == null || account.IsDeleted)
                    return TaskStatus.Canceled;
                var profile = await profileService.GetWithNoCacheAsync(task.RefId.GetValueOrDefault());
                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                {
                    account.IsConfirmed = false;
                    account.Locked = false;
                    await bankingAccountService.UpdateAsync(account);

                    
                    profile.StatusReasonType = StatusReasonType.EditBankingAccount;
                    await profileService.UpdateStatusAndReasonAsync(profile);
                    return TaskStatus.Done;
                }

                var privatePerson = await privatePersonService.GetByProfileIdAsync(task.RefId.GetValueOrDefault());
               
                if (privatePerson != null && privatePerson.IsConfirmed == null)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.IbanInquiryTimeToLater);
                    task.Status = TaskStatus.Pending;
                    task.RecoverySequence++;
                    await taskRepository.AddAsync(task);

                    return TaskStatus.Recovered;
                }

                if (privatePerson != null && privatePerson.IsConfirmed == false)
                    return TaskStatus.Done;


                var inquiry = await ibanInquiry.GetAsync(param.Iban);

                if (inquiry == null || inquiry.HasException)
                {

                    task.DueTime = DateTime.Now.AddMinutes(config.Value.InquiryCheckNextTime);
                    task.Status = TaskStatus.Pending;
                    task.RecoverySequence++;
                    await taskRepository.AddAsync(task);

                    return TaskStatus.Recovered;
                }


                var name = await profileService.GetFullName(task.RefId.GetValueOrDefault());
                if (inquiry.Successful == false)
                {

                    account.IsConfirmed = false;
                    account.Locked = false;
                    await bankingAccountService.UpdateAsync(account);

                    var allAccounts = await bankingAccountService.GetListAsync(task.RefId.GetValueOrDefault());
                    var accounts = allAccounts.ToList();
                    if (accounts.All(x => x.IsConfirmed != null) && accounts.Any(x => x.IsConfirmed == false))
                    {
                        if (!profile.IsEditPrivatePersonAndDeleteAgentEighteen() && !profile.IsEditBankingAccountAndDeleteAgentEighteen())
                        {
                            profile.Status = ProfileStatus.SemiSejami;
                            profile.StatusReasonType = StatusReasonType.EditBankingAccount;
                            await profileService.UpdateStatusAndReasonAsync(profile);
                        }

                        await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(), param.Mobile,
                      carrier.GetValueOrDefault(),
                      string.Format(config.Value.InquiryNotConfirmedMessage, name, config.Value.IbanInquiryTitle,
                          Environment.NewLine));
                    }



                    return TaskStatus.Done;
                }



                var userProfile = await profileService.GetAsync(task.RefId.GetValueOrDefault());
                string inquirySide = PersianChars.GetClearChars(
                    (inquiry.FirstName ?? "").Replace("0", "").Trim() + " " +
                    (inquiry.LastName ?? "").Replace("0", "").Trim());
                if ((userProfile.Type == ProfileOwnerType.IranianLegalPerson ||
                     userProfile.Type == ProfileOwnerType.ForeignLegalPerson)
                    && inquiry.FirstName == inquiry.LastName)
                    inquirySide = PersianChars.GetClearChars((inquiry.FirstName ?? "").Trim());

                string personSide = PersianChars.GetClearChars(name);
                if (personSide == inquirySide)
                {
                    account.IsConfirmed = true;
                    account.Locked = true;
                    //Set Used For Profile Package 

                    var profilePackage =
                        await profilePackageService.GetUnusedPackagesAsync(userProfile.Id,
                            PackageType.EditAllData, PackageUsedFlag.EditBankingAccount);
                    if (profilePackage != null || param.TaskCreator == TaskCreatorType.UpdatePrivatePerson)
                    {
                        await bankingAccountService.UpdateAsync(account);
                        var allAccounts = await bankingAccountService.GetListAsync(task.RefId.GetValueOrDefault());
                        var accounts = allAccounts.ToList();
                        if (accounts.All(x => x.IsConfirmed == true))
                        {
                            if (param.TaskCreator == TaskCreatorType.UpdateBankingAccount)
                            {
                                if (profilePackage != null)
                                {
                                    profilePackage.UsedFlag |= PackageUsedFlag.EditBankingAccount;
                                    await profilePackageService.UpdateAsync(profilePackage);
                                }
                            }

                            await messagingService.SendSmsAsync(task.RefId, mobile, carrier.GetValueOrDefault(),
                                string.Format(config.Value.ConfirmEditBanckingAccount, name, Environment.NewLine));
                        }
                        if (accounts.All(x => x.IsConfirmed != null) && accounts.Any(x => x.IsConfirmed == false))
                        {
                            await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(), param.Mobile,
                                carrier.GetValueOrDefault(),
                                string.Format(config.Value.InquiryNotConfirmedMessage, name, config.Value.IbanInquiryTitle,
                                    Environment.NewLine));
                        }
                    }


                    var bb = (await bankingAccountService.GetListAsync(account.ProfileId)).ToList();
                    if (bb.FirstOrDefault(x => x.IsConfirmed != true) == null)
                    {

                        if (!profile.IsEditPrivatePersonAndDeleteAgentEighteen() && !profile.IsEditBankingAccountAndDeleteAgentEighteen())
                        {
                            if (bb.All(x => x.IsDefault != true))
                            {
                                var c = bb.FirstOrDefault(x => x.IsDefault != true);
                                if (c!=null)
                                {
                                    c.IsDefault = true;
                                    await bankingAccountService.UpdateAsync(c);
                                }
                            }
                            await profileHistoryService.CreateAsync(new ProfileHistory()
                            {
                                ChangedTo = ProfileStatus.Sejami,
                                ReferenceId = ProfileHistoryReferenceType.UpdateBankingAccount.ToString(),
                                ProfileId = account.ProfileId,
                            });
                            profile.Status = ProfileStatus.Sejami;
                            profile.StatusReasonType = null;
                            await profileService.UpdateStatusAndReasonAsync(profile);
                        }
                    }

                    return TaskStatus.Done;
                }

                inquiry.Successful = false;
                await ibanInquiryRepository.UpdateAsync(inquiry);
                account.IsConfirmed = false;
                account.Locked = false;
                await bankingAccountService.UpdateAsync(account);
                var allAccount = await bankingAccountService.GetListAsync(task.RefId.GetValueOrDefault());
                var accountList = allAccount.ToList();
                if (accountList.All(x => x.IsConfirmed != null) && accountList.Any(x => x.IsConfirmed == false))
                {
                    if (!profile.IsEditPrivatePersonAndDeleteAgentEighteen() && !profile.IsEditBankingAccountAndDeleteAgentEighteen())
                    {
                        profile.Status = ProfileStatus.SemiSejami;
                        profile.StatusReasonType = StatusReasonType.EditBankingAccount;
                        await profileService.UpdateStatusAndReasonAsync(profile);
                    }

                    await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(), param.Mobile,
                        carrier.GetValueOrDefault(),
                        string.Format(config.Value.InquiryNotConfirmedMessage, name, config.Value.IbanInquiryTitle,
                            Environment.NewLine));
                }

                return TaskStatus.Done;

            }
        }

    }
}


﻿namespace Cerberus.OfflineTasks
{
    public class AdminReportConfiguration
    {
        public int PreDayForFromDate { get; set; }
        public int PreDayForToDate { get; set; }
        public string HeaderMessage { get; set; }
        public string CountMessage { get; set; }
        public string LinkMessage { get; set; }
        public string ProvinceMessage { get; set; }
        public string EntryNodeMessage { get; set; }
        public string ClubMessage { get; set; }
        public string SejamiPishkhanMessage { get; set; }
        public string SejamiBrokerMessage { get; set; }

        public string SejamiBankMessage { get; set; }
        public string SejamiGheirHozuriMessage { get; set; }
        public string SummaryCountMessage { get; set; }
        public string SejamiPishkhanMessage2 { get; set; }


    }
}
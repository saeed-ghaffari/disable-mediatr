﻿using System;
using System.Net.Http;
using AutoMapper;
using Cerberus.ApiService;
using Cerberus.ApiService.Implementations;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.ReadRepositories.Membership;
using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Repositories.Inquiries;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Interface.Repositories.Membership;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Interface.Services.FGM;
using Cerberus.Domain.Interface.Services.Membership;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Membership;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Domain.ViewModel.Membership;
using Cerberus.Domain.ViewModel.StatusData;
using Cerberus.OfflineTasks.Model;
using Cerberus.Repository.MsSql;
using Cerberus.Repository.MsSql.Log;
using Cerberus.Repository.MsSql.Membership;
using Cerberus.Repository.ReadRepositories;
using Cerberus.Repository.ReadRepositories.Membership;
using Cerberus.Service;
using Cerberus.Service.BankService.Configuration;
using Cerberus.Service.FGM;
using Cerberus.Service.Implementations;
using Cerberus.Service.Implementations.Inquiries;
using Cerberus.Service.Implementations.Membership;
using Cerberus.TasksManager.Core;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Repository;
using Cerberus.Utility;
using Komodo.Caching.Abstractions;
using Komodo.Caching.Redis;
using Komodo.Redis.StackExchange;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using StackExchange.Redis;

namespace Cerberus.OfflineTasks
{
    public static class ServicesExtensions
    {
        public static void AddCerberusServices(this IServiceCollection services, IConfiguration configuration)
        {
            //configurations
            var connectionSection = configuration.GetSection("ConnectionStrings");
            var connectionConfiguration = new ConnectionStringsConfiguration();
            connectionSection.Bind(connectionConfiguration);
            services.Configure<ConnectionStringsConfiguration>(connectionSection);
            services.Configure<FileStorageConfiguration>(configuration.GetSection("FileStorageConfiguration"));
            services.Configure<InquiryConfiguration>(configuration.GetSection("InquiryConfiguration"));
            services.Configure<MessagingConfiguration>(configuration.GetSection("MessagingConfiguration"));
            services.Configure<ObjectCacheServicesConfiguration>(configuration.GetSection("ObjectCacheServicesConfiguration"));
            services.Configure<SemiSejamiConfiguration>(configuration.GetSection("SemiSejamiConfiguration"));
            services.Configure<OfflineTasksConfiguration>(configuration.GetSection("OfflineTasksConfiguration"));
            services.Configure<Service.ObjectCacheConfiguration>(configuration.GetSection("ObjectCacheConfiguration"));
            services.Configure<AdminReportConfiguration>(configuration.GetSection("AdminReportConfiguration"));
            services.Configure<DeleteAgentEighteenConfiguration>(configuration.GetSection("DeleteAgentEighteenConfiguration"));
            services.Configure<IxServiceConfiguration>(configuration.GetSection("IxServiceConfiguration"));
            services.Configure<IssuanceStockCodeConfiguration>(configuration.GetSection("IssuanceStockCodeConfiguration"));
            services.Configure<FgmConfiguration>(configuration.GetSection("FgmConfiguration"));
            services.Configure<MasterCacheProviderConfiguration>(configuration.GetSection("MasterCacheProviderConfiguration"));
            services.Configure<ShahkarServiceConfiguration>(configuration.GetSection("ShahkarServiceConfiguration"));
            Connection.SetConnectionString(connectionConfiguration.MasterDbConnection);//Sejam_Db
            Connection.SetConnectionString(connectionConfiguration.ReadConnection);//Sejam_Read_Db
            Connection.SetConnectionString(connectionConfiguration.TaskManagerConnection);
            //db context
            services.AddDbContextPool<CerberusDbContext>(options =>
            {
                options.UseSqlServer(connectionConfiguration.MasterDbConnection);
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }, 4096);

            services.AddDbContextPool<ReadCerberusDbContext>(options =>
            {
                options.UseSqlServer(connectionConfiguration.ReadConnection);
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }, 4096);

            services.AddDbContextPool<CerberusLogDbContext>(options =>
            {
                options.UseSqlServer(connectionConfiguration.LogDbConnection);
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

            }, 2048);


            services.AddSingleton<ISqlConnectionProvider, SqlConnectionProvider>();

            //Redis
            var redisConfiguration = new ConfigurationOptions();
            foreach (var endpoint in connectionConfiguration.RedisWriteDbHost.Split(';'))
            {
                redisConfiguration.EndPoints.Add(endpoint);
            }
            redisConfiguration.Password = connectionConfiguration.RedisWriteDbPass;
            redisConfiguration.ConnectTimeout = 400;
            services.AddSingleton<ConfigurationOptions>(redisConfiguration);

            services.AddSingleton<IConnectionMultiplexerProvider>(new ConnectionMultiplexerProvider(redisConfiguration, Environment.ProcessorCount * 2));

            services.AddSingleton<IDataBaseProvider, DatabaseProvider>();

            services.AddTransient<IDatabase>(provider => provider.GetService<IDataBaseProvider>().GetWriteDatabase());

            services.AddSingleton<ISerializer, BinarySerializer>();


            services.AddTransient<ICacheProvider>(provider =>
            {
                if (!provider.GetService<IOptions<OfflineTasksConfiguration>>().Value.EnableCache)
                    return new NullCacheProvider();

                return new RedisCacheProvider(
                    provider.GetService<IDatabase>(),
                    provider.GetService<IDatabase>(),
                    provider.GetService<ISerializer>());
            });


            //repositories
            services.AddScoped<IMessageRepository, MessageRepository>();
            services.AddTransient<ITaskRepository, SqlTaskRepository>();
            services.AddTransient<IRecurringTaskRepository, SqlRecurringTaskRepository>();
            services.AddScoped<IIbanInquiryRepository, IbanInquiryRepository>();
            services.AddScoped<IBankingAccountRepository, BankingAccountRepository>();
            services.AddScoped<IIlencInquiryRepository, IlencInquiryRepository>();
            services.AddScoped<INocrInquiryRepository, NocrInquiryRepository>();
            services.AddScoped<IPrivatePersonRepository, PrivatePersonRepository>();
            services.AddScoped<ILegacyCodeInquiryRepository, LegacyCodeInquiryRepository>();
            services.AddScoped<IIndividualInquiryRepository, IndividualInquiryRepository>();
            services.AddScoped<IInstitutionInquiryRepository, InstitutionInquiryRepository>();
            services.AddScoped<IProfileRepository, ProfileRepository>();
            services.AddScoped<ILegacyCodeResponseRepository, LegacyCodeResponseRepository>();
            services.AddScoped<IMembershipRepository, MembershipRepository>();
            services.AddScoped<ICallbackThirdPartyServiceLogRepository, CallbackThirdPartyServiceLogRepository>();
            services.AddScoped<IProfileHistoryRepository, ProfileHistoryRepository>();
            services.AddScoped<IThirdPartyServiceRepository, ThirdPartyServiceRepository>();
            services.AddScoped<IAgentRepository, AgentRepository>();
            services.AddScoped<IAgentDocumentRepository, AgentDocumentRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IBrokerRepository, BrokerRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IFinancialInfoRepository, FinancialInfoRepository>();
            services.AddScoped<IJobInfoRepository, JobInfoRepository>();
            services.AddScoped<IJobRepository, JobRepository>();
            services.AddScoped<ITradingCodeRepository, TradingCodeRepository>();
            services.AddScoped<IProvinceRepository, ProvinceRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IAddressSectionRepository, AddressSectionRepository>();
            services.AddScoped<IBankRepository, BankRepository>();
            services.AddScoped<IPaymentRepository, PaymentRepository>();
            services.AddScoped<ILegalPersonShareholderRepository, LegalPersonShareholderRepository>();
            services.AddScoped<ILegalPersonStakeholderRepository, LegalPersonStakeholderRepository>();
            services.AddScoped<ILegalPersonRepository, LegalPersonRepository>();
            services.AddSingleton<ISmsLogRepository, SmsLogRepository>();
            services.AddScoped<IProfilePackageRepository, ProfilePackageRepository>();
            services.AddScoped<IProfileHistoryRepository, ProfileHistoryRepository>();
            services.AddScoped<ISmsReportRepository, SmsReportRepository>();
            services.AddScoped<IAgentArchiveRepository, AgentArchiveRepository>();
            services.AddScoped<IAgentDocumentArchiveRepository, AgentDocumentArchiveRepository>();
            services.AddScoped<IPermanentOtpRepository, PermanentOtpRepository>();
            services.AddScoped<IIssuanceStockCodeRepository, IssuanceStockCodeRepository>();
            services.AddScoped<IIssuanceStockCodeResponseRepository, IssuanceStockCodeResponseRepository>();
            services.AddScoped<ICalendarRepository, CalendarRepository>();
            services.AddScoped<IPostInquiryRepository, PostInquiryRepository>();
            services.AddScoped<IDeadInquiryRepository, DeadInquiryRepository>();
            services.AddScoped<IEtfMembershipRepository, EtfMembershipRepository>();
            services.AddScoped<IShahkarInquiryRepository, ShahkarInquiryRepository>();
            services.AddScoped<IMsisdnRepository, MsisdnRepository>();


            #region ReadRepository


            services.AddScoped<IIbanInquiryReadRepository, IbanInquiryReadRepository>();
            services.AddScoped<IBankingAccountReadRepository, BankingAccountReadRepository>();
            services.AddScoped<IIlencInquiryReadRepository, IlencInquiryReadRepository>();
            services.AddScoped<INocrInquiryReadRepository, NocrInquiryReadRepository>();
            services.AddScoped<IPrivatePersonReadRepository, PrivatePersonReadRepository>();
            services.AddScoped<IProfileReadRepository, ProfileReadRepository>();
            services.AddScoped<IMembershipReadRepository, MembershipReadRepository>();
            services.AddScoped<IProfileHistoryReadRepository, ProfileHistoryReadRepository>();
            services.AddScoped<IThirdPartyServiceReadRepository, ThirdPartyServiceReadRepository>();
            services.AddScoped<IAgentReadRepository, AgentReadRepository>();
            services.AddScoped<IAgentDocumentReadRepository, AgentDocumentReadRepository>();
            services.AddScoped<IAddressReadRepository, AddressReadRepository>();
            services.AddScoped<IBrokerReadRepository, BrokerReadRepository>();
            services.AddScoped<ICountryReadRepository, CountryReadRepository>();
            services.AddScoped<IFinancialInfoReadRepository, FinancialInfoReadRepository>();
            services.AddScoped<IJobInfoReadRepository, JobInfoReadRepository>();
            services.AddScoped<IJobReadRepository, JobReadRepository>();
            services.AddScoped<ITradingCodeReadRepository, TradingCodeReadRepository>();
            services.AddScoped<IProvinceReadRepository, ProvinceReadRepository>();
            services.AddScoped<ICityReadRepository, CityReadRepository>();
            services.AddScoped<IAddressSectionReadRepository, AddressSectionReadRepository>();
            services.AddScoped<IBankReadRepository, BankReadRepository>();
            services.AddScoped<IPaymentReadRepository, PaymentReadRepository>();
            services.AddScoped<ILegalPersonShareholderReadRepository, LegalPersonShareholderReadRepository>();
            services.AddScoped<ILegalPersonStakeholderReadRepository, LegalPersonStakeholderReadRepository>();
            services.AddScoped<ILegalPersonReadRepository, LegalPersonReadRepository>();
            services.AddScoped<IProfilePackageReadRepository, ProfilePackageReadRepository>();
            services.AddScoped<IProfileHistoryReadRepository, ProfileHistoryReadRepository>();
            services.AddScoped<ISmsReportReadRepository, SmsReportReadRepository>();
            services.AddScoped<IPermanentOtpReadRepository, PermanentOtpReadRepository>();
            services.AddScoped<IIssuanceStockCodeReadRepository, IssuanceStockCodeReadRepository>();
            services.AddScoped<IIssuanceStockCodeResponseReadRepository, IssuanceStockCodeResponseReadRepository>();
            services.AddScoped<ICalendarReadRepository, CalendarReadRepository>();
            services.AddScoped<IPostInquiryReadRepository, PostInquiryReadRepository>();
            services.AddScoped<IDeadInquiryReadRepository, DeadInquiryReadRepository>();
            services.AddScoped<IMsisdnReadRepository, MsisdnReadRepository>();
            services.AddScoped<IShahkarInquiryRepository, ShahkarInquiryRepository>();

            #endregion

            //services
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IMessageService, MessageService>();
            services.AddScoped<IAddressApiService, AddressApiService>();
            services.AddScoped<IProfileApiService, ProfileApiService>();
            services.AddScoped<IBankingAccountApiService, BankingAccountApiService>();
            services.AddScoped<IFinancialInfoApiService, FinancialInfoApiService>();
            services.AddScoped<IJobInfoApiService, JobInfoApiService>();
            services.AddScoped<ILegalPersonApiService, LegalPersonApiService>();
            services.AddScoped<IPrivatePersonApiService, PrivatePersonApiService>();
            services.AddScoped<ITradingCodeApiService, TradingCodesApiService>();
            services.AddScoped<IMembershipService, MembershipService>();
            services.AddScoped<IAgentService, AgentService>();
            services.AddScoped<IAgentDocumentService, AgentDocumentService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<IBankingAccountService, BankingAccountService>();
            services.AddScoped<IBrokerService, BrokerService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IFinancialInfoService, FinancialInfoService>();
            services.AddScoped<IJobInfoService, JobInfoService>();
            services.AddScoped<IJobService, JobService>();
            services.AddScoped<IPrivatePersonService, PrivatePersonService>();
            services.AddScoped<ITradingCodeService, TradingCodeService>();
            services.AddScoped<IProvinceService, ProvinceService>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IAddressSectionService, AddressSectionService>();
            services.AddScoped<IBankService, BankService>();
            services.AddScoped<ILegalPersonShareholderService, LegalPersonShareholderService>();
            services.AddScoped<ILegalPersonStakeholderService, LegalPersonStakeholderService>();
            services.AddScoped<ILegalPersonService, LegalPersonService>();
            services.AddScoped<IIbanInquiryService, IbanInquiryService>();
            services.AddScoped<IMessagingService, MessagingService>();
            services.AddScoped<IIlencInquiryService, IlencInquiryService>();
            services.AddScoped<INocrInquiryService, NocrInquiryService>();
            services.AddScoped<ILegacyCodeInquiryService, LegacyCodeInquiryService>();
            services.AddScoped<IIrenexInquiryService, IrenexInquiryService>();
            services.AddScoped<IIndividualInquiryService, IndividualInquiryService>();
            services.AddScoped<IInstitutionInquiryService, InstitutionInquiryService>();
            services.AddScoped<IThirdPartyServiceService, ThirdPartyServiceService>();
            services.AddScoped<IProfileService, ProfileService>();
            services.AddScoped<IProfilePackageService, ProfilePackageService>();
            services.AddScoped<IProfileHistoryService, ProfileHistoryService>();
            services.AddScoped<IPermanentOtpService, PermanentOtpService>();
            services.AddScoped<IPermanentOtpApiService, PermanentOtpApiService>();
            services.AddScoped<IIxService, IxService>();
            services.AddScoped<IPostInquiryService, PostInquiryService>();
            services.AddScoped<IFgmService, FgmService>();
            services.AddScoped<IDeadInquiryService, DeadInquiryService>();
            services.AddScoped<IShahkarService, ShahkarService>();
            services.AddScoped<IMasterCacheProvider, MasterCacheProvider>();
            services.AddScoped<IMsisdnService, MsisdnService>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IOfflineTaskService, OfflineTaskService>();

        }

        public static void AddMapper(this IServiceCollection services, IConfiguration configuration)
        {
            //mapper
            services.AddSingleton(provider =>
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<MemberPrivatePerson, PrivatePersonMembership>()
                        .ForMember(dis => dis.FirstName, opt => opt.MapFrom(s => s.FirstName.Unify()))
                        .ForMember(dis => dis.LastName, opt => opt.MapFrom(s => s.LastName.Unify()))
                        .ForMember(dis => dis.FatherName, opt => opt.MapFrom(s => s.FatherName.Unify()))
                        .ForMember(dis => dis.PlaceOfBirth, opt => opt.MapFrom(s => s.PlaceOfBirth.Unify()))
                        .ForMember(dis => dis.PlaceOfIssue, opt => opt.MapFrom(s => s.PlaceOfIssue.Unify()))
                        .ForMember(dis => dis.Mobile, opt => opt.MapFrom(s => s.Mobile.Unify()))
                        .ForMember(dis => dis.Serial, opt => opt.MapFrom(s => s.Serial.Unify()))
                        .ForMember(dis => dis.SeriSh, opt => opt.MapFrom(s => s.SeriSh.Unify()))
                        .ForMember(dis => dis.SeriShChar, opt => opt.MapFrom(s => s.SeriShChar.Unify()))
                        .ForMember(dis => dis.ShNumber, opt => opt.MapFrom(s => s.ShNumber.Unify()));

                    cfg.CreateMap<MemberAddressViewModel, AddressMembership>()
                        .ForMember(dis => dis.Address, opt => opt.MapFrom(s => s.Address.Unify()))
                        .ForMember(dis => dis.PostalCode, opt => opt.MapFrom(s => s.PostalCode.Unify()))
                        .ForMember(dis => dis.Tel, opt => opt.MapFrom(s => s.Tel.Unify()));

                    cfg.CreateMap<MemberBankingAccountViewModel, BankingAccountMembership>()
                        .ForMember(dis => dis.AccountNumber, opt => opt.MapFrom(s => s.AccountNumber.Unify()))
                        .ForMember(dis => dis.BranchName, opt => opt.MapFrom(s => s.BranchName.Unify()))
                        .ForMember(dis => dis.BranchCode, opt => opt.MapFrom(s => s.BranchCode.Unify()))
                        .ForMember(dis => dis.Sheba, opt => opt.MapFrom(s => s.Sheba.Unify()));

                    cfg.CreateMap<Agent, AgentArchive>()
                        .ForMember(dis => dis.AgentCreationDate, opt => opt.MapFrom(s => s.CreationDate))
                        .ForMember(dis => dis.AgentModifiedDate, opt => opt.MapFrom(s => s.ModifiedDate))
                        .ForMember(dis => dis.AgentId, opt => opt.MapFrom(s => s.Id))
                        .ForMember(dis => dis.Id, opt => opt.Ignore());

                    cfg.CreateMap<AgentDocument, AgentDocumentArchive>()
                        .ForMember(dis => dis.AgentDocumentCreationDate, opt => opt.MapFrom(s => s.CreationDate))
                        .ForMember(dis => dis.AgentDocumentModifiedDate, opt => opt.MapFrom(s => s.ModifiedDate))
                        .ForMember(dis => dis.AgentDocumentId, opt => opt.MapFrom(s => s.Id))
                        .ForMember(dis => dis.Id, opt => opt.Ignore());

                    cfg.CreateMap<Domain.Model.Profile, ProfileViewModel>();
                    cfg.CreateMap<PrivatePerson, PrivatePersonViewModel>();
                    cfg.CreateMap<LegalPerson, LegalPersonViewModel>();
                    cfg.CreateMap<Address, AddressViewModel>();
                    cfg.CreateMap<TradingCode, TradingCodeViewModel>();
                    cfg.CreateMap<Agent, AgentViewModel>();
                    cfg.CreateMap<BankingAccount, BankingAccountViewModel>();
                    cfg.CreateMap<JobInfo, JobInfoViewModel>();
                    cfg.CreateMap<FinancialInfo, FinancialInfoViewModel>();
                    cfg.CreateMap<LegalPersonShareholder, LegalPersonShareholderViewModel>();
                    cfg.CreateMap<Country, CountryViewModel>();
                    cfg.CreateMap<Province, ProvinceViewModel>();
                    cfg.CreateMap<City, CityViewModel>();
                    cfg.CreateMap<AddressSection, AddressSection>();
                    cfg.CreateMap<Broker, BrokerViewModel>();
                    cfg.CreateMap<FinancialBroker, FinancialBrokerViewModel>();
                    cfg.CreateMap<Bank, BankViewModel>();
                    cfg.CreateMap<Job, JobViewModel>();
                    cfg.CreateMap<EtfMemberShipViewModel, EtfMembership>();
                    cfg.CreateMap<BatchPrivatePersonResultViewModel, PrivatePersonViewModel>();

                    cfg.CreateMap<File, FileViewModel>()
                        .ForMember(file => file.FileName,
                            exp => exp.ResolveUsing(fileViewModel =>
                                fileViewModel.FileName
                                    = $"{configuration.GetValue<string>("FileStorageConfiguration:CdnFilePath")}/{fileViewModel.FileName}"));
                });
                return config.CreateMapper();
            });
        }

        public static void AddCerberusHttpClients(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpClient<IIssuanceStockCodeService, IssuanceStockCodeService>(client => { client.BaseAddress = new Uri(configuration.GetSection("IssuanceStockCodeConfiguration:Url").Value); })
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true });

            services.AddHttpClient("IxService", client => { client.BaseAddress = new Uri(configuration.GetSection("IxServiceConfiguration:IxAwaGetTradeCodeUrl").Value); })
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true });

            services.AddHttpClient("IxServiceAccessToken", client => { client.BaseAddress = new Uri(configuration.GetSection("IxServiceConfiguration:IxAwaGetAccessTokenUrl").Value); })
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true });

            services.AddHttpClient("IxServiceRefreshAccessToken", client => { client.BaseAddress = new Uri(configuration.GetSection("IxServiceConfiguration:IxAwaRefreshAccessTokenUrl").Value); })
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true });
        }
    }
}
﻿using Cerberus.Domain.Enum;

namespace Cerberus.OfflineTasks
{
    public class DeleteAgentEighteenConfiguration
    {
        public string MessageSevenDays { get; set; }
        public string MessageOneDays { get; set; }
        public ProfileStatus StatusAfterEighteen { get; set; }
        public ProfileStatus IntProfileStatus { get; set; }
        public string MessageSevenDaysSejami { get; set; }
        public string MessageOneDaysSejami { get; set; }

        public string MessageSevenDaysTraceCode { get; set; }
        public string MessageOneDaysTraceCode { get; set; }

    }
}
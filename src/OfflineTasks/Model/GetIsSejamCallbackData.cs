﻿using System.Collections.Generic;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.OfflineTasks.Model
{
    public class GetIsSejamCallbackData
    {
        public IList<KeyValuePair<string, IsSejamiViewModel>> Success { get; set; }
        public IList<CallbackErrorDataModel> Failed { get; set; }
        public long ReferenceId { get; set; }
    }
}
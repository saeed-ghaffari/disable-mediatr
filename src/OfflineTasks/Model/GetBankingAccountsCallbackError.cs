﻿namespace Cerberus.OfflineTasks.Model
{
    public class GetBankingAccountsCallbackError
    {
        public string UniqueIdentifier { get; set; }
        public string[] Meta { get; set; }
    }
}
﻿namespace Cerberus.OfflineTasks.Model
{
    public class CreateMemberCallbackError
    {
        public string UniqueIdentifier { get; set; }
        public string[] Meta { get; set; }
    }
}
﻿using System.Collections.Generic;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.OfflineTasks.Model
{
    public class GetPrivatePersonCallbackData
    {
        public IList<KeyValuePair<string, PrivatePersonViewModel>> Success { get; set; }
        public IList<CallbackErrorDataModel> Failed { get; set; }
        public long ReferenceId { get; set; }
    }
}
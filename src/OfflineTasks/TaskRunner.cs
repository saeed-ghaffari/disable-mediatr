﻿using System;
using System.Net;
using Aspina;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.OfflineTasks.Executers;
using Cerberus.TasksManager.Core.Implementation;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.Utility.JsonConverters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Cerberus.OfflineTasks
{
    public static class TaskRunner
    {
        public static void Init(IConfiguration configuration)
        {
            ServicePointManager.DefaultConnectionLimit = int.MaxValue;
            var services = new ServiceCollection();
            services.AddCerberusServices(configuration);
            services.AddLogging(op => op.AddConsole().SetMinimumLevel(LogLevel.Error));

            services.AddMapper(configuration);

            services.AddCerberusHttpClients(configuration);

            //executers
            services.AddTransient<IbanInquiryExecuter>();
            services.AddTransient<IlencInquiryExecuter>();
            services.AddTransient<NocrInquiryExecuter>();
            services.AddTransient<AgentInquiryExecuter>();
            services.AddTransient<LegacyCodeInquiryExecuter>();
            services.AddTransient<IrenexInquiryExecuter>();
            services.AddTransient<IndividualInquiryExecuter>();
            services.AddTransient<InstitutionInquiryExecuter>();
            services.AddTransient<InquiryCheckerExecuter>();
            services.AddTransient<SyncMembershipProcessingExecuter>();
            services.AddTransient<CallbackThirdPartyServicesExecuter>();
            services.AddTransient<SynchronizerExecuter>();
            services.AddTransient<CreateInquiriesTasksExecuter>();
            services.AddTransient<GetBankingAccountsExecuter>();
            services.AddTransient<GetSejamStatusExecuter>();
            services.AddTransient<GetIsSejamiExecuter>();
            services.AddTransient<GetPrivatePersonExecuter>();
            services.AddTransient<GetTraceCodeExecuter>();
            services.AddTransient<SendSmsExecuter>();
            services.AddTransient<SendSmsExecuter1>();
            services.AddTransient<SendSmsExecuter2>();
            services.AddTransient<SendSmsExecuter3>();
            services.AddTransient<GetCompactProfileExecuter>();

            services.AddTransient<UpdatePrivatePersonExecuter>();

            services.AddTransient<ChangeToSemiSejamiExecuter>();
            services.AddTransient<SendReportBySmsExecuter>();
            services.AddTransient<UpdateBankingAccountExecuter>();
            services.AddTransient<CreateEditAfterSejamiTasksExecuter>();
            services.AddTransient<GetPrivatePersonWithStatusExecuter>();
            services.AddTransient<DeleteAgentEighteenExecuter>();
            services.AddTransient<ChangeProfileStatusExecuter>();
            services.AddTransient<IssuanceStockCodeOneExecuter>();
            services.AddTransient<PostInquiryExecuter>();
            services.AddTransient<ChangeProfileDataExecuter>();
            services.AddTransient<SetFactorExecuter>();
            services.AddTransient<SetFakeFactorExecuter>();
            services.AddTransient<DeadSendInquiryExecuter>();
            services.AddTransient<DeadDownloadInquiryExecuter>();
            services.AddTransient<UpdateIssuanceStockCodeExecuter>();
            services.AddTransient<SyncStockCodeWithIxExecuter>();
            services.AddTransient<SyncEtfMembershipProcessingExecuter>();
            services.AddTransient<ShahkarInquiryExecuter>();
            services.AddTransient<InquirySuspiciousPaymentExecuter>();
            services.AddTransient<UpdateProfileCacheExecuter>();




            services.AddSingleton<ITaskExecuterFactory, PooledTaskExecuterFactory>(provider =>
             {
                 var factory = new PooledTaskExecuterFactory();
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.IbanInquiry.Value, type => provider.GetService<IbanInquiryExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.IlencInquiry.Value, type => provider.GetService<IlencInquiryExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.NocrInquiry.Value, type => provider.GetService<NocrInquiryExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.LegacyInquiry.Value, type => provider.GetService<LegacyCodeInquiryExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.IrenexInquiry.Value, type => provider.GetService<IrenexInquiryExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.IndividualInquiry.Value, type => provider.GetService<IndividualInquiryExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.InstitutionInquiry.Value, type => provider.GetService<InstitutionInquiryExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.InquiryChecker.Value, type => provider.GetService<InquiryCheckerExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.AgentInquiry.Value, type => provider.GetService<AgentInquiryExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.SyncMembershipProcessing.Value, type => provider.GetService<SyncMembershipProcessingExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Value, type => provider.GetService<CallbackThirdPartyServicesExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.Synchronizer.Value, type => provider.GetService<SynchronizerExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.CreateInquiriesTasks.Value, type => provider.GetService<CreateInquiriesTasksExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.GetBankingAccounts.Value, type => provider.GetService<GetBankingAccountsExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.GetProfileIsSejami.Value, type => provider.GetService<GetIsSejamiExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.GetProfileStatus.Value, type => provider.GetService<GetSejamStatusExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.GetPrivatePerson.Value, type => provider.GetService<GetPrivatePersonExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.GetTraceCode.Value, type => provider.GetService<GetTraceCodeExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.SendSms.Value, type => provider.GetService<SendSmsExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.GetCompactProfile.Value, type => provider.GetService<GetCompactProfileExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.UpdatePrivatePerson.Value, type => provider.GetService<UpdatePrivatePersonExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.ChangeProfileToSemiSejami.Value, type => provider.GetService<ChangeToSemiSejamiExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.SendReportBySms.Value, type => provider.GetService<SendReportBySmsExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.UpdateBanckingAccount.Value, type => provider.GetService<UpdateBankingAccountExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.CreateEditAfterSejamiTasks.Value, type => provider.GetService<CreateEditAfterSejamiTasksExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.GetProfilePrivatePerson.Value, type => provider.GetService<GetPrivatePersonWithStatusExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.DeleteAgentEighteen.Value, type => provider.GetService<DeleteAgentEighteenExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.ChangeProfileStatus.Value, type => provider.GetService<ChangeProfileStatusExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.IssuanceStockCodeOne.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.ChangeProfileData.Value, type => provider.GetService<ChangeProfileDataExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.DeadRequestInquiry.Value, type => provider.GetService<DeadSendInquiryExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.DeadDownloadInquiry.Value, type => provider.GetService<DeadDownloadInquiryExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.SetFactor.Value, type => provider.GetService<SetFactorExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.SetFakeFactor.Value, type => provider.GetService<SetFakeFactorExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.UpdateIssuanceStockCode.Value, type => provider.GetService<UpdateIssuanceStockCodeExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.SyncStockCodeWithIx.Value, type => provider.GetService<SyncStockCodeWithIxExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.SendSms1.Value, type => provider.GetService<SendSmsExecuter1>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.SendSms2.Value, type => provider.GetService<SendSmsExecuter2>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.SendSms3.Value, type => provider.GetService<SendSmsExecuter3>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.SyncEtfMembershipProcessing.Value, type => provider.GetService<SyncEtfMembershipProcessingExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeOne.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeTwo.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeThree.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeFour.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeFive.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeSix.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeSeven.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeEight.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeNine.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeTen.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeEleven.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeTwelve.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeThirteen.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeFourteen.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeFifteen.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeSixteen.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeSeventeen.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeEighteen.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeNinteen.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.OldIssuanceStockCodeTwenty.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.IssuanceStockCodeTwo.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.IssuanceStockCodeThree.Value, type => provider.GetService<IssuanceStockCodeOneExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.ShahkarInquiry.Value, type => provider.GetService<ShahkarInquiryExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.InquirySuspiciousPayment.Value, type => provider.GetService<InquirySuspiciousPaymentExecuter>());
                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.UpdateProfileCache.Value, type => provider.GetService<UpdateProfileCacheExecuter>());





                 factory.RegisterConstructor(CerberusTasksIdentifierHelper.PostInquiry.Value, type => provider.GetService<PostInquiryExecuter>());

                 return factory;
             });

            services.AddSingleton<TaskManager>(provider => new TaskManager(
                configuration.GetValue<int>("OfflineTasksConfiguration:RunnerId"),
                5000,
                provider.GetService<ITaskExecuterFactory>(),
                provider.GetService<ITaskRepository>(),
                provider.GetService<IRecurringTaskRepository>()));

            var defaultJsonConvertSetting = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,

            };

            defaultJsonConvertSetting.Converters.Add(new StandardDateTimeConverter());

            JsonConvert.DefaultSettings = () => defaultJsonConvertSetting;
            services.AddHostedService<QueuedHostedService>();
            services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();
            var serviceProvider = services.BuildServiceProvider();

            var taskManager = serviceProvider.GetService<TaskManager>();

            taskManager.Start();

            Console.WriteLine("Start");

            Console.ReadLine();
        }
    }
}
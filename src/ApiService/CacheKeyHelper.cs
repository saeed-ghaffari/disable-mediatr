﻿using System;
using Cerberus.Domain.Enum;

namespace Cerberus.ApiService
{
    internal static class CacheKeyHelper
    {
        private const string PreFix = "cacheS:";

        public static class Addresses
        {
            internal static string GetKey(string uniqueIdentifier)
            {
                return $"{PreFix}:profile:u:{uniqueIdentifier}:addresses";
            }
        }

        public static class Profile
        {
            public static string GetKey(string uniqueIdentifier, bool includeAllRelations)
            {
                return $"{PreFix}:profile:u:{uniqueIdentifier}:{includeAllRelations}";
            }
            public static string GetAuthenticatorKey(string uniqueIdentifier)
            {
                return $"{PreFix}:profile:u:{uniqueIdentifier}:Authenticator";
            }

            public static string GetMasterCacheKey(string uniqueIdentifier) => $"master-cache-profile-{uniqueIdentifier}";
        }

        public static class BankingAccount
        {
            internal static string GetKey(string uniqueIdentifier)
            {
                return $"{PreFix}:profile:u:{uniqueIdentifier}:accounts";
            }
        }

        public class FinancialInfo
        {
            internal static string GetKey(string uniqueIdentifier)
            {
                return $"{PreFix}:profile:u:{uniqueIdentifier}:financialInfo";
            }
        }

        public class JobInfo
        {

            internal static string GetKey(string uniqueIdentifier)
            {
                return $"{PreFix}:profile:u:{uniqueIdentifier}:jobInfo";
            }
        }

        public class LegalPerson
        {
            internal static string GetKey(string uniqueIdentifier)
            {
                return $"{PreFix}:profile:u:{uniqueIdentifier}:legalPerson";
            }
        }

        public class PrivatePerson
        {
            internal static string GetKey(string uniqueIdentifier)
            {
                return $"{PreFix}:profile:u:{uniqueIdentifier}:privatePerson";
            }
        }

        public class TradingCode
        {

            internal static string GetKey(string uniqueIdentifier)
            {
                return $"{PreFix}:profile:u:{uniqueIdentifier}:tradingCodes";
            }
        }
        public class PermanentOtp

        {
            internal static string GetKey(string uniqueIdentifier, long serviceId,OtpTypes type)
            {
                return $"{PreFix}:PermanentOtp:{uniqueIdentifier}:Person:{serviceId}:Type:{type}";
            }
        }

    }
}
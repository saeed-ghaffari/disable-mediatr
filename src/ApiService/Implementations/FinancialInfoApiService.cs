﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Service.Implementations;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cerberus.ApiService.Implementations
{
    public class FinancialInfoApiService : BaseObjectCacheService<FinancialInfo, FinancialInfoViewModel>, IFinancialInfoApiService
    {
        private readonly IFinancialInfoService _financialInfoService;

        public FinancialInfoApiService(
            ICacheProvider cacheProvider,
            ILogger<FinancialInfoApiService> logger,
            IOptions<ObjectCacheServicesConfiguration> options,
            IProfileApiService profileApiService,
            IMapper mapper,
            IFinancialInfoService financialInfoService)
            : base(cacheProvider, logger, options.Value.ReconstructTime, options.Value.FinancialInfoExpiration, options.Value.Ttl, profileApiService, mapper)
        {
            _financialInfoService = financialInfoService;
        }


        protected override ProfileViewModel UpdateCurrentProfileChildAsync(ProfileViewModel profile, FinancialInfoViewModel dataToStore)
        {
            profile.FinancialInfo = dataToStore;

            return profile;
        }

        protected override string GenerateCacheKey(string uniqueIdentifier) =>
            CacheKeyHelper.FinancialInfo.GetKey(uniqueIdentifier);

        protected override Task<FinancialInfo> GetObjectFromMasterDatabaseAsync(string uniqueIdentifier)
        {
            return _financialInfoService.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }
    }
}
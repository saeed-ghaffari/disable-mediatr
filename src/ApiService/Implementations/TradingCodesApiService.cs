﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Service.Implementations;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cerberus.ApiService.Implementations
{
    public class TradingCodesApiService : BaseListObjectCacheService<TradingCode, TradingCodeViewModel>, ITradingCodeApiService
    {
        private readonly ITradingCodeService _tradingCodeService;

        public TradingCodesApiService(
            ICacheProvider cacheProvider,
            ILogger<TradingCodesApiService> logger,
            IOptions<ObjectCacheServicesConfiguration> options,
            IProfileApiService profileApiService,
            IMapper mapper, ITradingCodeService tradingCodeService)
            : base(cacheProvider, logger, options.Value.ReconstructTime, options.Value.TradingCodesExpiration, options.Value.Ttl, profileApiService, mapper)
        {
            _tradingCodeService = tradingCodeService;
        }

        protected override ProfileViewModel UpdateCurrentProfileChildAsync(ProfileViewModel profile, IEnumerable<TradingCodeViewModel> dataToStore)
        {
            profile.TradingCodes = dataToStore;

            return profile;
        }

        protected override string GenerateCacheKey(string uniqueIdentifier) =>
            CacheKeyHelper.TradingCode.GetKey(uniqueIdentifier);


        protected override Task<IEnumerable<TradingCode>> GetObjectFromMasterDatabaseAsync(string uniqueIdentifier)
        {
            return _tradingCodeService.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);

        }
    }
}
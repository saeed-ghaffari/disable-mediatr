﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Service.Implementations;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cerberus.ApiService.Implementations
{
    public class BankingAccountApiService : BaseListObjectCacheService<BankingAccount, BankingAccountViewModel>, IBankingAccountApiService
    {
        private readonly IBankingAccountService _bankingAccountService;
        public BankingAccountApiService(
            ICacheProvider cacheProvider,
            ILogger<BankingAccountApiService> logger,
            IOptions<ObjectCacheServicesConfiguration> options,
            IProfileApiService profileApiService,
            IMapper mapper, IBankingAccountService bankingAccountService)
            : base(cacheProvider, logger, options.Value.ReconstructTime, options.Value.BankingAccountsExpiration, options.Value.Ttl, profileApiService, mapper)
        {
            _bankingAccountService = bankingAccountService;
        }

        protected override ProfileViewModel UpdateCurrentProfileChildAsync(ProfileViewModel profile, IEnumerable<BankingAccountViewModel> dataToStore)
        {
            profile.Accounts = dataToStore;

            return profile;
        }

        protected override string GenerateCacheKey(string uniqueIdentifier) =>
            CacheKeyHelper.BankingAccount.GetKey(uniqueIdentifier);

        protected override Task<IEnumerable<BankingAccount>> GetObjectFromMasterDatabaseAsync(string uniqueIdentifier)
        {
            return _bankingAccountService.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }
    }
}
﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Service.Implementations;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Localization.Internal;
using Microsoft.Extensions.Logging;
using Profile = AutoMapper.Profile;

namespace Cerberus.ApiService.Implementations
{
    public abstract class BaseObjectCacheService<TModel, TViewModel> : BaseService, IObjectCacheService<TModel, TViewModel>
        where TViewModel : new()
        where TModel : BaseModel
    {
        private readonly IMapper _mapper;
        private readonly IProfileApiService _profileApiService;
        private readonly ICacheProvider _cacheProvider;
        private readonly TimeSpan? _cacheExpiration;
        private readonly TimeSpan? _ttl;

        protected BaseObjectCacheService(
            ICacheProvider cacheProvider,
            ILogger logger,
            TimeSpan? reconstructTime,
            TimeSpan? cacheExpiration,
            TimeSpan? ttl,
            IProfileApiService profileApiService, IMapper mapper)
            : base(cacheProvider, logger, reconstructTime, cacheExpiration, ttl)
        {
            _cacheExpiration = cacheExpiration;
            _ttl = ttl;
            _cacheProvider = cacheProvider;
            _profileApiService = profileApiService;
            _mapper = mapper;
        }


        public async Task StoreAsync(string uniqueIdentifier, ProfileOwnerType? type)
        {

            var stopWatch = new Stopwatch();

            stopWatch.Start();
            TModel objectToStore = await GetObjectFromMasterDatabaseAsync(uniqueIdentifier);

            Console.WriteLine($"Get From DB({typeof(TModel).FullName}) : {stopWatch.ElapsedMilliseconds}");

            await StoreAsync(objectToStore, uniqueIdentifier, type);
        }

        public Task StoreAsync(TModel value, string uniqueIdentifier, ProfileOwnerType? type)
        {
            var stopWatch = new Stopwatch();

            stopWatch.Start();
            TViewModel mappedObject = _mapper.Map<TModel, TViewModel>(value);

            Console.WriteLine($"Map ({typeof(TModel).FullName}) : {stopWatch.ElapsedMilliseconds}");

            return StoreAsync(mappedObject, uniqueIdentifier, type);


        }

        public async Task StoreAsync(TViewModel viewModelValue, string uniqueIdentifier, ProfileOwnerType? type)
        {
            var stopWatch = new Stopwatch();

            stopWatch.Start();
            //store current child object
            await _cacheProvider.StoreAsync(GenerateCacheKey(uniqueIdentifier), viewModelValue,
                _cacheExpiration, _ttl);

            Console.WriteLine($"Store Child ({typeof(TModel).FullName}) : {stopWatch.ElapsedMilliseconds}");

          

            //update profile object

            await UpdateProfileChildObjectAsync(uniqueIdentifier, viewModelValue, type);
        }

        public async Task RemoveAsync(string uniqueIdentifier)
        {
            await _cacheProvider.RemoveAsync(GenerateCacheKey(uniqueIdentifier));
        }

        public Task<TViewModel> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return FetchAsync(GenerateCacheKey(uniqueIdentifier),
                async () =>
                {
                    var result = await GetObjectFromMasterDatabaseAsync(uniqueIdentifier);
                    return _mapper.Map<TModel, TViewModel>(result);
                });
        }

        public async Task UpdateProfileChildObjectAsync(string uniqueIdentifier, TViewModel childObject, ProfileOwnerType? type)
        {
            var stopWatch = new Stopwatch();

            stopWatch.Start();
            ProfileViewModel profile = await _profileApiService.GetByUniqueIdentifierAsync(uniqueIdentifier, true, type);

            Console.WriteLine($"Get Whole object ({typeof(TModel).FullName}) : {stopWatch.ElapsedMilliseconds}");

            UpdateCurrentProfileChildAsync(profile, childObject);

            await _profileApiService.StoreAsync(profile);
        }


        protected abstract ProfileViewModel UpdateCurrentProfileChildAsync(ProfileViewModel profile, TViewModel dataToStore);

        protected abstract string GenerateCacheKey(string uniqueIdentifier);

        protected abstract Task<TModel> GetObjectFromMasterDatabaseAsync(string uniqueIdentifier);


    }
}
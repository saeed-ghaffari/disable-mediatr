﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain;
using Cerberus.Domain.Common;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Service;
using Cerberus.Service.Implementations;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Profile = Cerberus.Domain.Model.Profile;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.ApiService.Implementations
{
    public class ProfileApiService : BaseService, IProfileApiService
    {
        private readonly IMapper _mapper;
        private readonly IProfileService _profileService;
        private readonly ICacheProvider _cacheProvider;
        private readonly IOptions<ObjectCacheServicesConfiguration> _options;
        private readonly IIxService _ixService;
        private readonly IxServiceConfiguration _ixServiceConfiguration;
        private readonly ILogger<ProfileApiService> _logger;
        private readonly IIssuanceStockCodeRepository _issuanceStockCodeRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly IOptions<MasterCacheProviderConfiguration> _masterCacheOptions;

        public ProfileApiService(
            IMapper mapper,
            IProfileService profileService,
            ICacheProvider cacheProvider,
            ILogger<ProfileApiService> logger,
            IOptions<ObjectCacheServicesConfiguration> options,
            IIxService ixService,
            IOptions<IxServiceConfiguration> ixServiceConfiguration,
            IIssuanceStockCodeRepository issuanceStockCodeRepository,
            ITaskRepository taskRepository,
            IMasterCacheProvider masterCacheProvider,
            IOptions<MasterCacheProviderConfiguration> masterCacheOptions) :
            base(
                cacheProvider,
                logger,
                options.Value.ReconstructTime,
                options.Value.AddressesExpiration,
                options.Value.Ttl)
        {
            _mapper = mapper;
            _profileService = profileService;
            _options = options;
            _ixService = ixService;
            _issuanceStockCodeRepository = issuanceStockCodeRepository;
            _taskRepository = taskRepository;
            _masterCacheProvider = masterCacheProvider;
            _masterCacheOptions = masterCacheOptions;
            _ixServiceConfiguration = ixServiceConfiguration.Value;
            _cacheProvider = cacheProvider;
            _logger = logger;
        }

        private async Task<Cerberus.Domain.Model.Profile> GetProfileAsync(string uniqueIdentifier, ProfileOwnerType? type = null)
        {
            return await _profileService.GetByUniqueIdentifierAsync(uniqueIdentifier, true, true, type);

        }

        public async Task<ProfileViewModel> GetByUniqueIdentifierAsync(string uniqueIdentifier, bool includeAllRelations = false, ProfileOwnerType? type = null)
        {
            return await FetchAsync(CacheKeyHelper.Profile.GetKey(uniqueIdentifier, includeAllRelations),
                      async () =>
                      {
                          var profile = includeAllRelations
                              ? await GetProfileAsync(uniqueIdentifier, type)
                              : await _profileService.GetByUniqueIdentifierAsync(uniqueIdentifier, false, false);
                          return _mapper.Map<Domain.Model.Profile, ProfileViewModel>(profile);
                      });
        }

        public async Task<ProfileViewModel> GetForProfilingByUniqueIdentifierAsync(string uniqueIdentifier, bool includeAllRelations = false,
            ProfileOwnerType? type = null)
        {
            var result = await GetByUniqueIdentifierAsync(uniqueIdentifier, includeAllRelations, type);


            if (result != null && includeAllRelations)
            {
                //clear trading code 
              Console.WriteLine($"get profiling for uniqueIdentifier: {uniqueIdentifier}");
                var res = result.IssuanceStockCodes?.LastOrDefault();
                if (res != null)
                {
                    var tradingCodes = new List<TradingCodeViewModel>();
                    foreach (var response in res.IssuanceStockCodeResponses)
                    {
                        tradingCodes.Add(new TradingCodeViewModel
                        {
                            FirstPart = response.LegacyCode,
                            SecondPart = string.Empty,
                            ThirdPart = string.Empty,
                            Type = TradingType.StockExchange
                        });
                    }
                    result.TradingCodes = tradingCodes;
                }


                ////if there was no need to get legacy code from ix or database
                //if (!_ixServiceConfiguration.EnableIxService && !_ixServiceConfiguration.EnableLegacyCodeFromDb)
                //    return result;

                ////if just need to read legacy code from database not IX
                //if (!_ixServiceConfiguration.EnableIxService && _ixServiceConfiguration.EnableLegacyCodeFromDb)
                //{
                //    try
                //    {
                //        return await GetLegacyCodeFromDbAsync(result);
                //    }
                //    catch (Exception e)
                //    {
                //        _logger.LogCritical(e, "Unable to get data from Sejam database");

                //        throw new CerberusException(ErrorCode.Conflicts, "Unable to get trading code.");
                //    }
                //}

                ////if just need to read legacy from IX not database
                //if (_ixServiceConfiguration.EnableIxService && !_ixServiceConfiguration.EnableLegacyCodeFromDb)
                //{
                //    try
                //    {
                //        return await GetLegacyCodeFromIxAsync(result);
                //    }
                //    catch (Exception e)
                //    {
                //        _logger.LogCritical(e, "Unable to get data from IX Service");
                //        throw new CerberusException(ErrorCode.Conflicts, "Unable to get trading code.");
                //    }
                //}

                ////if we want to use IX and sejam database -- if IX didn't respond then use sejam database
                //try
                //{
                //   await GetLegacyCodeFromIxAsync(result);

                //    //if ix response was empty we check database for legacy code
                //    if (result.TradingCodes == null || !result.TradingCodes.Any())
                //        return await GetLegacyCodeFromDbAsync(result);

                //    return result;
                //}
                //catch (Exception ex)
                //{
                //    try
                //    {
                //      return await GetLegacyCodeFromDbAsync(result);
                //    }
                //    catch (Exception e)
                //    {
                //        _logger.LogCritical(e, "Unable to get legacy code from database");

                //        throw new CerberusException(ErrorCode.Conflicts, "Unable to get trading code.");
                //    }

                //}

            }

            return result;
        }

        public async Task<ProfileViewModel> GetAuthenticatorProfileByUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return await FetchAsync(CacheKeyHelper.Profile.GetAuthenticatorKey(uniqueIdentifier),
                async () =>
                {
                    var profile = await _profileService.GetAuthenticatorProfileByUniqueIdentifierAsync(uniqueIdentifier);
                    return _mapper.Map<Domain.Model.Profile, ProfileViewModel>(profile);
                });
        }

        public async Task StoreAsync(string uniqueIdentifier, ProfileOwnerType? type)
        {
            await _cacheProvider.StoreAsync(key: CacheKeyHelper.Profile.GetKey(uniqueIdentifier: uniqueIdentifier, includeAllRelations: true),
                value: _mapper.Map<Domain.Model.Profile, ProfileViewModel>(
                    source: await GetProfileAsync(uniqueIdentifier: uniqueIdentifier, type: type)), expiration: _options.Value.ProfileExpiration, ttl: _options.Value.Ttl);

            await _cacheProvider.StoreAsync(key: CacheKeyHelper.Profile.GetKey(uniqueIdentifier: uniqueIdentifier, includeAllRelations: false),
                value: _mapper.Map<Domain.Model.Profile, ProfileViewModel>(
                    source: await _profileService.GetByUniqueIdentifierAsync(uniqueIdentifier: uniqueIdentifier, includeAllRelations: false, includeAllChildRelations: false, type: type)), expiration: _options.Value.ProfileExpiration, ttl: _options.Value.Ttl);
        }

        public async Task StoreAsync(ProfileViewModel profile, bool updateWholeObject = true, ProfileOwnerType? type = null)
        {
            if (updateWholeObject)
                await _cacheProvider.StoreAsync(CacheKeyHelper.Profile.GetKey(profile.UniqueIdentifier, true),
                   profile, _options.Value.ProfileExpiration, _options.Value.Ttl);

            else
            {
                var cacheProfile = await GetByUniqueIdentifierAsync(profile.UniqueIdentifier, true);

                cacheProfile.Email = profile.Email;
                cacheProfile.Mobile = profile.Mobile;
                cacheProfile.Status = profile.Status;
                cacheProfile.Type = profile.Type;
                cacheProfile.TraceCode = profile.TraceCode;


                await _cacheProvider.StoreAsync(CacheKeyHelper.Profile.GetKey(profile.UniqueIdentifier, true),
                    cacheProfile, _options.Value.ProfileExpiration, _options.Value.Ttl);

                await _cacheProvider.StoreAsync(CacheKeyHelper.Profile.GetKey(profile.UniqueIdentifier, false),
                    new ProfileViewModel()
                    {
                        Email = profile.Email,
                        Mobile = profile.Mobile,
                        Status = profile.Status,
                        Type = profile.Type,
                        TraceCode = profile.TraceCode,
                        Id = profile.Id,
                        UniqueIdentifier = profile.UniqueIdentifier

                    }, _options.Value.ProfileExpiration, _options.Value.Ttl);

            }
        }

        public async Task StoreProfileAsync(Profile profile)
        {
            if (profile != null)
            {
                //cache profile object
                await _masterCacheProvider.StoreAsync(
                    KeyGenerator.Profile.GetMasterCacheKey(profile.UniqueIdentifier),
                    profile,
                    _masterCacheOptions.Value.ProfileExpirationTime);

                //cache key/value as profileId/UniqueIdentifier to get profile from cache by profileId
                await _masterCacheProvider.StoreAsync(
                    KeyGenerator.Profile.GetMasterCacheKeyById(profile.Id),
                   new CacheIdentifierObject() { UniqueIdentifier = profile.UniqueIdentifier },
                    _masterCacheOptions.Value.ProfileExpirationTime);
            }
        }

        public async Task RemoveAsync(string uniqueIdentifier)
        {
            await _cacheProvider.RemoveAsync(CacheKeyHelper.Profile.GetKey(uniqueIdentifier, true));

            await _cacheProvider.RemoveAsync(CacheKeyHelper.Profile.GetKey(uniqueIdentifier, false));
        }

        private async Task<ProfileViewModel> GetLegacyCodeFromDbAsync(ProfileViewModel profile)
        {
            var legacyCode = await _issuanceStockCodeRepository.GetByProfileIdAsync(profile.Id, true);

            if (legacyCode == null)
                return profile;

            var tradingCodes = new List<TradingCodeViewModel>();
            foreach (var response in legacyCode.IssuanceStockCodeResponses.Where(x => x.IsActive).GroupBy(x => x.LegacyCode).Select(x => x.Last()))
            {
                tradingCodes.Add(new TradingCodeViewModel
                {
                    FirstPart = response.LegacyCode,
                    SecondPart = string.Empty,
                    ThirdPart = string.Empty,
                    Type = TradingType.StockExchange
                });
            }

            profile.TradingCodes = tradingCodes;

            return profile;
        }

        private async Task<ProfileViewModel> GetLegacyCodeFromIxAsync(ProfileViewModel profile)
        {
            var tradingCode = await _ixService.GetLegacyCodeAsync(profile.UniqueIdentifier);

            if (tradingCode?.LegacyCodeResponseList != null && tradingCode.LegacyCodeResponseList.Any())
            {
                //insert task for ix response with database for future use
                await _taskRepository.AddAsync(new ScheduledTask
                {
                    DueTime = DateTime.Now,
                    GroupId = CerberusTasksIdentifierHelper.SyncStockCodeWithIx.Key,
                    Type = CerberusTasksIdentifierHelper.SyncStockCodeWithIx.Value,
                    RefId = profile.Id,
                    Params = JsonConvert.SerializeObject(tradingCode, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        }),
                    Status = TaskStatus.Pending
                });


                var tradingCodes = new List<TradingCodeViewModel>();
                foreach (var item in tradingCode.LegacyCodeResponseList.Where(x => x.IsActive == true).ToList())
                {
                    tradingCodes.Add(new TradingCodeViewModel
                    {
                        FirstPart = item.LegacyCode,
                        SecondPart = string.Empty,
                        ThirdPart = string.Empty,
                        Type = TradingType.StockExchange
                    });
                }

                profile.TradingCodes = tradingCodes;
            }


            return profile;
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Service;
using Cerberus.Service.Implementations;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cerberus.ApiService.Implementations
{
    public class PermanentOtpApiService : BaseService, IPermanentOtpApiService
    {
        private readonly IPermanentOtpRepository _permanentOtpRepository;
        private readonly ICacheProvider _cacheProvider;
        private readonly IOptions<ObjectCacheServicesConfiguration> _objectCacheServiceConfiguration;

        public PermanentOtpApiService(IPermanentOtpRepository permanentOtpRepository,
            ICacheProvider cacheProvider,
            ILogger<PrivatePersonApiService> logger,
          IOptions<ObjectCacheServicesConfiguration> objectCacheServiceConfiguration)
        : base(cacheProvider, logger, objectCacheServiceConfiguration.Value.ReconstructTime, objectCacheServiceConfiguration.Value.PermanentOtpExpiration, objectCacheServiceConfiguration.Value.Ttl)
        {
            _permanentOtpRepository = permanentOtpRepository;
            _cacheProvider = cacheProvider;
            _objectCacheServiceConfiguration = objectCacheServiceConfiguration;
        }

        public async Task UpdateCacheAsync(PermanentOtp permanentOtp)
        {
            var key = CacheKeyHelper.PermanentOtp.GetKey(permanentOtp.UniqueIdentifier, permanentOtp.ServiceId, permanentOtp.Type);
            
            //update redis
            await _cacheProvider.StoreAsync(
                key: key,
                value: permanentOtp,
                expiration: _objectCacheServiceConfiguration.Value.PermanentOtpExpiration,
                ttl: _objectCacheServiceConfiguration.Value.Ttl);
        }

        public async Task RemoveAsync(PermanentOtp permanentOtp)
        {
            var key = CacheKeyHelper.PermanentOtp.GetKey(permanentOtp.UniqueIdentifier, permanentOtp.ServiceId, permanentOtp.Type);
            await _cacheProvider.RemoveAsync(key);
        }

    

        public async Task<PermanentOtp> GetAsync(long serviceId, string uniqueIdentifier,OtpTypes type)
        {
            return await FetchAsync(CacheKeyHelper.PermanentOtp.GetKey(uniqueIdentifier,serviceId,type),
                async () =>  await _permanentOtpRepository.GetAsync(serviceId, uniqueIdentifier,type)); 
        }

        public Task<PermanentOtp> GetAsync(PermanentOtp permanentOtp)
        {
            throw new NotImplementedException();
        }

    
    }

}
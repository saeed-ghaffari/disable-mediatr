﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskBroker.Domain.Common;

namespace TaskBroker.Domain.Events
{
    public class Event
    {
        public Event()
        {
            Timestamp = DateTime.Now;
        }

        public DateTime Timestamp { get; protected set; }

        public string RouteKey { get; set; }

        public Message Message { get; set; }
    }
}

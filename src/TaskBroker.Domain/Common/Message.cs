﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskBroker.Domain.Common
{
    public class Message
    {
        public Message()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        public string Data { get; set; }
    }
}

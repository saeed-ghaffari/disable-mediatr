﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using TaskBroker.Domain.Common;
using TaskBroker.Domain.Events;

namespace TaskBroker.Domain.Commands
{
    public class Command : IRequest<bool>
    {
        public Command()
        {
            Timestamp = DateTime.Now;
            this.GId = Guid.NewGuid();
        }

        public DateTime Timestamp { get; protected set; }

        public Guid GId { get; set; }

        public Message Message { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TaskBroker.Domain.Commands;

namespace TaskBroker.Domain.Commands
{
    public interface IEnqueueCommandHandler
    {
        Task<bool> Handle(Command request, CancellationToken cancellationToken);
    }
}

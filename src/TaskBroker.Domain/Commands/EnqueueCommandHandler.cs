﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using TaskBroker.Domain.Bus;
using TaskBroker.Domain.Commands;
using TaskBroker.Domain.Events;

namespace TaskBroker.Domain.Commands
{
    public class EnqueueCommandHandler : IEnqueueCommandHandler, IRequestHandler<Command, bool>
    {
        private readonly IEventBus _eventBus;

        public EnqueueCommandHandler(IEventBus eventBus)
        {
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        public Task<bool> Handle(Command request, CancellationToken cancellationToken)
        {
            _eventBus.Publish(new Event { Message = request.Message });

            return Task.FromResult(true);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskBroker.Domain.Commands;
using TaskBroker.Domain.Events;

namespace TaskBroker.Domain.Bus
{
    public interface IEventBus
    {
        Task SendCommand<TCommand>(TCommand command) where TCommand : Command;
        void Publish(Event @event);
        void Subscribe<TEventHandler>(Event @event) where TEventHandler : IEventHandler;
    }
}

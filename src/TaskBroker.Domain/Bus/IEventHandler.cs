﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskBroker.Domain.Events;

namespace TaskBroker.Domain.Bus
{
    public interface IEventHandler
    {
        Task Handle(Event @event);
    }
}

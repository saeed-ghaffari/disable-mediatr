﻿namespace Cerberus.Service
{
    public class ShahkarServiceConfiguration
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserKey { get; set; }
        public string ShahkarLoginUrl { get; set; }
        public string ShahkarUrl { get; set; }
        public string ProjectCode { get; set; }
        public string ShahkarWrapperUrl { get; set; }
        public int ShahkarInquiryTtl { get; set; }
    }
}
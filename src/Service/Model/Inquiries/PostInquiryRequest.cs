﻿using System.Collections.Generic;

namespace Cerberus.Service.Model.Inquiries
{
    public class PostInquiryRequest
    {
        public string Ssn { get; set; }
        public string BirthDate { get; set; }
        public string EndUsername { get; set; }
        public string EndIp { get; set; }
    }

    public class PostInquiryResponse
    {
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public List<string> Message { get; set; }
        public bool Successful { get; set; }
        public bool HasException { get; set; }
    }
}
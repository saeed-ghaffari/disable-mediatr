﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Service.Model
{
  public  class MelliPayResult
    {
            public string ResCode { get; set; }
            public string Token { get; set; }
            public string Description { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Service
{
    public class SejamCdnRequest
    {
        public string FileName { get; set; }
        public string FileInput { get; set; }
        public string MediaType { get; set; }
    }
}

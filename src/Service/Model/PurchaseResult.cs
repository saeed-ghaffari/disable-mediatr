﻿using Cerberus.Domain.ViewModel;

namespace Cerberus.Service.Model
{
    public class PurchaseResult
    {
        public string OrderId { get; set; }
        public string Token { get; set; }
        public string ResCode { get; set; }
        public MelliVerifyResult VerifyResultData { get; set; }
    }
}
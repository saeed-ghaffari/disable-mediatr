﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Service.Model
{
    public class SejamCdnResponse
    {
        public bool Successful { get; set; }
        public string Message { get; set; }
        public string FileGuid { get; set; }
    }
}

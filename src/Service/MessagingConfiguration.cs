﻿using System;

namespace Cerberus.Service
{
    public class MessagingConfiguration
    {
        public string CreateSessionMessage { get; set; }
        public int OtpMinValue { get; set; }
        public int OtpMaxValue { get; set; }
        public TimeSpan OtpTtl { get; set; } = DateTime.Now.AddHours(2).TimeOfDay;
        public string SmsApiUrl { get; set; }
        public string RegisterOtpMessage { get; set; }
        public string ChangeMobileOtpMessage { get; set; }
        public string ValidateAgentOtpMessage { get; set; }
        public TimeSpan ServiceProviderTimeout { get; set; }
        public string KycOtpMessage { get; set; }
        public string PishkhanOtpWithNoAgentMessage { get; set; }
        public string PishkhanOtpWithAgentMessage { get; set; }
        public string RecoverTraceCodeMessage { get; set; }
        public string SuccessRecoverTraceCode { get; set; }
        public string AuthenticationOfficeMessage { get; set; }
        public string OnSuccessAuthenticateMessage { get; set; }
        public string RemoveOldMobileMessage { get; set; }
        public string RegisterNewMobileMessage { get; set; }
        public string EntryNodeStatusOtpMessage { get; set; }
        public byte SendMessageDueTime { get; set; }
        public string SejamiProfileDeleteAgentMessage { get; set; }
        public string TraceCodeProfileDeleteAgentMessage { get; set; }
        public string InvalidInformationProfileDeleteAgentMessage { get; set; }
        public string SejamiProfileEditAgentMessage { get; set; }
        public string TraceCodeProfileEditAgentMessage { get; set; }

        public bool UseOriginalServer { get; set; }
        public bool UseRahyab { get; set; }
        public string OriginalSmsServerUrl { get; set; }
        public string RahyabSmsConectionString { get; set; }
        public string RahyabSmsFromMobile { get; set; }


        public string AdvertiseDdnMessage { get; set; }
    }
}
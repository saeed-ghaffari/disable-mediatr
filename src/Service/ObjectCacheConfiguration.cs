﻿using System;

namespace Cerberus.Service
{
    public class ObjectCacheConfiguration
    {
        public TimeSpan ReconstructTime { get; set; }
        public TimeSpan Ttl { get; set; }
        public TimeSpan MembershipExpiration { get; set; }

    }
}
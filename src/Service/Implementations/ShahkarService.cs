﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories.Inquiries;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Service.Model;
using Cerberus.Utility;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Cerberus.Service.Implementations
{
    public class ShahkarService : IShahkarService
    {
        private readonly ShahkarServiceConfiguration _serviceConfiguration;
        private readonly ILogger<ShahkarService> _logger;
        private readonly IShahkarInquiryRepository _shahkarInquiryRepository;
        private readonly IDatabase _readDatabase;
        private const string PreFix = "rayanAccessTokenService";
        public ShahkarService(IOptions<ShahkarServiceConfiguration> serviceConfiguration, ILogger<ShahkarService> logger, IShahkarInquiryRepository shahkarInquiryRepository, IDatabase readDatabase)
        {
            _logger = logger;
            _shahkarInquiryRepository = shahkarInquiryRepository;
            _readDatabase = readDatabase;
            _serviceConfiguration = serviceConfiguration.Value;
        }

        public async Task<bool> IsMobileForNationalCode(string mobile, string nationalCode)
        {
            var token = await ShahkarLogin();

            var shahkarUrl = string.Format(_serviceConfiguration.ShahkarUrl, nationalCode, NormalizeMobile(mobile));
            var auth = new AuthenticationHeaderValue("Bearer", token.Replace("\"", ""));
            var response = await HttpHepler.GetHttpResponseResultAsync(shahkarUrl, auth);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new CerberusException(ErrorCode.Unauthorized);

            return bool.Parse(response.Body);
        }

        private async Task<string> ShahkarLogin()
        {
            var token = await _readDatabase.StringGetAsync(PreFix);
            if (token != RedisValue.Null)
            {
                return token;
            }
            var login = new ShahkarLogin
            {
                UserName = _serviceConfiguration.UserName,
                Password = _serviceConfiguration.Password,
                ProjectCode = _serviceConfiguration.ProjectCode
            };


            var headers = new Dictionary<string, string> { { "UserKey", _serviceConfiguration.UserKey } };

            var response = await HttpHepler.PostAsync(_serviceConfiguration.ShahkarLoginUrl, JsonConvert.SerializeObject(login),
                "application/json", null, null, headers);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new CerberusException(ErrorCode.Unauthorized, response.Body);
        
            
           
            await _readDatabase.StringSetAsync(PreFix, response.Body, TimeSpan.FromHours(8));
            return response.Body;
        }

        public async Task<bool> IsValidMobile(string mobile, string nationalCode)
        {
           
            var existItem = await _shahkarInquiryRepository.GetBySsnAsync(nationalCode);

            var shahkarInquiry = existItem.FirstOrDefault(x=>x.CreationDate.AddHours(_serviceConfiguration.ShahkarInquiryTtl) > DateTime.Now && x.Mobile==mobile.ToLong());
            if (shahkarInquiry==null)
            {
                try
                {
                    var shahkarUrl = string.Format(_serviceConfiguration.ShahkarWrapperUrl, nationalCode,
                        NormalizeMobile(mobile), _serviceConfiguration.UserName);

                    var response = await HttpHepler.GetHttpResponseResultAsync(shahkarUrl);

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        _logger.LogCritical(response.Body, "Shahkar Service exception");
                        throw new CerberusException(ErrorCode.Unauthorized);
                    }

                    var jsonResponse = JsonConvert.DeserializeObject<bool>(response.Body);

                    await _shahkarInquiryRepository.CreateAsync(new ShahkarInquiry
                    {
                        Mobile = mobile.ToLong(),
                        Successful = jsonResponse,
                        SuccessInquiry = true,
                        Ssn = nationalCode,
                        HasException = false,
                    });
                    return jsonResponse;
                }
                catch (Exception e)
                {
                    _logger.LogCritical(e, " Shahkar Service unavailable");
                    throw new CerberusException(ErrorCode.UnknownServerError);
                }
            }

            return shahkarInquiry.Successful;

        }


        private string NormalizeMobile(string mobile)
        {
            if (mobile.StartsWith("98"))
                mobile = "0" + mobile.Substring(2);
            else if (!mobile.StartsWith("0"))
                mobile = "0" + mobile;

            return mobile;
        }



    }
}
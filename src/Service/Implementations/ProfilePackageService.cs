﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Microsoft.Extensions.Options;

namespace Cerberus.Service.Implementations
{
    public class ProfilePackageService : IProfilePackageService
    {
        private readonly IProfilePackageRepository _profilePackageRepository;
        private readonly ProfilePackageConfiguration _profilePackageConfiguration;
        private readonly IProfilePackageReadRepository _profilePackageReadRepository;
        public ProfilePackageService(IProfilePackageRepository profilePackageRepository, IOptions<ProfilePackageConfiguration> profilePackageConfiguration, IProfilePackageReadRepository profilePackageReadRepository)
        {
            _profilePackageRepository = profilePackageRepository;
            _profilePackageReadRepository = profilePackageReadRepository;
            _profilePackageConfiguration = profilePackageConfiguration.Value;
        }

        public Task CreateAsync(ProfilePackage profilePackage)
        {
            profilePackage.ExpirationDate = DateTime.Now.AddDays(_profilePackageConfiguration.AddDaysToExpiration);
            return _profilePackageRepository.CreateAsync(profilePackage);
        }

        public Task UpdateAsync(ProfilePackage profilePackage)
        {
            return _profilePackageRepository.UpdateAsync(profilePackage);
        }

        public Task<ProfilePackage> GetAsync(long id)
        {
            return _profilePackageReadRepository.GetAsync(id);
        }

        public Task<ProfilePackage> GetUnusedPackagesAsync(long profileId, PackageType type,PackageUsedFlag usePackage)
        {
            return _profilePackageReadRepository.GetUnusedPackageAsync(profileId, type,usePackage);
        }


        public Task<ProfilePackage> GetSuspiciousPackageAsync(long profileId, PackageType type)
        {
            return _profilePackageReadRepository.GetSuspiciousPackageAsync(profileId, type);
        }

        //public async Task<bool> CheckExistProfilePackage(long profileId, PackageType type)
        //{
        //    var list =await _profilePackageRepository.AllPackagesByProfileId(profileId, type);
        //    return list.ToList().Any(x=>x.ExpirationDate>=DateTime.Now);
        //}


    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;

namespace Cerberus.Service.Implementations
{
    public class AgentDocumentService : IAgentDocumentService
    {

        private readonly IAgentDocumentRepository _agentDocumentRepository;
        private readonly IAgentDocumentReadRepository _agentDocumentReadRepository;
   
        public AgentDocumentService(IAgentDocumentRepository agentDocumentRepository,
            IAgentDocumentReadRepository agentDocumentReadRepository)
        {
            _agentDocumentRepository = agentDocumentRepository;
            _agentDocumentReadRepository = agentDocumentReadRepository;
        }

        public async Task CreateAsync(long profileId, File file)
        {
            //await _fileService.CreateAsync(file, null, string.Empty);
            var agentDocumentModel = new AgentDocument
            {
                FileId = file.Id,
                AgentId=profileId,
                CreationDate=DateTime.Now,
                IsConfirmed=null
            };

            await _agentDocumentRepository.CreateAsync(agentDocumentModel);
        }

        public async Task DeleteAsync(AgentDocument entity)
        {
            await  _agentDocumentRepository.DeleteAsync(entity);
        }

        public async Task<List<AgentDocument>> GetListDocumentByAgentId(long id)
        {
            return await _agentDocumentReadRepository.GetListDocumentByAgentId(id);
        }

        public Task DeleteAsync(long id)
        {
          return  _agentDocumentRepository.DeleteAsync(id);
        }
    }
}
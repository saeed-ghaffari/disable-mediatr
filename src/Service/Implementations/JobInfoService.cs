﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;

namespace Cerberus.Service.Implementations
{
    public class JobInfoService : IJobInfoService
    {
        private readonly IJobInfoRepository _jobInfoRepository;
        private readonly IJobInfoReadRepository _jobInfoReadRepository;

        public JobInfoService(IJobInfoRepository jobInfoRepository, IJobInfoReadRepository jobInfoReadRepository)
        {
            _jobInfoRepository = jobInfoRepository;
            _jobInfoReadRepository = jobInfoReadRepository;
        }

        public Task CreateAsync(JobInfo jobInfo)
        {
            return _jobInfoRepository.CreateAsync(jobInfo);
        }

        public Task UpdateAsync(JobInfo jobInfo)
        {
            return _jobInfoRepository.UpdateAsync(jobInfo);
        }

        public Task<JobInfo> GetAsync(long id)
        {
            return _jobInfoReadRepository.GetAsync(id);
        }

        public Task<JobInfo> GetByProfileIdAsync(long profileId, bool getAllRelated = false)
        {
            return _jobInfoReadRepository.GetByProfileIdAsync(profileId, getAllRelated);
        }

        public Task<JobInfo> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return _jobInfoReadRepository.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }
    }
}
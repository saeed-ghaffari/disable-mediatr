﻿using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel;
using Cerberus.Utility;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cerberus.Service.Implementations.Inquiries
{
    public class IndividualInquiryService : IIndividualInquiryService
    {
        private readonly IIndividualInquiryRepository _iIndividualInquiryRepository;
        private readonly InquiryConfiguration _config;
        public IndividualInquiryService(IIndividualInquiryRepository iIndividualInquiryRepository, IOptions<InquiryConfiguration> config)
        {
            _config = config.Value;
            _iIndividualInquiryRepository = iIndividualInquiryRepository;
        }
        public async Task<IndividualInquiry> GetAsync(string privateCode)
        {
            
                string baseApiUrl = _config.BaseApiUrl;
                string inquiryUrl = _config.IndividualInquiryUrl;
                string url = baseApiUrl + inquiryUrl;
                var existItem = await _iIndividualInquiryRepository.GetConditionalListAsync(x => x.SuccessInquiry && (DateTime.Now - x.CreationDate).TotalHours < _config.IndividualInquiryTtl && x.PrivateCode==privateCode);
                var individualInquiries = existItem.ToList();
                if (!individualInquiries.Any())
                {
                    var resultString = await HttpHepler.PostAsync(url, Newtonsoft.Json.JsonConvert.SerializeObject(new { PrivateCode = privateCode }), "application/json");

                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<IndividualInquiry>(resultString.Body);
                    result.PrivateCode = privateCode;

                    result.SuccessInquiry = result.Successful;
                    await _iIndividualInquiryRepository.CreateAsync(result);
                    return result;
                }
                var inquiry= individualInquiries.FirstOrDefault();
                if (inquiry != null)
                {
                    inquiry.Successful = inquiry.SuccessInquiry;
                    await _iIndividualInquiryRepository.UpdateAsync(inquiry);
                    return inquiry;
                }

            return null;
        }
    }
}

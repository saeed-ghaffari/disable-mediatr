﻿using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel;
using Cerberus.Utility;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cerberus.Service.Implementations.Inquiries
{
    public class InstitutionInquiryService : IInstitutionInquiryService
    {
        private readonly IInstitutionInquiryRepository _institutionInquiryRepository;
        private readonly InquiryConfiguration _config;
        public InstitutionInquiryService(IInstitutionInquiryRepository institutionInquiryRepository, IOptions<InquiryConfiguration> config)
        {
            _config = config.Value;
            _institutionInquiryRepository = institutionInquiryRepository;
        }
        public async Task<Domain.Model.InstitutionInquiry> GetAsync(string privateCode)
        {

            string baseApiUrl = _config.BaseApiUrl;
            string inquiryUrl = _config.InstitutionInquiryUrl;
            string url = baseApiUrl + inquiryUrl;
            var existItem = await _institutionInquiryRepository.GetConditionalListAsync(x => x.SuccessInquiry == true && (DateTime.Now - x.CreationDate).TotalHours < _config.InstitutionInquiryTtl && x.PrivateCode == privateCode);
            var institutionInquiries = existItem.ToList();
            if (!institutionInquiries.Any())
            {
                var resultString = await HttpHepler.PostAsync(url, Newtonsoft.Json.JsonConvert.SerializeObject(new { PrivateCode = privateCode }), "application/json");

                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<InstitutionInquiry>(resultString.Body);
                result.PrivateCode = privateCode;
                result.SuccessInquiry = result.Successful;
                await _institutionInquiryRepository.CreateAsync(result);
                return result;
            }
            var inquiry = institutionInquiries.FirstOrDefault();
            if (inquiry != null)
            {
                inquiry.Successful = inquiry.SuccessInquiry;
                await _institutionInquiryRepository.UpdateAsync(inquiry);
                return inquiry;
            }

            return null;
        }
    }
}

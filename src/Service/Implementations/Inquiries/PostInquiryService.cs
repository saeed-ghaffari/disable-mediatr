﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Service.Model.Inquiries;
using Cerberus.Utility;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Cerberus.Service.Implementations.Inquiries
{
    public class PostInquiryService : IPostInquiryService
    {
        private readonly IPostInquiryRepository _postInquiryRepository;
        private readonly IPostInquiryReadRepository _postInquiryReadRepository;
        private readonly InquiryConfiguration _config;
        private readonly ILogger<PostInquiryService> _logger;
        public PostInquiryService(IPostInquiryRepository postInquiryRepository, IOptions<InquiryConfiguration> config, ILogger<PostInquiryService> logger, IPostInquiryReadRepository postInquiryReadRepository)
        {
            _postInquiryRepository = postInquiryRepository;
            _logger = logger;
            _postInquiryReadRepository = postInquiryReadRepository;
            _config = config.Value;
        }

        public async Task<PostInquiry> GetAsync(string ssn, string birthDate)
        {
            var resultBody = string.Empty;//just for log
            try
            {
                string baseApiUrl = _config.BaseApiUrl;
                string inquiryUrl = _config.PostInquiryUrl;
                string url = baseApiUrl + inquiryUrl;
                var existItem = await _postInquiryReadRepository.GetConditionalListAsync(x => x.Ssn == ssn && x.BirthDate == birthDate && x.SuccessInquiry && x.CreationDate.AddDays(_config.PostInquiryTtl)>DateTime.Now);
                var postInquiries = existItem.ToList();

                if (postInquiries.Any())
                {
                    var inquiry = postInquiries.OrderByDescending(x => x.Id).First();

                    inquiry.Successful = inquiry.SuccessInquiry;
                    await _postInquiryRepository.UpdateAsync(inquiry);
                    return inquiry;

                }

                var request = new PostInquiryRequest
                {
                    Ssn = ssn,
                    BirthDate = birthDate,
                    EndUsername = "faranam",//todo: ask kakuli to get correct username
                    EndIp = "127.0.0.1"
                };
                var resultString = await HttpHepler.PostAsync(url, JsonConvert.SerializeObject(request), "application/json",TimeSpan.FromSeconds(5));
                resultBody = resultString.Body;

                var result = JsonConvert.DeserializeObject<PostInquiryResponse>(resultString.Body);
                var postInquiry = new PostInquiry
                {
                    Ssn = request.Ssn,
                    BirthDate = request.BirthDate,
                    EndUsername = request.EndUsername,
                    EndIp = request.EndIp,
                    Message = result.Message != null ? string.Join(",", result.Message) : null,
                    HasException = result.HasException,
                    SuccessInquiry = result.Successful,
                    Successful = result.Successful,
                    PostalCode = result.PostalCode,
                    Address = result.Address
                };
                await _postInquiryRepository.CreateAsync(postInquiry);

                return postInquiry;
            }
            catch (Exception e)
            {
                _logger.LogCritical($"{nameof(PostInquiryService)} Error:{resultBody} --- {e.Message}---{e.InnerException?.Message}");
                return null;
            }

        }
    }
}
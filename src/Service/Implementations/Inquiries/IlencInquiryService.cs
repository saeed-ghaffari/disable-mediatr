﻿using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Repository.ReadRepositories;
using static Cerberus.Utility.HttpHepler;

namespace Cerberus.Service.Implementations.Inquiries
{
    public class IlencInquiryService : IIlencInquiryService
    {
        private readonly IIlencInquiryRepository _iIlencInquiryRepository;
        private readonly IIlencInquiryReadRepository _ilencInquiryReadRepository;
        private readonly InquiryConfiguration _config;
        public IlencInquiryService(IIlencInquiryRepository iIlencInquiryRepository, IOptions<InquiryConfiguration> config, IIlencInquiryReadRepository ilencInquiryReadRepository)
        {
            _config = config.Value;
            _iIlencInquiryRepository = iIlencInquiryRepository;
            _ilencInquiryReadRepository = ilencInquiryReadRepository;
        }
        public async Task<IlencInquiry> GetAsync(string nationalCode)
        {
           
                string baseApiUrl = _config.BaseApiUrl;
                string inquiryUrl = _config.IlencInquiryUrl;
                string url = baseApiUrl + inquiryUrl;
                var existItem = await _ilencInquiryReadRepository.GetConditionalListAsync(x => x.SuccessInquiry && x.NationalCode==nationalCode && (DateTime.Now - x.CreationDate).TotalHours < _config.IlencInquiryTtl);
                var ilencInquiries = existItem.ToList();
                if (!ilencInquiries.Any())
                {
                    var resultString = await PostAsync(url, Newtonsoft.Json.JsonConvert.SerializeObject(new { NationalCode = nationalCode }), "application/json");

                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<IlencInquiry>(resultString.Body);
                    result.NationalCode = nationalCode;
                    result.SuccessInquiry = result.Successful;
                    await _iIlencInquiryRepository.CreateAsync(result);
                    return result;
                }
                var inquiry= ilencInquiries.FirstOrDefault();
                if (inquiry != null)
                {
                    inquiry.Successful = inquiry.SuccessInquiry;
                    await _iIlencInquiryRepository.UpdateAsync(inquiry);
                    return inquiry;
                }

                return null;
           
        }
        public Task UpdateAsync(IlencInquiry ilencInquiry)
        {
            return _iIlencInquiryRepository.UpdateAsync(ilencInquiry);
        }
    }
}

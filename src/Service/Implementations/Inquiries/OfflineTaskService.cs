﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.TasksManager.Core.Enum;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.Service.Implementations.Inquiries
{
    public class OfflineTaskService : IOfflineTaskService
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IRecurringTaskRepository _recurringTaskRepository;

        public OfflineTaskService(
            ITaskRepository taskRepository,
            IRecurringTaskRepository recurringTaskRepository)
        {
            _taskRepository = taskRepository;
            _recurringTaskRepository = recurringTaskRepository;
        }

        public async Task CreateTaskAsync(KeyValuePair<long, string> taskGroup, long profileId, object taskParams)
        {

            var param = JsonConvert.SerializeObject(taskParams);
            await _taskRepository.AddAsync(new ScheduledTask()
            {
                RefId = profileId,
                DueTime = DateTime.Now,
                GroupId = taskGroup.Key,
                Type = taskGroup.Value,
                Params = param,
                Status = TaskStatus.Pending,
                RecoverySequence = 0
            });
        }
        public async Task CreateRecuringTaskAsync(KeyValuePair<long, string> taskGroup, long profileId, object taskParams)
        {
            //todo : set interval from configuration,inject configuration using IOptions

            var param = JsonConvert.SerializeObject(taskParams);
            await _recurringTaskRepository.AddAsync(new RecurringTask()
            {
                RefId = profileId,
                NextExecution = DateTime.Now,
                GroupId = taskGroup.Key,
                Type = taskGroup.Value,
                Params = param,
                Status = RecurringTaskStatus.Idle,
                IntervalPeriod = RecurringIntervalPeriod.Minute,
                Interval = 15// _config.CheckRecuringTaskInterval
            });
        }

        public async Task ScheduleOfflineTasksAsync(long profileId ,long? mobile,DateTime dueTime)
        {
            var param = new CreateInquiryParams
            {
                Mobile = mobile,
                ProfileId = profileId
            };

            await _taskRepository.AddAsync(new ScheduledTask
            {
                Type = CerberusTasksIdentifierHelper.CreateInquiriesTasks.Value,
                DueTime =dueTime,
                GroupId = CerberusTasksIdentifierHelper.CreateInquiriesTasks.Key,
                Params = JsonConvert.SerializeObject(param),
                RefId = profileId,
                Status = TaskStatus.Pending
            });


        }
        public async Task ScheduleEditAfterSejamiTasksAsync(long profileId)
        {
            await _taskRepository.AddAsync(new ScheduledTask
            {
                Type = CerberusTasksIdentifierHelper.CreateEditAfterSejamiTasks.Value,
                DueTime = DateTime.Now,
                GroupId = CerberusTasksIdentifierHelper.CreateEditAfterSejamiTasks.Key,
                Params = profileId.ToString(),
                RefId = profileId,
                Status = TaskStatus.Pending
            });


        }

        public async Task ScheduleSetFactorAsync(long paymentId, DateTime dueTime, bool setFactorForOtherClients = false)
        {
            var param = JsonConvert.SerializeObject(setFactorForOtherClients);
            await _taskRepository.AddAsync(new ScheduledTask
            {
                Type = CerberusTasksIdentifierHelper.SetFactor.Value,
                DueTime = dueTime,
                GroupId = CerberusTasksIdentifierHelper.SetFactor.Key,
                Params =param,
                RefId = paymentId,
                Status = TaskStatus.Pending
            });
        }

        public async Task ScheduleSetFakeFactorAsync(long paymentId, DateTime dueTime)
        {
            await _taskRepository.AddAsync(new ScheduledTask
            {
                Type = CerberusTasksIdentifierHelper.SetFakeFactor.Value,
                DueTime = dueTime,
                GroupId = CerberusTasksIdentifierHelper.SetFakeFactor.Key,
                Params =JsonConvert.SerializeObject(paymentId) ,
                RefId = paymentId,
                Status = TaskStatus.Pending
            });
        }

        public async Task ScheduleOfflineTasksShahkarAsync(long profileId, long mobile, long newMobile, string uniqueIdentifier, DateTime dueTime, ProfileStatus profileStatus)
        {
            var param = new ShahkarParams()
            {
                Mobile = mobile,
                UniqueIdentifier = uniqueIdentifier,
                NewMobile = newMobile,
                Status = profileStatus
            };

            await _taskRepository.AddAsync(new ScheduledTask
            {
                Type = CerberusTasksIdentifierHelper.ShahkarInquiry.Value,
                DueTime = dueTime,
                GroupId = CerberusTasksIdentifierHelper.ShahkarInquiry.Key,
                Params = JsonConvert.SerializeObject(param),
                RefId = profileId,
                Status = TaskStatus.Pending
            });
        }
    }
}
﻿using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using System;
using System.Threading.Tasks;
using Cerberus.Utility;
using Microsoft.Extensions.Options;
using System.Linq;
using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Model.Inquiries;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Cerberus.Service.Implementations.Inquiries
{
    public class IbanInquiryService : IIbanInquiryService
    {
        private readonly IIbanInquiryRepository _ibanInquiryRepository;
        private readonly IIbanInquiryReadRepository _ibanInquiryReadRepository;
        private readonly InquiryConfiguration _config;
        private ILogger<IbanInquiryService> _logger;
        public IbanInquiryService(IIbanInquiryRepository ibanInquiryRepository, IOptions<InquiryConfiguration> config, ILogger<IbanInquiryService> logger, IIbanInquiryReadRepository ibanInquiryReadRepository)
        {
            _config = config.Value;
            _ibanInquiryRepository = ibanInquiryRepository;
            _logger = logger;
            _ibanInquiryReadRepository = ibanInquiryReadRepository;
        }
        public async Task<IbanInquiry> GetAsync(string iban)
        {
            var resultBody = string.Empty;//just for log
            try
            {
                string baseApiUrl = _config.BaseApiUrl;
                string inquiryUrl = _config.IbanInquiryUrl;
                string url = baseApiUrl + inquiryUrl;


                var existItem = await _ibanInquiryReadRepository
                    .GetConditionalListAsync(x =>
                        x.SuccessInquiry && x.Iban == iban &&
                        x.CreationDate.AddHours(_config.IbanInquiryTtl) > DateTime.Now);


                var ibanInquiries = existItem.ToList();
                if (!ibanInquiries.Any())
                {
                    var resultString = await HttpHepler.PostAsync(url, JsonConvert.SerializeObject(new { Iban = iban }), "application/json");
                    resultBody = resultString.Body;
                    var result = JsonConvert.DeserializeObject<IbanInquiry>(resultString.Body);
                    result.Iban = iban;
                    if (result.FirstName == null && result.LastName == null && result.Successful && result.Message == null)
                    {
                        result.HasException = true;
                        result.Successful = false;
                    }
                    result.SuccessInquiry = result.Successful;
                    await _ibanInquiryRepository.CreateAsync(result);
                    return result;
                }
                var inquiry = ibanInquiries.FirstOrDefault();
                if (inquiry != null)
                {
                    inquiry.Successful = inquiry.SuccessInquiry;
                    await _ibanInquiryRepository.UpdateAsync(inquiry);
                    return inquiry;
                }

                return null;
            }
            catch (Exception e)
            {
                _logger.LogCritical($"{nameof(IbanInquiryService)} Error: {resultBody} --- {e.Message}---{e.InnerException?.Message}");
                throw;
            }
        }

        public Task UpdateAsync(IbanInquiry ibanInquiry)
        {
            return _ibanInquiryRepository.UpdateAsync(ibanInquiry);
        }
    }
}

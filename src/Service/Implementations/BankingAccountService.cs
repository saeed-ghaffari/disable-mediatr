﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;

namespace Cerberus.Service.Implementations
{
    public class BankingAccountService : IBankingAccountService
    {
        private readonly IBankingAccountRepository _bankingAccountRepository;
        private readonly IBankingAccountReadRepository _bankingAccountReadRepository;

        public BankingAccountService(IBankingAccountRepository bankingAccountRepository, IBankingAccountReadRepository bankingAccountReadRepository)
        {
            _bankingAccountRepository = bankingAccountRepository;
            _bankingAccountReadRepository = bankingAccountReadRepository;
        }

        public Task CreateAsync(BankingAccount bankingAccount)
        {
            return _bankingAccountRepository.CreateAsync(bankingAccount);
        }

        public Task UpdateAsync(BankingAccount bankingAccount)
        {
            return _bankingAccountRepository.UpdateAsync(bankingAccount);
        }

        public  Task DeleteAsync(BankingAccount bankingAccount)
        {
            return  _bankingAccountRepository.DeleteAsync(bankingAccount);
        }

        public async Task<BankingAccount> GetAsync(long id)
        {
            return await _bankingAccountRepository.GetAsync(id);
        }

        public async Task<IEnumerable<BankingAccount>> GetListAsync(long profileId, bool includeRelatedData = false)
        {
            return await _bankingAccountRepository.GetListAsync(profileId, includeRelatedData);
        }

      
        public async Task<IEnumerable<BankingAccount>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return await _bankingAccountReadRepository.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;

namespace Cerberus.Service.Implementations
{
    public class ProfileHistoryService : IProfileHistoryService
    {
        private readonly IProfileHistoryRepository _repository;
        private readonly IProfileHistoryReadRepository _profileHistoryReadRepository;
        public ProfileHistoryService(IProfileHistoryRepository repository, IProfileHistoryReadRepository profileHistoryReadRepository)
        {
            _repository = repository;
            _profileHistoryReadRepository = profileHistoryReadRepository;
        }


        public Task<ProfileHistory> GetAsync(long id)
        {
            return _profileHistoryReadRepository.GetAsync(id);
        }

        public Task<ProfileHistory> GetByUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return _profileHistoryReadRepository.GetByUniqueIdentifierAsync(uniqueIdentifier);
        }

        public Task CreateAsync(ProfileHistory history, ProfileStatus changedTo)
        {
            history.ChangedTo = changedTo;
            return _repository.CreateAsync(history);
        }

        public Task UpdateAsync(ProfileHistory history)
        {
            return _repository.UpdateAsync(history);
        }

        public Task<IEnumerable<ProfileHistory>> GetByReferenceIdAsync(string referenceId, DateTime from, DateTime to)
        {
            return _profileHistoryReadRepository.GetByReferenceIdAsync(referenceId, from, to);
        }

        public Task<ProfileHistory> GetTheLastHistoryAsync(long profileId)
        {
            return _profileHistoryReadRepository.GetTheLastHistoryAsync(profileId);
        }
        public Task CreateAsync(ProfileHistory history)
        {
            return _repository.CreateAsync(history);
        }
    }
}
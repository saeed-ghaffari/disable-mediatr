﻿using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Microsoft.Extensions.Options;

namespace Cerberus.Service.Implementations
{
    public class BankService : IBankService
    {
        private readonly IBankReadRepository _bankReadRepository;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;
        public BankService(IMasterCacheProvider masterCacheProvider, IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration, IBankReadRepository bankReadRepository)
        {
        
            _masterCacheProvider = masterCacheProvider;
            _bankReadRepository = bankReadRepository;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }

        public async Task<List<Bank>> GetListAsync()
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(BankService)}:GetAllList",
                async () => await _bankReadRepository.GetListAsync(),
                _masterCacheProviderConfiguration.ExpirationTime);

        }

        public async Task<List<Bank>> GetListForAuthenticationOfficeAsync()
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(BankService)}:authenticationOfficeKey",
                async () => await _bankReadRepository.GetListForAuthenticationOfficeAsync(),
                _masterCacheProviderConfiguration.ExpirationTime);
        }
    }
}
﻿using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Microsoft.Extensions.Options;

namespace Cerberus.Service.Implementations
{
    public class JobService : IJobService
    {
        private readonly IJobReadRepository _jobReadRepository;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;
        public JobService(IMasterCacheProvider masterCacheProvider,IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration, IJobReadRepository jobReadRepository)
        {
           
            _masterCacheProvider = masterCacheProvider;
            _jobReadRepository = jobReadRepository;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }

        public async Task<List<Job>> GetListAsync()
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(JobService)}:getAllList",
                async () => await _jobReadRepository.GetListAsync(),
                _masterCacheProviderConfiguration.ExpirationTime);
           
        }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Microsoft.Extensions.Options;

namespace Cerberus.Service.Implementations
{
    public class BrokerService : IBrokerService
    {
        private readonly IBrokerReadRepository _brokerReadRepository;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;

        public BrokerService(IBrokerReadRepository brokerReadRepository, IMasterCacheProvider masterCacheProvider,IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration)
        {
            _brokerReadRepository = brokerReadRepository;
            _masterCacheProvider = masterCacheProvider;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }

        public async Task<IEnumerable<Broker>> GetListAsync()
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(BrokerService)}:GetAllList",
                async () => await _brokerReadRepository.GetListAsync(),
                _masterCacheProviderConfiguration.ExpirationTime);
            
        }

        public async Task<Broker> GetAsync(long id)
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(BrokerService)}:GetById:{id}:key",
                async () => await _brokerReadRepository.GetAsync(id),
                _masterCacheProviderConfiguration.ExpirationTime);
          
        }
    }
}
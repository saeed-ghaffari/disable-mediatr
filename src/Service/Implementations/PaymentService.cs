﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;

namespace Cerberus.Service.Implementations
{
    public class PaymentService : IPaymentService
    {
        private readonly IPaymentRepository _paymentRepository;
        private readonly IPaymentReadRepository _paymentReadRepository;
        public PaymentService(IPaymentRepository paymentRepository, IPaymentReadRepository paymentReadRepository)
        {
            _paymentRepository = paymentRepository;
            _paymentReadRepository = paymentReadRepository;
        }

        public async Task<Payment> CreatePendingAsync(long profileId, PaymentGateway gateway, long amount, long issuer, long discount, long? profilePackageId)
        {
            var payment = new Payment()
            {
                Amount = amount,
                Gateway = gateway,
                ProfileId = profileId,
                Status = PaymentStatus.Pending,
                Discount = discount,
                DiscountIssuer = issuer,
            };
            if (profilePackageId != null && profilePackageId > 0) payment.ProfilePackageId = profilePackageId;
            await _paymentRepository.CreateAsync(payment);
            return payment;
        }


        public async Task UpdateAsync(Payment updatePayment)
        {
            await _paymentRepository.UpdateAsync(updatePayment);
        }

        public Task<Payment> GetAsync(long id)
        {
            return _paymentReadRepository.GetAsync(id);
        }
            

        public Task<int> GetCountByDiscountIssuerAsync(long serviceId, DateTime? from, DateTime? to)
        {
            return _paymentReadRepository.GetCountByDiscountIssuerAsync(serviceId, from, to);
        }


        public Task<IEnumerable<Payment>> GetListAsync(Expression<Func<Payment, bool>> predicate)
        {
            return _paymentReadRepository.GetListAsync(predicate);
        }

        public async Task DeleteAsync(Payment entity)
        {
            await _paymentRepository.DeleteAsync(entity);
        }

        public Task UpdateIsConfirmedReferenceAsync(long paymentId)
        {
            return _paymentRepository.UpdateIsConfirmedReferenceAsync(paymentId);
        }

        public Task CreateAsync(Payment payment)
        {
            return _paymentRepository.CreateAsync(payment);
        }

        public async Task<List<Payment>> GetSuspiciousPayments()
        {
            return await _paymentReadRepository.GetSuspiciousPayments();
        }
        public async Task UpdateSettledPaymentsAsync(long Id, string SaleReferenceId, string ReferenceNumber)
        {
            await _paymentRepository.UpdateSettledPaymentsAsync(Id, SaleReferenceId, ReferenceNumber);
        }
        public async Task UpdateSettleFailedPaymentsAsync(long Id)
        {
            await _paymentRepository.UpdateSettleFailedPaymentsAsync(Id);
        }

        public async Task SetFactorIdRepository(long paymentId, long factorId, string serialNumber)
        {
            await _paymentRepository.SetFactorIdRepository(paymentId, factorId, serialNumber);
        }
        public async Task<Payment> GetByIdAsync(long id)
        {
            return await _paymentReadRepository.GetByIdAsync(id);
        }

    }
}

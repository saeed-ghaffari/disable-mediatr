﻿using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;

namespace Cerberus.Service.Implementations
{
    public class LegalPersonService : ILegalPersonService
    {
        private readonly ILegalPersonRepository _legalPersonRepository;
        private readonly ILegalPersonReadRepository _legalPersonReadRepository;
        public LegalPersonService(ILegalPersonRepository legalPersonRepository, IMapper mapper, ILegalPersonReadRepository legalPersonReadRepository)
        {
            _legalPersonRepository = legalPersonRepository;
            _legalPersonReadRepository = legalPersonReadRepository;
        }

        public Task CreateAsync(LegalPerson legalPerson)
        {
            return _legalPersonRepository.CreateAsync(legalPerson);
        }

        public Task<LegalPerson> GetAsync(long id)
        {
            return _legalPersonReadRepository.GetAsync(id);
        }

        public Task UpdateAsync(LegalPerson legalPerson)
        {
            return _legalPersonRepository.UpdateAsync(legalPerson);
        }

        public Task<LegalPerson> GetByProfileIdAsync(long profileId)
        {
            return _legalPersonReadRepository.GetByProfileIdAsync(profileId);
        }

        public Task<LegalPerson> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return _legalPersonReadRepository.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }
    }
}
﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;

namespace Cerberus.Service.Implementations
{
    public class LegalPersonShareholderService : ILegalPersonShareholderService
    {
        private readonly ILegalPersonShareholderRepository _legalPersonShareholderRepository;
        private readonly ILegalPersonShareholderReadRepository _legalPersonShareholderReadRepository;

        public LegalPersonShareholderService(ILegalPersonShareholderRepository legalPersonShareholderRepository, ILegalPersonShareholderReadRepository legalPersonShareholderReadRepository)
        {
            _legalPersonShareholderRepository = legalPersonShareholderRepository;
            _legalPersonShareholderReadRepository = legalPersonShareholderReadRepository;
        }

        public async Task<List<LegalPersonShareholder>> GetListAsync(long profileId)
        {
            return await _legalPersonShareholderReadRepository.GetListAsync(profileId);
        }

        public async Task<LegalPersonShareholder> GetAsync(long id)
        {
            return await _legalPersonShareholderReadRepository.GetAsync(id);
        }

        public async Task CreateAsync(LegalPersonShareholder model)
        {
            await _legalPersonShareholderRepository.CreateAsync(model);
        }

        public async Task DeleteAsync(LegalPersonShareholder model)
        {
           await _legalPersonShareholderRepository.DeleteAsync(model);
        }

       
    }
}
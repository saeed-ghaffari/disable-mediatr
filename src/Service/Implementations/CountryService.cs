﻿using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Microsoft.Extensions.Options;

namespace Cerberus.Service.Implementations
{
    public class CountryService : ICountryService
    {
        private readonly ICountryReadRepository _countryReadRepository;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;

        public CountryService(ICountryReadRepository countryRepository, IMasterCacheProvider masterCacheProvider, IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration)
        {
            _countryReadRepository = countryRepository;
            _masterCacheProvider = masterCacheProvider;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }

        public async Task<List<Country>> GetListAsync()
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(CountryService)}:GetAllList",
                async () => await _countryReadRepository.GetListAsync(),
                _masterCacheProviderConfiguration.ExpirationTime);
            
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;

namespace Cerberus.Service.Implementations
{
    public class MessageService : IMessageService
    {
        private readonly IMessagingService _messagingService;
        private readonly IMessageRepository _messageRepository;
        private readonly IProfileService _profileService;

        public MessageService(IMessagingService messagingService, IMessageRepository messageRepository, IProfileService profileService)
        {
            _messagingService = messagingService;
            _messageRepository = messageRepository;
            _profileService = profileService;
        }

        public async Task<Message> CreateAsync(Message message, bool sendSms = true, Profile profile = null, DateTime? dueTime =null)
        {
            message.SendBySms = sendSms;

            await _messageRepository.CreateAsync(message);

            if (sendSms)
            {
                if (profile == null)
                    profile = await _profileService.GetAsync(message.ProfileId);

                await _messagingService.SendSmsAsync(profile.Id, profile.Mobile.ToString(), profile.Carrier, message.Body,dueTime);
            }

            return message;
        }

        public Task<Message> GetListAsync(long profileId)
        {
            throw new System.NotImplementedException();
        }
    }
}
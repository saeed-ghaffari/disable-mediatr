﻿using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Microsoft.Extensions.Options;

namespace Cerberus.Service.Implementations
{
    public class ThirdPartyServiceService : IThirdPartyServiceService
    {
        private readonly IThirdPartyServiceReadRepository _repository;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;
        public ThirdPartyServiceService(IThirdPartyServiceReadRepository repository, IMasterCacheProvider masterCacheProvider,
           IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration)
        {
            _repository = repository;
            _masterCacheProvider = masterCacheProvider;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }

        public async Task<ThirdPartyService> GetAsync(long id)
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(ThirdPartyServiceService)}:GetById:{id}",
                async () => await _repository.GetAsync(id),
                _masterCacheProviderConfiguration.UserExpirationTime);


        }

        public async Task<ThirdPartyService> GetAsync(string title)
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(ThirdPartyServiceService)}:GetByTitle:{title}",
                async () => await _repository.GetAsync(title),
                _masterCacheProviderConfiguration.UserExpirationTime);

        }

        public async Task<ThirdPartyService> GetByLinkAsync(string link)
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(ThirdPartyServiceService)}:GetByLink:{link}",
                async () => await _repository.GetByLinkAsync(link),
                _masterCacheProviderConfiguration.UserExpirationTime);

        }

        public async Task<string> GetThirdPartyName(long? id)
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(ThirdPartyServiceService)}:GetThirdPartyName:{id}",
                async () => await _repository.GetThirdPartyName(id),
                _masterCacheProviderConfiguration.UserExpirationTime);

        }
    }
}
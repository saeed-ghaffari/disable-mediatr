﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories.Membership;
using Cerberus.Domain.Interface.Repositories.Membership;
using Cerberus.Domain.Interface.Services.Membership;
using Cerberus.Domain.Model.Membership;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cerberus.Service.Implementations.Membership
{
    public class MembershipService : BaseService, IMembershipService
    {
        private readonly IMembershipRepository _membershipRepository;
        private readonly IMembershipReadRepository _membershipReadRepository;
        private readonly ICacheProvider _cacheProvider;
        private readonly IOptions<ObjectCacheConfiguration> _objectCacheConfig;
        public MembershipService(
             ICacheProvider cacheProvider, IMembershipRepository membership,
             IOptions<ObjectCacheConfiguration> objectCacheConfiguration, ILogger<MembershipService> logger, IMembershipReadRepository membershipReadRepository)
             : base(
                 cacheProvider,
                 logger,
                 reconstructTime: objectCacheConfiguration.Value.ReconstructTime,
                 cacheExpiration: objectCacheConfiguration.Value.MembershipExpiration,
                 ttl: objectCacheConfiguration.Value.Ttl)
        {
            _membershipRepository = membership;
            _cacheProvider = cacheProvider;
            _objectCacheConfig = objectCacheConfiguration;
            _membershipReadRepository = membershipReadRepository;
        }

        public async Task CreateAsync(PrivatePersonMembership model)
        {
            await _membershipRepository.CreateAsync(model);
        }

        public async Task<PrivatePersonMembership> GetPrivatePersonAsync(string uniqueIdentifier, string mobile, long serviceId)
        {
            var result = await FetchAsync(CacheKeyHelper.Membership.PrivatePerson(uniqueIdentifier, serviceId),
                () => _membershipReadRepository.GetPrivatePersonAsync(uniqueIdentifier, serviceId));

            return result?.Mobile == mobile ? result : null;
        }

        public async Task<PrivatePersonMembership> GetPrivatePersonAsync(string uniqueIdentifier, long serviceId)
        {
            return await FetchAsync(CacheKeyHelper.Membership.PrivatePerson(uniqueIdentifier, serviceId),
                () => _membershipReadRepository.GetPrivatePersonAsync(uniqueIdentifier, serviceId));
        }



        public async Task UpdateCacheAsync<T>(long serviceId, string uniqueIdentifier, T data, MembershipDataType type)
        {
            string key = GenerateKey(serviceId, uniqueIdentifier, type);

            //update redis
            await _cacheProvider.StoreAsync(
                key: key,
                value: data,
                expiration: _objectCacheConfig.Value.MembershipExpiration,
                ttl: _objectCacheConfig.Value.Ttl);
        }

        private string GenerateKey(long serviceId, string uniqueIdentifier, MembershipDataType type)
        {
            string key = string.Empty;
            switch (type)
            {
                case MembershipDataType.PrivatePerson:
                    key = CacheKeyHelper.Membership.PrivatePerson(uniqueIdentifier, serviceId);
                    break;
                case MembershipDataType.Address:
                    key = CacheKeyHelper.Membership.Address(uniqueIdentifier, serviceId);
                    break;
                case MembershipDataType.BankingAccount:
                    key = CacheKeyHelper.Membership.Accounts(uniqueIdentifier, serviceId);
                    break;
                default:
                    throw new Exception("unknown membership data type");
            }

            return key;
        }

        public async Task RemoveAsync(long serviceId, string uniqueIdentifier, MembershipDataType type)
        {
            string key = GenerateKey(serviceId, uniqueIdentifier, type);

            await _cacheProvider.RemoveAsync(key);
        }

        public Task<IEnumerable<PrivatePersonMembership>> GetPrivatePeronListAsync(long fromRow, long toRow, string uniqueIdentifier)
        {
            return _membershipReadRepository.GetPrivatePeronListAsync(fromRow, toRow, uniqueIdentifier);
        }

        public Task<IEnumerable<AddressMembership>> GetAddressListAsync(long fromRow, long toRow, string uniqueIdentifier)
        {
            return _membershipReadRepository.GetAddressListListAsync(fromRow, toRow, uniqueIdentifier);
        }

        public Task<IEnumerable<BankingAccountMembership>> GetBankingAccountListAsync(long fromRow, long toRow, string uniqueIdentifier)
        {
            return _membershipReadRepository.GetBankingAccountListAsync(fromRow, toRow, uniqueIdentifier);
        }

        public Task<int> CountAsync(MembershipDataType type)
        {
            return _membershipReadRepository.CountAsync(type);
        }

        public async Task<AddressMembership> GetAddressAsync(string uniqueIdentifier, long serviceId)
        {
            return await FetchAsync(CacheKeyHelper.Membership.Address(uniqueIdentifier, serviceId),
              () => _membershipReadRepository.GetAddressAsync(uniqueIdentifier, serviceId));
        }

        public async Task<BankingAccountMembership> GetAccountAsync(string uniqueIdentifier, long serviceId)
        {
            return await FetchAsync(CacheKeyHelper.Membership.Accounts(uniqueIdentifier, serviceId),
                () => _membershipReadRepository.GetAccountAsync(uniqueIdentifier, serviceId));
        }

        public async Task AddAsync(long serviceId, string uniqueIdentifier, PrivatePersonMembership privatePerson, AddressMembership address, List<BankingAccountMembership> accounts)
        {
            //write in sql server
            await _membershipRepository.AddAsync(privatePerson, address, accounts);

            //write person in redis
            await _cacheProvider.StoreAsync(
                key: CacheKeyHelper.Membership.PrivatePerson(uniqueIdentifier, serviceId),
                value: privatePerson,
                expiration: _objectCacheConfig.Value.MembershipExpiration,
                ttl: _objectCacheConfig.Value.Ttl);

            //write address in redis
            await _cacheProvider.StoreAsync(
                key: CacheKeyHelper.Membership.Address(uniqueIdentifier, serviceId),
                value: address,
                expiration: _objectCacheConfig.Value.MembershipExpiration,
                ttl: _objectCacheConfig.Value.Ttl);

            //write account in redis
            await _cacheProvider.StoreAsync(
                key: CacheKeyHelper.Membership.Accounts(uniqueIdentifier, serviceId),
                value: accounts,
                expiration: _objectCacheConfig.Value.MembershipExpiration,
                ttl: _objectCacheConfig.Value.Ttl);

        }
        public async Task UpdateAsync(PrivatePersonMembership model)
        {
            await _membershipRepository.UpdateAsync(model);
        }

    }
}

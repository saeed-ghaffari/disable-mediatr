﻿using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Microsoft.Extensions.Options;

namespace Cerberus.Service.Implementations
{
    public class PermanentOtpService : IPermanentOtpService
    {
        private readonly IPermanentOtpRepository _repository;
        private readonly IPermanentOtpReadRepository _permanentOtpReadRepository;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;
        public PermanentOtpService(IPermanentOtpRepository repository, IPermanentOtpReadRepository permanentOtpReadRepository, IMasterCacheProvider masterCacheProvider, IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration)
        {
            _repository = repository;
            _permanentOtpReadRepository = permanentOtpReadRepository;
            _masterCacheProvider = masterCacheProvider;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }

        public async Task<PermanentOtp> GetAsync(long serviceId, string uniqueIdentifier,OtpTypes type)
        {
            return await _masterCacheProvider.FetchPermanentOtp($"{nameof(PermanentOtpService)}:GetByServiceId:{serviceId}{uniqueIdentifier}{type}",
                async () => await _permanentOtpReadRepository.GetAsync(serviceId, uniqueIdentifier, type),
                _masterCacheProviderConfiguration.PermanentOtpExpirationTime);
          
        }

        public Task CreateAsync(PermanentOtp permanentOtp)
        {
            return _repository.AddAsync(permanentOtp);
        }
    }
}

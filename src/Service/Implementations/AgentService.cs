﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;

namespace Cerberus.Service.Implementations
{
    public class AgentService : IAgentService
    {
        private readonly IAgentRepository _agentRepository;
        private readonly IAgentReadRepository _agentReadRepository;
        private readonly IAgentDocumentRepository _agentDocumentRepository;
        private readonly IAgentDocumentReadRepository _agentDocumentReadRepository;
        private readonly IMapper _mapper;
        private readonly IAgentArchiveRepository _agentArchiveRepository;
        private readonly IAgentDocumentArchiveRepository _agentDocumentArchiveRepository;

        public AgentService(IAgentRepository agentRepository, IAgentDocumentRepository agentDocumentRepository,
            IMapper mapper, IAgentArchiveRepository agentArchiveRepository, IAgentDocumentArchiveRepository agentDocumentArchiveRepository, IAgentReadRepository agentReadRepository, IAgentDocumentReadRepository agentDocumentReadRepository)
        {
            _agentRepository = agentRepository;
            _agentDocumentRepository = agentDocumentRepository;
            _mapper = mapper;
            _agentArchiveRepository = agentArchiveRepository;
            _agentDocumentArchiveRepository = agentDocumentArchiveRepository;
            _agentReadRepository = agentReadRepository;
            _agentDocumentReadRepository = agentDocumentReadRepository;
        }

        public Task CreateAsync(Agent agent)
        {
            return _agentRepository.CreateAsync(agent);
        }

        public Task UpdateAsync(Agent agent)
        {
            return _agentRepository.UpdateAsync(agent);
        }

        public Task<Agent> GetAsync(long id)
        {
            return _agentReadRepository.GetAsync(id);
        }

        public Task<Agent> GetByProfileIdAsync(long profileId)
        {
            return _agentReadRepository.GetByProfileIdAsync(profileId);
        }

        public Task<Agent> GetByProfileIdWithNoLockAsync(long profileId)
        {
            return _agentReadRepository.GetByProfileIdWithNoLockAsync(profileId);
        }

        public Task<Agent> GetByAgentProfileIdAsync(long agentProfileId)
        {
            return _agentReadRepository.GetByAgentProfileIdAsync(agentProfileId);
        }

        public Task DeleteAsync(Agent entity)
        {
            return _agentRepository.DeleteAsync(entity);
        }

        public Task DeleteAsync(long id)
        {
            return _agentRepository.DeleteAsync(id);
        }
        public async Task MoveToArchiveAsync(Agent agent)
        {
            var mapAgentArchive = _mapper.Map<AgentArchive>(agent);
            await _agentArchiveRepository.CreateAsync(mapAgentArchive);

            var agentDoc = await _agentDocumentReadRepository.GetListDocumentByAgentId(agent.Id);
            if (agentDoc.Any())
            {
                foreach (var doc in agentDoc)
                {
                    var map = _mapper.Map<AgentDocumentArchive>(doc);
                    await _agentDocumentArchiveRepository.CreateAsync(map);
                    await _agentDocumentRepository.DeleteAsync(doc);
                }
            }
            await _agentRepository.DeleteAsync(agent);

        }

        public async Task AddToArchive(Agent oldAgent, DateTime fromDate)
        {
            var mapAgentArchive = _mapper.Map<AgentArchive>(oldAgent);
            await _agentArchiveRepository.CreateAsync(mapAgentArchive);
            var agentDoc = await _agentDocumentReadRepository.GetListDocumentByAgentId(oldAgent.Id);
            foreach (var doc in agentDoc.Where(x => x.ModifiedDate < fromDate))
            {
                var map = _mapper.Map<AgentDocumentArchive>(doc);
                await _agentDocumentArchiveRepository.CreateAsync(map);
                await _agentDocumentRepository.DeleteAsync(doc);
            }
        }

        public Task<List<Agent>> GetListByAgentProfileIdAsync(long agentProfileId)
        {
            return _agentReadRepository.GetListByAgentProfileIdAsync(agentProfileId);
        }
    }
}
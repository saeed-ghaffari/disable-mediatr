﻿using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;

namespace Cerberus.Service.Implementations
{
    public class LegalPersonStakeholderService : ILegalPersonStakeholderService
    {
        private readonly ILegalPersonStakeholderRepository _legalPersonStakeholderRepository;
        private readonly ILegalPersonStakeholderReadRepository _legalPersonStakeholderReadRepository;
        public LegalPersonStakeholderService(ILegalPersonStakeholderRepository legalPersonStakeholderRepository, ILegalPersonStakeholderReadRepository legalPersonStakeholderReadRepository)
        {
            _legalPersonStakeholderRepository = legalPersonStakeholderRepository;
            _legalPersonStakeholderReadRepository = legalPersonStakeholderReadRepository;
        }

        public async Task<List<LegalPersonStakeholder>> GetListAsync(long profileId, StakeHolderType type)
        {
            return await _legalPersonStakeholderReadRepository.GetListAsync(profileId, type);
        }

        public async Task<LegalPersonStakeholder> GetAsync(long id)
        {
            return await _legalPersonStakeholderReadRepository.GetAsync(id);
        }

        public async Task CreateAsync(LegalPersonStakeholder model)
        {
            await _legalPersonStakeholderRepository.CreateAsync(model);
        }

        public async Task DeleteAsync(LegalPersonStakeholder model)
        {
            await _legalPersonStakeholderRepository.DeleteAsync(model);
        }
        public async Task<List<LegalPersonStakeholder>> GetListAsync(long profileId)
        {
            return await _legalPersonStakeholderReadRepository.GetListAsync(profileId);
        }

    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;

namespace Cerberus.Service.Implementations
{
    public class PrivatePersonService : IPrivatePersonService
    {
        private readonly IPrivatePersonRepository _privatePersonRepository;
        private readonly IPrivatePersonReadRepository _privatePersonReadRepository;
        public PrivatePersonService(IPrivatePersonRepository privatePersonRepository, IPrivatePersonReadRepository privatePersonReadRepository)
        {
            _privatePersonRepository = privatePersonRepository;
            _privatePersonReadRepository = privatePersonReadRepository;
        }

        public Task CreateAsync(PrivatePerson privatePerson)
        {
            return _privatePersonRepository.CreateAsync(privatePerson);
        }

        public Task UpdateAsync(PrivatePerson privatePerson)
        {
            return _privatePersonRepository.UpdateAsync(privatePerson);
        }

        public Task<PrivatePerson> GetByProfileIdAsync(long profileId)
        {
            return _privatePersonReadRepository.GetByProfileIdAsync(profileId);
        }

        public Task<PrivatePerson> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return _privatePersonReadRepository.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }

        public Task<string> GetIncludeImageAsync(long profileId, ImageType imageType)
        {
            return _privatePersonReadRepository.GetIncludeImageAsync(profileId,imageType);
        }


        public Task<IEnumerable<PrivatePerson>> GetByDaysLeftToEighteenAsync(int days, int status)
        {
            return _privatePersonReadRepository.GetByDaysLeftToEighteenAsync(days, status);
        }
        public async Task<PrivatePerson> GetAsync(long referenceId)
        {
            return await _privatePersonReadRepository.GetAsync(referenceId);

        }

        public Task<PrivatePerson> GetByProfileIdWithNoLockAsync(long profileId)
        {
            return _privatePersonReadRepository.GetByProfileIdWithNoLockAsync(profileId);
        }
    }
}
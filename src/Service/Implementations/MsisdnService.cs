﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;

namespace Cerberus.Service.Implementations
{
  
   public class MsisdnService:IMsisdnService
   {
       private readonly IMsisdnReadRepository _msisdnReadRepository;
       private readonly IMsisdnRepository _msisdnRepository;
       public MsisdnService(IMsisdnReadRepository msisdnReadRepository, IMsisdnRepository msisdnRepository)
       {
           _msisdnReadRepository = msisdnReadRepository;
           _msisdnRepository = msisdnRepository;
       }

       public Task CreateAsync(Msisdn msisdn)
       {
           return _msisdnRepository.CreateAsync(msisdn);
       }

        public Task UpdateAsync(Msisdn msisdn)
        {
            return _msisdnRepository.UpdateAsync(msisdn);
        }

        public Task<Msisdn> GetAsync(long id)
        {
            return _msisdnReadRepository.GetAsync(id);
        }

        public Task<Msisdn> GetByProfileIdAsync(long profileId)
        {
            return _msisdnReadRepository.GetByProfileIdAsync(profileId);
        }
    }
}

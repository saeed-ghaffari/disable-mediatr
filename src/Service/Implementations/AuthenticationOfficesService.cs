﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Microsoft.Extensions.Options;

namespace Cerberus.Service.Implementations
{
    public class AuthenticationOfficesService : IAuthenticationOfficesService
    {
        private readonly IAuthenticationOfficesReadRepository _authenticationOfficesReadRepository;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;
        private readonly IMapper _mapper;
        public AuthenticationOfficesService(IMasterCacheProvider masterCacheProvider,
          IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration, IMapper mapper, IAuthenticationOfficesReadRepository authenticationOfficesReadRepository)
        {
            _masterCacheProvider = masterCacheProvider;
            _mapper = mapper;
            _authenticationOfficesReadRepository = authenticationOfficesReadRepository;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }


        public Task<(List<AuthenticationOffices> authenticationOfficeses, long recordCount)> GetListAsync(AuthenticationOffices offices, int skip, int take)
        {
            return _authenticationOfficesReadRepository.GetListAsync(offices, skip, take);
        }

        public async Task<List<AuthenticateOfficeViewModel>> GetAllAsync()
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(AuthenticationOffices)}:getAllList",
                  async () => _mapper.Map<List<AuthenticationOffices>, List<AuthenticateOfficeViewModel>>(await _authenticationOfficesReadRepository.GetAllAsync()),
                  _masterCacheProviderConfiguration.ExpirationTime);
        }

    }
}

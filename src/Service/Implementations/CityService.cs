﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Microsoft.Extensions.Options;

namespace Cerberus.Service.Implementations
{
    public class CityService : ICityService
    {
        private readonly ICityReadRepository _cityReadRepository;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;
        public CityService(ICityReadRepository cityReadRepository, IMasterCacheProvider masterCacheProvider,IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration)
        {
            _cityReadRepository = cityReadRepository;
            _masterCacheProvider = masterCacheProvider;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }

        public async Task<List<City>> GetListAsync(long provinceId)
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(CityService)}:Province{provinceId}:key",
                async () => await _cityReadRepository.GetListAsync(provinceId),
            _masterCacheProviderConfiguration.ExpirationTime);
          
        }

        public async Task<List<City>> GetListAsync()
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(CityService)}:getAllList",
                async () => await _cityReadRepository.GetListAsync(),
                _masterCacheProviderConfiguration.ExpirationTime);
           
        }

        public async Task<City> GetAsync(long id)
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(CityService)}:city{id}:key",
                async () => await _cityReadRepository.GetAsync(id),
                _masterCacheProviderConfiguration.ExpirationTime);
           
        }
    }
}
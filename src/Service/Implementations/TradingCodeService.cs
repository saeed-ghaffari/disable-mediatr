﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;

namespace Cerberus.Service.Implementations
{
    public class TradingCodeService : ITradingCodeService
    {
        private readonly ITradingCodeRepository _tradingCodeRepository;
        private readonly ITradingCodeReadRepository _tradingCodeReadRepository;
        public TradingCodeService(ITradingCodeRepository tradingCodeRepository, ITradingCodeReadRepository tradingCodeReadRepository)
        {
            _tradingCodeRepository = tradingCodeRepository;
            _tradingCodeReadRepository = tradingCodeReadRepository;
        }

        public Task CreateAsync(TradingCode tradingCode)
        {
            return _tradingCodeRepository.CreateAsync(tradingCode);
        }

        public Task UpdateAsync(TradingCode tradingCode)
        {
            return _tradingCodeRepository.UpdateAsync(tradingCode);
        }

        public Task DeleteAsync(TradingCode tradingCode)
        {
            return _tradingCodeRepository.DeleteAsync(tradingCode);
        }

        public Task<TradingCode> GetAsync(long id)
        {
            return _tradingCodeReadRepository.GetAsync(id);
        }

        public Task<List<TradingCode>> GetListAsync(long profileId)
        {
            return _tradingCodeReadRepository.GetListAsync(profileId);
        }

        public Task<IEnumerable<TradingCode>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return _tradingCodeReadRepository.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }
    }
}
﻿using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Microsoft.Extensions.Options;

namespace Cerberus.Service.Implementations
{
    public class ProvinceService : IProvinceService
    {
        private readonly IProvinceReadRepository _provinceReadRepository ;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;
        public ProvinceService(IMasterCacheProvider masterCacheProvider,IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration, IProvinceReadRepository provinceReadRepository)
        {
            
            _masterCacheProvider = masterCacheProvider;
            _provinceReadRepository = provinceReadRepository;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }

        public async Task<IEnumerable<Province>> GetListAsync(long countryId)
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(ProvinceService)}:Province:{countryId}:key",
                async () => await _provinceReadRepository.GetListAsync(countryId),
            _masterCacheProviderConfiguration.ExpirationTime);
         
        }

        public async Task<IEnumerable<Province>> GetListAsync()
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(ProvinceService)}:GetAllList",
                async () => await _provinceReadRepository.GetListAsync(),
                _masterCacheProviderConfiguration.ExpirationTime);

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Cerberus.Domain.Common;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.StatusData;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.StatusData;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;


namespace Cerberus.Service.Implementations
{
    public class ProfileService : IProfileService
    {
        private readonly IProfileRepository _profileRepository;
        private readonly IProfileReadRepository _profileReadRepository;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly IOptions<MasterCacheProviderConfiguration> _masterCacheOptions;
        private readonly ILogger<ProfileService> _logger;
        private readonly ITaskRepository _taskRepository;

        public ProfileService(IProfileRepository profileRepository,
            IProfileReadRepository profileReadRepository,
            IMasterCacheProvider masterCacheProvider,
            IOptions<MasterCacheProviderConfiguration> masterCacheOptions, ILogger<ProfileService> logger, ITaskRepository taskRepository)
        {
            _profileRepository = profileRepository;
            _profileReadRepository = profileReadRepository;
            _masterCacheProvider = masterCacheProvider;
            _masterCacheOptions = masterCacheOptions;
            _logger = logger;
            _taskRepository = taskRepository;
        }

        public Task<Profile> GetAsync(long id, bool includeAllRelations = false)
        {
            if (!includeAllRelations)
            {
                return GetCachedProfileAsync(id, () => _profileReadRepository.GetAsync(id, false),
                            _masterCacheOptions.Value.ProfileExpirationTime);

            }

            return _profileReadRepository.GetAsync(id, true);
        }


        public Task<Profile> GetWithNoCacheAsync(long id, bool includeAllRelations = false)
        {
            return _profileReadRepository.GetAsync(id, includeAllRelations);

        }
        public Task<Profile> GetByUniqueIdentifierAsync(string uniqueIdentifier,
            bool includeAllRelations = false,
            bool includeAllChildRelations = false,
            ProfileOwnerType? type = null)
        {
            if (!includeAllRelations && !includeAllChildRelations)
            {

                return GetCachedProfileAsync(uniqueIdentifier,
                    () => _profileReadRepository.GetByUniqueIdentifierAsync(uniqueIdentifier, false, false),
                    _masterCacheOptions.Value.ProfileExpirationTime);

            }

            return _profileReadRepository.GetByUniqueIdentifierAsync(uniqueIdentifier,
                includeAllRelations, includeAllChildRelations, type);
        }




        public Task<Profile> GetByUniqueIdentifierWithNoCacheAsync(string uniqueIdentifier, bool includeAllRelations = false,
            bool includeAllChildRelations = false, ProfileOwnerType? type = null)
        {
            return _profileReadRepository.GetByUniqueIdentifierAsync(uniqueIdentifier,
                includeAllRelations, includeAllChildRelations, type);
        }

        public async Task<Profile> CreateAsync(Profile profile)
        {
            await _profileRepository.CreateAsync(profile);
            return profile;
        }

        public async Task<Profile> UpdateAsync(Profile profile)
        {
            await _profileRepository.UpdateAsync(profile);

            //update cache
            await UpdateCacheProfileAsync(profile);

            return profile;
        }

        public async Task UpdateStatusAsync(Profile profile)
        {
            await _profileRepository.UpdateStatusAsync(profile);

            //update cache
            await UpdateCacheProfileAsync(profile);
        }

        public async Task UpdateStatusAndReasonAsync(Profile profile)
        {
            await _profileRepository.UpdateStatusAndReasonAsync(profile);

            //update cache
            await UpdateCacheProfileAsync(profile);
        }

        public Task CreateInTransactionAsync(Profile profile)
        {
            return _profileRepository.CreateInTransactionAsync(profile);
        }

        public async Task<IEnumerable<Profile>> GetGenericAsync(Expression<Func<Profile, bool>> predicate,
             Func<IQueryable<Profile>, IQueryable<Profile>> include = null, Func<IQueryable<Profile>, IOrderedQueryable<Profile>> order = null)
        {
            return await _profileReadRepository.GetGenericAsync(predicate, include, order);
        }

        public async Task<bool> IsExistMobileStatus(long mobile, ProfileStatus status)
        {
            var result = await _profileReadRepository.GetByMobileAsync(mobile, status);
            var resultList = result.ToList();
            return resultList.Any();
        }

        public async Task<bool> RegisterMobileCount(long mobile, ProfileStatus status, int count)
        {
            var result = await _profileReadRepository.GetByMobileAsync(mobile, status);
            var resultList = result.ToList();
            return resultList.Any() && resultList.Count >= count;
        }

        public async Task DeleteAsync(Profile entity)
        {
            await _profileRepository.DeleteAsync(entity);

            await RemoveProfileFromCacheAsync(entity.UniqueIdentifier, entity.Id);

        }

        public async Task<IEnumerable<Profile>> GetProfileListAsync(long fromRow, long toRow, string uniqueIdentifier = null, ProfileOwnerType? type = null, ProfileStatus status = ProfileStatus.SuccessPayment)
        {
            return await _profileReadRepository.GetProfileListAsync(fromRow, toRow, uniqueIdentifier, type, status);
        }

        public Task<int> CountAsync(ProfileOwnerType? type = null, ProfileStatus? status = null)
        {
            return _profileReadRepository.CountAsync(type, status);
        }

        public Task<Profile> GetDataForFactorAsync(long profileId)
        {
            return _profileReadRepository.GetDataForFactorAsync(profileId);
        }

        public Task<Profile> GetRelatedDataWithEntity(long? profileId, string uniqueIdentifier, EntityType entity)
        {
            return _profileReadRepository.GetRelatedDataWithEntityAsync(profileId, uniqueIdentifier, entity);
        }

        public Task<Profile> GetRelatedDataForDashboard(long profileId, ProfileOwnerType type)
        {
            return _profileReadRepository.GetRelatedDataForDashboardAsync(profileId, type);
        }


        public Task<Profile> GetRelatedDataForAgentAsync(long profileId)
        {
            return _profileReadRepository.GetRelatedDataForAgentAsync(profileId);
        }

        public Task<Profile> GetRelatedDataForScopeAsync(long profileId, ProfileOwnerType type)
        {
            return _profileReadRepository.GetRelatedDataForScopeAsync(profileId, type);
        }

 
        public Task<Profile> GetSignatureFileByUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return _profileReadRepository.GetSignatureFileByUniqueIdentifierAsync(uniqueIdentifier);
        }

        public Task<List<Profile>> GetForDeadAsync(string uniqueIdentifier, DateTime birthDate, ProfileStatus status = ProfileStatus.PolicyAccepted)
        {
            return _profileReadRepository.GetForDeadAsync(uniqueIdentifier, birthDate, status);
        }

        public Task<string> GetFullName(long profileId)
        {
            return _profileReadRepository.GetFullName(profileId);

        }
        public async Task<Profile> GetDataForFgmFactorAsync(long profileId)
        {
            return await _profileReadRepository.GetDataForFgmFactorAsync(profileId);
        }

        public Task<Profile> GetWithNoLockAsync(string uniqueIdentifier)
        {
            return GetCachedProfileAsync(KeyGenerator.Profile.GetMasterCacheKey(uniqueIdentifier),
                () => _profileRepository.GetWithNoLockAsync(uniqueIdentifier),
                _masterCacheOptions.Value.ProfileExpirationTime);
        }
        public async Task<IEnumerable<BatchSejamStatusResultViewModel>> GetSejamStatus(string referenceId, List<StatusData> validUniqueIdentifiers)
        {
            return await _profileReadRepository.GetSejamStatus(referenceId, validUniqueIdentifiers);
        }

        public async Task<Profile> GetCachedProfileAsync(string uniqueIdentifier, Func<Task<Profile>> getFromDatabase, TimeSpan expirationTime)
        {
            var profile = await _masterCacheProvider.FetchAsync(
                KeyGenerator.Profile.GetMasterCacheKey(uniqueIdentifier),
                getFromDatabase,
                expirationTime
            );

            if (profile != null)
            {
                await _masterCacheProvider.StoreAsync(
            KeyGenerator.Profile.GetMasterCacheKeyById(profile.Id),
            new CacheIdentifierObject() { UniqueIdentifier = profile.UniqueIdentifier },
            _masterCacheOptions.Value.ProfileExpirationTime);
            }


            return profile;
        }

        public async Task<Profile> GetCachedProfileAsync(long profileId, Func<Task<Profile>> getFromDatabase, TimeSpan expirationTime)
        {
            var result = await _masterCacheProvider.FetchAsync<CacheIdentifierObject>(KeyGenerator.Profile.GetMasterCacheKeyById(profileId));

            if (result?.UniqueIdentifier != null)
                return await _masterCacheProvider.FetchAsync(
                    KeyGenerator.Profile.GetMasterCacheKey(result.UniqueIdentifier),
                    getFromDatabase,
                    _masterCacheOptions.Value.ProfileExpirationTime);

            var profile = await getFromDatabase();

            if (profile == null)
                return default(Profile);

            await _masterCacheProvider.StoreAsync(
                KeyGenerator.Profile.GetMasterCacheKey(profile.UniqueIdentifier),
                profile,
                _masterCacheOptions.Value.ProfileExpirationTime);

            await _masterCacheProvider.StoreAsync(
                KeyGenerator.Profile.GetMasterCacheKeyById(profile.Id),
                new CacheIdentifierObject() { UniqueIdentifier = profile.UniqueIdentifier },
                _masterCacheOptions.Value.ProfileExpirationTime);

            return profile;
        }

        public async Task RemoveProfileFromCacheAsync(string uniqueIdentifier, long profileId)
        {
            try
            {
                await _masterCacheProvider.RemoveAsync(KeyGenerator.Profile.GetMasterCacheKey(uniqueIdentifier));
                await _masterCacheProvider.RemoveAsync(KeyGenerator.Profile.GetMasterCacheKeyById(profileId));
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, $"remove uniqueIdentifier cache failed  for u: {uniqueIdentifier} p:{profileId}");
            }

        }


        public async Task UpdateCacheProfileAsync(Profile profile)
        {
            try
            {
                await _masterCacheProvider.StoreAsync(KeyGenerator.Profile.GetMasterCacheKey(profile.UniqueIdentifier), profile, _masterCacheOptions.Value.ProfileExpirationTime);
            }
            catch
            {
                await _taskRepository.AddAsync(new ScheduledTask
                {
                    Type = CerberusTasksIdentifierHelper.UpdateProfileCache.Value,
                    DueTime = DateTime.Now,
                    GroupId = CerberusTasksIdentifierHelper.UpdateProfileCache.Key,
                    Params = JsonConvert.SerializeObject(profile.Id),
                    RefId = null,
                    Status = TaskStatus.Pending
                });
            }


        }

        public async Task<Profile> GetByUniqueIdentifierFromCacheAsync(string uniqueIdentifier)
        {
            return await _masterCacheProvider.FetchFromCacheAsync<Profile>(KeyGenerator.Profile.GetMasterCacheKey(uniqueIdentifier));
        }

        public async Task UpdateIsConfirmedEntryNodeAsync(Profile profile, long paymentId)
        {
            await _profileRepository.UpdateIsConfirmedEntryNodeAsync(profile.Id, profile.Status, paymentId);

            await UpdateCacheProfileAsync(profile);
        }

        public async Task<IEnumerable<BatchPrivatePersonResultViewModel>> GetBatchPrivatePerson(string referenceId, List<StatusData> validUniqueIdentifiers)
        {
            return await _profileReadRepository.GetBatchPrivatePerson(referenceId, validUniqueIdentifiers);
        }

        public Task<Profile> GetAuthenticatorProfileByUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return _profileReadRepository.GetAuthenticatorProfileByUniqueIdentifierAsync(uniqueIdentifier);
        }
    }
}
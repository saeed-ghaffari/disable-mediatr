﻿using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Microsoft.Extensions.Options;
using System.Net;
using System;
using System.Net.Http.Headers;
using Cerberus.Service.Model;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Cerberus.Domain;
using Cerberus.Domain.ViewModel;
using Cerberus.Utility;
using FluentFTP;

namespace Cerberus.Service.Implementations
{
    public class FileService : IFileService
    {
        private readonly IFileRepository _fileRepository;
        private readonly FileStorageConfiguration _fileStorageConfiguration;
        private readonly ILogger<FileService> _logger;

        public FileService(
           IFileRepository fileRepository,
            IMapper mapper,
            IOptions<FileStorageConfiguration> fileStorageConfiguration,
            ILogger<FileService> logger)
        {
            _fileRepository = fileRepository;
            _fileStorageConfiguration = fileStorageConfiguration.Value;
            _logger = logger;
        }

        public async Task CreateAsync(File file, byte[] arrayOfFile, string path)
        {
            //1.write file in cdn servers

            if (arrayOfFile.Length > 2000000 )
            {
                _logger.LogCritical(arrayOfFile.Length,$"The file size is not valid and too large : {arrayOfFile.Length} ");
                throw new CerberusException(Domain.Enum.ErrorCode.ImageTooLarge  , "Invalid size");
            }

            if (arrayOfFile.Length < 5000)
            {
                _logger.LogCritical(arrayOfFile.Length, $"The file size is not valid and too small : {arrayOfFile.Length} ");
                throw new CerberusException(Domain.Enum.ErrorCode.ImageTooSmall, "Invalid size");
            }

            var base64File = Convert.ToBase64String(arrayOfFile);
            var mimetype = base64File.GetFileExtension();
            if (mimetype == "image/png" || mimetype == "image/jpg" || mimetype == "image/jpeg" || mimetype == "application/pdf")
            {
                //todo: why ????? 

                if (_fileStorageConfiguration.WriteIntoCdn)
                {
                    var uploadFile = new SejamCdnRequest
                    {
                        FileInput = base64File,
                        FileName = file.FileName,
                        MediaType = mimetype,

                    };

                    try
                    {
                        var cdnResponse = await HttpHepler.PostAsync<SejamCdnResponse>(
                                      _fileStorageConfiguration.CdnEndPoint, uploadFile, "application/json");

                        if (cdnResponse.StatusCode != HttpStatusCode.OK || cdnResponse.Body == null ||
                            !cdnResponse.Body.Successful)
                        {
                            _logger.LogCritical($"Message: {cdnResponse?.Body?.Message}");
                            throw new CerberusException(Domain.Enum.ErrorCode.UnknownServerError,
                                "unable to write into CDN");
                        }

                        file.MimeType = mimetype;
                        file.FileName = cdnResponse.Body.FileGuid;
                    }
                    catch (Exception e)
                    {

                        _logger.LogCritical(e,"unable to connect to CDN server");
                        throw new CerberusException(Domain.Enum.ErrorCode.UnknownServerError,
                            "unable to connect to CDN server");
                    }

                }

                //2.write file in ftp
                if (_fileStorageConfiguration.WriteIntoFtpServer)
                {
                    var localFileName = $"{Guid.NewGuid()}{System.IO.Path.GetExtension(file.Title)}";

                    using (var ftpClient = new FtpClient(_fileStorageConfiguration.FtpEndPoint, new NetworkCredential()
                    {
                        UserName = _fileStorageConfiguration.FtpUserName,
                        Password = _fileStorageConfiguration.FtpPassword
                    }))
                    {
                        await ftpClient.ConnectAsync();
                        if (!await ftpClient.DirectoryExistsAsync(path))
                            await ftpClient.CreateDirectoryAsync(path);

                        await ftpClient.UploadAsync(arrayOfFile, $"{path}/{localFileName}", FtpExists.NoCheck);
                    }

                    file.LocalFileName = localFileName;
                    if (!_fileStorageConfiguration.WriteIntoCdn)
                        file.FileName = localFileName;
                }

                if (_fileStorageConfiguration.WriteIntoFileService)
                {
                    var uploadFile = new
                    {
                        fileString = base64File,
                        filename = file.FileName,
                    };

                    try
                    {
                        var httpResponse = await HttpHepler.PostAsync<Envelop<FileServiceResponse>>(
                            $"{_fileStorageConfiguration.FileServiceEndPoint}/files", uploadFile, "application/json",
                            TimeSpan.FromSeconds(60),
                            new AuthenticationHeaderValue("bearer", _fileStorageConfiguration.FileServiceToken));

                        if (httpResponse.StatusCode != HttpStatusCode.OK || httpResponse.Body?.Data == null)
                        {
                            _logger.LogCritical(
                                $"unable to write into fileService: {httpResponse.Body?.Meta?.OperationResult} \r\n errorMessage:{httpResponse.Body?.Meta?.Message} ");

                            throw new CerberusException(Domain.Enum.ErrorCode.UnknownServerError,
                                "unable to write into FileService");
                        }

                        if (string.IsNullOrEmpty(httpResponse.Body.Data.Reference))
                        {
                            _logger.LogCritical(
                                $"unable to write into fileService: {httpResponse.Body?.Meta?.OperationResult} \r\n errorMessage:{httpResponse.Body?.Meta?.Message} ");
                            throw new CerberusException(Domain.Enum.ErrorCode.UnknownServerError,
                                "unable to write into FileService");
                        }
                        file.MimeType = mimetype;
                        file.FileName = httpResponse.Body.Data.Reference;
                        await _fileRepository.CreateAsync(file);
                    }
                    catch (Exception e)
                    {
                        _logger.LogCritical(e, "unable to connect to file service");
                        throw new CerberusException(Domain.Enum.ErrorCode.UnknownServerError,
                            "unable to connect to file service");
                    }
                    
                }
                else
                {

                    _logger.LogCritical("The configurations is off for sending to the server");
                    throw new CerberusException(Domain.Enum.ErrorCode.UnknownServerError,
                        "Invalid mime Type");
                }
            }
            else
            {
                _logger.LogCritical("The file is not valid for sending to the server", JsonConvert.SerializeObject(mimetype));
                throw new CerberusException(Domain.Enum.ErrorCode.UnsupportedMediaType,
                    "Invalid mime Type");
            }
        }

        public async Task DeleteAsync(long id)
        {
            await _fileRepository.DeleteAsync(id);
        }
    }
}
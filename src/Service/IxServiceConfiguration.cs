﻿using System;

namespace Cerberus.Service
{
    public class IxServiceConfiguration
    {
        public string IxAwaGetTradeCodeUrl { get; set; }
        public string IxAwaGetAccessTokenUrl { get; set; }
        public string IxAwaRefreshAccessTokenUrl { get; set; }
        public TimeSpan IxAccessTokenTtl { get; set; }
        public bool EnableIxService { get; set; }
        public bool EnableLegacyCodeFromDb { get; set; }
    }
}
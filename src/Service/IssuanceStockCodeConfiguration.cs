﻿using System;
using System.Collections.Generic;

namespace Cerberus.Service
{
    public class IssuanceStockCodeConfiguration
    {
        public string Url { get; set; }
        public int RecordCountToSend { get; set; }
        public int RequestTimeOut { get; set; }

        public string SuccessIssuanceMessage { get; set; }
        public string UnSuccessIssuanceMessage { get; set; }

        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }
        public bool EnableSendErrorMessage { get; set; }
        public List<string> SendErrorMobiles { get; set; }
        public int Delay { get; set; }
        public byte Divisor { get; set; }
        public byte Mode { get; set; }
        public bool GetCurrentDay { get; set; }
        public byte BadResponseTryCount { get; set; }

    }
}
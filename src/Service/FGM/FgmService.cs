﻿using System;
using System.Net;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services.FGM;
using Cerberus.Domain.ViewModel;
using Cerberus.Domain.ViewModel.FGM;
using Cerberus.Service.BankService.Configuration;
using Cerberus.Service.Model.FGM;
using Cerberus.Utility;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Cerberus.Service.FGM
{
    public class FgmService : IFgmService
    {
        private readonly FgmConfiguration _configuration;
        private readonly ILogger<FgmService> _logger;
        private readonly IDatabase _readDatabase;
        private const string PreFix = "FgmAccessTokenForSejamApplication";
        public FgmService(IOptions<FgmConfiguration> configuration, ILogger<FgmService> logger, IDatabase readDatabase)
        {
            _logger = logger;
            _readDatabase = readDatabase;
            _configuration = configuration.Value;
        }

        public async Task<AuthenticationHeaderValue> GetAuthenticationHeaderValueAsync()
        {
            var customAccessToken = await GetAccessTokenService();
            return new AuthenticationHeaderValue("bearer", customAccessToken.AccessToken);
        }

        public async Task<string> GetPaymentToken(string serviceId, long orderId, int amount)
        {
            var dataBytes = Encoding.UTF8.GetBytes($"{serviceId};{orderId};{amount}");
            var symmetric = SymmetricAlgorithm.Create("TripleDes");
            symmetric.Mode = CipherMode.ECB;
            symmetric.Padding = PaddingMode.PKCS7;
            var encryption = symmetric.CreateEncryptor(Convert.FromBase64String(_configuration.Token),
                new byte[8]);

            var signData = Convert.ToBase64String(encryption.TransformFinalBlock(dataBytes, 0, dataBytes.Length));

            try
            {
                FgmService fgmService = this;
                var httpResponse = await HttpHepler.PostAsync<Envelop<PaymentTokenResponse>>($"{_configuration.ApiAddress}/v0.1/accessToken/PaymentToken", JsonConvert.SerializeObject((object)new
                {
                    username = _configuration.UserName,
                    password = _configuration.Password,
                    ServiceId = serviceId,
                    CallbackUrl = _configuration.CallbackUrl,
                    Amount = amount,
                    IsCalculatedAmount = false,
                    OrderId = orderId,
                    SignData = signData
                }), "application/json", null, await fgmService.GetAuthenticationHeaderValueAsync());
                if (httpResponse.Body.Error != null)
                {
                    _logger.LogCritical($"FGM payment token errorCode: {httpResponse.Body?.Error?.ErrorCode} \r\n errorMessage:{httpResponse.Body?.Error?.Exception} ");
                    return string.Empty;
                }

                return httpResponse.Body.Data.PaymentToken;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "unable to payment Token Service");
                return string.Empty;
            }
        }

        public async Task<Envelop<VerifyPaymentResponse>> VerifyPayment(string token)
        {
            try
            {
                var body = new { Token = token };
                FgmService fgmService = this;
                var responseResult = await HttpHepler.PostAsync($"{_configuration.ApiAddress}/v0.1/payments/verify",
                    JsonConvert.SerializeObject(body), "application/json", null,
                    await fgmService.GetAuthenticationHeaderValueAsync());
                return JsonConvert.DeserializeObject<Envelop<VerifyPaymentResponse>>(responseResult.Body);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "unable to verifyPayment Service");
                return new Envelop<VerifyPaymentResponse>()
                {
                    Error = new Error { ErrorCode = (int)HttpStatusCode.RequestTimeout }
                };
            }
        }

        public async Task<Envelop<CallbackResponseViewModel>> CheckPaymentStatus(string orderId, Guid serviceId)
        {
            var fgmService = this;
            try
            {
                var paymentStatusResult = await HttpHepler.GetContentAsync(
                    $"{_configuration.ApiAddress}/v0.1/payments/{orderId}/{serviceId}/paymentStatus",
                    await fgmService.GetAuthenticationHeaderValueAsync());

                return JsonConvert.DeserializeObject<Envelop<CallbackResponseViewModel>>(paymentStatusResult);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "unable to paymentStatus Service");
                return new Envelop<CallbackResponseViewModel>()
                {
                    Error = new Error { ErrorCode = (int)HttpStatusCode.RequestTimeout }
                };
            }
        }

        public async Task<Envelop<CallbackResponseViewModel>> CheckOtherPaymentStatus(string orderId, Guid serviceId)
        {
            var fgmService = this;
            try
            {
                var paymentStatusResult = await HttpHepler.GetContentAsync(
                    $"{_configuration.ApiAddress}/v0.1/payments/{orderId}/{serviceId}/paymentResult",
                    await fgmService.GetAuthenticationHeaderValueAsync());

                return JsonConvert.DeserializeObject<Envelop<CallbackResponseViewModel>>(paymentStatusResult);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "unable to paymentStatus Service");
                return new Envelop<CallbackResponseViewModel>
                {
                    Error = new Error { ErrorCode = (int)HttpStatusCode.RequestTimeout }
                };
            }
        }

        public async Task<Envelop<FactorResponseModel>> SetFactorNumber(FactorRequestModel model)
        {
            return await SetFactor(model, "/v0.1/payments/factor");
        }

        public async Task<Envelop<FactorResponseModel>> SetFactorForOtherPayment(FactorRequestModel model)
        {
            return await SetFactor(model, "/v0.1/payments/setFactor");
        }
        public async Task<Envelop<HttpStatusCode>> CreatePayment(PaymentRequest paymentRequest)
        {
            try
            {
                FgmService fgmService = this;
                var responseResult = await HttpHepler.PostAsync($"{_configuration.ApiAddress}/v0.1/payments/pay",
                    JsonConvert.SerializeObject(paymentRequest), "application/json", null,
                    await fgmService.GetAuthenticationHeaderValueAsync());
                if (responseResult.StatusCode != HttpStatusCode.OK && responseResult.StatusCode != HttpStatusCode.Conflict)
                {
                    _logger.LogCritical($"unable to create payment for {paymentRequest.ClientRefId} ;\r\n error:{responseResult.Body} ");
                    return new Envelop<HttpStatusCode>()
                    {
                        Error = new Error { ErrorCode = (int)responseResult.StatusCode }
                    };
                }
                return JsonConvert.DeserializeObject<Envelop<HttpStatusCode>>(JsonConvert.SerializeObject(new { Data = responseResult.StatusCode.ToString() }));
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "unable to verifyPayment service");
                throw;
            }
        }
        public async Task<CustomAccessToken> GetAccessTokenService()
        {
            var token = await _readDatabase.StringGetAsync(PreFix);
            if (token != RedisValue.Null)
            {
                var accessToken = JsonConvert.DeserializeObject<CustomAccessToken>(token);
                if (accessToken.ExpiredAt > DateTime.Now.AddSeconds(40)) return accessToken;
            }

            try
            {
                var httpResponse = await HttpHepler.PostAsync<Envelop<AccessTokenResponse>>(
                    $"{_configuration.ApiAddress}/v0.1/accessToken", JsonConvert.SerializeObject((object)new
                    {
                        username = _configuration.UserName,
                        password = _configuration.Password

                    }), "application/json");

                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    _logger.LogCritical($"FGM AccessToken Error StatusCode:{httpResponse.StatusCode}. \r\n Message:{httpResponse?.Body?.Error.CustomMessage}");

                    return new CustomAccessToken();
                }

                var access = new CustomAccessToken()
                {
                    ExpiredAt = DateTime.Now.Add(httpResponse.Body.Data.Ttl),
                    AccessToken = httpResponse.Body.Data.AccessToken
                };
                var serialize = JsonConvert.SerializeObject(access);
                await _readDatabase.StringSetAsync(PreFix, serialize, httpResponse.Body.Data.Ttl);

                return access;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "Unable Connect to Access Token Service ");
                return new CustomAccessToken();
            }


        }
     

        private async Task<Envelop<FactorResponseModel>> SetFactor(FactorRequestModel model, string route)
        {
            try
            {
                FgmService fgmService = this;
                var responseResult = await HttpHepler.PostAsync($"{_configuration.ApiAddress}{route}",
                    JsonConvert.SerializeObject(model), "application/json", null,
                    await fgmService.GetAuthenticationHeaderValueAsync());
                if (responseResult.StatusCode != HttpStatusCode.OK)
                {

                    _logger.LogCritical($"unable to setFactorNumber for {model.OrderId} ; \r\n error: {responseResult.Body}");
                    return new Envelop<FactorResponseModel>()
                    {
                        Error = new Error { ErrorCode = (int)responseResult.StatusCode }
                    };
                }
                return JsonConvert.DeserializeObject<Envelop<FactorResponseModel>>(responseResult.Body);

            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "unable to setFactorNumber service ");
                return new Envelop<FactorResponseModel>()
                {
                    Error = new Error { ErrorCode = (int)HttpStatusCode.RequestTimeout }
                };
            }
        }
    }
}

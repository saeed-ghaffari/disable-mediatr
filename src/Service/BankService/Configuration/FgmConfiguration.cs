﻿namespace Cerberus.Service.BankService.Configuration
{
    public class FgmConfiguration
    {
        public string ApiAddress { get; set; }
        public string WebAddress { get; set; }
        public string CallbackUrl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string RegisterServiceId { get; set; }
        public string EditServiceId { get; set; }

    }
}

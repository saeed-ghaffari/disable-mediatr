﻿namespace Cerberus.Service
{
    internal class CacheKeyHelper
    {
        private const string PreFix = "service";

        public static class Membership
        {
            public static string PrivatePerson(string uniqueIdentifier, long serviceId)
            {
                return $"{PreFix}:Membership:{uniqueIdentifier}:Person:{serviceId}";
            }

            public static string Address(string uniqueIdentifier, long serviceId)
            {
                return $"{PreFix}:Membership:{uniqueIdentifier}:Address:{serviceId}";
            }

            public static string Accounts(string uniqueIdentifier, long serviceId)
            {
                return $"{PreFix}:Membership:{uniqueIdentifier}:Accounts:{serviceId}";
            }
        }

        public static class ClientCertificate
        {
            public static string Client(string clientId)
            {
                return $"{PreFix}:ClientCertificate:{clientId}";
            }

        }
    }
}
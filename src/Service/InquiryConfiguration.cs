﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Service
{
    public class InquiryConfiguration
    {
        public string BaseApiUrl { get; set; }
        public string IbanInquiryUrl { get; set; }
        public string IlencInquiryUrl { get; set; }
        public string NocrInquiryUrl { get; set; }
        public string IndividualInquiryUrl { get; set; }
        public string InstitutionInquiryUrl { get; set; }
        public string LegacyInquiryUrl { get; set; }
        public string IrenexInquiryUrl { get; set; }
        public int NocrInquiryTtl { get; set; }
        public int IbanInquiryTtl { get; set; }
        public int LegacyInquiryTtl { get; set; }
        public int IrenexInquiryTtl { get; set; }
        public int IlencInquiryTtl { get; set; }
        public int IndividualInquiryTtl { get; set; }
        public int InstitutionInquiryTtl { get; set; }
        public int AgentVarificationNextTime { get; set; }
        public int MaxRecoverySequence { get; set; }
        public string InquiryNotConfirmedMessage { get; set; }
        public string ShahkarInquiryNotConfirmedMessage { get; set; }
        public string VerificationAgentInquiryMessage { get; set; }
        public int InquiryCheckNextTime { get; set; }
        public int TraceCodeLength { get; set; }

        public string RealTraceCodeMessage { get; set; }
        public string LegalTraceCodeMessage { get; set; }

        public string DirectorateNotificationMessage { get; set; }
        public string IbanInquiryTitle { get; set; }
        public string LegacyCodeInquiryTitle { get; set; }
        public string IrenexInquiryTitle { get; set; }
        public string IlencInquiryTitle { get; set; }
        public string IndividualInquiryTitle { get; set; }
        public string InstituteInquiryTitle { get; set; }
        public string NocrInquiryTitle { get; set; }
        public string AgentInquiryTitle { get; set; }
        public string ManagerTitle { get; set; }
        public string TakeAccessTitle { get; set; }
        public string OrderAccessTitle { get; set; }
        public string UnderEighteenYearsOldMessage { get; set; }
        public int CheckRecuringTaskInterval { get; set; }
        public int IbanInquiryTimeToLater { get; set; }
        public string ConfirmEditBanckingAccount { get; set; }
        public string ConfirmUpdatePrivatePerson { get; set; }
        public string NotConfirmUpdatePrivatePerson { get; set; }
        public string NotConfirmUpdateBankingAccount { get; set; }
        public string CheckAgentParentMessage { get; set; }
        public string RejectAgentDescription { get; set; }
        public string NotChooseProvinceAgent { get; set; }
        public string NotChooseProvinceAgentDescription { get; set; }
        public string PostInquiryUrl { get; set; }
        public int PostInquiryTtl { get; set; }
        public int SetFactorNextExecuteTime { get; set; }
        public string DeadInquiryRequestUrl { get; set; }
        public string DeadInquiryDownloadUrl { get; set; }
        public int DeadInquiryFromAddDay { get; set; }
        public int DeadInquiryToAddDay { get; set; }
        public int DeadAddTimeToDownloadRequest { get; set; }

        public string RemoveOldMobileMessage { get; set; }
        public string RegisterNewMobileMessage { get; set; }

    }
}

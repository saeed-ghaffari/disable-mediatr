﻿namespace Cerberus.Service
{
    public class FileStorageConfiguration
    {
        public bool WriteIntoCdn { get; set; }

        public bool WriteIntoFtpServer { get; set; }

        public string FtpEndPoint { get; set; }

        public string FtpUserName { get; set; }

        public string FtpPassword { get; set; }

        public string CdnEndPoint { get; set; }

        public string CdnFilePath { get; set; }

        public string PrivatePersonImagePath { get; set; }

        public string PrivatePersonSignaturePath { get; set; }

        public string AgentFileSignaturePath { get; set; }

        public string StackHolderTakeAccessSignaturePath { get; set; }

        public string StackHolderTakeOrderSignaturePath { get; set; }
        public string FileServiceDownloadUrl { get; set; }
        public bool WriteIntoFileService { get; set; }
        public string FileServiceEndPoint { get; set; }
        public string FileServiceToken { get; set; }
    }
}
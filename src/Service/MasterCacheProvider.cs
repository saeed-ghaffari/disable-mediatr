﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Google.Protobuf;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Cerberus.Service
{
    public class MasterCacheProvider : IMasterCacheProvider
    {
        private readonly ILogger<MasterCacheProvider> _logger;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;
        private readonly IDatabase _readDatabase;
        private readonly IDatabase _writeDatabase;
        public MasterCacheProvider(ILogger<MasterCacheProvider> logger, IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration, IDatabase readDatabase, IDatabase writeDatabase)
        {
            _logger = logger;
            _readDatabase = readDatabase;
            _writeDatabase = writeDatabase;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }

        public async Task<T> FetchAsync<T>(string key, Func<Task<T>> getFromDatabase, TimeSpan expirationTime)
        {
            if (!_masterCacheProviderConfiguration.IsActive)
                return await getFromDatabase();

            try
            {
                var value = await _readDatabase.StringGetAsync(key);
                if (!value.IsNull && value.HasValue) return JsonConvert.DeserializeObject<T>(value);
                var result = await getFromDatabase();

                if (result == null)
                    return default(T);

                await StoreAsync(key, result, expirationTime);

                return result;

            }
            catch (Exception e)
            {
                _logger.LogCritical(e, e.Message);
                throw;
            }

        }


        public async Task<T> FetchFromCacheAsync<T>(string key)
        {

            try
            {
                var value = await _readDatabase.StringGetAsync(key);
                if (!value.IsNull && value.HasValue) return JsonConvert.DeserializeObject<T>(value);
                return default(T);
                
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, e.Message);
                throw;
            }

        }

        public async Task<PermanentOtp> FetchPermanentOtp(string key, Func<Task<PermanentOtp>> getFromDatabase, TimeSpan expirationTime)
        {
            if (!_masterCacheProviderConfiguration.IsActive)
                return await getFromDatabase();

            try
            {
                var value = await _readDatabase.StringGetAsync(key);
                if (!value.IsNull && value.HasValue)
                {
                    var d = JsonConvert.DeserializeObject<PermanentOtp>(value);
                    if (d !=null && d.ExpirationDate > DateTime.Now)
                        return d;
                }

                var result = await getFromDatabase();

                if (result == null || result.ExpirationDate < DateTime.Now)
                    return null;

                await StoreAsync(key, result, expirationTime);

                return result;

            }
            catch (Exception e)
            {
                _logger.LogCritical(e, e.Message);
                throw;
            }
        }

        public async Task<T> FetchAsync<T>(string key, bool throwException = true)
        {
            if (!_masterCacheProviderConfiguration.IsActive)
            {
                _logger.LogCritical("MasterCacheProvider is not active");

                if (throwException)
                    throw new InvalidOperationException("MasterCacheProvider is not active");

                return default(T);
            }

            try
            {
                var value = await _readDatabase.StringGetAsync(key);

                if (value.IsNull || !value.HasValue)
                    return default(T);

                return JsonConvert.DeserializeObject<T>(value);

            }
            catch (Exception e)
            {
                _logger.LogCritical(e, e.Message);
                throw;
            }
        }

        public async Task StoreAsync<T>(string key, T value, TimeSpan expirationTime)
        {
            try
            {
                var valueBytes = JsonConvert.SerializeObject(value, Formatting.None,
                    new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                await _writeDatabase.StringSetAsync(key, valueBytes, expirationTime);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "StoreAsync Has Exception");
                throw;
            }


        }

        public Task RemoveAsync(string key, TimeSpan? removeAt = null)
        {
            return removeAt.HasValue
                ? _writeDatabase.KeyExpireAsync(key, removeAt)
                : _writeDatabase.KeyDeleteAsync(key);
        }
    }
}
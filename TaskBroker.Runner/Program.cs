﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using TaskBroker.MicroServices.Domain;
using MediatR;
using TaskBroker.MicroServices.Implementation.SendSms.Handlers;
using TaskBroker.Domain.Bus;
using TaskBroker.Domain.Events;
using TaskBroker.MicroServices.Implementation.SendSms.Infrastructure;
using TaskBroker.MicroServices.Implementation.SendSms.Services;
using TaskBroker.Domain.Commands;
using TaskBroker.Infrastructure.Bus;
using Microsoft.Extensions.Options;
using TaskBroker.MicroServices.Implementation.SendSms.Data;
using TaskBroker.MicroServices.Domain.Interfaces;

namespace TaskBroker.Runner
{
    class Program
    {

        public static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();

            Startup startup = new Startup();
            startup.ConfigureServices(services);
            IServiceProvider serviceProvider = services.BuildServiceProvider();

            //serviceProvider
            //    .GetService<ILoggerFactory>()
            //    .AddConsole(LogLevel.Debug);

            //var logger = serviceProvider.GetService<ILoggerFactory>()
            //    .CreateLogger<Program>();

            //logger.LogDebug("Logger is working!");

            // Get Service and call method
            //var service = serviceProvider.GetService<IMyService>();
            //service.MyServiceMethod();

            var eventBus = serviceProvider.GetService<IEventBus>(); // app.ApplicationServices.GetRequiredService<IEventBus>();
            eventBus.Subscribe<SendSmsEventHandler>(new Event { RouteKey = "Sejam.SendSMS" });
        }

        //public static IHostBuilder CreateHostBuilder(string[] args) =>
        //    Host.CreateDefaultBuilder(args)
        //        .ConfigureWebHostDefaults(webBuilder =>
        //        {
        //            webBuilder.UseStartup<Startup>();
        //        });



        //private static IServiceProvider _serviceProvider;

        //static void Main(string[] args)
        //{

        //    var builder = new ConfigurationBuilder()
        //                        .SetBasePath(Directory.GetCurrentDirectory())
        //                        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

        //    IConfigurationRoot configuration = builder.Build();

        //    var services = new ServiceCollection();
        //    _serviceProvider = services.BuildServiceProvider(true);

        //    var appSettings = configuration.GetSection("AppSettings");
        //    services.Configure<ApplicationSetting>(appSettings);

        //    var rabbitSettings = configuration.GetSection("RabbitMQ");
        //    services.Configure<RabbitMQSettings>(rabbitSettings);


        //    // Domain bus.
        //    services.AddSingleton<IEventBus, RabbitMQBus>(serviceProvider =>
        //    {
        //        var mediator = serviceProvider.GetService<IMediator>();
        //        var serviceScopeFactory = serviceProvider.GetService<IServiceScopeFactory>();
        //        var rabbitSettings = serviceProvider.GetService<IOptions<RabbitMQSettings>>();
        //        return new RabbitMQBus(mediator, serviceScopeFactory, rabbitSettings.Value.Hostname, rabbitSettings.Value.Port);
        //    });

        //    //Services.
        //    services.AddTransient<ISmsService, SmsService>();

        //    //Commands
        //    services.AddTransient<IEnqueueCommandHandler, EnqueueCommandHandler>();

        //    // Event Handlers.
        //    services.AddTransient<SendSmsEventHandler>();

        //    // Data services.
        //    services.AddTransient<ISmsRepository, SmsRepository>();

        //    // ConnectionManager
        //    services.AddSingleton<IConnectionManager, ConnectionManager>();

        //    services.AddTransient<Program>();

        //    //DependencyContainer.ConfigureServices(services);

        //    IServiceScope scope = _serviceProvider.CreateScope();


        //    var eventBus = scope.ServiceProvider.GetRequiredService<IEventBus>();
        //    eventBus.Subscribe<SendSmsEventHandler>(new Event { RouteKey = "Sejam.SendSMS" });



        //    Console.WriteLine("Hello World!");
        //    System.Console.ReadLine();
        //}
    }
}

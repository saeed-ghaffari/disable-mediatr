﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskBroker.Runner
{
    public class RabbitMQSettings
    {
        public string Hostname { get; set; }

        public int Port { get; set; }
    }
}

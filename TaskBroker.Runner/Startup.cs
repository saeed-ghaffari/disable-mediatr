﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using TaskBroker.Domain.Bus;
using TaskBroker.Domain.Events;
using TaskBroker.MicroServices.Domain;
using TaskBroker.MicroServices.Implementation.SendSms.Handlers;
using MediatR;

namespace TaskBroker.Runner
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
        }


        public void ConfigureServices(IServiceCollection services)
        {
            var appSettings = Configuration.GetSection("AppSettings");
            services.Configure<ApplicationSetting>(appSettings);

            var rabbitSettings = Configuration.GetSection("RabbitMQ");
            services.Configure<RabbitMQSettings>(rabbitSettings);

            services.AddMediatR(typeof(Startup));

            DependencyContainer.ConfigureServices(services);
        }

        public void Configure(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            eventBus.Subscribe<SendSmsEventHandler>(new Event { RouteKey = "Sejam.SendSMS" });
        }

    }
}

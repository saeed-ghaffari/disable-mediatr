﻿using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using TaskBroker.Domain.Commands;
using TaskBroker.Domain.Bus;
using TaskBroker.Infrastructure.Bus;
using TaskBroker.MicroServices.Implementation.SendSms.Infrastructure;
using TaskBroker.MicroServices.Implementation.SendSms.Services;
using TaskBroker.MicroServices.Implementation.SendSms.Handlers;
using TaskBroker.MicroServices.Implementation.SendSms.Data;
using TaskBroker.MicroServices.Domain.Interfaces;
using TaskBroker.MicroServices.Domain;
using MediatR;

namespace TaskBroker.Runner
{
    public static class DependencyContainer
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            // Domain bus.
            services.AddSingleton<IEventBus, RabbitMQBus>(serviceProvider =>
            {
                var mediator = serviceProvider.GetService<IMediator>();
                var serviceScopeFactory = serviceProvider.GetService<IServiceScopeFactory>();
                var rabbitSettings = serviceProvider.GetService<IOptions<RabbitMQSettings>>();
                return new RabbitMQBus(mediator, serviceScopeFactory, rabbitSettings.Value.Hostname, rabbitSettings.Value.Port);
            });

            //Services.
            services.AddTransient<ISmsService, SmsService>();

            //Commands
            services.AddTransient<IEnqueueCommandHandler, EnqueueCommandHandler>();

            // Event Handlers.
            services.AddTransient<SendSmsEventHandler>();

            // Data services.
            services.AddTransient<ISmsRepository, SmsRepository>();

            // ConnectionManager
            services.AddSingleton<IConnectionManager, ConnectionManager>();
        }
    }
}
